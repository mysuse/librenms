#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy ... \n\n";

$sql="SELECT * FROM bills WHERE bill_id='37' ORDER BY bill_id";

//echo ("SQL--Query--> " . $sql ."\n\n");

foreach (dbFetchRows($sql) as $bill) {
       // echo str_pad($bill['bill_id'].' '.$bill['bill_name'], 30)." \n";
   
        unset($class);
        unset($rate_data);
        

        $query="SELECT timestamp,period,delta,in_delta,out_delta FROM bill_data WHERE timestamp BETWEEN '2017-06-22 00:00:00' AND '2017-06-22 23:59:00' AND bill_id ='37' ORDER BY timestamp DESC";
        
        foreach (dbFetchRows($query) as $lastbilldata) {

          echo ("Lastbilldata Timestamp" . $lastbilldata['timestamp'] ."\n\n");

       // if ($lastbilldata['state']=='ok') {

            $allowed      = $bill['bill_cdr'];
            $used         = $rate_data['rate_95th'];
            $allowed_text = format_si($allowed).'bps';
            $used_text    = format_si($used).'bps';
            $overuse      = ($used - $allowed);
            $overuse      = (($overuse <= 0) ? '0' : $overuse);
            $percent      = round((($rate_data['rate_95th'] / $bill['bill_cdr']) * 100), 2);

            $rate_95th_in = $lastbilldata['in_delta']/$lastbilldata['period'] * 8;

            $rate_95th_out = $lastbilldata['out_delta']/$lastbilldata['period'] * 8;

            $rate_95th = $rate_95th_out;

            if ($rate_95th_in>$rate_95th_out) {

                $rate_95th = $rate_95th_in;
            }

            $percent      = round((($rate_95th / $bill['bill_cdr']) * 100), 2);     

            echo ("Bill_Daily value -- > rate_95th_in " . $lastbilldata['in_delta']);
            echo ("Bill_Daily value -- > rate_95th_out " . $lastbilldata['out_delta']);
            echo ("Bill_Daily value -- > Period " . $lastbilldata['period']);
            echo ("Bill_Daily value -- > rate_95th " . $rate_95th);
            echo ("Percentage -- > percentage " . $percent);

           /* Not updating tables, just print on console
             $now = dbFetchCell('SELECT NOW()');
                
                $bill_daily = array(
                   'bill_id' => $bill['bill_id'],
                    'bill_day'=>$now,
                    'bill_cdr'=> $bill['bill_cdr'],
                    'rate_95th_in'=> $rate_95th_in,
                    'rate_95th_out'=> $rate_95th_out,
                    'rate_95th'=> $rate_95th,    
                    'percentage'=> $percent,  
                );            
                
                dbInsert($bill_daily,'bill_daily');
               echo ' Bill Daily Updated! ';
            */

            echo "\n\n";   
       // }     

        }

      
  
}//end foreach

function testgetLastBillData($bill_id)
{
    $return = array();
    $query="SELECT timestamp,period,delta,in_delta,out_delta FROM bill_data WHERE timestamp BETWEEN '2017-06-22 00:00:00' AND '2017-06-22 23:59:00' AND bill_id ='37' ORDER BY timestamp DESC";

    //echo ("Query2 -->" . $query . "\n\n");

    //$row    = dbFetchRows($query);

   foreach (dbFetchRows($query) as $row) {


        $return[delta]     = $row['delta'];
        $return[in_delta]  = $row['in_delta'];
        $return[out_delta] = $row['out_delta'];
        $return[timestamp] = $row['timestamp'];
        $return[period]=$row['period'];
        $return[state]     = 'ok';
   
       //  echo ("Time Stamp -->" . $row['timestamp']);
   }
   

    return ($return);
}//end getLastBillData()