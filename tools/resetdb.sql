TRUNCATE bill_data;
TRUNCATE bill_history;
TRUNCATE bill_daily;
TRUNCATE bill_monthly;
TRUNCATE bill_yearly;
TRUNCATE bill_witel_daily;

TRUNCATE bill_poller;
TRUNCATE bill_ports;
TRUNCATE bill_port_counters;
TRUNCATE bills;

TRUNCATE eventlog;
TRUNCATE pollers;
TRUNCATE ports;
DELETE FROM ports_statistics where 0<1;
DELETE FROM device_group_device;
DELETE FROM device_perf;
DELETE FROM devices;