<script>
   
     var line = new RGraph.Line({
        id: 'cvs',
        data: <?php print($data_d_string) ?>,
        
        options: {
              gutterTop: 50,
              colors: ['#7CB5EC', 'red'],
              title:'Occupancy 24 Hour View',
              titleXaxis:'Hour',
              
              titleYaxis:'Occupancy (%)',
              titleYaxisX:10,
                shadow: false,
                spline: true,
                hmargin: 10,
                linewidth: 2,
                filled:true,
                ymin: 0,
                ymax: 120,
                ylabelsCount: 15,
                ylabelsOffsetx:0,
                backgroundGridAutofitNumhlines: 15,
                backgroundGridVlines: false,
                axisColor: 'gray',
                textSize: 10,
                textAccessible: true,
                gutterLeft: 50,
                gutterRight: 50,
                key: ['Occupancy'],
                keyPosition: 'gutter',
                highlightStyle: 'halo',
                labels: <?php print($labels_d_string); ?>
        }
    }).draw();
</script>