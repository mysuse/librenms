CREATE TABLE `witel` (
  `witel_id` int(11) NOT NULL AUTO_INCREMENT,
  `witelname` varchar(35) NOT NULL,
  PRIMARY KEY (`witel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `witel` (`witel_id`, `witelname`) VALUES (1,'Jambi'),(2,'Riau Kepulauan'),(3,'Sumatera Barat'),(4,'Aceh'),(5,'Riau Darataan'),(6,'Sumut-Barat');
