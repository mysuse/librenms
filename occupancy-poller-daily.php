#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy Daily ... \n\n";


foreach (dbFetchRows('SELECT * FROM `bills` ORDER BY `bill_id`') as $bill) {
        echo str_pad($bill['bill_id'].' '.$bill['bill_name'], 30)." \n";
   
        unset($class);
        unset($rate_data);
   
        $todaymax = getTodayHighest($bill['bill_id']);

        $todayexist = billDailyIsExist($bill['bill_id']);

        if ($todaymax['state']=='ok') {

            $bill_cdr = $todaymax['bill_cdr'];
            $bill_day = $todaymax['bill_day'];
            $percent  = $todaymax['percent']; 
            $period_d = $todaymax['period_d'];
            $period_m = $todaymax['period_m'];
            $period_y = $todaymax['period_y'];
        
            $now = dbFetchCell('SELECT NOW()');

            if ( $todayexist['exist']=='false') {

          
                
             $bill_daily = array(
                    'bill_id' => $bill['bill_id'],
                    'bill_day'=>$bill_day,
                    'bill_cdr'=> $bill_cdr,
                    'percentage'=> $percent,  
                    'period_d'=>$period_d,
                    'period_m'=>$period_m,
                    'period_y'=>$period_y,
                    'last_updated'=>$now,
                );            
                
                dbInsert($bill_daily,'bill_daily');
           
                echo 'Bill Daily Insert! '; 
            
            } //New Record
            
            else { //Update Record
                  
                
                $fields = array('bill_day'=>$bill_day,'bill_cdr'=>$bill_cdr,'percentage'=>$percent,'last_updated'=>$now);
                
                dbUpdate($fields,'bill_daily',"`bill_id`='" .$bill['bill_id'] ."' AND `period_d`='" . $period_d . "' AND  `period_m`='" . $period_m . "' AND `period_y`='" . $period_y . "'");
       
                echo ' Updating Bill Daily ! ';
            }

              //Update Table Bills
              $billparam = array('occupancy_day'=>$percent,'daily_last_calc'=>$now);    
              $bill_update = dbUpdate($billparam,'bills','`bill_id` = ?',array($bill['bill_id']));
              
              echo 'Billl ID ' . $bill['bill_id'] .  ' Percent ' . $percent . '\n';

              
              if ($bill_update>0) {

                  echo 'Occupancy on table bill udpdated !\n';
              }

            echo "\n\n";        
        }
  
}//end foreach
