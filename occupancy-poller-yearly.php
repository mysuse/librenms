#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy Yearly... \n\n";


foreach (dbFetchRows('SELECT * FROM `bills` ORDER BY `bill_id`') as $bill) {
        echo str_pad($bill['bill_id'].' '.$bill['bill_name'], 30)." \n";
   
        unset($class);
        unset($rate_data);
       
        $thisyearmax = getThisYearHighest($bill['bill_id']);

        $thisyearexist = billYearIsExist($bill['bill_id']);

        if ($thisyearmax['state']=='ok') {

            $bill_cdr = $thisyearmax['bill_cdr'];
            $bill_day = $thisyearmax['bill_day'];
            $percent  = $thisyearmax['percent']; 
            $period_y = $thisyearmax['period_y'];
         
            if ($thisyearexist['exist']=='false') {

            $now = dbFetchCell('SELECT NOW()');
                
             $bill_yearly = array(
                    'bill_id' => $bill['bill_id'],
                    'bill_day'=>$bill_day,
                    'bill_cdr'=> $bill_cdr,
                    'percentage'=> $percent,  
                    'period_y'=>$period_y,
                    'last_updated'=>$now,
                );            
                
                dbInsert($bill_yearly,'bill_yearly');
           
                echo 'Bill Yearly Insert! ';

              
            
            } //New Record
            
            else { //Update Record
                 
                $now = dbFetchCell('SELECT NOW()');
                
                $fields = array('bill_day'=>$bill_day,'bill_cdr'=>$bill_cdr,'percentage'=>$percent,'last_updated'=>$now);
               
                dbUpdate($fields,'bill_yearly',"`bill_id`='" .$bill['bill_id'] ."' AND `period_y`='" . $period_y . "'");
         
                echo ' Updating Bill Yearly ! ';
            }

               
           //Update Table Bills
           $billparam = array('occupancy_year'=>$percent,'yearly_last_calc'=>$now);    
           $bill_update = dbUpdate($billparam,'bills','`bill_id` = ?',array($bill['bill_id']));
           
           echo 'Billl ID ' . $bill['bill_id'] .  ' Percent ' . $percent . '\n';

           if ($bill_update>0) {

               echo 'Occupancy on table bill udpdated !\n';
           }

            echo "\n\n";        
        }
  
}//end foreach
