-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: librenms
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_points`
--

DROP TABLE IF EXISTS `access_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_points` (
  `accesspoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `radio_number` tinyint(4) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `mac_addr` varchar(24) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `channel` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `txpow` tinyint(4) NOT NULL DEFAULT '0',
  `radioutil` tinyint(4) NOT NULL DEFAULT '0',
  `numasoclients` smallint(6) NOT NULL DEFAULT '0',
  `nummonclients` smallint(6) NOT NULL DEFAULT '0',
  `numactbssid` tinyint(4) NOT NULL DEFAULT '0',
  `nummonbssid` tinyint(4) NOT NULL DEFAULT '0',
  `interference` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`accesspoint_id`),
  KEY `deleted` (`deleted`),
  KEY `name` (`name`,`radio_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Access Points';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_points`
--

LOCK TABLES `access_points` WRITE;
/*!40000 ALTER TABLE `access_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_log`
--

DROP TABLE IF EXISTS `alert_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `details` longblob,
  `time_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`),
  KEY `device_id` (`device_id`),
  KEY `time_logged` (`time_logged`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_log`
--

LOCK TABLES `alert_log` WRITE;
/*!40000 ALTER TABLE `alert_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_map`
--

DROP TABLE IF EXISTS `alert_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` int(11) NOT NULL DEFAULT '0',
  `target` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_map`
--

LOCK TABLES `alert_map` WRITE;
/*!40000 ALTER TABLE `alert_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_rules`
--

DROP TABLE IF EXISTS `alert_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) NOT NULL DEFAULT '',
  `rule` text NOT NULL,
  `severity` enum('ok','warning','critical') NOT NULL,
  `extra` varchar(255) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `proc` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_rules`
--

LOCK TABLES `alert_rules` WRITE;
/*!40000 ALTER TABLE `alert_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_schedule`
--

DROP TABLE IF EXISTS `alert_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `start` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `end` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `start_recurring_dt` date NOT NULL DEFAULT '1970-01-01',
  `end_recurring_dt` date DEFAULT NULL,
  `start_recurring_hr` time NOT NULL DEFAULT '00:00:00',
  `end_recurring_hr` time NOT NULL DEFAULT '00:00:00',
  `recurring_day` varchar(15) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_schedule`
--

LOCK TABLES `alert_schedule` WRITE;
/*!40000 ALTER TABLE `alert_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_schedule_items`
--

DROP TABLE IF EXISTS `alert_schedule_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_schedule_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `target` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_schedule_items`
--

LOCK TABLES `alert_schedule_items` WRITE;
/*!40000 ALTER TABLE `alert_schedule_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_schedule_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_template_map`
--

DROP TABLE IF EXISTS `alert_template_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_template_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_templates_id` int(11) NOT NULL,
  `alert_rule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alert_templates_id` (`alert_templates_id`,`alert_rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_template_map`
--

LOCK TABLES `alert_template_map` WRITE;
/*!40000 ALTER TABLE `alert_template_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_template_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_templates`
--

DROP TABLE IF EXISTS `alert_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` varchar(255) NOT NULL DEFAULT ',',
  `name` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_rec` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_templates`
--

LOCK TABLES `alert_templates` WRITE;
/*!40000 ALTER TABLE `alert_templates` DISABLE KEYS */;
INSERT INTO `alert_templates` VALUES (1,',','BGP Sessions.','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed\\r\\n{/if}\nTimestamp: %timestamp\\r\\n\nUnique-ID: %uid\\r\\n\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\n#%key: %value.string\\r\\n\nPeer: %value.astext\\r\\n\nPeer IP: %value.bgpPeerIdentifier\\r\\n\nPeer AS: %value.bgpPeerRemoteAs\\r\\n\nPeer EstTime: %value.bgpPeerFsmEstablishedTime\\r\\n\nPeer State: %value.bgpPeerState\\r\\n\n{/foreach}\n{/if}','',''),(2,',','Ports','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed{/if}\nTimestamp: %timestamp\nUnique-ID: %uid\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\\r\\n\n#%key: %value.string\\r\\n\nPort: %value.ifName\\r\\n\nPort Name: %value.ifAlias\\r\\n\nPort Status: %value.message\\r\\n\n{/foreach}\\r\\n\n{/if}\n','',''),(3,',','Temperature','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed{/if}\\r\\n\nTimestamp: %timestamp\\r\\n\nUnique-ID: %uid\\r\\n\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\\r\\n\n#%key: %value.string\\r\\n\nTemperature: %value.sensor_current\\r\\n\nPrevious Measurement: %value.sensor_prev\\r\\n\n{/foreach}\\r\\n\n{/if}','','');
/*!40000 ALTER TABLE `alert_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `alerted` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_alert` (`device_id`,`rule_id`),
  KEY `rule_id` (`rule_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_tokens`
--

DROP TABLE IF EXISTS `api_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token_hash` varchar(256) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_hash` (`token_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_tokens`
--

LOCK TABLES `api_tokens` WRITE;
/*!40000 ALTER TABLE `api_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applications` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `app_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `app_state` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'UNKNOWN',
  `app_status` varchar(8) NOT NULL,
  `app_instance` varchar(255) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authlog`
--

DROP TABLE IF EXISTS `authlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` text NOT NULL,
  `address` text NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authlog`
--

LOCK TABLES `authlog` WRITE;
/*!40000 ALTER TABLE `authlog` DISABLE KEYS */;
INSERT INTO `authlog` VALUES (47,'2017-03-14 05:06:40','admin','192.168.66.1','Logged In'),(48,'2017-03-14 05:07:20','admin','192.168.66.1','Logged Out'),(49,'2017-03-19 03:46:24','admin','192.168.66.1','Logged In'),(50,'2017-03-19 12:06:23','admin','192.168.66.1','Logged In'),(51,'2017-03-20 08:58:44','admin','192.168.66.1','Logged In'),(52,'2017-03-20 12:25:24','admin','192.168.66.1','Logged In'),(53,'2017-03-21 00:37:51','admin','192.168.66.1','Logged In'),(54,'2017-03-21 01:59:59','admin','192.168.66.1','Logged Out'),(55,'2017-03-21 03:10:14','admin','192.168.66.1','Logged In'),(56,'2017-03-31 06:59:53','admin','192.168.66.1','Logged In'),(57,'2017-03-31 07:03:00','admin','192.168.66.1','Logged Out'),(58,'2017-03-31 07:03:46','admin','192.168.66.1','Logged In'),(59,'2017-04-04 02:08:21','admin','192.168.66.1','Logged In'),(60,'2017-04-04 04:03:44','admin','192.168.66.1','Logged Out'),(61,'2017-04-04 04:03:52','admin','192.168.66.1','Logged In'),(62,'2017-04-04 04:04:06','admin','192.168.66.1','Logged Out'),(63,'2017-04-04 04:04:10','admin','192.168.66.1','Logged In'),(64,'2017-04-04 04:04:40','admin','192.168.66.1','Logged Out'),(65,'2017-04-04 04:04:46','admin','192.168.66.1','Logged In'),(66,'2017-04-04 04:08:55','admin','192.168.66.1','Logged Out'),(67,'2017-04-04 04:09:24','admin','192.168.66.1','Logged In'),(68,'2017-04-04 04:10:19','admin','192.168.66.1','Logged Out'),(69,'2017-04-04 04:10:24','admin','192.168.66.1','Logged In'),(70,'2017-04-04 07:16:25','admin','192.168.66.1','Logged In'),(71,'2017-04-04 08:43:55','admin','192.168.66.1','Logged In'),(72,'2017-04-04 13:11:27','admin','192.168.66.1','Logged In'),(73,'2017-04-04 13:48:52','admin','192.168.66.1','Logged In'),(74,'2017-04-04 13:49:12','admin','192.168.66.1','Logged Out'),(75,'2017-04-04 13:50:48','admin','192.168.66.1','Logged In'),(76,'2017-04-04 13:50:57','admin','192.168.66.1','Logged Out'),(77,'2017-04-04 13:54:51','admin','192.168.66.1','Logged In'),(78,'2017-04-04 13:54:59','admin','192.168.66.1','Logged Out'),(79,'2017-04-04 14:14:00','admin','192.168.66.1','Logged In'),(80,'2017-04-04 16:13:58','admin','192.168.66.1','Logged In'),(81,'2017-04-04 18:20:39','admin','192.168.66.1','Logged Out'),(82,'2017-04-04 18:24:22','admin','192.168.66.1','Logged In'),(83,'2017-04-04 18:25:34','admin','192.168.66.1','Logged Out'),(84,'2017-04-04 18:25:38','User','192.168.66.1','Authentication Failure'),(85,'2017-04-04 18:25:42','user','192.168.66.1','Logged In'),(86,'2017-04-04 18:26:08','user','192.168.66.1','Logged Out'),(87,'2017-04-04 18:26:12','admin','192.168.66.1','Logged In'),(88,'2017-04-04 18:26:59','admin','192.168.66.1','Logged Out'),(89,'2017-04-04 18:27:02','user','192.168.66.1','Logged In'),(90,'2017-04-04 18:28:24','user','192.168.66.1','Logged Out'),(91,'2017-04-04 18:28:28','admin','192.168.66.1','Logged In'),(92,'2017-04-04 18:28:54','admin','192.168.66.1','Logged Out'),(93,'2017-04-04 18:28:57','user','192.168.66.1','Logged In'),(94,'2017-04-04 18:38:00','user','192.168.66.1','Logged Out'),(95,'2017-04-05 02:07:54','admin','192.168.66.1','Logged In'),(96,'2017-04-05 09:31:08','admin','192.168.66.1','Logged In'),(97,'2017-04-06 05:53:29','admin','192.168.66.1','Logged In'),(98,'2017-04-06 08:48:17','admin','192.168.66.1','Logged Out'),(99,'2017-04-06 08:48:21','admin','192.168.66.1','Logged In'),(100,'2017-04-11 04:09:46','admin','192.168.66.1','Authentication Failure'),(101,'2017-04-11 04:09:51','admin','192.168.66.1','Logged In'),(102,'2017-04-11 11:22:52','admin','192.168.66.1','Authentication Failure'),(103,'2017-04-11 11:22:58','admin','192.168.66.1','Logged In'),(104,'2017-04-11 13:14:23','admin','192.168.66.1','Logged In'),(105,'2017-04-13 13:16:45','admin','192.168.66.1','Logged In'),(106,'2017-04-13 15:10:36','admin','192.168.66.1','Logged In'),(107,'2017-04-13 23:03:55','admin','192.168.66.1','Logged In'),(108,'2017-04-13 23:39:13','admin','192.168.66.1','Logged In'),(109,'2017-04-14 15:14:36','admin','192.168.66.1','Logged Out'),(110,'2017-04-14 15:14:46','admin','192.168.66.1','Logged In'),(111,'2017-04-16 00:53:37','admin','192.168.66.1','Logged In'),(112,'2017-04-16 02:03:13','admin','192.168.66.1','Logged In'),(113,'2017-04-16 03:41:32','admin','10.10.12.2','Logged In'),(114,'2017-04-16 04:44:58','admin','192.168.66.1','Logged In'),(115,'2017-04-18 12:40:59','admin','192.168.66.1','Logged In'),(116,'2017-04-19 06:35:51','admin','192.168.66.1','Logged In'),(117,'2017-04-19 12:13:33','admin','192.168.66.1','Logged In'),(118,'2017-04-19 15:35:38','admin','192.168.66.1','Logged Out'),(119,'2017-04-20 01:54:21','admin','192.168.66.1','Logged In'),(120,'2017-04-20 04:55:19','admin','192.168.66.1','Logged In'),(121,'2017-04-20 07:23:42','admin','192.168.66.1','Logged In'),(122,'2017-04-20 12:18:16','admin','192.168.66.1','Logged In'),(123,'2017-04-20 22:50:10','admin','192.168.66.1','Logged In'),(124,'2017-04-20 22:51:29','admin','192.168.66.1','Logged Out'),(125,'2017-04-20 23:08:36','admin','192.168.66.1','Logged In'),(126,'2017-04-20 23:46:10','admin','192.168.66.1','Logged Out'),(127,'2017-04-20 23:49:28','admin','192.168.66.1','Logged In'),(128,'2017-05-30 09:32:56','admin','192.168.66.1','Logged In'),(129,'2017-05-31 15:18:34','admin','192.168.66.1','Logged In'),(130,'2017-06-01 01:18:55','admin','54.254.142.157','Logged In'),(131,'2017-06-01 05:46:43','admin','192.168.66.1','Logged In'),(132,'2017-06-01 08:51:12','admin','192.168.66.1','Logged In'),(133,'2017-06-01 14:58:33','admin','192.168.66.1','Logged Out'),(134,'2017-06-01 14:58:39','rintoexa','192.168.66.1','Authentication Failure'),(135,'2017-06-01 14:58:46','rintoexa','192.168.66.1','Logged In'),(136,'2017-06-01 14:59:51','rintoexa','192.168.66.1','Logged Out'),(137,'2017-06-01 14:59:55','admin','192.168.66.1','Logged In'),(138,'2017-06-01 15:00:51','admin','192.168.66.1','Logged Out'),(139,'2017-06-01 15:00:56','rintoexa','192.168.66.1','Logged In'),(140,'2017-06-01 15:02:00','rintoexa','192.168.66.1','Logged Out'),(141,'2017-06-01 15:02:04','admin','192.168.66.1','Logged In'),(142,'2017-06-01 15:03:00','admin','192.168.66.1','Logged Out'),(143,'2017-06-01 15:03:04','rintoexa','192.168.66.1','Authentication Failure'),(144,'2017-06-01 15:03:11','rintoexa','192.168.66.1','Logged In'),(145,'2017-06-01 15:21:01','rintoexa','192.168.66.1','Logged Out'),(146,'2017-06-01 15:21:08','admin','192.168.66.1','Logged In'),(147,'2017-06-01 15:52:20','admin','192.168.66.1','Logged Out'),(148,'2017-06-02 14:56:05','admin','192.168.66.1','Logged In'),(149,'2017-06-02 16:28:02','admin','192.168.66.1','Logged In'),(150,'2017-06-04 03:22:25','admin','192.168.66.1','Logged In'),(151,'2017-06-04 07:42:00','admin','192.168.66.1','Logged In'),(152,'2017-06-04 09:15:38','admin','192.168.66.1','Logged Out'),(153,'2017-06-04 09:15:42','admin','192.168.66.1','Logged In'),(154,'2017-06-04 12:40:07','admin','192.168.66.1','Logged In'),(155,'2017-06-06 04:09:52','admin','192.168.66.1','Logged In'),(156,'2017-06-06 07:04:27','admin','192.168.66.1','Logged In'),(157,'2017-06-07 03:40:08','admin','192.168.66.1','Logged In'),(158,'2017-06-07 07:38:15','admin','192.168.66.1','Logged In'),(159,'2017-06-07 23:05:43','admin','192.168.66.1','Logged In'),(160,'2017-06-08 13:56:41','admin','192.168.66.1','Logged In'),(161,'2017-06-09 05:55:25','admin','192.168.66.1','Logged In'),(162,'2017-06-10 01:24:59','admin','192.168.66.1','Logged In'),(163,'2017-06-11 03:21:55','admin','192.168.66.1','Logged In'),(164,'2017-06-11 03:23:35','admin','192.168.66.1','Logged Out'),(165,'2017-06-11 06:25:58','admin','192.168.66.1','Logged In'),(166,'2017-06-12 03:23:05','admin','192.168.66.1','Logged In'),(167,'2017-06-12 05:40:13','admin','192.168.66.1','Logged In');
/*!40000 ALTER TABLE `authlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgpPeers`
--

DROP TABLE IF EXISTS `bgpPeers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgpPeers` (
  `bgpPeer_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `astext` varchar(64) NOT NULL,
  `bgpPeerIdentifier` text NOT NULL,
  `bgpPeerRemoteAs` bigint(20) NOT NULL,
  `bgpPeerState` text NOT NULL,
  `bgpPeerAdminStatus` text NOT NULL,
  `bgpLocalAddr` text NOT NULL,
  `bgpPeerRemoteAddr` text NOT NULL,
  `bgpPeerInUpdates` int(11) NOT NULL,
  `bgpPeerOutUpdates` int(11) NOT NULL,
  `bgpPeerInTotalMessages` int(11) NOT NULL,
  `bgpPeerOutTotalMessages` int(11) NOT NULL,
  `bgpPeerFsmEstablishedTime` int(11) NOT NULL,
  `bgpPeerInUpdateElapsedTime` int(11) NOT NULL,
  `context_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`bgpPeer_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgpPeers`
--

LOCK TABLES `bgpPeers` WRITE;
/*!40000 ALTER TABLE `bgpPeers` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgpPeers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgpPeers_cbgp`
--

DROP TABLE IF EXISTS `bgpPeers_cbgp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgpPeers_cbgp` (
  `device_id` int(11) NOT NULL,
  `bgpPeerIdentifier` varchar(64) NOT NULL,
  `afi` varchar(16) NOT NULL,
  `safi` varchar(16) NOT NULL,
  `AcceptedPrefixes` int(11) NOT NULL,
  `DeniedPrefixes` int(11) NOT NULL,
  `PrefixAdminLimit` int(11) NOT NULL,
  `PrefixThreshold` int(11) NOT NULL,
  `PrefixClearThreshold` int(11) NOT NULL,
  `AdvertisedPrefixes` int(11) NOT NULL,
  `SuppressedPrefixes` int(11) NOT NULL,
  `WithdrawnPrefixes` int(11) NOT NULL,
  `AcceptedPrefixes_delta` int(11) NOT NULL,
  `AcceptedPrefixes_prev` int(11) NOT NULL,
  `DeniedPrefixes_delta` int(11) NOT NULL,
  `DeniedPrefixes_prev` int(11) NOT NULL,
  `AdvertisedPrefixes_delta` int(11) NOT NULL,
  `AdvertisedPrefixes_prev` int(11) NOT NULL,
  `SuppressedPrefixes_delta` int(11) NOT NULL,
  `SuppressedPrefixes_prev` int(11) NOT NULL,
  `WithdrawnPrefixes_delta` int(11) NOT NULL,
  `WithdrawnPrefixes_prev` int(11) NOT NULL,
  `context_name` varchar(128) DEFAULT NULL,
  UNIQUE KEY `unique_index` (`device_id`,`bgpPeerIdentifier`,`afi`,`safi`),
  KEY `device_id` (`device_id`,`bgpPeerIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgpPeers_cbgp`
--

LOCK TABLES `bgpPeers_cbgp` WRITE;
/*!40000 ALTER TABLE `bgpPeers_cbgp` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgpPeers_cbgp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_daily`
--

DROP TABLE IF EXISTS `bill_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_daily` (
  `bill_daily_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_cdr` bigint(20) DEFAULT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `percentage` double(11,2) NOT NULL,
  PRIMARY KEY (`bill_daily_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_daily`
--

LOCK TABLES `bill_daily` WRITE;
/*!40000 ALTER TABLE `bill_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_data`
--

DROP TABLE IF EXISTS `bill_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_data` (
  `bill_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `period` int(11) NOT NULL,
  `delta` bigint(11) NOT NULL,
  `in_delta` bigint(11) NOT NULL,
  `out_delta` bigint(11) NOT NULL,
  PRIMARY KEY (`bill_id`,`timestamp`),
  KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_data`
--

LOCK TABLES `bill_data` WRITE;
/*!40000 ALTER TABLE `bill_data` DISABLE KEYS */;
INSERT INTO `bill_data` VALUES (2,'2017-06-12 14:20:02',0,0,0,0),(5,'2017-06-12 14:20:02',0,0,0,0);
/*!40000 ALTER TABLE `bill_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_history`
--

DROP TABLE IF EXISTS `bill_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_history` (
  `bill_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bill_datefrom` datetime NOT NULL,
  `bill_dateto` datetime NOT NULL,
  `bill_type` text NOT NULL,
  `bill_allowed` bigint(20) NOT NULL,
  `bill_used` bigint(20) NOT NULL,
  `bill_overuse` bigint(20) NOT NULL,
  `bill_percent` decimal(10,2) NOT NULL,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `dir_95th` varchar(3) NOT NULL,
  `rate_average` bigint(20) NOT NULL,
  `rate_average_in` bigint(20) NOT NULL,
  `rate_average_out` bigint(20) NOT NULL,
  `traf_in` bigint(20) NOT NULL,
  `traf_out` bigint(20) NOT NULL,
  `traf_total` bigint(20) NOT NULL,
  `pdf` longblob,
  PRIMARY KEY (`bill_hist_id`),
  UNIQUE KEY `unique_index` (`bill_id`,`bill_datefrom`,`bill_dateto`),
  KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_history`
--

LOCK TABLES `bill_history` WRITE;
/*!40000 ALTER TABLE `bill_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_perms`
--

DROP TABLE IF EXISTS `bill_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_perms` (
  `user_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_perms`
--

LOCK TABLES `bill_perms` WRITE;
/*!40000 ALTER TABLE `bill_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_port_counters`
--

DROP TABLE IF EXISTS `bill_port_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_port_counters` (
  `port_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_counter` bigint(20) DEFAULT NULL,
  `in_delta` bigint(20) NOT NULL DEFAULT '0',
  `out_counter` bigint(20) DEFAULT NULL,
  `out_delta` bigint(20) NOT NULL DEFAULT '0',
  `bill_id` int(11) NOT NULL,
  PRIMARY KEY (`port_id`,`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_port_counters`
--

LOCK TABLES `bill_port_counters` WRITE;
/*!40000 ALTER TABLE `bill_port_counters` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_port_counters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_ports`
--

DROP TABLE IF EXISTS `bill_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_ports` (
  `bill_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `bill_port_autoadded` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_ports`
--

LOCK TABLES `bill_ports` WRITE;
/*!40000 ALTER TABLE `bill_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bills` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_name` text NOT NULL,
  `bill_type` text NOT NULL,
  `bill_cdr` bigint(20) DEFAULT NULL,
  `bill_day` int(11) NOT NULL DEFAULT '1',
  `bill_quota` bigint(20) DEFAULT NULL,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `dir_95th` varchar(3) NOT NULL,
  `total_data` bigint(20) NOT NULL,
  `total_data_in` bigint(20) NOT NULL,
  `total_data_out` bigint(20) NOT NULL,
  `rate_average_in` bigint(20) NOT NULL,
  `rate_average_out` bigint(20) NOT NULL,
  `rate_average` bigint(20) NOT NULL,
  `bill_last_calc` datetime NOT NULL,
  `bill_custid` varchar(64) NOT NULL,
  `bill_ref` varchar(64) NOT NULL,
  `bill_notes` varchar(256) NOT NULL,
  `bill_autoadded` tinyint(1) NOT NULL,
  UNIQUE KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bills`
--

LOCK TABLES `bills` WRITE;
/*!40000 ALTER TABLE `bills` DISABLE KEYS */;
INSERT INTO `bills` VALUES (2,'        TBH714-Tembilahan-200Mbps','cdr',10000000,0,0,6013,89823,89823,'out',137640065,10058102,127581963,1213,15389,16602,'2017-06-12 14:01:01','','','',0),(5,'        TBH714-Tembilahan-200Mbps','cdr',10000000,0,0,2509,839,2509,'in',8145604,6530956,1614648,814,201,1015,'2017-06-12 14:01:02','','','',0);
/*!40000 ALTER TABLE `bills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `callback`
--

DROP TABLE IF EXISTS `callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callback` (
  `callback_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(64) NOT NULL,
  `value` char(64) NOT NULL,
  PRIMARY KEY (`callback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `callback`
--

LOCK TABLES `callback` WRITE;
/*!40000 ALTER TABLE `callback` DISABLE KEYS */;
/*!40000 ALTER TABLE `callback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cef_switching`
--

DROP TABLE IF EXISTS `cef_switching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cef_switching` (
  `cef_switching_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` int(11) NOT NULL,
  `afi` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cef_index` int(11) NOT NULL,
  `cef_path` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `drop` int(11) NOT NULL,
  `punt` int(11) NOT NULL,
  `punt2host` int(11) NOT NULL,
  `drop_prev` int(11) NOT NULL,
  `punt_prev` int(11) NOT NULL,
  `punt2host_prev` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `updated_prev` int(11) NOT NULL,
  PRIMARY KEY (`cef_switching_id`),
  UNIQUE KEY `device_id` (`device_id`,`entPhysicalIndex`,`afi`,`cef_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cef_switching`
--

LOCK TABLES `cef_switching` WRITE;
/*!40000 ALTER TABLE `cef_switching` DISABLE KEYS */;
/*!40000 ALTER TABLE `cef_switching` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciscoASA`
--

DROP TABLE IF EXISTS `ciscoASA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciscoASA` (
  `ciscoASA_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `data` bigint(20) NOT NULL,
  `high_alert` bigint(20) NOT NULL,
  `low_alert` bigint(20) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ciscoASA_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciscoASA`
--

LOCK TABLES `ciscoASA` WRITE;
/*!40000 ALTER TABLE `ciscoASA` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciscoASA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each component, unique index',
  `device_id` int(11) unsigned NOT NULL COMMENT 'device_id from the devices table',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name from the component_type table',
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Display label for the component',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The status of the component, retreived from the device',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Should this component be polled',
  `ignore` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Should this component be alerted on',
  `error` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Error message if in Alert state',
  PRIMARY KEY (`id`),
  KEY `device` (`device_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='components attached to a device.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component`
--

LOCK TABLES `component` WRITE;
/*!40000 ALTER TABLE `component` DISABLE KEYS */;
/*!40000 ALTER TABLE `component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_prefs`
--

DROP TABLE IF EXISTS `component_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_prefs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each entry',
  `component` int(11) unsigned NOT NULL COMMENT 'id from the component table',
  `attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Attribute for the Component',
  `value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Value for the Component',
  PRIMARY KEY (`id`),
  KEY `component` (`component`),
  CONSTRAINT `component_prefs_ibfk_1` FOREIGN KEY (`component`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AV Pairs for each component';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_prefs`
--

LOCK TABLES `component_prefs` WRITE;
/*!40000 ALTER TABLE `component_prefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `component_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_statuslog`
--

DROP TABLE IF EXISTS `component_statuslog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_statuslog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each log entry, unique index',
  `component_id` int(11) unsigned NOT NULL COMMENT 'id from the component table',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The status that the component was changed TO',
  `message` text COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the status of the component was changed',
  PRIMARY KEY (`id`),
  KEY `device` (`component_id`),
  CONSTRAINT `component_statuslog_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='log of status changes to a component.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_statuslog`
--

LOCK TABLES `component_statuslog` WRITE;
/*!40000 ALTER TABLE `component_statuslog` DISABLE KEYS */;
/*!40000 ALTER TABLE `component_statuslog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(512) NOT NULL,
  `config_default` varchar(512) NOT NULL,
  `config_descr` varchar(100) NOT NULL,
  `config_group` varchar(50) NOT NULL,
  `config_group_order` int(11) NOT NULL,
  `config_sub_group` varchar(50) NOT NULL,
  `config_sub_group_order` int(11) NOT NULL,
  `config_hidden` enum('0','1') NOT NULL DEFAULT '0',
  `config_disabled` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `uniqueindex_configname` (`config_name`)
) ENGINE=InnoDB AUTO_INCREMENT=773 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (441,'alert.macros.rule.now','NOW()','NOW()','Macro currenttime','alerting',0,'macros',0,'1','0'),(442,'alert.macros.rule.past_5m','DATE_SUB(NOW(),INTERVAL 5 MINUTE)','DATE_SUB(NOW(),INTERVAL 5 MINUTE)','Macro past 5 minutes','alerting',0,'macros',0,'1','0'),(443,'alert.macros.rule.past_10m','DATE_SUB(NOW(),INTERVAL 10 MINUTE)','DATE_SUB(NOW(),INTERVAL 10 MINUTE)','Macro past 10 minutes','alerting',0,'macros',0,'1','0'),(444,'alert.macros.rule.past_15m','DATE_SUB(NOW(),INTERVAL 15 MINUTE)','DATE_SUB(NOW(),INTERVAL 15 MINUTE)','Macro past 15 minutes','alerting',0,'macros',0,'1','0'),(445,'alert.macros.rule.past_30m','DATE_SUB(NOW(),INTERVAL 30 MINUTE)','DATE_SUB(NOW(),INTERVAL 30 MINUTE)','Macro past 30 minutes','alerting',0,'macros',0,'1','0'),(446,'alert.macros.rule.device','(%devices.disabled = 0 && %devices.ignore = 0)','(%devices.disabled = 0 && %devices.ignore = 0)','Devices that aren\'t disabled or ignored','alerting',0,'macros',0,'1','0'),(447,'alert.macros.rule.device_up','(%devices.status = 1 && %macros.device)','(%devices.status = 1 && %macros.device)','Devices that are up','alerting',0,'macros',0,'1','0'),(448,'alert.macros.rule.device_down','(%devices.status = 0 && %macros.device)','(%devices.status = 0 && %macros.device)','Devices that are down','alerting',0,'macros',0,'1','0'),(449,'alert.macros.rule.port','(%ports.deleted = 0 && %ports.ignore = 0 && %ports.disabled = 0)','(%ports.deleted = 0 && %ports.ignore = 0 && %ports.disabled = 0)','Ports that aren\'t disabled, ignored or delete','alerting',0,'macros',0,'1','0'),(450,'alert.macros.rule.port_up','(%ports.ifOperStatus = \"up\" && %ports.ifAdminStatus = \"up\" && %macros.port)','(%ports.ifOperStatus = \"up\" && %ports.ifAdminStatus = \"up\" && %macros.port)','Ports that are up','alerting',0,'macros',0,'1','0'),(451,'alert.admins','true','true','Alert administrators','alerting',0,'general',0,'0','0'),(452,'alert.default_only','true','true','Only alert default mail contact','alerting',0,'general',0,'0','0'),(453,'alert.default_mail','','','The default mail contact','alerting',0,'general',0,'0','0'),(454,'alert.transports.pagerduty','','','Pagerduty transport - put your API key here','alerting',0,'transports',0,'0','0'),(455,'alert.pagerduty.account','','','Pagerduty account name','alerting',0,'transports',0,'0','0'),(456,'alert.pagerduty.service','','','Pagerduty service name','alerting',0,'transports',0,'0','0'),(457,'alert.tolerance_window','5','5','Tolerance window in seconds','alerting',0,'general',0,'0','0'),(458,'email_backend','mail','mail','The backend to use for sending email, can be mail, sendmail or smtp','alerting',0,'general',0,'0','0'),(459,'alert.macros.rule.past_60m','DATE_SUB(NOW(),INTERVAL 60 MINUTE)','DATE_SUB(NOW(),INTERVAL 60 MINUTE)','Macro past 60 minutes','alerting',0,'macros',0,'1','0'),(460,'alert.macros.rule.port_usage_perc','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc','alerting',0,'macros',0,'1','0'),(461,'alert.transports.dummy','false','false','Dummy transport','alerting',0,'transports',0,'0','0'),(462,'alert.transports.mail','true','true','Mail alerting transport','alerting',0,'transports',0,'0','0'),(463,'alert.transports.irc','FALSE','false','IRC alerting transport','alerting',0,'transports',0,'0','0'),(464,'alert.globals','true','TRUE','Alert read only administrators','alerting',0,'general',0,'0','0'),(465,'email_from','NULL','NULL','Email address used for sending emails (from)','alerting',0,'general',0,'0','0'),(466,'email_user','LibreNMS','LibreNMS','Name used as part of the from address','alerting',0,'general',0,'0','0'),(467,'email_sendmail_path','/usr/sbin/sendmail','/usr/sbin/sendmail','Location of sendmail if using this option','alerting',0,'general',0,'0','0'),(468,'email_smtp_host','localhost','localhost','SMTP Host for sending email if using this option','alerting',0,'general',0,'0','0'),(469,'email_smtp_port','25','25','SMTP port setting','alerting',0,'general',0,'0','0'),(470,'email_smtp_timeout','10','10','SMTP timeout setting','alerting',0,'general',0,'0','0'),(471,'email_smtp_secure','','','Enable / disable encryption (use tls or ssl)','alerting',0,'general',0,'0','0'),(472,'email_smtp_auth','false','FALSE','Enable / disable smtp authentication','alerting',0,'general',0,'0','0'),(473,'email_smtp_username','NULL','NULL','SMTP Auth username','alerting',0,'general',0,'0','0'),(474,'email_smtp_password','NULL','NULL','SMTP Auth password','alerting',0,'general',0,'0','0'),(475,'alert.macros.rule.port_down','(%ports.ifOperStatus = \"down\" && %ports.ifAdminStatus != \"down\" && %macros.port)','(%ports.ifOperStatus = \"down\" && %ports.ifAdminStatus != \"down\" && %macros.port)','Ports that are down','alerting',0,'macros',0,'1','0'),(486,'alert.syscontact','true','TRUE','Issue alerts to sysContact','alerting',0,'general',0,'0','0'),(487,'alert.fixed-contacts','true','TRUE','If TRUE any changes to sysContact or users emails will not be honoured whilst alert is active','alerting',0,'general',0,'0','0'),(488,'alert.transports.nagios','','','Nagios compatible via FIFO','alerting',0,'transports',0,'0','0'),(720,'alert.macros.rule.sensor','(%sensors.sensor_alert = 1)','(%sensors.sensor_alert = 1)','Sensors of interest','alerting',0,'macros',0,'0',''),(721,'alert.macros.rule.packet_loss_15m','(%macros.past_15m && %device_perf.loss)','(%macros.past_15m && %device_perf.loss)','Packet loss over the last 15 minutes','alerting',0,'macros',0,'0',''),(722,'alert.macros.rule.packet_loss_5m','(%macros.past_5m && %device_perf.loss)','(%macros.past_5m && %device_perf.loss)','Packet loss over the last 5 minutes','alerting',0,'macros',0,'0',''),(723,'alert.transports.pushbullet','','','Pushbullet access token','alerting',0,'transports',0,'0','0'),(724,'oxidized.enabled','false','false','Enable Oxidized support','external',0,'oxidized',0,'0','0'),(725,'oxidized.url','','','Oxidized API url','external',0,'oxidized',0,'0','0'),(726,'oxidized.features.versioning','false','false','Enable Oxidized config versioning','external',0,'oxidized',0,'0','0'),(727,'alert.disable','false','false','Stop alerts being generated','alerting',0,'general',0,'0','0'),(728,'unix-agent.port','6556','6556','Default port for the Unix-agent (check_mk)','external',0,'unix-agent',0,'0','0'),(729,'unix-agent.connection-timeout','10','10','Unix-agent connection timeout','external',0,'unix-agent',0,'0','0'),(730,'unix-agent.read-timeout','10','10','Unix-agent read timeout','external',0,'unix-agent',0,'0','0'),(731,'alert.transports.clickatell.token','','','Clickatell access token','alerting',0,'transports',0,'0','0'),(732,'alert.transports.playsms.url','','','PlaySMS API URL','alerting',0,'transports',0,'0','0'),(733,'alert.transports.playsms.user','','','PlaySMS User','alerting',0,'transports',0,'0','0'),(734,'alert.transports.playsms.from','','','PlaySMS From','alerting',0,'transports',0,'0','0'),(735,'alert.transports.playsms.token','','','PlaySMS Token','alerting',0,'transports',0,'0','0'),(736,'alert.transports.victorops.url','','','VictorOps Post URL - Replace routing_key!','alerting',0,'transports',0,'0','0'),(737,'rrdtool','/usr/bin/rrdtool','/usr/bin/rrdtool','Path to rrdtool','external',0,'rrdtool',0,'0','0'),(738,'rrdtool_tune','false','false','Auto tune maximum value for rrd port files','external',0,'rrdtool',0,'0','0'),(739,'oxidized.default_group','','','Set the default group returned','external',0,'oxidized',0,'0','0'),(740,'oxidized.group_support','false','false','Enable the return of groups to Oxidized','external',0,'oxidized',0,'0','0'),(741,'oxidized.reload_nodes','false','false','Reload Oxidized nodes list, each time a device is added','external',0,'oxidized',0,'0','0'),(742,'alert.macros.rule.component','(%component.disabled = 0 && %component.ignore = 0)','(%component.disabled = 0 && %component.ignore = 0)','Component that isn\'t disabled or ignored','alerting',0,'macros',0,'1','0'),(743,'alert.macros.rule.component_warning','(%component.status = 1 && %macros.component)','(%component.status = 1 && %macros.component)','Component status Warning','alerting',0,'macros',0,'1','0'),(744,'alert.macros.rule.component_normal','(%component.status = 0 && %macros.component)','(%component.status = 0 && %macros.component)','Component status Normal','alerting',0,'macros',0,'1','0'),(745,'webui.global_search_result_limit','8','8','Global search results limit','webui',0,'search',0,'1','0'),(746,'email_html','false','false','Send HTML emails','alerting',0,'general',0,'0','0'),(747,'alert.transports.canopsis.user','','','Canopsis User','alerting',0,'transports',0,'',''),(748,'alert.transports.canopsis.passwd','','','Canopsis Password','alerting',0,'transports',0,'',''),(749,'alert.transports.canopsis.host','','','Canopsis Hostname','alerting',0,'transports',0,'',''),(750,'alert.transports.canopsis.vhost','','','Canopsis vHost','alerting',0,'transports',0,'',''),(751,'alert.transports.canopsis.port','','','Canopsis Port number','alerting',0,'transports',0,'',''),(752,'webui.min_graph_height','300','300','Minimum Graph Height','webui',0,'graph',0,'1','0'),(753,'alert.macros.rule.port_in_usage_perc','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc of capacity IN','alerting',0,'macros',0,'0',''),(754,'alert.macros.rule.port_out_usage_perc','((%ports.ifOutOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifOutOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc of capacity OUT','alerting',0,'macros',0,'0',''),(755,'alert.macros.rule.port_now_down','%ports.ifOperStatus != %ports.ifOperStatus_prev && %ports.ifOperStatus_prev = \"up\" && %ports.ifAdminStatus = \"up\"','%ports.ifOperStatus != %ports.ifOperStatus_prev && %ports.ifOperStatus_prev = \"up\" && %ports.ifAdminStatus = \"up\"','Port has gone down','alerting',0,'macros',0,'0',''),(756,'alert.macros.rule.device_component_down_junos','%sensors.sensor_class = \"state\" && %sensors.sensor_current != \"6\" && %sensors.sensor_type = \"jnxFruState\" && %sensors.sensor_current != \"2\"','%sensors.sensor_class = \"state\" && %sensors.sensor_current != \"6\" && %sensors.sensor_type = \"jnxFruState\" && %sensors.sensor_current != \"2\"','Device Component down [JunOS]','alerting',0,'macros',0,'0',''),(757,'alert.macros.rule.device_component_down_cisco','%sensors.sensor_current != \"1\" && %sensors.sensor_current != \"5\" && %sensors.sensor_type ~ \"^cisco.*State$\"','%sensors.sensor_current != \"1\" && %sensors.sensor_current != \"5\" && %sensors.sensor_type ~ \"^cisco.*State$\"','Device Component down [Cisco]','alerting',0,'macros',0,'0',''),(758,'alert.macros.rule.pdu_over_amperage_apc','%sensors.sensor_class = \"current\" && %sensors.sensor_descr = \"Bank Total\" && %sensors.sensor_current > %sensors.sensor_limit && %devices.os = \"apc\"','%sensors.sensor_class = \"current\" && %sensors.sensor_descr = \"Bank Total\" && %sensors.sensor_current > %sensors.sensor_limit && %devices.os = \"apc\"','PDU Over Amperage [APC]','alerting',0,'macros',0,'0',''),(759,'alert.macros.rule.component_critical','(%component.status = 2 && %macros.component)','(%component.status = 2 && %macros.component)','Component status Critical','alerting',0,'macros',0,'1','0'),(760,'webui.availability_map_compact','false','false','Availability map old view','webui',0,'graph',0,'0','0'),(761,'webui.default_dashboard_id','0','0','Global default dashboard_id for all users who do not have their own default set','webui',0,'dashboard',0,'0','0'),(762,'webui.availability_map_sort_status','false','false','Sort devices and services by status','webui',0,'graph',0,'0','0'),(763,'webui.availability_map_use_device_groups','false','false','Enable usage of device groups filter','webui',0,'graph',0,'0','0'),(764,'webui.availability_map_box_size','165','165','Input desired tile width in pixels for box size in full view','webui',0,'graph',0,'0','0'),(765,'alert.transports.osticket.url','','','osTicket API URL','alerting',0,'transports',0,'',''),(766,'alert.transports.osticket.token','','','osTicket API Token','alerting',0,'transports',0,'',''),(767,'alert.macros.rule.bill_quota_over_quota','((%bills.total_data / %bills.bill_quota)*100) && %bills.bill_type = \"quota\"','((%bills.total_data / %bills.bill_quota)*100) && %bills.bill_type = \"quota\"','Quota bills over X perc of quota','alerting',0,'macros',0,'0',''),(768,'alert.macros.rule.bill_cdr_over_quota','((%bills.rate_95th / %bills.bill_cdr)*100) && %bills.bill_type = \"cdr\"','((%bills.rate_95th / %bills.bill_cdr)*100) && %bills.bill_type = \"cdr\"','CDR bills over X perc of quota','alerting',0,'macros',0,'0',''),(769,'alert.transports.msteams.url','','','Microsoft Teams Webhook URL','alerting',0,'transports',0,'',''),(770,'email_auto_tls','true','true','Enable / disable Auto TLS support','alerting',0,'general',0,'0','0'),(771,'alert.transports.ciscospark.token','','','Cisco Spark API Token','alerting',0,'transports',0,'0','0'),(772,'alert.transports.ciscospark.roomid','','','Cisco Spark RoomID','alerting',0,'transports',0,'0','0');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(64) NOT NULL,
  `password` char(32) NOT NULL,
  `string` char(64) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboards`
--

DROP TABLE IF EXISTS `dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboards` (
  `dashboard_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `dashboard_name` varchar(255) NOT NULL,
  `access` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboards`
--

LOCK TABLES `dashboards` WRITE;
/*!40000 ALTER TABLE `dashboards` DISABLE KEYS */;
INSERT INTO `dashboards` VALUES (2,2,'Default',0),(3,1,'Default',2);
/*!40000 ALTER TABLE `dashboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbSchema`
--

DROP TABLE IF EXISTS `dbSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbSchema` (
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbSchema`
--

LOCK TABLES `dbSchema` WRITE;
/*!40000 ALTER TABLE `dbSchema` DISABLE KEYS */;
INSERT INTO `dbSchema` VALUES (168);
/*!40000 ALTER TABLE `dbSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_graphs`
--

DROP TABLE IF EXISTS `device_graphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_graphs` (
  `device_id` int(11) NOT NULL,
  `graph` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_graphs`
--

LOCK TABLES `device_graphs` WRITE;
/*!40000 ALTER TABLE `device_graphs` DISABLE KEYS */;
INSERT INTO `device_graphs` VALUES (21,'uptime'),(21,'netstat_ip'),(21,'netstat_ip_frag'),(21,'netstat_tcp'),(21,'netstat_udp'),(21,'netstat_icmp'),(21,'netstat_icmp_info'),(21,'netstat_snmp'),(21,'netstat_snmp_pkt'),(21,'hr_processes'),(21,'hr_users'),(21,'ucd_cpu'),(21,'ucd_swap_io'),(21,'ucd_io'),(21,'ucd_contexts'),(21,'ucd_interrupts'),(21,'ucd_memory'),(21,'ucd_load'),(21,'ipsystemstats_ipv4'),(21,'ipsystemstats_ipv4_frag'),(21,'ipsystemstats_ipv6'),(21,'ipsystemstats_ipv6_frag');
/*!40000 ALTER TABLE `device_graphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_group_device`
--

DROP TABLE IF EXISTS `device_group_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_group_device` (
  `device_group_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`device_group_id`,`device_id`),
  KEY `device_group_device_device_group_id_index` (`device_group_id`),
  KEY `device_group_device_device_id_index` (`device_id`),
  CONSTRAINT `device_group_device_device_group_id_foreign` FOREIGN KEY (`device_group_id`) REFERENCES `device_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `device_group_device_device_id_foreign` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_group_device`
--

LOCK TABLES `device_group_device` WRITE;
/*!40000 ALTER TABLE `device_group_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_group_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_groups`
--

DROP TABLE IF EXISTS `device_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `desc` varchar(255) NOT NULL DEFAULT '',
  `pattern` text,
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_groups`
--

LOCK TABLES `device_groups` WRITE;
/*!40000 ALTER TABLE `device_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_mibs`
--

DROP TABLE IF EXISTS `device_mibs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_mibs` (
  `device_id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `included_by` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`,`module`,`mib`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Device to MIB mappings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_mibs`
--

LOCK TABLES `device_mibs` WRITE;
/*!40000 ALTER TABLE `device_mibs` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_mibs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_oids`
--

DROP TABLE IF EXISTS `device_oids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_oids` (
  `device_id` int(11) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `numvalue` bigint(20) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`,`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Per-device MIB data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_oids`
--

LOCK TABLES `device_oids` WRITE;
/*!40000 ALTER TABLE `device_oids` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_oids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_perf`
--

DROP TABLE IF EXISTS `device_perf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_perf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `xmt` float NOT NULL,
  `rcv` float NOT NULL,
  `loss` float NOT NULL,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `avg` float NOT NULL,
  KEY `id` (`id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_perf`
--

LOCK TABLES `device_perf` WRITE;
/*!40000 ALTER TABLE `device_perf` DISABLE KEYS */;
INSERT INTO `device_perf` VALUES (1,21,'2017-06-12 14:20:03',3,3,0,0.04,0.05,0.04);
/*!40000 ALTER TABLE `device_perf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `device_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(128) CHARACTER SET latin1 NOT NULL,
  `sysName` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `ip` varbinary(16) DEFAULT NULL,
  `community` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authlevel` enum('noAuthNoPriv','authNoPriv','authPriv') COLLATE utf8_unicode_ci DEFAULT NULL,
  `authname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authpass` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authalgo` enum('MD5','SHA') COLLATE utf8_unicode_ci DEFAULT NULL,
  `cryptopass` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cryptoalgo` enum('AES','DES','') COLLATE utf8_unicode_ci DEFAULT NULL,
  `snmpver` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT 'v2c',
  `port` smallint(5) unsigned NOT NULL DEFAULT '161',
  `transport` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'udp',
  `timeout` int(11) DEFAULT NULL,
  `retries` int(11) DEFAULT NULL,
  `bgpLocalAs` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `sysObjectID` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sysDescr` text CHARACTER SET latin1,
  `sysContact` text CHARACTER SET latin1,
  `version` text CHARACTER SET latin1,
  `hardware` text CHARACTER SET latin1,
  `features` text CHARACTER SET latin1,
  `location` text COLLATE utf8_unicode_ci,
  `os` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `status_reason` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ignore` tinyint(4) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `uptime` bigint(20) DEFAULT NULL,
  `agent_uptime` int(11) NOT NULL DEFAULT '0',
  `last_polled` timestamp NULL DEFAULT NULL,
  `last_poll_attempted` timestamp NULL DEFAULT NULL,
  `last_polled_timetaken` double(5,2) DEFAULT NULL,
  `last_discovered_timetaken` double(5,2) DEFAULT NULL,
  `last_discovered` timestamp NULL DEFAULT NULL,
  `last_ping` timestamp NULL DEFAULT NULL,
  `last_ping_timetaken` double(5,2) DEFAULT NULL,
  `purpose` text COLLATE utf8_unicode_ci,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `serial` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poller_group` int(11) NOT NULL DEFAULT '0',
  `override_sysLocation` tinyint(1) DEFAULT '0',
  `notes` text COLLATE utf8_unicode_ci,
  `port_association_mode` int(11) NOT NULL DEFAULT '1',
  `site_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sto` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan2g` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan3g` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witel_id` int(11) DEFAULT NULL,
  `bandwidth` int(11) DEFAULT NULL,
  `metro_name` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metro_port` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metro_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_name` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_port` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ont_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ont_sn` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahundeploy` int(11) DEFAULT NULL,
  `dataservices` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tselregion_id` int(11) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `status` (`status`),
  KEY `hostname` (`hostname`),
  KEY `sysName` (`sysName`),
  KEY `os` (`os`),
  KEY `last_polled` (`last_polled`),
  KEY `last_poll_attempted` (`last_poll_attempted`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (21,'localhost','ubuntu14vm','\0\0','public',NULL,NULL,NULL,NULL,NULL,NULL,'v2c',161,'udp',NULL,NULL,NULL,'enterprises.8072.3.2.10','Linux ubuntu14vm 3.13.0-32-generic #57-Ubuntu SMP Tue Jul 15 03:51:08 UTC 2014 x86_64','Me <me@example.org>','3.13.0-32-generic','Generic x86 64-bit',NULL,'Sitting on the Dock of the Bay','linux',1,'',0,0,20191,0,'2017-06-12 07:20:05',NULL,2.29,4.52,'2017-06-12 05:33:06','2017-06-12 07:20:05',0.04,'','server',NULL,'linux.svg',0,0,NULL,1,'Tembilahan','TBH714','TEMBILAHAN','/','/',28,10,'ME-D1-BKRA','LAG-3','localhost','GPON03-D1-BKR-1','172.26.33.212','0/1/12','172.26.33.212','4.85754438355E+161',-0.33101670,103.15127800,'',2015,'3G',2);
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices_attribs`
--

DROP TABLE IF EXISTS `devices_attribs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices_attribs` (
  `attrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `attrib_type` varchar(32) NOT NULL,
  `attrib_value` text NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attrib_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices_attribs`
--

LOCK TABLES `devices_attribs` WRITE;
/*!40000 ALTER TABLE `devices_attribs` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices_attribs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices_perms`
--

DROP TABLE IF EXISTS `devices_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices_perms` (
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `access_level` int(4) NOT NULL DEFAULT '0',
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices_perms`
--

LOCK TABLES `devices_perms` WRITE;
/*!40000 ALTER TABLE `devices_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entPhysical`
--

DROP TABLE IF EXISTS `entPhysical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entPhysical` (
  `entPhysical_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` int(11) NOT NULL,
  `entPhysicalDescr` text NOT NULL,
  `entPhysicalClass` text NOT NULL,
  `entPhysicalName` text NOT NULL,
  `entPhysicalHardwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalFirmwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalSoftwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalAlias` varchar(32) DEFAULT NULL,
  `entPhysicalAssetID` varchar(32) DEFAULT NULL,
  `entPhysicalIsFRU` varchar(8) DEFAULT NULL,
  `entPhysicalModelName` text NOT NULL,
  `entPhysicalVendorType` text,
  `entPhysicalSerialNum` text NOT NULL,
  `entPhysicalContainedIn` int(11) NOT NULL,
  `entPhysicalParentRelPos` int(11) NOT NULL,
  `entPhysicalMfgName` text NOT NULL,
  `ifIndex` int(11) DEFAULT NULL,
  PRIMARY KEY (`entPhysical_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entPhysical`
--

LOCK TABLES `entPhysical` WRITE;
/*!40000 ALTER TABLE `entPhysical` DISABLE KEYS */;
/*!40000 ALTER TABLE `entPhysical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entPhysical_state`
--

DROP TABLE IF EXISTS `entPhysical_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entPhysical_state` (
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` varchar(64) NOT NULL,
  `subindex` varchar(64) DEFAULT NULL,
  `group` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(255) NOT NULL,
  KEY `device_id_index` (`device_id`,`entPhysicalIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entPhysical_state`
--

LOCK TABLES `entPhysical_state` WRITE;
/*!40000 ALTER TABLE `entPhysical_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `entPhysical_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventlog`
--

DROP TABLE IF EXISTS `eventlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventlog` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `host` int(11) NOT NULL DEFAULT '0',
  `device_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `message` text CHARACTER SET latin1,
  `type` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `reference` varchar(64) CHARACTER SET latin1 NOT NULL,
  `severity` int(1) DEFAULT '2',
  PRIMARY KEY (`event_id`),
  KEY `host` (`host`),
  KEY `datetime` (`datetime`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventlog`
--

LOCK TABLES `eventlog` WRITE;
/*!40000 ALTER TABLE `eventlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_types`
--

DROP TABLE IF EXISTS `graph_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_types` (
  `graph_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_subtype` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `graph_section` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `graph_order` int(11) NOT NULL,
  PRIMARY KEY (`graph_type`,`graph_subtype`,`graph_section`),
  KEY `graph_type` (`graph_type`),
  KEY `graph_subtype` (`graph_subtype`),
  KEY `graph_section` (`graph_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_types`
--

LOCK TABLES `graph_types` WRITE;
/*!40000 ALTER TABLE `graph_types` DISABLE KEYS */;
INSERT INTO `graph_types` VALUES ('device','asa_conns','firewall','Current connections',0),('device','panos_activetunnels','firewall','Active GlobalProtect Tunnels',0),('device','pulse_sessions','firewall','Active Sessions',0),('device','pulse_users','firewall','Active Users',0),('device','riverbed_connections','network','Connections',0),('device','riverbed_datastore','network','Datastore productivity',0),('device','riverbed_optimization','network','Optimization',0),('device','riverbed_passthrough','network','Bandwidth passthrough',0),('device','sgos_average_requests','network','Average HTTP Requests',0),('device','sonicwall_sessions','firewall','Active Sessions',0),('device','waas_cwotfostatsactiveoptconn','graphs','Optimized TCP Connections',0),('device','zywall_sessions','firewall','Sessions',0);
/*!40000 ALTER TABLE `graph_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_types_dead`
--

DROP TABLE IF EXISTS `graph_types_dead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_types_dead` (
  `graph_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_subtype` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_section` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_descr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `graph_order` int(11) NOT NULL,
  KEY `graph_type` (`graph_type`),
  KEY `graph_subtype` (`graph_subtype`),
  KEY `graph_section` (`graph_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_types_dead`
--

LOCK TABLES `graph_types_dead` WRITE;
/*!40000 ALTER TABLE `graph_types_dead` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph_types_dead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hrDevice`
--

DROP TABLE IF EXISTS `hrDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hrDevice` (
  `hrDevice_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `hrDeviceIndex` int(11) NOT NULL,
  `hrDeviceDescr` text CHARACTER SET latin1 NOT NULL,
  `hrDeviceType` text CHARACTER SET latin1 NOT NULL,
  `hrDeviceErrors` int(11) NOT NULL DEFAULT '0',
  `hrDeviceStatus` text CHARACTER SET latin1 NOT NULL,
  `hrProcessorLoad` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`hrDevice_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hrDevice`
--

LOCK TABLES `hrDevice` WRITE;
/*!40000 ALTER TABLE `hrDevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `hrDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipsec_tunnels`
--

DROP TABLE IF EXISTS `ipsec_tunnels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipsec_tunnels` (
  `tunnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `peer_port` int(11) NOT NULL,
  `peer_addr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `local_addr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `local_port` int(11) NOT NULL,
  `tunnel_name` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `tunnel_status` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`tunnel_id`),
  UNIQUE KEY `unique_index` (`device_id`,`peer_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipsec_tunnels`
--

LOCK TABLES `ipsec_tunnels` WRITE;
/*!40000 ALTER TABLE `ipsec_tunnels` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipsec_tunnels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_addresses`
--

DROP TABLE IF EXISTS `ipv4_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_addresses` (
  `ipv4_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv4_address` varchar(32) CHARACTER SET latin1 NOT NULL,
  `ipv4_prefixlen` int(11) NOT NULL,
  `ipv4_network_id` varchar(32) CHARACTER SET latin1 NOT NULL,
  `port_id` int(11) NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv4_address_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_addresses`
--

LOCK TABLES `ipv4_addresses` WRITE;
/*!40000 ALTER TABLE `ipv4_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_mac`
--

DROP TABLE IF EXISTS `ipv4_mac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_mac` (
  `port_id` int(11) NOT NULL,
  `mac_address` varchar(32) CHARACTER SET utf8 NOT NULL,
  `ipv4_address` varchar(32) CHARACTER SET utf8 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  KEY `port_id` (`port_id`),
  KEY `mac_address` (`mac_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_mac`
--

LOCK TABLES `ipv4_mac` WRITE;
/*!40000 ALTER TABLE `ipv4_mac` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_mac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_networks`
--

DROP TABLE IF EXISTS `ipv4_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_networks` (
  `ipv4_network_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv4_network` varchar(64) CHARACTER SET latin1 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv4_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_networks`
--

LOCK TABLES `ipv4_networks` WRITE;
/*!40000 ALTER TABLE `ipv4_networks` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv6_addresses`
--

DROP TABLE IF EXISTS `ipv6_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv6_addresses` (
  `ipv6_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv6_address` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ipv6_compressed` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ipv6_prefixlen` int(11) NOT NULL,
  `ipv6_origin` varchar(16) CHARACTER SET latin1 NOT NULL,
  `ipv6_network_id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `port_id` int(11) NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv6_address_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv6_addresses`
--

LOCK TABLES `ipv6_addresses` WRITE;
/*!40000 ALTER TABLE `ipv6_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv6_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv6_networks`
--

DROP TABLE IF EXISTS `ipv6_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv6_networks` (
  `ipv6_network_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv6_network` varchar(64) CHARACTER SET latin1 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv6_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv6_networks`
--

LOCK TABLES `ipv6_networks` WRITE;
/*!40000 ALTER TABLE `ipv6_networks` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv6_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juniAtmVp`
--

DROP TABLE IF EXISTS `juniAtmVp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juniAtmVp` (
  `juniAtmVp_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `vp_id` int(11) NOT NULL,
  `vp_descr` varchar(32) NOT NULL,
  KEY `port_id` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juniAtmVp`
--

LOCK TABLES `juniAtmVp` WRITE;
/*!40000 ALTER TABLE `juniAtmVp` DISABLE KEYS */;
/*!40000 ALTER TABLE `juniAtmVp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_port_id` int(11) DEFAULT NULL,
  `local_device_id` int(11) NOT NULL,
  `remote_port_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `protocol` varchar(11) DEFAULT NULL,
  `remote_hostname` varchar(128) NOT NULL,
  `remote_device_id` int(11) NOT NULL,
  `remote_port` varchar(128) NOT NULL,
  `remote_platform` varchar(128) NOT NULL,
  `remote_version` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `src_if` (`local_port_id`),
  KEY `dst_if` (`remote_port_id`),
  KEY `local_device_id` (`local_device_id`,`remote_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loadbalancer_rservers`
--

DROP TABLE IF EXISTS `loadbalancer_rservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loadbalancer_rservers` (
  `rserver_id` int(11) NOT NULL AUTO_INCREMENT,
  `farm_id` varchar(128) NOT NULL,
  `device_id` int(11) NOT NULL,
  `StateDescr` varchar(64) NOT NULL,
  PRIMARY KEY (`rserver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loadbalancer_rservers`
--

LOCK TABLES `loadbalancer_rservers` WRITE;
/*!40000 ALTER TABLE `loadbalancer_rservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loadbalancer_rservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loadbalancer_vservers`
--

DROP TABLE IF EXISTS `loadbalancer_vservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loadbalancer_vservers` (
  `classmap_id` int(11) NOT NULL,
  `classmap` varchar(128) NOT NULL,
  `serverstate` varchar(64) NOT NULL,
  `device_id` int(11) NOT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loadbalancer_vservers`
--

LOCK TABLES `loadbalancer_vservers` WRITE;
/*!40000 ALTER TABLE `loadbalancer_vservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loadbalancer_vservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` text NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `timestamp` datetime NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mac_accounting`
--

DROP TABLE IF EXISTS `mac_accounting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mac_accounting` (
  `ma_id` int(11) NOT NULL AUTO_INCREMENT,
  `port_id` int(11) NOT NULL,
  `mac` varchar(32) NOT NULL,
  `in_oid` varchar(128) NOT NULL,
  `out_oid` varchar(128) NOT NULL,
  `bps_out` int(11) NOT NULL,
  `bps_in` int(11) NOT NULL,
  `cipMacHCSwitchedBytes_input` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_rate` int(11) DEFAULT NULL,
  `poll_time` int(11) DEFAULT NULL,
  `poll_prev` int(11) DEFAULT NULL,
  `poll_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`ma_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mac_accounting`
--

LOCK TABLES `mac_accounting` WRITE;
/*!40000 ALTER TABLE `mac_accounting` DISABLE KEYS */;
/*!40000 ALTER TABLE `mac_accounting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mempools`
--

DROP TABLE IF EXISTS `mempools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mempools` (
  `mempool_id` int(11) NOT NULL AUTO_INCREMENT,
  `mempool_index` varchar(16) CHARACTER SET latin1 NOT NULL,
  `entPhysicalIndex` int(11) DEFAULT NULL,
  `hrDeviceIndex` int(11) DEFAULT NULL,
  `mempool_type` varchar(32) CHARACTER SET latin1 NOT NULL,
  `mempool_precision` int(11) NOT NULL DEFAULT '1',
  `mempool_descr` varchar(64) CHARACTER SET latin1 NOT NULL,
  `device_id` int(11) NOT NULL,
  `mempool_perc` int(11) NOT NULL,
  `mempool_used` bigint(16) NOT NULL,
  `mempool_free` bigint(16) NOT NULL,
  `mempool_total` bigint(16) NOT NULL,
  `mempool_largestfree` bigint(16) DEFAULT NULL,
  `mempool_lowestfree` bigint(16) DEFAULT NULL,
  `mempool_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mempool_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mempools`
--

LOCK TABLES `mempools` WRITE;
/*!40000 ALTER TABLE `mempools` DISABLE KEYS */;
INSERT INTO `mempools` VALUES (1,'1',NULL,NULL,'hrstorage',1024,'Physical memory',21,75,1571450880,512618496,2084069376,NULL,NULL,0),(2,'3',NULL,NULL,'hrstorage',1024,'Virtual memory',21,25,1571450880,4805484544,6376935424,NULL,NULL,0),(3,'10',NULL,NULL,'hrstorage',1024,'Swap space',21,0,0,4292866048,4292866048,NULL,NULL,0);
/*!40000 ALTER TABLE `mempools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mibdefs`
--

DROP TABLE IF EXISTS `mibdefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mibdefs` (
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `syntax` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `max_access` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `included_by` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module`,`mib`,`object_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='MIB definitions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mibdefs`
--

LOCK TABLES `mibdefs` WRITE;
/*!40000 ALTER TABLE `mibdefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mibdefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `month`
--

DROP TABLE IF EXISTS `month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `month` (
  `month_id` int(11) NOT NULL,
  `month_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`month_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `month`
--

LOCK TABLES `month` WRITE;
/*!40000 ALTER TABLE `month` DISABLE KEYS */;
INSERT INTO `month` VALUES (1,'January'),(2,'February'),(3,'March'),(4,'April'),(5,'May'),(6,'June'),(7,'July'),(8,'August'),(9,'September'),(10,'October'),(11,'November'),(12,'December');
/*!40000 ALTER TABLE `month` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `munin_plugins`
--

DROP TABLE IF EXISTS `munin_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `munin_plugins` (
  `mplug_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `mplug_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `mplug_instance` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_category` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `mplug_title` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_info` text COLLATE utf8_bin,
  `mplug_vlabel` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_args` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `mplug_total` binary(1) NOT NULL DEFAULT '0',
  `mplug_graph` binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`mplug_id`),
  UNIQUE KEY `UNIQUE` (`device_id`,`mplug_type`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `munin_plugins`
--

LOCK TABLES `munin_plugins` WRITE;
/*!40000 ALTER TABLE `munin_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `munin_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `munin_plugins_ds`
--

DROP TABLE IF EXISTS `munin_plugins_ds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `munin_plugins_ds` (
  `mplug_id` int(11) NOT NULL,
  `ds_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_type` enum('COUNTER','ABSOLUTE','DERIVE','GAUGE') COLLATE utf8_bin NOT NULL DEFAULT 'GAUGE',
  `ds_label` varchar(64) COLLATE utf8_bin NOT NULL,
  `ds_cdef` varchar(255) COLLATE utf8_bin NOT NULL,
  `ds_draw` varchar(64) COLLATE utf8_bin NOT NULL,
  `ds_graph` enum('no','yes') COLLATE utf8_bin NOT NULL DEFAULT 'yes',
  `ds_info` varchar(255) COLLATE utf8_bin NOT NULL,
  `ds_extinfo` text COLLATE utf8_bin NOT NULL,
  `ds_max` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_min` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_negative` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_warning` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_critical` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_colour` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_sum` text COLLATE utf8_bin NOT NULL,
  `ds_stack` text COLLATE utf8_bin NOT NULL,
  `ds_line` varchar(64) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `splug_id` (`mplug_id`,`ds_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `munin_plugins_ds`
--

LOCK TABLES `munin_plugins_ds` WRITE;
/*!40000 ALTER TABLE `munin_plugins_ds` DISABLE KEYS */;
/*!40000 ALTER TABLE `munin_plugins_ds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `netscaler_vservers`
--

DROP TABLE IF EXISTS `netscaler_vservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `netscaler_vservers` (
  `vsvr_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `vsvr_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_port` int(8) NOT NULL,
  `vsvr_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_state` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_clients` int(11) NOT NULL,
  `vsvr_server` int(11) NOT NULL,
  `vsvr_req_rate` int(11) NOT NULL,
  `vsvr_bps_in` int(11) NOT NULL,
  `vsvr_bps_out` int(11) NOT NULL,
  PRIMARY KEY (`vsvr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `netscaler_vservers`
--

LOCK TABLES `netscaler_vservers` WRITE;
/*!40000 ALTER TABLE `netscaler_vservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `netscaler_vservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `notifications_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT '',
  `checksum` varchar(128) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notifications_id`),
  UNIQUE KEY `checksum` (`checksum`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'Hello World!','This is the first notification. We will post these whenever an upcoming major change is about to happen.','http://www.librenms.org/notifications.rss','e9bf74634c19f09e03cdbd270563a258bc2ad1f4ab7d20de06ee60d2a4ebc3057c6779043b7475924cfb58ba505e6d0c752e4ec7b79c251c2a77d3426e34b12a','2015-09-26 17:00:00'),(2,'Important update being released','We are releasing an important update within the next 24 hours which provides some additional functionality when running daily.sh. Whilst we have done everything we can to test this across as many platforms as possible we thought it was worth letting you know.\\n\\nIf you notice issues with further updates after this one then please get in touch with us on either IRC or GitHub.','http://www.librenms.org/notifications.rss','cbcf6be1a832606d23f4d9093c7fa7a12bd6930701868050de00a13a3fe57a9fa48d75dab78f1f20d25d19633468b7849562cc45430bb4cc9b4b8a8eda632a99','2015-12-03 17:00:00'),(3,'Poller changes','Earlier this month we released some updates which enable alpha versions of InfluxDB and MIB-based polling.\\nWe have some further updates planned for the near future which may affect polling, so if you are running a mission-critical LibreNMS instance, you may wish to consider disabling updates for the time being.\\nIf you notice any issues, please get in touch with us via IRC or GitHub.','http://www.librenms.org/notifications.rss','e3c2426b447c0bf4c9f50d72dc6b14756bc86e59b3aefced08589d09763514269904ce713f9572723d2194263cc3728fd13ccef7f9b0045cbc1b8a3ac31dfb44','2016-01-09 17:00:00'),(4,'Redback SmartEdge OS change','On January 19th we will be merging a PR that will change devices currently detected as redback to be detected as seos.\\n\\nIf you have any alert rules or device groups that rely on this OS definition then you will need to update those rules to include seos.','http://www.librenms.org/notifications.rss','22fc9b7f8d0d73c0e84489717d3d36126ec1f718ea05324c92ccdc9e33fe6b929f53514b7a84dbf97fd6cf9abc282a60cd40b400b7817105d54f7289923b1ae0','2016-01-17 17:00:00'),(5,'Cisco syslog parsing changes','We have overhauled the Cisco syslog parsing.  Please monitor your syslog entries from Cisco IOS, IOSXR, and CatOS devices.  If you notice any issues please open a new issue on GitHub and include the original syslog message.','misc/notifications.rss','0a93529900a8bf1e7b2c7e8f52ab245747df3f8458e10edb76e0404057f14c931476ab4d9523b974d47018633cef70b7cbc99025e0b47f04d0144bff0de16363','2016-03-02 17:00:00'),(6,'Billing Data Changes','We have updated the method for storing billing data, removing a lot of information stored in \n   the database which was not used.  Please check that your billing data is updating as expected, and \n   if you notice any problems open an issue on GitHub.','misc/notifications.rss','779f4a4f9e4d65d3a00f5690d27a030c3cbebabd81eed33bd1a1b2ede6556eeda59fd2a3a3f3d0069f0de3409eb54256d31164525c830edee17fd45a7ab04498','2016-03-06 17:00:00'),(7,'Introduction of default purge interval or alert history','We have introduced a purge setting for alert history which is set to 365 days as default. If you would like to update this then please \n   set $config[\'alert_log_purge\'] to a custom value (set in days).','misc/notifications.rss','0a77fb4b52c8875343a5bfbf6b7946d7d33c28ef821c274f53de73ee9cfde9d9df6d804114b48ea49c0641806f14b29acba6c844f38ab1680df324e0489f71c4','2016-05-02 17:00:00'),(8,'Imagemagick vulnerability','A vulnerability in ImageMagick has been released, please see https://imagetragick.com/ for more details.\\n\\nLibreNMS uses ImageMagick for pdf generation, we advise that you add the temporary fix in on the aforementioned website until patches for your distribution are available to update ImageMagick.','http://www.librenms.org/notifications.rss','1de2979056fe29c86abf5e2eee546e249ee22a7e850ad43123e42c306b9e0d0c76c0e97505821ee171323c0fc904fb0b9e5faefdb9153e0edc290fa983c1f4a5','2016-05-03 17:00:00'),(9,'Component Alerting Change','On July 25th we will be merging a PR that changes the status codes that are used for the component feature. If you have alerting rules that reference component.status, %macros.component_alert or %macros.component_normal please disable these until after the PR has been merged. Updated documentation will be merged with the PR to: http://docs.librenms.org/Extensions/Component/#alert','http://www.librenms.org/notifications.rss','036e0ae37d936c5dc155ed489b63f41be79e7abcbd0ccf4229a93da0a91848bb519e87604c1cc59a29b6e3a5ffb8beda8044286bc45f883956c6a66e7f46711f','2016-07-24 17:00:00'),(10,'The Alerts Menu has moved','The Alerts menu has been relocated to a top level menu on the far right.  To make room for this, Plugins have been moved\n   to the overview menu.','misc/notifications.rss','91e67bd37c425d154ac3cc336d1aa6888290253629fa4024e60559a2a20edbb0e14899ce129a97ff6800fd207d3bad40096869448c6a340f6c044e7c3fffdafe','2016-09-12 17:00:00'),(11,'Multimatic UPS detection changes','On December 21st we will be merging a PR that will change devices currently detected as multimatic to be detected as generex-ups.\\n\\nIf you have any alert rules or device groups that rely on this OS definition then you will need to update those rules to include generex-ups.','http://www.librenms.org/notifications.rss','75b6edfe8a162dc5414e12420da3950cd9fecc452c9fb6e2e5578698de5b3200bcd08ff194224db6acaed91faaa0647d98b711d2132edd1185408b0791b0e9e2','2016-12-18 17:00:00'),(12,'NUT Ups Application monitoring update','On December 20th we will be merging a PR that fixes the polling of data for NUT Ups devices.\\n\\nUnfortunately this will require users to delete the existing data and allow it to be re-created.\\n\\nTo do this, you can delete rrd files app-ups-nut-ID.rrd. ID will be a unique ID. After removing this file then data collection will start again.','http://www.librenms.org/notifications.rss','c5efb05c52f95df86dd3138dd90a1e287af142ff9dff1a5a52d8528d7c59cfea016fe16c969e9f952ac64d195100dfbe33d8386a385329207864514331eba1e4','2016-12-18 17:00:00'),(13,'Large schema update due to be released','Please see: https://community.librenms.org/t/large-schema-update-being-released/637 for further info.','http://www.librenms.org/notifications.rss','1dd85dc0cc62c6c84da356897048c73aafeea5d533b32813a7d409b968235cc53e24a614d736739621ca779f5ac2b8ed90fc99dbf19151d0359c24099a11edac','2017-02-22 17:00:00'),(14,'Change to extremeware and xos discovery','On the 10th of March, we are changing the way that OS detection is done for Extremeware and XOS devices, this could lead to the OS changing so if you have alerting or groups that reference these os names then please be aware that you will need to update them.','http://www.librenms.org/notifications.rss','09399fca57e340bf330aea8054c659c585c265ae6fe029a106fd088af7ed71bd971c18e330500600d39f018382425e521a1dfeb7487caf66691e99b574a689a8','2017-03-06 17:00:00'),(15,'Change to Ubiquiti Airfibre device type','On the 24th of March, we are changing device type for Ubiquiti AirFibre from network to wireless. See https://github.com/librenms/librenms/pull/6160 for PR.','http://www.librenms.org/notifications.rss','5dd4cc9d5075610638d2e36ec290d886e84a0d2156f11c8dba9612ab29cc8f5493b92c4a7cb514bee3f1cba68c254c83b8c263bc09925da2e70acbccde6eaed3','2017-03-20 17:00:00'),(16,'Change to some DNOS discovery','On the 24th of March, we are changing the detection of some Dell DNOS devices to PowerConnect. This is to show the difference between v6 and v9 of the DNOS OS. See https://github.com/librenms/librenms/pull/6206 for PR.','http://www.librenms.org/notifications.rss','e2e20560a5b18930ae62d730e4ec0442379e752f61d38afd97fd65e2518a61a2c02e11b3f6e79417bd7d14b120e05f8ab40b04802c21f500101a69295b3b9d7c','2017-03-22 17:00:00');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications_attribs`
--

DROP TABLE IF EXISTS `notifications_attribs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications_attribs` (
  `attrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `notifications_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`attrib_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications_attribs`
--

LOCK TABLES `notifications_attribs` WRITE;
/*!40000 ALTER TABLE `notifications_attribs` DISABLE KEYS */;
INSERT INTO `notifications_attribs` VALUES (1,12,1,'read','1'),(2,10,1,'read','1'),(3,11,1,'read','1'),(4,9,1,'read','1'),(5,8,1,'read','1'),(6,7,1,'read','1'),(7,6,1,'read','1'),(8,5,1,'read','1'),(9,4,1,'read','1'),(10,3,1,'read','1'),(11,2,1,'read','1'),(12,1,1,'read','1'),(13,12,2,'read','1'),(14,11,2,'read','1'),(15,10,2,'read','1'),(16,9,2,'read','1'),(17,8,2,'read','1'),(18,7,2,'read','1'),(19,6,2,'read','1'),(20,5,2,'read','1'),(21,4,2,'read','1'),(22,3,2,'read','1'),(23,2,2,'read','1'),(24,1,2,'read','1'),(25,13,1,'read','1'),(26,16,1,'read','1'),(27,14,1,'read','1'),(28,15,1,'read','1'),(29,16,2,'read','1'),(30,15,2,'read','1'),(31,14,2,'read','1'),(32,13,2,'read','1');
/*!40000 ALTER TABLE `notifications_attribs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_areas`
--

DROP TABLE IF EXISTS `ospf_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_areas` (
  `device_id` int(11) NOT NULL,
  `ospfAreaId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAuthType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ospfImportAsExtern` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ospfSpfRuns` int(11) NOT NULL,
  `ospfAreaBdrRtrCount` int(11) NOT NULL,
  `ospfAsBdrRtrCount` int(11) NOT NULL,
  `ospfAreaLsaCount` int(11) NOT NULL,
  `ospfAreaLsaCksumSum` int(11) NOT NULL,
  `ospfAreaSummary` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAreaStatus` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_area` (`device_id`,`ospfAreaId`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_areas`
--

LOCK TABLES `ospf_areas` WRITE;
/*!40000 ALTER TABLE `ospf_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_instances`
--

DROP TABLE IF EXISTS `ospf_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_instances` (
  `device_id` int(11) NOT NULL,
  `ospf_instance_id` int(11) NOT NULL,
  `ospfRouterId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAdminStat` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfVersionNumber` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAreaBdrRtrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfASBdrRtrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfExternLsaCount` int(11) NOT NULL,
  `ospfExternLsaCksumSum` int(11) NOT NULL,
  `ospfTOSSupport` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfOriginateNewLsas` int(11) NOT NULL,
  `ospfRxNewLsas` int(11) NOT NULL,
  `ospfExtLsdbLimit` int(11) DEFAULT NULL,
  `ospfMulticastExtensions` int(11) DEFAULT NULL,
  `ospfExitOverflowInterval` int(11) DEFAULT NULL,
  `ospfDemandExtensions` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_instance_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_instances`
--

LOCK TABLES `ospf_instances` WRITE;
/*!40000 ALTER TABLE `ospf_instances` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_nbrs`
--

DROP TABLE IF EXISTS `ospf_nbrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_nbrs` (
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `ospf_nbr_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrIpAddr` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrAddressLessIndex` int(11) NOT NULL,
  `ospfNbrRtrId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrOptions` int(11) NOT NULL,
  `ospfNbrPriority` int(11) NOT NULL,
  `ospfNbrState` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrEvents` int(11) NOT NULL,
  `ospfNbrLsRetransQLen` int(11) NOT NULL,
  `ospfNbmaNbrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbmaNbrPermanence` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrHelloSuppressed` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_nbr_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_nbrs`
--

LOCK TABLES `ospf_nbrs` WRITE;
/*!40000 ALTER TABLE `ospf_nbrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_nbrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_ports`
--

DROP TABLE IF EXISTS `ospf_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_ports` (
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `ospf_port_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfIfIpAddress` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAddressLessIf` int(11) NOT NULL,
  `ospfIfAreaId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfIfType` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfAdminStat` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfRtrPriority` int(11) DEFAULT NULL,
  `ospfIfTransitDelay` int(11) DEFAULT NULL,
  `ospfIfRetransInterval` int(11) DEFAULT NULL,
  `ospfIfHelloInterval` int(11) DEFAULT NULL,
  `ospfIfRtrDeadInterval` int(11) DEFAULT NULL,
  `ospfIfPollInterval` int(11) DEFAULT NULL,
  `ospfIfState` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfDesignatedRouter` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfBackupDesignatedRouter` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfEvents` int(11) DEFAULT NULL,
  `ospfIfAuthKey` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfStatus` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfMulticastForwarding` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfDemand` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfAuthType` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_port_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_ports`
--

LOCK TABLES `ospf_ports` WRITE;
/*!40000 ALTER TABLE `ospf_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `pkg_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `manager` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL,
  `version` varchar(255) COLLATE utf8_bin NOT NULL,
  `build` varchar(64) COLLATE utf8_bin NOT NULL,
  `arch` varchar(16) COLLATE utf8_bin NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pkg_id`),
  UNIQUE KEY `unique_key` (`device_id`,`name`,`manager`,`arch`,`version`,`build`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perf_times`
--

DROP TABLE IF EXISTS `perf_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perf_times` (
  `type` varchar(8) CHARACTER SET latin1 NOT NULL,
  `doing` varchar(64) CHARACTER SET latin1 NOT NULL,
  `start` int(11) NOT NULL,
  `duration` double(8,2) NOT NULL,
  `devices` int(11) NOT NULL,
  `poller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perf_times`
--

LOCK TABLES `perf_times` WRITE;
/*!40000 ALTER TABLE `perf_times` DISABLE KEYS */;
INSERT INTO `perf_times` VALUES ('pollbill','',1497252003,0.06,0,'ubuntu14vm\n'),('poll','21',1497252003,2.36,1,'ubuntu14vm\n');
/*!40000 ALTER TABLE `perf_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `plugin_id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(60) NOT NULL,
  `plugin_active` int(11) NOT NULL,
  PRIMARY KEY (`plugin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (1,'Test',0);
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poller_groups`
--

DROP TABLE IF EXISTS `poller_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poller_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `descr` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poller_groups`
--

LOCK TABLES `poller_groups` WRITE;
/*!40000 ALTER TABLE `poller_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `poller_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollers`
--

DROP TABLE IF EXISTS `pollers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poller_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `last_polled` datetime NOT NULL,
  `devices` int(11) NOT NULL,
  `time_taken` double NOT NULL,
  PRIMARY KEY (`poller_name`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollers`
--

LOCK TABLES `pollers` WRITE;
/*!40000 ALTER TABLE `pollers` DISABLE KEYS */;
INSERT INTO `pollers` VALUES (1,'idempiere-devel\n','2017-06-09 17:00:08',6,5),(3,'ubuntu14-vmware\n','2017-06-09 17:05:06',6,5),(4,'ubuntu14vm\n','2017-06-12 14:20:05',1,2),(2,'ubuntu16\n','2017-04-06 16:05:22',2,20);
/*!40000 ALTER TABLE `pollers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_association_mode`
--

DROP TABLE IF EXISTS `port_association_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_association_mode` (
  `pom_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  PRIMARY KEY (`pom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_association_mode`
--

LOCK TABLES `port_association_mode` WRITE;
/*!40000 ALTER TABLE `port_association_mode` DISABLE KEYS */;
INSERT INTO `port_association_mode` VALUES (1,'ifIndex'),(2,'ifName'),(3,'ifDescr'),(4,'ifAlias');
/*!40000 ALTER TABLE `port_association_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports`
--

DROP TABLE IF EXISTS `ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports` (
  `port_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `port_descr_type` varchar(255) DEFAULT NULL,
  `port_descr_descr` varchar(255) DEFAULT NULL,
  `port_descr_circuit` varchar(255) DEFAULT NULL,
  `port_descr_speed` varchar(32) DEFAULT NULL,
  `port_descr_notes` varchar(255) DEFAULT NULL,
  `ifDescr` varchar(255) DEFAULT NULL,
  `ifName` varchar(255) DEFAULT NULL,
  `portName` varchar(128) DEFAULT NULL,
  `ifIndex` bigint(20) DEFAULT '0',
  `ifSpeed` bigint(20) DEFAULT NULL,
  `ifConnectorPresent` varchar(12) DEFAULT NULL,
  `ifPromiscuousMode` varchar(12) DEFAULT NULL,
  `ifHighSpeed` int(11) DEFAULT NULL,
  `ifOperStatus` varchar(16) DEFAULT NULL,
  `ifOperStatus_prev` varchar(16) DEFAULT NULL,
  `ifAdminStatus` varchar(16) DEFAULT NULL,
  `ifAdminStatus_prev` varchar(16) DEFAULT NULL,
  `ifDuplex` varchar(12) DEFAULT NULL,
  `ifMtu` int(11) DEFAULT NULL,
  `ifType` text,
  `ifAlias` text,
  `ifPhysAddress` text,
  `ifHardType` varchar(64) DEFAULT NULL,
  `ifLastChange` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ifVlan` varchar(8) NOT NULL DEFAULT '',
  `ifTrunk` varchar(8) DEFAULT '',
  `ifVrf` int(11) NOT NULL DEFAULT '0',
  `counter_in` int(11) DEFAULT NULL,
  `counter_out` int(11) DEFAULT NULL,
  `ignore` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `detailed` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `pagpOperationMode` varchar(32) DEFAULT NULL,
  `pagpPortState` varchar(16) DEFAULT NULL,
  `pagpPartnerDeviceId` varchar(48) DEFAULT NULL,
  `pagpPartnerLearnMethod` varchar(16) DEFAULT NULL,
  `pagpPartnerIfIndex` int(11) DEFAULT NULL,
  `pagpPartnerGroupIfIndex` int(11) DEFAULT NULL,
  `pagpPartnerDeviceName` varchar(128) DEFAULT NULL,
  `pagpEthcOperationMode` varchar(16) DEFAULT NULL,
  `pagpDeviceId` varchar(48) DEFAULT NULL,
  `pagpGroupIfIndex` int(11) DEFAULT NULL,
  `ifInUcastPkts` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutUcastPkts` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_rate` int(11) DEFAULT NULL,
  `ifInErrors` bigint(20) DEFAULT NULL,
  `ifInErrors_prev` bigint(20) DEFAULT NULL,
  `ifInErrors_delta` bigint(20) DEFAULT NULL,
  `ifInErrors_rate` int(11) DEFAULT NULL,
  `ifOutErrors` bigint(20) DEFAULT NULL,
  `ifOutErrors_prev` bigint(20) DEFAULT NULL,
  `ifOutErrors_delta` bigint(20) DEFAULT NULL,
  `ifOutErrors_rate` int(11) DEFAULT NULL,
  `ifInOctets` bigint(20) DEFAULT NULL,
  `ifInOctets_prev` bigint(20) DEFAULT NULL,
  `ifInOctets_delta` bigint(20) DEFAULT NULL,
  `ifInOctets_rate` bigint(20) DEFAULT NULL,
  `ifOutOctets` bigint(20) DEFAULT NULL,
  `ifOutOctets_prev` bigint(20) DEFAULT NULL,
  `ifOutOctets_delta` bigint(20) DEFAULT NULL,
  `ifOutOctets_rate` bigint(20) DEFAULT NULL,
  `poll_time` int(11) DEFAULT NULL,
  `poll_prev` int(11) DEFAULT NULL,
  `poll_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`port_id`),
  UNIQUE KEY `device_ifIndex` (`device_id`,`ifIndex`),
  KEY `if_2` (`ifDescr`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports`
--

LOCK TABLES `ports` WRITE;
/*!40000 ALTER TABLE `ports` DISABLE KEYS */;
INSERT INTO `ports` VALUES (1,21,NULL,NULL,NULL,NULL,NULL,'lo','lo',NULL,1,10000000,'false','false',10,'up','up','up','up',NULL,65536,'softwareLoopback','lo',NULL,NULL,0,'','',0,NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18419,18156,263,1,18419,18156,263,1,0,0,0,0,0,0,0,0,3101921,3056940,44981,150,3101921,3056940,44981,150,1497252004,1497251705,299),(2,21,NULL,NULL,NULL,NULL,NULL,'eth0','eth0',NULL,2,NULL,'true','false',0,'up','up','up','up',NULL,1500,'ethernetCsmacd','eth0','000c29ac8b75',NULL,0,'','',0,NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1497252004,1497251705,299),(3,21,NULL,NULL,NULL,NULL,NULL,'eth1','eth1',NULL,3,NULL,'true','false',0,'up','up','up','up',NULL,1500,'ethernetCsmacd','eth1','000c29ac8b7f',NULL,0,'','',0,NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,59097,58134,963,3,85528,84037,1491,5,0,0,0,0,0,0,0,0,7675798,7427616,248182,830,33581690,32000642,1581048,5288,1497252004,1497251705,299),(4,21,NULL,NULL,NULL,NULL,NULL,'eth2','eth2',NULL,4,NULL,'true','false',0,'up','up','up','up',NULL,1500,'ethernetCsmacd','eth2','000c29ac8b89',NULL,20211,'','',0,NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,58562,57702,860,3,10074,10043,31,0,0,0,0,0,0,0,0,0,6848134,6758351,89783,300,944925,942065,2860,10,1497252004,1497251705,299);
/*!40000 ALTER TABLE `ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_adsl`
--

DROP TABLE IF EXISTS `ports_adsl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_adsl` (
  `port_id` int(11) NOT NULL,
  `port_adsl_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adslLineCoding` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslLineType` varchar(16) COLLATE utf8_bin NOT NULL,
  `adslAtucInvVendorID` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAtucInvVersionNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAtucCurrSnrMgn` decimal(5,1) NOT NULL,
  `adslAtucCurrAtn` decimal(5,1) NOT NULL,
  `adslAtucCurrOutputPwr` decimal(5,1) NOT NULL,
  `adslAtucCurrAttainableRate` int(11) NOT NULL,
  `adslAtucChanCurrTxRate` int(11) NOT NULL,
  `adslAturInvSerialNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturInvVendorID` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturInvVersionNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturChanCurrTxRate` int(11) NOT NULL,
  `adslAturCurrSnrMgn` decimal(5,1) NOT NULL,
  `adslAturCurrAtn` decimal(5,1) NOT NULL,
  `adslAturCurrOutputPwr` decimal(5,1) NOT NULL,
  `adslAturCurrAttainableRate` int(11) NOT NULL,
  UNIQUE KEY `interface_id` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_adsl`
--

LOCK TABLES `ports_adsl` WRITE;
/*!40000 ALTER TABLE `ports_adsl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_adsl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_perms`
--

DROP TABLE IF EXISTS `ports_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_perms` (
  `user_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `access_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_perms`
--

LOCK TABLES `ports_perms` WRITE;
/*!40000 ALTER TABLE `ports_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_stack`
--

DROP TABLE IF EXISTS `ports_stack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_stack` (
  `device_id` int(11) NOT NULL,
  `port_id_high` int(11) NOT NULL,
  `port_id_low` int(11) NOT NULL,
  `ifStackStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `device_id` (`device_id`,`port_id_high`,`port_id_low`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_stack`
--

LOCK TABLES `ports_stack` WRITE;
/*!40000 ALTER TABLE `ports_stack` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_stack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_statistics`
--

DROP TABLE IF EXISTS `ports_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_statistics` (
  `port_id` int(11) NOT NULL,
  `ifInNUcastPkts` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutNUcastPkts` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_rate` int(11) DEFAULT NULL,
  `ifInDiscards` bigint(20) DEFAULT NULL,
  `ifInDiscards_prev` bigint(20) DEFAULT NULL,
  `ifInDiscards_delta` bigint(20) DEFAULT NULL,
  `ifInDiscards_rate` int(11) DEFAULT NULL,
  `ifOutDiscards` bigint(20) DEFAULT NULL,
  `ifOutDiscards_prev` bigint(20) DEFAULT NULL,
  `ifOutDiscards_delta` bigint(20) DEFAULT NULL,
  `ifOutDiscards_rate` int(11) DEFAULT NULL,
  `ifInUnknownProtos` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_prev` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_delta` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_rate` int(11) DEFAULT NULL,
  `ifInBroadcastPkts` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutBroadcastPkts` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_rate` int(11) DEFAULT NULL,
  `ifInMulticastPkts` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_rate` int(11) DEFAULT NULL,
  `ifOutMulticastPkts` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_statistics`
--

LOCK TABLES `ports_statistics` WRITE;
/*!40000 ALTER TABLE `ports_statistics` DISABLE KEYS */;
INSERT INTO `ports_statistics` VALUES (1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `ports_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_stp`
--

DROP TABLE IF EXISTS `ports_stp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_stp` (
  `port_stp_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL,
  `state` varchar(11) NOT NULL,
  `enable` varchar(8) NOT NULL,
  `pathCost` int(10) unsigned NOT NULL,
  `designatedRoot` varchar(32) NOT NULL,
  `designatedCost` smallint(5) unsigned NOT NULL,
  `designatedBridge` varchar(32) NOT NULL,
  `designatedPort` mediumint(9) NOT NULL,
  `forwardTransitions` int(10) unsigned NOT NULL,
  PRIMARY KEY (`port_stp_id`),
  UNIQUE KEY `device_id` (`device_id`,`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_stp`
--

LOCK TABLES `ports_stp` WRITE;
/*!40000 ALTER TABLE `ports_stp` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_stp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_vlans`
--

DROP TABLE IF EXISTS `ports_vlans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_vlans` (
  `port_vlan_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `vlan` int(11) NOT NULL,
  `baseport` int(11) NOT NULL,
  `priority` bigint(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cost` int(11) NOT NULL,
  `untagged` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`port_vlan_id`),
  UNIQUE KEY `unique` (`device_id`,`port_id`,`vlan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_vlans`
--

LOCK TABLES `ports_vlans` WRITE;
/*!40000 ALTER TABLE `ports_vlans` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_vlans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processes`
--

DROP TABLE IF EXISTS `processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processes` (
  `device_id` int(11) NOT NULL,
  `pid` int(255) NOT NULL,
  `vsz` int(255) NOT NULL,
  `rss` int(255) NOT NULL,
  `cputime` varchar(12) COLLATE utf8_bin NOT NULL,
  `user` varchar(50) COLLATE utf8_bin NOT NULL,
  `command` varchar(255) COLLATE utf8_bin NOT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processes`
--

LOCK TABLES `processes` WRITE;
/*!40000 ALTER TABLE `processes` DISABLE KEYS */;
/*!40000 ALTER TABLE `processes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processors`
--

DROP TABLE IF EXISTS `processors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processors` (
  `processor_id` int(11) NOT NULL AUTO_INCREMENT,
  `entPhysicalIndex` int(11) NOT NULL DEFAULT '0',
  `hrDeviceIndex` int(11) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `processor_oid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `processor_index` varchar(32) CHARACTER SET latin1 NOT NULL,
  `processor_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `processor_usage` int(11) NOT NULL,
  `processor_descr` varchar(64) CHARACTER SET latin1 NOT NULL,
  `processor_precision` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`processor_id`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processors`
--

LOCK TABLES `processors` WRITE;
/*!40000 ALTER TABLE `processors` DISABLE KEYS */;
INSERT INTO `processors` VALUES (1,0,196608,21,'.1.3.6.1.2.1.25.3.3.1.2.196608','196608','hr',4,'Intel Core i5-3340M @ 2.70GHz',1);
/*!40000 ALTER TABLE `processors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxmox`
--

DROP TABLE IF EXISTS `proxmox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxmox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `vmid` int(11) NOT NULL,
  `cluster` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cluster_vm` (`cluster`,`vmid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxmox`
--

LOCK TABLES `proxmox` WRITE;
/*!40000 ALTER TABLE `proxmox` DISABLE KEYS */;
/*!40000 ALTER TABLE `proxmox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxmox_ports`
--

DROP TABLE IF EXISTS `proxmox_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxmox_ports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vm_id` int(11) NOT NULL,
  `port` varchar(10) NOT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vm_port` (`vm_id`,`port`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxmox_ports`
--

LOCK TABLES `proxmox_ports` WRITE;
/*!40000 ALTER TABLE `proxmox_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `proxmox_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pseudowires`
--

DROP TABLE IF EXISTS `pseudowires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pseudowires` (
  `pseudowire_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `peer_device_id` int(11) NOT NULL,
  `peer_ldp_id` int(11) NOT NULL,
  `cpwVcID` int(11) NOT NULL,
  `cpwOid` int(11) NOT NULL,
  `pw_type` varchar(32) NOT NULL,
  `pw_psntype` varchar(32) NOT NULL,
  `pw_local_mtu` int(11) NOT NULL,
  `pw_peer_mtu` int(11) NOT NULL,
  `pw_descr` varchar(128) NOT NULL,
  PRIMARY KEY (`pseudowire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pseudowires`
--

LOCK TABLES `pseudowires` WRITE;
/*!40000 ALTER TABLE `pseudowires` DISABLE KEYS */;
/*!40000 ALTER TABLE `pseudowires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `device_id` int(11) NOT NULL,
  `context_name` varchar(128) NOT NULL,
  `ipRouteDest` varchar(256) NOT NULL,
  `ipRouteIfIndex` varchar(256) DEFAULT NULL,
  `ipRouteMetric` varchar(256) NOT NULL,
  `ipRouteNextHop` varchar(256) NOT NULL,
  `ipRouteType` varchar(256) NOT NULL,
  `ipRouteProto` varchar(256) NOT NULL,
  `discoveredAt` int(11) NOT NULL,
  `ipRouteMask` varchar(256) NOT NULL,
  KEY `device` (`device_id`,`context_name`,`ipRouteDest`(255),`ipRouteNextHop`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `sensor_class` varchar(64) CHARACTER SET latin1 NOT NULL,
  `device_id` int(11) unsigned NOT NULL DEFAULT '0',
  `poller_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'snmp',
  `sensor_oid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sensor_index` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sensor_descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_divisor` int(11) NOT NULL DEFAULT '1',
  `sensor_multiplier` int(11) NOT NULL DEFAULT '1',
  `sensor_current` float DEFAULT NULL,
  `sensor_limit` float DEFAULT NULL,
  `sensor_limit_warn` float DEFAULT NULL,
  `sensor_limit_low` float DEFAULT NULL,
  `sensor_limit_low_warn` float DEFAULT NULL,
  `sensor_alert` tinyint(1) NOT NULL DEFAULT '1',
  `sensor_custom` enum('No','Yes') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `entPhysicalIndex` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `entPhysicalIndex_measured` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sensor_prev` float DEFAULT NULL,
  PRIMARY KEY (`sensor_id`),
  KEY `sensor_host` (`device_id`),
  KEY `sensor_class` (`sensor_class`),
  KEY `sensor_type` (`sensor_type`),
  CONSTRAINT `sensors_device_id_foreign` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors`
--

LOCK TABLES `sensors` WRITE;
/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors_to_state_indexes`
--

DROP TABLE IF EXISTS `sensors_to_state_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors_to_state_indexes` (
  `sensors_to_state_translations_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) NOT NULL,
  `state_index_id` int(11) NOT NULL,
  PRIMARY KEY (`sensors_to_state_translations_id`),
  UNIQUE KEY `sensor_id_state_index_id` (`sensor_id`,`state_index_id`),
  KEY `state_index_id` (`state_index_id`),
  CONSTRAINT `sensors_to_state_indexes_ibfk_2` FOREIGN KEY (`state_index_id`) REFERENCES `state_indexes` (`state_index_id`),
  CONSTRAINT `sensors_to_state_indexes_sensor_id_foreign` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`sensor_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors_to_state_indexes`
--

LOCK TABLES `sensors_to_state_indexes` WRITE;
/*!40000 ALTER TABLE `sensors_to_state_indexes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensors_to_state_indexes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `service_ip` text NOT NULL,
  `service_type` varchar(16) NOT NULL,
  `service_desc` text NOT NULL,
  `service_param` text NOT NULL,
  `service_ignore` tinyint(1) NOT NULL,
  `service_status` tinyint(4) NOT NULL DEFAULT '0',
  `service_changed` int(11) NOT NULL DEFAULT '0',
  `service_message` text NOT NULL,
  `service_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `service_ds` varchar(400) NOT NULL COMMENT 'Data Sources available for this service',
  PRIMARY KEY (`service_id`),
  KEY `service_host` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_username` varchar(30) NOT NULL,
  `session_value` varchar(60) NOT NULL,
  `session_token` varchar(60) NOT NULL,
  `session_auth` varchar(16) NOT NULL,
  `session_expiry` int(11) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_value` (`session_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slas`
--

DROP TABLE IF EXISTS `slas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slas` (
  `sla_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `sla_nr` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8_bin NOT NULL,
  `tag` varchar(255) COLLATE utf8_bin NOT NULL,
  `rtt_type` varchar(16) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL,
  `opstatus` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sla_id`),
  UNIQUE KEY `unique_key` (`device_id`,`sla_nr`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slas`
--

LOCK TABLES `slas` WRITE;
/*!40000 ALTER TABLE `slas` DISABLE KEYS */;
/*!40000 ALTER TABLE `slas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_indexes`
--

DROP TABLE IF EXISTS `state_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_indexes` (
  `state_index_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`state_index_id`),
  UNIQUE KEY `state_name` (`state_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_indexes`
--

LOCK TABLES `state_indexes` WRITE;
/*!40000 ALTER TABLE `state_indexes` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_indexes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_translations`
--

DROP TABLE IF EXISTS `state_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_translations` (
  `state_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_index_id` int(11) NOT NULL,
  `state_descr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_draw_graph` tinyint(1) NOT NULL,
  `state_value` smallint(5) unsigned NOT NULL,
  `state_generic_value` tinyint(1) NOT NULL,
  `state_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`state_translation_id`),
  UNIQUE KEY `state_index_id_value` (`state_index_id`,`state_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_translations`
--

LOCK TABLES `state_translations` WRITE;
/*!40000 ALTER TABLE `state_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage`
--

DROP TABLE IF EXISTS `storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage` (
  `storage_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `storage_mib` varchar(16) NOT NULL,
  `storage_index` int(11) NOT NULL,
  `storage_type` varchar(32) DEFAULT NULL,
  `storage_descr` text NOT NULL,
  `storage_size` bigint(20) NOT NULL,
  `storage_units` int(11) NOT NULL,
  `storage_used` bigint(20) NOT NULL DEFAULT '0',
  `storage_free` bigint(20) NOT NULL DEFAULT '0',
  `storage_perc` int(11) NOT NULL DEFAULT '0',
  `storage_perc_warn` int(11) DEFAULT '60',
  `storage_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`storage_id`),
  UNIQUE KEY `index_unique` (`device_id`,`storage_mib`,`storage_index`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage`
--

LOCK TABLES `storage` WRITE;
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
INSERT INTO `storage` VALUES (1,21,'hrstorage',31,'hrStorageFixedDisk','/',37912903680,4096,32408633344,5504270336,85,60,0),(2,21,'hrstorage',35,'hrStorageFixedDisk','/sys/fs/cgroup',4096,4096,0,4096,0,60,0),(3,21,'hrstorage',41,'hrStorageFixedDisk','/run',208408576,4096,1572864,206835712,1,60,0),(4,21,'hrstorage',42,'hrStorageFixedDisk','/run/lock',5242880,4096,0,5242880,0,60,0),(5,21,'hrstorage',43,'hrStorageFixedDisk','/run/shm',1042034688,4096,0,1042034688,0,60,0),(6,21,'hrstorage',44,'hrStorageFixedDisk','/run/user',104857600,4096,0,104857600,0,60,0);
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp`
--

DROP TABLE IF EXISTS `stp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp` (
  `stp_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `rootBridge` tinyint(1) NOT NULL,
  `bridgeAddress` varchar(32) NOT NULL,
  `protocolSpecification` varchar(16) NOT NULL,
  `priority` mediumint(9) NOT NULL,
  `timeSinceTopologyChange` int(10) unsigned NOT NULL,
  `topChanges` mediumint(9) NOT NULL,
  `designatedRoot` varchar(32) NOT NULL,
  `rootCost` mediumint(9) NOT NULL,
  `rootPort` mediumint(9) NOT NULL,
  `maxAge` mediumint(9) NOT NULL,
  `helloTime` mediumint(9) NOT NULL,
  `holdTime` mediumint(9) NOT NULL,
  `forwardDelay` mediumint(9) NOT NULL,
  `bridgeMaxAge` smallint(6) NOT NULL,
  `bridgeHelloTime` smallint(6) NOT NULL,
  `bridgeForwardDelay` smallint(6) NOT NULL,
  PRIMARY KEY (`stp_id`),
  KEY `stp_host` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp`
--

LOCK TABLES `stp` WRITE;
/*!40000 ALTER TABLE `stp` DISABLE KEYS */;
/*!40000 ALTER TABLE `stp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syslog`
--

DROP TABLE IF EXISTS `syslog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `syslog` (
  `device_id` int(11) DEFAULT NULL,
  `facility` varchar(10) DEFAULT NULL,
  `priority` varchar(10) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `tag` varchar(10) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `program` varchar(32) DEFAULT NULL,
  `msg` text,
  `seq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seq`),
  KEY `datetime` (`timestamp`),
  KEY `device_id` (`device_id`),
  KEY `program` (`program`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syslog`
--

LOCK TABLES `syslog` WRITE;
/*!40000 ALTER TABLE `syslog` DISABLE KEYS */;
/*!40000 ALTER TABLE `syslog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toner`
--

DROP TABLE IF EXISTS `toner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toner` (
  `toner_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `toner_index` int(11) NOT NULL,
  `toner_type` varchar(64) NOT NULL,
  `toner_oid` varchar(64) NOT NULL,
  `toner_descr` varchar(32) NOT NULL DEFAULT '',
  `toner_capacity` int(11) NOT NULL DEFAULT '0',
  `toner_current` int(11) NOT NULL DEFAULT '0',
  `toner_capacity_oid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`toner_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toner`
--

LOCK TABLES `toner` WRITE;
/*!40000 ALTER TABLE `toner` DISABLE KEYS */;
/*!40000 ALTER TABLE `toner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tselregion`
--

DROP TABLE IF EXISTS `tselregion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tselregion` (
  `tselregion_id` int(11) NOT NULL AUTO_INCREMENT,
  `tselregion_name` varchar(45) NOT NULL,
  PRIMARY KEY (`tselregion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tselregion`
--

LOCK TABLES `tselregion` WRITE;
/*!40000 ALTER TABLE `tselregion` DISABLE KEYS */;
INSERT INTO `tselregion` VALUES (1,'SUMBAGSEL'),(2,'SUMBAGTENG'),(3,'SUMBAGUT');
/*!40000 ALTER TABLE `tselregion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucd_diskio`
--

DROP TABLE IF EXISTS `ucd_diskio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucd_diskio` (
  `diskio_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `diskio_index` int(11) NOT NULL,
  `diskio_descr` varchar(32) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`diskio_id`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucd_diskio`
--

LOCK TABLES `ucd_diskio` WRITE;
/*!40000 ALTER TABLE `ucd_diskio` DISABLE KEYS */;
INSERT INTO `ucd_diskio` VALUES (1,21,25,'sda'),(2,21,26,'sda1'),(3,21,27,'sda2'),(4,21,28,'sda5');
/*!40000 ALTER TABLE `ucd_diskio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `realname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `descr` varchar(200) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `can_modify_passwd` tinyint(4) NOT NULL DEFAULT '1',
  `twofactor` varchar(255) NOT NULL,
  `dashboard` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$P$Bd8WoWNF4FzJXKplpR/9WAV9nG2oUK.','','rinto@sinaga.or.id','',10,1,'0',0),(2,'rintoexa','$P$BUXBeo92UcKtZS6F.LgPYgONNgU1ov1','Rinto Exandi','','',5,1,'0',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_prefs`
--

DROP TABLE IF EXISTS `users_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_prefs` (
  `user_id` int(16) NOT NULL,
  `pref` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id.pref` (`user_id`,`pref`),
  KEY `pref` (`pref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_prefs`
--

LOCK TABLES `users_prefs` WRITE;
/*!40000 ALTER TABLE `users_prefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_widgets`
--

DROP TABLE IF EXISTS `users_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_widgets` (
  `user_widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `widget_id` int(11) NOT NULL,
  `col` tinyint(4) NOT NULL,
  `row` tinyint(4) NOT NULL,
  `size_x` tinyint(4) NOT NULL,
  `size_y` tinyint(4) NOT NULL,
  `title` varchar(255) NOT NULL,
  `refresh` tinyint(4) NOT NULL DEFAULT '60',
  `settings` text NOT NULL,
  `dashboard_id` int(11) NOT NULL,
  PRIMARY KEY (`user_widget_id`),
  KEY `user_id` (`user_id`,`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_widgets`
--

LOCK TABLES `users_widgets` WRITE;
/*!40000 ALTER TABLE `users_widgets` DISABLE KEYS */;
INSERT INTO `users_widgets` VALUES (20,2,16,1,1,6,3,'Bill Overview',60,'',2),(37,1,18,13,4,6,3,'Top Occupancy',60,'{\"title\":\"Top Occupancy\",\"witel_id\":\"other\",\"device_count\":\"10\"}',3),(41,2,18,7,1,6,3,'Top Occupancy',60,'',2),(69,1,19,13,1,6,3,'Occupancy Overview',60,'{\"title\":\"\",\"device_count\":\"10\"}',3),(79,1,20,1,1,12,6,'Occupancy By Witel',60,'',3);
/*!40000 ALTER TABLE `users_widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_billday_extended`
--

DROP TABLE IF EXISTS `v_billday_extended`;
/*!50001 DROP VIEW IF EXISTS `v_billday_extended`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_billday_extended` (
  `bill_id` tinyint NOT NULL,
  `bd_year` tinyint NOT NULL,
  `bd_month` tinyint NOT NULL,
  `1` tinyint NOT NULL,
  `2` tinyint NOT NULL,
  `3` tinyint NOT NULL,
  `4` tinyint NOT NULL,
  `5` tinyint NOT NULL,
  `6` tinyint NOT NULL,
  `7` tinyint NOT NULL,
  `8` tinyint NOT NULL,
  `9` tinyint NOT NULL,
  `10` tinyint NOT NULL,
  `11` tinyint NOT NULL,
  `12` tinyint NOT NULL,
  `13` tinyint NOT NULL,
  `14` tinyint NOT NULL,
  `15` tinyint NOT NULL,
  `16` tinyint NOT NULL,
  `17` tinyint NOT NULL,
  `18` tinyint NOT NULL,
  `19` tinyint NOT NULL,
  `20` tinyint NOT NULL,
  `21` tinyint NOT NULL,
  `22` tinyint NOT NULL,
  `23` tinyint NOT NULL,
  `24` tinyint NOT NULL,
  `25` tinyint NOT NULL,
  `26` tinyint NOT NULL,
  `27` tinyint NOT NULL,
  `28` tinyint NOT NULL,
  `29` tinyint NOT NULL,
  `30` tinyint NOT NULL,
  `31` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_listbilldaily`
--

DROP TABLE IF EXISTS `v_listbilldaily`;
/*!50001 DROP VIEW IF EXISTS `v_listbilldaily`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_listbilldaily` (
  `bill_id` tinyint NOT NULL,
  `bill_cdr` tinyint NOT NULL,
  `bd_year` tinyint NOT NULL,
  `bd_month` tinyint NOT NULL,
  `bill_date` tinyint NOT NULL,
  `percentage` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_listbilldailybysite`
--

DROP TABLE IF EXISTS `v_listbilldailybysite`;
/*!50001 DROP VIEW IF EXISTS `v_listbilldailybysite`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_listbilldailybysite` (
  `bill_id` tinyint NOT NULL,
  `bill_name` tinyint NOT NULL,
  `bill_cdr` tinyint NOT NULL,
  `rate_95th_in` tinyint NOT NULL,
  `rate_95th_out` tinyint NOT NULL,
  `rate_95th` tinyint NOT NULL,
  `percentage` tinyint NOT NULL,
  `bill_day` tinyint NOT NULL,
  `device_id` tinyint NOT NULL,
  `site_name` tinyint NOT NULL,
  `site_id` tinyint NOT NULL,
  `hostname` tinyint NOT NULL,
  `witel_id` tinyint NOT NULL,
  `witelname` tinyint NOT NULL,
  `port_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vlans`
--

DROP TABLE IF EXISTS `vlans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vlans` (
  `vlan_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `vlan_vlan` int(11) DEFAULT NULL,
  `vlan_domain` int(11) DEFAULT NULL,
  `vlan_name` varchar(64) DEFAULT NULL,
  `vlan_type` varchar(16) DEFAULT NULL,
  `vlan_mtu` int(11) DEFAULT NULL,
  PRIMARY KEY (`vlan_id`),
  KEY `device_id` (`device_id`,`vlan_vlan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vlans`
--

LOCK TABLES `vlans` WRITE;
/*!40000 ALTER TABLE `vlans` DISABLE KEYS */;
/*!40000 ALTER TABLE `vlans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vminfo`
--

DROP TABLE IF EXISTS `vminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vminfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `vm_type` varchar(16) NOT NULL DEFAULT 'vmware',
  `vmwVmVMID` int(11) NOT NULL,
  `vmwVmDisplayName` varchar(128) NOT NULL,
  `vmwVmGuestOS` varchar(128) NOT NULL,
  `vmwVmMemSize` int(11) NOT NULL,
  `vmwVmCpus` int(11) NOT NULL,
  `vmwVmState` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  KEY `vmwVmVMID` (`vmwVmVMID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vminfo`
--

LOCK TABLES `vminfo` WRITE;
/*!40000 ALTER TABLE `vminfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vminfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrf_lite_cisco`
--

DROP TABLE IF EXISTS `vrf_lite_cisco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrf_lite_cisco` (
  `vrf_lite_cisco_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `context_name` varchar(128) NOT NULL,
  `intance_name` varchar(128) DEFAULT '',
  `vrf_name` varchar(128) DEFAULT 'Default',
  PRIMARY KEY (`vrf_lite_cisco_id`),
  KEY `vrf` (`vrf_name`),
  KEY `context` (`context_name`),
  KEY `device` (`device_id`),
  KEY `mix` (`device_id`,`context_name`,`vrf_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrf_lite_cisco`
--

LOCK TABLES `vrf_lite_cisco` WRITE;
/*!40000 ALTER TABLE `vrf_lite_cisco` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrf_lite_cisco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrfs`
--

DROP TABLE IF EXISTS `vrfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrfs` (
  `vrf_id` int(11) NOT NULL AUTO_INCREMENT,
  `vrf_oid` varchar(256) NOT NULL,
  `vrf_name` varchar(128) DEFAULT NULL,
  `mplsVpnVrfRouteDistinguisher` varchar(128) DEFAULT NULL,
  `mplsVpnVrfDescription` text NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`vrf_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrfs`
--

LOCK TABLES `vrfs` WRITE;
/*!40000 ALTER TABLE `vrfs` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrfs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_title` varchar(255) NOT NULL,
  `widget` varchar(255) NOT NULL,
  `base_dimensions` varchar(10) NOT NULL,
  PRIMARY KEY (`widget_id`),
  UNIQUE KEY `widget` (`widget`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,'Availability map','availability-map','6,3'),(2,'Device summary horizontal','device-summary-horiz','6,3'),(3,'Alerts','alerts','6,3'),(4,'Device summary vertical','device-summary-vert','6,3'),(5,'Globe map','globe','6,3'),(6,'Syslog','syslog','6,3'),(7,'Eventlog','eventlog','6,3'),(8,'World map','worldmap','6,3'),(9,'Graylog','graylog','6,3'),(10,'Graph','generic-graph','6,3'),(11,'Top Devices','top-devices','6,3'),(12,'Top Interfaces','top-interfaces','6,3'),(13,'Notes','notes','6,3'),(14,'External Images','generic-image','6,3'),(15,'Component Status','component-status','6,3'),(16,'Bill Overview','bill-summary-witel','12,3'),(18,'Top Occupancy','top-occupancies','6,3'),(19,'Occupancy Overview','occupancy-overview','6,3'),(20,'Occupancy By Witel','occupancy-overview-bywitel','12,6');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `witel`
--

DROP TABLE IF EXISTS `witel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `witel` (
  `witel_id` int(11) NOT NULL AUTO_INCREMENT,
  `witelname` varchar(35) NOT NULL,
  PRIMARY KEY (`witel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `witel`
--

LOCK TABLES `witel` WRITE;
/*!40000 ALTER TABLE `witel` DISABLE KEYS */;
INSERT INTO `witel` VALUES (19,'Aceh'),(20,'Bandar Lampung'),(21,'Batam'),(22,'Bengkulu'),(23,'Jambi'),(24,'Medan'),(25,'Padang'),(26,'Palembang'),(27,'Pangkal Pinang'),(28,'Pekanbaru'),(29,'Pematangsiantar');
/*!40000 ALTER TABLE `witel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `v_billday_extended`
--

/*!50001 DROP TABLE IF EXISTS `v_billday_extended`*/;
/*!50001 DROP VIEW IF EXISTS `v_billday_extended`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_billday_extended` AS select `v_listbilldaily`.`bill_id` AS `bill_id`,`v_listbilldaily`.`bd_year` AS `bd_year`,`v_listbilldaily`.`bd_month` AS `bd_month`,(case when (`v_listbilldaily`.`bill_date` = 1) then `v_listbilldaily`.`percentage` else 0 end) AS `1`,(case when (`v_listbilldaily`.`bill_date` = 2) then `v_listbilldaily`.`percentage` else 0 end) AS `2`,(case when (`v_listbilldaily`.`bill_date` = 3) then `v_listbilldaily`.`percentage` else 0 end) AS `3`,(case when (`v_listbilldaily`.`bill_date` = 4) then `v_listbilldaily`.`percentage` else 0 end) AS `4`,(case when (`v_listbilldaily`.`bill_date` = 5) then `v_listbilldaily`.`percentage` else 0 end) AS `5`,(case when (`v_listbilldaily`.`bill_date` = 6) then `v_listbilldaily`.`percentage` else 0 end) AS `6`,(case when (`v_listbilldaily`.`bill_date` = 7) then `v_listbilldaily`.`percentage` else 0 end) AS `7`,(case when (`v_listbilldaily`.`bill_date` = 8) then `v_listbilldaily`.`percentage` else 0 end) AS `8`,(case when (`v_listbilldaily`.`bill_date` = 9) then `v_listbilldaily`.`percentage` else 0 end) AS `9`,(case when (`v_listbilldaily`.`bill_date` = 10) then `v_listbilldaily`.`percentage` else 0 end) AS `10`,(case when (`v_listbilldaily`.`bill_date` = 11) then `v_listbilldaily`.`percentage` else 0 end) AS `11`,(case when (`v_listbilldaily`.`bill_date` = 12) then `v_listbilldaily`.`percentage` else 0 end) AS `12`,(case when (`v_listbilldaily`.`bill_date` = 13) then `v_listbilldaily`.`percentage` else 0 end) AS `13`,(case when (`v_listbilldaily`.`bill_date` = 14) then `v_listbilldaily`.`percentage` else 0 end) AS `14`,(case when (`v_listbilldaily`.`bill_date` = 15) then `v_listbilldaily`.`percentage` else 0 end) AS `15`,(case when (`v_listbilldaily`.`bill_date` = 16) then `v_listbilldaily`.`percentage` else 0 end) AS `16`,(case when (`v_listbilldaily`.`bill_date` = 17) then `v_listbilldaily`.`percentage` else 0 end) AS `17`,(case when (`v_listbilldaily`.`bill_date` = 18) then `v_listbilldaily`.`percentage` else 0 end) AS `18`,(case when (`v_listbilldaily`.`bill_date` = 19) then `v_listbilldaily`.`percentage` else 0 end) AS `19`,(case when (`v_listbilldaily`.`bill_date` = 20) then `v_listbilldaily`.`percentage` else 0 end) AS `20`,(case when (`v_listbilldaily`.`bill_date` = 21) then `v_listbilldaily`.`percentage` else 0 end) AS `21`,(case when (`v_listbilldaily`.`bill_date` = 22) then `v_listbilldaily`.`percentage` else 0 end) AS `22`,(case when (`v_listbilldaily`.`bill_date` = 23) then `v_listbilldaily`.`percentage` else 0 end) AS `23`,(case when (`v_listbilldaily`.`bill_date` = 24) then `v_listbilldaily`.`percentage` else 0 end) AS `24`,(case when (`v_listbilldaily`.`bill_date` = 25) then `v_listbilldaily`.`percentage` else 0 end) AS `25`,(case when (`v_listbilldaily`.`bill_date` = 26) then `v_listbilldaily`.`percentage` else 0 end) AS `26`,(case when (`v_listbilldaily`.`bill_date` = 27) then `v_listbilldaily`.`percentage` else 0 end) AS `27`,(case when (`v_listbilldaily`.`bill_date` = 28) then `v_listbilldaily`.`percentage` else 0 end) AS `28`,(case when (`v_listbilldaily`.`bill_date` = 29) then `v_listbilldaily`.`percentage` else 0 end) AS `29`,(case when (`v_listbilldaily`.`bill_date` = 30) then `v_listbilldaily`.`percentage` else 0 end) AS `30`,(case when (`v_listbilldaily`.`bill_date` = 31) then `v_listbilldaily`.`percentage` else 0 end) AS `31` from `v_listbilldaily` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_listbilldaily`
--

/*!50001 DROP TABLE IF EXISTS `v_listbilldaily`*/;
/*!50001 DROP VIEW IF EXISTS `v_listbilldaily`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_listbilldaily` AS select `bd`.`bill_id` AS `bill_id`,`bd`.`bill_cdr` AS `bill_cdr`,year(`bd`.`bill_day`) AS `bd_year`,month(`bd`.`bill_day`) AS `bd_month`,dayofmonth(`bd`.`bill_day`) AS `bill_date`,max(`bd`.`percentage`) AS `percentage` from `bill_daily` `bd` group by dayofmonth(`bd`.`bill_day`),month(`bd`.`bill_day`),year(`bd`.`bill_day`),`bd`.`bill_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_listbilldailybysite`
--

/*!50001 DROP TABLE IF EXISTS `v_listbilldailybysite`*/;
/*!50001 DROP VIEW IF EXISTS `v_listbilldailybysite`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_listbilldailybysite` AS select `B`.`bill_id` AS `bill_id`,`B`.`bill_name` AS `bill_name`,`bd`.`bill_cdr` AS `bill_cdr`,`bd`.`rate_95th_in` AS `rate_95th_in`,`bd`.`rate_95th_out` AS `rate_95th_out`,`bd`.`rate_95th` AS `rate_95th`,max(`bd`.`percentage`) AS `percentage`,`bd`.`bill_day` AS `bill_day`,`D`.`device_id` AS `device_id`,`D`.`site_name` AS `site_name`,`D`.`site_id` AS `site_id`,`D`.`hostname` AS `hostname`,`D`.`witel_id` AS `witel_id`,`witel`.`witelname` AS `witelname`,`ports`.`port_id` AS `port_id` from (((((`bills` `B` join `bill_ports` on((`B`.`bill_id` = `bill_ports`.`bill_id`))) join `ports` on((`bill_ports`.`port_id` = `ports`.`port_id`))) join `devices` `D` on((`ports`.`device_id` = `D`.`device_id`))) join `witel` on((`D`.`witel_id` = `witel`.`witel_id`))) join `bill_daily` `bd` on((`bd`.`bill_id` = `B`.`bill_id`))) group by `B`.`bill_id`,dayofmonth(`bd`.`bill_day`),month(`bd`.`bill_day`),year(`bd`.`bill_day`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-12 14:21:45
