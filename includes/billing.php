<?php


function format_bytes_billing($value)
{
    global $config;

    return format_number($value, $config['billing']['base']).'B';
}//end format_bytes_billing()


function format_bytes_billing_short($value)
{
    global $config;

    return format_number($value, $config['billing']['base'], 2, 1);
}//end format_bytes_billing_short()


function getDates($dayofmonth, $months = 0)
{
    $dayofmonth = zeropad($dayofmonth);
    $year       = date('Y');
    $month      = date('m');

    if (date('d') > $dayofmonth) {
        // Billing day is past, so it is next month
        $date_end   = date_create($year.'-'.$month.'-'.$dayofmonth);
        $date_start = date_create($year.'-'.$month.'-'.$dayofmonth);
        date_add($date_end, date_interval_create_from_date_string('1 month'));
    } else {
        // Billing day will happen this month, therefore started last month
        $date_end   = date_create($year.'-'.$month.'-'.$dayofmonth);
        $date_start = date_create($year.'-'.$month.'-'.$dayofmonth);
        date_sub($date_start, date_interval_create_from_date_string('1 month'));
    }

    if ($months > 0) {
        date_sub($date_start, date_interval_create_from_date_string($months.' month'));
        date_sub($date_end, date_interval_create_from_date_string($months.' month'));
    }

    // date_sub($date_start, date_interval_create_from_date_string('1 month'));
    date_sub($date_end, date_interval_create_from_date_string('1 day'));

    $date_from = date_format($date_start, 'Ymd').'000000';
    $date_to   = date_format($date_end, 'Ymd').'235959';

    date_sub($date_start, date_interval_create_from_date_string('1 month'));
    date_sub($date_end, date_interval_create_from_date_string('1 month'));

    $last_from = date_format($date_start, 'Ymd').'000000';
    $last_to   = date_format($date_end, 'Ymd').'235959';

    $return      = array();
    $return['0'] = $date_from;
    $return['1'] = $date_to;
    $return['2'] = $last_from;
    $return['3'] = $last_to;

    return ($return);
}//end getDates()

function getPredictedUsage($bill_day, $cur_used)
{

    $tmp = getDates($bill_day, 0);
    $start = new DateTime($tmp[0], new DateTimeZone(date_default_timezone_get()));
    $end   = new DateTime($tmp[1], new DateTimeZone(date_default_timezone_get()));
    $now   = new DateTime(date("Y-m-d"), new DateTimeZone(date_default_timezone_get()));
    $total = $end->diff($start)->format("%a");
    $since = $now->diff($start)->format("%a");
    return($cur_used/$since*$total);
}

function getValue($host, $port, $id, $inout)
{
    global $config;

    if ($inout=="In") {
        //MIB IfInOctects
        $oid="1.3.6.1.2.1.31.1.1.1.6." . $id;
        }
    else
    {
       //MIB IfOutOctects
        $oid="1.3.6.1.2.1.31.1.1.1.10." .$id;
    }

    $device = dbFetchRow("SELECT * from `devices` WHERE `hostname` = '".mres($host)."' LIMIT 1");

    $tmpvalue = snmp_get($device,$oid);

    $value=trim(substr($tmpvalue, strpos($tmpvalue, ':') + 1));

    if (!is_numeric($value)) {

        $value=0;
    }

    $msg ="Fetching traffic for " . $host . " " . $inout . " Result : " . $value . "\n";

    echo $msg;

   // error_log($msg);

    return $value;
}//end getValue()

function getPollerValue($portid, $inout)
{
    //In
    if ($inout=='In') {

        $ifVal = dbFetchRow("SELECT ifInOctets as traffic from ports WHERE port_id = '".$portid."' LIMIT 1");

        $msg="SELECT ifInOctets  from ports WHERE port_id = '".$portid."' LIMIT 1";
    }
    //Out
    else {

        $ifVal = dbFetchRow("SELECT ifOutOctets as traffic from ports WHERE port_id = '". $portid . "' LIMIT 1");
        $msg="SELECT ifOutOctets from ports WHERE port_id = '".$portid."' LIMIT 1";
    }


    if (is_null($ifVal)) {

        $value=0;
    }

    else {

        $value=$ifVal['traffic'];

    }

    return $value;
}//end getValue()

function getLastPortCounter($port_id, $bill_id)
{
    $return = array();
    $row    = dbFetchRow("SELECT timestamp, in_counter, in_delta, out_counter, out_delta FROM bill_port_counters WHERE `port_id` = ? AND `bill_id` = ?", array($port_id, $bill_id));
    if (!is_null($row)) {
        $return[timestamp]   = $row['timestamp'];
        $return[in_counter]  = $row['in_counter'];
        $return[in_delta]    = $row['in_delta'];
        $return[out_counter] = $row['out_counter'];
        $return[out_delta]   = $row['out_delta'];
        $return[state]       = 'ok';
    } else {
        $return[state]       = 'failed';
    }
    return $return;
}//end getLastPortCounter()


function getLastMeasurement($bill_id)
{
    $return = array();
    $row    = dbFetchRow("SELECT timestamp,delta,in_delta,out_delta FROM bill_data WHERE bill_id = ? ORDER BY timestamp DESC LIMIT 1", array($bill_id));
    if (!is_null($row)) {
        $return[delta]     = $row['delta'];
        $return[delta_in]  = $row['delta_in'];
        $return[delta_out] = $row['delta_out'];
        $return[timestamp] = $row['timestamp'];
        $return[state]     = 'ok';
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastMeasurement()


function get95thin($bill_id, $datefrom, $dateto)
{
    $mq_sql           = "SELECT count(delta) FROM bill_data WHERE bill_id = '".mres($bill_id)."'";
    $mq_sql          .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'";
    $measurements     = dbFetchCell($mq_sql);
    $measurement_95th = (round(($measurements / 100 * 95)) - 1);

    $q_95_sql  = "SELECT (in_delta / period * 8) AS rate FROM bill_data  WHERE bill_id = '".mres($bill_id)."'";
    $q_95_sql .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."' ORDER BY rate ASC";
    $a_95th    = dbFetchColumn($q_95_sql);
    $m_95th    = $a_95th[$measurement_95th];

// echo("Function get95thin billing.php ---> SQL --> " . $mq_sql . "\n");

// echo("Function get95thin billing.php ---> Measurement_95th --> " . $measurement_95th . "\n");

// echo("Function get95thin billing.php ---> Q_05_SQL --> " . $q_95_sql . "\n");

// echo("Function get95thin billing.php ---> m_95_th --> " . $m_95th . "\n");

    return (round($m_95th, 2));
}//end get95thin()


function get95thout($bill_id, $datefrom, $dateto)
{
    $mq_sql           = "SELECT count(delta) FROM bill_data WHERE bill_id = '".mres($bill_id)."'";
    $mq_sql          .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'";
    $measurements     = dbFetchCell($mq_sql);
    $measurement_95th = (round(($measurements / 100 * 95)) - 1);

    $q_95_sql  = "SELECT (out_delta / period * 8) AS rate FROM bill_data  WHERE bill_id = '".mres($bill_id)."'";
    $q_95_sql .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."' ORDER BY rate ASC";

    $a_95th = dbFetchColumn($q_95_sql);
    $m_95th = $a_95th[$measurement_95th];

    return (round($m_95th, 2));
}//end get95thout()


function getRates($bill_id, $datefrom, $dateto)
{
    $data             = array();
    $mq_text          = 'SELECT count(delta) FROM bill_data ';
    $mq_text         .= " WHERE bill_id = '".mres($bill_id)."'";
    $mq_text         .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'";

    //echo("MQ Text @ Function getRates billing.php --->" . $mq_text);

    $measurements     = dbFetchCell($mq_text);
    $measurement_95th = (round(($measurements / 100 * 95)) - 1);

   //echo("MQ Text @ Function getRates billing.php ---> Measurements " . $measurements . " Measurement 95th" . $measurement_95th . "\n");

    $q_95_sql  = "SELECT delta FROM bill_data  WHERE bill_id = '".mres($bill_id)."'";
    $q_95_sql .= " AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."' ORDER BY delta ASC";

    $a_95th = dbFetchColumn($q_95_sql);

    //echo("MQ Text @ Function getRates billing.php --->" . $q_95_sql ."\n");

    $m_95th = $a_95th[$measurement_95th];

  //echo("measurement_95th @ Function getRates billing.php --->" . $measurement_95th ."\n");

  //echo("m_95th Function getRates billing.php --->" . $m_95th ."\n");

    $sum_data = getSum($bill_id, $datefrom, $dateto);
    $mtot     = $sum_data['total'];
    $mtot_in  = $sum_data['inbound'];
    $mtot_out = $sum_data['outbound'];
    $ptot     = $sum_data['period'];

    $data['rate_95th_in']  = get95thIn($bill_id, $datefrom, $dateto);
    $data['rate_95th_out'] = get95thOut($bill_id, $datefrom, $dateto);

    if ($data['rate_95th_out'] > $data['rate_95th_in']) {
        $data['rate_95th'] = $data['rate_95th_out'];
        $data['dir_95th']  = 'out';
    } else {
        $data['rate_95th'] = $data['rate_95th_in'];
        $data['dir_95th']  = 'in';
    }

    $data['total_data']     = $mtot;
    $data['total_data_in']  = $mtot_in;
    $data['total_data_out'] = $mtot_out;
    $data['rate_average']   = ($mtot / $ptot * 8);
    $data['rate_average_in']   = ($mtot_in / $ptot * 8);
    $data['rate_average_out']  = ($mtot_out / $ptot * 8);

    // print_r($data);
    return ($data);
}//end getRates()


function getTotal($bill_id, $datefrom, $dateto)
{
    $mtot = dbFetchCell("SELECT SUM(delta) FROM bill_data WHERE bill_id = '".mres($bill_id)."' AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'");
    return ($mtot);
}//end getTotal()


function getSum($bill_id, $datefrom, $dateto)
{
    $sum = dbFetchRow("SELECT SUM(period) as period, SUM(delta) as total, SUM(in_delta) as inbound, SUM(out_delta) as outbound FROM bill_data WHERE bill_id = '".mres($bill_id)."' AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'");
    return ($sum);
}//end getSum()


function getPeriod($bill_id, $datefrom, $dateto)
{
    $ptot = dbFetchCell("SELECT SUM(period) FROM bill_data WHERE bill_id = '".mres($bill_id)."' AND timestamp > '".mres($datefrom)."' AND timestamp <= '".mres($dateto)."'");
    return ($ptot);
}//end getPeriod()

function getLastBillData($bill_id)
{
    $return = array();
    $row    = dbFetchRow("SELECT timestamp,period,delta,in_delta,out_delta FROM bill_data WHERE bill_id = ? ORDER BY timestamp DESC LIMIT 1", array($bill_id));
    if (!is_null($row)) {
        $return[delta]     = $row['delta'];
        $return[in_delta]  = $row['in_delta'];
        $return[out_delta] = $row['out_delta'];
        $return[timestamp] = $row['timestamp'];
        $return[period]=$row['period'];
        $return[state]     = 'ok';
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastBillData()

function getTodayHighest($bill_id)
{
    $return = array();

   $start_d = date("Y") ."/" . date("m") ."/". date("d") . " 00:00:00";
   $end_d = date("Y") . "/" . date("m") ."/". date("d").  " 23:59:59";

   $query_m = "SELECT bill_id,bill_cdr,bill_day,rate_95th,DAY(bill_day) as period_d,MONTH(bill_day) as period_m,YEAR(bill_day) as period_y,max(percentage) as percent  FROM bill_poller WHERE bill_id=? AND bill_day>='$start_d' and bill_day<='$end_d' LIMIT 1";

    //error_log($query_m,0);

    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {

        $percent=$row['percent'];

        if ($percent >=1000) {

          $percent=0;
        }
        $return[bill_id] = $row['bill_id'];
        $return[bill_cdr] = $row['bill_cdr'];
        $return[bill_day] = $row['bill_day'];
        $return[percent] = $percent;
        $return[period_d]= $row['period_d'];
        $return[period_m]= $row['period_m'];
        $return[period_y]= $row['period_y'];
        $return[state]     = 'ok';
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastBillData()

function getWeeklyHighest($bill_id)
{
    $return = array();

   $start_w = date("W");
   $start_y = date("Y");

   echo "Billing id " . $bill_id . " start_w " . $start_w . " start_y " . $start_y . "\n";
   
   $query_w ="SELECT bill_id,bill_cdr,bill_day,WEEK(bill_day,1) as period_w,MONTH(bill_day) as period_m,YEAR(bill_day) as period_y,max(percentage) as percent FROM bill_daily WHERE bill_id=? AND WEEK(bill_day,1)='$start_w' AND YEAR(bill_day)='$start_y'";
   
    //error_log($query_w,0);
    
    $row    = dbFetchRow($query_w, array($bill_id));
    if (!is_null($row)) {

        $percent=$row['percent'];

        if ($percent >=1000) {

          $percent=0;
        }
        $return[bill_id] = $row['bill_id'];
        $return[bill_cdr] = $row['bill_cdr'];
        $return[bill_day] = $row['bill_day'];
        $return[percent] = $percent;
        $return[period_w]= $row['period_w'];
        $return[period_m]= $row['period_m'];
        $return[period_y]= $row['period_y'];
        $return[state]     = 'ok';
        
        //print_r($return);
         
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastBillData()

function getThisMonthHighest($bill_id)
{
    $return = array();

   $start_m = date("m");
   $start_y = date("Y");

   $query_m = "SELECT bill_id,bill_cdr,bill_day,max(percentage) as percent FROM bill_daily WHERE bill_id=? AND period_m='$start_m' and period_y='$start_y' LIMIT 1";


    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {

        $percent=$row['percent'];

        if ($percent >=1000) {

            $percent=0;
        }
        $return[bill_id]  = $row['bill_id'];
        $return[bill_cdr] = $row['bill_cdr'];
        $return[bill_day] = $row['bill_day'];
        $return[percent] = $percent;
        $return[period_m]= date('n');
        $return[period_y]= date('Y');
        $return[state]     = 'ok';
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastBillData()

function getThisYearHighest($bill_id)
{
    $return = array();

   $start_y = date("Y");

   $query_y = "SELECT bill_id,bill_cdr,bill_day,max(percentage) as percent FROM bill_monthly WHERE bill_id=? AND period_y='$start_y' LIMIT 1";

    $row    = dbFetchRow($query_y, array($bill_id));
    if (!is_null($row)) {

        $percent=$row['percent'];

                if ($percent >500) {

                    $percent=0;
                }

        $return[bill_id]     = $row['bill_id'];
        $return[bill_cdr]  = $row['bill_cdr'];
        $return[bill_day] = $row['bill_day'];
        $return[percent] = $percent;
        $return[period_y]= date('Y');
        $return[state]     = 'ok';
    } else {
        $return[state] = 'failed';
    }
    return ($return);
}//end getLastBillData()

function billDailyIsExist($bill_id)
{
    $return = array();

    $period_d = date("j");
    $period_m = date("m");
    $period_y = date("Y");

    $query_m = "SELECT * FROM bill_daily WHERE bill_id=? AND period_d='$period_d' AND period_m='$period_m' and period_y='$period_y' LIMIT 1";

    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {
       $return[exist]     = 'true';
    } else {
        $return[exist] = 'false';
    }
    return ($return);
}//end getLastBillData()

function billMonthlyIsExist($bill_id)
{
    $return = array();

    $period_m = date("m");
    $period_y = date("Y");

    $query_m = "SELECT * FROM bill_monthly WHERE bill_id=? AND period_m='$period_m' and period_y='$period_y' LIMIT 1";

    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {
       $return[exist]     = 'true';
    } else {
        $return[exist] = 'false';
    }
    return ($return);
}//end getLastBillData()

function billYearIsExist($bill_id)
{
    $return = array();
    $period_y = date("Y");

    $query_m = "SELECT * FROM bill_yearly WHERE bill_id=? AND period_y='$period_y' LIMIT 1";

    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {
       $return[exist]     = 'true';
    } else {
        $return[exist] = 'false';
    }
    return ($return);
}//end getLastBillData()


function billWitelDailyIsExist($witel_id)
{
    $return = array();

    $period_d = date("j");
    $period_m = date("n");
    $period_y = date("Y");

    $query_w = "SELECT * FROM bill_witel_daily WHERE witel_id='" . $witel_id . "' AND period_d='$period_d' AND period_m='$period_m' and period_y='$period_y' LIMIT 1";


    $row    = dbFetchRow($query_w);
    if (!is_null($row)) {
       $return[exist]     = 'ok';
    } else {
        $return[exist] = 'error';
    }

    return ($return);
}//end getLastBillData()

function billWeeklyIsExist($bill_id)
{
    $return = array();
    $period_y = date("Y");
    $period_w=date("W");

    $query_m = "SELECT * FROM bill_weekly WHERE bill_id=? AND period_w='$period_w' AND period_y='$period_y' LIMIT 1";

    $row    = dbFetchRow($query_m, array($bill_id));
    if (!is_null($row)) {
       $return[exist]     = 'true';
    } else {
        $return[exist] = 'false';
    }
    return ($return);
}//end getLastBillData()

function CollectData($bill_id)
{
    $host=getHostnameFromBill($bill_id);

    $test = isPingable($host);

    if ($test['result'] === false) {

        echo " Unpingable Device $host Skipping ....\n\n";
        logfile(" Unpingable Device $host Skipping ....\n\n");

    }

    else {
            $port_list = dbFetchRows('SELECT * FROM `bill_ports` as P, `ports` as I, `devices` as D WHERE P.bill_id=? AND I.port_id = P.port_id AND D.device_id = I.device_id', array($bill_id));

            $now = dbFetchCell('SELECT NOW()');
            $delta = 0;
            $in_delta = 0;
            $out_delta = 0;
            foreach ($port_list as $port_data) {
                $port_id = $port_data['port_id'];
                $host    = $port_data['hostname'];
                $port    = $port_data['port'];

                $msg = date(DATE_RFC822) . " ...Port Id : $port_id    Polling ${port_data['ifName']} (${port_data['ifDescr']}) on ${port_data['hostname']}\n";

                echo $msg;

                logfile($msg);

               $port_data['in_measurement']  = getValue($port_data['hostname'], $port_data['port'], $port_data['ifIndex'], 'In');
               $port_data['out_measurement'] = getValue($port_data['hostname'], $port_data['port'], $port_data['ifIndex'], 'Out');

              // $port_data['in_measurement']  = getPollerValue($port_id,'In'); //In
              // $port_data['out_measurement'] = getPollerValue($port_id,'Out'); //Out

               $last_counters = getLastPortCounter($port_id, $bill_id);
                if ($last_counters['state'] == 'ok') {
                    $port_data['last_in_measurement']  = $last_counters[in_counter];
                    $port_data['last_in_delta']        = $last_counters[in_delta];
                    $port_data['last_out_measurement'] = $last_counters[out_counter];
                    $port_data['last_out_delta']       = $last_counters[out_delta];

                    $tmp_period = dbFetchCell("SELECT UNIX_TIMESTAMP(CURRENT_TIMESTAMP()) - UNIX_TIMESTAMP('".mres($last_counters['timestamp'])."')");

                    if ($port_data['ifSpeed'] > 0 && (delta_to_bits($port_data['in_measurement'], $tmp_period)-delta_to_bits($port_data['last_in_measurement'], $tmp_period)) > $port_data['ifSpeed']) {
                        $port_data['in_delta'] = $port_data['last_in_delta'];
                    } elseif ($port_data['in_measurement'] >= $port_data['last_in_measurement']) {
                        $port_data['in_delta'] = ($port_data['in_measurement'] - $port_data['last_in_measurement']);
                    } else {
                        $port_data['in_delta'] = $port_data['last_in_delta'];
                    }

                    if ($port_data['ifSpeed'] > 0 && (delta_to_bits($port_data['out_measurement'], $tmp_period)-delta_to_bits($port_data['last_out_measurement'], $tmp_period)) > $port_data['ifSpeed']) {
                        $port_data['out_delta'] = $port_data['last_out_delta'];
                    } elseif ($port_data['out_measurement'] >= $port_data['last_out_measurement']) {
                        $port_data['out_delta'] = ($port_data['out_measurement'] - $port_data['last_out_measurement']);
                    } else {
                        $port_data['out_delta'] = $port_data['last_out_delta'];
                    }
                } else {
                    $port_data['in_delta'] = '0';
                    $port_data['out_delta'] = '0';
                }

                $fields = array('timestamp' => $now, 'in_counter' => set_numeric($port_data['in_measurement']), 'out_counter' => set_numeric($port_data['out_measurement']), 'in_delta' => set_numeric($port_data['in_delta']), 'out_delta' => set_numeric($port_data['out_delta']));
                if (dbUpdate($fields, 'bill_port_counters', "`port_id`='" . mres($port_id) . "' AND `bill_id`='$bill_id'") == 0) {
                    $fields['bill_id'] = $bill_id;
                    $fields['port_id'] = $port_id;
                    dbInsert($fields, 'bill_port_counters');
                }

                $delta     = ($delta + $port_data['in_delta'] + $port_data['out_delta']);
                $in_delta  = ($in_delta + $port_data['in_delta']);
                $out_delta = ($out_delta + $port_data['out_delta']);
            }//end foreach

            $last_data = getLastMeasurement($bill_id);

            if ($last_data[state] == 'ok') {
                $prev_delta     = $last_data[delta];
                $prev_in_delta  = $last_data[in_delta];
                $prev_out_delta = $last_data[out_delta];
                $prev_timestamp = $last_data[timestamp];
                $period         = dbFetchCell("SELECT UNIX_TIMESTAMP(CURRENT_TIMESTAMP()) - UNIX_TIMESTAMP('".mres($prev_timestamp)."')");
            } else {
                $prev_delta     = '0';
                $period         = '0';
                $prev_in_delta  = '0';
                $prev_out_delta = '0';
            }

            if ($delta < '0') {
                $delta     = $prev_delta;
                $in_delta  = $prev_in_delta;
                $out_delta = $prev_out_delta;
            }

            if (!empty($period) && $period < '0') {
                logfile("BILLING: negative period! id:$bill_id period:$period delta:$delta in_delta:$in_delta out_delta:$out_delta");
            } else {
                dbInsert(array('bill_id' => $bill_id, 'timestamp' => $now, 'period' => $period, 'delta' => $delta, 'in_delta' => $in_delta, 'out_delta' => $out_delta), 'bill_data');
            }
    }

}//end CollectData()

function OccupancyPoller($bill_id) {

        $bill=dbFetchRow('SELECT * FROM `bills` WHERE `bill_id`=?', array($bill_id));

        $now = dbFetchCell('SELECT NOW()');

        //echo str_pad("------> " . $bill['bill_id'].' '.$bill['bill_name'], 30)." \n";

        unset($class);
        unset($rate_data);

        $host=getHostnameFromBill($bill_id);


        $testPing = isPingable($host);

        //if the site is down, set occupancy to zero

        if ($testPing['result'] === false) {



        //Set to Zero
            $bill_daily = array(
                'bill_id' => $bill['bill_id'],
                'bill_day'=>$now,
                'bill_cdr'=> $bill['bill_cdr'],
                'rate_95th_in'=> 0,
                'rate_95th_out'=> 0,
                'rate_95th'=> 0,
                'percentage'=> 0,
            );

            dbInsert($bill_daily,'bill_poller');
            //echo ' Bill Poller Updated! ';

            //echo "\n\n";


        //Update Table Bills
        $billparam = array('current_occupancy'=>0,'occupancy_last_calc'=>$now);
        $bill_update = dbUpdate($billparam,'bills','`bill_id`= ?',array($bill['bill_id']));

        //echo "Updating Table Bilss Bill_ID : " .$bill['bill_id'] ." ". $bill_update . " \n";

        //echo 'Billl ID ' . $bill['bill_id'] .  ' is currently down, occupancy set to Zero \n';

        logfile("Billl ID " . $bill['bill_id'] .   "is currently down, occupancy set to Zero \n");

            if ($bill_update>0) {

                echo $now ." Occupancy Poller Error : Unpingable Device $host current occupancy will be set to zero ....\n";

                logfile($now . " Occupancy Poller Error : Unpingable Device $host current occupancy will be set to zero ....\n");

            }

        }

        //if not down, continue to calculate occupancy
        if ($testPing['result'] === true) {


            $lastbilldata = getLastBillData($bill['bill_id']);

            if ($lastbilldata['state']=='ok') {

                $allowed      = $bill['bill_cdr'];
                $used         = $rate_data['rate_95th'];
                $allowed_text = format_si($allowed).'bps';
                $used_text    = format_si($used).'bps';
                $overuse      = ($used - $allowed);
                $overuse      = (($overuse <= 0) ? '0' : $overuse);


                //SetMax rate 95thInOut to 1Gb=1000000000, above this rate it must be invalid

                $tmp95thIn = $lastbilldata['in_delta']/$lastbilldata['period'] * 8;
                $tmp95thOut =$lastbilldata['out_delta']/$lastbilldata['period'] * 8;

                if ($tmp95thIn>1000000000) {

                    $tmp95thIn=0;
                }

                if ($tmp95thOut>1000000000) {

                    $tmp95thOut=0;
                }


                $rate_95th_in = $tmp95thIn;

                $rate_95th_out = $tmp95thOut;

                $rate_95th = $rate_95th_out;


                if ($rate_95th_in>$rate_95th_out) {

                    $rate_95th = $rate_95th_in;
                }

                //$percent      = round((($rate_95th / $bill['bill_cdr']) * 100), 2);

                $lastpoller = $lastbilldata['timestamp'];

                echo "Time Diff with last poller " . timeDiffInMinutes($lastpoller,$now) . "Minutes \n\n";

                //if Last Bill executed more than 15 minutes, set percentage to zero

                if (timeDiffInMinutes($lastpoller,$now)>60) {

                    $percent=0;
                }
                //Otherwise set to actual las bill data
                else{

                    $percent  = round((($rate_95th / $bill['bill_cdr']) * 100), 2);
                }


                echo " Percent " . $percent . "\n\n";


                //$percent      = round((($rate_95th / $bill['bill_cdr']) * 100), 2);

                    $bill_daily = array(
                        'bill_id' => $bill['bill_id'],
                        'bill_day'=>$now,
                        'bill_cdr'=> $bill['bill_cdr'],
                        'rate_95th_in'=> $rate_95th_in,
                        'rate_95th_out'=> $rate_95th_out,
                        'rate_95th'=> $rate_95th,
                        'percentage'=> $percent,
                    );

                    dbInsert($bill_daily,'bill_poller');


                //Update Table Bills
                $billparam = array('current_occupancy'=>$percent,'occupancy_last_calc'=>$now);
                $bill_update = dbUpdate($billparam,'bills','`bill_id` =?',array($bill['bill_id']));


                echo 'Updating Current Occupancy IP ' . $host . ' Billl ID ' . $bill['bill_id'] .  ' Percent ' . $percent;

                logfile("Updating Current Occupancy IP" . $host . " Billl ID " . $bill['bill_id'] .  " Percent " . $percent);


                if ($bill_update>0) {

                    //echo 'Occupancy on table bill udpdated !\n';
                }

                echo "\n\n";
            }
    }

}//end OccupancyPoller
