#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy Weekly ... \n\n";


foreach (dbFetchRows('SELECT * FROM `bills` ORDER BY `bill_id`') as $bill) {
        echo str_pad($bill['bill_id'].'  '.$bill['bill_name'], 30)." \n";
   
        unset($class);
        unset($rate_data);
      
        $thisweekmax = getWeeklyHighest($bill['bill_id']);
        
        //var_dump($thisweekmax);
        
        $thisweekexist = billWeeklyIsExist($bill['bill_id']);

        $now = dbFetchCell('SELECT NOW()');
        
        if ($thisweekmax['state']=='ok') {

            $bill_cdr = $thisweekmax['bill_cdr'];
            $bill_day = $thisweekmax['bill_day'];
            $percent  = $thisweekmax['percent']; 
            $period_w = $thisweekmax['period_w'];
            $period_y = $thisweekmax['period_y'];
      
            if ($thisweekexist['exist']=='false') {

                
             $bill_weekly = array(
                    'bill_id' => $bill['bill_id'],
                    'bill_day'=>$bill_day,
                    'bill_cdr'=> $bill_cdr,
                    'percentage'=> $percent,  
                    'period_w'=>$period_w,
                    'period_y'=>$period_y,
                    'last_updated'=>$now,
                );            
                
                dbInsert($bill_weekly,'bill_weekly');
           
                echo 'Bill Weekly Insert! \n';

                

            
            } //New Record
            
            else { //Update Record
             //   $fields = array('timestamp' => $now, 'in_counter' => set_numeric($port_data['in_measurement']), 'out_counter' => set_numeric($port_data['out_measurement']), 'in_delta' => set_numeric($port_data['in_delta']), 'out_delta' => set_numeric($port_data['out_delta']));               
                
                $now = dbFetchCell('SELECT NOW()');

                $fields = array('bill_day'=>$bill_day,'bill_cdr'=>$bill_cdr,'percentage'=>$percent,'last_updated'=>$now);
                
                dbUpdate($fields,'bill_weekly',"`bill_id`='" .$bill['bill_id'] ."' AND `period_w`='" . $period_w . "' AND `period_y`='" . $period_y . "'");
              //  dbUpdate(array('bill_day' => $bill_day,), 'bills', '`bill_id` = ?', array($bill_data['bill_id']));
              echo ' Updating Bill Weekly ! ';
            }

          
           //Update Table Bills
           $billparam = array('occupancy_week'=>$percent,'weekly_last_calc'=>$now);    
           $bill_update = dbUpdate($billparam,'bills','`bill_id` = ?',array($bill['bill_id']));
           
           echo 'Billl ID ' . $bill['bill_id'] .  ' Percent ' . $percent . '\n';

           if ($bill_update>0) {

               echo 'Occupancy on table bill udpdated !\n';
           }



            echo "\n\n";        
        }
  
}//end foreach
