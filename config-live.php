<?php

## Have a look in includes/defaults.inc.php for examples of settings you can set here. DO NOT EDIT defaults.inc.php!

### Database config
$config['db_host'] = 'localhost';
$config['db_user'] = 'nms';
$config['db_pass'] = 'monitoring';
$config['db_name'] = 'nms';


$config['page_title_suffix']='ATRON-Analytic Traffic Occupancy Network for Collaborative Network Optimizing Project';

$config['page_title']='Analytic Traffic Occupancy Network for Collaborative Network Optimizing Project';

$config['app_name']='ATRON';

$config['title_image'] = 'images/logo_telkom.svg';

$config['user'] = 'admapp';

### This should *only* be set if you want to *force* a particular hostname/port
### It will prevent the web interface being usable form any other hostname
$config['base_url']        = "/";

### Enable this to use rrdcached. Be sure rrd_dir is within the rrdcached dir
### and that your web server has permission to talk to rrdcached.
#$config['rrdcached']    = "unix:/var/run/rrdcached.sock";

### Default community
$config['snmp']['community'] = array("public");

### Authentication Model
$config['auth_mechanism'] = "mysql"; # default, other options: ldap, http-auth
#$config['http_auth_guest'] = "guest"; # remember to configure this user if you use http-auth

### List of RFC1918 networks to allow scanning-based discovery
#$config['nets'][] = "10.0.0.0/8";
#$config['nets'][] = "172.16.0.0/12";
#$config['nets'][] = "192.168.0.0/16";

$config['nets'][] = "172.26.15.0/24";
$config['autodiscovery']['nets-exclude'][] = '172.26.15.1/32';
$config['autodiscovery']['nets-exclude'][] = '172.26.15.2/32';
$config['autodiscovery']['nets-exclude'][] = '172.26.15.3/32';
$config['autodiscovery']['nets-exclude'][] = '172.26.15.4/32';


# Uncomment the next line to disable daily updates
#$config['update'] = 0;

#Logfile

$config['log_file']="/var/log/nms/nms.log";

# Number in days of how long to keep old rrd files. 0 disables this feature
$config['rrd_purge'] = 365;

# Uncomment to submit callback stats via proxy
#$config['callback_proxy'] = "hostname:port";

# Set default port association mode for new devices (default: ifIndex)
#$config['default_port_association_mode'] = 'ifIndex';

# Enable the in-built billing extension
$config['enable_billing'] = 1;

# Enable the in-built services support (Nagios plugins)
$config['show_services'] = 0;

#Show Location
$config['show_locations'] = 0;

$config['show_device_group'] = 0;

$config['manage_groups']=0;

#PollerGroup
$config['distributed_poller'] = true;
$config['distributed_poller_name']           = file_get_contents('/etc/hostname');
$config['distributed_poller_group']          = '0';
$config['distributed_poller_memcached_host'] = "10.62.170.154";
$config['distributed_poller_memcached_port'] = 11211;
$config['distributed_poller_host']           = "10.62.170.154";
$config['distributed_poller_port']           = 11211;
$config['rrdcached']                         = "10.62.170.154:42217";
$config['rrd_dir']                           = "/data/rrd";
$config['update']                            = 0;

#Poller Wrapper

$config['discovery_modules']['os']                        = 1;
$config['discovery_modules']['ports']                     = 1;
$config['discovery_modules']['ports-stack']               = 0;
$config['discovery_modules']['entity-physical']           = 0;
$config['discovery_modules']['processors']                = 0;
$config['discovery_modules']['mempools']                  = 0;
$config['discovery_modules']['cisco-vrf-lite']            = 0;
$config['discovery_modules']['ipv4-addresses']            = 1;
$config['discovery_modules']['ipv6-addresses']            = 0;
$config['discovery_modules']['route']                     = 0;
$config['discovery_modules']['sensors']                   = 0;
$config['discovery_modules']['storage']                   = 0;
$config['discovery_modules']['hr-device']                 = 0;
$config['discovery_modules']['discovery-protocols']       = 0;
$config['discovery_modules']['arp-table']                 = 0;
$config['discovery_modules']['discovery-arp']             = 0;
$config['discovery_modules']['junose-atm-vp']             = 0;
$config['discovery_modules']['bgp-peers']                 = 0;
$config['discovery_modules']['vlans']                     = 0;
$config['discovery_modules']['cisco-mac-accounting']      = 0;
$config['discovery_modules']['cisco-pw']                  = 0;
$config['discovery_modules']['cisco-vrf']                 = 0;
#$config['discovery_modules']['cisco-cef']                = 0;
$config['discovery_modules']['cisco-sla']                 = 0;
$config['discovery_modules']['vmware-vminfo']             = 0;
$config['discovery_modules']['libvirt-vminfo']            = 0;
$config['discovery_modules']['toner']                     = 0;
$config['discovery_modules']['ucd-diskio']                = 0;
$config['discovery_modules']['services']                  = 0;
$config['discovery_modules']['charge']                    = 0;


#Fping

$config['fping']            = "/usr/bin/fping";
$config['fping6']           = "fping6";
$config['fping_options']['retries'] = 3;
$config['fping_options']['timeout'] = 500;
$config['fping_options']['count'] = 3;
$config['fping_options']['millisec'] = 200;

#SNMP Settings
$config['snmp']['timeout'] = 5;            # timeout in seconds
$config['snmp']['retries'] = 3;            # how many times to retry the query
$config['snmp']['transports'] = array('udp', 'udp6', 'tcp', 'tcp6');
$config['snmp']['version'] = "v2c";         # Default version to use
$config['snmp']['port'] = 161;
