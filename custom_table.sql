use librenms;

CREATE VIEW v_listbilldaily AS

SELECT bd.bill_id,bd.bill_cdr, YEAR(bd.bill_day) as bd_year,MONTH(bd.bill_day) as bd_month,DAY(bd.bill_day) as bill_date,
	   AVG(bd.percentage) as percentage
             
FROM bill_daily bd
  GROUP BY DATE(bd.bill_day),bd.bill_id