#!/bin/bash

# Check if gedit is running
# -x flag only match processes whose name (or command line if -f is
# specified) exactly match the pattern. 

if pgrep -x "python2" > /dev/null
then
    echo "Running"
else
    echo "Executing Poller "
    python2 /opt/nms/poller-wrapper.py  100
fi
