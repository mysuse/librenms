
<?php
$pagetitle[] = 'Occupancy';

echo "<meta http-equiv='refresh' content='10000'>";

$bd_month = mres($vars['bd_month']);
$bd_year = mres($vars['bd_year']);


if (!empty($vars['bd_month'])) {

    $monthNum  = $vars['bd_month'];
    $dateObj   = DateTime::createFromFormat('!n', $monthNum);
    $bd_month = $dateObj->format('F'); // Month name
    
}

else {
    $bd_month =date("F");
    $monthNum  = date("n");
}

if (!empty($vars['bd_year'])) {

    $bd_year = mres($vars['bd_year']);
}

else {
    $bd_year =date("Y");
}

//error_log("Occupancy2 Page Year ==> " . $bd_year . "Month : " . $bd_month,0);

?>


<h2>Occupancy Overview</h2>
<h4>Month : <?php echo $bd_month; ?> </h4>
<h4>Year : <?php echo $bd_year; ?></h4>
<?php
//Toolbar Export to CSV
//print_optionbar_end();
print_optionbar_start();

echo('<div style="float: right;">');
?>

  <a href="csv.php/report=<?php echo generate_url($vars, array('format'=>'')); ?>" title="Export as CSV" target="_blank" rel="noopener">Export CSV</a> |
  <a href="<?php echo(generate_url($vars)); ?>" title="Update the browser URL to reflect the search criteria." >Update URL</a> |

<?php
if (isset($vars['searchbar']) && $vars['searchbar'] == "hide") {
    echo('<a href="'. generate_url($vars, array('searchbar' => '')).'">Search</a>');
} else {
    echo('<a href="'. generate_url($vars, array('searchbar' => 'hide')).'">Search</a>');
}

echo("  | ");

if (isset($vars['bare']) && $vars['bare'] == "yes") {
    echo('<a href="'. generate_url($vars, array('bare' => '')).'">Header</a>');
} else {
    echo('<a href="'. generate_url($vars, array('bare' => 'yes')).'">Header</a>');
}

echo('</div>');

print_optionbar_end();


//End of csv export
?>

<form method='post' action='' class='form-inline' role='form'>
    <div class="form-group">
<!-- Witel -->

<div class="form-group">
       
        <div class="col-sm-6">
            <select id="witel_id" name="witel_id" class='form-control input-sm'>

            <option value=''>All Witel</option>
                <?php
	                  $witels = dbFetchRows('SELECT * FROM `witel` ORDER BY witelname');
            
                         $unknown = 1;
        
                         foreach ($witels as $witel) {
                             echo('          <option value="'.$witel['witel_id'].'"');
                             if ($witel['witel_id'] == $vars['witel_id']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($witel['witelname']) . '</option>');
                         }
                         if ($unknown) {
                            echo('          <option value="other">All Witel</option>');
                            }
                ?>
            </select>
       </div>
    </div>

  
<!-- Site Name -->
<? // Month and Year Selection ?>

<div class="form-group">
       
        <div class="col-sm-6">
            <select id="bd_month" name="bd_month" class='form-control input-sm'>

            <option value=''>This Month</option>
                <?php
	                  $months = dbFetchRows('SELECT * FROM `month` ORDER BY month_id');
            
                         $unknown = 1;
        
                         foreach ($months as $month) {
                             echo('          <option value="'.$month['month_id'].'"');
                             if ($month['month_id'] == $vars['bd_month']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($month['month_name']) . '</option>');
                         }
                         if ($unknown) {
                            echo('          <option value="other">All Month</option>');
                            }
                ?>
            </select>
       </div>
    </div>

<div class="form-group">
       
        <div class="col-sm-6">
            <select id="bd_year" name="bd_year" class='form-control input-sm'>

            <option value=''>This Year</option>
                <?php
	                  $yrs = dbFetchRows('SELECT  DISTINCT(period_y) as bd_year  FROM bill_yearly');
            
                         $unknown = 1;
        
                         foreach ($yrs as $yr) {
                             echo('          <option value="'.$yr['bd_year'].'"');
                             if ($yr['bd_year'] == $vars['bd_year']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($yr['bd_year']) . '</option>');
                         }
                        
                ?>
            </select>
       </div>
    </div>

<? //End Month and year selection ?>



     <input type="text" name="site_name" id="site_name" title="Site Name" class="form-control input-sm" <?php if (strlen($vars['site_name'])) {
            echo('value="'.$vars['site_name'].'"');
} ?> placeholder="Site Name" />

 <input type="text" name="hostname" id="hostname" title="Hostname" class="form-control input-sm" <?php if (strlen($vars['hostname'])) {
            echo('value="'.$vars['hostname'].'"');
} ?> placeholder="Hostname" />
    </div>

   <button type="submit" class="btn btn-default btn-sm">Search</button>
      <a class="btn btn-default btn-sm" href="<?php echo(generate_url(array('page' => 'occupancy-monthly', 'section' => $vars['section'], 'bare' => $vars['bare']))); ?>" title="Reset critera to default." >Reset</a>
      </td>

    </form>

    <?php /*end of search form */ ?>

<div class="panel panel-default panel-condensed">
    <div class="table-responsive">
        <table class="bordered table-condensed table-hover"  id="occupancy-monthly">
        <thead>
            <th data-column-id="tselregion" data-sortable="true"><span class="th-inner">TSEL Region</span></th> 
            <th data-column-id="site_id" data-sortable="true"><span class="th-inner">Site ID</span></th> 
            <th data-column-id="site_name" data-searchable="true" data-sortable="true"><span class="th-inner">Site Name</span></th>                  
            <th data-column-id="year">Year</th>
            <th data-column-id="month">Month</th>            
            <th data-column-id="bandwidth">Bandwidth</th>      
    
            <?php
               
                $number = cal_days_in_month(CAL_GREGORIAN, $monthNum, $bd_year); // Number of Days
              
                for ($x = 1; $x <= $number; $x++) {
                 
                $id="d".$x;
                               
                //error_log("<th data-column-id='" . $id . "' data-sortable='false'>" . $x ."</th>",0);

                echo "<th data-column-id='" . $id . "' data-sortable='false'>" . $x ."</th>";

            } 
            
            ?>
            
            
            
        </thead>
        </table>
    </div>
</div>



<script type="text/javascript">

   var grid = $('#occupancy-monthly').bootgrid({
       ajax: true,
       templates: {
           header: $('#table-header').html()
       },
       columnSelection: false,
       rowCount: [50,100,250,-1],
     
      templates: {
        search: "" // hide the generic search
       },
       post: function() {
           return {
               id: 'occupancy-monthly',
               bill_id: '<?php echo mres($vars['bill_id']); ?>',
               bd_month:'<?php echo mres($vars['bd_month']); ?>',
                bd_year:'<?php echo mres($vars['bd_year']); ?>',
               hostname: '<?php echo htmlspecialchars($vars['hostname']); ?>',
               site_id: '<?php echo htmlspecialchars($vars['site_id']); ?>',
               site_name: '<?php echo htmlspecialchars($vars['site_name']); ?>',
               witel_id:'<?php echo mres($vars['witel_id']); ?>',
               startdate:'<?php echo mres($vars['startdate']); ?>',
               enddate:'<?php echo mres($vars['enddate']); ?>', 
               percent: '<?php echo mres($vars['percent']); ?>'
           };
       },
      url: "ajax_table.php"
    }).on("loaded.rs.jquery.bootgrid", function() {
    });
    $('#table-filters select').on('change', function() { grid.bootgrid('reload'); }); 
</script>

<?php

   // error_log("Data String " . $data_string,0);
?>
