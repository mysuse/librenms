
<?php
$pagetitle[] = 'Witel Overview';

echo "<meta http-equiv='refresh' content='10000'>";

$witel_id = mres($vars['witel_id']);

if (!empty($_POST['bd_year'])) {
      $bw_year=$_POST['bd_year'];
}
else { //Default Year
     $bw_year= date("Y");
}

?>

<?php
//Toolbar Export to CSV
//print_optionbar_end();
print_optionbar_start();


?>

   <a href="<?php echo(generate_url($vars)); ?>" title="Update the browser URL to reflect the search criteria." >Update URL</a> |

<?php
if (isset($vars['searchbar']) && $vars['searchbar'] == "hide") {
    echo('<a href="'. generate_url($vars, array('searchbar' => '')).'">Search</a>');
} else {
    echo('<a href="'. generate_url($vars, array('searchbar' => 'hide')).'">Search</a>');
}

echo("  | ");

if (isset($vars['bare']) && $vars['bare'] == "yes") {
    echo('<a href="'. generate_url($vars, array('bare' => '')).'">Header</a>');
} else {
    echo('<a href="'. generate_url($vars, array('bare' => 'yes')).'">Header</a>');
}

echo('</div>');

print_optionbar_end();


$witel_data = dbFetchRow('SELECT * FROM witel WHERE witel_id = ?', array($witel_id));

$witel_name =$witel_data['witelname'];


//End of csv export
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-6 col-lg-push-6">
    <div class='panel panel-primary panel-condensed'>
          <div class="panel-heading"> 
         <h3 class="panel-title">
          <i class="fa fa-id-card fa-lg icon-theme" aria-hidden="true"></i> <strong>Witel Overview</strong>
          </h3>
          </div>
            
          
          <?php

             $nodeb=dbFetchRow("SELECT COALESCE(count(device_id),0) as total from devices where witel_id='" . $witel_id . "'");

             $_25 = dbFetchRow('SELECT * FROM witel WHERE witel_id = ?', array($witel_id));

             $queryo="SELECT percent25,percent2550,percent5075,percent75   FROM bill_witel_daily
                                   WHERE witel_id='" . $witel_id ."' AND period_d=DAY(NOW()) and period_m=MONTH(NOW()) and period_y=YEAR(NOW())";
             
             //error_log("Query --> " . $queryo,0);
             
             $occupancy=dbFetchRow($queryo);

          ?>
       <table class="table table-hover table-condensed table-striped"><tr>
            <td>Witel </td>
            <td><strong><?php echo $witel_name; ?></strong></td>
          </tr><tr>
            <td>Occupancy 25% and below today</td>
            <td><span class="badge bg-white"><?php echo $occupancy['percent25']; ?> </span></td>
          </tr>
          </tr><tr>
            <td>Occupancy 25% - 50% today</td>
            <td><span class="badge bg-green"><?php echo $occupancy['percent2550']; ?> </span> </td>
          </tr>
          </tr><tr>
            <td>Occupancy 50% - 75% today</td>
            <td><span class="badge bg-yellow"><?php echo $occupancy['percent5075']; ?> </span></td>
          </tr>
          </tr><tr>
            <td>Occupancy 75% and above today</td>
            <td><span class="badge bg-red"><?php echo $occupancy['percent75']; ?> </span> </td>
          </tr>
          </tr><tr>
            <td>Total Node-B</td>
            <td><span class="badge bg-white"><?php echo $nodeb['total']; ?></span></td>
          </tr>
         
      </table>
      </div>
      
    </div>

    <div class="col-lg-6 col-lg-pull-6">
      <div class='panel panel-primary panel-condensed'>
          <div class="panel-heading">
              <h3 class="panel-title">
                 <i class="fa fa-id-card fa-lg icon-theme" aria-hidden="true"></i> <strong>Occupancy Overview</strong>
       
              </h3>
          </div>

                  <form method='post' action='' class='form-inline' role='form'>
                   

              <div class="form-group">
                    
                      <div class="col-sm-6">
                          <select id="bd_year" name="bd_year" class='form-control input-sm'>

                          <option value=''>This Year</option>
                              <?php
                                  $yrs = dbFetchRows('SELECT  DISTINCT(period_y) as bd_year  FROM bill_witel_daily');
                          
                                      $unknown = 1;
                      
                                      foreach ($yrs as $yr) {
                                          echo('          <option value="'.$yr['bd_year'].'"');
                                          if ($yr['bd_year'] == $vars['bd_year']) {
                                                echo(' selected="1"');
                                                  $unknown = 0;
                                                  }
                                              echo(' >' . ucfirst($yr['bd_year']) . '</option>');
                                      }
                                      
                              ?>
                          </select>
                    </div>
                  </div>

          <? //End Month and year selection ?>


              <button type="submit" class="btn btn-default btn-sm">Search</button>
                  <a class="btn btn-default btn-sm" href="<?php echo(generate_url(array('page' => 'occupancy-overview', 'witel_id' => $vars['witel_id']))); ?>" title="Reset critera to default." >Reset</a>
                  </td>

              </form>

            <?php 
             include('pages/witel/monthlygraph.inc.php');
            ?>

            <canvas id="myChart2" width="200" height="150"></canvas>
                              <script>
                  var ctx = document.getElementById("myChart2");
                  var myChart = new Chart(ctx, {
                      type: 'bar',
                      data: {
                          labels: <?php echo $labels_string; ?>,
                          datasets: [{
                              label: 'Occupancy below 25%',
                              data: <?php echo $data_25_string; ?>,
                              backgroundColor:"rgb(34,206,206)"
                          },
                          
                          {
                              label: 'Occupancy 25%-50%',
                              data: <?php echo $data_2550_string; ?>,
                              backgroundColor:"rgb(255, 206, 86)"
                          },
                          
                          {
                              label: 'Occupancy 50-75%',
                              data: <?php echo $data_5075_string; ?>,
                              backgroundColor: "rgb(255,61,103)"
                              
                          },
                          {
                              label: 'Occupancy above 75%',
                              data:<?php echo $data_75_string; ?>,
                              backgroundColor: "rgb(128,0,0)"
                          }
                          ]
                      },
                      options: {
                         title:{
                        display:true,
                        text:"Occupancy Overview Per Month Year : <?php echo date('Y'); ?>"
                    },
                        tooltips: {
                        mode: 'index',
                        intersect: false
                       },
                          scales: {
                            xAxes: [{
                                      stacked: true,
                                    }],
                            yAxes: [{
                                      stacked: true
                                  }]
                          }
                      }
                  });
                  </script>

      </div>
    </div>

  </div>
</div>