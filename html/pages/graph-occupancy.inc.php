<?php

echo "<meta http-equiv='refresh' content='10000'>";

$wheres = array();
$param = array();

$bill_id = mres($vars['occupancy_id']);


if (!empty($vars['year'])) {

    $bd_year = mres($vars['year']);
    $wheres[] = 'bill_type = ?';
    $param[] = $_POST['bill_type'];
}

else {
    $bd_year =date("Y");
    
}



if (!empty($vars['month'])) {

    $bd_month = $vars['month'];
    
}

else {

    $bd_month =date("n");
}


if (!empty($vars['day'])) {

    $bd_day = mres($vars['day']);
}

else {
    $bd_day =date("j");
}

$wheres = array();
$param = array();



$sql = 'SELECT bills.*,D.device_id,D.site_name,D.site_id,D.hostname,D.location,
        witel.witelname,ports.port_id,ports.ifdescr,ports.ifname,bills.bill_cdr AS bill_allowed
        FROM bills
        INNER JOIN bill_ports ON bills.bill_id=bill_ports.bill_id
        INNER JOIN  ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        LEFT JOIN witel ON D.witel_id=witel.witel_id WHERE bills.bill_id = ?';

$bill_data = dbFetchRow($sql, array($bill_id));

$url_device=generate_url(array('page'=>'device','device'=> $bill_data['device_id']));

$ports = dbFetchRows(
        'SELECT * FROM `bill_ports` AS B, `ports` AS P, `devices` AS D
        WHERE B.bill_id = ? AND P.port_id = B.port_id
        AND D.device_id = P.device_id',
        array($bill_id)
    );

function print_port_list()
    {
        global $ports;
        echo '<div class="panel panel-primary panel-condensed">
            <div class="panel-heading">
                <h3 class="panel-title">Occupancy On Ports</h3>
            </div>
            <div class="list-group">';

        // Collected Earlier
        foreach ($ports as $port) {
            $portalias = (empty($port['ifAlias']) ? '' : ' - '.display($port['ifAlias']).'');

            echo '<div class="list-group-item">';
            echo generate_port_link($port, $port['ifName'].$portalias).' on '.generate_device_link($port);
            
            echo '</div>';
        }

        echo '</div></div>';
    }//end print_port_list

print_optionbar_start();
  echo '<div style="font-weight: bold; float: right;"><a href="'.generate_url(array('page' => 'occupancy-monthly')).'"><i class="fa fa-arrow-left fa-lg icon-theme" aria-hidden="true"></i> Back to Occupancy by Month</a></div></br>';
print_optionbar_end();
//error_log("SQL Query --->" . $sql,0);
?>
<div class="row">
<div class="col-lg-6 col-lg-push-6">
<?php print_port_list(); ?>
</div>
<div class="col-lg-6 col-lg-pull-6">
    <div class='panel panel-primary panel-condensed'>
        <div class="panel-heading">
            <h3 class="panel-title">
               <?php echo "${bill_data['site_name']}"; ?>   
            </h3>
        </div>

            <table class="table table-hover table-condensed table-striped"><tr>
                <td>Witel</td>
                <td><?php echo "<strong>${bill_data['witel_name']}</strong>"; ?> </td>
            </tr><tr>
                <td>Node B</td>
                <td><?php echo "<strong><a href='$url_device'>${bill_data['hostname']}</a></strong></br>"; ?></td>
            </tr><tr>
                <td>Bandwidth</td>
                <td><?php  echo "<strong>" . format_si($bill_data['bill_allowed'],0,0) . "</strong></br>"; ?></td>
            </tr>
        </table>

        </div>
     </div>   
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Occupation View</h3>

    </div>

    <?php


        //error_log("Occupancy Page Year ==> " . $bd_year . "  Month : " . $bd_month . " Day : " . $bd_day . " bydate " . $bydate,0);


        $labels = array();
        $data   = array();


        $str_time=$bd_year ."/" . $bd_month . "/" . $bd_day;
 
        $date_obj = strtotime($str_time);

        $newdate=date('Y/m/d',$date_obj);

        //error_log("New Date " . $newdate,0);

        $start_date = $newdate . " 00:00:00";
        $end_date = $newdate . " 23:59:59";

        $query = "SELECT * FROM bill_poller WHERE bill_id=$bill_id AND bill_day>='$start_date' and bill_day<='$end_date'";
          
        //error_log("Query graph " . $query,0);


        foreach (dbFetchRows($query, $param) as $row)
        {
            $date=date_create($row["bill_day"]);
            
            $strip_date=date_format($date,"H");
        
            if($strip_date == $tmp_label) {
            
                $labels[] ="";
                
            }
            
            else {

        $labels[] = $strip_date;

        
            }
        
            $data[]   = $row["percentage"];
            
            $tmp_label=$strip_date;

        }  

            $data_d_string = "[" . join(", ", $data) . "]";
            $labels_d_string = "['" . join("', '", $labels) . "']";

        ?>

        <canvas id="todaychart" width="400" height="100"></canvas>

            <script>
                var ctx = document.getElementById("todaychart");
                var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: <?php echo($labels_d_string) ?>,
                    datasets: [{
                        label: 'Occupancy (%) ',
                        data: <?php echo $data_d_string; ?>,      
                        backgroundColor:'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)'
                    
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Occupancy (%)',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }],

                        xAxes: [{
                        
                            scaleLabel: {
                                display: true,
                                labelString: 'Hour',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }]
                    },

                    title: {
                        display: true,
                        text:  <?php echo "'Occupancy Daily " . $start_date . " - " .$end_date . "'"; ?>,
                        fontSize:18,
                        fontColor:'#666'
                    }
                }
            });
            </script>
</div>