<h4>Occupancy Information</h4>

<?php

$info = preg_replace('/\s+/', '',$device_info['site_id'] . '-' . $device_info['site_name'] .'-' . $device_info['bandwidth'] . "Mbps");


?>
<div class="form-group">
  <label for="bill_name" class="col-sm-4 control-label">Description</label>
  <div class="col-sm-8">
    <input class="form-control input-sm" type="text" id="bill_name" name="bill_name" value="
    <?php 
       
    if ($bill_data['bill_name'] != '') {

        echo $bill_data['bill_name']; 
      }
    else {
        echo $info;

     }
    
    ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-4 control-label" for="bill_type">Occupancy Type</label>
  <div class="col-sm-8">
    <label class="radio-inline">
      <input type="radio" name="bill_type" id="bill_type_cdr" value="cdr" <?php if ($bill_data['bill_type'] == 'cdr') {
            echo "checked";
} ?> onchange="javascript: billType();" /> 
    </label>
  
  </div>
</div>
<div class="form-group">
  <div id="cdrDiv">
    <label class="col-sm-4 control-label" for="bill_cdr">Bandwith Allowed</label>
    <div class="col-sm-3">
      <input class="form-control input-sm" type="text" name="bill_cdr" value="<?php 
      //echo $device_info['bandwidth'];

      if ($cdr['data'] ==0)
        { echo $device_info['bandwidth']; }
      else
         { echo $cdr['data'];  }
      
      ?>">
    </div>
    <div class="col-sm-5">
      <select name="bill_cdr_type" class="form-control input-sm">
        <option <?php echo $cdr['select_kbps'] ?> value="Kbps">Kilobits per second (Kbps)</option>
        <option <?php echo $cdr['select_mbps'] ?> value="Mbps">Megabits per second (Mbps)</option>
        <option <?php echo $cdr['select_gbps'] ?> value="Gbps">Gigabits per second (Gbps)</option>
      </select>
    </div>
  </div>
  <div id="quotaDiv">
    <label class="col-sm-4 control-label" for="bill_quota">Quota</label>
    <div class="col-sm-3">
      <input class="form-control input-sm" type="text" name="bill_quota" value="<?php echo $quota['data'] ?>">
    </div>
    <div class="col-sm-5">
      <select name="bill_quota_type" class="form-control input-sm">
        <option <?php echo $quota['select_mb'] ?> value="MB">Megabytes (MB)</option>
        <option <?php echo $quota['select_gb'] ?> value="GB">Gigabytes (GB)</option>
        <option <?php echo $quota['select_tb'] ?> value="TB">Terabytes (TB)</option>
      </select>
    </div>
  </div>
</div>

<fieldset>
  

<script type="text/javascript">
function billType() {
    var selected = $('input[name=bill_type]:checked').val();
    
    $('#cdrDiv').toggle(selected === 'cdr');
    $('#quotaDiv').toggle(selected === 'quota');
}
billType();
</script>
