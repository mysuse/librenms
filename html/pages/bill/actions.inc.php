<?php

$port_id=$bill_id['port_id'];

$device = dbFetchRow("SELECT D.* FROM devices D JOIN ports P ON D.device_id=P.device_id WHERE P.port_id=$port_id");

if ($_POST['action'] == 'delete_bill' && $_POST['confirm'] == 'confirm') {
    foreach (dbFetchRows('SELECT * FROM `bill_ports` WHERE `bill_id` = ?', array($bill_id)) as $port_data) {
        dbDelete('port_in_measurements', '`port_id` = ?', array($port_data['bill_id']));
        dbDelete('port_out_measurements', '`port_id` = ?', array($port_data['bill_id']));
    }

    dbDelete('bill_history', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_ports', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_data', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_perms', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_daily', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_port_counters', '`bill_id` = ?', array($bill_id));
    dbDelete('bills', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_monthly', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_yearly', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_witel_daily', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_weekly', '`bill_id` = ?', array($bill_id));
    dbDelete('bill_poller', '`bill_id` = ?', array($bill_id));
    
    log_event("Occupancy measurement REMOVED " , $device,'system',3);
    
    echo '<div class=infobox>Bill Deleted. Redirecting to Bills list.</div>';

    echo "<meta http-equiv='Refresh' content=\"2; url='occupancies/'\">";
}

if ($_POST['action'] == 'reset_bill' && ($_POST['confirm'] == 'rrd' || $_POST['confirm'] == 'mysql')) {
    if ($_POST['confirm'] == 'mysql') {
        foreach (dbFetchRows('SELECT * FROM `bill_ports` WHERE `bill_id` = ?', array($bill_id)) as $port_data) {
            dbDelete('port_in_measurements', '`port_id` = ?', array($port_data['bill_id']));
            dbDelete('port_out_measurements', '`port_id` = ?', array($port_data['bill_id']));
        }

        dbDelete('bill_history', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_data', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_daily', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_port_counters', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_monthly', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_yearly', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_weekly', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_witel_daily', '`bill_id` = ?', array($bill_id));
        dbDelete('bill_poller', '`bill_id` = ?', array($bill_id));
        

        log_event("Occupancy measurement RESET " , $device,'system',3);

        //Reset Bill set value to Zero
        dbUpdate(
        array(
            'rate_95th_in'   => 0,
            'rate_95th_out'    => 0,
            'rate_95th'  => 0,
            'total_data'    => 0,
            'total_data_in'   => 0,
            'total_data_out' => 0,
            'rate_average_in'    => 0,
            'rate_average_out'  => 0,
            'rate_average'  => 0,
            'current_occupancy'=>0,
            'occupancy_day'=>0,
            'occupancy_week'=>0,
            'occupancy_month'=>0,
            'occupancy_year'=>0,
        ),
        'bills',
        '`bill_id` = ?',
        array($bill_id)
    );
    }

    if ($_POST['confirm'] == 'rrd') {
        // Stil todo
    }

    echo '<div class=infobox>Bill Reseting. Redirecting to Bills list.</div>';

    echo "<meta http-equiv='Refresh' content=\"2; url='occupancies/'\">";
}
//get device information

   $billinfo = dbFetchRow("SELECT * FROM bills WHERE bill_id=$bill_id");


if ($_POST['action'] == 'add_bill_port') {
    dbInsert(array('bill_id' => $_POST['bill_id'], 'port_id' => $_POST['port_id']), 'bill_ports');
    
    log_event("Occupancy measurement ADDED for ONT  " , $device,'system',3);
}   

if ($_POST['action'] == 'delete_bill_port') {
    dbDelete('bill_ports', '`bill_id` =  ? AND `port_id` = ?', array($bill_id, $_POST['port_id']));
    
    log_event("Occupancy measurement REMOVED for ONT  " , $device,'system',3);
}

if ($_POST['action'] == 'update_bill') {

    $bandwidth=$_POST['bill_cdr'];
   
    if (isset($_POST['bill_quota']) or isset($_POST['bill_cdr'])) {
        if ($_POST['bill_type'] == 'quota') {
            if (isset($_POST['bill_quota_type'])) {
                if ($_POST['bill_quota_type'] == 'MB') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'GB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'TB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                $bill_quota = (is_numeric($_POST['bill_quota']) ? $_POST['bill_quota'] * $config['billing']['base'] * $multiplier : 0);
                $bill_cdr   = 0;
            }
        }

        if ($_POST['bill_type'] == 'cdr') {
            if (isset($_POST['bill_cdr_type'])) {
                if ($_POST['bill_cdr_type'] == 'Kbps') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Mbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Gbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                

                $bill_cdr   = (is_numeric($_POST['bill_cdr']) ? $_POST['bill_cdr'] * $multiplier : 0);
                $bill_quota = 0;
            }
        }
    }//end if

    
    if (dbUpdate(
        array(
            'bill_name'   => $_POST['bill_name'],
            'bill_day'    => $_POST['bill_day'],
            'bill_quota'  => $bill_quota,
            'bill_cdr'    => $bill_cdr,
            'bill_type'   => $_POST['bill_type'],
            'bill_custid' => $_POST['bill_custid'],
            'bill_ref'    => $_POST['bill_ref'],
            'bill_notes'  => $_POST['bill_notes'],
        ),
        'bills',
        '`bill_id` = ?',
        array($bill_id)
    )) {
    
        //Update Bandwidth di Device

        $hostname=getHostnameFromBill($bill_id);
        $devparam['bandwidth']=$bandwidth;  
       
        $update_bw = dbUpdate($devparam,'devices','`hostname` = ?',array($hostname));

        if ($update_bw>0) {
            
            print_message('Bandwidth on Node-B Properties Updated');
            log_event("Bandwidth Changed from " . format_si($billinfo['bill_cdr']).'bps'.  " to " . format_si($bill_cdr) . 'bps' . " for ONT  " . $ip , $device,'system',3);
            
        }
        log_event("Occupancy Changed from " . format_si($billinfo['bill_cdr']).'bps'.  " to " . format_si($bill_cdr) . 'bps' . " for ONT  " . $ip , $device,'system',3);
        
        print_message('Bill Properties Updated');

    }
}//end if
