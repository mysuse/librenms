<?php
$pagetitle[] = 'Grafik';

$labels = array();
$data   = array();

$data2   = array();

$query = "SELECT * FROM bill_daily WHERE bill_id=12";

foreach (dbFetchRows($query, $param) as $row)
{
    $labels[] = $row["bill_day"];
    $data[]   = $row["percentage"];

    //error_log("Percentage   " . $row["percentage"],0);
}  

$query2 = "SELECT * FROM bill_daily WHERE bill_id=13";

foreach (dbFetchRows($query2, $param) as $row)
{
    //$labels[] = $row["bill_day"];
    $data2[]   = $row["percentage"];

    //error_log("Percentage   " . $row["percentage"],0);
}   
      $data_string = "[[" . join(", ", $data) . "],[" . join(", ", $data2) . "]]";
      $labels_string = "['" . join("', '", $labels) . "']";

     // error_log("Data   " . $data_string,0);
     // error_log("Label" . $labels_string,0);

?>

<canvas id="cvs" width="900" height="300">[No canvas support]</canvas>

<script>
   
     var line = new RGraph.Line({
        id: 'cvs',
        data: <?php print($data_string) ?>,
        
        options: {
              title:'Test Title',
              titleXaxis:'Date',
              titleYaxis:'Occupancy (%)',
                gutterLeft: 35,
                gutterRight: 5,
                hmargin: 10,
                tickmarks: 'endcircle',
                labels: <?php print($labels_string) ?>
        }
    }).draw();
</script>