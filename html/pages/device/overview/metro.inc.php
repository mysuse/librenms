<?php

$metro = dbFetchRows('SELECT * FROM `devices` WHERE device_id = ?', array($device['device_id']));

if (count($metro)) {
    echo '<div class="container-fluid ">
      <div class="row">
        <div class="col-md-12 ">
          <div class="panel panel-primary panel-condensed">
            <div class="panel-heading">
';
    
    echo '<i class="fa fa-microchip fa-lg icon-theme" aria-hidden="true"></i> <strong>Node-B Data Teknik</strong></a>';
    echo '</div>
        <table class="table table-hover table-condensed table-striped">';

   //Custom table

echo '<tr>
        <td>Metro Name</td>
        <td>'.$device['metro_name'].' </td>
      </tr>';

echo '<tr>
        <td>Metro Port</td>
        <td>'.$device['metro_port'].' </td>
      </tr>';

echo '<tr>
        <td>Metro IP</td>
        <td>'.$device['metro_ip'].' </td>
      </tr>';

echo '<tr>
        <td>Bandwidth</td>
        <td>'.format_si($device['bandwidth'],0,0).' </td>
      </tr>';


echo '<tr>
        <td>OLT Name</td>
        <td>'.$device['olt_name'].' </td>
      </tr>';

echo '<tr>
        <td>OLT Port</td>
        <td>'.$device['olt_port'].' </td>
      </tr>';

echo '<tr>
        <td>OLT IP</td>
        <td>'.$device['olt_ip'].' </td>
      </tr>';

echo '<tr>
        <td>ONT SN</td>
        <td>'.$device['ont_sn'].' </td>
      </tr>';

echo '<tr>
        <td>ONT IP</td>
        <td>'.$device['ont_ip'].' </td>
      </tr>';

    echo '</table>
        </div>
        </div>
        </div>
        </div>';
}//end if
