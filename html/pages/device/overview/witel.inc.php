<?php

$metro = dbFetchRows('SELECT * FROM `devices` WHERE device_id = ?', array($device['device_id']));


if (count($metro)) {
    echo '<div class="container-fluid ">
      <div class="row">
        <div class="col-md-12 ">
          <div class="panel panel-green panel-condensed">
            <div class="panel-heading">
';
    
    echo '<i class="fa fa-microchip fa-lg icon-theme" aria-hidden="true"></i> <strong>Location Data</strong></a>';
    echo '</div>
        <table class="table table-hover table-condensed table-striped">';

   //Custom table

$witel_data = dbFetchRow('SELECT * FROM witel WHERE witel_id = ?', array($device['witel_id']));

echo '<tr>
        <td>Witel</td>
        <td>'.$witel_data['witelname'].' </td>
      </tr>';

$tsel_region =dbFetchRow('SELECT * FROM tselregion WHERE tselregion_id = ?', array($device['tselregion_id']));

echo '<tr>
        <td>Regional TSEL</td>
        <td>'.$tsel_region['tselregion_name'].' </td>
      </tr>';

echo '<tr>
        <td>Alamat</td>
        <td>'.$device['address'].' </td>
      </tr>';
//GetLocation from Device Info

echo '<tr>
        <td>Lat / Lng</td>
        <td>['.$device['latitude'].','.$device['longitude'].'] <div class="pull-right"><a href="https://maps.google.com/?q='.$device['latitude'].','.$device['longitude'].'" target="_blank" class="btn btn-success btn-xs" role="button"><i class="fa fa-map-marker" style="color:white" aria-hidden="true"></i> Map</button></div></a></td>
    </tr>';

echo '<tr>
        <td>Tahun Deploy</td>
        <td>'.$device['tahundeploy'].' </td>
      </tr>';

echo '<tr>
        <td>Layanan Data</td>
        <td>'.$device['dataservices'].' </td>
      </tr>';

echo '<tr>
        <td>STO</td>
        <td>'.$device['sto'].' </td>
      </tr>';


    echo '</table>
        </div>
        </div>
        </div>
        </div>';
}//end if