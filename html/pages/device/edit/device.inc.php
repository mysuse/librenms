<?php
if ($_POST['editing']) {
    if ($_SESSION['userlevel'] > "7") {
        $updated = 0;

        $override_sysLocation_bool = mres($_POST['override_sysLocation']);
        if (isset($_POST['sysLocation'])) {
            $override_sysLocation_string = $_POST['sysLocation'];
        }

        if ($device['override_sysLocation'] != $override_sysLocation_bool || $device['location'] != $override_sysLocation_string) {
            $updated = 1;
        }

        if ($override_sysLocation_bool) {
            $override_sysLocation = 1;
        } else {
            $override_sysLocation = 0;
        }

        dbUpdate(array('override_sysLocation'=>$override_sysLocation), 'devices', '`device_id`=?', array($device['device_id']));

        if (isset($override_sysLocation_string)) {
            dbUpdate(array('location'=>$override_sysLocation_string), 'devices', '`device_id`=?', array($device['device_id']));
        }

        if ($device['type'] != $vars['type']) {
            $param['type'] = $vars['type'];
            $update_type = true;
        }

        #FIXME needs more sanity checking! and better feedback

        $param['purpose']  = $vars['descr'];
        $param['ignore']   = set_numeric($vars['ignore']);
        $param['disabled'] = set_numeric($vars['disabled']);

        #Custom Fields

        $param['site_name']=$vars['site_name'];

        $param['sto']=$vars['sto'];

        $param['site_id']=$vars['site_id'];

        $param['vlan2g']=$vars['vlan2g'];

        $param['vlan3g']=$vars['vlan3g'];

        $param['bandwidth']=$vars['bandwidth'];

        $param['metro_name']=$vars['metro_name'];

        $param['metro_port']=$vars['metro_port'];

        $param['metro_ip']=$vars['metro_ip'];

        $param['olt_name']=$vars['olt_name'];

        $param['olt_ip']=$vars['olt_ip'];

        $param['olt_port']=$vars['olt_port'];

        $param['ont_ip']=$vars['ont_ip'];
        $param['ont_sn']=$vars['ont_sn'];

        $param['longitude']=$vars['longitude'];

        $param['latitude']=$vars['latitude'];

        $param['address']=$vars['address'];

        $param['witel_id']=$vars['witel_id'];
		
        $param['tselregion_id']=$vars['tselregion_id'];

        $param['bandwidth']=$vars['bandwidth'];


        $rows_updated = dbUpdate($param, 'devices', '`device_id` = ?', array($device['device_id']));

        if ($rows_updated > 0 || $updated) {

            //Update Bandwidth on Occupancy Measurement
            $device = dbFetchRow("SELECT * FROM `devices` WHERE `device_id` = ?", array($device['device_id']));

            $billid=getBillfromHostname($device['hostname']);

            $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
 
            $bill_cdr = $multiplier * $device['bandwidth'];
    
            $billinfo = dbFetchRow("SELECT * FROM bills WHERE bill_id=$billid");
            
            $billparam['bill_cdr']=$bill_cdr;

            $bill_update = dbUpdate($billparam,'bills','`bill_id` = ?',array($billid));

            if ($bill_update>0) {
               
               log_event("Occupancy Changed from " . format_si($billinfo['bill_cdr']).'bps'.  " to " . format_si($bill_cdr) . 'bps' . " for ONT  " . $ip , $device,'system',3);
                
               print_message('Occupancy Properties Updated' . format_si($billinfo['bill_cdr']).'bps'.  " to " . format_si($bill_cdr) . 'bps' . " for ONT  " . $ip);

            }

            
            if ($update_type === true) {
                set_dev_attrib($device, 'override_device_type', true);

                
            }
            $update_message = "Device record updated.";
            $updated = 1;
         
        } elseif ($rows_updated == 0) {
            $update_message = "Device record unchanged. No update necessary.";
            $updated = -1;
        } else {
            $update_message = "Device record update error.";
        }
        if (isset($_POST['hostname']) && $_POST['hostname'] !== '' && $_POST['hostname'] !== $device['hostname']) {
            if (is_admin()) {
                $result = renamehost($device['device_id'], $_POST['hostname'], 'webui');
                if ($result == "") {
                    print_message("Hostname updated from {$device['hostname']} to {$_POST['hostname']}");
                    echo '
                        <script>
                            var loc = window.location;
                            window.location.replace(loc.protocol + "//" + loc.host + loc.pathname + loc.search);
                        </script>
                    ';
                } else {
                    print_error($result . ".  Does your web server have permission to modify the rrd files?");
                }
            } else {
                print_error('Only administrative users may update the device hostname');
            }
        }
    } else {
        include 'includes/error-no-perm.inc.php';
    }
}

$descr  = $device['purpose'];
$override_sysLocation = $device['override_sysLocation'];
$override_sysLocation_string = $device['location'];

$sto_string = $device['sto'];
$site_name_string = $device['site_name'];
$site_id_string = $device['site_id'];
$vlan2g_string =$device['vlan2g'];
$vlan3g_string =$device['vlan3g'];

$metro_name_string=$device['metro_name'];
$metro_port_string=$device['metro_port'];
$metro_ip_string=$device['metro_ip'];
$olt_name_string=$device['olt_name'];
$olt_ip_string=$device['olt_ip'];
$olt_port_string=$device['olt_port'];
$ont_ip_string=$device['ont_ip'];
$ont_sn_string=$device['ont_sn'];
$latitude_string=$device['latitude'];
$longitude_string=$device['longitude'];
$address_string=$device['address'];


$bandwidth_string=$device['bandwidth'];

if ($updated && $update_message) {
    print_message($update_message);
} elseif ($update_message) {
    print_error($update_message);
}

?>
<h3> Device Settings </h3>
<div class="row">
    <div class="col-md-1 col-md-offset-2">
        <form id="delete_host" name="delete_host" method="post" action="delhost/" role="form">
            <input type="hidden" name="id" value="<?php echo($device['device_id']); ?>">
            <button type="submit" class="btn btn-danger" name="Submit"><i class="fa fa-trash"></i> Delete device</button>
        </form>
    </div>
    <div class="col-md-1 col-md-offset-2">
        <?php
        if ($config['enable_clear_discovery'] == 1) {
        ?>
            <button type="submit" id="rediscover" data-device_id="<?php echo($device['device_id']); ?>" class="btn btn-primary" name="rediscover"><i class="fa fa-retweet"></i> Rediscover device</button>
        <?php
        }
        ?>
    </div>
</div>
<br>
<form id="edit" name="edit" method="post" action="" role="form" class="form-horizontal">
<input type=hidden name="editing" value="yes">
    <div class="form-group" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Change the hostname used for name resolution" >
        <label for="edit-hostname-input" class="col-sm-2 control-label" >Hostname:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-hostname-input" name="hostname" class="form-control" disabled value=<?php echo(display($device['hostname'])); ?> />
        </div>
        <div class="col-sm-2">
            <button name="hostname-edit-button" id="hostname-edit-button" class="btn btn-danger"> <i class="fa fa-pencil"></i> </button>
        </div>
    </div>
     <div class="form-group">
        <label for="descr" class="col-sm-2 control-label">Description:</label>
        <div class="col-sm-6">
            <textarea id="descr" name="descr" class="form-control"><?php echo(display($device['purpose'])); ?></textarea>
        </div>
    </div>

    <!--Custom Field -->

    <h4> Site Info </h4>
    <div class="form-group">
        <label for="site_id" class="col-sm-2 control-label">Site ID:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-site_id" name="site_id" class="form-control" value=<?php echo(display($site_id_string)); ?> />
        </div>
    </div>
    
    <div class="form-group">
        <label for="site_name" class="col-sm-2 control-label">Site Name:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-site_name" name="site_name" class="form-control" value=<?php echo(display($site_name_string)); ?> />
        </div>
    </div>



<div class="form-group">
        <label for="witel_id" class="col-sm-2 control-label">Witel:</label>
        <div class="col-sm-6">
            <select id="witel_id" name="witel_id" class="form-control">
                <?php
	  $witels = dbFetchRows('SELECT * FROM `witel` ORDER BY witelname');
            
             $unknown = 1;
        
            foreach ($witels as $witel) {
                    echo('          <option value="'.$witel['witel_id'].'"');
                    if ($device['witel_id'] == $witel['witel_id']) {
                        echo(' selected="1"');
                        $unknown = 0;
                    }
                    echo(' >' . ucfirst($witel['witelname']) . '</option>');
                }
                if ($unknown) {
                    echo('          <option value="other">Other</option>');
                }
        ?>
            </select>
       </div>
    </div>


<div class="form-group">
        <label for="tselregion_id" class="col-sm-2 control-label">Telkomsel Regional:</label>
        <div class="col-sm-6">
            <select id="tselregion_id" name="tselregion_id" class="form-control">
                <?php
	  $tsels = dbFetchRows('SELECT * FROM `tselregion` ORDER BY tselregion_name');
            
             $unknown = 1;
        
            foreach ($tsels as $tsel) {
                    echo('          <option value="'.$tsel['tselregion_id'].'"');
                    if ($device['tselregion_id'] == $tsel['tselregion_id']) {
                        echo(' selected="1"');
                        $unknown = 0;
                    }
                    echo(' >' . ucfirst($tsel['tselregion_name']) . '</option>');
                }
                if ($unknown) {
                    echo('          <option value="other">Other</option>');
                }
        ?>
            </select>
       </div>
    </div>

    <div class="form-group">
        <label for="sto" class="col-sm-2 control-label">STO:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-sto" name="sto" class="form-control" value=<?php echo $sto_string; ?> />
        </div>
    </div>

  <div class="form-group">
        <label for="adress" class="col-sm-2 control-label">Alamat:</label>
        <div class="col-sm-6">
            <textarea id="address" name="address" class="form-control"><?php echo(display($address_string)); ?></textarea>
        </div>
    </div>

   <div class="form-group">
        <label for="latitude" class="col-sm-2 control-label">Latitude:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-latitude" name="latitude" class="form-control" value=<?php echo $latitude_string; ?> />
        </div>
    </div>

    <div class="form-group">
        <label for="longitude" class="col-sm-2 control-label">Longitude:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-longitude" name="longitude" class="form-control" value=<?php echo $longitude_string; ?> />
        </div>
    </div>

     <h4> Metro Info </h4>
     <div class="form-group">
        <label for="metro_name" class="col-sm-2 control-label">Metro Name:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-metro_name" name="metro_name" class="form-control" value=<?php echo(display($metro_name_string)); ?> />
        </div>
    </div>

    <div class="form-group">
        <label for="metro_port" class="col-sm-2 control-label">Metro Port:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-metro_port" name="metro_port" class="form-control" value=<?php echo(display($metro_port_string)); ?> />
        </div>
    </div>

  <div class="form-group">
        <label for="metro_ip" class="col-sm-2 control-label">Metro IP:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-metro_ip" name="metro_ip" class="form-control" value=<?php echo(display($metro_ip_string)); ?> />
        </div>
    </div>

<div class="form-group">
        <label for="bandwidth" class="col-sm-2 control-label">Bandwidth (Mbps) :</label>
        <div class="col-sm-6">
            <input type="text" id="bandwidth" name="bandwidth" class="form-control" value=<?php echo(display($bandwidth_string)); ?> />
        </div>
    </div>

   <h4> OLT Info </h4>

 <div class="form-group">
        <label for="olt_name" class="col-sm-2 control-label">Olt Name:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-olt_name" name="olt_name" class="form-control" value=<?php echo(display($olt_name_string)); ?> />
        </div>
    </div>

    <div class="form-group">
        <label for="olt_port" class="col-sm-2 control-label">Olt Port:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-olt_port" name="olt_port" class="form-control" value=<?php echo(display($olt_port_string)); ?> />
        </div>
    </div>

  <div class="form-group">
        <label for="olt_ip" class="col-sm-2 control-label">Olt IP:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-olt_ip" name="olt_ip" class="form-control" value=<?php echo(display($olt_ip_string)); ?> />
        </div>
    </div>

 <h4> ONT Info </h4>

 <div class="form-group">
        <label for="ont_ip" class="col-sm-2 control-label">ONT IP:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-ont_ip" name="ont_ip" class="form-control" value=<?php echo(display($ont_ip_string)); ?> />
        </div>
    </div>

  <div class="form-group">
        <label for="olt_sn" class="col-sm-2 control-label">ONT SN:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-ont_sn" name="ont_sn" class="form-control" value=<?php echo(display($ont_sn_string)); ?> />
        </div>
    </div>

    

    <div class="form-group">
        <label for="vlan2g" class="col-sm-2 control-label">Vlan 2G:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-vlan2g" name="vlan2g" class="form-control" value=<?php echo $vlan2g_string; ?> />
        </div>
    </div>

    <div class="form-group">
        <label for="vlan3g" class="col-sm-2 control-label">Vlan 3G:</label>
        <div class="col-sm-6">
            <input type="text" id="edit-vlan3g" name="vlan3g" class="form-control" value=<?php echo $vlan3g_string; ?> />
        </div>
    </div>
    <!--End of Custom Field -->

    <div class="form-group">
        <label for="type" class="col-sm-2 control-label">Type:</label>
        <div class="col-sm-6">
            <select id="type" name="type" class="form-control">
                <?php
                $unknown = 1;

                foreach ($config['device_types'] as $type) {
                    echo('          <option value="'.$type['type'].'"');
                    if ($device['type'] == $type['type']) {
                        echo(' selected="1"');
                        $unknown = 0;
                    }
                    echo(' >' . ucfirst($type['type']) . '</option>');
                }
                if ($unknown) {
                    echo('          <option value="other">Other</option>');
                }
                ?>
            </select>
       </div>
    </div>

<div class="form-group">
    <label for="sysLocation" class="col-sm-2 control-label">Override sysLocation:</label>
    <div class="col-sm-6">
      <input onclick="edit.sysLocation.disabled=!edit.override_sysLocation.checked" type="checkbox" name="override_sysLocation"<?php if ($override_sysLocation) {
            echo(' checked="1"');
} ?> />
    </div>
</div>
<div class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-6">
      <input id="sysLocation" name="sysLocation" class="form-control" <?php if (!$override_sysLocation) {
            echo(' disabled="1"');
} ?> value="<?php echo($override_sysLocation_string); ?>" />
    </div>
</div>
<div class="form-group">
    <label for="disabled" class="col-sm-2 control-label">Disable:</label>
    <div class="col-sm-6">
      <input name="disabled" type="checkbox" id="disabled" value="1" <?php if ($device["disabled"]) {
            echo("checked=checked");
} ?> />
    </div>
</div>
<div class="form-group">
    <label for="ignore" class="col-sm-2 control-label">Ignore</label>
    <div class="col-sm-6">
       <input name="ignore" type="checkbox" id="ignore" value="1" <?php if ($device['ignore']) {
            echo("checked=checked");
} ?> />
    </div>
</div>
<div class="row">
    <div class="col-md-1 col-md-offset-2">
        <button type="submit" name="Submit"  class="btn btn-default"><i class="fa fa-check"></i> Save</button>
    </div>
</div>
</form>
<br />
<script>
    $("#rediscover").click(function() {
        var device_id = $(this).data("device_id");
        $.ajax({
            type: 'POST',
            url: 'ajax_form.php',
            data: { type: "rediscover-device", device_id: device_id },
            dataType: "json",
            success: function(data){
                if(data['status'] == 'ok') {
                    toastr.success(data['message']);
                } else {
                    toastr.error(data['message']);
                }
            },
            error:function(){
                toastr.error('An error occured setting this device to be rediscovered');
            }
        });
    });
    $('#hostname-edit-button').click(function(e) {
        e.preventDefault();
        disabled_state = document.getElementById('edit-hostname-input').disabled;
        if (disabled_state == true) {
            document.getElementById('edit-hostname-input').disabled = false;
        } else {
            document.getElementById('edit-hostname-input').disabled = true;
        }
    });
</script>
<?php
print_optionbar_start();
list($sizeondisk, $numrrds) = foldersize($config['rrd_dir']."/".$device['hostname']);
echo("Size on Disk: <b>" . formatStorage($sizeondisk) . "</b> in <b>" . $numrrds . " RRD files</b>.");
print_optionbar_end();
?>
