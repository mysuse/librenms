<?php
$bill_id = mres($vars['occupancy_id']);

$by_date = mres($vars['bydate']);

$wheres = array();
$param = array();

//error_log("Date Start  " . $vars['startdate'] . " until--" . $vars['enddate'] . "by_date--->" . $by_date,0);

$sql = 'SELECT bills.*,D.device_id,D.site_name,D.site_id,D.hostname,D.location,
        witel.witelname,ports.port_id,ports.ifdescr,ports.ifname,bills.bill_cdr AS bill_allowed
        FROM bills
        INNER JOIN bill_ports ON bills.bill_id=bill_ports.bill_id
        INNER JOIN  ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        LEFT JOIN witel ON D.witel_id=witel.witel_id WHERE bills.bill_id = ?';

$bill_data = dbFetchRow($sql, array($bill_id));

$url_device=generate_url(array('page'=>'device','device'=> $bill_data['device_id']));

$ports = dbFetchRows(
        'SELECT * FROM `bill_ports` AS B, `ports` AS P, `devices` AS D
        WHERE B.bill_id = ? AND P.port_id = B.port_id
        AND D.device_id = P.device_id',
        array($bill_id)
    );

function print_port_list()
    {
        global $ports;
        echo '<div class="panel panel-primary panel-condensed">
            <div class="panel-heading">
                <h3 class="panel-title">Occupancy On Ports</h3>
            </div>
            <div class="list-group">';

        // Collected Earlier
        foreach ($ports as $port) {
            $portalias = (empty($port['ifAlias']) ? '' : ' - '.display($port['ifAlias']).'');

            echo '<div class="list-group-item">';
            echo generate_port_link($port, $port['ifName'].$portalias).' on '.generate_device_link($port);
            
            echo '</div>';
        }

        echo '</div></div>';
    }//end print_port_list

print_optionbar_start();
  echo '<div style="font-weight: bold; float: right;"><a href="'.generate_url(array('page' => 'occupancies')).'"><i class="fa fa-arrow-left fa-lg icon-theme" aria-hidden="true"></i> Back to Occupancy</a></div></br>';
print_optionbar_end();
//error_log("SQL Query --->" . $sql,0);
?>
<div class="row">
<div class="col-lg-6 col-lg-push-6">
<?php print_port_list(); ?>
</div>
<div class="col-lg-6 col-lg-pull-6">
    <div class='panel panel-primary panel-condensed'>
        <div class="panel-heading">
            <h3 class="panel-title">
               <?php echo "${bill_data['site_name']}"; ?>   
            </h3>
        </div>

            <table class="table table-hover table-condensed table-striped"><tr>
                <td>Witel</td>
                <td><?php echo "<strong>${bill_data['witel_name']}</strong>"; ?> </td>
            </tr><tr>
                <td>Host</td>
                <td><?php echo "<strong><a href='$url_device'>${bill_data['hostname']}</a></strong></br>"; ?></td>
            </tr><tr>
                <td>Bandwidth</td>
                <td><?php  echo "<strong>" . format_si($bill_data['bill_allowed'],0,0) . "</strong></br>"; ?></td>
            </tr>
        </table>

        </div>
     </div>   
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Occupation View</h3>

 <form method='post' action='' class='form-inline' role='form'>
 
 <div class="form-group">
 
 <div class='input-group date' id='datetimepicker6'>
              <input type="text" name="startdate" id="startdate" class="form-control input-sm" <?php if (strlen($vars['startdate'])) {
            echo('value="'.$vars['startdate'].'"');
} ?> placeholder="Start Date" />

                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
      
    
 
        
            <div class='input-group date' id='datetimepicker7'>
                <input type="text" name="enddate" id="enddate" class="form-control input-sm" <?php if (strlen($vars['enddate'])) {
                    echo('value="'.$vars['enddate'].'"');
} ?> placeholder="End Date" />

                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker({format: 'YYYY/MM/DD HH:mm:ss'});
        $('#datetimepicker7').datetimepicker({
            format: 'YYYY/MM/DD HH:mm:ss',
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>

   <input type="hidden" name="bydate" value="true">

   <button type="submit" class="btn btn-default btn-sm">Search</button>
   <a class="btn btn-default btn-sm" href="<?php echo(generate_url(array('page' => 'occupancy','occupancy_id' => $vars['occupancy_id']))); ?>" title="Reset critera to default." >Reset</a>
 </div>  

</form>

    </div>

    <?php


        $labels = array();
        $data   = array();



        $start_date = date("Y/m/d") . " 00:00:00";
        $end_date = date("Y/m/d") . " 23:59:59";

        if ($by_date=="true") {
        
        $start_date=$vars['startdate'];
        $end_date=$vars['enddate'];
        }

        $query = "SELECT * FROM bill_poller WHERE bill_id=$bill_id AND bill_day>='$start_date' and bill_day<='$end_date'";


        foreach (dbFetchRows($query, $param) as $row)
        {
            $date=date_create($row["bill_day"]);
            
            $strip_date=date_format($date,"H");
        
            if($strip_date == $tmp_label) {
            
                $labels[] ="";
                
            }
            
            else {

        $labels[] = $strip_date;

        
            }
        
            $data[]   = $row["percentage"];
            
            $tmp_label=$strip_date;

        }  

            $data_d_string = "[" . join(", ", $data) . "]";
            $labels_d_string = "['" . join("', '", $labels) . "']";

        ?>

        <canvas id="todaychart" width="400" height="100"></canvas>

            <script>
                var ctx = document.getElementById("todaychart");
                var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: <?php echo($labels_d_string) ?>,
                    datasets: [{
                        label: 'Occupancy (%) ',
                        data: <?php echo $data_d_string; ?>,      
                        backgroundColor:'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)'
                    
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Occupancy (%)',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }],

                        xAxes: [{
                        
                            scaleLabel: {
                                display: true,
                                labelString: 'Hour',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }]
                    },

                    title: {
                        display: true,
                        text:  <?php echo "'Occupancy Daily " . $start_date . " - " .$end_date . "'"; ?>,
                        fontSize:18,
                        fontColor:'#666'
                    }
                }
            });
            </script>
    </div>


   

    <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Monthly View</h3>
  
   <?php
    
    //Monthly View
        $data_m   = array();
        $label_m = array();

        $period_y = date("Y");
        $period_m = date("m");

        $query_m = "SELECT * FROM bill_daily WHERE bill_id=$bill_id AND period_m='$period_m'  AND period_y='$period_y' ORDER BY period_d";

        foreach (dbFetchRows($query_m, $param) as $row)
            {
                $labels_m[] = $row["period_d"];
                $data_m[]   = $row["percentage"];
            }   

            $data_m_string = "[" . join(", ", $data_m) . "]";
            $labels_m_string = "['" . join("', '", $labels_m) . "']";

            ?>

            </div>
            
            <canvas id="monthly" width="300" height="100"></canvas>
                <script>
                    var ctx = document.getElementById("monthly");
                    var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                    labels: <?php echo($labels_m_string) ?>,
                    datasets: [{
                    label: 'Occupancy',
                    data: <?php echo $data_m_string; ?>,
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 0.2)',
                    borderWidth: 1
                    }]
                },
                options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Occupancy (%)',
                            fontSize:18,
                            fontColor:'#666'
                        }
                    }],

                    xAxes: [{
                    
                        scaleLabel: {
                            display: true,
                            labelString: 'Date',
                            fontSize:18,
                            fontColor:'#666'
                        }
                    }]
                },
                title: {
                    display: true,
                    text:  <?php echo "'Occupancy Monthly " . date("M Y") . "'"; ?>,
                    fontSize:18,
                    fontColor:'#666'
                    }
                }
                });
                </script>

    </div>

  
    <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Yearly View</h3>

         <?php
                //Yearly View
                $data_y   = array();
                $label_y = array();

                $start_y = date("Y") ."/" . "1" ."/1" . " 00:00:00";
                $end_y = date("Y") . "/" . "12" ."/31".  " 23:59:59";

                //error_log("Start M -->" . $end_m,0);
                $period_y = date("Y");
                $query_y = "SELECT * FROM bill_monthly WHERE bill_id=$bill_id AND period_y ='$period_y' ORDER BY period_m";

                
                foreach (dbFetchRows($query_y, $param) as $row)
                {

                    $dateObj   = DateTime::createFromFormat('!m', $row["period_m"]);
                    $data_y[]   = $row["percentage"];
                    $labels_y[] = $dateObj->format('F');

                }   

                    $data_y_string = "[" . join(", ", $data_y) . "]";
                    $labels_y_string = "['" . join("', '", $labels_y) . "']";

                ?>

                </div>
            <canvas id="yearly" width="400" height="100"></canvas>
            <script>
                var ctx = document.getElementById("yearly");
                var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo($labels_y_string) ?>,
                    datasets: [{
                        label: '%',
                        data: <?php echo $data_y_string; ?>,
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Occupancy (%)',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }],

                        xAxes: [{
                        
                            scaleLabel: {
                                display: true,
                                labelString: 'Year',
                                fontSize:18,
                                fontColor:'#666'
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text:  <?php echo "'Occupancy Year : " . $period_y . "'"; ?>,
                        fontSize:18,
                        fontColor:'#666'
                    }
                }
            });
            </script>
</div>    