<?php

$no_refresh = true;

if ($_POST['addbill'] == 'yes') {
    if ($_SESSION['userlevel'] < 10) {
        include 'includes/error-no-perm.inc.php';
        exit;
    }
    
    $updated = '1';



    if (isset($_POST['bill_quota']) or isset($_POST['bill_cdr'])) {
        if ($_POST['bill_type'] == 'quota') {
            if (isset($_POST['bill_quota_type'])) {
                if ($_POST['bill_quota_type'] == 'MB') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'GB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'TB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                $bill_quota = (is_numeric($_POST['bill_quota']) ? $_POST['bill_quota'] * $config['billing']['base'] * $multiplier : 0);
                $bill_cdr   = 0;
            }
        }

        if ($_POST['bill_type'] == 'cdr') {
            if (isset($_POST['bill_cdr_type'])) {
                if ($_POST['bill_cdr_type'] == 'Kbps') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Mbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Gbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                $bill_cdr   = (is_numeric($_POST['bill_cdr']) ? $_POST['bill_cdr'] * $multiplier : 0);
                $bill_quota = 0;
            }
        }
    }//end if

    $insert = array(
        'bill_name'   => $_POST['bill_name'],
        'bill_type'   => $_POST['bill_type'],
        'bill_cdr'    => $bill_cdr,
        'bill_day'    => $_POST['bill_day'],
        'bill_quota'  => $bill_quota,
        'bill_custid' => $_POST['bill_custid'],
        'bill_ref'    => $_POST['bill_ref'],
        'bill_notes'  => $_POST['bill_notes'],
        'rate_95th_in'      => 0,
        'rate_95th_out'     => 0,
        'rate_95th'         => 0,
        'dir_95th'          => 'in',
        'total_data'        => 0,
        'total_data_in'     => 0,
        'total_data_out'    => 0,
        'rate_average'      => 0,
        'rate_average_in'   => 0,
        'rate_average_out'  => 0,
        'bill_last_calc'    => array('NOW()'),
        'bill_autoadded'    => 0,
    );

    $bill_id = dbInsert($insert, 'bills');

    if (is_numeric($bill_id) && is_numeric($_POST['port_id'])) {
        dbInsert(array('bill_id' => $bill_id, 'port_id' => $_POST['port_id']), 'bill_ports');
    }
    
    header('Location: ' . generate_url(array('page' => 'bills', 'bill_id' => $bill_id, 'view' => 'edit')));
    
    exit();
}

$pagetitle[] = 'Billing';

echo "<meta http-equiv='refresh' content='10000'>";
//Toolbar Export to CSV
//print_optionbar_end();
print_optionbar_start();

echo('<div style="float: right;">');
?>

  <a href="csv.php/report=<?php echo generate_url($vars, array('format'=>'')); ?>" title="Export as CSV" target="_blank" rel="noopener">Export CSV</a> |
  <a href="<?php echo(generate_url($vars)); ?>" title="Update the browser URL to reflect the search criteria." >Update URL</a> |

<?php
if (isset($vars['searchbar']) && $vars['searchbar'] == "hide") {
    echo('<a href="'. generate_url($vars, array('searchbar' => '')).'">Search</a>');
} else {
    echo('<a href="'. generate_url($vars, array('searchbar' => 'hide')).'">Search</a>');
}

echo("  | ");

if (isset($vars['bare']) && $vars['bare'] == "yes") {
    echo('<a href="'. generate_url($vars, array('bare' => '')).'">Header</a>');
} else {
    echo('<a href="'. generate_url($vars, array('bare' => 'yes')).'">Header</a>');
}

echo('</div>');

print_optionbar_end();


//End of Toolbar Export to CSV
print_optionbar_start();

if ((isset($vars['searchbar']) && $vars['searchbar'] != "hide") || !isset($vars['searchbar'])) {
?>
<form method='post' action='' class='form-inline' role='form'>
    <div class="form-group">
<!-- Witel -->

<div class="form-group">
       
        <div class="col-sm-6">
            <select id="witel_id" name="witel_id" class='form-control input-sm'>

            <option value=''>All Witel</option>
                <?php
	                  $witels = dbFetchRows('SELECT * FROM `witel` ORDER BY witelname');
            
                         $unknown = 1;
        
                         foreach ($witels as $witel) {
                             echo('          <option value="'.$witel['witel_id'].'"');
                             if ($witel['witel_id'] == $vars['witel_id']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($witel['witelname']) . '</option>');
                         }
                         if ($unknown) {
                            echo('          <option value="other">All Witel</option>');
                            }
                ?>
            </select>
       </div>
    </div>


<!-- End Wite -->    
<!-- Site ID -->
     <input type="text" name="site_id" id="site_id" title="Site ID" class="form-control input-sm" <?php if (strlen($vars['site_id'])) {
            echo('value="'.$vars['site_id'].'"');
} ?> placeholder="Site ID" />


<!-- Site Name -->
     <input type="text" name="site_name" id="site_name" title="Site Name" class="form-control input-sm" <?php if (strlen($vars['site_name'])) {
            echo('value="'.$vars['site_name'].'"');
} ?> placeholder="Site Name" />

          
<?php

/** Disable device_id

 echo"<select name='device_id' id='device_id' class='form-control input-sm'>";
 echo "       <option value=''>All Devices</option>";

if ($_SESSION['userlevel'] >= 5) {
    $results = dbFetchRows("SELECT `device_id`,`hostname` FROM `devices` ORDER BY `hostname`");
} else {
    $results = dbFetchRows("SELECT `D`.`device_id`,`D`.`hostname` FROM `devices` AS `D`, `devices_perms` AS `P` WHERE `P`.`user_id` = ? AND `P`.`device_id` = `D`.`device_id` ORDER BY `hostname`", array($_SESSION['user_id']));
}
foreach ($results as $data) {
    echo('        <option value="'.$data['device_id'].'"');
    if ($data['device_id'] == $vars['device_id']) {
        echo("selected");
    }
    echo(">".$data['hostname']."</option>");
}

if ($_SESSION['userlevel'] < 5) {
    $results = dbFetchRows("SELECT `D`.`device_id`,`D`.`hostname` FROM `ports` AS `I` JOIN `devices` AS `D` ON `D`.`device_id`=`I`.`device_id` JOIN `ports_perms` AS `PP` ON `PP`.`port_id`=`I`.`port_id` WHERE `PP`.`user_id` = ? AND `PP`.`port_id` = `I`.`port_id` ORDER BY `hostname`", array($_SESSION['user_id']));
} else {
    $results = array();
}
foreach ($results as $data) {
    echo('        <option value="'.$data['device_id'].'"');
    if ($data['device_id'] == $vars['device_id']) {
        echo("selected");
    }
    echo(">".$data['hostname']."</option>");
}

echo " </select>";

**/

?>
     
      
      <input type="text" name="hostname" id="hostname" title="Hostname" class="form-control input-sm" <?php if (strlen($vars['hostname'])) {
            echo('value="'.$vars['hostname'].'"');
} ?> placeholder="Hostname" />
    </div>
   
   
    <div class="form-group">

        <select title="Location" name="location" id="location" class="form-control input-sm">
          <option value="">All Locations</option>
<?php

           // FIXME function?
foreach (getlocations() as $location) {
    if ($location) {
        echo('<option value="'.$location.'"');
        if ($location == $vars['location']) {
            echo(" selected");
        }
        echo(">".$location."</option>");
    }
}
            ?>
        </select>
      </div>
     
      <button type="submit" class="btn btn-default btn-sm">Search</button>
      <a class="btn btn-default btn-sm" href="<?php echo(generate_url(array('page' => 'bills', 'section' => $vars['section'], 'bare' => $vars['bare']))); ?>" title="Reset critera to default." >Reset</a>
      </td>

    </form>
<?php 
}
print_optionbar_end();

include 'includes/modal/new_bill.inc.php';
?>

<div class="panel panel-default panel-condensed">
    <div class="table-responsive">
        <table class="table table-condensed table-hover" id="bills-list">
        <thead>
          <th data-column-id="witel" data-searchable="true" data-sortable="false">Witel</th>

            <th data-column-id="site_id" data-sortable="true">Site ID</th> 
            <th data-column-id="site_name" data-searchable="true" data-sortable="true">Site Name</th>
          
           
            <th data-column-id="hostname">Hostname</th>
            
            <th data-column-id="bill_allowed"  data-align="right" data-css-class="green">Allowed</th>
            <th data-column-id="total_data_in" data-align="right" data-css-class="blue">Inbound</th>
            <th data-column-id="total_data_out" data-align="right" data-css-class="blue">Outbound</th>
            <th data-column-id="graph" data-sortable="false">Occupancy</th>
            <th data-column-id="actions" data-sortable="false">Action</th>
        </thead>
        </table>
    </div>
</div>


<script type="text/html" id="table-header">


    <div id="{{ctx.id}}" class="{{css.header}}">
        <div class="row">
            <div class="col-sm-4">
            <?php if ($_SESSION['userlevel'] >= 10) {  ?>
                <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#create-bill"><i class="fa fa-plus"></i> Create Bill</button>
            <?php } ?>     
            </div>
            <div class="col-sm-8 actionBar">
             
                <span class="form-inline" id="table-filters">

                <fieldset class="form-group">

                   <select name='period' id='period' class="form-control input-sm">
                      <option value=''>Current Billing Period</option>
                      <option value='prev'>Previous Billing Period</option>
                    </select>
                    <select name='bill_type' id='bill_type' class="form-control input-sm">
                      <option value=''>All Types</option>
                      <option value='cdr' <?php if ($_GET['bill_type'] === 'cdr') {
                            echo 'selected';
} ?>>CDR</option>
                      <option value='quota' <?php if ($_GET['bill_type'] === 'quota') {
                            echo 'selected';
} ?>>Quota</option>
                    </select>
                    <select name='state' id='state' class="form-control input-sm">
                      <option value=''>All States</option>
                      <option value='under' <?php if ($_GET['state'] === 'under') {
                            echo 'selected';
} ?>>Under Quota</option>
                      <option value='over' <?php if ($_GET['state'] === 'over') {
                            echo 'selected';
} ?>>Over Quota</option>
                    </select>
                  </fieldset>
                </span>
                <form>
                <p class="{{css.search}}"></p>
                <p class="{{css.actions}}"></p>
                
        </div>
    </div>
</script>


<script type="text/javascript">

   var grid = $('#bills-list').bootgrid({
       ajax: true,
       templates: {
           header: $('#table-header').html()
       },
       columnSelection: false,
       rowCount: [50,100,250,-1],
     
      templates: {
        search: "" // hide the generic search
       },
       post: function() {
           return {
               id: 'bills',
               device_id: '<?php echo mres($vars['device_id']); ?>',
               hostname: '<?php echo htmlspecialchars($vars['hostname']); ?>',
               site_id: '<?php echo htmlspecialchars($vars['site_id']); ?>',
               site_name: '<?php echo htmlspecialchars($vars['site_name']); ?>',
               witel_id:'<?php echo mres($vars['witel_id']); ?>',
               state: $('select#state').val(),
               period: $('select#period').val(),
               location: '<?php echo mres($vars['location']); ?>'
           };
       },
      url: "ajax_table.php"
    }).on("loaded.rs.jquery.bootgrid", function() {
    });
    $('#table-filters select').on('change', function() { grid.bootgrid('reload'); }); 
        
<?php
if ($vars['view'] == 'add') {
?>
$(function() {
    $('#create-bill').modal('show');    
});
<?php
}
?>
</script>


