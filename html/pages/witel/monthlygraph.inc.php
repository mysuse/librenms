<?php
  
   //error_log("Year " . $bw_year,0);

   $query_graph_bar1="SELECT witel_id,period_d,period_m,period_y,bill_day,
        COALESCE(percent25,0) as percent25,
        COALESCE(percent2550,0) as percent2550, 
        COALESCE(percent5075,0) as percent5075,
        COALESCE(percent75,0) as percent75
        FROM bill_witel_daily
        WHERE witel_id='" .$witel_id . "' AND period_y='" . $bw_year . "'";

    $labels = array();
    
    $data25 = array();
    $data2550=array();
    $data5075=array();
    $data75=array();

    //error_log("Witel data " . $query_graph_bar1,0);

    foreach (dbFetchRows($query_graph_bar1) as $row) {
   
        $data25[]=$row['percent25'];
        $data2550[]=$row['percent2550'];
        $data5075[]=$row['percent5075'];
        $data75[]=$row['percent75'];
       
        $mysqldate = strtotime($row['bill_day']);
        $date_l = date( 'd/M', $mysqldate);

        $labels[] = $date_l;

    
    }

    
    $data_25_string = "[" . join(", ", $data25) . "]";
    $data_2550_string = "[" . join(", ", $data2550) . "]";
    $data_5075_string = "[" . join(", ", $data5075) . "]";
    $data_75_string = "[" . join(", ", $data75) . "]";

    $labels_string = "['" . join("', '", $labels) . "']";

    //error_log("Graph data " . $data_75_string,0);

?>