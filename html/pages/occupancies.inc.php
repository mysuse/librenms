
<?php

$no_refresh = true;

//Add or Edit Occupancy

if ($_POST['addbill'] == 'yes') {
    if ($_SESSION['userlevel'] < 10) {
        include 'includes/error-no-perm.inc.php';
        exit;
    }
    
    $updated = '1';



    if (isset($_POST['bill_quota']) or isset($_POST['bill_cdr'])) {
        if ($_POST['bill_type'] == 'quota') {
            if (isset($_POST['bill_quota_type'])) {
                if ($_POST['bill_quota_type'] == 'MB') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'GB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_quota_type'] == 'TB') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                $bill_quota = (is_numeric($_POST['bill_quota']) ? $_POST['bill_quota'] * $config['billing']['base'] * $multiplier : 0);
                $bill_cdr   = 0;
            }
        }

        if ($_POST['bill_type'] == 'cdr') {
            if (isset($_POST['bill_cdr_type'])) {
                if ($_POST['bill_cdr_type'] == 'Kbps') {
                    $multiplier = (1 * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Mbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base']);
                }

                if ($_POST['bill_cdr_type'] == 'Gbps') {
                    $multiplier = (1 * $config['billing']['base'] * $config['billing']['base'] * $config['billing']['base']);
                }

                $bill_cdr   = (is_numeric($_POST['bill_cdr']) ? $_POST['bill_cdr'] * $multiplier : 0);
                $bill_quota = 0;
            }
        }
    }//end if

    $insert = array(
        'bill_name'   => $_POST['bill_name'],
        'bill_type'   => $_POST['bill_type'],
        'bill_cdr'    => $bill_cdr,
        'bill_day'    => $_POST['bill_day'],
        'bill_quota'  => $bill_quota,
        'bill_custid' => $_POST['bill_custid'],
        'bill_ref'    => $_POST['bill_ref'],
        'bill_notes'  => $_POST['bill_notes'],
        'rate_95th_in'      => 0,
        'rate_95th_out'     => 0,
        'rate_95th'         => 0,
        'dir_95th'          => 'in',
        'total_data'        => 0,
        'total_data_in'     => 0,
        'total_data_out'    => 0,
        'rate_average'      => 0,
        'rate_average_in'   => 0,
        'rate_average_out'  => 0,
        'bill_last_calc'    => array('NOW()'),
        'bill_autoadded'    => 0,
    );

    $bill_id = dbInsert($insert, 'bills');
    
    if (is_numeric($bill_id) && is_numeric($_POST['port_id'])) {

       // error_log("Occupancy Insert " . $bill_id ,0);

        dbInsert(array('bill_id' => $bill_id, 'port_id' => $_POST['port_id']), 'bill_ports');
    }
    
    header('Location: ' . generate_url(array('page' => 'occupancies', 'bill_id' => $bill_id, 'view' => 'edit')));
    
    exit();
}
//end of add and edit occupancy

include 'includes/modal/new_bill.inc.php';

$pagetitle[] = 'Occupancy';

echo "<meta http-equiv='refresh' content='10000'>";

//Toolbar Export to CSV
//print_optionbar_end();
echo "<h2>Occupancy Overview on Node-B </h2>";
print_optionbar_start();

echo('<div style="float: right;">');
?>

  <a href="csv.php/report=<?php echo generate_url($vars, array('format'=>'')); ?>" title="Export as CSV" target="_blank" rel="noopener">Export CSV</a> |
  <a href="<?php echo(generate_url($vars)); ?>" title="Update the browser URL to reflect the search criteria." >Update URL</a> |

<?php
if (isset($vars['searchbar']) && $vars['searchbar'] == "hide") {
    echo('<a href="'. generate_url($vars, array('searchbar' => '')).'">Search</a>');
} else {
    echo('<a href="'. generate_url($vars, array('searchbar' => 'hide')).'">Search</a>');
}

echo("  | ");

if (isset($vars['bare']) && $vars['bare'] == "yes") {
    echo('<a href="'. generate_url($vars, array('bare' => '')).'">Header</a>');
} else {
    echo('<a href="'. generate_url($vars, array('bare' => 'yes')).'">Header</a>');
}

echo('</div>');

print_optionbar_end();


//End of csv export
?>

<form method='post' action='' class='form-inline' role='form'>
 
 <!-- Tsel Region -->

<div class="form-group">
       
       <div class="col-sm-6">
            <select id="tselregion_id" name="tselregion_id" class='form-control input-sm'>

            <option value=''>All Region</option>
                <?php
	                  $tsels = dbFetchRows('SELECT * FROM `tselregion` ORDER BY tselregion_name');
            
                         $unknown = 1;
        
                         foreach ($tsels as $tsel) {
                             echo('          <option value="'.$tsel['tselregion_id'].'"');
                             if ($tsel['tselregion_id'] == $vars['tselregion_id']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($tsel['tselregion_name']) . '</option>');
                         }
                         if ($unknown) {
                            echo('          <option value="other">All Region</option>');
                            }
                ?>
            </select>
       </div>
 </div>

 <div class="form-group">

    <div class="col-sm-6">
            <select id="witel_id" name="witel_id" class='form-control input-sm'>

            <option value=''>All Witel</option>
                <?php
	                  $witels = dbFetchRows('SELECT * FROM `witel` ORDER BY witelname');
            
                         $unknown = 1;
        
                         foreach ($witels as $witel) {
                             echo('          <option value="'.$witel['witel_id'].'"');
                             if ($witel['witel_id'] == $vars['witel_id']) {
                                  echo(' selected="1"');
                                    $unknown = 0;
                                    }
                                 echo(' >' . ucfirst($witel['witelname']) . '</option>');
                         }
                         if ($unknown) {
                            echo('          <option value="other">All Witel</option>');
                            }
                ?>
            </select>
       </div>

    </div>

    <div class="form-group">

    <div class="col-sm-6">
            <select id="occupancy" name="occupancy" class='form-control input-sm'>

            <option value=''>All Occupancy</option>
            <option value='25' <?php if ($vars['occupancy']=="25") { echo('selected="1"'); } ?> >0 - 25 %</option>
            <option value='50' <?php if ($vars['occupancy']=="50") { echo('selected="1"'); } ?>>26 - 50%</option>
            <option value='70' <?php if ($vars['occupancy']=="70") { echo('selected="1"'); } ?>>51 - 70%</option>
            <option value='100' <?php if ($vars['occupancy']=="100") { echo('selected="1"'); } ?>>Above 70%</option>
             
            </select>
       </div>

    </div>



   
  
<!-- Site Name -->
     <input type="text" name="hostname" id="hostname" title="IP ONT" class="form-control input-sm" <?php if (strlen($vars['hostname'])) {
            echo('value="'.$vars['hostname'].'"');
} ?> placeholder="IP ONT" />

 <input type="text" name="site_id" id="site_id" title="Site ID" class="form-control input-sm" <?php if (strlen($vars['site_id'])) {
            echo('value="'.$vars['site_id'].'"');
} ?> placeholder="Site ID" />
   



<?php
 
 /*     
 Disable Date Selection
            <div class='input-group date' id='datetimepicker6'>
              <input type="text" name="startdate" id="startdate" class="form-control input-sm" <?php if (strlen($vars['startdate'])) {
            echo('value="'.$vars['startdate'].'"');
} ?> placeholder="Start Date" />

                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
      
    
 
        
            <div class='input-group date' id='datetimepicker7'>
                <input type="text" name="enddate" id="enddate" class="form-control input-sm" <?php if (strlen($vars['enddate'])) {
                    echo('value="'.$vars['enddate'].'"');
} ?> placeholder="End Date" />

                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        
  

<script type="text/javascript">
    $(function () {
        $('#datetimepicker6').datetimepicker();
        $('#datetimepicker7').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>

Disable Date Selection
*/

?>
   <button type="submit" class="btn btn-default btn-sm">Search</button>
      <a class="btn btn-default btn-sm" href="<?php echo(generate_url(array('page' => 'occupancies', 'section' => $vars['section'], 'bare' => $vars['bare']))); ?>" title="Reset critera to default." >Reset</a>
      </td>


</div>
    </form>

    <?php /*end of search form */ ?>

<div class="panel panel-default panel-condensed">
    <div class="table-responsive">
        <table  class="bordered table-condensed table-hover" id="occupancy-list">
        <thead>
           
           <?php 
                $startdate = strtotime($vars['startdate']);
                $enddate = strtotime($vars['enddate']);
                
                $str_startdate=date('Y-m-d',$startdate);
                $str_enddate=date('Y-m-d',$enddate);

                $str_today=date('Y-m-d');

                     
           ?>

            <th data-column-id="device_status" data-searchable="false" data-sortable="false">Status</th>
            <th data-column-id="tselregion" data-searchable="true" data-sortable="true">Tsel Region</th>                  
            <th data-column-id="site_id" data-sortable="true">Site ID</th> 
            <th data-column-id="site_name" data-sortable="true">Site Name</th> 
            
            <th data-column-id="bandwidth" data-sortable="true">Bandwidth(Mbps)</th>
           
            <th data-column-id="current_occupancy" data-sortable="true">Current</th>    
           
            <th data-column-id="day_high" data-sortable="true">Today High</th>
          
            <th data-column-id="week_high" data-sortable="true">Weekly High</th>

            <th data-column-id="month_high" data-sortable="true">Month High</th>

            <th data-column-id="actions" data-sortable="false">Action</th>
        </thead>
        </table>
    </div>
</div>



<script type="text/javascript">

   var grid = $('#occupancy-list').bootgrid({
       ajax: true,
       templates: {
           header: $('#table-header').html()
       },
       columnSelection: false,
       rowCount: [50,100,250,-1],
     
      templates: {
        search: "" // hide the generic search
       },
       post: function() {
           return {
               id: 'occupancies',
               hostname: '<?php echo htmlspecialchars($vars['hostname']); ?>',
               site_id: '<?php echo htmlspecialchars($vars['site_id']); ?>',
               site_name: '<?php echo htmlspecialchars($vars['site_name']); ?>',
               witel_id:'<?php echo mres($vars['witel_id']); ?>',
               tselregion_id:'<?php echo mres($vars['tselregion_id']); ?>',
               startdate:'<?php echo mres($vars['startdate']); ?>',
               enddate:'<?php echo mres($vars['enddate']); ?>',
               occupancy: '<?php echo mres($vars['occupancy']); ?>'
           };
       },
      url: "ajax_table.php"
    }).on("loaded.rs.jquery.bootgrid", function() {
    });
    $('#table-filters select').on('change', function() { grid.bootgrid('reload'); }); 
</script>

<?php

   // error_log("Data String " . $data_string,0);
?>

<script type="text/html" id="table-header">


    <div id="{{ctx.id}}" class="{{css.header}}">
        <div class="row">
            <div class="col-sm-4">
            <?php if ($_SESSION['userlevel'] >= 10) {  ?>
                <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#create-bill"><i class="fa fa-plus"></i> Create Bill</button>
            <?php } ?>     
            </div>
            <div class="col-sm-8 actionBar">
             
                <span class="form-inline" id="table-filters">

                <fieldset class="form-group">

                   <select name='period' id='period' class="form-control input-sm">
                      <option value=''>Current Billing Period</option>
                      <option value='prev'>Previous Billing Period</option>
                    </select>
                    <select name='bill_type' id='bill_type' class="form-control input-sm">
                      <option value=''>All Types</option>
                      <option value='cdr' <?php if ($_GET['bill_type'] === 'cdr') {
                            echo 'selected';
} ?>>CDR</option>
                      <option value='quota' <?php if ($_GET['bill_type'] === 'quota') {
                            echo 'selected';
} ?>>Quota</option>
                    </select>
                    <select name='state' id='state' class="form-control input-sm">
                      <option value=''>All States</option>
                      <option value='under' <?php if ($_GET['state'] === 'under') {
                            echo 'selected';
} ?>>Under Quota</option>
                      <option value='over' <?php if ($_GET['state'] === 'over') {
                            echo 'selected';
} ?>>Over Quota</option>
                    </select>
                  </fieldset>
                </span>
                <form>
                <p class="{{css.search}}"></p>
                <p class="{{css.actions}}"></p>
                
        </div>
    </div>
</script>


<script type="text/javascript">
        
<?php
if ($vars['view'] == 'add') {
?>
$(function() {
    $('#create-bill').modal('show');    
});
<?php
}
?>
</script>
