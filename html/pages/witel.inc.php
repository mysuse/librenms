<?php

   if ($_POST['addwitel'] == 'yes') {
    if ($_SESSION['userlevel'] < 10) {
        include 'includes/error-no-perm.inc.php';
        exit;
    }
   
    
    if (isset($_POST['witelname'])){
        
        $witelname = $_POST['witelname'];

        
    }//end if

    if (isset($_POST['witel_id'])) {

        $witel_id=$_POST['witel_id'];
    }

    $insert = array(
        'witelname'   => $witelname,
    );
    
     $witel_id = dbInsert($insert, 'witel');
     
    header('Location:/witel');
    exit();
}

$pagetitle[] = 'Witel';

?>




 <!-- Modal -->
 <?php
  include 'includes/modal/new_witel.inc.php';

  function print_witel_list()
  {
     
      echo '<div class="panel panel-primary panel-condensed">
          <div class="panel-heading">
              <h3 class="panel-title">Witel List</h3>
          </div>
          <div class="list-group">
          <table class="table table-hover table-condensed table-striped">
         <tr> 
         <td>Witel ID</td>
         <td>Witel Name </td> 
         <td>Action</td></tr>';
         

      // Collected Earlier
      $sql ="SELECT witel_id,witelname FROM witel ORDER BY witelname";
      foreach (dbFetchRows($sql, $param) as $witel) {
       
        $table_row ="<tr><td>" . $witel['witel_id'] ."</td>";
        $table_row .="<td>" . $witel['witelname'] ."</td>";
        $table_row .="<td><i class='fa fa-pencil fa-lg icon-theme' title='Edit' aria-hidden='true'></i> Edit</a></td></tr>";
       
       echo $table_row;
        }      
      echo '</table></div></div>';
  }//end print_witel_list

  function print_tselregion_list()
  {
     
      echo '<div class="panel panel-primary panel-condensed">
          <div class="panel-heading">
              <h3 class="panel-title">Telkomsel Region</h3>
          </div>
          <div class="list-group">
          <table class="table table-hover table-condensed table-striped">
         <tr> 
         <td>Region ID</td> 
         <td>Region Name</td>
         <td>Action</td>
         </tr>';
         

      // Collected Earlier
      $sql ="SELECT tselregion_id,tselregion_name FROM tselregion ORDER BY tselregion_name";
      foreach (dbFetchRows($sql, $param) as $region) {

        $tsel_row ="<tr><td>" . $region['tselregion_id'] ."</td>";
        $tsel_row .="<td>" . $region['tselregion_name'] ."</td>";
        $tsel_row.="<td><i class='fa fa-pencil fa-lg icon-theme' title='Edit' aria-hidden='true'></i> Edit</a></td></tr>";
       
       echo $tsel_row;
        }      
      echo '</table></div></div>';
      
  }//end print_tsel_list
?>


<div class="row">
<div class="col-lg-6 col-lg-push-6">
<?php print_witel_list(); ?>

<form id="myform" class="form-wizard">
        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#create-witel"><i class="fa fa-plus"></i> Add Witel</button>  
</form>

</div>

<div class="col-lg-6 col-lg-pull-6">
    
<?php print_tselregion_list(); ?>

<form id="myform" class="form-wizard">
        <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#create-region"><i class="fa fa-plus"></i> Add Region</button>  
</form>

</div>


