<?php
/* Copyright (C) 2015 Sergiusz Paprzycki <serek@walcz.net>
 * Copyright (C) 2016 Cercel Valentin <crc@nuamchefazi.ro>
 *
 * This widget is based on legacy frontpage module created by Paul Gear.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$widget_settings['witel_count']  = $widget_settings['witel_count'] > 0 ? $widget_settings['witel_count'] : 10;
$widget_settings['time_interval'] = $widget_settings['time_interval'] > 0 ? $widget_settings['time_interval'] : 15;

if (defined('SHOW_SETTINGS') || empty($widget_settings)) {
    $common_out = '
    <form class="form" onsubmit="widget_settings(this); return false;">
        <div class="form-group">
            <div class="col-sm-4">
                <label for="title" class="control-label availability-map-widget-header">Widget title</label>
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title" placeholder="Custom title for widget" value="' . htmlspecialchars($widget_settings['title']) . '">
            </div>
        </div>
                
        <div class="form-group">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-default">Set</button>
            </div>
        </div>
    </form>';

    //error_log("Common Outpunt -->" . $common_out,0);

    $common_output[]=$common_out;

} else {
    $interval = $widget_settings['time_interval'];
    (integer)$interval_seconds = ($interval * 60);
    (integer)$witel_count = $widget_settings['witel_count'];


  $common_output[] ='<h4> Occupancy Overview All Witel by Today ' . date('d-M-Y') . '</h4>';
  
  $basedate = date('Y-m-d');
  $time1 = strtotime($basedate . "00:00");
  $time2 = strtotime($basedate . "23:59");

  $startdate = date('Y-m-d H:i',$time1);
  $enddate = date('Y-m-d H:i',$time2);

 //error_log("StartDate ---> " . $enddate,0);
$select="SELECT DISTINCT w.witel_id,
w.witelname,
COALESCE(bd1.numofnodb,0) as _25,
COALESCE(bd2.numofnodb,0) as _2550,
COALESCE(bd3.numofnodb,0) as _5075,
COALESCE(bd4.numofnodb,0) as _75
FROM witel w 
LEFT JOIN devices d ON d.witel_id = w.witel_id
LEFT JOIN ports p ON p.device_id = d.device_id
LEFT JOIN bill_ports bp ON bp.port_id = p.port_id
LEFT JOIN bills b ON b.bill_id=bp.bill_id      
LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bills b1
        JOIN bill_ports bp ON b1.bill_id = bp.bill_id
        JOIN ports p ON bp.port_id = p.port_id
        JOIN devices dev ON p.device_id = dev.device_id
        JOIN witel w2 ON dev.witel_id = w2.witel_id
        WHERE b1.current_occupancy<=25 AND dev.status='1'
        GROUP BY w2.witel_id
     ) bd1 ON w.witel_id=bd1.witel_id
 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bills b1
        JOIN bill_ports bp ON b1.bill_id = bp.bill_id
        JOIN ports p ON bp.port_id = p.port_id
        JOIN devices dev ON p.device_id = dev.device_id
        JOIN witel w2 ON dev.witel_id = w2.witel_id
        WHERE b1.current_occupancy>25 AND b1.current_occupancy<=50 AND dev.status='1'
        GROUP BY w2.witel_id
        
     ) bd2 ON w.witel_id=bd2.witel_id
 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bills b1
        JOIN bill_ports bp ON b1.bill_id = bp.bill_id
        JOIN ports p ON bp.port_id = p.port_id
        JOIN devices dev ON p.device_id = dev.device_id
        JOIN witel w2 ON dev.witel_id = w2.witel_id
        WHERE b1.current_occupancy>50 AND b1.current_occupancy<=70 AND dev.status='1'
        GROUP BY w2.witel_id
 ) bd3 ON w.witel_id=bd3.witel_id

LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bills b1
        JOIN bill_ports bp ON b1.bill_id = bp.bill_id
        JOIN ports p ON bp.port_id = p.port_id
        JOIN devices dev ON p.device_id = dev.device_id
        JOIN witel w2 ON dev.witel_id = w2.witel_id
        WHERE b1.current_occupancy>70 AND dev.status='1'
        GROUP BY w2.witel_id
 ) bd4 ON w.witel_id=bd4.witel_id";

/*
     $select ="SELECT w.witel_id,w.witelname,
              (SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage<=25 AND v.witel_id=w.witel_id AND
              v.bill_day>='" . $startdate ."' and v.bill_day<='" .$enddate . "' ) as _25,

              (SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>25 AND v.percentage<=50 AND v.witel_id=w.witel_id AND
              v.bill_day>='" . $startdate ."' and v.bill_day<='" .$enddate . "') as _2550,

                (SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>50 AND v.percentage<=75 AND v.witel_id=w.witel_id AND
                v.bill_day>='" . $startdate ."' and v.bill_day<='" .$enddate . "') as _5075,

                (SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>75 AND v.witel_id=w.witel_id AND
                v.bill_day>='" . $startdate ."' and v.bill_day<='" .$enddate . "') as _75
            
               FROM witel w";
*/

     $sql = $select;

    //error_log("Query ---> " . $sql,0);

    $common_output[] = '
  <div class="panel panel-primary-condensed table-responsive">
    <table class="table table-hover table-condensed table-striped">
        <thead>
            <tr>
                <th>WITEL</th>
                <th><span class="badge bg-white">  0 - 25%</span></th>               
                <th><span class="badge bg-green"> 26 - 50%</span></th>    
                <th><span class="badge bg-yellow">51 - 70%</span></th>    
                <th><span class="badge bg-red">Above 70%</span></th>    
                
            </tr>
        </thead>
        <tbody>';

        $total25=0;
        $total2550=0;
        $total5075=0;
        $total75=0;

    foreach (dbFetchRows($sql, $params) as $result) {

        $url_witel = "occupancy-overview/witel_id=" .  $result['witel_id'];

        $url_numdevice = "occupancies/witel_id=" .  $result['witel_id'];
 
        $url_occupancy25 ="occupancies/occupancy=25/witel_id=" .  $result['witel_id'];
        $url_occupancy50 ="occupancies/occupancy=50/witel_id=" .  $result['witel_id'];
        $url_occupancy75 ="occupancies/occupancy=70/witel_id=" .  $result['witel_id'];
        $url_occupancy100 ="occupancies/occupancy=100/witel_id=" .  $result['witel_id'];
       //error_log("URL ---> " . $url,0);

        $common_output[] = '
        <tr>
            <td> <a href='.$url_numdevice.'>' . shorthost($result['witelname']) . '</td>
                      
            <td> <a href='.$url_occupancy25 . '> <span class="badge bg-white">' . format_si($result['_25'],0,0) . '</span></a></td>

            <td> <a href='.$url_occupancy50 . '><span class="badge bg-green">' . format_si($result['_2550'],0,0) . '</span></a></td>

            <td> <a href='.$url_occupancy75 . '> <span class="badge bg-yellow">' . format_si($result['_5075'],0,0) . '</span></a></td>

            <td> <a href='.$url_occupancy100 . '><span class="badge bg-red">' . format_si($result['_75'],0,0) . '</span></a></td>

        </tr>';

        $total25=$total25 +$result['_25'];
        $total2550=$total2550 + $result['_2550'];
        $total5075=$total5075 + $result['_5075'];
        $total75=$total75+$result['_75'];
    }

     //Total 
     $common_output[] = '
        <tr>
            <td><strong> Total</strong>  </td>
                      
            <td><span class="badge bg-white">' . format_si($total25,0,0) . '</span></td>

            <td><span class="badge bg-white">' . format_si($total2550,0,0) . '</span></td>

            <td><span class="badge bg-white">' . format_si($total507,0,0) . '</span></td>

            <td><span class="badge bg-white">' . format_si($total75,0,0) . '</span></td>

        </tr>';
    
    $common_output[] = '
        </tbody>
    </table>
    </div>';
}
