<?php
/* Copyright (C) 2015 Sergiusz Paprzycki <serek@walcz.net>
 * Copyright (C) 2016 Cercel Valentin <crc@nuamchefazi.ro>
 *
 * This widget is based on legacy frontpage module created by Paul Gear.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


$widget_settings['witel_count']  = $widget_settings['witel_count'] > 0 ? $widget_settings['witel_count'] : 10;
$widget_settings['time_interval'] = $widget_settings['time_interval'] > 0 ? $widget_settings['time_interval'] : 15;

if (defined('SHOW_SETTINGS') || empty($widget_settings)) {
    $common_out = '
    <form class="form" onsubmit="widget_settings(this); return false;">
        <div class="form-group">
            <div class="col-sm-4">
                <label for="title" class="control-label availability-map-widget-header">Widget title</label>
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title" placeholder="Custom title for widget" value="' . htmlspecialchars($widget_settings['title']) . '">
            </div>
        </div>
       
       
        <div class="form-group">
            <div class="col-sm-4">
                <label for="graph_type" class="control-label availability-map-widget-header">Number of Witel</label>
            </div>
            <div class="col-sm-6">
                <input class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="device_count" id="input_count_' . $unique_id . '" value="' . $widget_settings['device_count'] . '">
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-default">Set</button>
            </div>
        </div>
    </form>';

    //error_log("Common Outpunt -->" . $common_out,0);

    $common_output[]=$common_out;

} else {
    $interval = $widget_settings['time_interval'];
    (integer)$interval_seconds = ($interval * 60);
    (integer)$witel_count = $widget_settings['witel_count'];
    

    $common_output[] ='<h4> ' . $witel_count . ' Highest Occupancies  all WITEL</h4>';

      
   $query="SELECT DISTINCT w.witel_id,
        w.witelname,d.device_id,
        d.site_id,d.site_name,
        bd.bill_cdr,bd.percentage
    FROM witel w 
		LEFT JOIN devices d ON d.witel_id = w.witel_id
        LEFT JOIN ports p ON p.device_id = d.device_id
        LEFT JOIN bill_ports bp ON bp.port_id = p.port_id
        LEFT JOIN bills b ON b.bill_id=bp.bill_id 
        LEFT JOIN bill_daily bd ON bd.bill_id=b.bill_id
        WHERE bd.period_d=DAY(NOW()) AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW()) AND d.status=1
        
        ORDER BY bd.percentage DESC";
     
     $count = " LIMIT " . $witel_count;

     

     $params = array('count' => array($witel_count));

     $sql = $query .$count;

    // error_log("Query ---> " . $sql,0);

    $common_output[] = '
  <div class="panel panel-default panel-condensed table-responsive">
    <table class="table table-hover table-condensed table-striped">
        <thead>
            <tr>
                <th>Site Name</a></th>               
                <th><span class="grey">Allowed</span></th>
                <th><span class="green">Occupancy</span></th>
                
            </tr>
        </thead>
        <tbody>';

    foreach (dbFetchRows($sql, $params) as $result) {
        $url_device   =generate_url(array('page'=>'device','device'=> $result['device_id']));
        $common_output[] = "
        <tr>
                       
            <td> <a href='$url_device'> " . $result['site_id'] . "</a></td>

            <td>" . format_si($result['bill_cdr'],0,0) . "</td>

            <td>" . formatPercentage($result['percentage']) .  "</td>

        </tr>";
    }
    $common_output[] = "
        </tbody>
    </table>
    </div>";
}
