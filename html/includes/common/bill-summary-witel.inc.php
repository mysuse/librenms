<?php
//require_once 'includes/object-cache.inc.php';
 $witel_id = $widget_settings['witel_id'];

if (defined('SHOW_SETTINGS') || empty($widget_settings)) {
    $temp_output = '
    <form class="form" onsubmit="widget_settings(this); return false;">
        <div class="form-group">
            <div class="col-sm-4">
                <label for="title" class="control-label availability-map-widget-header">Widget title</label>
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title" placeholder="Custom title for widget" value="' . htmlspecialchars($widget_settings['title']) . '">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="top_query" class="control-label availability-map-widget-header">Top query</label>
            </div>
            <div class="col-sm-6">
                <select class="form-control" name="top_query">
                    <option value="traffic" ' . $selected_traffic . '>Traffic</option>
                    <option value="uptime" ' . $selected_uptime . '>Uptime</option>
                    <option value="ping" ' . $selected_ping . '>Response time</option>
                    <option value="poller" ' . $selected_poller . '>Poller duration</option>
                    <option value="cpu" ' . $selected_cpu . '>Processor load</option>
                    <option value="ram" ' . $selected_ram . '>Memory usage</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="top_query" class="control-label availability-map-widget-header">Sort order</label>
            </div>
            <div class="col-sm-6">
                <select class="form-control" name="sort_order">
                    <option value="asc" ' . $selected_sort_asc . '>Ascending</option>
                    <option value="desc" ' . $selected_sort_desc . '>Descending</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="graph_type" class="control-label availability-map-widget-header">Number of Devices</label>
            </div>
            <div class="col-sm-6">
                <input class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="device_count" id="input_count_' . $unique_id . '" value="' . $widget_settings['device_count'] . '">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <label for="graph_type" class="control-label availability-map-widget-header">Time interval (minutes)</label>
            </div>
            <div class="col-sm-6">
                <input class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="time_interval" id="input_time_' . $unique_id . '" value="' . $widget_settings['time_interval'] . '">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-default">Set</button>
            </div>
        </div>
    </form>';
}

$temp_output = '
<div class="panel panel-default panel-condensed table-responsive">
<table class="table table-hover table-condensed table-striped">
  <thead>
    <tr>
      <th>Occupancy </th>
      <th>Jumlah Node</th>
     
';



$q0_25= dbFetchCell("SELECT COUNT(*) as jumlah FROM bills WHERE ROUND((bills.rate_95th/bills.bill_cdr)*100,2)<=25", $param);
$q26_50 = dbFetchCell("SELECT COUNT(*) as jumlah FROM bills WHERE ROUND((bills.rate_95th/bills.bill_cdr)*100,2)>25 AND ROUND((bills.rate_95th/bills.bill_cdr)*100,2)<=50", $param);
$q51_75= dbFetchCell("SELECT COUNT(*) as jumlah FROM bills WHERE ROUND((bills.rate_95th/bills.bill_cdr)*100,2)>50  AND ROUND((bills.rate_95th/bills.bill_cdr)*100,2)<=75", $param);
$q76  = dbFetchCell("SELECT COUNT(*) as jumlah FROM bills WHERE ROUND((bills.rate_95th/bills.bill_cdr)*100,2)>75", $param); 

$temp_output .= '
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span class="badge bg-white"> <25% </span></td> 
      <td><span class="badge bg-green">'.   $q0_25  .'</span></td>
   ';


$temp_output .= '
    </tr>
    <tr>
      <td><span class="badge bg-white">>25% s.d 50%</span></td>
      <td><span class="badge bg-yellow">'.   $q26_50  .'</span></td>
';

$temp_output .= '
    </tr>
    <tr>
      <th><span class="badge bg-white">>50% s.d 75%</span></td>
      <td><span class="badge bg-orange">'.   $q51_75  .'</span></td>
';

$temp_output .= '
    </tr>
    <tr>
      <th><span class="badge bg-white">>75% </span></td>
      <td><span class="badge bg-red">'.   $q76  .'</span></td>
';



$temp_output .= '
    </tr>
  </tbody>
</table>
</div>
';
   

unset($common_output);
$common_output[] = $temp_output;
