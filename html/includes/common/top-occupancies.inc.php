<?php
/* Copyright (C) 2015 Sergiusz Paprzycki <serek@walcz.net>
 * Copyright (C) 2016 Cercel Valentin <crc@nuamchefazi.ro>
 *
 * This widget is based on legacy frontpage module created by Paul Gear.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$sort_order = $widget_settings['sort_order'];
$witel_id =$widget_settings['witel_id'];

$selected_sort_asc = '';
$selected_sort_desc = '';

if ($sort_order === 'asc') {
    $selected_sort_asc = 'selected';
} elseif ($sort_order === 'desc') {
    $selected_sort_desc = 'selected';
}


$select_witel  =  '<select id="witel_id" name="witel_id" class="form-control">';

$select_witel  .=   '<option value=\'\'>All Witel</option>';

	           $witels = dbFetchRows('SELECT * FROM `witel` ORDER BY witelname');
                       $unknown = 1;
                        foreach ($witels as $witel) {
                         $select_witel  .= '  <option value="'.$witel['witel_id'].'"';
                             if ($witel['witel_id'] == $vars['witel_id']) {
                                  
                                  $select_witel  .= ' selected="1"';
                                    $unknown = 0;
                                    }
                                  $select_witel  .=' >' . ucfirst($witel['witelname']) . '</option>';
                         }
                         if ($unknown) {
                            $select_witel  .='          <option value="other">All Witel</option>';
                     }

                    $select_witel  .='</select>';


$widget_settings['device_count']  = $widget_settings['device_count'] > 0 ? $widget_settings['device_count'] : 5;
$widget_settings['time_interval'] = $widget_settings['time_interval'] > 0 ? $widget_settings['time_interval'] : 15;
$widget_settings['witel_id'] = $widget_settings['witel_id'] > 0 ? $widget_settings['witel_id'] : 0;

if (defined('SHOW_SETTINGS') || empty($widget_settings)) {
    $common_out = '
    <form class="form" onsubmit="widget_settings(this); return false;">
        <div class="form-group">
            <div class="col-sm-4">
                <label for="title" class="control-label availability-map-widget-header">Widget title</label>
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title" placeholder="Custom title for widget" value="' . htmlspecialchars($widget_settings['title']) . '">
            </div>
        </div>
       
        <div class="form-group">
            <div class="col-sm-4">
                <label for="top_query" class="control-label availability-map-widget-header">Witel </label>
            </div>
            
            <div class="col-sm-6">' . $select_witel .
            
            '

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <label for="graph_type" class="control-label availability-map-widget-header">Number of Devices</label>
            </div>
            <div class="col-sm-6">
                <input class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="device_count" id="input_count_' . $unique_id . '" value="' . $widget_settings['device_count'] . '">
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-default">Set</button>
            </div>
        </div>
    </form>';

    //error_log("Common Outpunt -->" . $common_out,0);

    $common_output[]=$common_out;

} else {
    $interval = $widget_settings['time_interval'];
    (integer)$interval_seconds = ($interval * 60);
    (integer)$device_count = $widget_settings['device_count'];
    (integer)$witel_id=$widget_settings['witel_id'];

    $witel_name="All Witel";
    
    if ($witel_id>0) {
         $wtl_query = "SELECT witelname FROM witel WHERE witel_id=" .$witel_id;
         $witel_name = dbFetchCell($wtl_query, $param);
    }
    $common_output[] ='<h4><b>WITEL : ' . $witel_name . '</b></br>' . $device_count . ' Highest Occupancies </h4>';

    
   // $query="SELECT w.witel_id,w.witelname,v.site_name,v.device_id,v.site_id,v.bill_cdr,v.percentage
   //        FROM witel w JOIN v_listbilldailybysite v ON w.witel_id=v.witel_id";

$query="SELECT  B.bill_id,B.bill_name,B.bill_cdr,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,w.witelname,
                B.current_occupancy as percentage
                FROM bills B
                INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
                INNER JOIN  ports ON bill_ports.port_id=ports.port_id
                INNER JOIN devices AS D ON ports.device_id=D.device_id 
                INNER JOIN witel w ON D.witel_id=w.witel_id";

    $where=" WHERE B.current_occupancy>=20 AND D.status=1";
    
    $group_by=" ORDER BY B.current_occupancy DESC";   

    if ($witel_id>0) {
   
     $where = $where . " AND w.witel_id =" . $witel_id;       
    
     }
     
     if ($device_count>0) {

         $device_count=$device_count;
     }
    
    else {

        $device_count=10;
    }
     
     $count = " LIMIT " . $device_count;


     $params = array('witel_id'=>array($witel_id),'count' => array($device_count));

     $sql = $query .$where .$group_by .$count;

     //error_log("Query ---> " . $sql,0);

    $common_output[] = '
    <div class="table-responsive">
        <table class="table table-hover table-condensed table-striped bootgrid-table">
        <thead>
            <tr>
                <th class="text-left">Site Name</th>
                <th><span class="grey">Allowed</span></th>
                <th><span class="green">Occupancy</span></th>
            </tr>
        </thead>
        <tbody>';

    foreach (dbFetchRows($sql, $params) as $result) {
        $url_device   =generate_url(array('page'=>'device','device'=> $result['device_id']));
        $common_output[] = "
        <tr>
            <td class='text-left'><a href='$url_device'>" . shorthost($result['site_name']) . "</a></td>
            <td>" . format_si($result['bill_cdr'],0,0) . "</td>

            <td>" . formatPercentage($result['percentage']) .  "</td>
        </tr>";
    }
    $common_output[] = "
        </tbody>
    </table>
    </div>";
}
