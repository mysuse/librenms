<?php

$wheres = array();
$param = array();

if (isset($searchPhrase) && !empty($searchPhrase)) {
    $wheres[] = 'B.bill_name LIKE ?';
    $param[] = "%$searchPhrase%";
}

if (!empty($_POST['site_name'])) {
    $wheres[] = ' D.site_name LIKE ?';
    $param[] = '%' . $_POST['site_name'] . '%';
}

if (!empty($_POST['witel_id'])) {
    $wheres[] = 'D.witel_id = ?';
    $param[] =  $_POST['witel_id'];
}

if (!empty($_POST['tselregion_id'])) {
    $wheres[] = 'D.tselregion_id = ?';
    $param[] =  $_POST['tselregion_id'];

}


if (!empty($_POST['startdate']) and !empty($_POST['enddate']) ) {
 
    $startdate = strtotime($_POST['startdate']);
    $enddate = strtotime($_POST['enddate']);
}
else {

    $startdate = date('Y-m-d h:i)');
    $enddate =   date('Y-m-d h:i)');
}

 

if (!empty($_POST['site_id'])) {
    $wheres[] = 'D.site_id LIKE ?';
    $param[] = '%' . $_POST['site_id'] . '%';
}



if (!empty($_POST['hostname'])) {
    $wheres[] = 'D.hostname = ?';
    $param[] = $_POST['hostname'];
}


if (!empty($_POST['occupancy'])) {
    
    $pos1="0";
    $pos2="26";
    $pos3="51";
    $pos4="71";
    $pos5="75";

    //error_log("Occupancy  " . $_POST['occupancy'],0);
    
    if ($_POST['occupancy']=="25") {

        $wheres[] ="B.current_occupancy BETWEEN '$pos1'  AND '$pos2'";
    }
    
    if ($_POST['occupancy']=="50") {

        $wheres[] ="B.current_occupancy BETWEEN '$pos2'  AND '$pos3'";
        
    }

    if ($_POST['occupancy']=="70") {
        
         $wheres[] ="B.current_occupancy BETWEEN '$pos3'  AND '$pos4'";
                
    }
    
    if ($_POST['occupancy']=="100") {
                
         $wheres[] ="B.current_occupancy > $pos5";
                        
    }
}


if (!isset($sort) || empty($sort)) {
    $order_by=" ORDER BY B.current_occupancy DESC ";
}
else {
    $order_by = " ORDER BY " . $sort;

}

if (isset($current)) {
    $limit_low  = (($current * $rowCount) - ($rowCount));
    $limit_high = $rowCount;
}

if ($rowCount != -1) {
    $limitby .= " LIMIT $limit_low,$limit_high";
}

//error_log("Start Date " . $startdate  . "End Date " . $enddate);
 
$str_startdate=date('Y-m-d',$startdate);
$str_enddate=date('Y-m-d',$enddate);




//error_log("Start Date Str" . $str_startdate  . "End Date Str" . $str_enddate);

$select=" B.bill_id,B.bill_name,D.bandwidth,B.current_occupancy as percent,B.occupancy_day,B.occupancy_week,B.occupancy_month,B.occupancy_year,
            D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
            ports.port_id,T.tselregion_name,
            COALESCE(B.rate_95th_out,0) as rate_95th_out,D.status,
            COALESCE(B.rate_95th,0) as rate_95th,
            B.bill_last_calc AS bill_day,
            COALESCE(B.bill_cdr) as bill_cdr, COALESCE(B.rate_95th_in,0) as rate_95th_in";
                
 $from =" FROM bills B
        INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
        INNER JOIN  ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        LEFT JOIN witel ON D.witel_id=witel.witel_id
        LEFT JOIN tselregion T ON D.tselregion_id=T.tselregion_id";
             

          
     
if (sizeof($wheres) > 0) {
    $query .= " WHERE " . implode(' AND ', $wheres) . "\n";
   }

 $sql = "SELECT " . $select .$from . $query . $order_by  .$limitby;

 $count_sql = "SELECT COUNT(B.bill_id) " .$from . $query;
 
  $total = dbFetchCell($count_sql, $param);
   
 if (empty($total)) {
     $total = 0;
 } 

  
  
   foreach (dbFetchRows($sql, $param) as $occupancy) {
   
   $paramstring=join(", ", $param); 
   
  // error_log("Param ---->" . $paramstring,0);
   $datemeasurement = $occupancy['bill_day'];
   $url          = generate_url(array('page' => 'occupancy', 'occupancy_id' => $occupancy['bill_id']));
   $bill_id= $occupancy['bill_id'];
  
   $url_witel = generate_url(array('page'=>'occupancy-overview','witel_id'=>$occupancy['witel_id']));

   //$site_name="<a href='$url_witel'><span style='font-weight: bold;' class='interface'>" . $occupancy['site_id'] . "</span></a>";
                //$datemeasurement;
   
   //error_log("Tsel Region Name --->" . $tselregion,0);


   $url_device=generate_url(array('page'=>'device','device'=> $occupancy['device_id']));
 
  
   $hostname= "<a href='$url'><strong>" . $occupancy['hostname'] . "</strong></a>";
   $site_id = $occupancy['site_id'];
   $site_name=$occupancy['site_name'];
   
 
   $allowed    = "<span class='badge bg-green'>"  . format_bytes_billing_short($occupancy['bill_cdr']).'bps' . "</span>";
   
  //$allowed    = "<span class='badge bg-green'>"  . $occupancy['bandwidth'].'Mbps' . "</span>";
   
   $in         = "<span class='badge bg-yellow'>"  . format_si($occupancy['rate_95th_in']).'bps'. "</span>";
   $out        = "<span class='badge bg-white'>"  . format_si($occupancy['rate_95th_out']).'bps'. "</span>";
   
   $status_message = array("<span class='badge bg-red'>Down</span>", "<span class='badge bg-green'>Up</span>");
   $device_status=$status_message[$occupancy['status']];
  
   $day_high=formatPercentage($occupancy['occupancy_day']);
   
    $percent_d = round($occupancy['occupancy_day'],2);
    $percent_w=round($occupancy['occupancy_week'],2);
    $percent_m = round($occupancy['occupancy_month'],2);
    $percent_y = round($occupancy['occupancy_year'],2);

    $week_high=formatPercentage($occupancy['occupancy_week']);
    $month_high=formatPercentage($occupancy['occupancy_month']);
   

   $year_high=formatPercentage($occupancy['occupancy_year']);
   

       
    $percent = round($occupancy['percent'],2);

    if ($occupancy['status']==0) {
        
        $percent=0;
    }
     //error_log("Percent ---->" . $percent,0);

    $background        = get_percentage_colours($percent);
    $right_background  = $background['right'];
    $left_background   = $background['left'];

   $device_id=$occupancy['device_id'];
    
   $bar = formatPercentage($percent);

   
   $tselregion=$occupancy['tselregion_name'];
   
   $rate_95th    = "<span class='badge bg-red'>"  . format_si($occupancy['rate_95th']) . 'bps' . "</span>";
   
   $to=$config['time']['now'];
   $port_id=$occupancy['port_id'];

   $actions    = "";

   
   $actions .= "<a href='" . generate_url(array('page' => 'graphs', 'from'=>'-1day','to'=>$to,'id'=>$port_id,'type'=>'port_bits')) .
    "'><i class='fa fa-globe fa-lg icon-theme' title='Traffic' aria-hidden='true'></i>Traffic</a> ";

   $actions .= "<a href='" . generate_url(array('page' => 'device', 'device' =>$device_id)) .
   "'><i class='fa fa-id-card fa-lg icon-theme' title='Datek' aria-hidden='true'></i>Datek</a> ";

   
    if (is_admin()) {
         
        $actions .= "<a href='" . generate_url(array('page' => 'bill', 'bill_id' =>$bill_id, 'view' => 'edit')) .
            "'><i class='fa fa-pencil fa-lg icon-theme' title='Edit' aria-hidden='true'></i>Edit</a> ";
       
    
    }
  
    $response[] = array(
        'bill_id'=>$bill_id,
        'tselregion'=>$tselregion,
        'site_id'=>$site_id,
        'site_name'=>$site_name,
        'hostname'=>$hostname,
        'tselregion'=>$tselregion,
        'bandwidth'=>$allowed,
        'in'=>$in,
        'out'=>$out,
        'current_occupancy'=>$bar,
        'day_high'=>$day_high,
        'week_high'=>$week_high,
        'month_high'=>$month_high,
        'year_high'=>$year_high,
        'device_status'=>$device_status,
        'actions'=>$actions
   );

} //end foreach

$output = array(
    'current'  => $current,
    'rowCount' => $rowCount,
    'rows'     => $response,
    'total'    => $total,
);

//error_log("JSON OUTPUT -->" .  _json_encode($output));

echo _json_encode($output);
