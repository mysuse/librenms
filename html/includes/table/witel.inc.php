<?php

$sql ="SELECT witel_id,witelname FROM witel ORDER BY witelname";

$count_sql = "SELECT COUNT(witel_id) FROM witel";

$total = dbFetchCell($count_sql, $param);

if (empty($total)) {
    $total = 0;
} 

 foreach (dbFetchRows($sql, $param) as $witel) {
 
   $witel_id= $witel['witel_id'];
   $witelname=$witel['witelname'];
   $actions = "<i class='fa fa-pencil fa-lg icon-theme' title='Edit' aria-hidden='true'></i> Edit</a>";  

   $response[] = array(
    'witel_id'=>$witel_id,
    'witelname'=>$witelname,
    'actions'=>$actions
   );
 

 }
 
   // error_log("SQL -->" . $sql,0);
 
    $output = array(
    'current'  => $current,
    'rowCount' => $rowCount,
    'rows'     => $response,
    'total'    => $total,
    );

    //rror_log("JSON -->" . _json_encode($output),0 );

    echo _json_encode($output);