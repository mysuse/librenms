<?php

// Calculate filters
$prev = !empty($_POST['period']) && ($_POST['period'] == 'prev');
$wheres = array();
$param = array();
if (isset($searchPhrase) && !empty($searchPhrase)) {
    $wheres[] = 'bills.bill_name LIKE ?';
    $param[] = "%$searchPhrase%";
}

if (!empty($_POST['bill_type'])) {
    if ($prev) {
        $wheres[] = 'bill_history.bill_type = ?';
    } else {
        $wheres[] = 'bill_type = ?';
    }
    $param[] = $_POST['bill_type'];
}


if (!empty($_POST['hostname'])) {
    $wheres[] = ' D.hostname LIKE ?';
    $param[] = '%' . $_POST['hostname'] . '%';
}


if (!empty($_POST['device_id'])) {
    $wheres[] = 'D.device_id = ?';
    $param[] = $_POST['device_id'];
}

if (!empty($_POST['witel_id'])) {
    $wheres[] = 'D.witel_id = ?';
    $param[] =  $_POST['witel_id'];
}

if (!empty($_POST['site_id'])) {
    $wheres[] = 'D.site_id LIKE ?';
    $param[] = '%' . $_POST['site_id'] . '%';
}

if (!empty($_POST['site_name'])) {
    $wheres[] = 'D.site_name LIKE ?';
    $param[] = '%' . $_POST['site_name'] . '%';
}

if (!empty($_POST['location'])) {
    $wheres[] = 'D.location LIKE ?';
    $param[] = '%' . $_POST['location'] . '%';
}

if (!empty($_POST['state'])) {
    if ($_POST['state'] === 'under') {
        if ($prev) {
            $wheres[] = "((bill_history.bill_type = 'cdr' AND bill_history.rate_95th <= bill_history.bill_allowed) OR (bill_history.bill_type = 'quota' AND bill_history.traf_total <= bill_history.bill_allowed))";
        } else {
            $wheres[] = "((bill_type = 'cdr' AND rate_95th <= bill_cdr) OR (bill_type = 'quota' AND total_data <= bill_quota))";
        }
    } elseif ($_POST['state'] === 'over') {
        if ($prev) {
            $wheres[] = "((bill_history.bill_type = 'cdr' AND bill_history.rate_95th > bill_history.bill_allowed) OR (bill_history.bill_type = 'quota' AND bill_history.traf_total > bill_allowed))";
        } else {
            $wheres[] = "((bill_type = 'cdr' AND rate_95th > bill_cdr) OR (bill_type = 'quota' AND total_data > bill_quota))";
        }
    }
}
 
if ($prev) {
    $select = "SELECT bills.bill_name, bills.bill_notes, bill_history.*, bill_history.traf_total as total_data, bill_history.traf_in as total_data_in, bill_history.traf_out as total_data_out ";
    $query = 'FROM `bills`
        INNER JOIN (SELECT bill_id, MAX(bill_hist_id) AS bill_hist_id FROM bill_history WHERE bill_dateto < NOW() AND bill_dateto > subdate(NOW(), 40) GROUP BY bill_id) qLastBills ON bills.bill_id = qLastBills.bill_id
        INNER JOIN bill_history ON qLastBills.bill_hist_id = bill_history.bill_hist_id
';
} else {
    $select = "SELECT bills.*,ROUND((bills.rate_95th/bills.bill_cdr)*100,2)  AS percent_cdr, ROUND((bills.total_data/bills.bill_quota)*100,2)  AS percent_quota,

                      D.device_id,D.site_name,D.site_id,D.sto,D.vlan2g,D.vlan3g,D.hostname,D.location,
                      D.metro_name,D.metro_port,D.metro_ip,D.olt_name,D.olt_port,D.olt_ip,D.ont_ip,D.ont_sn,
                      witel.witelname,
                      ports.port_id,
                      IF(bills.bill_type = 'CDR', bill_cdr, bill_quota) AS bill_allowed
    ";
    $query = "FROM `bills`
     INNER JOIN bill_ports ON bills.bill_id=bill_ports.bill_id
     INNER JOIN  ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     LEFT JOIN witel ON D.witel_id=witel.witel_id";

   // echo "Query -->" . $query;
    //Put Inner Join Here
}


// Permissions check
if (is_admin() === false && is_read() === false) {
    $query  .= ' INNER JOIN `bill_perms` AS `BP` ON `bills`.`bill_id` = `BP`.`bill_id` ';
    $wheres[] = '`BP`.`user_id`=?';
    $param[] = $_SESSION['user_id'];
}

if (sizeof($wheres) > 0) {
    $query .= " WHERE " . implode(' AND ', $wheres) . "\n";

 
}
$orderby = "ORDER BY percent_cdr";

//Custom Field

  
$total = dbFetchCell("SELECT COUNT(bills.bill_id) $query", $param);

$sql = "$select $query";

if (!isset($sort) || empty($sort)) {
    $sort = 'percent_cdr DESC';
}

$sql .= "\nORDER BY $sort";

if (isset($current)) {
    $limit_low  = (($current * $rowCount) - ($rowCount));
    $limit_high = $rowCount;
}

if ($rowCount != -1) {
    $sql .= " LIMIT $limit_low,$limit_high";
}



foreach (dbFetchRows($sql, $param) as $bill) {


    if ($prev) {
        $datefrom = $bill['bill_datefrom'];
        $dateto   = $bill['bill_dateto'];
    } else {
        $day_data = getDates($bill['bill_day']);
        $datefrom = $day_data['0'];
        $dateto   = $day_data['1'];
    }
    $rate_95th    = "<span class='badge bg-red'>"  . format_si($bill['rate_95th']) . 'bps' . "</span>";
    $dir_95th     = $bill['dir_95th'];
    $total_data   =   format_bytes_billing_short($bill['total_data']);
    $rate_average = $bill['rate_average'];
    $url          = generate_url(array('page' => 'bill', 'bill_id' => $bill['bill_id']));
    $used95th     = "<span class='badge bg-white'>" . format_si($bill['rate_95th']).'bps' . "</span>";
    $notes        = $bill['bill_notes'];
   
    $url_device   =generate_url(array('page'=>'device','device'=> $bill['device_id']));
    //custom field
    $bill_last_calc = $bill['bill_last_calc'];  
    $site_name      = "<a href='$url'><span style='font-weight: bold;' class='interface'>${bill['site_name']}</span></a><br />" .
                       strftime('%F', strtotime($datefrom)) . " to " . strftime('%F', strtotime($dateto));
   
    $hostname  =  "<a href='$url_device'>" . $bill['hostname'] . "</a>";

    $site_id        = $bill['site_id'];
    $sto            = $bill['sto'];
    $vlan2g         = $bill['vlan2g'];
    $vlan3g         = $bill['vlan3g'];

    $location = $bill['location'];
    
    $witel =$bill['witelname'];

    $port_id        = $bill['port_id'];
   
    $metro_name=$bill['metro_name'];

    $metro_port=$bill['metro_port'];

    $metro_ip=$bill['metro_ip'];

    $olt_name=$bill['olt_name'];
        $olt_ip=$bill['olt_ip'];

        $olt_port=$bill['olt_port'];

        $ont_ip=$bill['ont_ip'];
        $ont_sn=$bill['ont_sn'];

        $witel_id=$bill['witel_id'];

    if ($prev) {
        $percent = $bill['bill_percent'];
        $overuse = $bill['bill_overuse'];
    } else {
    }

    if (strtolower($bill['bill_type']) == 'cdr') {
        $type       = 'CDR';
        $allowed    = "<span class='badge bg-green'>"  . format_bytes_billing_short($bill['bill_allowed']).'bps' . "</span>";
        $in         = "<span class='badge bg-yellow'>"  . format_si($bill['rate_95th_in']).'bps'. "</span>";
        $out        = "<span class='badge bg-white'>"  . format_si($bill['rate_95th_out']).'bps'. "</span>";
   

        if (!$prev) {
            $percent    = round((($bill['rate_95th'] / $bill['bill_allowed']) * 100), 2);
            $overuse    = ($bill['rate_95th'] - $bill['bill_allowed']);
        }
        
        $overuse_formatted    = format_si($overuse).'bps';
        $used                 = $rate_95th;
        $tmp_used             = $bill['rate_95th'];
        $rate_95th            = "<b>$rate_95th</b>";
    } elseif (strtolower($bill['bill_type']) == 'quota') {
        $type       = 'Quota';
        $allowed    = format_bytes_billing($bill['bill_allowed']);
        if (!empty($prev)) {
            $in  = format_bytes_billing($bill['traf_in']);
            $out = format_bytes_billing($bill['traf_out']);
        } else {
            $in  = format_bytes_billing($bill['total_data_in']);
            $out = format_bytes_billing($bill['total_data_out']);
        }
        if (!$prev) {
            $percent    = round((($bill['total_data'] / ($bill['bill_allowed'])) * 100), 2);
            $overuse    = ($bill['total_data'] - $bill['bill_allowed']);
        }
        
        $overuse_formatted    = format_bytes_billing($overuse);
        $used                 = $total_data;
        $tmp_used             = $bill['total_data'];
        $total_data           = "<b>$total_data</b>";
    }

    $background        = get_percentage_colours($percent);
    $right_background  = $background['right'];
    $left_background   = $background['left'];
    $overuse_formatted = (($overuse <= 0) ? '-' : "<span style='color: #${background['left']}; font-weight: bold;'>$overuse_formatted</span>");
    
    $bill_name  = "<a href='$url'><span style='font-weight: bold;' class='interface'>${bill['bill_name']}</span></a><br />"  .
                    strftime('%F', strtotime($datefrom)) . " to " . strftime('%F', strtotime($dateto));
    $bar        = print_percentage_bar(200, 20, $percent, null, 'ffffff', $background['left'], $percent.'%', 'ffffff', $background['right']);
    $actions    = "";
    
    if (!$prev && is_admin()) {
        $actions .= "<a href='" . generate_url(array('page' => 'bill', 'bill_id' => $bill['bill_id'], 'view' => 'edit')) .
            "'><i class='fa fa-pencil fa-lg icon-theme' title='Edit' aria-hidden='true'></i> Edit</a> ";
    }
    $predicted = format_bytes_billing(getPredictedUsage($bill['bill_day'], $tmp_used));
       
    $response[] = array(
        'bill_name'     => $bill_name,
        'notes'         => $notes,
        'bill_type'     => $type,
        'bill_allowed'    => $allowed,
        'total_data_in' => $in,
        'total_data_out'=> $out,
        'total_data'    => $total_data,
        'rate_95th'     => $rate_95th,
        'rate_average'     => $rate_average,
        'used'          => $used,
        'overusage'     => $overuse_formatted,
        'predicted'     => $predicted,
        'bill_last_calc'=> $bill_last_calc, //Custom Fields
        'site_name'     => $site_name, //Custom Fields
        'site_id'       => $site_id,
        'port_id'       => $port_id,
        'hostname'      => $hostname ,
        'graph_url'     => $graph_url,
        'location'      => $location,
        'witel'         => $witel,
        'metro_port'    => $metro_port,
        'metro_name'    => $metro_name,
        'metro_ip'      => $metro_ip,
        'graph'         => $bar,
        'actions'       => $actions
    );
}


$output = array('current' => $current, 'rowCount' => $rowCount, 'rows' => $response, 'total' => $total);
echo _json_encode($output);


 //For debugging
 