<?php

$wheres = array();
$param = array();

if (isset($searchPhrase) && !empty($searchPhrase)) {
    $wheres[] = 'B.bill_name LIKE ?';
    $param[] = "%$searchPhrase%";
}

if (!empty($_POST['hostname'])) {
    $wheres[] = ' D.hostname LIKE ?';
    $param[] = '%' . $_POST['hostname'] . '%';
}

if (!empty($_POST['witel_id'])) {
    $wheres[] = 'D.witel_id = ?';
    $param[] =  $_POST['witel_id'];
}

if (!empty($_POST['site_id'])) {
    $wheres[] = 'D.site_id LIKE ?';
    $param[] = '%' . $_POST['site_id'] . '%';
}

if (!empty($_POST['site_name'])) {
    $wheres[] = 'D.site_name LIKE ?';
    $param[] = '%' . $_POST['site_name'] . '%';
}

if (!empty($_POST['bd_month'])) {
    $wheres[] = 'bv.bd_month = ?';
    $param[] = $_POST['bd_month'];
}
else { //Default Month
    $wheres[] = 'bv.bd_month = ?';
   $param[] = date("n");
}

if (!empty($_POST['bd_year'])) {
    $wheres[] = 'bv.bd_year = ?';
    $param[] = $_POST['bd_year'];
}
else { //Default Year

    $wheres[] = 'bv.bd_year = ?';
    $param[] = date("Y");
}

//Function FormatDate
function formatDate($fpercentdate) {

  if ($fpercentdate<1) {

       $tmp_format= "<span class='badge bg-white'>"  . '-'. "</span>";
   }

  elseif (($fpercentdate>=1) and ($fpercentdate<=25)) {

       $tmp_format= "<span class='badge bg-green'>"  . format_si($fpercentdate,0,0).'%'. "</span>";
       
   }

   elseif (($fpercentdate>25) and ($fpercentdate<=50)) {

       $tmp_format= "<span class='badge bg-green'>"  . format_si($fpercentdate,0,0).'%'. "</span>";
       
   }

   elseif (($fpercentdate>50) and ($fpercentdate<=75)) {

       $tmp_format= "<span class='badge bg-yellow'>"  . format_si($fpercentdate,0,0).'%'. "</span>";
   }

   else {

      $tmp_format= "<span class='badge bg-red'>"  . format_si($fpercentdate,0,0).'%'. "</span>";
   }

   return $tmp_format;
} //End function 


$select = "SELECT B.bill_id,B.bill_name,bd.bill_cdr,bd.rate_95th_in,bd.rate_95th_out,bd.rate_95th,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
    ports.port_id,bv.bd_year,bv.bd_month,bv.1,bv.2,bv.3,bv.4,bv.5,bv.6,bv.7,bv.8,bv.9,bv.10,bv.11,bv.12,bv.13,bv.14,bv.15,bv.16,bv.17,bv.18,bv.19,
    bv.20,bv.21,bv.22,bv.23,bv.24,bv.25,bv.26,bv.27,bv.28,bv.29,bv.30,bv.31";
        

$query = " FROM bills B
   INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
     INNER JOIN  ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     INNER JOIN witel ON D.witel_id=witel.witel_id
     INNER JOIN bill_daily bd ON bd.bill_id=B.bill_id
     INNER JOIN v_billday_extended bv ON bd.bill_id=bv.bill_id";

          
 $group_by =" GROUP BY B.bill_id,bv.bd_year,bv.bd_month";
     
if (sizeof($wheres) > 0) {
    $query .= " WHERE " . implode(' AND ', $wheres) . "\n";
}

 $sql = $select . $query . $group_by;

 
 $count_sql = "SELECT COUNT(bill_id) FROM bills";

 $total = dbFetchCell($count_sql, $param);
  
if (empty($total)) {
    $total = 0;
} 
   foreach (dbFetchRows($sql, $param) as $occupancy) {
   
   $bill_id= $occupancy['bill_id'];
   $site_name=$occupancy['site_name'];
   $hostname=  $occupancy['hostname'];
   $site_id = $occupancy['site_id'];
    $witelname = $occupancy['witelname'];
   $o_year=$occupancy['bd_year'];
   $o_month=$occupancy['bd_month'];

   $d1= formatDate($occupancy['1']);
   
    
   //d2

    

    $d2= formatDate($occupancy['2']);
 

//$d3        
    
     

   $d3= formatDate($occupancy['3']);
 

   $d4 =  formatDate($occupancy['4']);
$d5 =  formatDate($occupancy['5']);
$d6 =  formatDate($occupancy['6']);
$d7 =  formatDate($occupancy['7']);
$d8 =  formatDate($occupancy['8']);
$d9 =  formatDate($occupancy['9']);
$d10 =  formatDate($occupancy['10']);
$d11 =  formatDate($occupancy['11']);
$d12 =  formatDate($occupancy['12']);
$d13 =  formatDate($occupancy['13']);
$d14 =  formatDate($occupancy['14']);
$d15 =  formatDate($occupancy['15']);
$d16 =  formatDate($occupancy['16']);
$d17 =  formatDate($occupancy['17']);
$d18 =  formatDate($occupancy['18']);
$d19 =  formatDate($occupancy['19']);
$d20 =  formatDate($occupancy['20']);
$d21 =  formatDate($occupancy['21']);
$d22 =  formatDate($occupancy['22']);
$d23 =  formatDate($occupancy['23']);
$d24 =  formatDate($occupancy['24']);
$d25 =  formatDate($occupancy['25']);
$d26 =  formatDate($occupancy['26']);
$d27 =  formatDate($occupancy['27']);
$d28 =  formatDate($occupancy['28']);
$d29 =  formatDate($occupancy['29']);
$d30 =  formatDate($occupancy['30']);
$d31 =  formatDate($occupancy['31']);

   
    $response[] = array(
        'bill_id'=>$bill_id,
        'site_id'=>$site_id,
        'site_name'=>$site_name,
        'hostname'=>$hostname,
        'witelname'=>$witelname,
        'year'=>$o_year,
        'month'=>$o_month,
        'd1'=>$d1,
        'd2'=>$d2,
        'd3'=>$d3,
        'd4'=>$d4,
        'd5'=>$d5,
        'd6'=>$d6,
        'd7'=>$d7,
        'd8'=>$d8,
        'd9'=>$d9,
        'd10'=>$d10,
        'd11'=>$d11,
        'd12'=>$d12,
        'd13'=>$d13,
        'd14'=>$d14,
        'd15'=>$d15,
        'd16'=>$d16,
        'd17'=>$d17,
        'd18'=>$d18,
        'd19'=>$d19,
        'd20'=>$d20,
        'd21'=>$d21,
        'd22'=>$d22,
        'd23'=>$d23,
        'd24'=>$d24,
        'd25'=>$d25,
        'd26'=>$d26,
        'd27'=>$d27,
        'd28'=>$d28,
        'd29'=>$d29,
        'd30'=>$d30,
        'd31'=>$d31
   );

} //end foreach

$output = array(
    'current'  => $current,
    'rowCount' => $rowCount,
    'rows'     => $response,
    'total'    => $total,
);


echo _json_encode($output);



?>