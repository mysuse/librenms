<?php

$wheres = array();
$param = array();

$this_month=date("n");
$this_year=date("Y");

if (isset($searchPhrase) && !empty($searchPhrase)) {
    $wheres[] = 'B.bill_name LIKE ?';
    $param[] = "%$searchPhrase%";
}

if (!empty($_POST['hostname'])) {
    $wheres[] = ' D.hostname LIKE ?';
    $param[] = '%' . $_POST['hostname'] . '%';
}

if (!empty($_POST['witel_id'])) {
    $wheres[] = 'D.witel_id = ?';
    $param[] =  $_POST['witel_id'];
}

if (!empty($_POST['site_id'])) {
    $wheres[] = 'D.site_id LIKE ?';
    $param[] = '%' . $_POST['site_id'] . '%';
}

if (!empty($_POST['site_name'])) {
    $wheres[] = 'D.site_name LIKE ?';
    $param[] = '%' . $_POST['site_name'] . '%';
}

if (!empty($_POST['bd_month'])) {
     $this_month=$_POST['bd_month'];
}
else { //Default Month
  $this_month = date("n");
}

if (!empty($_POST['bd_year'])) {
      $this_year=$_POST['bd_year'];
}
else { //Default Year
   $this_year= date("Y");
}

$select="SELECT b.bill_id,b.bill_cdr,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,T.tselregion_name,
      bm._1,bm._2,bm._3,bm._4,bm._5,bm._6,bm._7,bm._8,bm._9,bm._10,
      bm._11,bm._12,bm._13,bm._14,bm._15,bm._16,bm._17,bm._18,bm._19,bm._20,
      bm._21,bm._12,bm._23,bm._24,bm._25,bm._26,bm._27,bm._28,bm._29,bm._30,bm._31";
      
    $from =" FROM  bills b
     
     INNER JOIN bill_ports ON b.bill_id=bill_ports.bill_id
     
     INNER JOIN ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     LEFT JOIN tselregion T ON D.tselregion_id=T.tselregion_id
     LEFT JOIN witel ON D.witel_id=witel.witel_id
     LEFT JOIN (SELECT * FROM bill_monthly bd WHERE  bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bm ON b.bill_id=bm.bill_id";
     
if (sizeof($wheres) > 0) {
    $query .= " WHERE " . implode(' AND ', $wheres) . "\n";
}

 
 $sql = $select . $from . $query;

 
 $count_sql = "SELECT COUNT(bill_id) FROM bills";

 $monthName = date('F', mktime(0, 0, 0, $this_month, 10));

 $total = dbFetchCell($count_sql, $param);
  
if (empty($total)) {
    $total = 0;
} 
   foreach (dbFetchRows($sql, $param) as $occupancy) {
   
   $bill_id= $occupancy['bill_id'];
   $url_device=generate_url(array('page'=>'device','device'=> $occupancy['device_id']));
 
   $hostname= "<a href='$url_device'><span style='font-weight: bold;' class='interface'>" . $occupancy['hostname'] . "<br /> ${occupancy['witelname']}</span></a>";
   $site_id = $occupancy['site_id'];
   
  
   $site_name=$occupancy['site_name'];
   
   
   $tselregion=$occupancy['tselregion_name']; 

   $witelname="<a href='$url_witel'><span style='font-weight: bold;' class='interface'>" . $occupancy['witelname'] . "<br /> ${occupancy['witelname']}</span></a>";
  
   
   $bandwidth=format_si($occupancy['bill_cdr'],0,0);

    $url_1=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'1','bydate'=>'true'));

    $url_2=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'2','bydate'=>'true'));
    $url_3=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'3','bydate'=>'true'));

    $url_4=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'4','bydate'=>'true'));
    $url_5=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'5','bydate'=>'true'));

    $url_6=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'6','bydate'=>'true'));

    $url_7=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'7','bydate'=>'true'));

    $url_8=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'8','bydate'=>'true'));

    $url_9=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'9','bydate'=>'true'));

    $url_10=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'10','bydate'=>'true'));

    $url_11=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'11','bydate'=>'true'));

    $url_12=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'12','bydate'=>'true'));

    $url_13=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'13','bydate'=>'true'));

    $url_14=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'14','bydate'=>'true'));

    $url_15=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'15','bydate'=>'true'));
    $url_16=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'16','bydate'=>'true'));


    $url_17=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'17','bydate'=>'true'));

    $url_18=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'18','bydate'=>'true'));

    $url_19=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'19','bydate'=>'true'));

    $url_20=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'20','bydate'=>'true'));

    $url_21=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'21','bydate'=>'true'));

    $url_22=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'22','bydate'=>'true'));
    $url_23=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'23','bydate'=>'true'));

    $url_24=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'24','bydate'=>'true'));

    $url_25=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'25','bydate'=>'true'));
    $url_26=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'26','bydate'=>'true'));


    $url_27=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'27','bydate'=>'true'));

    $url_28=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'28','bydate'=>'true'));

    $url_29=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'29','bydate'=>'true'));

    $url_30=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'30','bydate'=>'true'));

    $url_31=generate_url(array('page'=>'graph-occupancy','occupancy_id'=> $bill_id,'year'=>$this_year,'month'=>$this_month,'day'=>'31','bydate'=>'true'));


    $d1="<a href='$url_1'" . formatPercentage($occupancy['_1']) . "</a>";
    $d2="<a href='$url_2'" . formatPercentage($occupancy['_2']). "</a>";
    $d3="<a href='$url_3'" . formatPercentage($occupancy['_3']). "</a>";
    $d4 ="<a href='$url_4'" .   formatPercentage($occupancy['_4']). "</a>";
    $d5 ="<a href='$url_5'" .   formatPercentage($occupancy['_5']). "</a>";
    $d6 ="<a href='$url_6'" .   formatPercentage($occupancy['_6']). "</a>";
    $d7 ="<a href='$url_7'" .   formatPercentage($occupancy['_7']). "</a>";
    $d8 ="<a href='$url_8'" .   formatPercentage($occupancy['_8']). "</a>";
    $d9 ="<a href='$url_9'" .   formatPercentage($occupancy['_9']). "</a>";
    $d10 ="<a href='$url_10'" .   formatPercentage($occupancy['_10']). "</a>";
    $d11 ="<a href='$url_11'" .   formatPercentage($occupancy['_11']). "</a>";
    $d12 ="<a href='$url_12'" .   formatPercentage($occupancy['_12']). "</a>";
    $d13 ="<a href='$url_13'" .   formatPercentage($occupancy['_13']). "</a>";
    $d14 ="<a href='$url_14'" .   formatPercentage($occupancy['_14']). "</a>";
    $d15 ="<a href='$url_15'" .   formatPercentage($occupancy['_15']). "</a>";
    $d16 ="<a href='$url_16'" .   formatPercentage($occupancy['_16']). "</a>";
    $d17 ="<a href='$url_17'" .   formatPercentage($occupancy['_17']). "</a>";
    $d18 ="<a href='$url_18'" .   formatPercentage($occupancy['_18']). "</a>";
    $d19 ="<a href='$url_19'" .   formatPercentage($occupancy['_19']). "</a>";
    $d20 ="<a href='$url_20'" .   formatPercentage($occupancy['_20']). "</a>";
    $d21 ="<a href='$url_21'" .   formatPercentage($occupancy['_21']). "</a>";
    $d22 ="<a href='$url_22'" .   formatPercentage($occupancy['_22']). "</a>";
    $d23 ="<a href='$url_23'" .   formatPercentage($occupancy['_23']). "</a>";
    $d24 ="<a href='$url_24'" .   formatPercentage($occupancy['_24']). "</a>";
    $d25 ="<a href='$url_25'" .   formatPercentage($occupancy['_25']). "</a>";
    $d26 ="<a href='$url_26'" .   formatPercentage($occupancy['_26']). "</a>";
    $d27 ="<a href='$url_27'" .   formatPercentage($occupancy['_27']). "</a>";
    $d28 ="<a href='$url_28'" .   formatPercentage($occupancy['_28']). "</a>";
    $d29 ="<a href='$url_29'" .   formatPercentage($occupancy['_29']). "</a>";
    $d30 ="<a href='$url_30'" .   formatPercentage($occupancy['_30']). "</a>";
    $d31 ="<a href='$url_31'" .   formatPercentage($occupancy['_31']). "</a>";

   
    $response[] = array(
        'bill_id'=>$bill_id,
        'tselregion'=>$tselregion,
        'site_id'=>$site_id,
        'site_name'=>$site_name,
        'hostname'=>$hostname,
        'witelname'=>$witelname,
        'bandwidth'=>$bandwidth,
        'year'=>$this_year,
        'month'=>$monthName,
        'd1'=>$d1,
        'd2'=>$d2,
        'd3'=>$d3,
        'd4'=>$d4,
        'd5'=>$d5,
        'd6'=>$d6,
        'd7'=>$d7,
        'd8'=>$d8,
        'd9'=>$d9,
        'd10'=>$d10,
        'd11'=>$d11,
        'd12'=>$d12,
        'd13'=>$d13,
        'd14'=>$d14,
        'd15'=>$d15,
        'd16'=>$d16,
        'd17'=>$d17,
        'd18'=>$d18,
        'd19'=>$d19,
        'd20'=>$d20,
        'd21'=>$d21,
        'd22'=>$d22,
        'd23'=>$d23,
        'd24'=>$d24,
        'd25'=>$d25,
        'd26'=>$d26,
        'd27'=>$d27,
        'd28'=>$d28,
        'd29'=>$d29,
        'd30'=>$d30,
        'd31'=>$d31
   );

} //end foreach

$output = array(
    'current'  => $current,
    'rowCount' => $rowCount,
    'rows'     => $response,
    'total'    => $total,
);

echo _json_encode($output);

    