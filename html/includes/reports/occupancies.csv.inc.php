<?php

$wheres = array();
$param = array();


if (!empty($_POST['startdate']) and !empty($_POST['enddate']) ) {
 
    $startdate = strtotime($_POST['startdate']);
    $enddate = strtotime($_POST['enddate']);
}
else {
    $startdate = date('Y-m-d h:i)');
    $enddate =   date('Y-m-d h:i)');
}
 

if (!empty($_POST['site_name'])) {
    $wheres[] = 'D.site_name LIKE ?';
    $param[] = '%' . $_POST['site_name'] . '%';
}

if (!empty($_POST['occupancy'])) {
    
    $pos1="0";
    $pos2="26";
    $pos3="51";
    $pos4="76";
    $pos5="75";

    //error_log("Occupancy  " . $_POST['occupancy'],0);
    
    if ($_POST['occupancy']=="25") {

        $wheres[] ="B.current_occupancy BETWEEN '$pos1'  AND '$pos2'";
    }
    
    if ($_POST['occupancy']=="50") {

        $wheres[] ="B.current_occupancy BETWEEN '$pos2'  AND '$pos3'";
        
    }

    if ($_POST['occupancy']=="75") {
        
         $wheres[] ="B.current_occupancy BETWEEN '$pos3'  AND '$pos4'";
                
    }
    
    if ($_POST['occupancy']=="100") {
                
         $wheres[] ="B.current_occupancy > $pos5";
                        
    }
}

$where = " WHERE B.bill_id> 0 ";

foreach ($vars as $var => $value) {
    if ($value != '') {
        switch ($var) {
            case 'site_id':
                $where  .= ' AND D.site_id LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'site_name':
                $where  .= ' AND D.site_name LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'hostname':
                $where  .= ' AND D.hostname = ?';
                $param[] = $value;
                break;

            case 'witel_id':
                $where  .= ' AND D.witel_id = ?';
                $param[] = $value;
                break;
            case 'occupancy':
           
            $pos1="0";
            $pos2="26";
            $pos3="51";
            $pos4="76";
            $pos5="75";
        
            //error_log("Occupancy  " . $_POST['occupancy'],0);
            
            if ($value=="25") {
        
                $where .=" AND B.current_occupancy BETWEEN '$pos1'  AND '$pos2'";
            }
            
            if ($value=="50") {
        
                $where .=" AND B.current_occupancy BETWEEN '$pos2'  AND '$pos3'";
                
            }
        
            if ($value=="75") {
                
                 $where .=" AND B.current_occupancy BETWEEN '$pos3'  AND '$pos4'";
                        
            }
            
            if ($value=="100") {
                        
                 $where .=" AND B.current_occupancy > $pos5";
                                
            }
            break;
        }//end switch
    }//end if
}//end foreach

$count_sql = "SELECT COUNT(bill_id) FROM bills";

 $total = dbFetchCell($count_sql, $param);
  
if (empty($total)) {
    $total = 0;
} 

//error_log("Start Date " . $startdate  . "End Date " . $enddate);
 
$str_startdate=date('Y-m-d',$startdate);
$str_enddate=date('Y-m-d',$enddate);




//error_log("Start Date Str" . $str_startdate  . "End Date Str" . $str_enddate);

$select="SELECT B.bill_id,B.bill_name,B.current_occupancy as percent,
            D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
            ports.port_id,T.tselregion_name,D.metro_port,D.metro_name,D.olt_ip,D.olt_port,
            D.hostname,D.ont_sn,D.vlan2g,D.vlan3g,D.sto,D.latitude,D.longitude,D.tahundeploy,D.dataservices,
            COALESCE(B.rate_95th_out,0) as rate_95th_out,
            COALESCE(B.rate_95th,0) as rate_95th,

            COALESCE(B.bill_cdr) as bill_cdr, COALESCE(B.rate_95th_in,0) as rate_95th_in,
            B.occupancy_day as day_high,B.occupancy_week as week_high,B.occupancy_month as month_high";
                
 $from =" FROM bills B
        INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
        INNER JOIN  ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        LEFT JOIN witel ON D.witel_id=witel.witel_id
        LEFT JOIN tselregion T ON D.tselregion_id=T.tselregion_id";          

$order_by=" ORDER BY B.current_occupancy DESC LIMIT $total";          
     
$sql = $select .$from .$where . $order_by;

    $csv[] = array(
    'TSEL_Region',
    'Site_ID',
    'Site_Name',
    'Bandwidth_Allocated',
    'Current_Occupancy (%)',
    'Today High(%)',
    'This Week High(%)',
    'This Month High(%)',
    'ONT IP',
    'ONT SN',
    'OLT Port',
    'OLT IP',
    'Metro Port',
    'Metro Name',
    'Vlan 2G',
    'Vlan 3G',
    'STO',
    'Latitude',
    'Longitude',
    'Measurement Date'
 );
 
   foreach (dbFetchRows($sql, $param) as $occupancy) {
   
   $paramstring=join(", ", $param); 
     
   $datemeasurement = date('d-m-Y h:i');
  
   $bill_id= $occupancy['bill_id'];
  
   $url_witel = generate_url(array('page'=>'occupancy-overview','witel_id'=>$occupancy['witel_id']));

   $site_name=$occupancy['site_name'];

   $tselregion=$occupancy['tselregion_name']; 

    
  
   $hostname= $occupancy['hostname'];
   $site_id = $occupancy['site_id'];
 
   $allowed    = format_bytes_billing_short($occupancy['bill_cdr']).'bps';
   $in         = format_si($occupancy['rate_95th_in']).'bps';
   $out        = format_si($occupancy['rate_95th_out']).'bps';
   
   $current=round($occupancy['percent'],2);

   $day_high=round($occupancy['day_high'],2);
  
   $month_high=round($occupancy['month_high'],2);
  
   $week_high=round($occupancy['week_high'],2);
   
   $ont_sn=$occupancy['ont_sn'];
   $metro_name=$occupancy['metro_name'];
   $metro_port=$occupancy['metro_port'];

   $olt_port=$occupancy['olt_port'];
   $olt_ip=$occupancy['olt_ip'];
   
    $vlan2g=$occupancy['vlan2g'];
    $vlan3g=$occupancy['vlan3g'];
    $sto=$occupancy['sto'];
    $lat=$occupancy['latitude'];
    $long=$occupancy['longitude'];


 
     
 
    $csv[] = array(
        $tselregion,
        $site_id,
        $site_name,       
        $allowed,
        $current,
        $day_high,
        $week_high,
        $month_high,
        $hostname,
        $ont_sn,
        $olt_port,
        $olt_ip,
        $metro_port,
        $metro_name,
        $vlan2g,
        $vlan3g,
        $sto,
        $lat,
        $long,
        $datemeasurement,

   );

} //end foreach