<?php

$param = array();


if (!isset($vars['period'])) {
    $vars['period']='';
}



$where = '';

foreach ($vars as $var => $value) {
    if ($value != '') {
        switch ($var) {
            case 'hostname':
                $where  .= ' AND D.hostname LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'location':
                $where  .= ' AND D.location LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'device_id':
                $where  .= ' AND D.device_id = ?';
                $param[] = $value;
                break;

            case 'witel_id': 
                $where = 'AND D.witel_id = ?';
                $param[] =  $value;
                break;

            case 'site_name': 
                $where = 'AND D.site_name LIKE ?';
                $param[] =  '%' . $value . '%';
                break;

            
            case 'deleted':
            case 'ignore':
                if ($value == 1) {
                    $where .= ' AND (I.ignore = 1 OR D.ignore = 1) AND I.deleted = 0';
                }
                break;

            case 'disable':
            case 'ifSpeed':
                if (is_numeric($value)) {
                    $where  .= " AND I.$var = ?";
                    $param[] = $value;
                }
                break;

            case 'ifType':
                $where  .= " AND I.$var = ?";
                $param[] = $value;
                break;

            case 'ifAlias':
            case 'port_descr_type':
                $where  .= " AND I.$var LIKE ?";
                $param[] = '%'.$value.'%';
                break;

            case 'errors':
                if ($value == 1) {
                    $where .= " AND (I.`ifInErrors_delta` > '0' OR I.`ifOutErrors_delta` > '0')";
                }
                break;

            case 'state':
                if ($value == 'down') {
                    $where  .= 'AND I.ifAdminStatus = ? AND I.ifOperStatus = ?';
                    $param[] = 'up';
                    $param[] = 'down';
                } elseif ($value == 'up') {
                    $where  .= "AND I.ifAdminStatus = ? AND I.ifOperStatus = ?  AND I.ignore = '0' AND D.ignore='0' AND I.deleted='0'";
                    $param[] = 'up';
                    $param[] = 'up';
                } elseif ($value == 'admindown') {
                    $where  .= 'AND I.ifAdminStatus = ? AND D.ignore = 0';
                    $param[] = 'down';
                }
                break;

            case 'period' :
                if ($value=='') {
                    //$prev = !empty($_POST['period']) && ($_POST['period'] == 'prev');
                }
        }//end switch
    }//end if
}//end foreach

//$query = 'SELECT * FROM `ports` AS I, `devices` AS D WHERE I.device_id = D.device_id '.$where.' '.$query_sort;

//Bill query

//$query="SELECT B.*,D.device_id,D.site_name,D.site_id,D.sto,D.vlan2g,D.vlan3g,D.hostname,
//        IF(B.bill_type = 'CDR', bill_cdr, bill_quota) AS bill_allowed
//        FROM `bills` AS B
//     INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
//     INNER JOIN  ports ON bill_ports.port_id=ports.port_id
//     INNER JOIN `devices` AS D ON ports.device_id=D.device_id" . $where;

   $query = "SELECT bills.*,ROUND((bills.rate_95th/bills.bill_cdr)*100,2)  AS percent_cdr, ROUND((bills.total_data/bills.bill_quota)*100,2)  AS percent_quota,

                      D.device_id,D.site_name,D.site_id,D.sto,D.vlan2g,D.vlan3g,D.hostname,D.location,
                      D.metro_name,D.metro_port,D.metro_ip,D.olt_name,D.olt_port,D.olt_ip,D.ont_ip,D.ont_sn,
                      witel.witelname,
                      ports.port_id,
                      IF(bills.bill_type = 'CDR', bill_cdr, bill_quota) AS bill_allowed
            FROM `bills`
            INNER JOIN bill_ports ON bills.bill_id=bill_ports.bill_id
            INNER JOIN  ports ON bill_ports.port_id=ports.port_id
            INNER JOIN devices AS D ON ports.device_id=D.device_id 
            LEFT JOIN witel ON D.witel_id=witel.witel_id" . $where;

$row = 1;

list($format, $subformat) = explode('_', $vars['format']);

$bills = dbFetchRows($query, $param);

switch ($vars['sort']) {
    case 'traffic':
        $ports = array_sort($ports, 'ifOctets_rate', SORT_DESC);
        break;

    case 'traffic_in':
        $ports = array_sort($ports, 'ifInOctets_rate', SORT_DESC);
        break;

    case 'traffic_out':
        $ports = array_sort($ports, 'ifOutOctets_rate', SORT_DESC);
        break;

    case 'packets':
        $ports = array_sort($ports, 'ifUcastPkts_rate', SORT_DESC);
        break;

    case 'packets_in':
        $ports = array_sort($ports, 'ifInUcastOctets_rate', SORT_DESC);
        break;

    case 'packets_out':
        $ports = array_sort($ports, 'ifOutUcastOctets_rate', SORT_DESC);
        break;

    case 'errors':
        $ports = array_sort($ports, 'ifErrors_rate', SORT_DESC);
        break;

    case 'speed':
        $ports = array_sort($ports, 'ifSpeed', SORT_DESC);
        break;

    case 'port':
        $ports = array_sort($ports, 'ifDescr', SORT_ASC);
        break;

    case 'media':
        $ports = array_sort($ports, 'ifType', SORT_ASC);
        break;

    case 'descr':
        $ports = array_sort($ports, 'ifAlias', SORT_ASC);
        break;

    case 'device':
    default:
        $ports = array_sort($ports, 'hostname', SORT_ASC);
}//end switch


$csv[] = array(
    'Witel',
    'Site_ID',
    'Site_Name',
    'Hostname',
    'Allowed',
    'In-Bound',
    'Out-Bound',
    '95th_Percentile',
    'Occupancy',
    'Metro_Name',
    'Metro_Port',
    'Metro_IP',
    'OLT_Name',
    'OLT_Port',
    'OLT_IP',
    'ONT_IP',
     'ONT_SN',
     'VLAN_2G',
     'VLAN_3G',
);

foreach ($bills as $bill) {


        $day_data = getDates($bill['bill_day']);
        $datefrom = $day_data['0'];
        $dateto   = $day_data['1'];
    
        $rate_95th    = format_si($bill['rate_95th']) . 'bps';
        $dir_95th     = $bill['dir_95th'];
        $total_data   = format_bytes_billing($bill['total_data']);
        $rate_average = $bill['rate_average'];
        $used95th     = format_bi($bill['rate_95th']).'bps';
        $notes        = $bill['bill_notes'];
        $hostname  = $bill['hostname'];
        $bill_last_calc = $bill['bill_last_calc'];  
        $site_name      = $bill['site_name'];
        $site_id        = $bill['site_id'];

        $sto            = $bill['sto'];

        $vlan2g         = $bill['vlan2g'];
        $vlan3g         = $bill['vlan3g'];

        $bill_name = $bill['bill_name'];

        $metro_name=$bill['metro_name'];

        $metro_port=$bill['metro_port'];

        $metro_ip=$bill['metro_ip'];

        $olt_name=$bill['olt_name'];

        $olt_ip=$bill['olt_ip'];

        $olt_port=$bill['olt_port'];

        $ont_ip=$bill['ont_ip'];
        $ont_sn=$bill['ont_sn'];

        $witel_name= "\"" . $bill['witelname'] . "\"";
        
        $measurement_date = strftime('%F', strtotime($datefrom)) . " to " . strftime('%F', strtotime($dateto));

    if (strtolower($bill['bill_type']) == 'cdr') {
        $type       = 'CDR';
        $allowed    = format_si($bill['bill_allowed']).'bps';
        $in         = format_si($bill['rate_95th_in']).'bps';
        $out        = format_si($bill['rate_95th_out']).'bps';
        if (!$prev) {
            $percent    = round((($bill['rate_95th'] / $bill['bill_allowed']) * 100), 2);
            $overuse    = ($bill['rate_95th'] - $bill['bill_allowed']);
        }
        
        $overuse_formatted    = format_si($overuse).'bps';
        $used                 = $rate_95th;
        $tmp_used             = $bill['rate_95th'];
        $rate_95th            = $rate_95th;
    
    } elseif (strtolower($bill['bill_type']) == 'quota') {
        $type       = 'Quota';
        $allowed    = format_bytes_billing($bill['bill_allowed']);
        if (!empty($prev)) {
            $in  = format_bytes_billing($bill['traf_in']);
            $out = format_bytes_billing($bill['traf_out']);
        } else {
            $in  = format_bytes_billing($bill['total_data_in']);
            $out = format_bytes_billing($bill['total_data_out']);
        }
        if (!$prev) {
            $percent    = round((($bill['total_data'] / ($bill['bill_allowed'])) * 100), 2);
            $overuse    = ($bill['total_data'] - $bill['bill_allowed']);
        }
    }
        $overuse_formatted    = format_bytes_billing($overuse);
        $used                 = $total_data;
        $tmp_used             = $bill['total_data'];
        $total_data           = $total_data;

        $predicted = format_bytes_billing(getPredictedUsage($bill['bill_day'], $tmp_used));
    

       //Generate CSV
        $csv[]            = array(
            $witel_name,
            $site_id,
            $site_name,
            $hostname,
            $allowed,
            $in,
            $out,
            $rate_95th,
            $percent,
            $metro_name,
            $metro_port,
            $metro_ip,
            $olt_name,
            $olt_port,
            $olt_ip,
            $ont_ip,
            $ont_sn,
            $vlan2g,
            $vlan3g,
         
        );

}
    

