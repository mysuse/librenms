<?php

$wheres = array();
$param = array();

$this_month = mres($vars['bd_month']);
$this_year = mres($vars['bd_year']);

    if (!empty($vars['bd_month'])) {
        
        $this_month = mres($vars['bd_month']);
        
    }
    
    else {
        $this_month =date("n");
        
    }
    
    if (!empty($vars['bd_year'])) {
    
        $this_year = mres($vars['bd_year']);
        
    }
    
    else {
        $this_year =date("Y");
    }

$where = " WHERE b.bill_id> 0 ";

foreach ($vars as $var => $value) {
    if ($value != '') {
        switch ($var) {
            case 'site_id':
                $where  .= ' AND D.site_id LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'site_name':
                $where  .= ' AND D.site_name LIKE ?';
                $param[] = '%'.$value.'%';
                break;

            case 'witel_id':
                $where  .= ' AND D.witel_id = ?';
                $param[] = $value;
                break;

            case 'witel_id':
                $where  .= ' AND D.witel_id = ?';
                $param[] = $value;
                break;

        }//end switch
    }//end if
}//end foreach

$select="SELECT b.bill_id,b.bill_cdr,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,T.tselregion_name,
      COALESCE(bd1.percentage,0) as _1,
      COALESCE(bd2.percentage,0) as _2,
      COALESCE(bd3.percentage,0) as _3,
      COALESCE(bd4.percentage,0) as _5,
      COALESCE(bd5.percentage,0) as _5,
      COALESCE(bd6.percentage,0) as _6,
      COALESCE(bd7.percentage,0) as _7,
      COALESCE(bd8.percentage,0) as _8,
      COALESCE(bd9.percentage,0) as _9,
      COALESCE(bd10.percentage,0) as _10,
      COALESCE(bd11.percentage,0) as _11,
      COALESCE(bd12.percentage,0) as _12,
      COALESCE(bd13.percentage,0) as _13,
      COALESCE(bd14.percentage,0) as _14,
      COALESCE(bd15.percentage,0) as _15,
      COALESCE(bd16.percentage,0) as _16,
      COALESCE(bd17.percentage,0) as _17,
      COALESCE(bd18.percentage,0) as _18,
      COALESCE(bd19.percentage,0) as _19,
      COALESCE(bd20.percentage,0) as _20,
      COALESCE(bd21.percentage,0) as _21,
	  COALESCE(bd22.percentage,0) as _22,
      COALESCE(bd23.percentage,0) as _23,
      COALESCE(bd24.percentage,0) as _24,
      COALESCE(bd25.percentage,0) as _25,
      COALESCE(bd26.percentage,0) as _26,
      COALESCE(bd27.percentage,0) as _27,
      COALESCE(bd28.percentage,0) as _28,
      COALESCE(bd29.percentage,0) as _29,
	  COALESCE(bd30.percentage,0) as _30,
      COALESCE(bd31.percentage,0) as _31";
      
    $from =" FROM  bills b
     
     INNER JOIN bill_ports ON b.bill_id=bill_ports.bill_id
     
     INNER JOIN ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     LEFT JOIN tselregion T ON D.tselregion_id=T.tselregion_id
     LEFT JOIN witel ON D.witel_id=witel.witel_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='1' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd1 ON b.bill_id=bd1.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='2' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd2 ON b.bill_id=bd2.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='3' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd3 ON b.bill_id=bd3.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='4' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd4 ON b.bill_id=bd4.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='5' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd5 ON b.bill_id=bd5.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='6' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd6 ON b.bill_id=bd6.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='7' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd7 ON b.bill_id=bd7.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='8' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd8 ON b.bill_id=bd8.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='9' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd9 ON b.bill_id=bd9.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='10' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd10 ON b.bill_id=bd10.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='11' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd11 ON b.bill_id=bd11.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='12' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd12 ON b.bill_id=bd12.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='13' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd13 ON b.bill_id=bd13.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='14' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd14 ON b.bill_id=bd14.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='15' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd15 ON b.bill_id=bd15.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='16' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd16 ON b.bill_id=bd16.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='17' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd17 ON b.bill_id=bd17.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='18' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd18 ON b.bill_id=bd18.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='19' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd19 ON b.bill_id=bd19.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='20' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd20 ON b.bill_id=bd20.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='21' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd21 ON b.bill_id=bd21.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='22' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd22 ON b.bill_id=bd22.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='23' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd23 ON b.bill_id=bd23.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='24' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd24 ON b.bill_id=bd24.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='25' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd25 ON b.bill_id=bd25.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='26' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd26 ON b.bill_id=bd26.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='27' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd27 ON b.bill_id=bd26.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='28' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd28 ON b.bill_id=bd28.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='29' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd29 ON b.bill_id=bd29.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='30' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd30 ON b.bill_id=bd30.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='31' AND bd.period_m='" . $this_month . "' AND bd.period_y='" . $this_year . "') bd31 ON b.bill_id=bd31.bill_id";

//if (sizeof($wheres) > 0) {
//    $query .= " WHERE " . implode(' AND ', $wheres) . "\n";
//}


 
 $sql = $select . $from . $query . $where;

 $monthName = date('F', mktime(0, 0, 0, $this_month, 10));
 
 $numberofDays = cal_days_in_month(CAL_GREGORIAN, $this_month, $this_year); // Number of Days

 $count_sql = "SELECT COUNT(bill_id) FROM bills";

 $total = dbFetchCell($count_sql, $param);
  
if (empty($total)) {
    $total = 0;
} 

if ($numberofDays==28) {
    $csv[] = array(
        'TSEL_Region',
        'Site_ID',
        'Site_Name',
        'Year',
        'Month',
        'Bandwidth_Allocated',
        'Day_1 (%)',
        'Day_2 (%)',
        'Day_3 (%)',
        'Day_4 (%)',
        'Day_5 (%)',
        'Day_6 (%)',
        'Day_7 (%)',
        'Day_8 (%)',
        'Day_9 (%)',
        'Day_10 (%)',
        'Day_11 (%)',
        'Day_12 (%)',
        'Day_13 (%)',
        'Day_14 (%)',
        'Day_15 (%)',
        'Day_16 (%)',
        'Day_17 (%)',
        'Day_18 (%)',
        'Day_19 (%)',
        'Day_20 (%)',
        'Day_21 (%)',
        'Day_22 (%)',
        'Day_23 (%)',
        'Day_24 (%)',
        'Day_25 (%)',
        'Day_26 (%)',
        'Day_27 (%)',
        'Day_28 (%)'
        
        
     );
    
}
elseif ($numberofDays==29) {
    $csv[] = array(
        'TSEL_Region',
        'Site_ID',
        'Site_Name',
        'Year',
        'Month',
        'Bandwidth_Allocated',
        'Day_1 (%)',
        'Day_2 (%)',
        'Day_3 (%)',
        'Day_4 (%)',
        'Day_5 (%)',
        'Day_6 (%)',
        'Day_7 (%)',
        'Day_8 (%)',
        'Day_9 (%)',
        'Day_10 (%)',
        'Day_11 (%)',
        'Day_12 (%)',
        'Day_13 (%)',
        'Day_14 (%)',
        'Day_15 (%)',
        'Day_16 (%)',
        'Day_17 (%)',
        'Day_18 (%)',
        'Day_19 (%)',
        'Day_20 (%)',
        'Day_21 (%)',
        'Day_22 (%)',
        'Day_23 (%)',
        'Day_24 (%)',
        'Day_25 (%)',
        'Day_26 (%)',
        'Day_27 (%)',
        'Day_28 (%)',
        'Day_29 (%)'
     );
    
}

elseif ($numberofDays==30) {
    $csv[] = array(
        'TSEL_Region',
        'Site_ID',
        'Site_Name',
        'Year',
        'Month',
        'Bandwidth_Allocated',
        'Day_1 (%)',
        'Day_2 (%)',
        'Day_3 (%)',
        'Day_4 (%)',
        'Day_5 (%)',
        'Day_6 (%)',
        'Day_7 (%)',
        'Day_8 (%)',
        'Day_9 (%)',
        'Day_10 (%)',
        'Day_11 (%)',
        'Day_12 (%)',
        'Day_13 (%)',
        'Day_14 (%)',
        'Day_15 (%)',
        'Day_16 (%)',
        'Day_17 (%)',
        'Day_18 (%)',
        'Day_19 (%)',
        'Day_20 (%)',
        'Day_21 (%)',
        'Day_22 (%)',
        'Day_23 (%)',
        'Day_24 (%)',
        'Day_25 (%)',
        'Day_26 (%)',
        'Day_27 (%)',
        'Day_28 (%)',
        'Day_29 (%)',
        'Day_30 (%)'
     );
    
}


else {
    $csv[] = array(
        'TSEL_Region',
        'Site_ID',
        'Site_Name',
        'Year',
        'Month',
        'Bandwidth_Allocated',
        'Day_1 (%)',
        'Day_2 (%)',
        'Day_3 (%)',
        'Day_4 (%)',
        'Day_5 (%)',
        'Day_6 (%)',
        'Day_7 (%)',
        'Day_8 (%)',
        'Day_9 (%)',
        'Day_10 (%)',
        'Day_11 (%)',
        'Day_12 (%)',
        'Day_13 (%)',
        'Day_14 (%)',
        'Day_15 (%)',
        'Day_16 (%)',
        'Day_17 (%)',
        'Day_18 (%)',
        'Day_19 (%)',
        'Day_20 (%)',
        'Day_21 (%)',
        'Day_22 (%)',
        'Day_23 (%)',
        'Day_24 (%)',
        'Day_25 (%)',
        'Day_26 (%)',
        'Day_27 (%)',
        'Day_28 (%)',
        'Day_29 (%)',
        'Day_30 (%)',
        'Day_31 (%)'
     );
    
}


   foreach (dbFetchRows($sql, $param) as $occupancy) {
   
   $bill_id= $occupancy['bill_id'];
    
   $hostname= $occupancy['hostname'];
   $site_id = $occupancy['site_id'];    
   $site_name= $occupancy['site_name'];     
   $witelname= $occupancy['witelname']; 
   $tselregion=$occupancy['tselregion_name']; 
   $bandwidth=format_si($occupancy['bill_cdr'],0,0);

   $d1=round($occupancy['_1'],2);
   $d2=round($occupancy['_2'],2);
   $d3=round($occupancy['_3'],2);
   $d4 =round($occupancy['_4'],2);
   $d5 =round($occupancy['_5'],2);
   $d6 =round($occupancy['_6'],2);
   $d7 =round($occupancy['_7'],2);
   $d8 =round($occupancy['_8'],2);
   $d9 =round($occupancy['_9'],2);
   $d10 =round($occupancy['_10'],2);
   $d11 =round($occupancy['_11'],2);
   $d12 =round($occupancy['_12'],2);
   $d13 =round($occupancy['_13'],2);
   $d14 =round($occupancy['_14'],2);
   $d15 =round($occupancy['_15'],2);
   $d16 =round($occupancy['_16'],2);
   $d17 =round($occupancy['_17'],2);
   $d18 =round($occupancy['_18'],2);
   $d19 =round($occupancy['_19'],2);
   $d20 =round($occupancy['_20'],2);
   $d21 =round($occupancy['_21'],2);
   $d22 =round($occupancy['_22'],2);
   $d23 =round($occupancy['_23'],2);
   $d24 =round($occupancy['_24'],2);
   $d25 =round($occupancy['_25'],2);
   $d26 =round($occupancy['_26'],2);
   $d27 =round($occupancy['_27'],2);
   $d28 =round($occupancy['_28'],2);
   $d29 =round($occupancy['_29'],2);
   $d30 =round($occupancy['_30'],2);
   $d31 =round($occupancy['_31'],2);

   if ($numberofDays==28) {
    $csv[] = array(
        $tselregion,
        $site_id,
        $site_name, 
        $this_year,
        $monthName,
        $bandwidth,
        $d1,
        $d2,
        $d3,
        $d4,
        $d5,
        $d6,
        $d7,
        $d8,
        $d9,
        $d10,
        $d11,
        $d12,
        $d13,
        $d14,
        $d15,
        $d16,
        $d17,
        $d18,
        $d19,
        $d20,
        $d21,
        $d22,
        $d23,
        $d24,
        $d25,
        $d26,
        $d27,
        $d28,
   );

   }
   elseif ($numberofDays==29) {
    $csv[] = array(
        $tselregion,
        $site_id,
        $site_name, 
        $this_year,
        $monthName,
        $bandwidth,
        $d1,
        $d2,
        $d3,
        $d4,
        $d5,
        $d6,
        $d7,
        $d8,
        $d9,
        $d10,
        $d11,
        $d12,
        $d13,
        $d14,
        $d15,
        $d16,
        $d17,
        $d18,
        $d19,
        $d20,
        $d21,
        $d22,
        $d23,
        $d24,
        $d25,
        $d26,
        $d27,
        $d28,
        $d29,
   );

   }
   elseif ($numberofDays==30) {
    $csv[] = array(
        $tselregion,
        $site_id,
        $site_name, 
        $this_year,
        $monthName,
        $bandwidth,
        $d1,
        $d2,
        $d3,
        $d4,
        $d5,
        $d6,
        $d7,
        $d8,
        $d9,
        $d10,
        $d11,
        $d12,
        $d13,
        $d14,
        $d15,
        $d16,
        $d17,
        $d18,
        $d19,
        $d20,
        $d21,
        $d22,
        $d23,
        $d24,
        $d25,
        $d26,
        $d27,
        $d28,
        $d29,
        $d30,

   );
    
}
else {
    $csv[] = array(
        $tselregion,
        $site_id,
        $site_name, 
        $this_year,
        $monthName,
        $bandwidth,
        $d1,
        $d2,
        $d3,
        $d4,
        $d5,
        $d6,
        $d7,
        $d8,
        $d9,
        $d10,
        $d11,
        $d12,
        $d13,
        $d14,
        $d15,
        $d16,
        $d17,
        $d18,
        $d19,
        $d20,
        $d21,
        $d22,
        $d23,
        $d24,
        $d25,
        $d26,
        $d27,
        $d28,
        $d29,
        $d30,
        $d31,
   );
    
}
    
} //end foreach


    