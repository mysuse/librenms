<?php

?>
<div class="modal fade bs-example-modal-sm" id="create-witel" tabindex="-1" role="dialog" aria-labelledby="Create" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="Create">Add Witel</h4>
        </div>
        <div class="modal-body">
            <form method="post" role="form" action="witel/" class="form-horizontal alerts-form">
                <input type="hidden" name="addwitel" value="yes" />
                <input type="text" name="witelname" id="witelname" title="Nama Witel" class="form-control input-sm" <?php if (strlen($vars['witelname'])) {
                  echo('value="'.$vars['witelname'].'"');
} ?> placeholder="Nama Witel" />

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-4">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Add Witel</button>
                  </div>
                </div>

            </form>
        </div>
      </div>
    </div>
</div>