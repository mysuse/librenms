CREATE OR REPLACE VIEW v_listbilldailybysite

AS

SELECT B.bill_id,B.bill_name,bd.bill_cdr,bd.rate_95th_in,bd.rate_95th_out,bd.rate_95th,MAX(bd.percentage) as percentage,bd.bill_day,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
    ports.port_id
FROM bills B
   INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
     INNER JOIN  ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     INNER JOIN witel ON D.witel_id=witel.witel_id
     INNER JOIN bill_poller bd ON bd.bill_id=B.bill_id
     GROUP by B.bill_id,DAY(bd.bill_day),MONTH(bd.bill_day),YEAR(bd.bill_day)  
