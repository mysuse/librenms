SELECT DISTINCT w.witel_id,
        w.witelname,
        COALESCE(bd1.numofnodb,0) as _25,
        COALESCE(bd2.numofnodb,0) as _2550,
        COALESCE(bd3.numofnodb,0) as _5075,
        COALESCE(bd4.numofnodb,0) as _75,
        DAY(NOW()) as period_d,
        MONTH(NOW()) as period_m,
        YEAR(NOW()) as period_y
        
    FROM witel w 
		LEFT JOIN devices d ON d.witel_id = w.witel_id
        LEFT JOIN ports p ON p.device_id = d.device_id
        LEFT JOIN bill_ports bp ON bp.port_id = p.port_id
        LEFT JOIN bills b ON b.bill_id=bp.bill_id      
        LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id,b1.period_d,b1.period_m,b1.period_y FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage<=25 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
			 ) bd1 ON w.witel_id=bd1.witel_id
		 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>25 AND b1.percentage<=50 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
			 ) bd2 ON w.witel_id=bd2.witel_id
		 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>50 AND b1.percentage<=75 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
		 ) bd3 ON w.witel_id=bd3.witel_id

		LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>75 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
		 ) bd4 ON w.witel_id=bd4.witel_id



       