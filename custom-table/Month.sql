CREATE TABLE `month` (
  `month_id` int(11) NOT NULL,
  `month_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`month_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('1', 'January');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('2', 'February');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('3', 'March');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('4', 'April');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('5', 'May');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('6', 'June');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('7', 'July');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('8', 'August');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('9', 'September');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('10', 'October');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('11', 'November');
INSERT INTO `librenms`.`month` (`month_id`, `month_name`) VALUES ('12', 'December');
