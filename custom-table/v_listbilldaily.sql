CREATE OR REPLACE VIEW v_listbilldaily AS
SELECT 
        bd.bill_id AS bill_id,
        bd.bill_cdr AS bill_cdr,
        YEAR(bd.bill_day) AS bd_year,
        MONTH(bd.bill_day) AS bd_month,
        DAYOFMONTH(bd.bill_day) AS bill_date,
        MAX(bd.percentage) AS percentage
     FROM
        bill_poller bd
    GROUP BY DAY(bd.bill_day),MONTH(bd.bill_day),YEAR(bd.bill_day) , bd.bill_id