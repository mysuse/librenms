CREATE TABLE `bill_witel_daily` (
  `bill_witeldaily_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `witel_id` INT NOT NULL COMMENT '',
  `bill_day` TIMESTAMP NULL COMMENT '',
  `percent25`INT DEFAULT 0  COMMENT '',
  `percent2550`INT DEFAULT 0  COMMENT '',
  `percent5075`INT DEFAULT 0  COMMENT '',
  `percent75`INT DEFAULT 0  COMMENT '',
  `period_d` INT NOT NULL DEFAULT 0 COMMENT '',
  `period_m` INT NOT NULL DEFAULT 0 COMMENT '',
  `period_y` INT NOT NULL DEFAULT 0 COMMENT '',
  `last_updated` TIMESTAMP COMMENT '',
PRIMARY KEY (`bill_witeldaily_id`) COMMENT '');
