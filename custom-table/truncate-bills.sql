use nms;
truncate bill_daily;
truncate bill_monthly;
truncate bill_weekly;
truncate bill_yearly;
truncate bill_poller;
truncate bill_data;
truncate bill_history;
truncate bill_port_counters;

UPDATE bills set rate_95th_in=0,rate_95th_out=0,rate_95th=0,total_data=0,total_data_in=0,total_data_out=0,rate_average=0,
rate_average_in=0,rate_average_out=0,current_occupancy=0,occupancy_day=0,occupancy_week=0,occupancy_month=0,occupancy_year=0

WHERE bills.bill_id>0;


