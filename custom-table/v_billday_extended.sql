CREATE VIEW v_billday_extended AS 
    SELECT 
        v_listbilldaily.bill_id AS bill_id,
        v_listbilldaily.bd_year AS bd_year,
        v_listbilldaily.bd_month AS bd_month,
        (CASE
            WHEN (v_listbilldaily.bill_date = 1) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 1,
        (CASE
            WHEN (v_listbilldaily.bill_date = 2) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 2,
        (CASE
            WHEN (v_listbilldaily.bill_date = 3) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 3,
        (CASE
            WHEN (v_listbilldaily.bill_date = 4) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 4,
        (CASE
            WHEN (v_listbilldaily.bill_date = 5) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 5,
        (CASE
            WHEN (v_listbilldaily.bill_date = 6) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 6,
        (CASE
            WHEN (v_listbilldaily.bill_date = 7) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 7,
        (CASE
            WHEN (v_listbilldaily.bill_date = 8) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 8,
        (CASE
            WHEN (v_listbilldaily.bill_date = 9) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 9,
        (CASE
            WHEN (v_listbilldaily.bill_date = 10) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 10,
        (CASE
            WHEN (v_listbilldaily.bill_date = 11) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 11,
        (CASE
            WHEN (v_listbilldaily.bill_date = 12) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 12,
        (CASE
            WHEN (v_listbilldaily.bill_date = 13) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 13,
        (CASE
            WHEN (v_listbilldaily.bill_date = 14) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 14,
        (CASE
            WHEN (v_listbilldaily.bill_date = 15) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 15,
        (CASE
            WHEN (v_listbilldaily.bill_date = 16) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 16,
        (CASE
            WHEN (v_listbilldaily.bill_date = 17) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 17,
        (CASE
            WHEN (v_listbilldaily.bill_date = 18) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 18,
        (CASE
            WHEN (v_listbilldaily.bill_date = 19) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 19,
        (CASE
            WHEN (v_listbilldaily.bill_date = 20) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 20,
        (CASE
            WHEN (v_listbilldaily.bill_date = 21) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 21,
        (CASE
            WHEN (v_listbilldaily.bill_date = 22) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 22,
        (CASE
            WHEN (v_listbilldaily.bill_date = 23) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 23,
        (CASE
            WHEN (v_listbilldaily.bill_date = 24) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 24,
        (CASE
            WHEN (v_listbilldaily.bill_date = 25) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 25,
        (CASE
            WHEN (v_listbilldaily.bill_date = 26) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 26,
        (CASE
            WHEN (v_listbilldaily.bill_date = 27) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 27,
        (CASE
            WHEN (v_listbilldaily.bill_date = 28) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 28,
        (CASE
            WHEN (v_listbilldaily.bill_date = 29) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 29,
        (CASE
            WHEN (v_listbilldaily.bill_date = 30) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 30,
        (CASE
            WHEN (v_listbilldaily.bill_date = 31) THEN v_listbilldaily.percentage
            ELSE 0
        END) AS 31
    FROM
       v_listbilldaily