DELETE FROM bill_ports WHERE bill_Ports_id IN(SELECT l.bill_ports_id FROM (
SELECT d.hostname,bp.bill_ports_id,p.port_id,COUNT(b.bill_id) as bill
    FROM bill_ports bp JOIN ports p ON p.port_id=bp.port_id
    JOIN bills b ON bp.bill_id=b.bill_id
    JOIN devices d ON d.device_id=p.device_id
    GROUP BY d.hostname
) l WHERE l. bill>1)
