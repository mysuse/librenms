ALTER TABLE `bills` ADD `current_occupancy` double(11,2) NOT NULL;
ALTER TABLE `bills` ADD `occupancy_last_calc` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:01';