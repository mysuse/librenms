SELECT B.bill_id,B.bill_name,
            D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
            ports.port_id,COALESCE(bp.percentage,0) as percent,T.tselregion_name,
            COALESCE(bp.rate_95th_out,0) as rate_95th_out,
            COALESCE(bp.rate_95th,0) as rate_95th,
            bp.bill_day,
            COALESCE(bp.bill_cdr) as bill_cdr, COALESCE(bp.rate_95th_in,0) as rate_95th_in,
            COALESCE(bd.percentage,0) as day_high,
            COALESCE(bm.percentage,0) as month_high,
            COALESCE(byy.percentage,0) as year_high
                
 FROM bills B
        INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
        INNER JOIN  ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        LEFT JOIN (SELECT b.* FROM bill_daily b  WHERE b.period_d=DAY(NOW()) AND b.period_m=MONTH(NOW()) AND b.period_y=YEAR(NOW())) bd ON B.bill_id=bd.bill_id
        LEFT JOIN (SELECT b.* FROM bill_monthly b  WHERE b.period_m=MONTH(NOW()) AND b.period_y=YEAR(NOW())) bm ON B.bill_id=bm.bill_id
        LEFT JOIN (SELECT b.* FROM bill_yearly b  WHERE  b.period_y=YEAR(NOW()))  byy ON B.bill_id=byy.bill_id
        LEFT JOIN witel ON D.witel_id=witel.witel_id
        LEFT JOIN tselregion T ON D.tselregion_id=T.tselregion_id
        LEFT JOIN (SELECT bl.* FROM bill_poller bl  WHERE bl.bill_day=(SELECT MAX(bill_day) FROM bill_poller)) bp ON bp.bill_id=B.bill_id
             
        
        ORDER BY bd.percentage DESC
       
        LIMIT 100