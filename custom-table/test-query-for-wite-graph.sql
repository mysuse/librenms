SELECT B.bill_id,B.bill_name,
            D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
            ports.port_id,
            bd.*
          
FROM bills B
        INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id
        INNER JOIN ports ON bill_ports.port_id=ports.port_id
        INNER JOIN devices AS D ON ports.device_id=D.device_id 
        INNER JOIN witel ON D.witel_id=witel.witel_id
        INNER JOIN bill_daily bd ON  B.bill_id=bd.bill_id

WHERE   bd.bill_day = (SELECT MAX(bill_day) FROM bill_daily where bill_daily.bill_id=B.bill_id)

GROUP BY B.bill_id
             
        