#Table Bill Monthly untuk menampung Max Occupancy Per Month

CREATE TABLE `bill_monthly` (
  `bill_monthly_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `bill_id` INT NOT NULL COMMENT '',
  `bill_day` TIMESTAMP COMMENT '',
  `bill_cdr` BIGINT NOT NULL DEFAULT 0 COMMENT '',
  `percentage` DOUBLE DEFAULT 0  COMMENT '',
  `period_m` INT NOT NULL DEFAULT 0 COMMENT '',
  `period_y` INT NOT NULL DEFAULT 0 COMMENT '',
  `last_updated` TIMESTAMP COMMENT '',
  PRIMARY KEY (`bill_monthly_id`) COMMENT '');
