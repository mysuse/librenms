ALTER TABLE `bills` ADD `occupancy_day` INT NOT NULL DEFAULT 0;
ALTER TABLE `bills` ADD `occupancy_week` INT NOT NULL DEFAULT 0;
ALTER TABLE `bills` ADD `occupancy_month` INT NOT NULL DEFAULT 0;
ALTER TABLE `bills` ADD `occupancy_year` INT NOT NULL DEFAULT 0;