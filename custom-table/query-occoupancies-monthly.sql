SELECT b.bill_id,b.bill_cdr,D.device_id,D.site_name,D.site_id,D.hostname,D.witel_id,witel.witelname,
      COALESCE(bd1.percentage,0) as _1,
      COALESCE(bd2.percentage,0) as _2,
      COALESCE(bd3.percentage,0) as _3,
      COALESCE(bd4.percentage,0) as _5,
      COALESCE(bd5.percentage,0) as _5,
      COALESCE(bd6.percentage,0) as _6,
      COALESCE(bd7.percentage,0) as _7,
      COALESCE(bd8.percentage,0) as _8,
      COALESCE(bd9.percentage,0) as _9,
      COALESCE(bd10.percentage,0) as _10,
      COALESCE(bd11.percentage,0) as _11,
      COALESCE(bd12.percentage,0) as _12,
      COALESCE(bd13.percentage,0) as _13,
      COALESCE(bd14.percentage,0) as _14,
      COALESCE(bd15.percentage,0) as _15,
      COALESCE(bd16.percentage,0) as _16,
      COALESCE(bd17.percentage,0) as _17,
      COALESCE(bd18.percentage,0) as _18,
      COALESCE(bd19.percentage,0) as _19,
      COALESCE(bd20.percentage,0) as _20,
      COALESCE(bd21.percentage,0) as _21,
	  COALESCE(bd22.percentage,0) as _22,
      COALESCE(bd23.percentage,0) as _23,
      COALESCE(bd24.percentage,0) as _24,
      COALESCE(bd25.percentage,0) as _25,
      COALESCE(bd26.percentage,0) as _26,
      COALESCE(bd27.percentage,0) as _27,
      COALESCE(bd28.percentage,0) as _28,
      COALESCE(bd29.percentage,0) as _29,
	  COALESCE(bd30.percentage,0) as _30,
      COALESCE(bd31.percentage,0) as _31
      
     FROM  bills b
     
     INNER JOIN bill_ports ON b.bill_id=bill_ports.bill_id
     
     INNER JOIN ports ON bill_ports.port_id=ports.port_id
     INNER JOIN devices AS D ON ports.device_id=D.device_id 
     INNER JOIN witel ON D.witel_id=witel.witel_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='1' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd1 ON b.bill_id=bd1.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='2' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd2 ON b.bill_id=bd2.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='3' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd3 ON b.bill_id=bd3.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='4' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd4 ON b.bill_id=bd4.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='5' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd5 ON b.bill_id=bd5.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='6' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd6 ON b.bill_id=bd6.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='7' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd7 ON b.bill_id=bd7.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='8' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd8 ON b.bill_id=bd8.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='9' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd9 ON b.bill_id=bd9.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='10' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd10 ON b.bill_id=bd10.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='11' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd11 ON b.bill_id=bd11.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='12' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd12 ON b.bill_id=bd12.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='13' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd13 ON b.bill_id=bd13.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='14' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd14 ON b.bill_id=bd14.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='15' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd15 ON b.bill_id=bd15.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='16' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd16 ON b.bill_id=bd16.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='17' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd17 ON b.bill_id=bd17.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='18' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd18 ON b.bill_id=bd18.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='19' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd19 ON b.bill_id=bd19.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='20' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd20 ON b.bill_id=bd20.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='21' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd21 ON b.bill_id=bd21.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='22' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd22 ON b.bill_id=bd22.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='23' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd23 ON b.bill_id=bd23.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='24' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd24 ON b.bill_id=bd24.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='25' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd25 ON b.bill_id=bd25.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='26' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd26 ON b.bill_id=bd26.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='27' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd27 ON b.bill_id=bd26.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='28' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd28 ON b.bill_id=bd28.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='29' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd29 ON b.bill_id=bd29.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='30' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd30 ON b.bill_id=bd30.bill_id
     LEFT JOIN (SELECT * FROM bill_daily bd WHERE bd.period_d='31' AND bd.period_m=MONTH(NOW()) AND bd.period_y=YEAR(NOW())) bd31 ON b.bill_id=bd31.bill_id
     
                        