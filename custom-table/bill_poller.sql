CREATE TABLE `bill_poller` (
  `bill_poller_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_cdr` bigint(20) DEFAULT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `percentage` double(11,2) NOT NULL,
  PRIMARY KEY (`bill_poller_id`));
