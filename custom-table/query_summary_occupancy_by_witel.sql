SELECT w.witel_id,w.witelname, 

(SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage<=25 AND v.witel_id=w.witel_id AND
v.bill_day>='2017-06-01 00:00' and v.bill_day<='2017-06-31 59:59'

) as _25,

(SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>25 AND v.percentage<=50 AND v.witel_id=w.witel_id AND
v.bill_day>='2017-06-01 00:00' and v.bill_day<='2017-06-31 59:59'

) as _25_50,

(SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>50 AND v.percentage<=75 AND v.witel_id=w.witel_id AND
v.bill_day>='2017-06-01 00:00' and v.bill_day<='2017-06-31 59:59'

) as _50_75,

(SELECT count(distinct(bill_id)) FROM v_listbilldailybysite v WHERE v.percentage>75 AND v.witel_id=w.witel_id AND
v.bill_day>='2017-06-01 00:00' and v.bill_day<='2017-06-31 59:59'

) as _50_75




FROM witel w
