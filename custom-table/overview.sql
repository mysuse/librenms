SELECT 
        w.witel_id,
        w.witelname,
        (SELECT count(distinct(bill_id)) FROM bill_daily bv WHERE bv.percentage<=25 AND bv.bill_id=b.bill_id AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW()) ) as _25

        
    FROM witel w 
		LEFT JOIN devices d ON d.witel_id = w.witel_id
        LEFT JOIN ports p ON p.device_id = d.device_id
        LEFT JOIN bill_ports bp ON bp.port_id = p.port_id
        LEFT JOIN bills b ON b.bill_id=bp.bill_id        
        LEFT JOIN (SELECT b1.* FROM bill_daily b1 WHERE b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW()) ) bd ON b.bill_id=bd.bill_id
