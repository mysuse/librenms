#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy Monthly ... \n\n";


foreach (dbFetchRows('SELECT * FROM `bills` ORDER BY `bill_id`') as $bill) {
        echo str_pad($bill['bill_id'].' '.$bill['bill_name'], 30)." \n";
   
        unset($class);
        unset($rate_data);
      
        $thismonthmax = getThisMonthHighest($bill['bill_id']);

        $thismonthexist = billMonthlyIsExist($bill['bill_id']);
        
        $now = dbFetchCell('SELECT NOW()');
        
        $month_day = "_" . date("j");         
        
        if ($thismonthmax['state']=='ok') {

            $bill_cdr = $thismonthmax['bill_cdr'];
            $bill_day = $thismonthmax['bill_day'];
            $percent  = $thismonthmax['percent']; 
            $period_m = $thismonthmax['period_m'];
            $period_y = $thismonthmax['period_y'];
      
            $bill_monthly=array();
            
            $bill_monthly['bill_id'] = $bill['bill_id'];
            $bill_monthly['bill_day'] =$bill_day;
            $bill_monthly['bill_cdr']= $bill_cdr;
            $bill_monthly['percentage']=$percent;  
            $bill_monthly['period_m']=$period_m;
            $bill_monthly['period_y']=$period_y;
            $bill_monthly['last_updated']=$now;
            $bill_monthly[$month_day]=$percent;
            

            if ($thismonthexist['exist']=='false') {
                    
                dbInsert($bill_monthly,'bill_monthly');
           
                echo 'Bill Monthly Insert! ';

            
            } //New Record
            
            else { //Update Record
             //   $fields = array('timestamp' => $now, 'in_counter' => set_numeric($port_data['in_measurement']), 'out_counter' => set_numeric($port_data['out_measurement']), 'in_delta' => set_numeric($port_data['in_delta']), 'out_delta' => set_numeric($port_data['out_delta']));               
        
            //    $fields = array('bill_day'=>$bill_day,'bill_cdr'=>$bill_cdr,'percentage'=>$percent,//'last_updated'=>$now);
                
                $fields=array();

                $fields['bill_day'] =$bill_day;
                $fields['bill_cdr']= $bill_cdr;
                $fields['percentage']=$percent;  
                $fields['period_m']=$period_m;
                $fields['period_y']=$period_y;
                $fields['last_updated']=$now;
                $fields[$month_day]=$percent;

                dbUpdate($fields,'bill_monthly',"`bill_id`='" .$bill['bill_id'] ."' AND `period_m`='" . $period_m . "' AND `period_y`='" . $period_y . "'");
      
                echo ' Updating Bill Monthly ! ';
            }


           //Update Table Bills
           $billparam = array('occupancy_month'=>$percent,'monthly_last_calc'=>$now);    
           $bill_update = dbUpdate($billparam,'bills','`bill_id` = ?',array($bill['bill_id']));
           
           echo 'Billl ID ' . $bill['bill_id'] .  ' Percent ' . $percent . '\n';

           if ($bill_update>0) {

               echo 'Occupancy on table bill udpdated !\n';
           }

            echo "\n\n";        
        }
  
}//end foreach
