#!/bin/bash
TODAY=`date +%Y_%m_%d_%H_%M_%S`
HOST=$(hostname)

OUTPUT="output-$TODAY.txt"
LOG="/var/log/nms/"

echo "Date: $TODAY   Host:$HOST" > "$LOG\$OUTPUT"

FILE=$1
while read -r line;do
   
   printf 'Adding host  %s .... \n' "$line"

   RESULT=php ./addhost.php  "$line" 2>&1 | tee "$LOG/$line"-log.txt

   file="$LOG/$line"-log.txt

    if  grep "Added device" "$file"
    then

      echo "\"$line\",\"Adding $line result:\",\"ADDED\"" >> "$LOG/$OUTPUT"
    elif  grep "Already have" "$file"
     then
      	echo "\"$line\",\"Adding $line result:\",\"EXISTING\"" >> "$LOG/$OUTPUT"
    elif  grep "Could not ping" "$file"
     then
         echo "\"$line\",\"Adding $line result:\",\"COULD-NOT-PING\"" >> "$LOG/$OUTPUT"
    else

	 echo "\"$line\",\"Adding $line result:\",\"ERROR\"" >> "$LOG/OUTPUT"
    fi

done<$FILE
