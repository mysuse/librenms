-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: nms
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_points`
--

DROP TABLE IF EXISTS `access_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_points` (
  `accesspoint_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `radio_number` tinyint(4) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `mac_addr` varchar(24) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `channel` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `txpow` tinyint(4) NOT NULL DEFAULT '0',
  `radioutil` tinyint(4) NOT NULL DEFAULT '0',
  `numasoclients` smallint(6) NOT NULL DEFAULT '0',
  `nummonclients` smallint(6) NOT NULL DEFAULT '0',
  `numactbssid` tinyint(4) NOT NULL DEFAULT '0',
  `nummonbssid` tinyint(4) NOT NULL DEFAULT '0',
  `interference` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`accesspoint_id`),
  KEY `deleted` (`deleted`),
  KEY `name` (`name`,`radio_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Access Points';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_points`
--

LOCK TABLES `access_points` WRITE;
/*!40000 ALTER TABLE `access_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_log`
--

DROP TABLE IF EXISTS `alert_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `details` longblob,
  `time_logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rule_id` (`rule_id`),
  KEY `device_id` (`device_id`),
  KEY `time_logged` (`time_logged`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_log`
--

LOCK TABLES `alert_log` WRITE;
/*!40000 ALTER TABLE `alert_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_map`
--

DROP TABLE IF EXISTS `alert_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` int(11) NOT NULL DEFAULT '0',
  `target` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_map`
--

LOCK TABLES `alert_map` WRITE;
/*!40000 ALTER TABLE `alert_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_rules`
--

DROP TABLE IF EXISTS `alert_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) NOT NULL DEFAULT '',
  `rule` text NOT NULL,
  `severity` enum('ok','warning','critical') NOT NULL,
  `extra` varchar(255) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `proc` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_rules`
--

LOCK TABLES `alert_rules` WRITE;
/*!40000 ALTER TABLE `alert_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_schedule`
--

DROP TABLE IF EXISTS `alert_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `recurring` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `start` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `end` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `start_recurring_dt` date NOT NULL DEFAULT '1970-01-01',
  `end_recurring_dt` date DEFAULT NULL,
  `start_recurring_hr` time NOT NULL DEFAULT '00:00:00',
  `end_recurring_hr` time NOT NULL DEFAULT '00:00:00',
  `recurring_day` varchar(15) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_schedule`
--

LOCK TABLES `alert_schedule` WRITE;
/*!40000 ALTER TABLE `alert_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_schedule_items`
--

DROP TABLE IF EXISTS `alert_schedule_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_schedule_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `target` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_schedule_items`
--

LOCK TABLES `alert_schedule_items` WRITE;
/*!40000 ALTER TABLE `alert_schedule_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_schedule_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_template_map`
--

DROP TABLE IF EXISTS `alert_template_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_template_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_templates_id` int(11) NOT NULL,
  `alert_rule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alert_templates_id` (`alert_templates_id`,`alert_rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_template_map`
--

LOCK TABLES `alert_template_map` WRITE;
/*!40000 ALTER TABLE `alert_template_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_template_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alert_templates`
--

DROP TABLE IF EXISTS `alert_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` varchar(255) NOT NULL DEFAULT ',',
  `name` varchar(255) NOT NULL,
  `template` longtext NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_rec` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_templates`
--

LOCK TABLES `alert_templates` WRITE;
/*!40000 ALTER TABLE `alert_templates` DISABLE KEYS */;
INSERT INTO `alert_templates` VALUES (1,',','BGP Sessions.','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed\\r\\n{/if}\nTimestamp: %timestamp\\r\\n\nUnique-ID: %uid\\r\\n\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\n#%key: %value.string\\r\\n\nPeer: %value.astext\\r\\n\nPeer IP: %value.bgpPeerIdentifier\\r\\n\nPeer AS: %value.bgpPeerRemoteAs\\r\\n\nPeer EstTime: %value.bgpPeerFsmEstablishedTime\\r\\n\nPeer State: %value.bgpPeerState\\r\\n\n{/foreach}\n{/if}','',''),(2,',','Ports','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed{/if}\nTimestamp: %timestamp\nUnique-ID: %uid\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\\r\\n\n#%key: %value.string\\r\\n\nPort: %value.ifName\\r\\n\nPort Name: %value.ifAlias\\r\\n\nPort Status: %value.message\\r\\n\n{/foreach}\\r\\n\n{/if}\n','',''),(3,',','Temperature','%title\\r\\n\nSeverity: %severity\\r\\n\n{if %state == 0}Time elapsed: %elapsed{/if}\\r\\n\nTimestamp: %timestamp\\r\\n\nUnique-ID: %uid\\r\\n\nRule: {if %name}%name{else}%rule{/if}\\r\\n\n{if %faults}Faults:\\r\\n\n{foreach %faults}\\r\\n\n#%key: %value.string\\r\\n\nTemperature: %value.sensor_current\\r\\n\nPrevious Measurement: %value.sensor_prev\\r\\n\n{/foreach}\\r\\n\n{/if}','','');
/*!40000 ALTER TABLE `alert_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `alerted` int(11) NOT NULL,
  `open` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_alert` (`device_id`,`rule_id`),
  KEY `rule_id` (`rule_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_tokens`
--

DROP TABLE IF EXISTS `api_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token_hash` varchar(256) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_hash` (`token_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_tokens`
--

LOCK TABLES `api_tokens` WRITE;
/*!40000 ALTER TABLE `api_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applications` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `app_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `app_state` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'UNKNOWN',
  `app_status` varchar(8) NOT NULL,
  `app_instance` varchar(255) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authlog`
--

DROP TABLE IF EXISTS `authlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` text NOT NULL,
  `address` text NOT NULL,
  `result` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authlog`
--

LOCK TABLES `authlog` WRITE;
/*!40000 ALTER TABLE `authlog` DISABLE KEYS */;
INSERT INTO `authlog` VALUES (47,'2017-03-14 05:06:40','admin','192.168.66.1','Logged In'),(48,'2017-03-14 05:07:20','admin','192.168.66.1','Logged Out'),(49,'2017-03-19 03:46:24','admin','192.168.66.1','Logged In'),(50,'2017-03-19 12:06:23','admin','192.168.66.1','Logged In'),(51,'2017-03-20 08:58:44','admin','192.168.66.1','Logged In'),(52,'2017-03-20 12:25:24','admin','192.168.66.1','Logged In'),(53,'2017-03-21 00:37:51','admin','192.168.66.1','Logged In'),(54,'2017-03-21 01:59:59','admin','192.168.66.1','Logged Out'),(55,'2017-03-21 03:10:14','admin','192.168.66.1','Logged In'),(56,'2017-03-31 06:59:53','admin','192.168.66.1','Logged In'),(57,'2017-03-31 07:03:00','admin','192.168.66.1','Logged Out'),(58,'2017-03-31 07:03:46','admin','192.168.66.1','Logged In'),(59,'2017-04-04 02:08:21','admin','192.168.66.1','Logged In'),(60,'2017-04-04 04:03:44','admin','192.168.66.1','Logged Out'),(61,'2017-04-04 04:03:52','admin','192.168.66.1','Logged In'),(62,'2017-04-04 04:04:06','admin','192.168.66.1','Logged Out'),(63,'2017-04-04 04:04:10','admin','192.168.66.1','Logged In'),(64,'2017-04-04 04:04:40','admin','192.168.66.1','Logged Out'),(65,'2017-04-04 04:04:46','admin','192.168.66.1','Logged In'),(66,'2017-04-04 04:08:55','admin','192.168.66.1','Logged Out'),(67,'2017-04-04 04:09:24','admin','192.168.66.1','Logged In'),(68,'2017-04-04 04:10:19','admin','192.168.66.1','Logged Out'),(69,'2017-04-04 04:10:24','admin','192.168.66.1','Logged In'),(70,'2017-04-04 07:16:25','admin','192.168.66.1','Logged In'),(71,'2017-04-04 08:43:55','admin','192.168.66.1','Logged In'),(72,'2017-04-04 13:11:27','admin','192.168.66.1','Logged In'),(73,'2017-04-04 13:48:52','admin','192.168.66.1','Logged In'),(74,'2017-04-04 13:49:12','admin','192.168.66.1','Logged Out'),(75,'2017-04-04 13:50:48','admin','192.168.66.1','Logged In'),(76,'2017-04-04 13:50:57','admin','192.168.66.1','Logged Out'),(77,'2017-04-04 13:54:51','admin','192.168.66.1','Logged In'),(78,'2017-04-04 13:54:59','admin','192.168.66.1','Logged Out'),(79,'2017-04-04 14:14:00','admin','192.168.66.1','Logged In'),(80,'2017-04-04 16:13:58','admin','192.168.66.1','Logged In'),(81,'2017-04-04 18:20:39','admin','192.168.66.1','Logged Out'),(82,'2017-04-04 18:24:22','admin','192.168.66.1','Logged In'),(83,'2017-04-04 18:25:34','admin','192.168.66.1','Logged Out'),(84,'2017-04-04 18:25:38','User','192.168.66.1','Authentication Failure'),(85,'2017-04-04 18:25:42','user','192.168.66.1','Logged In'),(86,'2017-04-04 18:26:08','user','192.168.66.1','Logged Out'),(87,'2017-04-04 18:26:12','admin','192.168.66.1','Logged In'),(88,'2017-04-04 18:26:59','admin','192.168.66.1','Logged Out'),(89,'2017-04-04 18:27:02','user','192.168.66.1','Logged In'),(90,'2017-04-04 18:28:24','user','192.168.66.1','Logged Out'),(91,'2017-04-04 18:28:28','admin','192.168.66.1','Logged In'),(92,'2017-04-04 18:28:54','admin','192.168.66.1','Logged Out'),(93,'2017-04-04 18:28:57','user','192.168.66.1','Logged In'),(94,'2017-04-04 18:38:00','user','192.168.66.1','Logged Out'),(95,'2017-04-05 02:07:54','admin','192.168.66.1','Logged In'),(96,'2017-04-05 09:31:08','admin','192.168.66.1','Logged In'),(97,'2017-04-06 05:53:29','admin','192.168.66.1','Logged In'),(98,'2017-04-06 08:48:17','admin','192.168.66.1','Logged Out'),(99,'2017-04-06 08:48:21','admin','192.168.66.1','Logged In'),(100,'2017-04-11 04:09:46','admin','192.168.66.1','Authentication Failure'),(101,'2017-04-11 04:09:51','admin','192.168.66.1','Logged In'),(102,'2017-04-11 11:22:52','admin','192.168.66.1','Authentication Failure'),(103,'2017-04-11 11:22:58','admin','192.168.66.1','Logged In'),(104,'2017-04-11 13:14:23','admin','192.168.66.1','Logged In'),(105,'2017-04-13 13:16:45','admin','192.168.66.1','Logged In'),(106,'2017-04-13 15:10:36','admin','192.168.66.1','Logged In'),(107,'2017-04-13 23:03:55','admin','192.168.66.1','Logged In'),(108,'2017-04-13 23:39:13','admin','192.168.66.1','Logged In'),(109,'2017-04-14 15:14:36','admin','192.168.66.1','Logged Out'),(110,'2017-04-14 15:14:46','admin','192.168.66.1','Logged In'),(111,'2017-04-16 00:53:37','admin','192.168.66.1','Logged In'),(112,'2017-04-16 02:03:13','admin','192.168.66.1','Logged In'),(113,'2017-04-16 03:41:32','admin','10.10.12.2','Logged In'),(114,'2017-04-16 04:44:58','admin','192.168.66.1','Logged In'),(115,'2017-04-18 12:40:59','admin','192.168.66.1','Logged In'),(116,'2017-04-19 06:35:51','admin','192.168.66.1','Logged In'),(117,'2017-04-19 12:13:33','admin','192.168.66.1','Logged In'),(118,'2017-04-19 15:35:38','admin','192.168.66.1','Logged Out'),(119,'2017-04-20 01:54:21','admin','192.168.66.1','Logged In'),(120,'2017-04-20 04:55:19','admin','192.168.66.1','Logged In'),(121,'2017-04-20 07:23:42','admin','192.168.66.1','Logged In'),(122,'2017-04-20 12:18:16','admin','192.168.66.1','Logged In'),(123,'2017-04-20 22:50:10','admin','192.168.66.1','Logged In'),(124,'2017-04-20 22:51:29','admin','192.168.66.1','Logged Out'),(125,'2017-04-20 23:08:36','admin','192.168.66.1','Logged In'),(126,'2017-04-20 23:46:10','admin','192.168.66.1','Logged Out'),(127,'2017-04-20 23:49:28','admin','192.168.66.1','Logged In'),(128,'2017-05-30 09:32:56','admin','192.168.66.1','Logged In'),(129,'2017-05-31 15:18:34','admin','192.168.66.1','Logged In'),(130,'2017-06-01 01:18:55','admin','54.254.142.157','Logged In'),(131,'2017-06-01 05:46:43','admin','192.168.66.1','Logged In'),(132,'2017-06-01 08:51:12','admin','192.168.66.1','Logged In'),(133,'2017-06-01 14:58:33','admin','192.168.66.1','Logged Out'),(134,'2017-06-01 14:58:39','rintoexa','192.168.66.1','Authentication Failure'),(135,'2017-06-01 14:58:46','rintoexa','192.168.66.1','Logged In'),(136,'2017-06-01 14:59:51','rintoexa','192.168.66.1','Logged Out'),(137,'2017-06-01 14:59:55','admin','192.168.66.1','Logged In'),(138,'2017-06-01 15:00:51','admin','192.168.66.1','Logged Out'),(139,'2017-06-01 15:00:56','rintoexa','192.168.66.1','Logged In'),(140,'2017-06-01 15:02:00','rintoexa','192.168.66.1','Logged Out'),(141,'2017-06-01 15:02:04','admin','192.168.66.1','Logged In'),(142,'2017-06-01 15:03:00','admin','192.168.66.1','Logged Out'),(143,'2017-06-01 15:03:04','rintoexa','192.168.66.1','Authentication Failure'),(144,'2017-06-01 15:03:11','rintoexa','192.168.66.1','Logged In'),(145,'2017-06-01 15:21:01','rintoexa','192.168.66.1','Logged Out'),(146,'2017-06-01 15:21:08','admin','192.168.66.1','Logged In'),(147,'2017-06-01 15:52:20','admin','192.168.66.1','Logged Out'),(148,'2017-06-02 14:56:05','admin','192.168.66.1','Logged In'),(149,'2017-06-02 16:28:02','admin','192.168.66.1','Logged In'),(150,'2017-06-04 03:22:25','admin','192.168.66.1','Logged In'),(151,'2017-06-04 07:42:00','admin','192.168.66.1','Logged In'),(152,'2017-06-04 09:15:38','admin','192.168.66.1','Logged Out'),(153,'2017-06-04 09:15:42','admin','192.168.66.1','Logged In'),(154,'2017-06-04 12:40:07','admin','192.168.66.1','Logged In'),(155,'2017-06-06 04:09:52','admin','192.168.66.1','Logged In'),(156,'2017-06-06 07:04:27','admin','192.168.66.1','Logged In'),(157,'2017-06-07 03:40:08','admin','192.168.66.1','Logged In'),(158,'2017-06-07 07:38:15','admin','192.168.66.1','Logged In'),(159,'2017-06-07 23:05:43','admin','192.168.66.1','Logged In'),(160,'2017-06-08 13:56:41','admin','192.168.66.1','Logged In'),(161,'2017-06-09 05:55:25','admin','192.168.66.1','Logged In'),(162,'2017-06-10 01:24:59','admin','192.168.66.1','Logged In'),(163,'2017-06-11 03:21:55','admin','192.168.66.1','Logged In'),(164,'2017-06-11 03:23:35','admin','192.168.66.1','Logged Out'),(165,'2017-06-11 06:25:58','admin','192.168.66.1','Logged In'),(166,'2017-06-12 03:23:05','admin','192.168.66.1','Logged In'),(167,'2017-06-12 05:40:13','admin','192.168.66.1','Logged In'),(168,'2017-06-27 09:27:58','admin','192.168.66.1','Logged In'),(169,'2017-06-28 01:40:28','admin','192.168.66.1','Logged In'),(170,'2017-06-28 03:50:02','admin','192.168.66.1','Logged In'),(171,'2017-07-04 01:21:34','admin','192.168.66.1','Logged In'),(172,'2017-07-04 04:16:50','admin','192.168.66.1','Logged In'),(173,'2017-07-04 13:20:17','admin','192.168.66.1','Logged In'),(174,'2017-07-05 01:19:03','admin','192.168.66.1','Logged In'),(175,'2017-07-05 04:40:38','rintoexa','192.168.66.1','Authentication Failure'),(176,'2017-07-05 06:09:14','rintoexa','192.168.66.1','Authentication Failure'),(177,'2017-07-05 06:09:24','admin','192.168.66.1','Logged In'),(178,'2017-07-05 06:09:24','admin','192.168.66.1','Logged Out'),(179,'2017-07-05 06:09:33','admin','192.168.66.1','Logged In'),(180,'2017-07-05 08:41:47','admin','192.168.66.1','Logged In'),(181,'2017-07-05 14:00:51','admin','192.168.66.1','Logged In'),(182,'2017-07-06 02:58:35','admin','192.168.66.1','Logged In'),(183,'2017-07-06 07:43:36','admin','192.168.66.1','Logged In'),(184,'2017-07-06 13:43:24','admin','192.168.66.1','Logged In'),(185,'2017-07-07 02:31:20','admin','192.168.66.1','Logged In'),(186,'2017-07-07 03:12:52','admin','192.168.66.1','Logged In'),(187,'2017-07-07 14:00:19','admin','192.168.66.1','Logged In'),(188,'2017-07-07 16:35:50','admin','192.168.66.1','Logged In'),(189,'2017-07-08 00:13:49','admin','192.168.66.1','Logged In'),(190,'2017-07-09 08:41:40','admin','192.168.66.1','Logged In'),(191,'2017-07-13 22:33:24','admin','192.168.66.1','Logged In'),(192,'2017-07-15 09:27:08','admin','192.168.66.1','Logged In'),(193,'2017-07-15 09:54:56','admin','192.168.66.1','Logged Out'),(194,'2017-07-15 11:24:29','admin','192.168.66.1','Logged In'),(195,'2017-07-16 09:01:58','admin','192.168.66.1','Logged In'),(196,'2017-07-21 08:19:57','admin','192.168.66.1','Logged In'),(197,'2017-09-03 13:24:47','admin','192.168.66.1','Logged In'),(198,'2017-09-05 23:58:56','admin','192.168.66.1','Logged In'),(199,'2017-09-06 06:32:45','rintoexa','192.168.66.1','Authentication Failure'),(200,'2017-09-06 06:32:50','admin','192.168.66.1','Logged In'),(201,'2017-09-08 13:22:07','admin','192.168.66.1','Logged In'),(202,'2017-09-10 07:56:20','admin','192.168.66.1','Logged In'),(203,'2017-09-10 13:29:18','admin','192.168.66.1','Logged In'),(204,'2017-09-12 09:35:27','admin','10.16.8.245','Logged In'),(205,'2017-09-12 09:53:10','admin','10.16.8.245','Logged In'),(206,'2017-09-12 11:17:48','admin','10.16.8.245','Logged In');
/*!40000 ALTER TABLE `authlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgpPeers`
--

DROP TABLE IF EXISTS `bgpPeers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgpPeers` (
  `bgpPeer_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `astext` varchar(64) NOT NULL,
  `bgpPeerIdentifier` text NOT NULL,
  `bgpPeerRemoteAs` bigint(20) NOT NULL,
  `bgpPeerState` text NOT NULL,
  `bgpPeerAdminStatus` text NOT NULL,
  `bgpLocalAddr` text NOT NULL,
  `bgpPeerRemoteAddr` text NOT NULL,
  `bgpPeerInUpdates` int(11) NOT NULL,
  `bgpPeerOutUpdates` int(11) NOT NULL,
  `bgpPeerInTotalMessages` int(11) NOT NULL,
  `bgpPeerOutTotalMessages` int(11) NOT NULL,
  `bgpPeerFsmEstablishedTime` int(11) NOT NULL,
  `bgpPeerInUpdateElapsedTime` int(11) NOT NULL,
  `context_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`bgpPeer_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgpPeers`
--

LOCK TABLES `bgpPeers` WRITE;
/*!40000 ALTER TABLE `bgpPeers` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgpPeers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bgpPeers_cbgp`
--

DROP TABLE IF EXISTS `bgpPeers_cbgp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bgpPeers_cbgp` (
  `device_id` int(11) NOT NULL,
  `bgpPeerIdentifier` varchar(64) NOT NULL,
  `afi` varchar(16) NOT NULL,
  `safi` varchar(16) NOT NULL,
  `AcceptedPrefixes` int(11) NOT NULL,
  `DeniedPrefixes` int(11) NOT NULL,
  `PrefixAdminLimit` int(11) NOT NULL,
  `PrefixThreshold` int(11) NOT NULL,
  `PrefixClearThreshold` int(11) NOT NULL,
  `AdvertisedPrefixes` int(11) NOT NULL,
  `SuppressedPrefixes` int(11) NOT NULL,
  `WithdrawnPrefixes` int(11) NOT NULL,
  `AcceptedPrefixes_delta` int(11) NOT NULL,
  `AcceptedPrefixes_prev` int(11) NOT NULL,
  `DeniedPrefixes_delta` int(11) NOT NULL,
  `DeniedPrefixes_prev` int(11) NOT NULL,
  `AdvertisedPrefixes_delta` int(11) NOT NULL,
  `AdvertisedPrefixes_prev` int(11) NOT NULL,
  `SuppressedPrefixes_delta` int(11) NOT NULL,
  `SuppressedPrefixes_prev` int(11) NOT NULL,
  `WithdrawnPrefixes_delta` int(11) NOT NULL,
  `WithdrawnPrefixes_prev` int(11) NOT NULL,
  `context_name` varchar(128) DEFAULT NULL,
  UNIQUE KEY `unique_index` (`device_id`,`bgpPeerIdentifier`,`afi`,`safi`),
  KEY `device_id` (`device_id`,`bgpPeerIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bgpPeers_cbgp`
--

LOCK TABLES `bgpPeers_cbgp` WRITE;
/*!40000 ALTER TABLE `bgpPeers_cbgp` DISABLE KEYS */;
/*!40000 ALTER TABLE `bgpPeers_cbgp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_daily`
--

DROP TABLE IF EXISTS `bill_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_daily` (
  `bill_daily_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bill_cdr` bigint(20) NOT NULL DEFAULT '0',
  `percentage` double DEFAULT '0',
  `period_d` int(11) NOT NULL DEFAULT '0',
  `period_m` int(11) NOT NULL DEFAULT '0',
  `period_y` int(11) NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`bill_daily_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_daily`
--

LOCK TABLES `bill_daily` WRITE;
/*!40000 ALTER TABLE `bill_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_data`
--

DROP TABLE IF EXISTS `bill_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_data` (
  `bill_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `period` int(11) NOT NULL,
  `delta` bigint(11) NOT NULL,
  `in_delta` bigint(11) NOT NULL,
  `out_delta` bigint(11) NOT NULL,
  PRIMARY KEY (`bill_id`,`timestamp`),
  KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_data`
--

LOCK TABLES `bill_data` WRITE;
/*!40000 ALTER TABLE `bill_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_history`
--

DROP TABLE IF EXISTS `bill_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_history` (
  `bill_hist_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bill_datefrom` datetime NOT NULL,
  `bill_dateto` datetime NOT NULL,
  `bill_type` text NOT NULL,
  `bill_allowed` bigint(20) NOT NULL,
  `bill_used` bigint(20) NOT NULL,
  `bill_overuse` bigint(20) NOT NULL,
  `bill_percent` decimal(10,2) NOT NULL,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `dir_95th` varchar(3) NOT NULL,
  `rate_average` bigint(20) NOT NULL,
  `rate_average_in` bigint(20) NOT NULL,
  `rate_average_out` bigint(20) NOT NULL,
  `traf_in` bigint(20) NOT NULL,
  `traf_out` bigint(20) NOT NULL,
  `traf_total` bigint(20) NOT NULL,
  `pdf` longblob,
  PRIMARY KEY (`bill_hist_id`),
  UNIQUE KEY `unique_index` (`bill_id`,`bill_datefrom`,`bill_dateto`),
  KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_history`
--

LOCK TABLES `bill_history` WRITE;
/*!40000 ALTER TABLE `bill_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_monthly`
--

DROP TABLE IF EXISTS `bill_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_monthly` (
  `bill_monthly_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bill_cdr` bigint(20) NOT NULL DEFAULT '0',
  `percentage` double DEFAULT '0',
  `period_m` int(11) NOT NULL DEFAULT '0',
  `period_y` int(11) NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`bill_monthly_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_monthly`
--

LOCK TABLES `bill_monthly` WRITE;
/*!40000 ALTER TABLE `bill_monthly` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_monthly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_perms`
--

DROP TABLE IF EXISTS `bill_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_perms` (
  `user_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_perms`
--

LOCK TABLES `bill_perms` WRITE;
/*!40000 ALTER TABLE `bill_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_poller`
--

DROP TABLE IF EXISTS `bill_poller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_poller` (
  `bill_poller_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_cdr` bigint(20) DEFAULT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `percentage` double(11,2) NOT NULL,
  PRIMARY KEY (`bill_poller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_poller`
--

LOCK TABLES `bill_poller` WRITE;
/*!40000 ALTER TABLE `bill_poller` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_poller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_port_counters`
--

DROP TABLE IF EXISTS `bill_port_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_port_counters` (
  `port_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `in_counter` bigint(20) DEFAULT NULL,
  `in_delta` bigint(20) NOT NULL DEFAULT '0',
  `out_counter` bigint(20) DEFAULT NULL,
  `out_delta` bigint(20) NOT NULL DEFAULT '0',
  `bill_id` int(11) NOT NULL,
  PRIMARY KEY (`port_id`,`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_port_counters`
--

LOCK TABLES `bill_port_counters` WRITE;
/*!40000 ALTER TABLE `bill_port_counters` DISABLE KEYS */;
INSERT INTO `bill_port_counters` VALUES (5,'2017-09-12 02:36:01',0,9046,0,8988,11),(6,'2017-09-12 02:35:02',0,476,0,182,7),(14,'2017-09-12 02:35:36',0,0,0,0,9),(16,'2017-09-12 02:36:01',4944,1499,11599,1393,10);
/*!40000 ALTER TABLE `bill_port_counters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_ports`
--

DROP TABLE IF EXISTS `bill_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_ports` (
  `bill_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `bill_port_autoadded` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_ports`
--

LOCK TABLES `bill_ports` WRITE;
/*!40000 ALTER TABLE `bill_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_witel_daily`
--

DROP TABLE IF EXISTS `bill_witel_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_witel_daily` (
  `bill_witeldaily_id` int(11) NOT NULL AUTO_INCREMENT,
  `witel_id` int(11) NOT NULL,
  `bill_day` timestamp NULL DEFAULT NULL,
  `percent25` int(11) DEFAULT '0',
  `percent2550` int(11) DEFAULT '0',
  `percent5075` int(11) DEFAULT '0',
  `percent75` int(11) DEFAULT '0',
  `period_d` int(11) NOT NULL DEFAULT '0',
  `period_m` int(11) NOT NULL DEFAULT '0',
  `period_y` int(11) NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`bill_witeldaily_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_witel_daily`
--

LOCK TABLES `bill_witel_daily` WRITE;
/*!40000 ALTER TABLE `bill_witel_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_witel_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_yearly`
--

DROP TABLE IF EXISTS `bill_yearly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill_yearly` (
  `bill_yearly_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `bill_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bill_cdr` bigint(20) NOT NULL DEFAULT '0',
  `percentage` double DEFAULT '0',
  `period_y` int(11) NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`bill_yearly_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_yearly`
--

LOCK TABLES `bill_yearly` WRITE;
/*!40000 ALTER TABLE `bill_yearly` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_yearly` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bills` (
  `bill_id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_name` text NOT NULL,
  `bill_type` text NOT NULL,
  `bill_cdr` bigint(20) DEFAULT NULL,
  `bill_day` int(11) NOT NULL DEFAULT '1',
  `bill_quota` bigint(20) DEFAULT NULL,
  `rate_95th_in` bigint(20) NOT NULL,
  `rate_95th_out` bigint(20) NOT NULL,
  `rate_95th` bigint(20) NOT NULL,
  `dir_95th` varchar(3) NOT NULL,
  `total_data` bigint(20) NOT NULL,
  `total_data_in` bigint(20) NOT NULL,
  `total_data_out` bigint(20) NOT NULL,
  `rate_average_in` bigint(20) NOT NULL,
  `rate_average_out` bigint(20) NOT NULL,
  `rate_average` bigint(20) NOT NULL,
  `bill_last_calc` datetime NOT NULL,
  `bill_custid` varchar(64) NOT NULL,
  `bill_ref` varchar(64) NOT NULL,
  `bill_notes` varchar(256) NOT NULL,
  `bill_autoadded` tinyint(1) NOT NULL,
  UNIQUE KEY `bill_id` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bills`
--

LOCK TABLES `bills` WRITE;
/*!40000 ALTER TABLE `bills` DISABLE KEYS */;
/*!40000 ALTER TABLE `bills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `callback`
--

DROP TABLE IF EXISTS `callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callback` (
  `callback_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(64) NOT NULL,
  `value` char(64) NOT NULL,
  PRIMARY KEY (`callback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `callback`
--

LOCK TABLES `callback` WRITE;
/*!40000 ALTER TABLE `callback` DISABLE KEYS */;
/*!40000 ALTER TABLE `callback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cef_switching`
--

DROP TABLE IF EXISTS `cef_switching`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cef_switching` (
  `cef_switching_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` int(11) NOT NULL,
  `afi` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `cef_index` int(11) NOT NULL,
  `cef_path` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `drop` int(11) NOT NULL,
  `punt` int(11) NOT NULL,
  `punt2host` int(11) NOT NULL,
  `drop_prev` int(11) NOT NULL,
  `punt_prev` int(11) NOT NULL,
  `punt2host_prev` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `updated_prev` int(11) NOT NULL,
  PRIMARY KEY (`cef_switching_id`),
  UNIQUE KEY `device_id` (`device_id`,`entPhysicalIndex`,`afi`,`cef_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cef_switching`
--

LOCK TABLES `cef_switching` WRITE;
/*!40000 ALTER TABLE `cef_switching` DISABLE KEYS */;
/*!40000 ALTER TABLE `cef_switching` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciscoASA`
--

DROP TABLE IF EXISTS `ciscoASA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciscoASA` (
  `ciscoASA_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `data` bigint(20) NOT NULL,
  `high_alert` bigint(20) NOT NULL,
  `low_alert` bigint(20) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ciscoASA_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciscoASA`
--

LOCK TABLES `ciscoASA` WRITE;
/*!40000 ALTER TABLE `ciscoASA` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciscoASA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each component, unique index',
  `device_id` int(11) unsigned NOT NULL COMMENT 'device_id from the devices table',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name from the component_type table',
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Display label for the component',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The status of the component, retreived from the device',
  `disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Should this component be polled',
  `ignore` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Should this component be alerted on',
  `error` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Error message if in Alert state',
  PRIMARY KEY (`id`),
  KEY `device` (`device_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='components attached to a device.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component`
--

LOCK TABLES `component` WRITE;
/*!40000 ALTER TABLE `component` DISABLE KEYS */;
/*!40000 ALTER TABLE `component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_prefs`
--

DROP TABLE IF EXISTS `component_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_prefs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each entry',
  `component` int(11) unsigned NOT NULL COMMENT 'id from the component table',
  `attribute` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Attribute for the Component',
  `value` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Value for the Component',
  PRIMARY KEY (`id`),
  KEY `component` (`component`),
  CONSTRAINT `component_prefs_ibfk_1` FOREIGN KEY (`component`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='AV Pairs for each component';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_prefs`
--

LOCK TABLES `component_prefs` WRITE;
/*!40000 ALTER TABLE `component_prefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `component_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_statuslog`
--

DROP TABLE IF EXISTS `component_statuslog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_statuslog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID for each log entry, unique index',
  `component_id` int(11) unsigned NOT NULL COMMENT 'id from the component table',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The status that the component was changed TO',
  `message` text COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the status of the component was changed',
  PRIMARY KEY (`id`),
  KEY `device` (`component_id`),
  CONSTRAINT `component_statuslog_ibfk_1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='log of status changes to a component.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_statuslog`
--

LOCK TABLES `component_statuslog` WRITE;
/*!40000 ALTER TABLE `component_statuslog` DISABLE KEYS */;
/*!40000 ALTER TABLE `component_statuslog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(512) NOT NULL,
  `config_default` varchar(512) NOT NULL,
  `config_descr` varchar(100) NOT NULL,
  `config_group` varchar(50) NOT NULL,
  `config_group_order` int(11) NOT NULL,
  `config_sub_group` varchar(50) NOT NULL,
  `config_sub_group_order` int(11) NOT NULL,
  `config_hidden` enum('0','1') NOT NULL DEFAULT '0',
  `config_disabled` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `uniqueindex_configname` (`config_name`)
) ENGINE=InnoDB AUTO_INCREMENT=773 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (441,'alert.macros.rule.now','NOW()','NOW()','Macro currenttime','alerting',0,'macros',0,'1','0'),(442,'alert.macros.rule.past_5m','DATE_SUB(NOW(),INTERVAL 5 MINUTE)','DATE_SUB(NOW(),INTERVAL 5 MINUTE)','Macro past 5 minutes','alerting',0,'macros',0,'1','0'),(443,'alert.macros.rule.past_10m','DATE_SUB(NOW(),INTERVAL 10 MINUTE)','DATE_SUB(NOW(),INTERVAL 10 MINUTE)','Macro past 10 minutes','alerting',0,'macros',0,'1','0'),(444,'alert.macros.rule.past_15m','DATE_SUB(NOW(),INTERVAL 15 MINUTE)','DATE_SUB(NOW(),INTERVAL 15 MINUTE)','Macro past 15 minutes','alerting',0,'macros',0,'1','0'),(445,'alert.macros.rule.past_30m','DATE_SUB(NOW(),INTERVAL 30 MINUTE)','DATE_SUB(NOW(),INTERVAL 30 MINUTE)','Macro past 30 minutes','alerting',0,'macros',0,'1','0'),(446,'alert.macros.rule.device','(%devices.disabled = 0 && %devices.ignore = 0)','(%devices.disabled = 0 && %devices.ignore = 0)','Devices that aren\'t disabled or ignored','alerting',0,'macros',0,'1','0'),(447,'alert.macros.rule.device_up','(%devices.status = 1 && %macros.device)','(%devices.status = 1 && %macros.device)','Devices that are up','alerting',0,'macros',0,'1','0'),(448,'alert.macros.rule.device_down','(%devices.status = 0 && %macros.device)','(%devices.status = 0 && %macros.device)','Devices that are down','alerting',0,'macros',0,'1','0'),(449,'alert.macros.rule.port','(%ports.deleted = 0 && %ports.ignore = 0 && %ports.disabled = 0)','(%ports.deleted = 0 && %ports.ignore = 0 && %ports.disabled = 0)','Ports that aren\'t disabled, ignored or delete','alerting',0,'macros',0,'1','0'),(450,'alert.macros.rule.port_up','(%ports.ifOperStatus = \"up\" && %ports.ifAdminStatus = \"up\" && %macros.port)','(%ports.ifOperStatus = \"up\" && %ports.ifAdminStatus = \"up\" && %macros.port)','Ports that are up','alerting',0,'macros',0,'1','0'),(451,'alert.admins','true','true','Alert administrators','alerting',0,'general',0,'0','0'),(452,'alert.default_only','true','true','Only alert default mail contact','alerting',0,'general',0,'0','0'),(453,'alert.default_mail','','','The default mail contact','alerting',0,'general',0,'0','0'),(454,'alert.transports.pagerduty','','','Pagerduty transport - put your API key here','alerting',0,'transports',0,'0','0'),(455,'alert.pagerduty.account','','','Pagerduty account name','alerting',0,'transports',0,'0','0'),(456,'alert.pagerduty.service','','','Pagerduty service name','alerting',0,'transports',0,'0','0'),(457,'alert.tolerance_window','5','5','Tolerance window in seconds','alerting',0,'general',0,'0','0'),(458,'email_backend','mail','mail','The backend to use for sending email, can be mail, sendmail or smtp','alerting',0,'general',0,'0','0'),(459,'alert.macros.rule.past_60m','DATE_SUB(NOW(),INTERVAL 60 MINUTE)','DATE_SUB(NOW(),INTERVAL 60 MINUTE)','Macro past 60 minutes','alerting',0,'macros',0,'1','0'),(460,'alert.macros.rule.port_usage_perc','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc','alerting',0,'macros',0,'1','0'),(461,'alert.transports.dummy','false','false','Dummy transport','alerting',0,'transports',0,'0','0'),(462,'alert.transports.mail','true','true','Mail alerting transport','alerting',0,'transports',0,'0','0'),(463,'alert.transports.irc','FALSE','false','IRC alerting transport','alerting',0,'transports',0,'0','0'),(464,'alert.globals','true','TRUE','Alert read only administrators','alerting',0,'general',0,'0','0'),(465,'email_from','NULL','NULL','Email address used for sending emails (from)','alerting',0,'general',0,'0','0'),(466,'email_user','LibreNMS','LibreNMS','Name used as part of the from address','alerting',0,'general',0,'0','0'),(467,'email_sendmail_path','/usr/sbin/sendmail','/usr/sbin/sendmail','Location of sendmail if using this option','alerting',0,'general',0,'0','0'),(468,'email_smtp_host','localhost','localhost','SMTP Host for sending email if using this option','alerting',0,'general',0,'0','0'),(469,'email_smtp_port','25','25','SMTP port setting','alerting',0,'general',0,'0','0'),(470,'email_smtp_timeout','10','10','SMTP timeout setting','alerting',0,'general',0,'0','0'),(471,'email_smtp_secure','','','Enable / disable encryption (use tls or ssl)','alerting',0,'general',0,'0','0'),(472,'email_smtp_auth','false','FALSE','Enable / disable smtp authentication','alerting',0,'general',0,'0','0'),(473,'email_smtp_username','NULL','NULL','SMTP Auth username','alerting',0,'general',0,'0','0'),(474,'email_smtp_password','NULL','NULL','SMTP Auth password','alerting',0,'general',0,'0','0'),(475,'alert.macros.rule.port_down','(%ports.ifOperStatus = \"down\" && %ports.ifAdminStatus != \"down\" && %macros.port)','(%ports.ifOperStatus = \"down\" && %ports.ifAdminStatus != \"down\" && %macros.port)','Ports that are down','alerting',0,'macros',0,'1','0'),(486,'alert.syscontact','true','TRUE','Issue alerts to sysContact','alerting',0,'general',0,'0','0'),(487,'alert.fixed-contacts','true','TRUE','If TRUE any changes to sysContact or users emails will not be honoured whilst alert is active','alerting',0,'general',0,'0','0'),(488,'alert.transports.nagios','','','Nagios compatible via FIFO','alerting',0,'transports',0,'0','0'),(720,'alert.macros.rule.sensor','(%sensors.sensor_alert = 1)','(%sensors.sensor_alert = 1)','Sensors of interest','alerting',0,'macros',0,'0',''),(721,'alert.macros.rule.packet_loss_15m','(%macros.past_15m && %device_perf.loss)','(%macros.past_15m && %device_perf.loss)','Packet loss over the last 15 minutes','alerting',0,'macros',0,'0',''),(722,'alert.macros.rule.packet_loss_5m','(%macros.past_5m && %device_perf.loss)','(%macros.past_5m && %device_perf.loss)','Packet loss over the last 5 minutes','alerting',0,'macros',0,'0',''),(723,'alert.transports.pushbullet','','','Pushbullet access token','alerting',0,'transports',0,'0','0'),(724,'oxidized.enabled','false','false','Enable Oxidized support','external',0,'oxidized',0,'0','0'),(725,'oxidized.url','','','Oxidized API url','external',0,'oxidized',0,'0','0'),(726,'oxidized.features.versioning','false','false','Enable Oxidized config versioning','external',0,'oxidized',0,'0','0'),(727,'alert.disable','false','false','Stop alerts being generated','alerting',0,'general',0,'0','0'),(728,'unix-agent.port','6556','6556','Default port for the Unix-agent (check_mk)','external',0,'unix-agent',0,'0','0'),(729,'unix-agent.connection-timeout','10','10','Unix-agent connection timeout','external',0,'unix-agent',0,'0','0'),(730,'unix-agent.read-timeout','10','10','Unix-agent read timeout','external',0,'unix-agent',0,'0','0'),(731,'alert.transports.clickatell.token','','','Clickatell access token','alerting',0,'transports',0,'0','0'),(732,'alert.transports.playsms.url','','','PlaySMS API URL','alerting',0,'transports',0,'0','0'),(733,'alert.transports.playsms.user','','','PlaySMS User','alerting',0,'transports',0,'0','0'),(734,'alert.transports.playsms.from','','','PlaySMS From','alerting',0,'transports',0,'0','0'),(735,'alert.transports.playsms.token','','','PlaySMS Token','alerting',0,'transports',0,'0','0'),(736,'alert.transports.victorops.url','','','VictorOps Post URL - Replace routing_key!','alerting',0,'transports',0,'0','0'),(737,'rrdtool','/usr/bin/rrdtool','/usr/bin/rrdtool','Path to rrdtool','external',0,'rrdtool',0,'0','0'),(738,'rrdtool_tune','false','false','Auto tune maximum value for rrd port files','external',0,'rrdtool',0,'0','0'),(739,'oxidized.default_group','','','Set the default group returned','external',0,'oxidized',0,'0','0'),(740,'oxidized.group_support','false','false','Enable the return of groups to Oxidized','external',0,'oxidized',0,'0','0'),(741,'oxidized.reload_nodes','false','false','Reload Oxidized nodes list, each time a device is added','external',0,'oxidized',0,'0','0'),(742,'alert.macros.rule.component','(%component.disabled = 0 && %component.ignore = 0)','(%component.disabled = 0 && %component.ignore = 0)','Component that isn\'t disabled or ignored','alerting',0,'macros',0,'1','0'),(743,'alert.macros.rule.component_warning','(%component.status = 1 && %macros.component)','(%component.status = 1 && %macros.component)','Component status Warning','alerting',0,'macros',0,'1','0'),(744,'alert.macros.rule.component_normal','(%component.status = 0 && %macros.component)','(%component.status = 0 && %macros.component)','Component status Normal','alerting',0,'macros',0,'1','0'),(745,'webui.global_search_result_limit','8','8','Global search results limit','webui',0,'search',0,'1','0'),(746,'email_html','false','false','Send HTML emails','alerting',0,'general',0,'0','0'),(747,'alert.transports.canopsis.user','','','Canopsis User','alerting',0,'transports',0,'',''),(748,'alert.transports.canopsis.passwd','','','Canopsis Password','alerting',0,'transports',0,'',''),(749,'alert.transports.canopsis.host','','','Canopsis Hostname','alerting',0,'transports',0,'',''),(750,'alert.transports.canopsis.vhost','','','Canopsis vHost','alerting',0,'transports',0,'',''),(751,'alert.transports.canopsis.port','','','Canopsis Port number','alerting',0,'transports',0,'',''),(752,'webui.min_graph_height','300','300','Minimum Graph Height','webui',0,'graph',0,'1','0'),(753,'alert.macros.rule.port_in_usage_perc','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifInOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc of capacity IN','alerting',0,'macros',0,'0',''),(754,'alert.macros.rule.port_out_usage_perc','((%ports.ifOutOctets_rate*8) / %ports.ifSpeed)*100','((%ports.ifOutOctets_rate*8) / %ports.ifSpeed)*100','Ports using more than X perc of capacity OUT','alerting',0,'macros',0,'0',''),(755,'alert.macros.rule.port_now_down','%ports.ifOperStatus != %ports.ifOperStatus_prev && %ports.ifOperStatus_prev = \"up\" && %ports.ifAdminStatus = \"up\"','%ports.ifOperStatus != %ports.ifOperStatus_prev && %ports.ifOperStatus_prev = \"up\" && %ports.ifAdminStatus = \"up\"','Port has gone down','alerting',0,'macros',0,'0',''),(756,'alert.macros.rule.device_component_down_junos','%sensors.sensor_class = \"state\" && %sensors.sensor_current != \"6\" && %sensors.sensor_type = \"jnxFruState\" && %sensors.sensor_current != \"2\"','%sensors.sensor_class = \"state\" && %sensors.sensor_current != \"6\" && %sensors.sensor_type = \"jnxFruState\" && %sensors.sensor_current != \"2\"','Device Component down [JunOS]','alerting',0,'macros',0,'0',''),(757,'alert.macros.rule.device_component_down_cisco','%sensors.sensor_current != \"1\" && %sensors.sensor_current != \"5\" && %sensors.sensor_type ~ \"^cisco.*State$\"','%sensors.sensor_current != \"1\" && %sensors.sensor_current != \"5\" && %sensors.sensor_type ~ \"^cisco.*State$\"','Device Component down [Cisco]','alerting',0,'macros',0,'0',''),(758,'alert.macros.rule.pdu_over_amperage_apc','%sensors.sensor_class = \"current\" && %sensors.sensor_descr = \"Bank Total\" && %sensors.sensor_current > %sensors.sensor_limit && %devices.os = \"apc\"','%sensors.sensor_class = \"current\" && %sensors.sensor_descr = \"Bank Total\" && %sensors.sensor_current > %sensors.sensor_limit && %devices.os = \"apc\"','PDU Over Amperage [APC]','alerting',0,'macros',0,'0',''),(759,'alert.macros.rule.component_critical','(%component.status = 2 && %macros.component)','(%component.status = 2 && %macros.component)','Component status Critical','alerting',0,'macros',0,'1','0'),(760,'webui.availability_map_compact','false','false','Availability map old view','webui',0,'graph',0,'0','0'),(761,'webui.default_dashboard_id','0','0','Global default dashboard_id for all users who do not have their own default set','webui',0,'dashboard',0,'0','0'),(762,'webui.availability_map_sort_status','false','false','Sort devices and services by status','webui',0,'graph',0,'0','0'),(763,'webui.availability_map_use_device_groups','false','false','Enable usage of device groups filter','webui',0,'graph',0,'0','0'),(764,'webui.availability_map_box_size','165','165','Input desired tile width in pixels for box size in full view','webui',0,'graph',0,'0','0'),(765,'alert.transports.osticket.url','','','osTicket API URL','alerting',0,'transports',0,'',''),(766,'alert.transports.osticket.token','','','osTicket API Token','alerting',0,'transports',0,'',''),(767,'alert.macros.rule.bill_quota_over_quota','((%bills.total_data / %bills.bill_quota)*100) && %bills.bill_type = \"quota\"','((%bills.total_data / %bills.bill_quota)*100) && %bills.bill_type = \"quota\"','Quota bills over X perc of quota','alerting',0,'macros',0,'0',''),(768,'alert.macros.rule.bill_cdr_over_quota','((%bills.rate_95th / %bills.bill_cdr)*100) && %bills.bill_type = \"cdr\"','((%bills.rate_95th / %bills.bill_cdr)*100) && %bills.bill_type = \"cdr\"','CDR bills over X perc of quota','alerting',0,'macros',0,'0',''),(769,'alert.transports.msteams.url','','','Microsoft Teams Webhook URL','alerting',0,'transports',0,'',''),(770,'email_auto_tls','true','true','Enable / disable Auto TLS support','alerting',0,'general',0,'0','0'),(771,'alert.transports.ciscospark.token','','','Cisco Spark API Token','alerting',0,'transports',0,'0','0'),(772,'alert.transports.ciscospark.roomid','','','Cisco Spark RoomID','alerting',0,'transports',0,'0','0');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(64) NOT NULL,
  `password` char(32) NOT NULL,
  `string` char(64) NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboards`
--

DROP TABLE IF EXISTS `dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboards` (
  `dashboard_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `dashboard_name` varchar(255) NOT NULL,
  `access` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboards`
--

LOCK TABLES `dashboards` WRITE;
/*!40000 ALTER TABLE `dashboards` DISABLE KEYS */;
INSERT INTO `dashboards` VALUES (2,2,'Default',0),(3,1,'Default',2);
/*!40000 ALTER TABLE `dashboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbSchema`
--

DROP TABLE IF EXISTS `dbSchema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbSchema` (
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbSchema`
--

LOCK TABLES `dbSchema` WRITE;
/*!40000 ALTER TABLE `dbSchema` DISABLE KEYS */;
INSERT INTO `dbSchema` VALUES (168);
/*!40000 ALTER TABLE `dbSchema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_graphs`
--

DROP TABLE IF EXISTS `device_graphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_graphs` (
  `device_id` int(11) NOT NULL,
  `graph` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_graphs`
--

LOCK TABLES `device_graphs` WRITE;
/*!40000 ALTER TABLE `device_graphs` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_graphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_group_device`
--

DROP TABLE IF EXISTS `device_group_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_group_device` (
  `device_group_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`device_group_id`,`device_id`),
  KEY `device_group_device_device_group_id_index` (`device_group_id`),
  KEY `device_group_device_device_id_index` (`device_id`),
  CONSTRAINT `device_group_device_device_group_id_foreign` FOREIGN KEY (`device_group_id`) REFERENCES `device_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `device_group_device_device_id_foreign` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_group_device`
--

LOCK TABLES `device_group_device` WRITE;
/*!40000 ALTER TABLE `device_group_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_group_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_groups`
--

DROP TABLE IF EXISTS `device_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `desc` varchar(255) NOT NULL DEFAULT '',
  `pattern` text,
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_groups`
--

LOCK TABLES `device_groups` WRITE;
/*!40000 ALTER TABLE `device_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_mibs`
--

DROP TABLE IF EXISTS `device_mibs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_mibs` (
  `device_id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `included_by` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`,`module`,`mib`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Device to MIB mappings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_mibs`
--

LOCK TABLES `device_mibs` WRITE;
/*!40000 ALTER TABLE `device_mibs` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_mibs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_oids`
--

DROP TABLE IF EXISTS `device_oids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_oids` (
  `device_id` int(11) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `numvalue` bigint(20) DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`device_id`,`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Per-device MIB data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_oids`
--

LOCK TABLES `device_oids` WRITE;
/*!40000 ALTER TABLE `device_oids` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_oids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_perf`
--

DROP TABLE IF EXISTS `device_perf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_perf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL,
  `xmt` float NOT NULL,
  `rcv` float NOT NULL,
  `loss` float NOT NULL,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `avg` float NOT NULL,
  KEY `id` (`id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_perf`
--

LOCK TABLES `device_perf` WRITE;
/*!40000 ALTER TABLE `device_perf` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_perf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `device_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(128) CHARACTER SET latin1 NOT NULL,
  `sysName` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `ip` varbinary(16) DEFAULT NULL,
  `community` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authlevel` enum('noAuthNoPriv','authNoPriv','authPriv') COLLATE utf8_unicode_ci DEFAULT NULL,
  `authname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authpass` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authalgo` enum('MD5','SHA') COLLATE utf8_unicode_ci DEFAULT NULL,
  `cryptopass` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cryptoalgo` enum('AES','DES','') COLLATE utf8_unicode_ci DEFAULT NULL,
  `snmpver` varchar(4) CHARACTER SET latin1 NOT NULL DEFAULT 'v2c',
  `port` smallint(5) unsigned NOT NULL DEFAULT '161',
  `transport` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'udp',
  `timeout` int(11) DEFAULT NULL,
  `retries` int(11) DEFAULT NULL,
  `bgpLocalAs` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `sysObjectID` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sysDescr` text CHARACTER SET latin1,
  `sysContact` text CHARACTER SET latin1,
  `version` text CHARACTER SET latin1,
  `hardware` text CHARACTER SET latin1,
  `features` text CHARACTER SET latin1,
  `location` text COLLATE utf8_unicode_ci,
  `os` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `status_reason` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ignore` tinyint(4) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `uptime` bigint(20) DEFAULT NULL,
  `agent_uptime` int(11) NOT NULL DEFAULT '0',
  `last_polled` timestamp NULL DEFAULT NULL,
  `last_poll_attempted` timestamp NULL DEFAULT NULL,
  `last_polled_timetaken` double(5,2) DEFAULT NULL,
  `last_discovered_timetaken` double(5,2) DEFAULT NULL,
  `last_discovered` timestamp NULL DEFAULT NULL,
  `last_ping` timestamp NULL DEFAULT NULL,
  `last_ping_timetaken` double(5,2) DEFAULT NULL,
  `purpose` text COLLATE utf8_unicode_ci,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `serial` text COLLATE utf8_unicode_ci,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poller_group` int(11) NOT NULL DEFAULT '0',
  `override_sysLocation` tinyint(1) DEFAULT '0',
  `notes` text COLLATE utf8_unicode_ci,
  `port_association_mode` int(11) NOT NULL DEFAULT '1',
  `site_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sto` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan2g` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlan3g` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witel_id` int(11) DEFAULT NULL,
  `bandwidth` int(11) DEFAULT NULL,
  `metro_name` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metro_port` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metro_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_name` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `olt_port` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ont_ip` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ont_sn` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahundeploy` int(11) DEFAULT NULL,
  `dataservices` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tselregion_id` int(11) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `status` (`status`),
  KEY `hostname` (`hostname`),
  KEY `sysName` (`sysName`),
  KEY `os` (`os`),
  KEY `last_polled` (`last_polled`),
  KEY `last_poll_attempted` (`last_poll_attempted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices_attribs`
--

DROP TABLE IF EXISTS `devices_attribs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices_attribs` (
  `attrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `attrib_type` varchar(32) NOT NULL,
  `attrib_value` text NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attrib_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices_attribs`
--

LOCK TABLES `devices_attribs` WRITE;
/*!40000 ALTER TABLE `devices_attribs` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices_attribs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices_perms`
--

DROP TABLE IF EXISTS `devices_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices_perms` (
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `access_level` int(4) NOT NULL DEFAULT '0',
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices_perms`
--

LOCK TABLES `devices_perms` WRITE;
/*!40000 ALTER TABLE `devices_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entPhysical`
--

DROP TABLE IF EXISTS `entPhysical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entPhysical` (
  `entPhysical_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` int(11) NOT NULL,
  `entPhysicalDescr` text NOT NULL,
  `entPhysicalClass` text NOT NULL,
  `entPhysicalName` text NOT NULL,
  `entPhysicalHardwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalFirmwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalSoftwareRev` varchar(64) DEFAULT NULL,
  `entPhysicalAlias` varchar(32) DEFAULT NULL,
  `entPhysicalAssetID` varchar(32) DEFAULT NULL,
  `entPhysicalIsFRU` varchar(8) DEFAULT NULL,
  `entPhysicalModelName` text NOT NULL,
  `entPhysicalVendorType` text,
  `entPhysicalSerialNum` text NOT NULL,
  `entPhysicalContainedIn` int(11) NOT NULL,
  `entPhysicalParentRelPos` int(11) NOT NULL,
  `entPhysicalMfgName` text NOT NULL,
  `ifIndex` int(11) DEFAULT NULL,
  PRIMARY KEY (`entPhysical_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entPhysical`
--

LOCK TABLES `entPhysical` WRITE;
/*!40000 ALTER TABLE `entPhysical` DISABLE KEYS */;
/*!40000 ALTER TABLE `entPhysical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entPhysical_state`
--

DROP TABLE IF EXISTS `entPhysical_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entPhysical_state` (
  `device_id` int(11) NOT NULL,
  `entPhysicalIndex` varchar(64) NOT NULL,
  `subindex` varchar(64) DEFAULT NULL,
  `group` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(255) NOT NULL,
  KEY `device_id_index` (`device_id`,`entPhysicalIndex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entPhysical_state`
--

LOCK TABLES `entPhysical_state` WRITE;
/*!40000 ALTER TABLE `entPhysical_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `entPhysical_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventlog`
--

DROP TABLE IF EXISTS `eventlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventlog` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `host` int(11) NOT NULL DEFAULT '0',
  `device_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT '1970-01-01 00:00:01',
  `message` text CHARACTER SET latin1,
  `type` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `reference` varchar(64) CHARACTER SET latin1 NOT NULL,
  `severity` int(1) DEFAULT '2',
  PRIMARY KEY (`event_id`),
  KEY `host` (`host`),
  KEY `datetime` (`datetime`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=540 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventlog`
--

LOCK TABLES `eventlog` WRITE;
/*!40000 ALTER TABLE `eventlog` DISABLE KEYS */;
INSERT INTO `eventlog` VALUES (1,0,0,'2017-06-27 16:28:21','Device localhost has been removed','system','NULL',3),(142,0,0,'2017-06-27 19:06:02','Device localhost has been removed','system','NULL',3),(534,0,0,'2017-09-12 18:18:11','Device 192.168.66.111 has been removed','system','NULL',3),(535,0,0,'2017-09-12 18:18:15','Device 192.168.66.112 has been removed','system','NULL',3),(536,0,0,'2017-09-12 18:18:17','Device 192.168.66.113 has been removed','system','NULL',3),(537,0,0,'2017-09-12 18:18:23','Device 192.168.66.114 has been removed','system','NULL',3),(538,0,0,'2017-09-12 18:18:26','Device 192.168.66.137 has been removed','system','NULL',3),(539,0,0,'2017-09-12 18:18:29','Device 192.168.66.138 has been removed','system','NULL',3);
/*!40000 ALTER TABLE `eventlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_types`
--

DROP TABLE IF EXISTS `graph_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_types` (
  `graph_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_subtype` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `graph_section` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `graph_order` int(11) NOT NULL,
  PRIMARY KEY (`graph_type`,`graph_subtype`,`graph_section`),
  KEY `graph_type` (`graph_type`),
  KEY `graph_subtype` (`graph_subtype`),
  KEY `graph_section` (`graph_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_types`
--

LOCK TABLES `graph_types` WRITE;
/*!40000 ALTER TABLE `graph_types` DISABLE KEYS */;
INSERT INTO `graph_types` VALUES ('device','asa_conns','firewall','Current connections',0),('device','panos_activetunnels','firewall','Active GlobalProtect Tunnels',0),('device','pulse_sessions','firewall','Active Sessions',0),('device','pulse_users','firewall','Active Users',0),('device','riverbed_connections','network','Connections',0),('device','riverbed_datastore','network','Datastore productivity',0),('device','riverbed_optimization','network','Optimization',0),('device','riverbed_passthrough','network','Bandwidth passthrough',0),('device','sgos_average_requests','network','Average HTTP Requests',0),('device','sonicwall_sessions','firewall','Active Sessions',0),('device','waas_cwotfostatsactiveoptconn','graphs','Optimized TCP Connections',0),('device','zywall_sessions','firewall','Sessions',0);
/*!40000 ALTER TABLE `graph_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_types_dead`
--

DROP TABLE IF EXISTS `graph_types_dead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_types_dead` (
  `graph_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_subtype` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_section` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `graph_descr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `graph_order` int(11) NOT NULL,
  KEY `graph_type` (`graph_type`),
  KEY `graph_subtype` (`graph_subtype`),
  KEY `graph_section` (`graph_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_types_dead`
--

LOCK TABLES `graph_types_dead` WRITE;
/*!40000 ALTER TABLE `graph_types_dead` DISABLE KEYS */;
/*!40000 ALTER TABLE `graph_types_dead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hrDevice`
--

DROP TABLE IF EXISTS `hrDevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hrDevice` (
  `hrDevice_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `hrDeviceIndex` int(11) NOT NULL,
  `hrDeviceDescr` text CHARACTER SET latin1 NOT NULL,
  `hrDeviceType` text CHARACTER SET latin1 NOT NULL,
  `hrDeviceErrors` int(11) NOT NULL DEFAULT '0',
  `hrDeviceStatus` text CHARACTER SET latin1 NOT NULL,
  `hrProcessorLoad` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`hrDevice_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hrDevice`
--

LOCK TABLES `hrDevice` WRITE;
/*!40000 ALTER TABLE `hrDevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `hrDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipsec_tunnels`
--

DROP TABLE IF EXISTS `ipsec_tunnels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipsec_tunnels` (
  `tunnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `peer_port` int(11) NOT NULL,
  `peer_addr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `local_addr` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `local_port` int(11) NOT NULL,
  `tunnel_name` varchar(96) COLLATE utf8_unicode_ci NOT NULL,
  `tunnel_status` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`tunnel_id`),
  UNIQUE KEY `unique_index` (`device_id`,`peer_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipsec_tunnels`
--

LOCK TABLES `ipsec_tunnels` WRITE;
/*!40000 ALTER TABLE `ipsec_tunnels` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipsec_tunnels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_addresses`
--

DROP TABLE IF EXISTS `ipv4_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_addresses` (
  `ipv4_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv4_address` varchar(32) CHARACTER SET latin1 NOT NULL,
  `ipv4_prefixlen` int(11) NOT NULL,
  `ipv4_network_id` varchar(32) CHARACTER SET latin1 NOT NULL,
  `port_id` int(11) NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv4_address_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_addresses`
--

LOCK TABLES `ipv4_addresses` WRITE;
/*!40000 ALTER TABLE `ipv4_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_mac`
--

DROP TABLE IF EXISTS `ipv4_mac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_mac` (
  `port_id` int(11) NOT NULL,
  `mac_address` varchar(32) CHARACTER SET utf8 NOT NULL,
  `ipv4_address` varchar(32) CHARACTER SET utf8 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  KEY `port_id` (`port_id`),
  KEY `mac_address` (`mac_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_mac`
--

LOCK TABLES `ipv4_mac` WRITE;
/*!40000 ALTER TABLE `ipv4_mac` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_mac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv4_networks`
--

DROP TABLE IF EXISTS `ipv4_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv4_networks` (
  `ipv4_network_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv4_network` varchar(64) CHARACTER SET latin1 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv4_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv4_networks`
--

LOCK TABLES `ipv4_networks` WRITE;
/*!40000 ALTER TABLE `ipv4_networks` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv4_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv6_addresses`
--

DROP TABLE IF EXISTS `ipv6_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv6_addresses` (
  `ipv6_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv6_address` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ipv6_compressed` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ipv6_prefixlen` int(11) NOT NULL,
  `ipv6_origin` varchar(16) CHARACTER SET latin1 NOT NULL,
  `ipv6_network_id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `port_id` int(11) NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv6_address_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv6_addresses`
--

LOCK TABLES `ipv6_addresses` WRITE;
/*!40000 ALTER TABLE `ipv6_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv6_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipv6_networks`
--

DROP TABLE IF EXISTS `ipv6_networks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipv6_networks` (
  `ipv6_network_id` int(11) NOT NULL AUTO_INCREMENT,
  `ipv6_network` varchar(64) CHARACTER SET latin1 NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ipv6_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipv6_networks`
--

LOCK TABLES `ipv6_networks` WRITE;
/*!40000 ALTER TABLE `ipv6_networks` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipv6_networks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `juniAtmVp`
--

DROP TABLE IF EXISTS `juniAtmVp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `juniAtmVp` (
  `juniAtmVp_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `vp_id` int(11) NOT NULL,
  `vp_descr` varchar(32) NOT NULL,
  KEY `port_id` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `juniAtmVp`
--

LOCK TABLES `juniAtmVp` WRITE;
/*!40000 ALTER TABLE `juniAtmVp` DISABLE KEYS */;
/*!40000 ALTER TABLE `juniAtmVp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_port_id` int(11) DEFAULT NULL,
  `local_device_id` int(11) NOT NULL,
  `remote_port_id` int(11) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `protocol` varchar(11) DEFAULT NULL,
  `remote_hostname` varchar(128) NOT NULL,
  `remote_device_id` int(11) NOT NULL,
  `remote_port` varchar(128) NOT NULL,
  `remote_platform` varchar(128) NOT NULL,
  `remote_version` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `src_if` (`local_port_id`),
  KEY `dst_if` (`remote_port_id`),
  KEY `local_device_id` (`local_device_id`,`remote_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loadbalancer_rservers`
--

DROP TABLE IF EXISTS `loadbalancer_rservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loadbalancer_rservers` (
  `rserver_id` int(11) NOT NULL AUTO_INCREMENT,
  `farm_id` varchar(128) NOT NULL,
  `device_id` int(11) NOT NULL,
  `StateDescr` varchar(64) NOT NULL,
  PRIMARY KEY (`rserver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loadbalancer_rservers`
--

LOCK TABLES `loadbalancer_rservers` WRITE;
/*!40000 ALTER TABLE `loadbalancer_rservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loadbalancer_rservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loadbalancer_vservers`
--

DROP TABLE IF EXISTS `loadbalancer_vservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loadbalancer_vservers` (
  `classmap_id` int(11) NOT NULL,
  `classmap` varchar(128) NOT NULL,
  `serverstate` varchar(64) NOT NULL,
  `device_id` int(11) NOT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loadbalancer_vservers`
--

LOCK TABLES `loadbalancer_vservers` WRITE;
/*!40000 ALTER TABLE `loadbalancer_vservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `loadbalancer_vservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` text NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `timestamp` datetime NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Medan,Indonesia',3.595196,98.672226,'2017-06-27 16:40:03');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mac_accounting`
--

DROP TABLE IF EXISTS `mac_accounting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mac_accounting` (
  `ma_id` int(11) NOT NULL AUTO_INCREMENT,
  `port_id` int(11) NOT NULL,
  `mac` varchar(32) NOT NULL,
  `in_oid` varchar(128) NOT NULL,
  `out_oid` varchar(128) NOT NULL,
  `bps_out` int(11) NOT NULL,
  `bps_in` int(11) NOT NULL,
  `cipMacHCSwitchedBytes_input` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_input_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedBytes_output_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_input_rate` int(11) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_prev` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_delta` bigint(20) DEFAULT NULL,
  `cipMacHCSwitchedPkts_output_rate` int(11) DEFAULT NULL,
  `poll_time` int(11) DEFAULT NULL,
  `poll_prev` int(11) DEFAULT NULL,
  `poll_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`ma_id`),
  KEY `interface_id` (`port_id`),
  KEY `interface_id_2` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mac_accounting`
--

LOCK TABLES `mac_accounting` WRITE;
/*!40000 ALTER TABLE `mac_accounting` DISABLE KEYS */;
/*!40000 ALTER TABLE `mac_accounting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mempools`
--

DROP TABLE IF EXISTS `mempools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mempools` (
  `mempool_id` int(11) NOT NULL AUTO_INCREMENT,
  `mempool_index` varchar(16) CHARACTER SET latin1 NOT NULL,
  `entPhysicalIndex` int(11) DEFAULT NULL,
  `hrDeviceIndex` int(11) DEFAULT NULL,
  `mempool_type` varchar(32) CHARACTER SET latin1 NOT NULL,
  `mempool_precision` int(11) NOT NULL DEFAULT '1',
  `mempool_descr` varchar(64) CHARACTER SET latin1 NOT NULL,
  `device_id` int(11) NOT NULL,
  `mempool_perc` int(11) NOT NULL,
  `mempool_used` bigint(16) NOT NULL,
  `mempool_free` bigint(16) NOT NULL,
  `mempool_total` bigint(16) NOT NULL,
  `mempool_largestfree` bigint(16) DEFAULT NULL,
  `mempool_lowestfree` bigint(16) DEFAULT NULL,
  `mempool_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mempool_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mempools`
--

LOCK TABLES `mempools` WRITE;
/*!40000 ALTER TABLE `mempools` DISABLE KEYS */;
/*!40000 ALTER TABLE `mempools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mibdefs`
--

DROP TABLE IF EXISTS `mibdefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mibdefs` (
  `module` varchar(255) NOT NULL,
  `mib` varchar(255) NOT NULL,
  `object_type` varchar(255) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `syntax` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `max_access` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `included_by` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`module`,`mib`,`object_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='MIB definitions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mibdefs`
--

LOCK TABLES `mibdefs` WRITE;
/*!40000 ALTER TABLE `mibdefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mibdefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `month`
--

DROP TABLE IF EXISTS `month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `month` (
  `month_id` int(11) NOT NULL,
  `month_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`month_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `month`
--

LOCK TABLES `month` WRITE;
/*!40000 ALTER TABLE `month` DISABLE KEYS */;
INSERT INTO `month` VALUES (1,'January'),(2,'February'),(3,'March'),(4,'April'),(5,'May'),(6,'June'),(7,'July'),(8,'August'),(9,'September'),(10,'October'),(11,'November'),(12,'December');
/*!40000 ALTER TABLE `month` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `munin_plugins`
--

DROP TABLE IF EXISTS `munin_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `munin_plugins` (
  `mplug_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `mplug_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `mplug_instance` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_category` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `mplug_title` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_info` text COLLATE utf8_bin,
  `mplug_vlabel` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `mplug_args` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `mplug_total` binary(1) NOT NULL DEFAULT '0',
  `mplug_graph` binary(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`mplug_id`),
  UNIQUE KEY `UNIQUE` (`device_id`,`mplug_type`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `munin_plugins`
--

LOCK TABLES `munin_plugins` WRITE;
/*!40000 ALTER TABLE `munin_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `munin_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `munin_plugins_ds`
--

DROP TABLE IF EXISTS `munin_plugins_ds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `munin_plugins_ds` (
  `mplug_id` int(11) NOT NULL,
  `ds_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_type` enum('COUNTER','ABSOLUTE','DERIVE','GAUGE') COLLATE utf8_bin NOT NULL DEFAULT 'GAUGE',
  `ds_label` varchar(64) COLLATE utf8_bin NOT NULL,
  `ds_cdef` varchar(255) COLLATE utf8_bin NOT NULL,
  `ds_draw` varchar(64) COLLATE utf8_bin NOT NULL,
  `ds_graph` enum('no','yes') COLLATE utf8_bin NOT NULL DEFAULT 'yes',
  `ds_info` varchar(255) COLLATE utf8_bin NOT NULL,
  `ds_extinfo` text COLLATE utf8_bin NOT NULL,
  `ds_max` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_min` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_negative` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_warning` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_critical` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_colour` varchar(32) COLLATE utf8_bin NOT NULL,
  `ds_sum` text COLLATE utf8_bin NOT NULL,
  `ds_stack` text COLLATE utf8_bin NOT NULL,
  `ds_line` varchar(64) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `splug_id` (`mplug_id`,`ds_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `munin_plugins_ds`
--

LOCK TABLES `munin_plugins_ds` WRITE;
/*!40000 ALTER TABLE `munin_plugins_ds` DISABLE KEYS */;
/*!40000 ALTER TABLE `munin_plugins_ds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `netscaler_vservers`
--

DROP TABLE IF EXISTS `netscaler_vservers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `netscaler_vservers` (
  `vsvr_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `vsvr_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_port` int(8) NOT NULL,
  `vsvr_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_state` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `vsvr_clients` int(11) NOT NULL,
  `vsvr_server` int(11) NOT NULL,
  `vsvr_req_rate` int(11) NOT NULL,
  `vsvr_bps_in` int(11) NOT NULL,
  `vsvr_bps_out` int(11) NOT NULL,
  PRIMARY KEY (`vsvr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `netscaler_vservers`
--

LOCK TABLES `netscaler_vservers` WRITE;
/*!40000 ALTER TABLE `netscaler_vservers` DISABLE KEYS */;
/*!40000 ALTER TABLE `netscaler_vservers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `notifications_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `source` varchar(255) NOT NULL DEFAULT '',
  `checksum` varchar(128) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notifications_id`),
  UNIQUE KEY `checksum` (`checksum`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'Hello World!','This is the first notification. We will post these whenever an upcoming major change is about to happen.','http://www.librenms.org/notifications.rss','e9bf74634c19f09e03cdbd270563a258bc2ad1f4ab7d20de06ee60d2a4ebc3057c6779043b7475924cfb58ba505e6d0c752e4ec7b79c251c2a77d3426e34b12a','2015-09-26 17:00:00'),(2,'Important update being released','We are releasing an important update within the next 24 hours which provides some additional functionality when running daily.sh. Whilst we have done everything we can to test this across as many platforms as possible we thought it was worth letting you know.\\n\\nIf you notice issues with further updates after this one then please get in touch with us on either IRC or GitHub.','http://www.librenms.org/notifications.rss','cbcf6be1a832606d23f4d9093c7fa7a12bd6930701868050de00a13a3fe57a9fa48d75dab78f1f20d25d19633468b7849562cc45430bb4cc9b4b8a8eda632a99','2015-12-03 17:00:00'),(3,'Poller changes','Earlier this month we released some updates which enable alpha versions of InfluxDB and MIB-based polling.\\nWe have some further updates planned for the near future which may affect polling, so if you are running a mission-critical LibreNMS instance, you may wish to consider disabling updates for the time being.\\nIf you notice any issues, please get in touch with us via IRC or GitHub.','http://www.librenms.org/notifications.rss','e3c2426b447c0bf4c9f50d72dc6b14756bc86e59b3aefced08589d09763514269904ce713f9572723d2194263cc3728fd13ccef7f9b0045cbc1b8a3ac31dfb44','2016-01-09 17:00:00'),(4,'Redback SmartEdge OS change','On January 19th we will be merging a PR that will change devices currently detected as redback to be detected as seos.\\n\\nIf you have any alert rules or device groups that rely on this OS definition then you will need to update those rules to include seos.','http://www.librenms.org/notifications.rss','22fc9b7f8d0d73c0e84489717d3d36126ec1f718ea05324c92ccdc9e33fe6b929f53514b7a84dbf97fd6cf9abc282a60cd40b400b7817105d54f7289923b1ae0','2016-01-17 17:00:00'),(5,'Cisco syslog parsing changes','We have overhauled the Cisco syslog parsing.  Please monitor your syslog entries from Cisco IOS, IOSXR, and CatOS devices.  If you notice any issues please open a new issue on GitHub and include the original syslog message.','misc/notifications.rss','0a93529900a8bf1e7b2c7e8f52ab245747df3f8458e10edb76e0404057f14c931476ab4d9523b974d47018633cef70b7cbc99025e0b47f04d0144bff0de16363','2016-03-02 17:00:00'),(6,'Billing Data Changes','We have updated the method for storing billing data, removing a lot of information stored in \n   the database which was not used.  Please check that your billing data is updating as expected, and \n   if you notice any problems open an issue on GitHub.','misc/notifications.rss','779f4a4f9e4d65d3a00f5690d27a030c3cbebabd81eed33bd1a1b2ede6556eeda59fd2a3a3f3d0069f0de3409eb54256d31164525c830edee17fd45a7ab04498','2016-03-06 17:00:00'),(7,'Introduction of default purge interval or alert history','We have introduced a purge setting for alert history which is set to 365 days as default. If you would like to update this then please \n   set $config[\'alert_log_purge\'] to a custom value (set in days).','misc/notifications.rss','0a77fb4b52c8875343a5bfbf6b7946d7d33c28ef821c274f53de73ee9cfde9d9df6d804114b48ea49c0641806f14b29acba6c844f38ab1680df324e0489f71c4','2016-05-02 17:00:00'),(8,'Imagemagick vulnerability','A vulnerability in ImageMagick has been released, please see https://imagetragick.com/ for more details.\\n\\nLibreNMS uses ImageMagick for pdf generation, we advise that you add the temporary fix in on the aforementioned website until patches for your distribution are available to update ImageMagick.','http://www.librenms.org/notifications.rss','1de2979056fe29c86abf5e2eee546e249ee22a7e850ad43123e42c306b9e0d0c76c0e97505821ee171323c0fc904fb0b9e5faefdb9153e0edc290fa983c1f4a5','2016-05-03 17:00:00'),(9,'Component Alerting Change','On July 25th we will be merging a PR that changes the status codes that are used for the component feature. If you have alerting rules that reference component.status, %macros.component_alert or %macros.component_normal please disable these until after the PR has been merged. Updated documentation will be merged with the PR to: http://docs.librenms.org/Extensions/Component/#alert','http://www.librenms.org/notifications.rss','036e0ae37d936c5dc155ed489b63f41be79e7abcbd0ccf4229a93da0a91848bb519e87604c1cc59a29b6e3a5ffb8beda8044286bc45f883956c6a66e7f46711f','2016-07-24 17:00:00'),(10,'The Alerts Menu has moved','The Alerts menu has been relocated to a top level menu on the far right.  To make room for this, Plugins have been moved\n   to the overview menu.','misc/notifications.rss','91e67bd37c425d154ac3cc336d1aa6888290253629fa4024e60559a2a20edbb0e14899ce129a97ff6800fd207d3bad40096869448c6a340f6c044e7c3fffdafe','2016-09-12 17:00:00'),(11,'Multimatic UPS detection changes','On December 21st we will be merging a PR that will change devices currently detected as multimatic to be detected as generex-ups.\\n\\nIf you have any alert rules or device groups that rely on this OS definition then you will need to update those rules to include generex-ups.','http://www.librenms.org/notifications.rss','75b6edfe8a162dc5414e12420da3950cd9fecc452c9fb6e2e5578698de5b3200bcd08ff194224db6acaed91faaa0647d98b711d2132edd1185408b0791b0e9e2','2016-12-18 17:00:00'),(12,'NUT Ups Application monitoring update','On December 20th we will be merging a PR that fixes the polling of data for NUT Ups devices.\\n\\nUnfortunately this will require users to delete the existing data and allow it to be re-created.\\n\\nTo do this, you can delete rrd files app-ups-nut-ID.rrd. ID will be a unique ID. After removing this file then data collection will start again.','http://www.librenms.org/notifications.rss','c5efb05c52f95df86dd3138dd90a1e287af142ff9dff1a5a52d8528d7c59cfea016fe16c969e9f952ac64d195100dfbe33d8386a385329207864514331eba1e4','2016-12-18 17:00:00'),(13,'Large schema update due to be released','Please see: https://community.librenms.org/t/large-schema-update-being-released/637 for further info.','http://www.librenms.org/notifications.rss','1dd85dc0cc62c6c84da356897048c73aafeea5d533b32813a7d409b968235cc53e24a614d736739621ca779f5ac2b8ed90fc99dbf19151d0359c24099a11edac','2017-02-22 17:00:00'),(14,'Change to extremeware and xos discovery','On the 10th of March, we are changing the way that OS detection is done for Extremeware and XOS devices, this could lead to the OS changing so if you have alerting or groups that reference these os names then please be aware that you will need to update them.','http://www.librenms.org/notifications.rss','09399fca57e340bf330aea8054c659c585c265ae6fe029a106fd088af7ed71bd971c18e330500600d39f018382425e521a1dfeb7487caf66691e99b574a689a8','2017-03-06 17:00:00'),(15,'Change to Ubiquiti Airfibre device type','On the 24th of March, we are changing device type for Ubiquiti AirFibre from network to wireless. See https://github.com/librenms/librenms/pull/6160 for PR.','http://www.librenms.org/notifications.rss','5dd4cc9d5075610638d2e36ec290d886e84a0d2156f11c8dba9612ab29cc8f5493b92c4a7cb514bee3f1cba68c254c83b8c263bc09925da2e70acbccde6eaed3','2017-03-20 17:00:00'),(16,'Change to some DNOS discovery','On the 24th of March, we are changing the detection of some Dell DNOS devices to PowerConnect. This is to show the difference between v6 and v9 of the DNOS OS. See https://github.com/librenms/librenms/pull/6206 for PR.','http://www.librenms.org/notifications.rss','e2e20560a5b18930ae62d730e4ec0442379e752f61d38afd97fd65e2518a61a2c02e11b3f6e79417bd7d14b120e05f8ab40b04802c21f500101a69295b3b9d7c','2017-03-22 17:00:00');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications_attribs`
--

DROP TABLE IF EXISTS `notifications_attribs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications_attribs` (
  `attrib_id` int(11) NOT NULL AUTO_INCREMENT,
  `notifications_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`attrib_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications_attribs`
--

LOCK TABLES `notifications_attribs` WRITE;
/*!40000 ALTER TABLE `notifications_attribs` DISABLE KEYS */;
INSERT INTO `notifications_attribs` VALUES (1,12,1,'read','1'),(2,10,1,'read','1'),(3,11,1,'read','1'),(4,9,1,'read','1'),(5,8,1,'read','1'),(6,7,1,'read','1'),(7,6,1,'read','1'),(8,5,1,'read','1'),(9,4,1,'read','1'),(10,3,1,'read','1'),(11,2,1,'read','1'),(12,1,1,'read','1'),(13,12,2,'read','1'),(14,11,2,'read','1'),(15,10,2,'read','1'),(16,9,2,'read','1'),(17,8,2,'read','1'),(18,7,2,'read','1'),(19,6,2,'read','1'),(20,5,2,'read','1'),(21,4,2,'read','1'),(22,3,2,'read','1'),(23,2,2,'read','1'),(24,1,2,'read','1'),(25,13,1,'read','1'),(26,16,1,'read','1'),(27,14,1,'read','1'),(28,15,1,'read','1'),(29,16,2,'read','1'),(30,15,2,'read','1'),(31,14,2,'read','1'),(32,13,2,'read','1');
/*!40000 ALTER TABLE `notifications_attribs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_areas`
--

DROP TABLE IF EXISTS `ospf_areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_areas` (
  `device_id` int(11) NOT NULL,
  `ospfAreaId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAuthType` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ospfImportAsExtern` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ospfSpfRuns` int(11) NOT NULL,
  `ospfAreaBdrRtrCount` int(11) NOT NULL,
  `ospfAsBdrRtrCount` int(11) NOT NULL,
  `ospfAreaLsaCount` int(11) NOT NULL,
  `ospfAreaLsaCksumSum` int(11) NOT NULL,
  `ospfAreaSummary` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAreaStatus` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_area` (`device_id`,`ospfAreaId`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_areas`
--

LOCK TABLES `ospf_areas` WRITE;
/*!40000 ALTER TABLE `ospf_areas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_instances`
--

DROP TABLE IF EXISTS `ospf_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_instances` (
  `device_id` int(11) NOT NULL,
  `ospf_instance_id` int(11) NOT NULL,
  `ospfRouterId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAdminStat` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfVersionNumber` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAreaBdrRtrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfASBdrRtrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfExternLsaCount` int(11) NOT NULL,
  `ospfExternLsaCksumSum` int(11) NOT NULL,
  `ospfTOSSupport` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfOriginateNewLsas` int(11) NOT NULL,
  `ospfRxNewLsas` int(11) NOT NULL,
  `ospfExtLsdbLimit` int(11) DEFAULT NULL,
  `ospfMulticastExtensions` int(11) DEFAULT NULL,
  `ospfExitOverflowInterval` int(11) DEFAULT NULL,
  `ospfDemandExtensions` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_instance_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_instances`
--

LOCK TABLES `ospf_instances` WRITE;
/*!40000 ALTER TABLE `ospf_instances` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_nbrs`
--

DROP TABLE IF EXISTS `ospf_nbrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_nbrs` (
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `ospf_nbr_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrIpAddr` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrAddressLessIndex` int(11) NOT NULL,
  `ospfNbrRtrId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrOptions` int(11) NOT NULL,
  `ospfNbrPriority` int(11) NOT NULL,
  `ospfNbrState` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrEvents` int(11) NOT NULL,
  `ospfNbrLsRetransQLen` int(11) NOT NULL,
  `ospfNbmaNbrStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbmaNbrPermanence` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfNbrHelloSuppressed` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_nbr_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_nbrs`
--

LOCK TABLES `ospf_nbrs` WRITE;
/*!40000 ALTER TABLE `ospf_nbrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_nbrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospf_ports`
--

DROP TABLE IF EXISTS `ospf_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospf_ports` (
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `ospf_port_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfIfIpAddress` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfAddressLessIf` int(11) NOT NULL,
  `ospfIfAreaId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ospfIfType` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfAdminStat` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfRtrPriority` int(11) DEFAULT NULL,
  `ospfIfTransitDelay` int(11) DEFAULT NULL,
  `ospfIfRetransInterval` int(11) DEFAULT NULL,
  `ospfIfHelloInterval` int(11) DEFAULT NULL,
  `ospfIfRtrDeadInterval` int(11) DEFAULT NULL,
  `ospfIfPollInterval` int(11) DEFAULT NULL,
  `ospfIfState` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfDesignatedRouter` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfBackupDesignatedRouter` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfEvents` int(11) DEFAULT NULL,
  `ospfIfAuthKey` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfStatus` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfMulticastForwarding` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfDemand` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ospfIfAuthType` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  UNIQUE KEY `device_id` (`device_id`,`ospf_port_id`,`context_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospf_ports`
--

LOCK TABLES `ospf_ports` WRITE;
/*!40000 ALTER TABLE `ospf_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospf_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `pkg_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `manager` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL,
  `version` varchar(255) COLLATE utf8_bin NOT NULL,
  `build` varchar(64) COLLATE utf8_bin NOT NULL,
  `arch` varchar(16) COLLATE utf8_bin NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pkg_id`),
  UNIQUE KEY `unique_key` (`device_id`,`name`,`manager`,`arch`,`version`,`build`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perf_times`
--

DROP TABLE IF EXISTS `perf_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perf_times` (
  `type` varchar(8) CHARACTER SET latin1 NOT NULL,
  `doing` varchar(64) CHARACTER SET latin1 NOT NULL,
  `start` int(11) NOT NULL,
  `duration` double(8,2) NOT NULL,
  `devices` int(11) NOT NULL,
  `poller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perf_times`
--

LOCK TABLES `perf_times` WRITE;
/*!40000 ALTER TABLE `perf_times` DISABLE KEYS */;
INSERT INTO `perf_times` VALUES ('pollbill','',1497252003,0.06,0,'ubuntu14vm\n'),('poll','21',1497252003,2.36,1,'ubuntu14vm\n'),('pollbill','',1498556102,0.86,0,'ubuntu16\n'),('discover','new',1498556102,2.14,1,'ubuntu16\n'),('poll','22',1498556108,1.54,1,'ubuntu16\n'),('pollbill','',1498556402,0.09,0,'ubuntu16\n'),('poll','22',1498556402,1.97,1,'ubuntu16\n'),('pollbill','',1498556702,0.10,0,'ubuntu16\n'),('poll','22',1498556702,1.17,1,'ubuntu16\n'),('pollbill','',1498557002,0.02,0,'ubuntu16\n'),('poll','22',1498557002,1.20,1,'ubuntu16\n'),('pollbill','',1498557302,0.07,0,'ubuntu16\n'),('poll','22',1498557302,1.15,1,'ubuntu16\n'),('pollbill','',1498557602,0.21,0,'ubuntu16\n'),('poll','22',1498557602,1.11,1,'ubuntu16\n'),('pollbill','',1498557902,0.05,0,'ubuntu16\n'),('poll','22',1498557903,1.22,1,'ubuntu16\n'),('pollbill','',1498558202,0.15,0,'ubuntu16\n'),('poll','22',1498558202,1.13,1,'ubuntu16\n'),('pollbill','',1498558502,0.08,0,'ubuntu16\n'),('poll','22',1498558502,1.15,1,'ubuntu16\n'),('pollbill','',1498558802,0.16,0,'ubuntu16\n'),('poll','22',1498558802,1.14,1,'ubuntu16\n'),('pollbill','',1498559102,0.03,0,'ubuntu16\n'),('poll','22',1498559103,1.19,1,'ubuntu16\n'),('pollbill','',1498559402,0.14,0,'ubuntu16\n'),('poll','22',1498559402,1.19,1,'ubuntu16\n'),('pollbill','',1498559702,0.06,0,'ubuntu16\n'),('poll','22',1498559702,1.14,1,'ubuntu16\n'),('pollbill','',1498560002,0.14,0,'ubuntu16\n'),('poll','22',1498560003,1.33,1,'ubuntu16\n'),('pollbill','',1498560302,0.18,0,'ubuntu16\n'),('poll','22',1498560302,1.35,1,'ubuntu16\n'),('pollbill','',1498560602,0.40,0,'ubuntu16\n'),('poll','22',1498560603,1.47,1,'ubuntu16\n'),('pollbill','',1498560902,0.34,0,'ubuntu16\n'),('poll','22',1498560902,1.33,1,'ubuntu16\n'),('pollbill','',1498561202,0.07,0,'ubuntu16\n'),('poll','22',1498561202,1.29,1,'ubuntu16\n'),('pollbill','',1498561502,0.05,0,'ubuntu16\n'),('poll','22',1498561502,1.28,1,'ubuntu16\n'),('pollbill','',1498561802,0.16,0,'ubuntu16\n'),('poll','22',1498561803,1.47,1,'ubuntu16\n'),('pollbill','',1498562102,0.04,0,'ubuntu16\n'),('pollbill','',1498562102,0.01,0,'ubuntu16-02\n'),('discover','new',1498562102,0.90,1,'ubuntu16\n'),('poll','22',1498562102,1.41,1,'ubuntu16\n'),('poll','23',1498562104,2.70,1,'ubuntu16-02\n'),('poll','23',1498562239,1.29,1,'ubuntu16-02\n'),('poll','23',1498562396,1.29,1,'ubuntu16-02\n'),('pollbill','',1498562402,0.09,0,'ubuntu16-02\n'),('pollbill','',1498562402,0.07,0,'ubuntu16\n'),('poll','23',1498562402,1.56,1,'ubuntu16-02\n'),('poll','22',1498562402,1.40,1,'ubuntu16\n'),('pollbill','',1498562701,0.05,0,'ubuntu16-02\n'),('pollbill','',1498562702,0.04,0,'ubuntu16\n'),('poll','23',1498562701,1.57,1,'ubuntu16-02\n'),('poll','22',1498562703,1.31,1,'ubuntu16\n'),('pollbill','',1498563002,0.07,0,'ubuntu16\n'),('pollbill','',1498563002,0.04,0,'ubuntu16-02\n'),('poll','22',1498563002,1.51,1,'ubuntu16\n'),('poll','23',1498563002,1.53,1,'ubuntu16-02\n'),('poll','23',1498563062,1.31,1,'ubuntu16-02\n'),('poll','23',1498563073,1.24,1,'ubuntu16-02\n'),('poll','22',1498563117,1.40,1,'ubuntu16\n'),('poll','23',1498563150,1.54,1,'ubuntu16-02\n'),('poll','23',1498563161,1.30,1,'ubuntu16-02\n'),('poll','23',1498563275,1.43,1,'ubuntu16-02\n'),('poll','23',1498563297,1.42,1,'ubuntu16-02\n'),('pollbill','',1498563302,0.01,0,'ubuntu16\n'),('pollbill','',1498563302,0.05,0,'ubuntu16-02\n'),('poll','22',1498563302,1.54,1,'ubuntu16\n'),('poll','23',1498563302,1.63,1,'ubuntu16-02\n'),('poll','22',1498563497,1.29,1,'ubuntu16\n'),('poll','22',1498563517,1.55,1,'ubuntu16\n'),('poll','23',1498563541,40.72,1,'ubuntu16-02\n'),('pollbill','',1498563602,0.04,0,'ubuntu16-02\n'),('pollbill','',1498563603,0.03,0,'ubuntu16\n'),('poll','23',1498563602,1.07,1,'ubuntu16-02\n'),('poll','22',1498563603,1.44,1,'ubuntu16\n'),('pollbill','',1498563902,0.05,0,'ubuntu16-02\n'),('pollbill','',1498563902,0.07,0,'ubuntu16\n'),('discover','new',1498563902,1.02,1,'ubuntu16\n'),('poll','22',1498563903,1.60,1,'ubuntu16\n'),('poll','24',1498563903,2.56,1,'ubuntu16-02\n'),('poll','23',1498563903,2.56,1,'ubuntu16-02\n'),('pollbill','',1498564202,0.20,0,'ubuntu16\n'),('pollbill','',1498564202,0.30,0,'ubuntu16-02\n'),('discover','new',1498564202,1.12,1,'ubuntu16\n'),('poll','22',1498564202,1.49,1,'ubuntu16\n'),('poll','24',1498564202,3.24,1,'ubuntu16-02\n'),('poll','23',1498564202,3.22,1,'ubuntu16-02\n'),('poll','25',1498564202,3.31,1,'ubuntu16-02\n'),('pollbill','',1498564501,0.06,0,'ubuntu16-02\n'),('pollbill','',1498564503,0.02,0,'ubuntu16\n'),('discover','new',1498564502,1.20,1,'ubuntu16\n'),('poll','22',1498564503,1.80,1,'ubuntu16\n'),('poll','26',1498564502,2.86,1,'ubuntu16-02\n'),('poll','25',1498564502,4.06,1,'ubuntu16-02\n'),('poll','24',1498564502,4.04,1,'ubuntu16-02\n'),('poll','23',1498564502,4.08,1,'ubuntu16-02\n'),('pollbill','',1498564802,0.06,0,'ubuntu16-02\n'),('pollbill','',1498564802,0.04,0,'ubuntu16\n'),('poll','22',1498564803,1.43,1,'ubuntu16\n'),('poll','26',1498564803,2.74,1,'ubuntu16-02\n'),('poll','24',1498564803,3.81,1,'ubuntu16-02\n'),('poll','25',1498564803,3.78,1,'ubuntu16-02\n'),('poll','23',1498564803,3.86,1,'ubuntu16-02\n'),('pollbill','',1498565102,0.13,0,'ubuntu16\n'),('pollbill','',1498565102,0.08,0,'ubuntu16-02\n'),('poll','23',1498565103,1.36,1,'ubuntu16-02\n'),('poll','26',1498565103,2.56,1,'ubuntu16-02\n'),('poll','25',1498565103,3.17,1,'ubuntu16-02\n'),('poll','24',1498565103,3.16,1,'ubuntu16-02\n'),('poll','22',1498565102,32.40,1,'ubuntu16\n'),('pollbill','',1498565402,0.10,0,'ubuntu16\n'),('pollbill','',1498565402,0.04,0,'ubuntu16-02\n'),('discover','new',1498565402,1.05,1,'ubuntu16\n'),('poll','23',1498565402,1.32,1,'ubuntu16-02\n'),('poll','24',1498565402,3.52,1,'ubuntu16-02\n'),('poll','25',1498565402,3.50,1,'ubuntu16-02\n'),('poll','27',1498565403,3.57,1,'ubuntu16-02\n'),('poll','22',1498565402,9.63,1,'ubuntu16\n'),('pollbill','',1498565701,0.03,0,'ubuntu16-02\n'),('pollbill','',1498565702,0.10,0,'ubuntu16\n'),('poll','23',1498565702,1.32,1,'ubuntu16-02\n'),('poll','25',1498565702,2.86,1,'ubuntu16-02\n'),('poll','24',1498565702,2.84,1,'ubuntu16-02\n'),('poll','27',1498565702,2.96,1,'ubuntu16-02\n'),('poll','22',1498565702,22.96,1,'ubuntu16\n'),('pollbill','',1498566002,0.10,0,'ubuntu16-02\n'),('pollbill','',1498566002,0.03,0,'ubuntu16\n'),('poll','22',1498566003,1.00,1,'ubuntu16\n'),('poll','25',1498566002,3.51,1,'ubuntu16-02\n'),('poll','24',1498566002,3.52,1,'ubuntu16-02\n'),('poll','27',1498566002,3.66,1,'ubuntu16-02\n'),('poll','23',1498566002,67.22,1,'ubuntu16-02\n'),('pollbill','',1498566301,0.04,0,'ubuntu16-02\n'),('pollbill','',1498566302,0.03,0,'ubuntu16\n'),('poll','24',1498566302,2.73,1,'ubuntu16-02\n'),('poll','25',1498566302,2.77,1,'ubuntu16-02\n'),('poll','27',1498566302,2.89,1,'ubuntu16-02\n'),('poll','22',1498566302,4.76,1,'ubuntu16\n'),('poll','23',1498566302,29.46,1,'ubuntu16-02\n'),('pollbill','',1498566602,0.07,0,'ubuntu16\n'),('pollbill','',1498566602,0.03,0,'ubuntu16-02\n'),('poll','22',1498566602,1.07,1,'ubuntu16\n'),('poll','25',1498566602,3.20,1,'ubuntu16-02\n'),('poll','24',1498566602,3.19,1,'ubuntu16-02\n'),('poll','27',1498566602,3.37,1,'ubuntu16-02\n'),('poll','23',1498566602,4.41,1,'ubuntu16-02\n'),('pollbill','',1498566902,0.08,0,'ubuntu16-02\n'),('pollbill','',1498566902,0.07,0,'ubuntu16\n'),('poll','25',1498566902,2.25,1,'ubuntu16-02\n'),('poll','24',1498566902,2.26,1,'ubuntu16-02\n'),('poll','23',1498566902,3.60,1,'ubuntu16-02\n'),('poll','27',1498566902,4.22,1,'ubuntu16-02\n'),('poll','22',1498566902,13.19,1,'ubuntu16\n'),('pollbill','',1498567202,0.07,0,'ubuntu16-02\n'),('pollbill','',1498567202,0.06,0,'ubuntu16\n'),('poll','24',1498567203,2.57,1,'ubuntu16-02\n'),('poll','25',1498567203,2.55,1,'ubuntu16-02\n'),('poll','27',1498567203,2.83,1,'ubuntu16-02\n'),('poll','22',1498567203,3.82,1,'ubuntu16\n'),('poll','23',1498567203,41.76,1,'ubuntu16-02\n'),('pollbill','',1498567502,0.07,0,'ubuntu16\n'),('pollbill','',1498567502,0.04,0,'ubuntu16-02\n'),('poll','24',1498567502,2.26,1,'ubuntu16-02\n'),('poll','25',1498567502,2.29,1,'ubuntu16-02\n'),('poll','22',1498567502,3.63,1,'ubuntu16\n'),('poll','27',1498567502,4.23,1,'ubuntu16-02\n'),('poll','23',1498567502,20.23,1,'ubuntu16-02\n'),('pollbill','',1498567801,0.04,0,'ubuntu16-02\n'),('pollbill','',1498567802,0.02,0,'ubuntu16\n'),('poll','24',1498567802,3.15,1,'ubuntu16-02\n'),('poll','25',1498567802,3.17,1,'ubuntu16-02\n'),('poll','27',1498567802,3.32,1,'ubuntu16-02\n'),('poll','23',1498567802,11.68,1,'ubuntu16-02\n'),('poll','22',1498567802,26.49,1,'ubuntu16\n'),('pollbill','',1498568102,0.11,0,'ubuntu16-02\n'),('pollbill','',1498568102,0.09,0,'ubuntu16\n'),('poll','23',1498568102,1.32,1,'ubuntu16-02\n'),('poll','22',1498568103,1.00,1,'ubuntu16\n'),('poll','25',1498568102,2.66,1,'ubuntu16-02\n'),('poll','24',1498568102,2.80,1,'ubuntu16-02\n'),('poll','27',1498568102,2.97,1,'ubuntu16-02\n'),('pollbill','',1498568401,0.08,0,'ubuntu16-02\n'),('pollbill','',1498568402,0.05,0,'ubuntu16\n'),('poll','25',1498568402,2.81,1,'ubuntu16-02\n'),('poll','24',1498568402,2.84,1,'ubuntu16-02\n'),('poll','27',1498568402,3.04,1,'ubuntu16-02\n'),('poll','23',1498568402,3.91,1,'ubuntu16-02\n'),('poll','22',1498568402,40.26,1,'ubuntu16\n'),('pollbill','',1498568702,0.04,0,'ubuntu16-02\n'),('pollbill','',1498568702,0.10,0,'ubuntu16\n'),('poll','25',1498568702,2.32,1,'ubuntu16-02\n'),('poll','24',1498568702,2.33,1,'ubuntu16-02\n'),('poll','27',1498568702,4.49,1,'ubuntu16-02\n'),('poll','22',1498568703,41.64,1,'ubuntu16\n'),('poll','23',1498568702,61.82,1,'ubuntu16-02\n'),('pollbill','',1498569002,0.04,0,'ubuntu16-02\n'),('pollbill','',1498569002,0.11,0,'ubuntu16\n'),('poll','23',1498569002,1.24,1,'ubuntu16-02\n'),('poll','25',1498569002,2.65,1,'ubuntu16-02\n'),('poll','24',1498569002,2.67,1,'ubuntu16-02\n'),('poll','27',1498569002,2.81,1,'ubuntu16-02\n'),('poll','22',1498569003,44.72,1,'ubuntu16\n'),('pollbill','',1498569302,0.10,0,'ubuntu16\n'),('pollbill','',1498569302,0.08,0,'ubuntu16-02\n'),('poll','24',1498569303,2.72,1,'ubuntu16-02\n'),('poll','25',1498569303,2.75,1,'ubuntu16-02\n'),('poll','27',1498569303,2.98,1,'ubuntu16-02\n'),('poll','22',1498569303,23.58,1,'ubuntu16\n'),('poll','23',1498569303,53.87,1,'ubuntu16-02\n'),('pollbill','',1498569602,0.05,0,'ubuntu16-02\n'),('pollbill','',1498569603,0.01,0,'ubuntu16\n'),('poll','22',1498569603,1.08,1,'ubuntu16\n'),('poll','25',1498569602,2.66,1,'ubuntu16-02\n'),('poll','24',1498569602,2.79,1,'ubuntu16-02\n'),('poll','27',1498569602,2.88,1,'ubuntu16-02\n'),('poll','23',1498569602,3.49,1,'ubuntu16-02\n'),('pollbill','',1498569902,0.06,0,'ubuntu16\n'),('pollbill','',1498569902,0.05,0,'ubuntu16-02\n'),('poll','22',1498569902,1.04,1,'ubuntu16\n'),('poll','25',1498569903,2.53,1,'ubuntu16-02\n'),('poll','24',1498569903,2.60,1,'ubuntu16-02\n'),('poll','27',1498569903,2.84,1,'ubuntu16-02\n'),('poll','23',1498569903,63.82,1,'ubuntu16-02\n'),('pollbill','',1498570202,0.05,0,'ubuntu16-02\n'),('pollbill','',1498570203,0.02,0,'ubuntu16\n'),('poll','25',1498570202,2.84,1,'ubuntu16-02\n'),('poll','24',1498570202,2.84,1,'ubuntu16-02\n'),('poll','27',1498570202,3.01,1,'ubuntu16-02\n'),('poll','22',1498570203,29.81,1,'ubuntu16\n'),('poll','23',1498570202,52.43,1,'ubuntu16-02\n'),('pollbill','',1498570501,0.05,0,'ubuntu16-02\n'),('pollbill','',1498570502,0.07,0,'ubuntu16\n'),('poll','24',1498570502,3.03,1,'ubuntu16-02\n'),('poll','25',1498570502,3.07,1,'ubuntu16-02\n'),('poll','27',1498570502,3.25,1,'ubuntu16-02\n'),('poll','22',1498570502,27.73,1,'ubuntu16\n'),('poll','23',1498570502,47.38,1,'ubuntu16-02\n'),('pollbill','',1498570802,0.05,0,'ubuntu16-02\n'),('pollbill','',1498570803,0.04,0,'ubuntu16\n'),('poll','22',1498570803,1.05,1,'ubuntu16\n'),('poll','25',1498570803,2.73,1,'ubuntu16-02\n'),('poll','24',1498570803,2.76,1,'ubuntu16-02\n'),('poll','27',1498570803,3.13,1,'ubuntu16-02\n'),('poll','23',1498570803,20.49,1,'ubuntu16-02\n'),('pollbill','',1498571102,0.24,0,'ubuntu16-02\n'),('poll','24',1498571102,4.01,1,'ubuntu16-02\n'),('poll','25',1498571102,4.00,1,'ubuntu16-02\n'),('poll','27',1498571102,4.05,1,'ubuntu16-02\n'),('poll','23',1498571102,4.05,1,'ubuntu16-02\n'),('pollbill','',1498571110,0.16,0,'ubuntu16\n'),('poll','22',1498571111,2.85,1,'ubuntu16\n'),('pollbill','',1498571401,0.04,0,'ubuntu16-02\n'),('pollbill','',1498571402,0.03,0,'ubuntu16\n'),('poll','22',1498571403,1.43,1,'ubuntu16\n'),('poll','24',1498571402,3.65,1,'ubuntu16-02\n'),('poll','23',1498571402,3.74,1,'ubuntu16-02\n'),('poll','25',1498571402,3.77,1,'ubuntu16-02\n'),('poll','27',1498571402,3.84,1,'ubuntu16-02\n'),('pollbill','',1498571702,0.11,0,'ubuntu16-02\n'),('pollbill','',1498571702,0.01,0,'ubuntu16\n'),('poll','22',1498571702,1.49,1,'ubuntu16\n'),('poll','25',1498571703,3.89,1,'ubuntu16-02\n'),('poll','24',1498571703,4.01,1,'ubuntu16-02\n'),('poll','23',1498571703,3.98,1,'ubuntu16-02\n'),('poll','27',1498571703,4.14,1,'ubuntu16-02\n'),('pollbill','',1498572002,0.18,0,'ubuntu16-02\n'),('pollbill','',1498572003,0.08,0,'ubuntu16\n'),('poll','22',1498572003,1.73,1,'ubuntu16\n'),('poll','24',1498572002,4.35,1,'ubuntu16-02\n'),('poll','25',1498572002,4.78,1,'ubuntu16-02\n'),('poll','23',1498572002,5.10,1,'ubuntu16-02\n'),('poll','27',1498572002,5.47,1,'ubuntu16-02\n'),('pollbill','',1498572602,20.84,0,'ubuntu16-02\n'),('poll','23',1498572603,23.89,1,'ubuntu16-02\n'),('poll','25',1498572603,23.82,1,'ubuntu16-02\n'),('poll','24',1498572603,23.70,1,'ubuntu16-02\n'),('poll','27',1498572603,23.94,1,'ubuntu16-02\n'),('pollbill','',1498572614,14.54,0,'ubuntu16\n'),('poll','22',1498572615,14.34,1,'ubuntu16\n'),('pollbill','',1498572901,0.24,0,'ubuntu16-02\n'),('pollbill','',1498572903,0.28,0,'ubuntu16\n'),('poll','22',1498572903,1.49,1,'ubuntu16\n'),('poll','25',1498572902,3.37,1,'ubuntu16-02\n'),('poll','23',1498572902,3.44,1,'ubuntu16-02\n'),('poll','24',1498572902,3.41,1,'ubuntu16-02\n'),('poll','27',1498572902,4.48,1,'ubuntu16-02\n'),('pollbill','',1498573202,0.29,0,'ubuntu16-02\n'),('pollbill','',1498573202,0.43,0,'ubuntu16\n'),('poll','22',1498573203,1.34,1,'ubuntu16\n'),('poll','25',1498573203,3.00,1,'ubuntu16-02\n'),('poll','24',1498573203,3.10,1,'ubuntu16-02\n'),('poll','23',1498573203,3.07,1,'ubuntu16-02\n'),('poll','27',1498573203,4.52,1,'ubuntu16-02\n'),('pollbill','',1498573502,0.34,0,'ubuntu16-02\n'),('pollbill','',1498573502,0.45,0,'ubuntu16\n'),('poll','22',1498573502,1.45,1,'ubuntu16\n'),('poll','24',1498573502,2.97,1,'ubuntu16-02\n'),('poll','25',1498573502,2.97,1,'ubuntu16-02\n'),('poll','23',1498573502,3.03,1,'ubuntu16-02\n'),('poll','27',1498573502,4.62,1,'ubuntu16-02\n'),('pollbill','',1498573801,0.24,0,'ubuntu16-02\n'),('pollbill','',1498573802,0.36,0,'ubuntu16\n'),('poll','22',1498573803,1.47,1,'ubuntu16\n'),('poll','24',1498573802,3.24,1,'ubuntu16-02\n'),('poll','25',1498573802,3.38,1,'ubuntu16-02\n'),('poll','23',1498573802,3.41,1,'ubuntu16-02\n'),('poll','27',1498573802,4.54,1,'ubuntu16-02\n'),('pollbill','',1498574102,0.25,0,'ubuntu16-02\n'),('pollbill','',1498574102,0.27,0,'ubuntu16\n'),('poll','22',1498574103,1.43,1,'ubuntu16\n'),('poll','25',1498574103,4.21,1,'ubuntu16-02\n'),('poll','23',1498574103,4.34,1,'ubuntu16-02\n'),('poll','24',1498574103,4.37,1,'ubuntu16-02\n'),('poll','27',1498574103,4.47,1,'ubuntu16-02\n'),('pollbill','',1498574402,0.24,0,'ubuntu16-02\n'),('pollbill','',1498574403,0.26,0,'ubuntu16\n'),('poll','22',1498574403,1.46,1,'ubuntu16\n'),('poll','25',1498574402,3.01,1,'ubuntu16-02\n'),('poll','24',1498574402,3.04,1,'ubuntu16-02\n'),('poll','23',1498574402,3.14,1,'ubuntu16-02\n'),('poll','27',1498574402,4.42,1,'ubuntu16-02\n'),('pollbill','',1498574701,0.29,0,'ubuntu16-02\n'),('pollbill','',1498574702,0.38,0,'ubuntu16\n'),('poll','22',1498574702,1.35,1,'ubuntu16\n'),('poll','24',1498574702,3.32,1,'ubuntu16-02\n'),('poll','25',1498574702,3.32,1,'ubuntu16-02\n'),('poll','23',1498574702,3.36,1,'ubuntu16-02\n'),('poll','27',1498574702,4.75,1,'ubuntu16-02\n'),('pollbill','',1498575002,0.23,0,'ubuntu16-02\n'),('pollbill','',1498575002,0.43,0,'ubuntu16\n'),('poll','22',1498575003,1.34,1,'ubuntu16\n'),('poll','25',1498575002,3.04,1,'ubuntu16-02\n'),('poll','24',1498575002,3.14,1,'ubuntu16-02\n'),('poll','23',1498575002,3.16,1,'ubuntu16-02\n'),('poll','27',1498575002,4.58,1,'ubuntu16-02\n'),('pollbill','',1498575301,0.25,0,'ubuntu16-02\n'),('pollbill','',1498575302,0.33,0,'ubuntu16\n'),('poll','22',1498575302,1.38,1,'ubuntu16\n'),('poll','24',1498575302,3.62,1,'ubuntu16-02\n'),('poll','25',1498575302,3.61,1,'ubuntu16-02\n'),('poll','23',1498575302,3.69,1,'ubuntu16-02\n'),('poll','27',1498575302,3.77,1,'ubuntu16-02\n'),('pollbill','',1498575602,0.29,0,'ubuntu16-02\n'),('pollbill','',1498575602,0.33,0,'ubuntu16\n'),('poll','22',1498575603,1.34,1,'ubuntu16\n'),('poll','25',1498575603,3.49,1,'ubuntu16-02\n'),('poll','24',1498575603,3.48,1,'ubuntu16-02\n'),('poll','27',1498575603,3.60,1,'ubuntu16-02\n'),('poll','23',1498575603,3.58,1,'ubuntu16-02\n'),('pollbill','',1498575902,0.26,0,'ubuntu16\n'),('pollbill','',1498575902,0.27,0,'ubuntu16-02\n'),('poll','22',1498575902,1.29,1,'ubuntu16\n'),('poll','24',1498575903,3.00,1,'ubuntu16-02\n'),('poll','25',1498575903,3.02,1,'ubuntu16-02\n'),('poll','23',1498575903,3.08,1,'ubuntu16-02\n'),('poll','27',1498575903,4.46,1,'ubuntu16-02\n'),('pollbill','',1498576202,0.35,0,'ubuntu16-02\n'),('pollbill','',1498576202,0.37,0,'ubuntu16\n'),('poll','22',1498576203,1.34,1,'ubuntu16\n'),('poll','24',1498576202,3.07,1,'ubuntu16-02\n'),('poll','25',1498576202,3.06,1,'ubuntu16-02\n'),('poll','23',1498576202,3.11,1,'ubuntu16-02\n'),('poll','27',1498576202,4.64,1,'ubuntu16-02\n'),('pollbill','',1498576501,0.30,0,'ubuntu16-02\n'),('pollbill','',1498576502,0.32,0,'ubuntu16\n'),('poll','22',1498576502,1.37,1,'ubuntu16\n'),('poll','24',1498576502,3.18,1,'ubuntu16-02\n'),('poll','25',1498576502,3.17,1,'ubuntu16-02\n'),('poll','23',1498576502,3.20,1,'ubuntu16-02\n'),('poll','27',1498576502,4.41,1,'ubuntu16-02\n'),('poll','25',1498612815,6.27,1,'ubuntu16-02\n'),('pollbill','',1498612803,30.68,0,'ubuntu16\n'),('poll','22',1498612806,30.86,1,'ubuntu16\n'),('pollbill','',1498612815,37.36,0,'ubuntu16-02\n'),('poll','27',1498612815,40.51,1,'ubuntu16-02\n'),('poll','23',1498612815,40.71,1,'ubuntu16-02\n'),('poll','24',1498612815,40.78,1,'ubuntu16-02\n'),('pollbill','',1498613102,0.26,0,'ubuntu16-02\n'),('pollbill','',1498613103,0.23,0,'ubuntu16\n'),('poll','22',1498613103,1.72,1,'ubuntu16\n'),('poll','24',1498613102,5.16,1,'ubuntu16-02\n'),('poll','23',1498613102,5.38,1,'ubuntu16-02\n'),('poll','27',1498613102,5.39,1,'ubuntu16-02\n'),('poll','25',1498613102,5.48,1,'ubuntu16-02\n'),('pollbill','',1498613402,0.36,0,'ubuntu16-02\n'),('pollbill','',1498613402,0.33,0,'ubuntu16\n'),('poll','22',1498613403,1.58,1,'ubuntu16\n'),('poll','25',1498613403,4.26,1,'ubuntu16-02\n'),('poll','27',1498613403,4.33,1,'ubuntu16-02\n'),('poll','24',1498613403,4.28,1,'ubuntu16-02\n'),('poll','23',1498613403,4.31,1,'ubuntu16-02\n'),('pollbill','',1498613702,0.40,0,'ubuntu16-02\n'),('pollbill','',1498613702,0.61,0,'ubuntu16\n'),('poll','22',1498613703,1.70,1,'ubuntu16\n'),('poll','24',1498613703,4.89,1,'ubuntu16-02\n'),('poll','25',1498613703,5.01,1,'ubuntu16-02\n'),('poll','23',1498613703,5.07,1,'ubuntu16-02\n'),('poll','27',1498613703,5.19,1,'ubuntu16-02\n'),('pollbill','',1498614002,0.34,0,'ubuntu16-02\n'),('pollbill','',1498614002,0.50,0,'ubuntu16\n'),('poll','22',1498614003,1.55,1,'ubuntu16\n'),('poll','25',1498614003,4.25,1,'ubuntu16-02\n'),('poll','24',1498614003,4.26,1,'ubuntu16-02\n'),('poll','23',1498614003,4.33,1,'ubuntu16-02\n'),('poll','27',1498614003,5.37,1,'ubuntu16-02\n'),('pollbill','',1498614302,0.61,0,'ubuntu16-02\n'),('pollbill','',1498614303,0.42,0,'ubuntu16\n'),('poll','22',1498614303,1.63,1,'ubuntu16\n'),('poll','25',1498614303,4.17,1,'ubuntu16-02\n'),('poll','24',1498614303,4.45,1,'ubuntu16-02\n'),('poll','23',1498614303,4.53,1,'ubuntu16-02\n'),('poll','27',1498614303,4.69,1,'ubuntu16-02\n'),('pollbill','',1498614601,1.63,0,'ubuntu16-02\n'),('pollbill','',1498614603,1.48,0,'ubuntu16\n'),('poll','22',1498614604,2.35,1,'ubuntu16\n'),('poll','23',1498614603,6.24,1,'ubuntu16-02\n'),('poll','25',1498614603,6.15,1,'ubuntu16-02\n'),('poll','24',1498614603,6.45,1,'ubuntu16-02\n'),('poll','27',1498614603,6.51,1,'ubuntu16-02\n'),('pollbill','',1498614901,1.07,0,'ubuntu16-02\n'),('pollbill','',1498614902,0.94,0,'ubuntu16\n'),('poll','22',1498614903,1.78,1,'ubuntu16\n'),('poll','24',1498614902,4.49,1,'ubuntu16-02\n'),('poll','25',1498614902,4.45,1,'ubuntu16-02\n'),('poll','23',1498614902,4.52,1,'ubuntu16-02\n'),('poll','27',1498614902,4.79,1,'ubuntu16-02\n'),('pollbill','',1498615202,1.22,0,'ubuntu16-02\n'),('pollbill','',1498615203,0.99,0,'ubuntu16\n'),('poll','22',1498615203,1.70,1,'ubuntu16\n'),('poll','24',1498615203,4.60,1,'ubuntu16-02\n'),('poll','27',1498615203,4.69,1,'ubuntu16-02\n'),('poll','25',1498615203,4.64,1,'ubuntu16-02\n'),('poll','23',1498615203,4.61,1,'ubuntu16-02\n'),('pollbill','',1498615501,1.07,0,'ubuntu16-02\n'),('pollbill','',1498615502,1.07,0,'ubuntu16\n'),('poll','22',1498615503,1.63,1,'ubuntu16\n'),('poll','25',1498615502,4.34,1,'ubuntu16-02\n'),('poll','24',1498615502,4.37,1,'ubuntu16-02\n'),('poll','23',1498615502,4.44,1,'ubuntu16-02\n'),('poll','27',1498615502,4.49,1,'ubuntu16-02\n'),('pollbill','',1498615802,1.26,0,'ubuntu16\n'),('pollbill','',1498615802,1.15,0,'ubuntu16-02\n'),('poll','22',1498615803,1.72,1,'ubuntu16\n'),('poll','25',1498615803,4.79,1,'ubuntu16-02\n'),('poll','24',1498615803,4.84,1,'ubuntu16-02\n'),('poll','23',1498615803,4.92,1,'ubuntu16-02\n'),('poll','27',1498615803,5.01,1,'ubuntu16-02\n'),('pollbill','',1498616102,1.46,0,'ubuntu16-02\n'),('pollbill','',1498616103,1.25,0,'ubuntu16\n'),('poll','22',1498616104,1.91,1,'ubuntu16\n'),('poll','25',1498616103,5.24,1,'ubuntu16-02\n'),('poll','24',1498616103,5.36,1,'ubuntu16-02\n'),('poll','27',1498616103,5.38,1,'ubuntu16-02\n'),('poll','23',1498616103,5.40,1,'ubuntu16-02\n'),('pollbill','',1498616402,1.17,0,'ubuntu16-02\n'),('pollbill','',1498616402,1.05,0,'ubuntu16\n'),('poll','22',1498616403,1.78,1,'ubuntu16\n'),('poll','24',1498616403,5.01,1,'ubuntu16-02\n'),('poll','25',1498616403,5.07,1,'ubuntu16-02\n'),('poll','27',1498616403,5.13,1,'ubuntu16-02\n'),('poll','23',1498616403,5.11,1,'ubuntu16-02\n'),('pollbill','',1498616701,2.09,0,'ubuntu16-02\n'),('pollbill','',1498616703,1.37,0,'ubuntu16\n'),('poll','22',1498616704,1.90,1,'ubuntu16\n'),('poll','25',1498616703,5.78,1,'ubuntu16-02\n'),('poll','24',1498616704,5.28,1,'ubuntu16-02\n'),('poll','27',1498616703,5.98,1,'ubuntu16-02\n'),('poll','23',1498616703,5.49,1,'ubuntu16-02\n'),('pollbill','',1498617001,1.24,0,'ubuntu16-02\n'),('pollbill','',1498617002,0.68,0,'ubuntu16\n'),('poll','24',1498617002,4.97,1,'ubuntu16-02\n'),('poll','27',1498617002,4.95,1,'ubuntu16-02\n'),('poll','23',1498617002,4.98,1,'ubuntu16-02\n'),('poll','25',1498617002,5.05,1,'ubuntu16-02\n'),('pollbill','',1498617302,1.09,0,'ubuntu16-02\n'),('pollbill','',1498617303,0.89,0,'ubuntu16\n'),('poll','22',1498617303,1.42,1,'ubuntu16\n'),('poll','25',1498617303,3.78,1,'ubuntu16-02\n'),('poll','24',1498617303,3.80,1,'ubuntu16-02\n'),('poll','27',1498617303,4.02,1,'ubuntu16-02\n'),('poll','23',1498617303,3.98,1,'ubuntu16-02\n'),('pollbill','',1498617602,1.17,0,'ubuntu16-02\n'),('pollbill','',1498617603,0.71,0,'ubuntu16\n'),('poll','22',1498617603,1.45,1,'ubuntu16\n'),('poll','24',1498617603,3.92,1,'ubuntu16-02\n'),('poll','25',1498617603,3.86,1,'ubuntu16-02\n'),('poll','23',1498617603,4.03,1,'ubuntu16-02\n'),('poll','27',1498617603,4.06,1,'ubuntu16-02\n'),('pollbill','',1498617901,1.04,0,'ubuntu16-02\n'),('pollbill','',1498617902,0.94,0,'ubuntu16\n'),('poll','22',1498617903,1.47,1,'ubuntu16\n'),('poll','25',1498617902,3.69,1,'ubuntu16-02\n'),('poll','23',1498617902,3.84,1,'ubuntu16-02\n'),('poll','24',1498617902,3.82,1,'ubuntu16-02\n'),('poll','27',1498617902,3.80,1,'ubuntu16-02\n'),('pollbill','',1498618202,1.12,0,'ubuntu16-02\n'),('pollbill','',1498618202,0.90,0,'ubuntu16\n'),('poll','22',1498618203,1.46,1,'ubuntu16\n'),('poll','25',1498618203,3.88,1,'ubuntu16-02\n'),('poll','24',1498618203,3.97,1,'ubuntu16-02\n'),('poll','23',1498618203,4.01,1,'ubuntu16-02\n'),('poll','27',1498618203,4.10,1,'ubuntu16-02\n'),('pollbill','',1498618501,1.01,0,'ubuntu16-02\n'),('pollbill','',1498618502,0.97,0,'ubuntu16\n'),('poll','22',1498618503,1.57,1,'ubuntu16\n'),('poll','24',1498618502,3.75,1,'ubuntu16-02\n'),('poll','25',1498618502,3.80,1,'ubuntu16-02\n'),('poll','27',1498618502,4.06,1,'ubuntu16-02\n'),('poll','23',1498618502,4.07,1,'ubuntu16-02\n'),('pollbill','',1498618802,0.99,0,'ubuntu16-02\n'),('pollbill','',1498618802,0.92,0,'ubuntu16\n'),('poll','22',1498618803,1.56,1,'ubuntu16\n'),('poll','23',1498618803,3.90,1,'ubuntu16-02\n'),('poll','25',1498618803,3.89,1,'ubuntu16-02\n'),('poll','24',1498618803,3.93,1,'ubuntu16-02\n'),('poll','27',1498618803,4.03,1,'ubuntu16-02\n'),('pollbill','',1498619102,0.97,0,'ubuntu16-02\n'),('pollbill','',1498619102,0.82,0,'ubuntu16\n'),('poll','22',1498619103,1.50,1,'ubuntu16\n'),('poll','25',1498619103,3.46,1,'ubuntu16-02\n'),('poll','24',1498619103,3.59,1,'ubuntu16-02\n'),('poll','23',1498619103,3.69,1,'ubuntu16-02\n'),('poll','27',1498619103,3.74,1,'ubuntu16-02\n'),('pollbill','',1498619402,1.21,0,'ubuntu16-02\n'),('pollbill','',1498619402,1.25,0,'ubuntu16\n'),('poll','22',1498619403,1.42,1,'ubuntu16\n'),('poll','23',1498619403,3.43,1,'ubuntu16-02\n'),('poll','25',1498619403,3.63,1,'ubuntu16-02\n'),('poll','24',1498619403,3.66,1,'ubuntu16-02\n'),('poll','27',1498619403,3.76,1,'ubuntu16-02\n'),('pollbill','',1498619701,0.90,0,'ubuntu16-02\n'),('pollbill','',1498619702,0.78,0,'ubuntu16\n'),('poll','22',1498619703,1.44,1,'ubuntu16\n'),('poll','25',1498619702,3.76,1,'ubuntu16-02\n'),('poll','24',1498619702,3.82,1,'ubuntu16-02\n'),('poll','23',1498619702,3.79,1,'ubuntu16-02\n'),('poll','27',1498619702,3.90,1,'ubuntu16-02\n'),('pollbill','',1498620002,0.70,0,'ubuntu16\n'),('pollbill','',1498620002,0.82,0,'ubuntu16-02\n'),('poll','22',1498620002,1.34,1,'ubuntu16\n'),('poll','24',1498620003,3.21,1,'ubuntu16-02\n'),('poll','25',1498620003,3.21,1,'ubuntu16-02\n'),('poll','23',1498620003,3.34,1,'ubuntu16-02\n'),('poll','27',1498620003,3.39,1,'ubuntu16-02\n'),('pollbill','',1498620301,1.01,0,'ubuntu16-02\n'),('pollbill','',1498620302,0.72,0,'ubuntu16\n'),('poll','22',1498620302,1.42,1,'ubuntu16\n'),('poll','25',1498620302,3.34,1,'ubuntu16-02\n'),('poll','24',1498620302,3.45,1,'ubuntu16-02\n'),('poll','23',1498620302,3.52,1,'ubuntu16-02\n'),('poll','27',1498620302,3.57,1,'ubuntu16-02\n'),('pollbill','',1498620602,1.01,0,'ubuntu16-02\n'),('pollbill','',1498620602,0.88,0,'ubuntu16\n'),('poll','22',1498620603,1.47,1,'ubuntu16\n'),('poll','24',1498620603,3.56,1,'ubuntu16-02\n'),('poll','25',1498620603,3.54,1,'ubuntu16-02\n'),('poll','23',1498620603,3.78,1,'ubuntu16-02\n'),('poll','27',1498620603,3.82,1,'ubuntu16-02\n'),('pollbill','',1498620901,1.03,0,'ubuntu16-02\n'),('pollbill','',1498620902,0.84,0,'ubuntu16\n'),('poll','22',1498620902,1.38,1,'ubuntu16\n'),('poll','25',1498620902,3.57,1,'ubuntu16-02\n'),('poll','24',1498620902,3.69,1,'ubuntu16-02\n'),('poll','23',1498620902,3.79,1,'ubuntu16-02\n'),('poll','27',1498620902,3.87,1,'ubuntu16-02\n'),('pollbill','',1498621202,0.94,0,'ubuntu16\n'),('pollbill','',1498621202,0.97,0,'ubuntu16-02\n'),('poll','22',1498621203,1.42,1,'ubuntu16\n'),('poll','24',1498621203,3.20,1,'ubuntu16-02\n'),('poll','25',1498621203,3.20,1,'ubuntu16-02\n'),('poll','23',1498621203,3.27,1,'ubuntu16-02\n'),('poll','27',1498621203,3.38,1,'ubuntu16-02\n'),('pollbill','',1498621502,1.16,0,'ubuntu16-02\n'),('pollbill','',1498621503,0.73,0,'ubuntu16\n'),('poll','22',1498621503,1.43,1,'ubuntu16\n'),('poll','27',1498621503,3.71,1,'ubuntu16-02\n'),('poll','24',1498621503,3.59,1,'ubuntu16-02\n'),('poll','25',1498621503,3.72,1,'ubuntu16-02\n'),('poll','23',1498621503,3.79,1,'ubuntu16-02\n'),('pollbill','',1498621801,2.08,0,'ubuntu16-02\n'),('pollbill','',1498621803,1.42,0,'ubuntu16\n'),('poll','22',1498621804,1.62,1,'ubuntu16\n'),('poll','25',1498621803,4.19,1,'ubuntu16-02\n'),('poll','24',1498621803,4.37,1,'ubuntu16-02\n'),('poll','27',1498621803,4.19,1,'ubuntu16-02\n'),('poll','23',1498621803,4.53,1,'ubuntu16-02\n'),('pollbill','',1498622102,1.25,0,'ubuntu16-02\n'),('pollbill','',1498622103,1.24,0,'ubuntu16\n'),('poll','22',1498622103,1.79,1,'ubuntu16\n'),('poll','24',1498622103,4.54,1,'ubuntu16-02\n'),('poll','25',1498622103,4.88,1,'ubuntu16-02\n'),('poll','23',1498622103,4.86,1,'ubuntu16-02\n'),('poll','27',1498622103,5.22,1,'ubuntu16-02\n'),('pollbill','',1498622402,0.84,0,'ubuntu16-02\n'),('pollbill','',1498622402,0.86,0,'ubuntu16\n'),('poll','22',1498622403,1.44,1,'ubuntu16\n'),('poll','25',1498622403,3.78,1,'ubuntu16-02\n'),('poll','24',1498622403,3.82,1,'ubuntu16-02\n'),('poll','23',1498622403,3.89,1,'ubuntu16-02\n'),('poll','27',1498622403,4.09,1,'ubuntu16-02\n'),('pollbill','',1498622702,1.53,0,'ubuntu16-02\n'),('pollbill','',1498622703,0.89,0,'ubuntu16\n'),('poll','22',1498622703,1.42,1,'ubuntu16\n'),('poll','24',1498622703,3.87,1,'ubuntu16-02\n'),('poll','23',1498622703,3.90,1,'ubuntu16-02\n'),('poll','25',1498622703,3.84,1,'ubuntu16-02\n'),('poll','27',1498622703,3.92,1,'ubuntu16-02\n'),('pollbill','',1498623002,0.73,0,'ubuntu16-02\n'),('pollbill','',1498623003,0.76,0,'ubuntu16\n'),('poll','22',1498623003,1.53,1,'ubuntu16\n'),('poll','25',1498623002,3.85,1,'ubuntu16-02\n'),('poll','24',1498623002,4.00,1,'ubuntu16-02\n'),('poll','23',1498623002,4.07,1,'ubuntu16-02\n'),('poll','27',1498623002,4.12,1,'ubuntu16-02\n'),('pollbill','',1498623302,0.77,0,'ubuntu16\n'),('pollbill','',1498623302,0.91,0,'ubuntu16-02\n'),('poll','22',1498623302,1.40,1,'ubuntu16\n'),('poll','24',1498623303,4.41,1,'ubuntu16-02\n'),('poll','25',1498623303,4.39,1,'ubuntu16-02\n'),('poll','23',1498623303,4.58,1,'ubuntu16-02\n'),('poll','27',1498623303,4.67,1,'ubuntu16-02\n'),('pollbill','',1498623602,0.89,0,'ubuntu16\n'),('pollbill','',1498623602,1.07,0,'ubuntu16-02\n'),('poll','22',1498623602,1.48,1,'ubuntu16\n'),('poll','24',1498623603,3.42,1,'ubuntu16-02\n'),('poll','23',1498623603,3.49,1,'ubuntu16-02\n'),('poll','25',1498623603,3.51,1,'ubuntu16-02\n'),('poll','27',1498623603,3.58,1,'ubuntu16-02\n'),('pollbill','',1498623902,0.96,0,'ubuntu16-02\n'),('pollbill','',1498623902,0.88,0,'ubuntu16\n'),('poll','22',1498623903,1.43,1,'ubuntu16\n'),('poll','24',1498623903,3.47,1,'ubuntu16-02\n'),('poll','25',1498623903,3.55,1,'ubuntu16-02\n'),('poll','23',1498623903,3.53,1,'ubuntu16-02\n'),('poll','27',1498623903,3.62,1,'ubuntu16-02\n'),('pollbill','',1498624202,1.03,0,'ubuntu16-02\n'),('pollbill','',1498624202,0.95,0,'ubuntu16\n'),('poll','22',1498624203,1.69,1,'ubuntu16\n'),('poll','24',1498624203,4.20,1,'ubuntu16-02\n'),('poll','25',1498624203,4.28,1,'ubuntu16-02\n'),('poll','23',1498624203,4.28,1,'ubuntu16-02\n'),('poll','27',1498624203,4.37,1,'ubuntu16-02\n'),('pollbill','',1498624502,0.82,0,'ubuntu16\n'),('pollbill','',1498624502,0.84,0,'ubuntu16-02\n'),('poll','22',1498624502,1.52,1,'ubuntu16\n'),('poll','25',1498624503,3.04,1,'ubuntu16-02\n'),('poll','24',1498624503,3.04,1,'ubuntu16-02\n'),('poll','23',1498624503,3.06,1,'ubuntu16-02\n'),('poll','27',1498624503,4.70,1,'ubuntu16-02\n'),('pollbill','',1498624801,0.88,0,'ubuntu16-02\n'),('pollbill','',1498624802,1.03,0,'ubuntu16\n'),('poll','22',1498624803,1.49,1,'ubuntu16\n'),('poll','23',1498624802,3.22,1,'ubuntu16-02\n'),('poll','24',1498624802,3.17,1,'ubuntu16-02\n'),('poll','25',1498624802,3.17,1,'ubuntu16-02\n'),('poll','27',1498624802,4.60,1,'ubuntu16-02\n'),('pollbill','',1498625102,0.90,0,'ubuntu16-02\n'),('pollbill','',1498625103,0.68,0,'ubuntu16\n'),('poll','22',1498625103,1.54,1,'ubuntu16\n'),('poll','23',1498625103,3.51,1,'ubuntu16-02\n'),('poll','25',1498625103,3.62,1,'ubuntu16-02\n'),('poll','24',1498625103,3.67,1,'ubuntu16-02\n'),('poll','27',1498625103,3.69,1,'ubuntu16-02\n'),('pollbill','',1498625401,1.59,0,'ubuntu16-02\n'),('pollbill','',1498625402,1.11,0,'ubuntu16\n'),('poll','22',1498625403,1.46,1,'ubuntu16\n'),('poll','24',1498625403,3.11,1,'ubuntu16-02\n'),('poll','25',1498625403,3.16,1,'ubuntu16-02\n'),('poll','23',1498625403,3.16,1,'ubuntu16-02\n'),('poll','27',1498625403,3.81,1,'ubuntu16-02\n'),('pollbill','',1498625702,0.76,0,'ubuntu16\n'),('pollbill','',1498625702,0.80,0,'ubuntu16-02\n'),('poll','22',1498625702,1.41,1,'ubuntu16\n'),('poll','25',1498625703,4.04,1,'ubuntu16-02\n'),('poll','24',1498625703,4.10,1,'ubuntu16-02\n'),('poll','23',1498625703,4.13,1,'ubuntu16-02\n'),('poll','27',1498625703,4.32,1,'ubuntu16-02\n'),('pollbill','',1498626002,1.06,0,'ubuntu16-02\n'),('pollbill','',1498626002,1.00,0,'ubuntu16\n'),('poll','22',1498626003,1.65,1,'ubuntu16\n'),('poll','25',1498626003,4.43,1,'ubuntu16-02\n'),('poll','24',1498626003,4.52,1,'ubuntu16-02\n'),('poll','23',1498626003,4.57,1,'ubuntu16-02\n'),('poll','27',1498626003,5.25,1,'ubuntu16-02\n'),('pollbill','',1498626302,0.86,0,'ubuntu16\n'),('pollbill','',1498626302,0.97,0,'ubuntu16-02\n'),('poll','22',1498626302,1.33,1,'ubuntu16\n'),('poll','25',1498626303,3.90,1,'ubuntu16-02\n'),('poll','27',1498626303,4.08,1,'ubuntu16-02\n'),('poll','23',1498626303,4.30,1,'ubuntu16-02\n'),('poll','24',1498626303,4.27,1,'ubuntu16-02\n'),('pollbill','',1498626601,0.96,0,'ubuntu16-02\n'),('pollbill','',1498626603,1.45,0,'ubuntu16\n'),('poll','22',1498626603,2.32,1,'ubuntu16\n'),('poll','25',1498626602,4.51,1,'ubuntu16-02\n'),('poll','24',1498626602,4.53,1,'ubuntu16-02\n'),('poll','23',1498626602,4.59,1,'ubuntu16-02\n'),('poll','27',1498626602,4.79,1,'ubuntu16-02\n'),('pollbill','',1498626902,1.25,0,'ubuntu16\n'),('pollbill','',1498626902,1.11,0,'ubuntu16-02\n'),('poll','22',1498626902,1.45,1,'ubuntu16\n'),('poll','25',1498626903,3.83,1,'ubuntu16-02\n'),('poll','24',1498626903,4.29,1,'ubuntu16-02\n'),('poll','27',1498626903,4.28,1,'ubuntu16-02\n'),('poll','23',1498626903,4.30,1,'ubuntu16-02\n'),('pollbill','',1498627202,1.25,0,'ubuntu16-02\n'),('pollbill','',1498627203,0.97,0,'ubuntu16\n'),('poll','22',1498627203,1.48,1,'ubuntu16\n'),('poll','25',1498627202,4.17,1,'ubuntu16-02\n'),('poll','24',1498627202,4.18,1,'ubuntu16-02\n'),('poll','23',1498627203,4.15,1,'ubuntu16-02\n'),('poll','27',1498627203,4.26,1,'ubuntu16-02\n'),('pollbill','',1498627501,1.08,0,'ubuntu16-02\n'),('pollbill','',1498627502,0.79,0,'ubuntu16\n'),('poll','22',1498627503,1.44,1,'ubuntu16\n'),('poll','24',1498627502,4.08,1,'ubuntu16-02\n'),('poll','25',1498627502,4.08,1,'ubuntu16-02\n'),('poll','27',1498627502,4.21,1,'ubuntu16-02\n'),('poll','23',1498627502,4.17,1,'ubuntu16-02\n'),('pollbill','',1498627802,1.44,0,'ubuntu16-02\n'),('pollbill','',1498627803,1.24,0,'ubuntu16\n'),('poll','22',1498627804,1.69,1,'ubuntu16\n'),('poll','27',1498627803,4.50,1,'ubuntu16-02\n'),('poll','25',1498627803,4.41,1,'ubuntu16-02\n'),('poll','24',1498627803,4.49,1,'ubuntu16-02\n'),('poll','23',1498627803,4.45,1,'ubuntu16-02\n'),('pollbill','',1498628101,1.08,0,'ubuntu16-02\n'),('pollbill','',1498628103,0.87,0,'ubuntu16\n'),('poll','22',1498628103,1.58,1,'ubuntu16\n'),('poll','24',1498628102,3.84,1,'ubuntu16-02\n'),('poll','27',1498628102,4.07,1,'ubuntu16-02\n'),('poll','25',1498628102,4.27,1,'ubuntu16-02\n'),('poll','23',1498628102,4.31,1,'ubuntu16-02\n'),('pollbill','',1498628402,1.12,0,'ubuntu16\n'),('pollbill','',1498628402,1.20,0,'ubuntu16-02\n'),('poll','22',1498628403,1.56,1,'ubuntu16\n'),('poll','25',1498628403,5.21,1,'ubuntu16-02\n'),('poll','24',1498628403,5.16,1,'ubuntu16-02\n'),('poll','23',1498628403,5.20,1,'ubuntu16-02\n'),('poll','27',1498628403,5.58,1,'ubuntu16-02\n'),('pollbill','',1498628702,1.30,0,'ubuntu16-02\n'),('pollbill','',1498628703,0.83,0,'ubuntu16\n'),('poll','22',1498628703,1.49,1,'ubuntu16\n'),('poll','25',1498628702,3.90,1,'ubuntu16-02\n'),('poll','24',1498628702,3.92,1,'ubuntu16-02\n'),('poll','23',1498628702,4.00,1,'ubuntu16-02\n'),('poll','27',1498628702,4.07,1,'ubuntu16-02\n'),('pollbill','',1498629001,1.04,0,'ubuntu16-02\n'),('pollbill','',1498629002,1.00,0,'ubuntu16\n'),('poll','22',1498629003,1.47,1,'ubuntu16\n'),('poll','24',1498629002,3.73,1,'ubuntu16-02\n'),('poll','27',1498629002,3.98,1,'ubuntu16-02\n'),('poll','25',1498629002,3.91,1,'ubuntu16-02\n'),('poll','23',1498629002,3.98,1,'ubuntu16-02\n'),('pollbill','',1498629302,1.40,0,'ubuntu16-02\n'),('pollbill','',1498629302,1.22,0,'ubuntu16\n'),('poll','22',1498629303,1.66,1,'ubuntu16\n'),('poll','25',1498629303,3.54,1,'ubuntu16-02\n'),('poll','23',1498629303,3.52,1,'ubuntu16-02\n'),('poll','24',1498629303,3.65,1,'ubuntu16-02\n'),('poll','27',1498629303,4.14,1,'ubuntu16-02\n'),('pollbill','',1498629602,0.85,0,'ubuntu16-02\n'),('pollbill','',1498629602,0.80,0,'ubuntu16\n'),('poll','22',1498629602,1.38,1,'ubuntu16\n'),('poll','24',1498629602,3.28,1,'ubuntu16-02\n'),('poll','25',1498629602,3.26,1,'ubuntu16-02\n'),('poll','23',1498629602,3.29,1,'ubuntu16-02\n'),('poll','27',1498629602,3.77,1,'ubuntu16-02\n'),('pollbill','',1498629901,0.79,0,'ubuntu16-02\n'),('pollbill','',1498629903,1.09,0,'ubuntu16\n'),('poll','22',1498629903,1.58,1,'ubuntu16\n'),('poll','24',1498629902,4.41,1,'ubuntu16-02\n'),('poll','25',1498629902,4.43,1,'ubuntu16-02\n'),('poll','23',1498629902,4.58,1,'ubuntu16-02\n'),('poll','27',1498629902,4.68,1,'ubuntu16-02\n'),('pollbill','',1498630202,0.92,0,'ubuntu16\n'),('pollbill','',1498630202,0.92,0,'ubuntu16-02\n'),('poll','22',1498630203,1.40,1,'ubuntu16\n'),('poll','25',1498630203,3.45,1,'ubuntu16-02\n'),('poll','24',1498630203,3.69,1,'ubuntu16-02\n'),('poll','23',1498630203,3.71,1,'ubuntu16-02\n'),('poll','27',1498630203,3.79,1,'ubuntu16-02\n'),('pollbill','',1498630502,1.02,0,'ubuntu16-02\n'),('pollbill','',1498630503,0.69,0,'ubuntu16\n'),('poll','22',1498630503,1.49,1,'ubuntu16\n'),('poll','24',1498630502,4.03,1,'ubuntu16-02\n'),('poll','25',1498630502,4.07,1,'ubuntu16-02\n'),('poll','27',1498630502,4.09,1,'ubuntu16-02\n'),('poll','23',1498630502,4.01,1,'ubuntu16-02\n'),('pollbill','',1498630802,0.97,0,'ubuntu16-02\n'),('pollbill','',1498630802,0.86,0,'ubuntu16\n'),('poll','22',1498630803,1.63,1,'ubuntu16\n'),('poll','24',1498630803,3.34,1,'ubuntu16-02\n'),('poll','25',1498630803,3.50,1,'ubuntu16-02\n'),('poll','23',1498630803,3.59,1,'ubuntu16-02\n'),('poll','27',1498630803,3.69,1,'ubuntu16-02\n'),('pollbill','',1498631102,1.15,0,'ubuntu16-02\n'),('pollbill','',1498631102,0.96,0,'ubuntu16\n'),('poll','22',1498631103,1.60,1,'ubuntu16\n'),('poll','23',1498631103,3.70,1,'ubuntu16-02\n'),('poll','24',1498631103,3.73,1,'ubuntu16-02\n'),('poll','25',1498631103,3.74,1,'ubuntu16-02\n'),('poll','27',1498631103,3.84,1,'ubuntu16-02\n'),('pollbill','',1498631402,0.76,0,'ubuntu16\n'),('pollbill','',1498631402,0.78,0,'ubuntu16-02\n'),('poll','22',1498631402,1.38,1,'ubuntu16\n'),('poll','24',1498631402,2.91,1,'ubuntu16-02\n'),('poll','25',1498631402,2.93,1,'ubuntu16-02\n'),('poll','23',1498631402,3.00,1,'ubuntu16-02\n'),('poll','27',1498631402,4.38,1,'ubuntu16-02\n'),('pollbill','',1498631702,1.14,0,'ubuntu16\n'),('pollbill','',1498631702,1.47,0,'ubuntu16-02\n'),('poll','22',1498631703,1.54,1,'ubuntu16\n'),('poll','24',1498631703,3.74,1,'ubuntu16-02\n'),('poll','25',1498631703,3.83,1,'ubuntu16-02\n'),('poll','27',1498631703,3.92,1,'ubuntu16-02\n'),('poll','23',1498631703,3.86,1,'ubuntu16-02\n'),('pollbill','',1498632002,1.00,0,'ubuntu16-02\n'),('pollbill','',1498632002,0.73,0,'ubuntu16\n'),('poll','22',1498632003,1.43,1,'ubuntu16\n'),('poll','24',1498632003,3.65,1,'ubuntu16-02\n'),('poll','23',1498632003,3.67,1,'ubuntu16-02\n'),('poll','25',1498632003,4.15,1,'ubuntu16-02\n'),('poll','27',1498632003,4.20,1,'ubuntu16-02\n'),('pollbill','',1498632301,1.00,0,'ubuntu16-02\n'),('pollbill','',1498632302,0.74,0,'ubuntu16\n'),('poll','22',1498632302,1.38,1,'ubuntu16\n'),('poll','24',1498632302,3.41,1,'ubuntu16-02\n'),('poll','25',1498632302,3.46,1,'ubuntu16-02\n'),('poll','27',1498632302,3.61,1,'ubuntu16-02\n'),('poll','23',1498632302,3.55,1,'ubuntu16-02\n'),('pollbill','',1498632602,0.89,0,'ubuntu16-02\n'),('pollbill','',1498632602,0.76,0,'ubuntu16\n'),('poll','22',1498632603,1.45,1,'ubuntu16\n'),('poll','25',1498632603,3.38,1,'ubuntu16-02\n'),('poll','24',1498632603,3.41,1,'ubuntu16-02\n'),('poll','27',1498632603,3.74,1,'ubuntu16-02\n'),('poll','23',1498632603,3.74,1,'ubuntu16-02\n'),('pollbill','',1498632902,1.62,0,'ubuntu16-02\n'),('pollbill','',1498632903,1.11,0,'ubuntu16\n'),('poll','22',1498632903,1.92,1,'ubuntu16\n'),('poll','23',1498632903,4.51,1,'ubuntu16-02\n'),('poll','27',1498632903,4.56,1,'ubuntu16-02\n'),('poll','25',1498632903,4.68,1,'ubuntu16-02\n'),('poll','24',1498632903,4.87,1,'ubuntu16-02\n'),('pollbill','',1498633201,0.99,0,'ubuntu16-02\n'),('pollbill','',1498633202,0.75,0,'ubuntu16\n'),('poll','22',1498633203,1.36,1,'ubuntu16\n'),('poll','27',1498633202,3.82,1,'ubuntu16-02\n'),('poll','24',1498633202,3.90,1,'ubuntu16-02\n'),('poll','25',1498633202,3.85,1,'ubuntu16-02\n'),('poll','23',1498633202,3.89,1,'ubuntu16-02\n'),('pollbill','',1498633501,0.96,0,'ubuntu16-02\n'),('pollbill','',1498633502,0.78,0,'ubuntu16\n'),('poll','22',1498633502,1.41,1,'ubuntu16\n'),('poll','25',1498633502,3.26,1,'ubuntu16-02\n'),('poll','24',1498633502,3.21,1,'ubuntu16-02\n'),('poll','23',1498633502,3.36,1,'ubuntu16-02\n'),('poll','27',1498633502,3.91,1,'ubuntu16-02\n'),('pollbill','',1498633802,0.84,0,'ubuntu16\n'),('pollbill','',1498633802,0.96,0,'ubuntu16-02\n'),('poll','22',1498633802,1.34,1,'ubuntu16\n'),('poll','25',1498633803,3.38,1,'ubuntu16-02\n'),('poll','24',1498633803,3.38,1,'ubuntu16-02\n'),('poll','23',1498633803,3.45,1,'ubuntu16-02\n'),('poll','27',1498633803,3.59,1,'ubuntu16-02\n'),('pollbill','',1498634101,0.83,0,'ubuntu16-02\n'),('pollbill','',1498634102,0.73,0,'ubuntu16\n'),('poll','22',1498634103,1.41,1,'ubuntu16\n'),('poll','24',1498634102,3.35,1,'ubuntu16-02\n'),('poll','23',1498634102,3.43,1,'ubuntu16-02\n'),('poll','25',1498634102,3.51,1,'ubuntu16-02\n'),('poll','27',1498634102,4.12,1,'ubuntu16-02\n'),('pollbill','',1498634402,1.14,0,'ubuntu16-02\n'),('pollbill','',1498634403,0.78,0,'ubuntu16\n'),('poll','22',1498634403,1.54,1,'ubuntu16\n'),('poll','24',1498634403,3.63,1,'ubuntu16-02\n'),('poll','25',1498634403,3.62,1,'ubuntu16-02\n'),('poll','23',1498634403,3.78,1,'ubuntu16-02\n'),('poll','27',1498634403,3.81,1,'ubuntu16-02\n'),('pollbill','',1498634701,0.96,0,'ubuntu16-02\n'),('pollbill','',1498634703,0.81,0,'ubuntu16\n'),('poll','22',1498634702,2.06,1,'ubuntu16\n'),('poll','23',1498634702,3.65,1,'ubuntu16-02\n'),('poll','25',1498634702,3.77,1,'ubuntu16-02\n'),('poll','24',1498634702,3.82,1,'ubuntu16-02\n'),('poll','27',1498634702,3.88,1,'ubuntu16-02\n'),('pollbill','',1498635002,0.96,0,'ubuntu16-02\n'),('pollbill','',1498635002,0.76,0,'ubuntu16\n'),('poll','22',1498635003,1.40,1,'ubuntu16\n'),('poll','25',1498635003,4.09,1,'ubuntu16-02\n'),('poll','24',1498635003,4.24,1,'ubuntu16-02\n'),('poll','23',1498635003,4.20,1,'ubuntu16-02\n'),('poll','27',1498635003,4.37,1,'ubuntu16-02\n'),('pollbill','',1498635301,0.98,0,'ubuntu16-02\n'),('pollbill','',1498635302,0.66,0,'ubuntu16\n'),('poll','22',1498635302,1.45,1,'ubuntu16\n'),('poll','23',1498635302,3.93,1,'ubuntu16-02\n'),('poll','27',1498635302,3.93,1,'ubuntu16-02\n'),('poll','25',1498635302,3.88,1,'ubuntu16-02\n'),('poll','24',1498635302,3.99,1,'ubuntu16-02\n'),('pollbill','',1498635602,0.81,0,'ubuntu16\n'),('pollbill','',1498635602,0.90,0,'ubuntu16-02\n'),('poll','22',1498635603,1.32,1,'ubuntu16\n'),('poll','25',1498635603,3.44,1,'ubuntu16-02\n'),('poll','24',1498635603,3.43,1,'ubuntu16-02\n'),('poll','23',1498635603,3.56,1,'ubuntu16-02\n'),('poll','27',1498635603,3.63,1,'ubuntu16-02\n'),('pollbill','',1498635902,0.90,0,'ubuntu16-02\n'),('pollbill','',1498635903,0.74,0,'ubuntu16\n'),('poll','22',1498635903,1.53,1,'ubuntu16\n'),('poll','25',1498635902,3.58,1,'ubuntu16-02\n'),('poll','24',1498635902,3.59,1,'ubuntu16-02\n'),('poll','23',1498635902,3.71,1,'ubuntu16-02\n'),('poll','27',1498635902,3.78,1,'ubuntu16-02\n'),('pollbill','',1498636202,1.15,0,'ubuntu16\n'),('pollbill','',1498636202,1.18,0,'ubuntu16-02\n'),('poll','22',1498636203,1.65,1,'ubuntu16\n'),('poll','25',1498636203,4.08,1,'ubuntu16-02\n'),('poll','23',1498636203,4.26,1,'ubuntu16-02\n'),('poll','24',1498636203,4.11,1,'ubuntu16-02\n'),('poll','27',1498636203,4.18,1,'ubuntu16-02\n'),('pollbill','',1498636502,0.89,0,'ubuntu16-02\n'),('pollbill','',1498636503,0.68,0,'ubuntu16\n'),('poll','22',1498636503,1.41,1,'ubuntu16\n'),('poll','25',1498636503,3.54,1,'ubuntu16-02\n'),('poll','24',1498636503,3.62,1,'ubuntu16-02\n'),('poll','27',1498636503,3.76,1,'ubuntu16-02\n'),('poll','23',1498636503,3.80,1,'ubuntu16-02\n'),('pollbill','',1498636801,0.89,0,'ubuntu16-02\n'),('pollbill','',1498636802,0.75,0,'ubuntu16\n'),('poll','22',1498636802,1.35,1,'ubuntu16\n'),('poll','24',1498636802,3.56,1,'ubuntu16-02\n'),('poll','25',1498636802,3.54,1,'ubuntu16-02\n'),('poll','23',1498636802,3.60,1,'ubuntu16-02\n'),('poll','27',1498636802,3.63,1,'ubuntu16-02\n'),('pollbill','',1498637101,0.91,0,'ubuntu16-02\n'),('pollbill','',1498637102,0.72,0,'ubuntu16\n'),('poll','22',1498637103,1.34,1,'ubuntu16\n'),('poll','24',1498637102,3.43,1,'ubuntu16-02\n'),('poll','25',1498637102,3.50,1,'ubuntu16-02\n'),('poll','27',1498637102,3.74,1,'ubuntu16-02\n'),('poll','23',1498637102,3.73,1,'ubuntu16-02\n'),('pollbill','',1498637401,0.67,0,'ubuntu16-02\n'),('pollbill','',1498637403,0.69,0,'ubuntu16\n'),('poll','22',1498637403,1.43,1,'ubuntu16\n'),('poll','24',1498637402,3.61,1,'ubuntu16-02\n'),('poll','25',1498637402,3.62,1,'ubuntu16-02\n'),('poll','23',1498637402,3.67,1,'ubuntu16-02\n'),('poll','27',1498637402,3.73,1,'ubuntu16-02\n'),('pollbill','',1498637702,0.92,0,'ubuntu16-02\n'),('pollbill','',1498637702,0.96,0,'ubuntu16\n'),('poll','22',1498637702,1.32,1,'ubuntu16\n'),('poll','24',1498637702,3.56,1,'ubuntu16-02\n'),('poll','25',1498637702,3.58,1,'ubuntu16-02\n'),('poll','23',1498637702,3.57,1,'ubuntu16-02\n'),('poll','27',1498637702,3.69,1,'ubuntu16-02\n'),('pollbill','',1498638002,0.81,0,'ubuntu16\n'),('pollbill','',1498638002,0.78,0,'ubuntu16-02\n'),('poll','22',1498638002,1.43,1,'ubuntu16\n'),('poll','24',1498638003,3.38,1,'ubuntu16-02\n'),('poll','25',1498638003,3.40,1,'ubuntu16-02\n'),('poll','23',1498638003,3.61,1,'ubuntu16-02\n'),('poll','27',1498638003,3.63,1,'ubuntu16-02\n'),('pollbill','',1498638302,1.01,0,'ubuntu16-02\n'),('pollbill','',1498638302,0.83,0,'ubuntu16\n'),('poll','22',1498638303,1.47,1,'ubuntu16\n'),('poll','25',1498638303,3.79,1,'ubuntu16-02\n'),('poll','24',1498638303,3.80,1,'ubuntu16-02\n'),('poll','23',1498638303,3.81,1,'ubuntu16-02\n'),('poll','27',1498638303,3.93,1,'ubuntu16-02\n'),('poll','22',1498640103,9.47,1,'ubuntu16\n'),('pollbill','',1498640102,33.84,0,'ubuntu16\n'),('poll','22',1498640402,1.30,1,'ubuntu16\n'),('pollbill','',1498640402,24.78,0,'ubuntu16\n'),('poll','22',1498640702,1.23,1,'ubuntu16\n'),('pollbill','',1498640702,24.73,0,'ubuntu16\n'),('pollbill','',1498641002,2.39,0,'ubuntu16\n'),('poll','22',1498641002,2.31,1,'ubuntu16\n'),('pollbill','',1498641005,11.83,0,'ubuntu16-02\n'),('poll','25',1498641007,12.70,1,'ubuntu16-02\n'),('poll','27',1498641007,12.89,1,'ubuntu16-02\n'),('poll','23',1498641007,13.05,1,'ubuntu16-02\n'),('poll','24',1498641007,13.07,1,'ubuntu16-02\n'),('pollbill','',1498641302,0.56,0,'ubuntu16-02\n'),('pollbill','',1498641302,0.96,0,'ubuntu16\n'),('poll','22',1498641303,1.60,1,'ubuntu16\n'),('pollbill','',1498641602,1.19,0,'ubuntu16-02\n'),('pollbill','',1498641604,0.70,0,'ubuntu16\n'),('poll','22',1498641603,1.64,1,'ubuntu16\n'),('poll','24',1498641602,3.32,1,'ubuntu16-02\n'),('poll','23',1498641602,3.30,1,'ubuntu16-02\n'),('poll','25',1498641603,2.93,1,'ubuntu16-02\n'),('poll','27',1498641603,3.76,1,'ubuntu16-02\n'),('pollbill','',1498641902,1.82,0,'ubuntu16-02\n'),('pollbill','',1498641903,0.78,0,'ubuntu16\n'),('poll','22',1498641903,1.78,1,'ubuntu16\n'),('poll','24',1498641903,4.74,1,'ubuntu16-02\n'),('poll','25',1498641902,4.83,1,'ubuntu16-02\n'),('poll','23',1498641903,4.92,1,'ubuntu16-02\n'),('poll','27',1498641902,4.97,1,'ubuntu16-02\n'),('pollbill','',1498642201,1.44,0,'ubuntu16-02\n'),('pollbill','',1498642202,0.99,0,'ubuntu16\n'),('poll','22',1498642203,1.52,1,'ubuntu16\n'),('poll','25',1498642202,3.69,1,'ubuntu16-02\n'),('poll','24',1498642202,3.69,1,'ubuntu16-02\n'),('poll','23',1498642202,3.77,1,'ubuntu16-02\n'),('poll','27',1498642202,3.84,1,'ubuntu16-02\n'),('pollbill','',1498642502,0.82,0,'ubuntu16\n'),('pollbill','',1498642502,1.16,0,'ubuntu16-02\n'),('poll','22',1498642502,1.65,1,'ubuntu16\n'),('poll','25',1498642503,4.12,1,'ubuntu16-02\n'),('poll','24',1498642503,4.14,1,'ubuntu16-02\n'),('poll','23',1498642503,4.08,1,'ubuntu16-02\n'),('poll','27',1498642503,4.48,1,'ubuntu16-02\n'),('pollbill','',1498642802,1.40,0,'ubuntu16-02\n'),('pollbill','',1498642804,1.48,0,'ubuntu16\n'),('poll','22',1498642805,2.17,1,'ubuntu16\n'),('poll','23',1498642803,4.41,1,'ubuntu16-02\n'),('poll','24',1498642803,4.61,1,'ubuntu16-02\n'),('poll','25',1498642803,4.64,1,'ubuntu16-02\n'),('poll','27',1498642803,4.84,1,'ubuntu16-02\n'),('pollbill','',1498643102,1.09,0,'ubuntu16-02\n'),('poll','27',1498643102,4.61,1,'ubuntu16-02\n'),('poll','25',1498643102,4.78,1,'ubuntu16-02\n'),('poll','24',1498643102,4.79,1,'ubuntu16-02\n'),('poll','23',1498643102,4.96,1,'ubuntu16-02\n'),('pollbill','',1498643106,1.46,0,'ubuntu16\n'),('poll','22',1498643107,1.79,1,'ubuntu16\n'),('pollbill','',1498643402,0.94,0,'ubuntu16-02\n'),('pollbill','',1498643402,1.14,0,'ubuntu16\n'),('poll','22',1498643403,1.55,1,'ubuntu16\n'),('poll','25',1498643403,3.67,1,'ubuntu16-02\n'),('poll','24',1498643403,3.91,1,'ubuntu16-02\n'),('poll','27',1498643403,3.94,1,'ubuntu16-02\n'),('poll','23',1498643403,3.91,1,'ubuntu16-02\n'),('pollbill','',1498643702,0.89,0,'ubuntu16-02\n'),('pollbill','',1498643703,0.87,0,'ubuntu16\n'),('poll','22',1498643703,1.54,1,'ubuntu16\n'),('poll','24',1498643702,3.46,1,'ubuntu16-02\n'),('poll','25',1498643703,3.77,1,'ubuntu16-02\n'),('poll','23',1498643703,3.77,1,'ubuntu16-02\n'),('poll','27',1498643703,3.77,1,'ubuntu16-02\n'),('pollbill','',1498644001,1.19,0,'ubuntu16-02\n'),('pollbill','',1498644002,1.01,0,'ubuntu16\n'),('poll','22',1498644003,1.50,1,'ubuntu16\n'),('poll','24',1498644002,3.35,1,'ubuntu16-02\n'),('poll','25',1498644002,3.35,1,'ubuntu16-02\n'),('poll','23',1498644002,3.62,1,'ubuntu16-02\n'),('poll','27',1498644002,4.77,1,'ubuntu16-02\n'),('pollbill','',1498644303,0.63,0,'ubuntu16\n'),('pollbill','',1498644302,1.45,0,'ubuntu16-02\n'),('poll','22',1498644303,1.71,1,'ubuntu16\n'),('poll','25',1498644304,2.85,1,'ubuntu16-02\n'),('poll','24',1498644304,3.37,1,'ubuntu16-02\n'),('poll','23',1498644304,3.38,1,'ubuntu16-02\n'),('poll','27',1498644304,5.14,1,'ubuntu16-02\n'),('poll','22',1498654503,5.27,1,'ubuntu16\n'),('poll','25',1498654509,4.44,1,'ubuntu16-02\n'),('poll','24',1498654509,4.45,1,'ubuntu16-02\n'),('poll','23',1498654509,4.45,1,'ubuntu16-02\n'),('poll','27',1498654509,23.25,1,'ubuntu16-02\n'),('pollbill','',1498654502,88.79,0,'ubuntu16\n'),('pollbill','',1498654509,91.56,0,'ubuntu16-02\n'),('poll','23',1498654802,1.32,1,'ubuntu16-02\n'),('poll','24',1498654802,1.36,1,'ubuntu16-02\n'),('poll','25',1498654802,1.34,1,'ubuntu16-02\n'),('poll','22',1498654803,1.01,1,'ubuntu16\n'),('poll','27',1498654802,4.17,1,'ubuntu16-02\n'),('pollbill','',1498654802,72.92,0,'ubuntu16-02\n'),('pollbill','',1498654802,73.07,0,'ubuntu16\n'),('poll','23',1498655102,1.21,1,'ubuntu16-02\n'),('poll','24',1498655102,1.27,1,'ubuntu16-02\n'),('poll','25',1498655102,1.27,1,'ubuntu16-02\n'),('poll','22',1498655103,1.02,1,'ubuntu16\n'),('poll','27',1498655103,4.13,1,'ubuntu16-02\n'),('pollbill','',1498655102,73.15,0,'ubuntu16-02\n'),('pollbill','',1498655103,73.02,0,'ubuntu16\n'),('poll','24',1498655402,1.32,1,'ubuntu16-02\n'),('poll','23',1498655402,1.34,1,'ubuntu16-02\n'),('poll','25',1498655402,1.33,1,'ubuntu16-02\n'),('poll','22',1498655403,1.02,1,'ubuntu16\n'),('poll','27',1498655402,4.19,1,'ubuntu16-02\n'),('pollbill','',1498655402,72.95,0,'ubuntu16-02\n'),('pollbill','',1498655402,73.04,0,'ubuntu16\n'),('poll','24',1498655702,1.35,1,'ubuntu16-02\n'),('poll','25',1498655702,1.32,1,'ubuntu16-02\n'),('poll','22',1498655703,1.03,1,'ubuntu16\n'),('poll','23',1498655702,1.44,1,'ubuntu16-02\n'),('poll','27',1498655702,4.16,1,'ubuntu16-02\n'),('pollbill','',1498655702,73.11,0,'ubuntu16-02\n'),('pollbill','',1498655702,73.28,0,'ubuntu16\n'),('poll','24',1498656003,1.31,1,'ubuntu16-02\n'),('poll','23',1498656003,1.33,1,'ubuntu16-02\n'),('poll','25',1498656003,1.33,1,'ubuntu16-02\n'),('poll','22',1498656003,1.01,1,'ubuntu16\n'),('poll','27',1498656003,4.05,1,'ubuntu16-02\n'),('pollbill','',1498656002,72.95,0,'ubuntu16-02\n'),('pollbill','',1498656003,73.05,0,'ubuntu16\n'),('poll','24',1498656302,1.33,1,'ubuntu16-02\n'),('poll','25',1498656302,1.32,1,'ubuntu16-02\n'),('poll','23',1498656302,1.32,1,'ubuntu16-02\n'),('poll','22',1498656302,1.01,1,'ubuntu16\n'),('poll','27',1498656302,4.03,1,'ubuntu16-02\n'),('pollbill','',1498656301,72.99,0,'ubuntu16-02\n'),('pollbill','',1498656302,73.21,0,'ubuntu16\n'),('poll','22',1498656603,1.04,1,'ubuntu16\n'),('poll','23',1498656602,1.34,1,'ubuntu16-02\n'),('poll','24',1498656602,1.35,1,'ubuntu16-02\n'),('poll','25',1498656602,1.36,1,'ubuntu16-02\n'),('poll','27',1498656602,4.28,1,'ubuntu16-02\n'),('pollbill','',1498656602,72.99,0,'ubuntu16-02\n'),('pollbill','',1498656602,73.19,0,'ubuntu16\n'),('poll','22',1498656903,1.04,1,'ubuntu16\n'),('poll','25',1498656903,1.29,1,'ubuntu16-02\n'),('poll','23',1498656903,1.27,1,'ubuntu16-02\n'),('poll','24',1498656903,1.30,1,'ubuntu16-02\n'),('poll','27',1498656903,4.11,1,'ubuntu16-02\n'),('pollbill','',1498656902,72.99,0,'ubuntu16-02\n'),('pollbill','',1498656902,73.15,0,'ubuntu16\n'),('poll','23',1498657202,1.25,1,'ubuntu16-02\n'),('poll','24',1498657202,1.27,1,'ubuntu16-02\n'),('poll','25',1498657202,1.25,1,'ubuntu16-02\n'),('poll','22',1498657203,1.03,1,'ubuntu16\n'),('poll','27',1498657202,4.06,1,'ubuntu16-02\n'),('pollbill','',1498657201,72.87,0,'ubuntu16-02\n'),('pollbill','',1498657203,73.10,0,'ubuntu16\n'),('poll','22',1498657502,1.07,1,'ubuntu16\n'),('poll','25',1498657502,1.25,1,'ubuntu16-02\n'),('poll','24',1498657502,1.27,1,'ubuntu16-02\n'),('poll','23',1498657502,1.26,1,'ubuntu16-02\n'),('poll','27',1498657502,4.02,1,'ubuntu16-02\n'),('pollbill','',1498657502,73.08,0,'ubuntu16-02\n'),('pollbill','',1498657502,73.04,0,'ubuntu16\n'),('poll','22',1498657802,1.05,1,'ubuntu16\n'),('poll','25',1498657803,1.34,1,'ubuntu16-02\n'),('poll','24',1498657803,1.37,1,'ubuntu16-02\n'),('poll','23',1498657803,1.37,1,'ubuntu16-02\n'),('poll','27',1498657803,4.12,1,'ubuntu16-02\n'),('pollbill','',1498657802,73.17,0,'ubuntu16\n'),('pollbill','',1498657802,73.10,0,'ubuntu16-02\n'),('poll','23',1498658102,1.29,1,'ubuntu16-02\n'),('poll','25',1498658102,1.31,1,'ubuntu16-02\n'),('poll','24',1498658102,1.31,1,'ubuntu16-02\n'),('poll','22',1498658102,1.02,1,'ubuntu16\n'),('poll','27',1498658102,4.09,1,'ubuntu16-02\n'),('pollbill','',1498658101,73.00,0,'ubuntu16-02\n'),('pollbill','',1498658102,73.08,0,'ubuntu16\n'),('poll','23',1498658402,1.35,1,'ubuntu16-02\n'),('poll','25',1498658402,1.49,1,'ubuntu16-02\n'),('poll','24',1498658402,1.45,1,'ubuntu16-02\n'),('poll','22',1498658403,1.12,1,'ubuntu16\n'),('poll','27',1498658402,5.07,1,'ubuntu16-02\n'),('pollbill','',1498658402,73.18,0,'ubuntu16-02\n'),('pollbill','',1498658402,73.32,0,'ubuntu16\n'),('poll','23',1498658703,1.32,1,'ubuntu16-02\n'),('poll','24',1498658703,1.30,1,'ubuntu16-02\n'),('poll','25',1498658703,1.32,1,'ubuntu16-02\n'),('poll','22',1498658703,1.01,1,'ubuntu16\n'),('poll','27',1498658703,1.91,1,'ubuntu16-02\n'),('pollbill','',1498658702,73.14,0,'ubuntu16-02\n'),('pollbill','',1498658703,73.19,0,'ubuntu16\n'),('poll','22',1498659002,1.05,1,'ubuntu16\n'),('poll','23',1498659002,1.27,1,'ubuntu16-02\n'),('poll','24',1498659002,1.28,1,'ubuntu16-02\n'),('poll','25',1498659002,1.25,1,'ubuntu16-02\n'),('poll','27',1498659002,4.14,1,'ubuntu16-02\n'),('pollbill','',1498659002,73.02,0,'ubuntu16-02\n'),('pollbill','',1498659002,73.14,0,'ubuntu16\n'),('poll','22',1498659302,1.02,1,'ubuntu16\n'),('poll','24',1498659303,1.22,1,'ubuntu16-02\n'),('poll','25',1498659303,1.23,1,'ubuntu16-02\n'),('poll','23',1498659303,1.24,1,'ubuntu16-02\n'),('poll','27',1498659303,4.10,1,'ubuntu16-02\n'),('pollbill','',1498659302,73.22,0,'ubuntu16-02\n'),('pollbill','',1498659302,73.15,0,'ubuntu16\n'),('poll','23',1498659602,1.48,1,'ubuntu16-02\n'),('poll','24',1498659602,1.49,1,'ubuntu16-02\n'),('poll','25',1498659602,1.48,1,'ubuntu16-02\n'),('poll','22',1498659603,1.02,1,'ubuntu16\n'),('poll','27',1498659602,4.34,1,'ubuntu16-02\n'),('pollbill','',1498659602,73.07,0,'ubuntu16-02\n'),('pollbill','',1498659603,73.18,0,'ubuntu16\n'),('poll','22',1498659902,1.03,1,'ubuntu16\n'),('poll','24',1498659902,1.25,1,'ubuntu16-02\n'),('poll','25',1498659902,1.24,1,'ubuntu16-02\n'),('poll','23',1498659903,1.23,1,'ubuntu16-02\n'),('poll','27',1498659902,4.07,1,'ubuntu16-02\n'),('pollbill','',1498659902,73.21,0,'ubuntu16\n'),('pollbill','',1498659902,73.22,0,'ubuntu16-02\n'),('poll','23',1498660202,1.30,1,'ubuntu16-02\n'),('poll','25',1498660202,1.32,1,'ubuntu16-02\n'),('poll','24',1498660202,1.31,1,'ubuntu16-02\n'),('poll','22',1498660203,1.05,1,'ubuntu16\n'),('poll','27',1498660202,4.20,1,'ubuntu16-02\n'),('pollbill','',1498660201,72.95,0,'ubuntu16-02\n'),('pollbill','',1498660202,72.85,0,'ubuntu16\n'),('poll','24',1498660502,1.21,1,'ubuntu16-02\n'),('poll','25',1498660502,1.24,1,'ubuntu16-02\n'),('poll','23',1498660502,1.28,1,'ubuntu16-02\n'),('poll','22',1498660502,1.00,1,'ubuntu16\n'),('poll','27',1498660502,3.94,1,'ubuntu16-02\n'),('pollbill','',1498660502,73.01,0,'ubuntu16-02\n'),('pollbill','',1498660502,72.93,0,'ubuntu16\n'),('poll','22',1498660802,1.05,1,'ubuntu16\n'),('poll','25',1498660802,1.18,1,'ubuntu16-02\n'),('poll','24',1498660802,1.21,1,'ubuntu16-02\n'),('poll','23',1498660802,1.19,1,'ubuntu16-02\n'),('poll','27',1498660802,3.84,1,'ubuntu16-02\n'),('pollbill','',1498660802,72.93,0,'ubuntu16-02\n'),('pollbill','',1498660802,72.71,0,'ubuntu16\n'),('poll','22',1498661102,1.05,1,'ubuntu16\n'),('poll','24',1498661103,1.25,1,'ubuntu16-02\n'),('poll','23',1498661103,1.25,1,'ubuntu16-02\n'),('poll','25',1498661103,1.25,1,'ubuntu16-02\n'),('poll','27',1498661103,1.88,1,'ubuntu16-02\n'),('pollbill','',1498661102,73.09,0,'ubuntu16\n'),('pollbill','',1498661102,72.74,0,'ubuntu16-02\n'),('poll','22',1498661402,1.03,1,'ubuntu16\n'),('poll','25',1498661403,1.18,1,'ubuntu16-02\n'),('poll','24',1498661403,1.21,1,'ubuntu16-02\n'),('poll','23',1498661403,1.21,1,'ubuntu16-02\n'),('poll','27',1498661403,3.88,1,'ubuntu16-02\n'),('pollbill','',1498661402,73.08,0,'ubuntu16\n'),('pollbill','',1498661402,73.35,0,'ubuntu16-02\n'),('poll','24',1498661702,1.29,1,'ubuntu16-02\n'),('poll','25',1498661702,1.36,1,'ubuntu16-02\n'),('poll','23',1498661702,1.39,1,'ubuntu16-02\n'),('poll','22',1498661703,1.14,1,'ubuntu16\n'),('poll','27',1498661702,1.90,1,'ubuntu16-02\n'),('pollbill','',1498661702,73.89,0,'ubuntu16-02\n'),('pollbill','',1498661703,74.12,0,'ubuntu16\n'),('poll','22',1498662002,1.01,1,'ubuntu16\n'),('poll','25',1498662002,1.19,1,'ubuntu16-02\n'),('poll','24',1498662002,1.20,1,'ubuntu16-02\n'),('poll','23',1498662002,1.20,1,'ubuntu16-02\n'),('poll','27',1498662002,4.01,1,'ubuntu16-02\n'),('pollbill','',1498662002,73.12,0,'ubuntu16\n'),('pollbill','',1498662002,73.03,0,'ubuntu16-02\n'),('poll','23',1498662303,1.32,1,'ubuntu16-02\n'),('poll','24',1498662303,1.44,1,'ubuntu16-02\n'),('poll','25',1498662303,1.44,1,'ubuntu16-02\n'),('poll','22',1498662303,1.08,1,'ubuntu16\n'),('poll','27',1498662303,1.95,1,'ubuntu16-02\n'),('pollbill','',1498662303,73.57,0,'ubuntu16\n'),('pollbill','',1498662302,76.26,0,'ubuntu16-02\n'),('poll','24',1498662602,2.02,1,'ubuntu16-02\n'),('poll','23',1498662602,2.03,1,'ubuntu16-02\n'),('poll','25',1498662602,1.96,1,'ubuntu16-02\n'),('poll','22',1498662603,1.53,1,'ubuntu16\n'),('poll','27',1498662602,5.67,1,'ubuntu16-02\n'),('pollbill','',1498662603,73.32,0,'ubuntu16\n'),('pollbill','',1498662602,74.34,0,'ubuntu16-02\n'),('poll','22',1498662902,1.02,1,'ubuntu16\n'),('poll','25',1498662903,1.13,1,'ubuntu16-02\n'),('poll','23',1498662903,1.20,1,'ubuntu16-02\n'),('poll','24',1498662903,1.20,1,'ubuntu16-02\n'),('poll','27',1498662903,4.02,1,'ubuntu16-02\n'),('pollbill','',1498662902,72.98,0,'ubuntu16\n'),('pollbill','',1498662902,72.73,0,'ubuntu16-02\n'),('poll','22',1498663202,1.01,1,'ubuntu16\n'),('poll','23',1498663202,1.28,1,'ubuntu16-02\n'),('poll','24',1498663202,1.28,1,'ubuntu16-02\n'),('poll','25',1498663202,1.31,1,'ubuntu16-02\n'),('poll','27',1498663202,4.11,1,'ubuntu16-02\n'),('pollbill','',1498663202,73.00,0,'ubuntu16\n'),('pollbill','',1498663202,72.74,0,'ubuntu16-02\n'),('poll','22',1498663503,1.03,1,'ubuntu16\n'),('poll','23',1498663503,1.28,1,'ubuntu16-02\n'),('poll','24',1498663503,1.27,1,'ubuntu16-02\n'),('poll','25',1498663503,1.23,1,'ubuntu16-02\n'),('poll','27',1498663503,3.94,1,'ubuntu16-02\n'),('pollbill','',1498663502,72.81,0,'ubuntu16-02\n'),('pollbill','',1498663502,72.79,0,'ubuntu16\n'),('poll','22',1498663802,1.05,1,'ubuntu16\n'),('poll','25',1498663803,1.18,1,'ubuntu16-02\n'),('poll','23',1498663803,1.21,1,'ubuntu16-02\n'),('poll','24',1498663803,1.21,1,'ubuntu16-02\n'),('poll','27',1498663803,4.25,1,'ubuntu16-02\n'),('pollbill','',1498663802,72.99,0,'ubuntu16-02\n'),('pollbill','',1498663802,72.91,0,'ubuntu16\n'),('poll','25',1498664102,1.30,1,'ubuntu16-02\n'),('poll','23',1498664102,1.30,1,'ubuntu16-02\n'),('poll','24',1498664102,1.35,1,'ubuntu16-02\n'),('poll','22',1498664102,1.00,1,'ubuntu16\n'),('poll','27',1498664102,4.06,1,'ubuntu16-02\n'),('pollbill','',1498664101,72.86,0,'ubuntu16-02\n'),('pollbill','',1498664102,72.88,0,'ubuntu16\n'),('poll','24',1498664402,1.35,1,'ubuntu16-02\n'),('poll','25',1498664402,1.36,1,'ubuntu16-02\n'),('poll','23',1498664402,1.42,1,'ubuntu16-02\n'),('poll','22',1498664403,1.03,1,'ubuntu16\n'),('poll','27',1498664402,4.43,1,'ubuntu16-02\n'),('pollbill','',1498664401,72.99,0,'ubuntu16-02\n'),('pollbill','',1498664402,73.04,0,'ubuntu16\n'),('poll','23',1498664703,1.28,1,'ubuntu16-02\n'),('poll','25',1498664703,1.30,1,'ubuntu16-02\n'),('poll','24',1498664703,1.37,1,'ubuntu16-02\n'),('poll','22',1498664703,1.07,1,'ubuntu16\n'),('poll','27',1498664703,4.45,1,'ubuntu16-02\n'),('pollbill','',1498664702,73.56,0,'ubuntu16-02\n'),('pollbill','',1498664703,73.21,0,'ubuntu16\n'),('poll','22',1498665002,1.03,1,'ubuntu16\n'),('poll','24',1498665002,1.20,1,'ubuntu16-02\n'),('poll','23',1498665002,1.22,1,'ubuntu16-02\n'),('poll','25',1498665002,1.21,1,'ubuntu16-02\n'),('poll','27',1498665002,4.05,1,'ubuntu16-02\n'),('pollbill','',1498665002,72.73,0,'ubuntu16-02\n'),('pollbill','',1498665002,72.83,0,'ubuntu16\n'),('poll','22',1498665302,1.03,1,'ubuntu16\n'),('poll','23',1498665302,1.27,1,'ubuntu16-02\n'),('poll','24',1498665302,1.26,1,'ubuntu16-02\n'),('poll','25',1498665302,1.25,1,'ubuntu16-02\n'),('poll','27',1498665302,4.00,1,'ubuntu16-02\n'),('pollbill','',1498665302,73.05,0,'ubuntu16\n'),('pollbill','',1498665302,72.91,0,'ubuntu16-02\n'),('poll','23',1498665602,1.27,1,'ubuntu16-02\n'),('poll','24',1498665602,1.39,1,'ubuntu16-02\n'),('poll','25',1498665602,1.38,1,'ubuntu16-02\n'),('poll','27',1498665602,4.38,1,'ubuntu16-02\n'),('poll','22',1498665609,1.09,1,'ubuntu16\n'),('pollbill','',1498665602,72.81,0,'ubuntu16-02\n'),('pollbill','',1498665608,72.72,0,'ubuntu16\n'),('poll','22',1498665902,1.19,1,'ubuntu16\n'),('pollbill','',1498665902,72.93,0,'ubuntu16\n'),('poll','22',1498666202,1.02,1,'ubuntu16\n'),('pollbill','',1498666201,72.80,0,'ubuntu16\n'),('poll','22',1498666502,1.03,1,'ubuntu16\n'),('poll','23',1498666502,1.28,1,'ubuntu16-02\n'),('poll','25',1498666502,1.30,1,'ubuntu16-02\n'),('poll','24',1498666502,1.30,1,'ubuntu16-02\n'),('poll','27',1498666502,4.58,1,'ubuntu16-02\n'),('poll','22',1498666802,1.01,1,'ubuntu16\n'),('poll','23',1498666802,1.25,1,'ubuntu16-02\n'),('poll','24',1498666802,1.24,1,'ubuntu16-02\n'),('poll','25',1498666802,1.23,1,'ubuntu16-02\n'),('poll','27',1498666802,3.92,1,'ubuntu16-02\n'),('poll','25',1498667102,1.22,1,'ubuntu16-02\n'),('poll','23',1498667102,1.26,1,'ubuntu16-02\n'),('poll','24',1498667102,1.26,1,'ubuntu16-02\n'),('poll','22',1498667102,1.01,1,'ubuntu16\n'),('poll','27',1498667102,4.03,1,'ubuntu16-02\n'),('poll','22',1498667703,1.05,1,'ubuntu16\n'),('pollbill','',1498667702,72.76,0,'ubuntu16\n'),('pollbill','',1498699204,88.51,0,'ubuntu16\n'),('poll','22',1498699504,1.08,1,'ubuntu16\n'),('pollbill','',1498699503,73.10,0,'ubuntu16\n'),('poll','23',1498699815,9.71,1,'ubuntu16-02\n'),('poll','24',1498699816,8.75,1,'ubuntu16-02\n'),('poll','25',1498699824,1.14,1,'ubuntu16-02\n'),('poll','27',1498699815,11.10,1,'ubuntu16-02\n'),('pollbill','',1498699803,73.02,0,'ubuntu16\n'),('pollbill','',1498699802,77.50,0,'ubuntu16-02\n'),('poll','24',1498700113,10.05,1,'ubuntu16-02\n'),('poll','22',1498700171,6.82,1,'ubuntu16\n'),('poll','25',1498700113,67.73,1,'ubuntu16-02\n'),('poll','23',1498700113,74.38,1,'ubuntu16-02\n'),('poll','27',1498700113,80.18,1,'ubuntu16-02\n'),('pollbill','',1498700104,126.40,0,'ubuntu16-02\n'),('pollbill','',1498700127,161.80,0,'ubuntu16\n'),('poll','24',1498700403,3.13,1,'ubuntu16-02\n'),('poll','23',1498700403,3.15,1,'ubuntu16-02\n'),('poll','25',1498700403,3.26,1,'ubuntu16-02\n'),('poll','27',1498700403,6.56,1,'ubuntu16-02\n'),('poll','22',1498700423,3.78,1,'ubuntu16\n'),('pollbill','',1498700402,76.78,0,'ubuntu16-02\n'),('pollbill','',1498700402,113.00,0,'ubuntu16\n'),('poll','24',1498700703,1.29,1,'ubuntu16-02\n'),('poll','23',1498700703,1.31,1,'ubuntu16-02\n'),('poll','25',1498700703,1.28,1,'ubuntu16-02\n'),('poll','22',1498700703,1.11,1,'ubuntu16\n'),('poll','27',1498700703,2.52,1,'ubuntu16-02\n'),('pollbill','',1498700702,73.00,0,'ubuntu16-02\n'),('pollbill','',1498700703,73.07,0,'ubuntu16\n'),('poll','22',1498701002,1.05,1,'ubuntu16\n'),('poll','23',1498701002,1.31,1,'ubuntu16-02\n'),('poll','24',1498701002,1.43,1,'ubuntu16-02\n'),('poll','25',1498701002,1.41,1,'ubuntu16-02\n'),('poll','27',1498701002,3.37,1,'ubuntu16-02\n'),('pollbill','',1498701001,73.36,0,'ubuntu16-02\n'),('pollbill','',1498701002,73.87,0,'ubuntu16\n'),('poll','22',1498701303,1.25,1,'ubuntu16\n'),('poll','24',1498701303,1.62,1,'ubuntu16-02\n'),('poll','23',1498701303,1.61,1,'ubuntu16-02\n'),('poll','25',1498701303,1.64,1,'ubuntu16-02\n'),('poll','27',1498701303,5.09,1,'ubuntu16-02\n'),('pollbill','',1498701302,73.64,0,'ubuntu16-02\n'),('pollbill','',1498701302,75.06,0,'ubuntu16\n'),('poll','23',1498701902,1.45,1,'ubuntu16-02\n'),('poll','25',1498701903,1.38,1,'ubuntu16-02\n'),('poll','24',1498701903,1.38,1,'ubuntu16-02\n'),('poll','27',1498701903,4.62,1,'ubuntu16-02\n'),('pollbill','',1498701902,74.32,0,'ubuntu16-02\n'),('poll','24',1498702203,1.48,1,'ubuntu16-02\n'),('poll','23',1498702203,1.61,1,'ubuntu16-02\n'),('poll','25',1498702203,1.64,1,'ubuntu16-02\n'),('poll','27',1498702203,3.64,1,'ubuntu16-02\n'),('pollbill','',1498702202,76.05,0,'ubuntu16-02\n'),('poll','24',1498702503,1.20,1,'ubuntu16-02\n'),('pollbill','',1498702502,74.10,0,'ubuntu16-02\n'),('poll','25',1498702808,2.86,1,'ubuntu16-02\n'),('poll','23',1498702808,2.48,1,'ubuntu16-02\n'),('poll','24',1498702808,2.76,1,'ubuntu16-02\n'),('poll','27',1498702808,9.19,1,'ubuntu16-02\n'),('pollbill','',1498702807,76.84,0,'ubuntu16-02\n'),('poll','25',1498703102,1.90,1,'ubuntu16-02\n'),('poll','24',1498703102,1.90,1,'ubuntu16-02\n'),('poll','23',1498703102,1.89,1,'ubuntu16-02\n'),('poll','27',1498703102,4.29,1,'ubuntu16-02\n'),('pollbill','',1498703101,85.20,0,'ubuntu16-02\n'),('poll','23',1498703403,3.18,1,'ubuntu16-02\n'),('poll','25',1498703403,2.53,1,'ubuntu16-02\n'),('poll','24',1498703403,3.21,1,'ubuntu16-02\n'),('poll','27',1498703403,14.73,1,'ubuntu16-02\n'),('pollbill','',1498703402,81.47,0,'ubuntu16-02\n'),('poll','24',1498703703,1.48,1,'ubuntu16-02\n'),('poll','23',1498703703,1.32,1,'ubuntu16-02\n'),('poll','27',1498703703,2.66,1,'ubuntu16-02\n'),('pollbill','',1498703702,73.62,0,'ubuntu16-02\n'),('poll','23',1498704003,1.87,1,'ubuntu16-02\n'),('poll','24',1498704003,2.00,1,'ubuntu16-02\n'),('poll','25',1498704003,2.22,1,'ubuntu16-02\n'),('poll','27',1498704003,5.22,1,'ubuntu16-02\n'),('pollbill','',1498704002,76.35,0,'ubuntu16-02\n'),('pollbill','',1498704302,73.05,0,'ubuntu16-02\n'),('poll','25',1498704604,2.37,1,'ubuntu16-02\n'),('poll','23',1498704603,2.67,1,'ubuntu16-02\n'),('poll','24',1498704603,2.60,1,'ubuntu16-02\n'),('poll','27',1498704604,5.00,1,'ubuntu16-02\n'),('pollbill','',1498704602,84.39,0,'ubuntu16-02\n'),('poll','25',1498704907,7.66,1,'ubuntu16-02\n'),('poll','24',1498704906,8.94,1,'ubuntu16-02\n'),('poll','23',1498704909,9.05,1,'ubuntu16-02\n'),('poll','27',1498704906,44.29,1,'ubuntu16-02\n'),('pollbill','',1498704904,102.40,0,'ubuntu16-02\n'),('pollbill','',1498705202,81.04,0,'ubuntu16-02\n'),('poll','23',1498705502,1.30,1,'ubuntu16-02\n'),('poll','25',1498705502,1.42,1,'ubuntu16-02\n'),('poll','24',1498705502,1.43,1,'ubuntu16-02\n'),('poll','27',1498705502,2.81,1,'ubuntu16-02\n'),('pollbill','',1498705502,73.29,0,'ubuntu16-02\n'),('poll','23',1498705802,1.25,1,'ubuntu16-02\n'),('poll','24',1498705802,1.24,1,'ubuntu16-02\n'),('poll','25',1498705802,1.18,1,'ubuntu16-02\n'),('poll','27',1498705802,2.79,1,'ubuntu16-02\n'),('pollbill','',1498705801,73.45,0,'ubuntu16-02\n'),('poll','24',1498706103,1.27,1,'ubuntu16-02\n'),('poll','25',1498706103,1.25,1,'ubuntu16-02\n'),('poll','23',1498706103,1.21,1,'ubuntu16-02\n'),('poll','27',1498706103,3.73,1,'ubuntu16-02\n'),('pollbill','',1498706102,73.12,0,'ubuntu16-02\n'),('poll','23',1498706402,1.46,1,'ubuntu16-02\n'),('poll','24',1498706402,1.45,1,'ubuntu16-02\n'),('poll','25',1498706402,1.46,1,'ubuntu16-02\n'),('poll','27',1498706402,2.41,1,'ubuntu16-02\n'),('pollbill','',1498706401,73.42,0,'ubuntu16-02\n'),('poll','23',1498706703,1.51,1,'ubuntu16-02\n'),('poll','25',1498706703,1.49,1,'ubuntu16-02\n'),('poll','24',1498706703,1.84,1,'ubuntu16-02\n'),('poll','27',1498706703,3.56,1,'ubuntu16-02\n'),('pollbill','',1498706702,73.37,0,'ubuntu16-02\n'),('poll','23',1498707002,1.33,1,'ubuntu16-02\n'),('poll','25',1498707002,1.27,1,'ubuntu16-02\n'),('poll','24',1498707002,1.32,1,'ubuntu16-02\n'),('poll','27',1498707002,1.97,1,'ubuntu16-02\n'),('pollbill','',1498707002,73.26,0,'ubuntu16-02\n'),('poll','24',1498707303,1.21,1,'ubuntu16-02\n'),('poll','23',1498707303,1.25,1,'ubuntu16-02\n'),('poll','25',1498707303,1.25,1,'ubuntu16-02\n'),('poll','27',1498707303,1.98,1,'ubuntu16-02\n'),('pollbill','',1498707302,72.82,0,'ubuntu16-02\n'),('poll','23',1498707602,1.23,1,'ubuntu16-02\n'),('poll','25',1498707602,1.34,1,'ubuntu16-02\n'),('poll','24',1498707602,1.37,1,'ubuntu16-02\n'),('poll','27',1498707602,1.86,1,'ubuntu16-02\n'),('pollbill','',1498707601,72.88,0,'ubuntu16-02\n'),('poll','23',1498707902,1.28,1,'ubuntu16-02\n'),('poll','25',1498707902,1.25,1,'ubuntu16-02\n'),('poll','24',1498707902,1.26,1,'ubuntu16-02\n'),('poll','27',1498707902,1.81,1,'ubuntu16-02\n'),('pollbill','',1498707901,72.76,0,'ubuntu16-02\n'),('poll','24',1498708202,1.26,1,'ubuntu16-02\n'),('poll','25',1498708202,1.33,1,'ubuntu16-02\n'),('poll','23',1498708202,1.34,1,'ubuntu16-02\n'),('poll','27',1498708202,2.70,1,'ubuntu16-02\n'),('pollbill','',1498708201,72.89,0,'ubuntu16-02\n'),('poll','24',1498708502,1.09,1,'ubuntu16-02\n'),('poll','25',1498708502,1.13,1,'ubuntu16-02\n'),('poll','23',1498708502,1.13,1,'ubuntu16-02\n'),('poll','27',1498708502,2.24,1,'ubuntu16-02\n'),('pollbill','',1498708502,73.07,0,'ubuntu16-02\n'),('poll','23',1498708803,1.46,1,'ubuntu16-02\n'),('poll','24',1498708803,1.41,1,'ubuntu16-02\n'),('poll','25',1498708803,1.43,1,'ubuntu16-02\n'),('poll','27',1498708803,2.86,1,'ubuntu16-02\n'),('pollbill','',1498708802,73.14,0,'ubuntu16-02\n'),('poll','25',1498709102,1.29,1,'ubuntu16-02\n'),('poll','24',1498709102,1.30,1,'ubuntu16-02\n'),('poll','23',1498709102,1.32,1,'ubuntu16-02\n'),('poll','27',1498709102,4.29,1,'ubuntu16-02\n'),('pollbill','',1498709101,72.84,0,'ubuntu16-02\n'),('poll','24',1498709403,3.46,1,'ubuntu16-02\n'),('poll','25',1498709403,3.52,1,'ubuntu16-02\n'),('poll','23',1498709403,3.54,1,'ubuntu16-02\n'),('poll','27',1498709403,16.45,1,'ubuntu16-02\n'),('pollbill','',1498709402,88.70,0,'ubuntu16-02\n'),('poll','24',1498709707,1.42,1,'ubuntu16-02\n'),('poll','23',1498709707,1.87,1,'ubuntu16-02\n'),('poll','25',1498709707,2.24,1,'ubuntu16-02\n'),('poll','27',1498709707,2.65,1,'ubuntu16-02\n'),('poll','23',1498710006,5.43,1,'ubuntu16-02\n'),('poll','24',1498710005,5.54,1,'ubuntu16-02\n'),('poll','25',1498710004,7.13,1,'ubuntu16-02\n'),('poll','27',1498710004,9.88,1,'ubuntu16-02\n'),('pollbill','',1498710004,78.99,0,'ubuntu16-02\n'),('poll','23',1498710303,1.31,1,'ubuntu16-02\n'),('poll','24',1498710303,1.29,1,'ubuntu16-02\n'),('poll','25',1498710303,1.35,1,'ubuntu16-02\n'),('poll','27',1498710303,2.75,1,'ubuntu16-02\n'),('pollbill','',1498710302,73.31,0,'ubuntu16-02\n'),('poll','24',1498710602,1.22,1,'ubuntu16-02\n'),('poll','23',1498710602,1.24,1,'ubuntu16-02\n'),('poll','25',1498710602,1.29,1,'ubuntu16-02\n'),('poll','27',1498710602,2.30,1,'ubuntu16-02\n'),('pollbill','',1498710602,72.98,0,'ubuntu16-02\n'),('poll','24',1498710903,1.34,1,'ubuntu16-02\n'),('poll','23',1498710902,1.35,1,'ubuntu16-02\n'),('poll','25',1498710903,1.35,1,'ubuntu16-02\n'),('poll','27',1498710902,2.06,1,'ubuntu16-02\n'),('pollbill','',1498710902,72.90,0,'ubuntu16-02\n'),('poll','23',1498711202,1.25,1,'ubuntu16-02\n'),('poll','25',1498711202,1.23,1,'ubuntu16-02\n'),('poll','24',1498711202,1.24,1,'ubuntu16-02\n'),('poll','27',1498711202,3.50,1,'ubuntu16-02\n'),('poll','24',1498725008,4.20,1,'ubuntu16-02\n'),('poll','25',1498725008,4.19,1,'ubuntu16-02\n'),('poll','23',1498725008,4.23,1,'ubuntu16-02\n'),('poll','27',1498725008,12.80,1,'ubuntu16-02\n'),('pollbill','',1498725006,85.34,0,'ubuntu16-02\n'),('poll','24',1498725302,1.59,1,'ubuntu16-02\n'),('poll','25',1498725303,1.28,1,'ubuntu16-02\n'),('poll','23',1498725303,1.24,1,'ubuntu16-02\n'),('poll','27',1498725302,3.46,1,'ubuntu16-02\n'),('pollbill','',1498725302,73.01,0,'ubuntu16-02\n'),('poll','24',1498725602,2.15,1,'ubuntu16-02\n'),('poll','25',1498725602,3.31,1,'ubuntu16-02\n'),('poll','27',1498725602,3.63,1,'ubuntu16-02\n'),('poll','23',1498725602,3.68,1,'ubuntu16-02\n'),('pollbill','',1498725602,73.50,0,'ubuntu16-02\n'),('poll','23',1498725902,1.18,1,'ubuntu16-02\n'),('poll','24',1498725902,1.21,1,'ubuntu16-02\n'),('poll','25',1498725902,1.18,1,'ubuntu16-02\n'),('poll','27',1498725902,1.79,1,'ubuntu16-02\n'),('pollbill','',1498725901,72.83,0,'ubuntu16-02\n'),('poll','24',1498726202,1.21,1,'ubuntu16-02\n'),('poll','23',1498726202,1.23,1,'ubuntu16-02\n'),('poll','25',1498726202,1.27,1,'ubuntu16-02\n'),('poll','27',1498726202,2.00,1,'ubuntu16-02\n'),('pollbill','',1498726201,73.18,0,'ubuntu16-02\n'),('poll','25',1498726502,1.73,1,'ubuntu16-02\n'),('poll','23',1498726502,2.29,1,'ubuntu16-02\n'),('poll','27',1498726502,3.20,1,'ubuntu16-02\n'),('poll','24',1498726502,3.37,1,'ubuntu16-02\n'),('pollbill','',1498726502,73.37,0,'ubuntu16-02\n'),('poll','24',1498726802,1.24,1,'ubuntu16-02\n'),('poll','25',1498726802,1.28,1,'ubuntu16-02\n'),('poll','23',1498726802,1.29,1,'ubuntu16-02\n'),('poll','27',1498726802,2.40,1,'ubuntu16-02\n'),('pollbill','',1498726801,72.86,0,'ubuntu16-02\n'),('poll','25',1498727102,1.18,1,'ubuntu16-02\n'),('poll','23',1498727102,1.20,1,'ubuntu16-02\n'),('poll','24',1498727102,1.21,1,'ubuntu16-02\n'),('poll','27',1498727102,2.14,1,'ubuntu16-02\n'),('pollbill','',1498727101,72.90,0,'ubuntu16-02\n'),('poll','23',1498727402,1.18,1,'ubuntu16-02\n'),('poll','25',1498727402,1.26,1,'ubuntu16-02\n'),('poll','24',1498727402,1.27,1,'ubuntu16-02\n'),('poll','27',1498727402,3.45,1,'ubuntu16-02\n'),('pollbill','',1498727402,72.98,0,'ubuntu16-02\n'),('poll','25',1498727702,1.20,1,'ubuntu16-02\n'),('poll','24',1498727702,1.21,1,'ubuntu16-02\n'),('poll','23',1498727702,1.20,1,'ubuntu16-02\n'),('poll','27',1498727702,1.76,1,'ubuntu16-02\n'),('pollbill','',1498727702,72.80,0,'ubuntu16-02\n'),('poll','25',1498728003,1.77,1,'ubuntu16-02\n'),('poll','23',1498728003,1.77,1,'ubuntu16-02\n'),('poll','24',1498728003,1.67,1,'ubuntu16-02\n'),('poll','27',1498728003,2.44,1,'ubuntu16-02\n'),('pollbill','',1498728002,73.47,0,'ubuntu16-02\n'),('poll','24',1498728302,1.21,1,'ubuntu16-02\n'),('poll','25',1498728302,1.21,1,'ubuntu16-02\n'),('poll','23',1498728302,1.22,1,'ubuntu16-02\n'),('poll','27',1498728302,2.32,1,'ubuntu16-02\n'),('pollbill','',1498728302,72.96,0,'ubuntu16-02\n'),('poll','24',1498728602,1.24,1,'ubuntu16-02\n'),('poll','23',1498728602,1.23,1,'ubuntu16-02\n'),('poll','25',1498728602,1.22,1,'ubuntu16-02\n'),('poll','27',1498728602,1.80,1,'ubuntu16-02\n'),('pollbill','',1498728602,73.49,0,'ubuntu16-02\n'),('poll','25',1498728902,1.20,1,'ubuntu16-02\n'),('poll','23',1498728902,1.17,1,'ubuntu16-02\n'),('poll','24',1498728902,1.16,1,'ubuntu16-02\n'),('poll','27',1498728902,3.54,1,'ubuntu16-02\n'),('pollbill','',1498728902,72.85,0,'ubuntu16-02\n'),('poll','25',1498729202,1.23,1,'ubuntu16-02\n'),('poll','23',1498729202,1.23,1,'ubuntu16-02\n'),('poll','24',1498729202,1.23,1,'ubuntu16-02\n'),('poll','27',1498729202,2.57,1,'ubuntu16-02\n'),('pollbill','',1498729202,72.80,0,'ubuntu16-02\n'),('poll','25',1498729502,1.21,1,'ubuntu16-02\n'),('poll','23',1498729502,1.20,1,'ubuntu16-02\n'),('poll','24',1498729502,1.19,1,'ubuntu16-02\n'),('poll','27',1498729502,1.62,1,'ubuntu16-02\n'),('pollbill','',1498729502,72.85,0,'ubuntu16-02\n'),('poll','25',1498729803,1.21,1,'ubuntu16-02\n'),('poll','23',1498729803,1.27,1,'ubuntu16-02\n'),('poll','24',1498729803,1.26,1,'ubuntu16-02\n'),('poll','27',1498729803,3.13,1,'ubuntu16-02\n'),('pollbill','',1498729802,73.11,0,'ubuntu16-02\n'),('poll','25',1498730102,1.29,1,'ubuntu16-02\n'),('poll','24',1498730102,1.35,1,'ubuntu16-02\n'),('poll','23',1498730102,1.36,1,'ubuntu16-02\n'),('poll','27',1498730102,2.12,1,'ubuntu16-02\n'),('pollbill','',1498730102,73.14,0,'ubuntu16-02\n'),('poll','24',1498730402,1.22,1,'ubuntu16-02\n'),('poll','25',1498730402,1.24,1,'ubuntu16-02\n'),('poll','23',1498730402,1.33,1,'ubuntu16-02\n'),('poll','27',1498730402,1.92,1,'ubuntu16-02\n'),('pollbill','',1498730402,72.76,0,'ubuntu16-02\n'),('poll','23',1498730702,1.26,1,'ubuntu16-02\n'),('poll','25',1498730702,1.30,1,'ubuntu16-02\n'),('poll','24',1498730702,1.34,1,'ubuntu16-02\n'),('poll','27',1498730702,2.25,1,'ubuntu16-02\n'),('pollbill','',1498730702,73.35,0,'ubuntu16-02\n'),('poll','23',1498731002,1.20,1,'ubuntu16-02\n'),('poll','24',1498731002,1.25,1,'ubuntu16-02\n'),('poll','25',1498731002,1.25,1,'ubuntu16-02\n'),('poll','27',1498731002,3.39,1,'ubuntu16-02\n'),('pollbill','',1498731002,73.03,0,'ubuntu16-02\n'),('poll','25',1498740606,3.68,1,'ubuntu16-02\n'),('poll','23',1498740606,3.70,1,'ubuntu16-02\n'),('poll','24',1498740606,3.69,1,'ubuntu16-02\n'),('poll','27',1498740606,19.78,1,'ubuntu16-02\n'),('pollbill','',1498740605,90.03,0,'ubuntu16-02\n'),('poll','25',1498740903,1.24,1,'ubuntu16-02\n'),('poll','23',1498740903,1.27,1,'ubuntu16-02\n'),('poll','24',1498740903,1.26,1,'ubuntu16-02\n'),('poll','27',1498740902,4.10,1,'ubuntu16-02\n'),('pollbill','',1498740902,72.89,0,'ubuntu16-02\n'),('poll','25',1498741203,1.17,1,'ubuntu16-02\n'),('poll','24',1498741203,1.24,1,'ubuntu16-02\n'),('poll','23',1498741203,1.36,1,'ubuntu16-02\n'),('poll','27',1498741203,4.18,1,'ubuntu16-02\n'),('pollbill','',1498741202,73.26,0,'ubuntu16-02\n'),('poll','25',1498741502,1.29,1,'ubuntu16-02\n'),('poll','23',1498741503,1.30,1,'ubuntu16-02\n'),('poll','24',1498741502,1.32,1,'ubuntu16-02\n'),('poll','27',1498741502,4.22,1,'ubuntu16-02\n'),('pollbill','',1498741502,73.17,0,'ubuntu16-02\n'),('poll','25',1498741802,1.40,1,'ubuntu16-02\n'),('poll','23',1498741802,1.42,1,'ubuntu16-02\n'),('poll','24',1498741802,1.48,1,'ubuntu16-02\n'),('poll','27',1498741802,4.51,1,'ubuntu16-02\n'),('pollbill','',1498741802,73.12,0,'ubuntu16-02\n'),('poll','24',1498742103,1.32,1,'ubuntu16-02\n'),('poll','23',1498742103,1.46,1,'ubuntu16-02\n'),('poll','25',1498742103,1.50,1,'ubuntu16-02\n'),('poll','27',1498742103,4.32,1,'ubuntu16-02\n'),('pollbill','',1498742102,73.18,0,'ubuntu16-02\n'),('poll','23',1498742402,1.23,1,'ubuntu16-02\n'),('poll','24',1498742402,1.26,1,'ubuntu16-02\n'),('poll','25',1498742402,1.28,1,'ubuntu16-02\n'),('poll','27',1498742402,4.22,1,'ubuntu16-02\n'),('pollbill','',1498742401,72.96,0,'ubuntu16-02\n'),('poll','23',1498742702,1.25,1,'ubuntu16-02\n'),('poll','25',1498742702,1.24,1,'ubuntu16-02\n'),('poll','24',1498742702,1.24,1,'ubuntu16-02\n'),('poll','27',1498742702,4.20,1,'ubuntu16-02\n'),('pollbill','',1498742702,73.04,0,'ubuntu16-02\n'),('poll','24',1498743004,1.45,1,'ubuntu16-02\n'),('poll','23',1498743004,1.56,1,'ubuntu16-02\n'),('poll','25',1498743004,1.44,1,'ubuntu16-02\n'),('poll','27',1498743004,4.77,1,'ubuntu16-02\n'),('pollbill','',1498743002,73.63,0,'ubuntu16-02\n'),('poll','24',1498743302,1.35,1,'ubuntu16-02\n'),('poll','23',1498743302,1.35,1,'ubuntu16-02\n'),('poll','25',1498743302,1.34,1,'ubuntu16-02\n'),('poll','27',1498743302,4.50,1,'ubuntu16-02\n'),('pollbill','',1498743301,73.11,0,'ubuntu16-02\n'),('poll','25',1498743602,1.23,1,'ubuntu16-02\n'),('poll','23',1498743602,1.25,1,'ubuntu16-02\n'),('poll','24',1498743602,1.23,1,'ubuntu16-02\n'),('poll','27',1498743602,2.05,1,'ubuntu16-02\n'),('pollbill','',1498743602,73.10,0,'ubuntu16-02\n'),('poll','23',1498743903,1.31,1,'ubuntu16-02\n'),('poll','24',1498743903,1.32,1,'ubuntu16-02\n'),('poll','25',1498743903,1.37,1,'ubuntu16-02\n'),('poll','27',1498743903,4.70,1,'ubuntu16-02\n'),('pollbill','',1498743902,72.99,0,'ubuntu16-02\n'),('poll','23',1498744202,1.25,1,'ubuntu16-02\n'),('poll','25',1498744202,1.23,1,'ubuntu16-02\n'),('poll','24',1498744202,1.25,1,'ubuntu16-02\n'),('poll','27',1498744202,4.15,1,'ubuntu16-02\n'),('pollbill','',1498744201,73.05,0,'ubuntu16-02\n'),('poll','23',1498744502,1.24,1,'ubuntu16-02\n'),('poll','25',1498744502,1.25,1,'ubuntu16-02\n'),('poll','24',1498744502,1.25,1,'ubuntu16-02\n'),('poll','27',1498744502,4.26,1,'ubuntu16-02\n'),('pollbill','',1498744502,73.30,0,'ubuntu16-02\n'),('poll','22',1499131388,1.51,1,'ubuntu16\n'),('poll','22',1499131502,1.12,1,'ubuntu16\n'),('poll','25',1499131513,2.38,1,'ubuntu16-02\n'),('poll','24',1499131513,2.40,1,'ubuntu16-02\n'),('poll','23',1499131513,2.41,1,'ubuntu16-02\n'),('poll','27',1499131513,9.97,1,'ubuntu16-02\n'),('pollbill','',1499131501,73.16,0,'ubuntu16\n'),('pollbill','',1499131510,84.52,0,'ubuntu16-02\n'),('poll','22',1499131613,1.01,1,'ubuntu16\n'),('poll','28',1499131613,1.26,1,'ubuntu16\n'),('discover','new',1499131802,0.89,1,'ubuntu16\n'),('poll','22',1499131802,1.12,1,'ubuntu16\n'),('poll','23',1499131802,1.54,1,'ubuntu16-02\n'),('poll','25',1499131802,3.48,1,'ubuntu16-02\n'),('poll','24',1499131802,3.52,1,'ubuntu16-02\n'),('poll','27',1499131802,5.12,1,'ubuntu16-02\n'),('poll','28',1499131802,5.82,1,'ubuntu16\n'),('pollbill','',1499131802,83.79,0,'ubuntu16-02\n'),('pollbill','',1499131802,91.35,0,'ubuntu16\n'),('poll','24',1499132106,3.23,1,'ubuntu16-02\n'),('poll','25',1499132108,1.21,1,'ubuntu16-02\n'),('poll','23',1499132106,3.14,1,'ubuntu16-02\n'),('poll','22',1499132108,0.99,1,'ubuntu16\n'),('poll','27',1499132107,3.29,1,'ubuntu16-02\n'),('poll','28',1499132108,3.09,1,'ubuntu16\n'),('pollbill','',1499132102,73.93,0,'ubuntu16\n'),('pollbill','',1499132102,79.31,0,'ubuntu16-02\n'),('poll','25',1499132402,1.26,1,'ubuntu16-02\n'),('poll','24',1499132402,1.31,1,'ubuntu16-02\n'),('poll','23',1499132402,1.30,1,'ubuntu16-02\n'),('poll','22',1499132403,0.99,1,'ubuntu16\n'),('poll','27',1499132402,1.68,1,'ubuntu16-02\n'),('poll','28',1499132403,1.42,1,'ubuntu16\n'),('pollbill','',1499132402,72.99,0,'ubuntu16-02\n'),('pollbill','',1499132403,73.33,0,'ubuntu16\n'),('poll','22',1499132702,1.17,1,'ubuntu16\n'),('poll','28',1499132702,1.55,1,'ubuntu16\n'),('poll','23',1499132703,1.24,1,'ubuntu16-02\n'),('poll','24',1499132703,1.25,1,'ubuntu16-02\n'),('poll','25',1499132703,1.26,1,'ubuntu16-02\n'),('poll','27',1499132703,1.66,1,'ubuntu16-02\n'),('pollbill','',1499132701,73.06,0,'ubuntu16\n'),('pollbill','',1499132702,73.14,0,'ubuntu16-02\n'),('poll','25',1499133002,1.39,1,'ubuntu16-02\n'),('poll','23',1499133002,1.38,1,'ubuntu16-02\n'),('poll','24',1499133002,1.49,1,'ubuntu16-02\n'),('poll','22',1499133002,1.05,1,'ubuntu16\n'),('poll','27',1499133002,1.75,1,'ubuntu16-02\n'),('poll','28',1499133002,1.71,1,'ubuntu16\n'),('pollbill','',1499133001,73.04,0,'ubuntu16-02\n'),('pollbill','',1499133002,72.98,0,'ubuntu16\n'),('poll','24',1499133311,11.34,1,'ubuntu16-02\n'),('poll','25',1499133311,11.35,1,'ubuntu16-02\n'),('poll','23',1499133311,12.21,1,'ubuntu16-02\n'),('poll','22',1499133324,31.92,1,'ubuntu16\n'),('poll','27',1499133311,45.19,1,'ubuntu16-02\n'),('poll','28',1499133351,14.57,1,'ubuntu16\n'),('pollbill','',1499133310,96.84,0,'ubuntu16-02\n'),('pollbill','',1499133318,101.90,0,'ubuntu16\n'),('poll','24',1499133602,6.23,1,'ubuntu16-02\n'),('poll','25',1499133602,6.24,1,'ubuntu16-02\n'),('poll','23',1499133602,7.02,1,'ubuntu16-02\n'),('poll','27',1499133602,7.34,1,'ubuntu16-02\n'),('poll','22',1499133609,1.04,1,'ubuntu16\n'),('poll','28',1499133603,7.79,1,'ubuntu16\n'),('pollbill','',1499133602,78.36,0,'ubuntu16-02\n'),('pollbill','',1499133609,73.17,0,'ubuntu16\n'),('poll','23',1499133902,39.94,1,'ubuntu16-02\n'),('poll','25',1499133902,38.44,1,'ubuntu16-02\n'),('poll','24',1499133902,80.03,1,'ubuntu16-02\n'),('poll','27',1499133902,126.30,1,'ubuntu16-02\n'),('poll','22',1499134078,17.28,1,'ubuntu16\n'),('poll','28',1499134078,17.46,1,'ubuntu16\n'),('pollbill','',1499133901,200.70,0,'ubuntu16-02\n'),('pollbill','',1499134078,81.68,0,'ubuntu16\n'),('poll','23',1499134202,1.23,1,'ubuntu16-02\n'),('poll','25',1499134202,1.25,1,'ubuntu16-02\n'),('poll','24',1499134202,1.26,1,'ubuntu16-02\n'),('poll','27',1499134202,1.93,1,'ubuntu16-02\n'),('pollbill','',1499134201,73.11,0,'ubuntu16-02\n'),('pollbill','',1499134202,73.05,0,'ubuntu16\n'),('poll','22',1499134502,1.09,1,'ubuntu16\n'),('poll','28',1499134502,1.67,1,'ubuntu16\n'),('poll','25',1499134502,1.22,1,'ubuntu16-02\n'),('poll','23',1499134502,1.25,1,'ubuntu16-02\n'),('poll','24',1499134502,1.23,1,'ubuntu16-02\n'),('poll','27',1499134502,2.09,1,'ubuntu16-02\n'),('pollbill','',1499134501,73.09,0,'ubuntu16\n'),('pollbill','',1499134502,72.97,0,'ubuntu16-02\n'),('poll','23',1499134802,1.24,1,'ubuntu16-02\n'),('poll','24',1499134802,1.22,1,'ubuntu16-02\n'),('poll','25',1499134803,1.18,1,'ubuntu16-02\n'),('poll','22',1499134803,1.06,1,'ubuntu16\n'),('poll','27',1499134803,1.98,1,'ubuntu16-02\n'),('poll','28',1499134803,1.79,1,'ubuntu16\n'),('pollbill','',1499134802,73.16,0,'ubuntu16-02\n'),('pollbill','',1499134802,72.78,0,'ubuntu16\n'),('poll','22',1499135102,0.98,1,'ubuntu16\n'),('poll','23',1499135102,1.17,1,'ubuntu16-02\n'),('poll','28',1499135102,1.89,1,'ubuntu16\n'),('poll','24',1499135103,1.18,1,'ubuntu16-02\n'),('poll','25',1499135103,1.18,1,'ubuntu16-02\n'),('poll','27',1499135103,1.78,1,'ubuntu16-02\n'),('pollbill','',1499135102,73.11,0,'ubuntu16\n'),('pollbill','',1499135102,73.22,0,'ubuntu16-02\n'),('poll','24',1499135402,1.11,1,'ubuntu16-02\n'),('poll','23',1499135402,1.26,1,'ubuntu16-02\n'),('poll','25',1499135402,1.25,1,'ubuntu16-02\n'),('poll','22',1499135403,1.10,1,'ubuntu16\n'),('poll','27',1499135402,2.13,1,'ubuntu16-02\n'),('poll','28',1499135403,2.20,1,'ubuntu16\n'),('pollbill','',1499135403,72.83,0,'ubuntu16\n'),('pollbill','',1499135402,74.27,0,'ubuntu16-02\n'),('poll','22',1499135702,1.08,1,'ubuntu16\n'),('poll','25',1499135703,1.22,1,'ubuntu16-02\n'),('poll','23',1499135703,1.26,1,'ubuntu16-02\n'),('poll','24',1499135703,1.31,1,'ubuntu16-02\n'),('poll','28',1499135702,1.70,1,'ubuntu16\n'),('poll','27',1499135703,1.85,1,'ubuntu16-02\n'),('pollbill','',1499135702,73.14,0,'ubuntu16-02\n'),('pollbill','',1499135702,73.10,0,'ubuntu16\n'),('poll','25',1499136002,1.25,1,'ubuntu16-02\n'),('poll','24',1499136002,1.31,1,'ubuntu16-02\n'),('poll','22',1499136003,1.07,1,'ubuntu16\n'),('poll','23',1499136002,1.43,1,'ubuntu16-02\n'),('poll','27',1499136002,1.83,1,'ubuntu16-02\n'),('poll','28',1499136003,1.61,1,'ubuntu16\n'),('pollbill','',1499136002,73.03,0,'ubuntu16-02\n'),('pollbill','',1499136002,72.83,0,'ubuntu16\n'),('poll','22',1499136303,1.15,1,'ubuntu16\n'),('poll','23',1499136303,1.12,1,'ubuntu16-02\n'),('poll','25',1499136303,1.18,1,'ubuntu16-02\n'),('poll','24',1499136303,1.18,1,'ubuntu16-02\n'),('poll','28',1499136303,1.73,1,'ubuntu16\n'),('poll','27',1499136303,2.72,1,'ubuntu16-02\n'),('pollbill','',1499136302,72.98,0,'ubuntu16\n'),('pollbill','',1499136302,73.45,0,'ubuntu16-02\n'),('poll','23',1499136602,1.24,1,'ubuntu16-02\n'),('poll','24',1499136602,1.34,1,'ubuntu16-02\n'),('poll','22',1499136602,1.01,1,'ubuntu16\n'),('poll','25',1499136602,1.14,1,'ubuntu16-02\n'),('poll','28',1499136602,1.70,1,'ubuntu16\n'),('poll','27',1499136602,1.90,1,'ubuntu16-02\n'),('pollbill','',1499136602,72.93,0,'ubuntu16-02\n'),('pollbill','',1499136602,72.81,0,'ubuntu16\n'),('poll','22',1499136902,1.02,1,'ubuntu16\n'),('poll','28',1499136902,1.72,1,'ubuntu16\n'),('poll','24',1499136902,1.20,1,'ubuntu16-02\n'),('poll','23',1499136902,1.22,1,'ubuntu16-02\n'),('poll','25',1499136902,1.21,1,'ubuntu16-02\n'),('poll','27',1499136902,1.80,1,'ubuntu16-02\n'),('pollbill','',1499136901,72.82,0,'ubuntu16\n'),('pollbill','',1499136902,72.79,0,'ubuntu16-02\n'),('poll','22',1499137202,1.04,1,'ubuntu16\n'),('poll','23',1499137202,1.24,1,'ubuntu16-02\n'),('poll','25',1499137202,1.26,1,'ubuntu16-02\n'),('poll','24',1499137202,1.34,1,'ubuntu16-02\n'),('poll','28',1499137202,1.56,1,'ubuntu16\n'),('poll','27',1499137202,1.83,1,'ubuntu16-02\n'),('pollbill','',1499137202,73.19,0,'ubuntu16-02\n'),('pollbill','',1499137202,73.32,0,'ubuntu16\n'),('poll','22',1499137502,1.10,1,'ubuntu16\n'),('poll','28',1499137502,1.30,1,'ubuntu16\n'),('poll','24',1499137502,1.20,1,'ubuntu16-02\n'),('poll','23',1499137502,1.20,1,'ubuntu16-02\n'),('poll','25',1499137502,1.19,1,'ubuntu16-02\n'),('poll','27',1499137502,1.91,1,'ubuntu16-02\n'),('pollbill','',1499137502,72.90,0,'ubuntu16\n'),('pollbill','',1499137502,73.01,0,'ubuntu16-02\n'),('poll','22',1499137802,1.13,1,'ubuntu16\n'),('poll','24',1499137802,1.22,1,'ubuntu16-02\n'),('poll','23',1499137802,1.31,1,'ubuntu16-02\n'),('poll','28',1499137802,1.70,1,'ubuntu16\n'),('poll','25',1499137802,1.31,1,'ubuntu16-02\n'),('poll','27',1499137802,1.70,1,'ubuntu16-02\n'),('pollbill','',1499137801,73.00,0,'ubuntu16\n'),('pollbill','',1499137802,72.96,0,'ubuntu16-02\n'),('poll','22',1499138102,1.11,1,'ubuntu16\n'),('poll','25',1499138103,1.15,1,'ubuntu16-02\n'),('poll','23',1499138103,1.26,1,'ubuntu16-02\n'),('poll','24',1499138103,1.31,1,'ubuntu16-02\n'),('poll','28',1499138102,1.99,1,'ubuntu16\n'),('poll','27',1499138103,2.29,1,'ubuntu16-02\n'),('pollbill','',1499138102,72.96,0,'ubuntu16-02\n'),('pollbill','',1499138102,73.01,0,'ubuntu16\n'),('poll','25',1499138407,4.58,1,'ubuntu16-02\n'),('poll','23',1499138407,4.09,1,'ubuntu16-02\n'),('poll','24',1499138407,4.38,1,'ubuntu16-02\n'),('poll','27',1499138408,4.65,1,'ubuntu16-02\n'),('poll','22',1499138412,1.14,1,'ubuntu16\n'),('poll','28',1499138412,1.57,1,'ubuntu16\n'),('pollbill','',1499138403,78.43,0,'ubuntu16-02\n'),('pollbill','',1499138412,73.15,0,'ubuntu16\n'),('poll','25',1499138702,2.08,1,'ubuntu16-02\n'),('poll','23',1499138702,2.05,1,'ubuntu16-02\n'),('poll','24',1499138702,2.27,1,'ubuntu16-02\n'),('poll','27',1499138703,3.40,1,'ubuntu16-02\n'),('poll','22',1499138703,4.14,1,'ubuntu16\n'),('poll','28',1499138703,4.23,1,'ubuntu16\n'),('pollbill','',1499138701,73.43,0,'ubuntu16-02\n'),('pollbill','',1499138706,73.57,0,'ubuntu16\n'),('poll','22',1499139002,1.29,1,'ubuntu16\n'),('poll','25',1499139002,1.21,1,'ubuntu16-02\n'),('poll','24',1499139002,1.22,1,'ubuntu16-02\n'),('poll','23',1499139002,1.23,1,'ubuntu16-02\n'),('poll','28',1499139002,1.65,1,'ubuntu16\n'),('poll','27',1499139002,1.85,1,'ubuntu16-02\n'),('pollbill','',1499139002,73.08,0,'ubuntu16\n'),('pollbill','',1499139002,73.35,0,'ubuntu16-02\n'),('poll','22',1499139302,1.01,1,'ubuntu16\n'),('poll','28',1499139302,1.47,1,'ubuntu16\n'),('poll','24',1499139302,1.14,1,'ubuntu16-02\n'),('poll','23',1499139302,1.15,1,'ubuntu16-02\n'),('poll','25',1499139302,1.14,1,'ubuntu16-02\n'),('poll','27',1499139302,1.46,1,'ubuntu16-02\n'),('pollbill','',1499139301,72.97,0,'ubuntu16\n'),('pollbill','',1499139302,72.92,0,'ubuntu16-02\n'),('poll','22',1499139602,1.01,1,'ubuntu16\n'),('poll','25',1499139602,1.18,1,'ubuntu16-02\n'),('poll','24',1499139602,1.20,1,'ubuntu16-02\n'),('poll','23',1499139602,1.20,1,'ubuntu16-02\n'),('poll','28',1499139602,1.49,1,'ubuntu16\n'),('poll','27',1499139602,1.56,1,'ubuntu16-02\n'),('pollbill','',1499139602,72.94,0,'ubuntu16-02\n'),('pollbill','',1499139602,73.11,0,'ubuntu16\n'),('poll','22',1499139902,1.07,1,'ubuntu16\n'),('poll','23',1499139903,1.15,1,'ubuntu16-02\n'),('poll','24',1499139903,1.18,1,'ubuntu16-02\n'),('poll','25',1499139903,1.30,1,'ubuntu16-02\n'),('poll','28',1499139902,2.74,1,'ubuntu16\n'),('poll','27',1499139903,1.96,1,'ubuntu16-02\n'),('pollbill','',1499139901,73.22,0,'ubuntu16\n'),('pollbill','',1499139903,73.19,0,'ubuntu16-02\n'),('poll','22',1499140203,1.03,1,'ubuntu16\n'),('poll','23',1499140203,1.16,1,'ubuntu16-02\n'),('poll','24',1499140203,1.15,1,'ubuntu16-02\n'),('poll','25',1499140203,1.17,1,'ubuntu16-02\n'),('poll','28',1499140203,1.60,1,'ubuntu16\n'),('poll','27',1499140203,1.77,1,'ubuntu16-02\n'),('pollbill','',1499140202,73.17,0,'ubuntu16\n'),('pollbill','',1499140202,73.40,0,'ubuntu16-02\n'),('poll','22',1499140502,1.06,1,'ubuntu16\n'),('poll','24',1499140502,1.10,1,'ubuntu16-02\n'),('poll','25',1499140502,1.19,1,'ubuntu16-02\n'),('poll','23',1499140502,1.20,1,'ubuntu16-02\n'),('poll','28',1499140502,1.53,1,'ubuntu16\n'),('poll','27',1499140502,1.61,1,'ubuntu16-02\n'),('pollbill','',1499140502,72.99,0,'ubuntu16-02\n'),('pollbill','',1499140502,73.30,0,'ubuntu16\n'),('poll','22',1499140802,1.02,1,'ubuntu16\n'),('poll','24',1499140803,1.16,1,'ubuntu16-02\n'),('poll','28',1499140802,1.85,1,'ubuntu16\n'),('poll','25',1499140803,1.20,1,'ubuntu16-02\n'),('poll','23',1499140803,1.19,1,'ubuntu16-02\n'),('poll','27',1499140803,1.73,1,'ubuntu16-02\n'),('pollbill','',1499140802,72.97,0,'ubuntu16\n'),('pollbill','',1499140802,73.09,0,'ubuntu16-02\n'),('poll','22',1499141102,1.07,1,'ubuntu16\n'),('poll','28',1499141102,1.50,1,'ubuntu16\n'),('poll','24',1499141102,1.18,1,'ubuntu16-02\n'),('poll','23',1499141102,1.21,1,'ubuntu16-02\n'),('poll','25',1499141102,1.19,1,'ubuntu16-02\n'),('poll','27',1499141102,1.54,1,'ubuntu16-02\n'),('pollbill','',1499141101,72.98,0,'ubuntu16-02\n'),('pollbill','',1499141101,73.06,0,'ubuntu16\n'),('poll','25',1499141404,1.52,1,'ubuntu16-02\n'),('poll','23',1499141404,1.70,1,'ubuntu16-02\n'),('poll','24',1499141404,1.52,1,'ubuntu16-02\n'),('poll','22',1499141404,2.21,1,'ubuntu16\n'),('poll','27',1499141405,1.77,1,'ubuntu16-02\n'),('poll','28',1499141405,1.65,1,'ubuntu16\n'),('pollbill','',1499141402,76.05,0,'ubuntu16-02\n'),('pollbill','',1499141402,76.86,0,'ubuntu16\n'),('poll','24',1499141702,1.35,1,'ubuntu16-02\n'),('poll','25',1499141702,1.48,1,'ubuntu16-02\n'),('poll','23',1499141702,1.55,1,'ubuntu16-02\n'),('poll','22',1499141702,1.10,1,'ubuntu16\n'),('poll','28',1499141702,1.81,1,'ubuntu16\n'),('poll','27',1499141702,2.05,1,'ubuntu16-02\n'),('pollbill','',1499141702,73.08,0,'ubuntu16\n'),('pollbill','',1499141701,73.79,0,'ubuntu16-02\n'),('poll','22',1499142002,1.14,1,'ubuntu16\n'),('poll','25',1499142002,1.20,1,'ubuntu16-02\n'),('poll','23',1499142002,1.25,1,'ubuntu16-02\n'),('poll','24',1499142002,1.24,1,'ubuntu16-02\n'),('poll','28',1499142002,1.68,1,'ubuntu16\n'),('poll','27',1499142002,1.71,1,'ubuntu16-02\n'),('pollbill','',1499142001,73.15,0,'ubuntu16-02\n'),('pollbill','',1499142002,73.08,0,'ubuntu16\n'),('poll','24',1499142302,1.23,1,'ubuntu16-02\n'),('poll','23',1499142302,1.28,1,'ubuntu16-02\n'),('poll','22',1499142303,1.21,1,'ubuntu16\n'),('poll','25',1499142302,1.48,1,'ubuntu16-02\n'),('poll','27',1499142302,1.81,1,'ubuntu16-02\n'),('poll','28',1499142303,1.76,1,'ubuntu16\n'),('pollbill','',1499142302,73.35,0,'ubuntu16-02\n'),('pollbill','',1499142302,73.12,0,'ubuntu16\n'),('poll','24',1499142602,1.21,1,'ubuntu16-02\n'),('poll','25',1499142602,1.21,1,'ubuntu16-02\n'),('poll','23',1499142602,1.25,1,'ubuntu16-02\n'),('poll','22',1499142602,1.02,1,'ubuntu16\n'),('poll','27',1499142602,1.91,1,'ubuntu16-02\n'),('poll','28',1499142602,2.04,1,'ubuntu16\n'),('pollbill','',1499142601,72.99,0,'ubuntu16-02\n'),('pollbill','',1499142602,72.87,0,'ubuntu16\n'),('poll','25',1499142903,1.65,1,'ubuntu16-02\n'),('poll','24',1499142903,1.84,1,'ubuntu16-02\n'),('poll','23',1499142903,2.14,1,'ubuntu16-02\n'),('poll','27',1499142903,2.48,1,'ubuntu16-02\n'),('poll','22',1499142905,1.06,1,'ubuntu16\n'),('poll','28',1499142905,1.42,1,'ubuntu16\n'),('pollbill','',1499142902,73.86,0,'ubuntu16-02\n'),('pollbill','',1499142904,73.83,0,'ubuntu16\n'),('poll','22',1499143202,1.01,1,'ubuntu16\n'),('poll','28',1499143202,1.41,1,'ubuntu16\n'),('poll','23',1499143202,1.16,1,'ubuntu16-02\n'),('poll','25',1499143202,1.15,1,'ubuntu16-02\n'),('poll','24',1499143202,1.15,1,'ubuntu16-02\n'),('poll','27',1499143202,1.78,1,'ubuntu16-02\n'),('pollbill','',1499143202,72.90,0,'ubuntu16\n'),('pollbill','',1499143202,73.09,0,'ubuntu16-02\n'),('poll','22',1499143502,1.01,1,'ubuntu16\n'),('poll','24',1499143503,1.17,1,'ubuntu16-02\n'),('poll','25',1499143503,1.21,1,'ubuntu16-02\n'),('poll','23',1499143503,1.21,1,'ubuntu16-02\n'),('poll','28',1499143502,3.94,1,'ubuntu16\n'),('poll','27',1499143503,4.03,1,'ubuntu16-02\n'),('pollbill','',1499143502,73.08,0,'ubuntu16\n'),('pollbill','',1499143502,73.09,0,'ubuntu16-02\n'),('poll','25',1499143803,1.15,1,'ubuntu16-02\n'),('poll','24',1499143803,1.16,1,'ubuntu16-02\n'),('poll','23',1499143803,1.24,1,'ubuntu16-02\n'),('poll','22',1499143803,1.04,1,'ubuntu16\n'),('poll','27',1499143803,1.83,1,'ubuntu16-02\n'),('poll','28',1499143803,1.67,1,'ubuntu16\n'),('pollbill','',1499143802,73.27,0,'ubuntu16-02\n'),('pollbill','',1499143803,73.03,0,'ubuntu16\n'),('poll','22',1499144102,1.04,1,'ubuntu16\n'),('poll','24',1499144102,1.18,1,'ubuntu16-02\n'),('poll','25',1499144102,1.36,1,'ubuntu16-02\n'),('poll','23',1499144102,1.36,1,'ubuntu16-02\n'),('poll','28',1499144102,2.04,1,'ubuntu16\n'),('poll','27',1499144102,1.77,1,'ubuntu16-02\n'),('pollbill','',1499144101,72.99,0,'ubuntu16\n'),('pollbill','',1499144102,73.21,0,'ubuntu16-02\n'),('poll','23',1499144402,1.11,1,'ubuntu16-02\n'),('poll','24',1499144402,1.18,1,'ubuntu16-02\n'),('poll','25',1499144402,1.28,1,'ubuntu16-02\n'),('poll','22',1499144402,0.99,1,'ubuntu16\n'),('poll','27',1499144402,1.76,1,'ubuntu16-02\n'),('poll','28',1499144402,1.47,1,'ubuntu16\n'),('pollbill','',1499144401,73.14,0,'ubuntu16-02\n'),('pollbill','',1499144402,72.95,0,'ubuntu16\n'),('poll','22',1499144702,1.00,1,'ubuntu16\n'),('poll','28',1499144702,1.48,1,'ubuntu16\n'),('poll','25',1499144702,1.19,1,'ubuntu16-02\n'),('poll','24',1499144702,1.22,1,'ubuntu16-02\n'),('poll','23',1499144702,1.23,1,'ubuntu16-02\n'),('poll','27',1499144702,2.02,1,'ubuntu16-02\n'),('pollbill','',1499144702,72.98,0,'ubuntu16\n'),('pollbill','',1499144702,73.16,0,'ubuntu16-02\n'),('poll','23',1499145002,1.13,1,'ubuntu16-02\n'),('poll','24',1499145002,1.15,1,'ubuntu16-02\n'),('poll','25',1499145002,1.15,1,'ubuntu16-02\n'),('poll','22',1499145002,1.02,1,'ubuntu16\n'),('poll','27',1499145002,2.12,1,'ubuntu16-02\n'),('poll','28',1499145002,2.13,1,'ubuntu16\n'),('pollbill','',1499145001,72.90,0,'ubuntu16-02\n'),('pollbill','',1499145002,72.89,0,'ubuntu16\n'),('poll','25',1499145302,1.28,1,'ubuntu16-02\n'),('poll','23',1499145302,1.31,1,'ubuntu16-02\n'),('poll','24',1499145302,1.24,1,'ubuntu16-02\n'),('poll','22',1499145302,1.09,1,'ubuntu16\n'),('poll','27',1499145302,2.41,1,'ubuntu16-02\n'),('poll','28',1499145302,2.93,1,'ubuntu16\n'),('pollbill','',1499145302,73.35,0,'ubuntu16-02\n'),('pollbill','',1499145302,72.80,0,'ubuntu16\n'),('poll','25',1499145602,1.61,1,'ubuntu16-02\n'),('poll','24',1499145602,1.81,1,'ubuntu16-02\n'),('poll','23',1499145602,1.60,1,'ubuntu16-02\n'),('poll','22',1499145602,1.90,1,'ubuntu16\n'),('poll','27',1499145602,2.69,1,'ubuntu16-02\n'),('poll','28',1499145602,3.91,1,'ubuntu16\n'),('pollbill','',1499145601,75.06,0,'ubuntu16-02\n'),('pollbill','',1499145602,74.22,0,'ubuntu16\n'),('poll','23',1499145903,3.70,1,'ubuntu16-02\n'),('poll','25',1499145902,4.09,1,'ubuntu16-02\n'),('poll','24',1499145902,4.50,1,'ubuntu16-02\n'),('poll','27',1499145903,10.71,1,'ubuntu16-02\n'),('poll','22',1499145914,1.07,1,'ubuntu16\n'),('poll','28',1499145914,1.30,1,'ubuntu16\n'),('pollbill','',1499145901,73.95,0,'ubuntu16-02\n'),('pollbill','',1499145910,76.12,0,'ubuntu16\n'),('poll','22',1499146202,1.03,1,'ubuntu16\n'),('poll','28',1499146202,1.40,1,'ubuntu16\n'),('poll','23',1499146202,1.16,1,'ubuntu16-02\n'),('poll','25',1499146202,1.15,1,'ubuntu16-02\n'),('poll','24',1499146202,1.15,1,'ubuntu16-02\n'),('poll','27',1499146202,1.48,1,'ubuntu16-02\n'),('pollbill','',1499146202,73.05,0,'ubuntu16\n'),('pollbill','',1499146202,73.02,0,'ubuntu16-02\n'),('poll','24',1499146503,6.54,1,'ubuntu16-02\n'),('poll','25',1499146503,6.85,1,'ubuntu16-02\n'),('poll','23',1499146503,6.95,1,'ubuntu16-02\n'),('poll','27',1499146504,9.71,1,'ubuntu16-02\n'),('poll','22',1499146513,1.76,1,'ubuntu16\n'),('poll','28',1499146513,1.86,1,'ubuntu16\n'),('pollbill','',1499146502,73.74,0,'ubuntu16-02\n'),('pollbill','',1499146503,78.59,0,'ubuntu16\n'),('poll','23',1499146803,1.07,1,'ubuntu16-02\n'),('poll','25',1499146803,1.30,1,'ubuntu16-02\n'),('poll','22',1499146803,1.73,1,'ubuntu16\n'),('poll','24',1499146803,1.35,1,'ubuntu16-02\n'),('poll','28',1499146803,2.19,1,'ubuntu16\n'),('poll','27',1499146803,1.90,1,'ubuntu16-02\n'),('pollbill','',1499146802,73.77,0,'ubuntu16\n'),('pollbill','',1499146802,73.89,0,'ubuntu16-02\n'),('poll','24',1499147102,1.71,1,'ubuntu16-02\n'),('poll','23',1499147102,1.71,1,'ubuntu16-02\n'),('poll','25',1499147102,1.71,1,'ubuntu16-02\n'),('poll','22',1499147102,1.18,1,'ubuntu16\n'),('poll','27',1499147102,1.90,1,'ubuntu16-02\n'),('poll','28',1499147102,4.31,1,'ubuntu16\n'),('pollbill','',1499147101,73.17,0,'ubuntu16-02\n'),('pollbill','',1499147102,73.28,0,'ubuntu16\n'),('poll','22',1499147402,1.12,1,'ubuntu16\n'),('poll','24',1499147402,1.58,1,'ubuntu16-02\n'),('poll','25',1499147402,1.54,1,'ubuntu16-02\n'),('poll','23',1499147402,1.55,1,'ubuntu16-02\n'),('poll','27',1499147402,2.41,1,'ubuntu16-02\n'),('poll','28',1499147402,2.99,1,'ubuntu16\n'),('pollbill','',1499147402,73.12,0,'ubuntu16\n'),('pollbill','',1499147402,74.22,0,'ubuntu16-02\n'),('poll','22',1499147702,0.99,1,'ubuntu16\n'),('poll','28',1499147702,1.66,1,'ubuntu16\n'),('poll','23',1499147702,1.15,1,'ubuntu16-02\n'),('poll','25',1499147702,1.15,1,'ubuntu16-02\n'),('poll','24',1499147702,1.16,1,'ubuntu16-02\n'),('poll','27',1499147702,1.50,1,'ubuntu16-02\n'),('pollbill','',1499147701,72.98,0,'ubuntu16\n'),('pollbill','',1499147702,73.41,0,'ubuntu16-02\n'),('poll','23',1499148002,1.32,1,'ubuntu16-02\n'),('poll','25',1499148002,1.30,1,'ubuntu16-02\n'),('poll','24',1499148002,1.27,1,'ubuntu16-02\n'),('poll','28',1499148002,1.60,1,'ubuntu16\n'),('poll','22',1499148002,1.60,1,'ubuntu16\n'),('poll','27',1499148002,1.84,1,'ubuntu16-02\n'),('pollbill','',1499148002,73.75,0,'ubuntu16-02\n'),('pollbill','',1499148002,73.81,0,'ubuntu16\n'),('poll','23',1499148302,7.01,1,'ubuntu16-02\n'),('poll','24',1499148302,7.61,1,'ubuntu16-02\n'),('poll','25',1499148302,7.70,1,'ubuntu16-02\n'),('poll','27',1499148302,12.92,1,'ubuntu16-02\n'),('poll','22',1499148315,1.18,1,'ubuntu16\n'),('poll','28',1499148315,1.78,1,'ubuntu16\n'),('pollbill','',1499148302,73.53,0,'ubuntu16-02\n'),('pollbill','',1499148313,74.27,0,'ubuntu16\n'),('poll','22',1499148602,1.12,1,'ubuntu16\n'),('poll','23',1499148602,1.16,1,'ubuntu16-02\n'),('poll','25',1499148602,1.17,1,'ubuntu16-02\n'),('poll','24',1499148602,1.16,1,'ubuntu16-02\n'),('poll','28',1499148602,1.52,1,'ubuntu16\n'),('poll','27',1499148602,1.70,1,'ubuntu16-02\n'),('pollbill','',1499148601,73.13,0,'ubuntu16\n'),('pollbill','',1499148602,74.09,0,'ubuntu16-02\n'),('poll','22',1499148903,1.02,1,'ubuntu16\n'),('poll','24',1499148902,1.23,1,'ubuntu16-02\n'),('poll','25',1499148902,1.22,1,'ubuntu16-02\n'),('poll','23',1499148902,1.23,1,'ubuntu16-02\n'),('poll','27',1499148902,1.69,1,'ubuntu16-02\n'),('poll','28',1499148903,1.61,1,'ubuntu16\n'),('pollbill','',1499148902,72.96,0,'ubuntu16-02\n'),('pollbill','',1499148902,72.98,0,'ubuntu16\n'),('poll','22',1499149202,1.06,1,'ubuntu16\n'),('poll','28',1499149202,1.38,1,'ubuntu16\n'),('poll','24',1499149202,1.15,1,'ubuntu16-02\n'),('poll','25',1499149202,1.17,1,'ubuntu16-02\n'),('poll','23',1499149202,1.18,1,'ubuntu16-02\n'),('poll','27',1499149202,1.48,1,'ubuntu16-02\n'),('pollbill','',1499149202,72.96,0,'ubuntu16\n'),('pollbill','',1499149202,72.92,0,'ubuntu16-02\n'),('poll','22',1499149502,1.04,1,'ubuntu16\n'),('poll','28',1499149502,1.42,1,'ubuntu16\n'),('poll','24',1499149503,1.15,1,'ubuntu16-02\n'),('poll','23',1499149503,1.15,1,'ubuntu16-02\n'),('poll','25',1499149503,1.16,1,'ubuntu16-02\n'),('poll','27',1499149503,1.53,1,'ubuntu16-02\n'),('pollbill','',1499149502,73.06,0,'ubuntu16\n'),('pollbill','',1499149502,72.90,0,'ubuntu16-02\n'),('poll','23',1499149802,1.09,1,'ubuntu16-02\n'),('poll','25',1499149802,1.33,1,'ubuntu16-02\n'),('poll','24',1499149802,1.33,1,'ubuntu16-02\n'),('poll','22',1499149802,1.07,1,'ubuntu16\n'),('poll','27',1499149802,1.73,1,'ubuntu16-02\n'),('poll','28',1499149802,1.46,1,'ubuntu16\n'),('pollbill','',1499149801,73.00,0,'ubuntu16-02\n'),('pollbill','',1499149802,72.97,0,'ubuntu16\n'),('poll','22',1499150102,1.02,1,'ubuntu16\n'),('poll','23',1499150102,1.20,1,'ubuntu16-02\n'),('poll','24',1499150102,1.20,1,'ubuntu16-02\n'),('poll','25',1499150102,1.21,1,'ubuntu16-02\n'),('poll','28',1499150102,1.50,1,'ubuntu16\n'),('poll','27',1499150102,1.63,1,'ubuntu16-02\n'),('pollbill','',1499150102,72.95,0,'ubuntu16\n'),('pollbill','',1499150102,73.20,0,'ubuntu16-02\n'),('poll','23',1499150402,1.14,1,'ubuntu16-02\n'),('poll','25',1499150402,1.13,1,'ubuntu16-02\n'),('poll','24',1499150402,1.16,1,'ubuntu16-02\n'),('poll','27',1499150402,1.60,1,'ubuntu16-02\n'),('poll','22',1499150402,1.00,1,'ubuntu16\n'),('poll','28',1499150402,1.32,1,'ubuntu16\n'),('pollbill','',1499150401,72.95,0,'ubuntu16-02\n'),('pollbill','',1499150402,72.78,0,'ubuntu16\n'),('poll','22',1499150702,1.03,1,'ubuntu16\n'),('poll','24',1499150702,1.15,1,'ubuntu16-02\n'),('poll','25',1499150702,1.15,1,'ubuntu16-02\n'),('poll','28',1499150702,1.41,1,'ubuntu16\n'),('poll','23',1499150702,1.15,1,'ubuntu16-02\n'),('poll','27',1499150702,1.52,1,'ubuntu16-02\n'),('pollbill','',1499150701,73.32,0,'ubuntu16-02\n'),('pollbill','',1499150702,73.06,0,'ubuntu16\n'),('poll','22',1499151002,1.00,1,'ubuntu16\n'),('poll','28',1499151002,1.31,1,'ubuntu16\n'),('poll','23',1499151002,1.18,1,'ubuntu16-02\n'),('poll','25',1499151002,1.17,1,'ubuntu16-02\n'),('poll','24',1499151002,1.19,1,'ubuntu16-02\n'),('poll','27',1499151002,1.91,1,'ubuntu16-02\n'),('pollbill','',1499151001,72.95,0,'ubuntu16\n'),('pollbill','',1499151002,73.08,0,'ubuntu16-02\n'),('poll','24',1499151301,1.13,1,'ubuntu16-02\n'),('poll','23',1499151301,1.14,1,'ubuntu16-02\n'),('poll','25',1499151302,1.18,1,'ubuntu16-02\n'),('poll','27',1499151302,1.58,1,'ubuntu16-02\n'),('poll','22',1499151302,1.00,1,'ubuntu16\n'),('poll','28',1499151302,1.32,1,'ubuntu16\n'),('pollbill','',1499151301,72.91,0,'ubuntu16-02\n'),('pollbill','',1499151302,72.98,0,'ubuntu16\n'),('poll','22',1499151602,1.02,1,'ubuntu16\n'),('poll','23',1499151602,1.17,1,'ubuntu16-02\n'),('poll','25',1499151602,1.16,1,'ubuntu16-02\n'),('poll','24',1499151602,1.17,1,'ubuntu16-02\n'),('poll','28',1499151602,1.77,1,'ubuntu16\n'),('poll','27',1499151602,1.58,1,'ubuntu16-02\n'),('pollbill','',1499151601,73.09,0,'ubuntu16\n'),('pollbill','',1499151601,73.04,0,'ubuntu16-02\n'),('poll','25',1499151902,1.24,1,'ubuntu16-02\n'),('poll','24',1499151902,1.24,1,'ubuntu16-02\n'),('poll','23',1499151902,1.30,1,'ubuntu16-02\n'),('poll','22',1499151903,1.01,1,'ubuntu16\n'),('poll','27',1499151902,1.64,1,'ubuntu16-02\n'),('poll','28',1499151903,1.53,1,'ubuntu16\n'),('pollbill','',1499151902,72.87,0,'ubuntu16-02\n'),('pollbill','',1499151902,73.17,0,'ubuntu16\n'),('poll','22',1499152203,1.02,1,'ubuntu16\n'),('poll','23',1499152203,1.29,1,'ubuntu16-02\n'),('poll','25',1499152203,1.11,1,'ubuntu16-02\n'),('poll','24',1499152203,1.17,1,'ubuntu16-02\n'),('poll','28',1499152203,2.00,1,'ubuntu16\n'),('poll','27',1499152203,2.48,1,'ubuntu16-02\n'),('pollbill','',1499152202,73.42,0,'ubuntu16-02\n'),('pollbill','',1499152203,72.86,0,'ubuntu16\n'),('poll','22',1499152502,1.42,1,'ubuntu16\n'),('poll','25',1499152503,1.51,1,'ubuntu16-02\n'),('poll','24',1499152503,1.50,1,'ubuntu16-02\n'),('poll','23',1499152503,1.59,1,'ubuntu16-02\n'),('poll','27',1499152503,2.86,1,'ubuntu16-02\n'),('poll','28',1499152502,3.64,1,'ubuntu16\n'),('pollbill','',1499152501,73.16,0,'ubuntu16\n'),('pollbill','',1499152502,73.55,0,'ubuntu16-02\n'),('poll','22',1499152802,1.21,1,'ubuntu16\n'),('poll','28',1499152802,1.92,1,'ubuntu16\n'),('poll','25',1499152802,1.33,1,'ubuntu16-02\n'),('poll','23',1499152802,1.32,1,'ubuntu16-02\n'),('poll','24',1499152802,1.32,1,'ubuntu16-02\n'),('poll','27',1499152802,2.16,1,'ubuntu16-02\n'),('pollbill','',1499152801,73.11,0,'ubuntu16\n'),('pollbill','',1499152802,73.14,0,'ubuntu16-02\n'),('poll','22',1499153102,1.15,1,'ubuntu16\n'),('poll','24',1499153103,1.31,1,'ubuntu16-02\n'),('poll','23',1499153103,1.31,1,'ubuntu16-02\n'),('poll','28',1499153102,1.94,1,'ubuntu16\n'),('poll','25',1499153103,1.39,1,'ubuntu16-02\n'),('poll','27',1499153103,2.13,1,'ubuntu16-02\n'),('pollbill','',1499153102,73.00,0,'ubuntu16\n'),('pollbill','',1499153102,73.13,0,'ubuntu16-02\n'),('poll','22',1499153402,1.07,1,'ubuntu16\n'),('poll','23',1499153402,1.36,1,'ubuntu16-02\n'),('poll','24',1499153402,1.37,1,'ubuntu16-02\n'),('poll','25',1499153402,1.33,1,'ubuntu16-02\n'),('poll','28',1499153402,2.02,1,'ubuntu16\n'),('poll','27',1499153402,2.11,1,'ubuntu16-02\n'),('pollbill','',1499153401,73.18,0,'ubuntu16-02\n'),('pollbill','',1499153402,73.05,0,'ubuntu16\n'),('poll','22',1499153702,1.03,1,'ubuntu16\n'),('poll','23',1499153702,1.28,1,'ubuntu16-02\n'),('poll','24',1499153703,1.32,1,'ubuntu16-02\n'),('poll','25',1499153703,1.33,1,'ubuntu16-02\n'),('poll','28',1499153702,1.80,1,'ubuntu16\n'),('poll','27',1499153703,2.09,1,'ubuntu16-02\n'),('pollbill','',1499153702,73.13,0,'ubuntu16\n'),('pollbill','',1499153702,73.66,0,'ubuntu16-02\n'),('poll','22',1499154002,1.01,1,'ubuntu16\n'),('poll','25',1499154003,1.34,1,'ubuntu16-02\n'),('poll','24',1499154003,1.36,1,'ubuntu16-02\n'),('poll','23',1499154003,1.36,1,'ubuntu16-02\n'),('poll','28',1499154002,1.73,1,'ubuntu16\n'),('poll','27',1499154003,2.12,1,'ubuntu16-02\n'),('pollbill','',1499154002,73.04,0,'ubuntu16\n'),('pollbill','',1499154002,73.13,0,'ubuntu16-02\n'),('poll','22',1499154302,1.02,1,'ubuntu16\n'),('poll','23',1499154302,1.33,1,'ubuntu16-02\n'),('poll','24',1499154302,1.34,1,'ubuntu16-02\n'),('poll','25',1499154302,1.32,1,'ubuntu16-02\n'),('poll','28',1499154302,2.26,1,'ubuntu16\n'),('poll','27',1499154302,2.58,1,'ubuntu16-02\n'),('pollbill','',1499154301,73.20,0,'ubuntu16-02\n'),('pollbill','',1499154302,72.97,0,'ubuntu16\n'),('poll','22',1499154602,1.09,1,'ubuntu16\n'),('poll','28',1499154602,1.82,1,'ubuntu16\n'),('poll','24',1499154603,1.36,1,'ubuntu16-02\n'),('poll','23',1499154603,1.36,1,'ubuntu16-02\n'),('poll','25',1499154603,1.37,1,'ubuntu16-02\n'),('poll','27',1499154603,3.83,1,'ubuntu16-02\n'),('pollbill','',1499154601,74.14,0,'ubuntu16\n'),('pollbill','',1499154602,73.69,0,'ubuntu16-02\n'),('poll','22',1499154902,1.04,1,'ubuntu16\n'),('poll','24',1499154902,1.37,1,'ubuntu16-02\n'),('poll','25',1499154902,1.38,1,'ubuntu16-02\n'),('poll','23',1499154903,1.32,1,'ubuntu16-02\n'),('poll','28',1499154902,1.99,1,'ubuntu16\n'),('poll','27',1499154902,2.58,1,'ubuntu16-02\n'),('pollbill','',1499154902,73.30,0,'ubuntu16-02\n'),('pollbill','',1499154902,73.33,0,'ubuntu16\n'),('poll','22',1499155203,1.14,1,'ubuntu16\n'),('poll','23',1499155203,1.22,1,'ubuntu16-02\n'),('poll','24',1499155203,1.35,1,'ubuntu16-02\n'),('poll','25',1499155203,1.31,1,'ubuntu16-02\n'),('poll','28',1499155202,2.22,1,'ubuntu16\n'),('poll','27',1499155203,2.40,1,'ubuntu16-02\n'),('pollbill','',1499155202,73.33,0,'ubuntu16\n'),('pollbill','',1499155202,73.68,0,'ubuntu16-02\n'),('poll','24',1499155504,11.53,1,'ubuntu16-02\n'),('poll','23',1499155503,12.70,1,'ubuntu16-02\n'),('poll','25',1499155503,13.32,1,'ubuntu16-02\n'),('poll','22',1499155518,3.67,1,'ubuntu16\n'),('poll','27',1499155503,38.83,1,'ubuntu16-02\n'),('poll','28',1499155504,39.10,1,'ubuntu16\n'),('pollbill','',1499155502,97.52,0,'ubuntu16-02\n'),('pollbill','',1499155502,100.20,0,'ubuntu16\n'),('poll','24',1499155846,4.51,1,'ubuntu16-02\n'),('poll','23',1499155846,4.59,1,'ubuntu16-02\n'),('poll','25',1499155845,5.58,1,'ubuntu16-02\n'),('poll','27',1499155845,10.62,1,'ubuntu16-02\n'),('poll','22',1499155854,2.40,1,'ubuntu16\n'),('poll','28',1499155854,4.70,1,'ubuntu16\n'),('pollbill','',1499155805,117.30,0,'ubuntu16-02\n'),('pollbill','',1499155855,74.47,0,'ubuntu16\n'),('poll','22',1499156102,1.02,1,'ubuntu16\n'),('poll','28',1499156102,1.82,1,'ubuntu16\n'),('poll','23',1499156103,1.30,1,'ubuntu16-02\n'),('poll','25',1499156103,1.31,1,'ubuntu16-02\n'),('poll','24',1499156103,1.32,1,'ubuntu16-02\n'),('poll','27',1499156103,2.26,1,'ubuntu16-02\n'),('pollbill','',1499156101,73.44,0,'ubuntu16\n'),('pollbill','',1499156102,73.39,0,'ubuntu16-02\n'),('poll','22',1499156402,1.09,1,'ubuntu16\n'),('poll','25',1499156403,1.36,1,'ubuntu16-02\n'),('poll','24',1499156403,1.36,1,'ubuntu16-02\n'),('poll','23',1499156403,1.38,1,'ubuntu16-02\n'),('poll','28',1499156402,2.24,1,'ubuntu16\n'),('poll','27',1499156403,2.60,1,'ubuntu16-02\n'),('pollbill','',1499156402,73.36,0,'ubuntu16\n'),('pollbill','',1499156402,74.10,0,'ubuntu16-02\n'),('poll','22',1499156703,1.70,1,'ubuntu16\n'),('poll','25',1499156703,2.07,1,'ubuntu16-02\n'),('poll','24',1499156703,2.30,1,'ubuntu16-02\n'),('poll','23',1499156703,2.39,1,'ubuntu16-02\n'),('poll','27',1499156703,3.42,1,'ubuntu16-02\n'),('poll','28',1499156703,3.95,1,'ubuntu16\n'),('pollbill','',1499156702,75.06,0,'ubuntu16-02\n'),('pollbill','',1499156702,75.72,0,'ubuntu16\n'),('poll','22',1499157003,1.85,1,'ubuntu16\n'),('poll','23',1499157004,1.48,1,'ubuntu16-02\n'),('poll','24',1499157004,1.51,1,'ubuntu16-02\n'),('poll','25',1499157004,1.76,1,'ubuntu16-02\n'),('poll','27',1499157004,3.38,1,'ubuntu16-02\n'),('poll','28',1499157003,4.32,1,'ubuntu16\n'),('pollbill','',1499157002,75.11,0,'ubuntu16-02\n'),('pollbill','',1499157002,75.55,0,'ubuntu16\n'),('poll','22',1499157303,2.12,1,'ubuntu16\n'),('poll','25',1499157303,1.52,1,'ubuntu16-02\n'),('poll','23',1499157303,1.60,1,'ubuntu16-02\n'),('poll','24',1499157303,1.53,1,'ubuntu16-02\n'),('poll','27',1499157303,3.22,1,'ubuntu16-02\n'),('poll','28',1499157302,4.27,1,'ubuntu16\n'),('pollbill','',1499157302,74.49,0,'ubuntu16\n'),('pollbill','',1499157302,74.38,0,'ubuntu16-02\n'),('poll','24',1499157603,1.63,1,'ubuntu16-02\n'),('poll','22',1499157603,1.54,1,'ubuntu16\n'),('poll','25',1499157603,1.66,1,'ubuntu16-02\n'),('poll','23',1499157603,1.70,1,'ubuntu16-02\n'),('poll','27',1499157603,3.54,1,'ubuntu16-02\n'),('poll','28',1499157603,4.02,1,'ubuntu16\n'),('pollbill','',1499157601,74.57,0,'ubuntu16-02\n'),('pollbill','',1499157602,74.42,0,'ubuntu16\n'),('poll','22',1499157902,1.05,1,'ubuntu16\n'),('poll','24',1499157902,1.30,1,'ubuntu16-02\n'),('poll','25',1499157902,1.32,1,'ubuntu16-02\n'),('poll','23',1499157902,1.31,1,'ubuntu16-02\n'),('poll','28',1499157902,2.03,1,'ubuntu16\n'),('poll','27',1499157902,2.18,1,'ubuntu16-02\n'),('pollbill','',1499157902,73.27,0,'ubuntu16\n'),('pollbill','',1499157901,73.60,0,'ubuntu16-02\n'),('poll','22',1499158202,1.08,1,'ubuntu16\n'),('poll','28',1499158202,2.06,1,'ubuntu16\n'),('poll','25',1499158203,1.42,1,'ubuntu16-02\n'),('poll','24',1499158203,1.42,1,'ubuntu16-02\n'),('poll','23',1499158203,1.42,1,'ubuntu16-02\n'),('poll','27',1499158203,2.30,1,'ubuntu16-02\n'),('pollbill','',1499158202,73.21,0,'ubuntu16-02\n'),('pollbill','',1499158202,73.53,0,'ubuntu16\n'),('poll','24',1499158503,5.99,1,'ubuntu16-02\n'),('poll','23',1499158503,5.99,1,'ubuntu16-02\n'),('poll','25',1499158503,5.97,1,'ubuntu16-02\n'),('poll','27',1499158504,14.81,1,'ubuntu16-02\n'),('poll','22',1499158504,15.36,1,'ubuntu16\n'),('poll','28',1499158504,18.90,1,'ubuntu16\n'),('pollbill','',1499158503,83.52,0,'ubuntu16\n'),('pollbill','',1499158503,85.60,0,'ubuntu16-02\n'),('poll','24',1499158802,1.74,1,'ubuntu16-02\n'),('poll','25',1499158802,1.92,1,'ubuntu16-02\n'),('poll','23',1499158802,1.98,1,'ubuntu16-02\n'),('poll','27',1499158802,4.57,1,'ubuntu16-02\n'),('poll','22',1499158816,3.82,1,'ubuntu16\n'),('poll','28',1499158816,5.90,1,'ubuntu16\n'),('pollbill','',1499158801,74.40,0,'ubuntu16-02\n'),('pollbill','',1499158816,77.85,0,'ubuntu16\n'),('poll','22',1499159102,1.62,1,'ubuntu16\n'),('poll','28',1499159102,1.64,1,'ubuntu16\n'),('poll','23',1499159102,1.41,1,'ubuntu16-02\n'),('poll','25',1499159102,1.48,1,'ubuntu16-02\n'),('poll','24',1499159102,1.54,1,'ubuntu16-02\n'),('poll','27',1499159103,1.64,1,'ubuntu16-02\n'),('pollbill','',1499159101,73.31,0,'ubuntu16\n'),('pollbill','',1499159102,72.99,0,'ubuntu16-02\n'),('poll','22',1499159402,1.05,1,'ubuntu16\n'),('poll','28',1499159402,1.69,1,'ubuntu16\n'),('poll','24',1499159403,1.19,1,'ubuntu16-02\n'),('poll','23',1499159403,1.21,1,'ubuntu16-02\n'),('poll','25',1499159403,1.23,1,'ubuntu16-02\n'),('poll','27',1499159403,1.89,1,'ubuntu16-02\n'),('pollbill','',1499159402,73.49,0,'ubuntu16\n'),('pollbill','',1499159402,73.04,0,'ubuntu16-02\n'),('poll','23',1499159702,1.19,1,'ubuntu16-02\n'),('poll','24',1499159702,1.21,1,'ubuntu16-02\n'),('poll','25',1499159702,1.22,1,'ubuntu16-02\n'),('poll','27',1499159702,1.85,1,'ubuntu16-02\n'),('poll','22',1499159703,1.04,1,'ubuntu16\n'),('poll','28',1499159703,1.50,1,'ubuntu16\n'),('pollbill','',1499159702,73.32,0,'ubuntu16-02\n'),('pollbill','',1499159703,73.04,0,'ubuntu16\n'),('poll','22',1499160002,1.06,1,'ubuntu16\n'),('poll','28',1499160002,1.73,1,'ubuntu16\n'),('poll','24',1499160003,1.24,1,'ubuntu16-02\n'),('poll','23',1499160003,1.30,1,'ubuntu16-02\n'),('poll','25',1499160003,1.31,1,'ubuntu16-02\n'),('poll','27',1499160003,1.84,1,'ubuntu16-02\n'),('pollbill','',1499160002,73.17,0,'ubuntu16\n'),('pollbill','',1499160002,73.40,0,'ubuntu16-02\n'),('poll','25',1499160302,1.42,1,'ubuntu16-02\n'),('poll','24',1499160302,1.51,1,'ubuntu16-02\n'),('poll','23',1499160302,1.54,1,'ubuntu16-02\n'),('poll','27',1499160302,2.09,1,'ubuntu16-02\n'),('poll','22',1499160304,1.26,1,'ubuntu16\n'),('poll','28',1499160304,1.41,1,'ubuntu16\n'),('pollbill','',1499160302,73.49,0,'ubuntu16-02\n'),('pollbill','',1499160304,73.21,0,'ubuntu16\n'),('poll','22',1499160603,1.16,1,'ubuntu16\n'),('poll','23',1499160602,1.32,1,'ubuntu16-02\n'),('poll','25',1499160602,1.31,1,'ubuntu16-02\n'),('poll','24',1499160603,1.14,1,'ubuntu16-02\n'),('poll','28',1499160603,1.74,1,'ubuntu16\n'),('poll','27',1499160602,4.23,1,'ubuntu16-02\n'),('pollbill','',1499160603,73.10,0,'ubuntu16\n'),('pollbill','',1499160602,73.83,0,'ubuntu16-02\n'),('poll','22',1499160902,1.13,1,'ubuntu16\n'),('poll','28',1499160902,1.67,1,'ubuntu16\n'),('poll','23',1499160903,1.23,1,'ubuntu16-02\n'),('poll','25',1499160903,1.24,1,'ubuntu16-02\n'),('poll','24',1499160903,1.24,1,'ubuntu16-02\n'),('poll','27',1499160903,1.96,1,'ubuntu16-02\n'),('pollbill','',1499160902,73.51,0,'ubuntu16\n'),('pollbill','',1499160902,73.58,0,'ubuntu16-02\n'),('poll','24',1499161203,1.36,1,'ubuntu16-02\n'),('poll','23',1499161203,1.38,1,'ubuntu16-02\n'),('poll','25',1499161203,1.37,1,'ubuntu16-02\n'),('poll','27',1499161203,1.84,1,'ubuntu16-02\n'),('poll','22',1499161204,1.03,1,'ubuntu16\n'),('poll','28',1499161203,2.00,1,'ubuntu16\n'),('pollbill','',1499161202,73.65,0,'ubuntu16-02\n'),('pollbill','',1499161204,73.25,0,'ubuntu16\n'),('poll','22',1499161502,1.04,1,'ubuntu16\n'),('poll','28',1499161502,1.54,1,'ubuntu16\n'),('poll','23',1499161502,1.25,1,'ubuntu16-02\n'),('poll','24',1499161502,1.71,1,'ubuntu16-02\n'),('poll','25',1499161503,1.76,1,'ubuntu16-02\n'),('poll','27',1499161503,2.21,1,'ubuntu16-02\n'),('pollbill','',1499161501,73.56,0,'ubuntu16\n'),('pollbill','',1499161502,73.15,0,'ubuntu16-02\n'),('poll','22',1499161802,1.14,1,'ubuntu16\n'),('poll','24',1499161802,1.22,1,'ubuntu16-02\n'),('poll','25',1499161802,1.21,1,'ubuntu16-02\n'),('poll','23',1499161802,1.22,1,'ubuntu16-02\n'),('poll','28',1499161802,1.60,1,'ubuntu16\n'),('poll','27',1499161802,1.63,1,'ubuntu16-02\n'),('pollbill','',1499161802,73.33,0,'ubuntu16\n'),('pollbill','',1499161802,73.31,0,'ubuntu16-02\n'),('poll','22',1499162103,1.05,1,'ubuntu16\n'),('poll','25',1499162103,1.29,1,'ubuntu16-02\n'),('poll','24',1499162103,1.19,1,'ubuntu16-02\n'),('poll','23',1499162103,1.07,1,'ubuntu16-02\n'),('poll','27',1499162103,1.67,1,'ubuntu16-02\n'),('poll','28',1499162103,1.88,1,'ubuntu16\n'),('pollbill','',1499162102,73.44,0,'ubuntu16\n'),('pollbill','',1499162102,73.63,0,'ubuntu16-02\n'),('poll','22',1499162402,1.08,1,'ubuntu16\n'),('poll','28',1499162402,1.91,1,'ubuntu16\n'),('poll','24',1499162402,1.25,1,'ubuntu16-02\n'),('poll','23',1499162402,1.28,1,'ubuntu16-02\n'),('poll','25',1499162402,1.31,1,'ubuntu16-02\n'),('poll','27',1499162402,1.85,1,'ubuntu16-02\n'),('pollbill','',1499162401,73.09,0,'ubuntu16\n'),('pollbill','',1499162402,73.09,0,'ubuntu16-02\n'),('poll','25',1499162702,1.15,1,'ubuntu16-02\n'),('poll','24',1499162702,1.24,1,'ubuntu16-02\n'),('poll','23',1499162702,1.25,1,'ubuntu16-02\n'),('poll','27',1499162702,1.94,1,'ubuntu16-02\n'),('poll','22',1499162703,1.12,1,'ubuntu16\n'),('poll','28',1499162703,2.68,1,'ubuntu16\n'),('pollbill','',1499162701,73.16,0,'ubuntu16-02\n'),('pollbill','',1499162703,73.20,0,'ubuntu16\n'),('poll','24',1499163002,1.16,1,'ubuntu16-02\n'),('poll','25',1499163002,1.32,1,'ubuntu16-02\n'),('poll','23',1499163002,1.31,1,'ubuntu16-02\n'),('poll','22',1499163003,1.00,1,'ubuntu16\n'),('poll','27',1499163002,1.84,1,'ubuntu16-02\n'),('poll','28',1499163003,1.39,1,'ubuntu16\n'),('pollbill','',1499163002,73.45,0,'ubuntu16-02\n'),('pollbill','',1499163003,73.18,0,'ubuntu16\n'),('poll','22',1499163302,1.04,1,'ubuntu16\n'),('poll','24',1499163302,1.15,1,'ubuntu16-02\n'),('poll','25',1499163302,1.33,1,'ubuntu16-02\n'),('poll','23',1499163302,1.33,1,'ubuntu16-02\n'),('poll','28',1499163302,1.63,1,'ubuntu16\n'),('poll','27',1499163302,1.68,1,'ubuntu16-02\n'),('pollbill','',1499163302,84.95,0,'ubuntu16-02\n'),('pollbill','',1499163302,88.90,0,'ubuntu16\n'),('poll','22',1499174104,1.00,1,'ubuntu16\n'),('poll','25',1499174102,1.21,1,'ubuntu16-02\n'),('poll','23',1499174102,1.22,1,'ubuntu16-02\n'),('poll','24',1499174102,1.22,1,'ubuntu16-02\n'),('poll','28',1499174104,3.95,1,'ubuntu16\n'),('poll','27',1499174102,4.21,1,'ubuntu16-02\n'),('pollbill','',1499174104,73.60,0,'ubuntu16\n'),('pollbill','',1499174102,73.43,0,'ubuntu16-02\n'),('poll','22',1499174402,1.07,1,'ubuntu16\n'),('poll','28',1499174402,3.88,1,'ubuntu16\n'),('poll','25',1499174402,1.23,1,'ubuntu16-02\n'),('poll','23',1499174402,1.38,1,'ubuntu16-02\n'),('poll','24',1499174402,1.38,1,'ubuntu16-02\n'),('poll','27',1499174402,4.08,1,'ubuntu16-02\n'),('pollbill','',1499174402,73.68,0,'ubuntu16\n'),('pollbill','',1499174401,73.65,0,'ubuntu16-02\n'),('poll','22',1499174701,4.73,1,'ubuntu16\n'),('poll','23',1499174704,1.42,1,'ubuntu16-02\n'),('poll','25',1499174704,1.71,1,'ubuntu16-02\n'),('poll','24',1499174704,1.49,1,'ubuntu16-02\n'),('poll','28',1499174701,8.57,1,'ubuntu16\n'),('poll','27',1499174704,4.66,1,'ubuntu16-02\n'),('pollbill','',1499174701,77.15,0,'ubuntu16\n'),('pollbill','',1499174702,74.92,0,'ubuntu16-02\n'),('poll','25',1499175004,2.18,1,'ubuntu16-02\n'),('poll','23',1499175004,2.43,1,'ubuntu16-02\n'),('poll','24',1499175003,2.93,1,'ubuntu16-02\n'),('poll','22',1499175004,4.26,1,'ubuntu16\n'),('poll','27',1499175004,5.57,1,'ubuntu16-02\n'),('poll','28',1499175004,7.08,1,'ubuntu16\n'),('pollbill','',1499175002,75.16,0,'ubuntu16-02\n'),('pollbill','',1499175007,73.42,0,'ubuntu16\n'),('poll','22',1499175303,1.04,1,'ubuntu16\n'),('poll','25',1499175303,1.17,1,'ubuntu16-02\n'),('poll','24',1499175303,1.19,1,'ubuntu16-02\n'),('poll','23',1499175303,1.19,1,'ubuntu16-02\n'),('poll','28',1499175303,3.93,1,'ubuntu16\n'),('poll','27',1499175303,4.03,1,'ubuntu16-02\n'),('pollbill','',1499175302,73.82,0,'ubuntu16\n'),('pollbill','',1499175302,74.07,0,'ubuntu16-02\n'),('poll','22',1499175602,1.08,1,'ubuntu16\n'),('poll','23',1499175602,1.19,1,'ubuntu16-02\n'),('poll','24',1499175602,1.23,1,'ubuntu16-02\n'),('poll','25',1499175602,1.30,1,'ubuntu16-02\n'),('poll','28',1499175602,4.00,1,'ubuntu16\n'),('poll','27',1499175602,4.20,1,'ubuntu16-02\n'),('pollbill','',1499175601,74.12,0,'ubuntu16-02\n'),('pollbill','',1499175602,73.46,0,'ubuntu16\n'),('poll','22',1499175902,1.11,1,'ubuntu16\n'),('poll','24',1499175902,1.24,1,'ubuntu16-02\n'),('poll','25',1499175902,1.24,1,'ubuntu16-02\n'),('poll','23',1499175902,1.28,1,'ubuntu16-02\n'),('poll','28',1499175902,3.92,1,'ubuntu16\n'),('poll','27',1499175902,4.01,1,'ubuntu16-02\n'),('pollbill','',1499175901,73.71,0,'ubuntu16\n'),('pollbill','',1499175902,73.57,0,'ubuntu16-02\n'),('poll','22',1499176202,1.05,1,'ubuntu16\n'),('poll','25',1499176203,1.17,1,'ubuntu16-02\n'),('poll','24',1499176203,1.18,1,'ubuntu16-02\n'),('poll','23',1499176203,1.25,1,'ubuntu16-02\n'),('poll','28',1499176202,4.58,1,'ubuntu16\n'),('poll','27',1499176203,4.40,1,'ubuntu16-02\n'),('pollbill','',1499176202,73.56,0,'ubuntu16\n'),('pollbill','',1499176202,73.50,0,'ubuntu16-02\n'),('poll','22',1499176502,1.12,1,'ubuntu16\n'),('poll','23',1499176502,1.28,1,'ubuntu16-02\n'),('poll','25',1499176502,1.29,1,'ubuntu16-02\n'),('poll','24',1499176502,1.28,1,'ubuntu16-02\n'),('poll','28',1499176502,4.00,1,'ubuntu16\n'),('poll','27',1499176502,4.16,1,'ubuntu16-02\n'),('pollbill','',1499176502,73.36,0,'ubuntu16-02\n'),('pollbill','',1499176502,73.43,0,'ubuntu16\n'),('poll','23',1499176802,1.32,1,'ubuntu16-02\n'),('poll','25',1499176802,1.32,1,'ubuntu16-02\n'),('poll','24',1499176802,1.32,1,'ubuntu16-02\n'),('poll','22',1499176803,1.07,1,'ubuntu16\n'),('poll','27',1499176802,4.35,1,'ubuntu16-02\n'),('poll','28',1499176803,3.84,1,'ubuntu16\n'),('pollbill','',1499176801,73.51,0,'ubuntu16-02\n'),('pollbill','',1499176802,73.65,0,'ubuntu16\n'),('poll','22',1499177102,1.09,1,'ubuntu16\n'),('poll','24',1499177102,1.22,1,'ubuntu16-02\n'),('poll','25',1499177102,1.22,1,'ubuntu16-02\n'),('poll','23',1499177102,1.34,1,'ubuntu16-02\n'),('poll','28',1499177102,3.79,1,'ubuntu16\n'),('poll','27',1499177102,4.14,1,'ubuntu16-02\n'),('pollbill','',1499177102,73.17,0,'ubuntu16\n'),('pollbill','',1499177102,73.12,0,'ubuntu16-02\n'),('poll','22',1499177402,1.03,1,'ubuntu16\n'),('poll','25',1499177402,1.22,1,'ubuntu16-02\n'),('poll','24',1499177402,1.27,1,'ubuntu16-02\n'),('poll','23',1499177402,1.24,1,'ubuntu16-02\n'),('poll','28',1499177402,4.10,1,'ubuntu16\n'),('poll','27',1499177402,4.22,1,'ubuntu16-02\n'),('pollbill','',1499177402,73.26,0,'ubuntu16-02\n'),('pollbill','',1499177402,72.93,0,'ubuntu16\n'),('poll','25',1499177720,11.92,1,'ubuntu16-02\n'),('poll','23',1499177723,15.38,1,'ubuntu16-02\n'),('poll','24',1499177720,18.41,1,'ubuntu16-02\n'),('poll','22',1499177733,11.51,1,'ubuntu16\n'),('poll','27',1499177720,23.88,1,'ubuntu16-02\n'),('poll','28',1499177733,13.87,1,'ubuntu16\n'),('pollbill','',1499177707,103.70,0,'ubuntu16-02\n'),('pollbill','',1499177739,77.68,0,'ubuntu16\n'),('poll','22',1499178002,1.03,1,'ubuntu16\n'),('poll','28',1499178002,3.79,1,'ubuntu16\n'),('pollbill','',1499178001,73.08,0,'ubuntu16-02\n'),('pollbill','',1499178002,73.10,0,'ubuntu16\n'),('poll','25',1499178303,1.27,1,'ubuntu16-02\n'),('poll','24',1499178303,1.28,1,'ubuntu16-02\n'),('poll','23',1499178303,1.34,1,'ubuntu16-02\n'),('poll','22',1499178303,1.04,1,'ubuntu16\n'),('poll','28',1499178303,4.02,1,'ubuntu16\n'),('poll','27',1499178303,4.28,1,'ubuntu16-02\n'),('pollbill','',1499178303,73.11,0,'ubuntu16\n'),('pollbill','',1499178302,74.24,0,'ubuntu16-02\n'),('poll','22',1499178602,1.00,1,'ubuntu16\n'),('poll','24',1499178602,1.19,1,'ubuntu16-02\n'),('poll','23',1499178602,1.19,1,'ubuntu16-02\n'),('poll','25',1499178602,1.28,1,'ubuntu16-02\n'),('poll','28',1499178602,3.77,1,'ubuntu16\n'),('poll','27',1499178602,3.81,1,'ubuntu16-02\n'),('pollbill','',1499178601,73.36,0,'ubuntu16-02\n'),('pollbill','',1499178602,73.11,0,'ubuntu16\n'),('poll','24',1499178902,1.35,1,'ubuntu16-02\n'),('poll','25',1499178902,1.38,1,'ubuntu16-02\n'),('poll','23',1499178902,1.42,1,'ubuntu16-02\n'),('poll','22',1499178903,1.03,1,'ubuntu16\n'),('poll','28',1499178903,3.78,1,'ubuntu16\n'),('poll','27',1499178902,4.04,1,'ubuntu16-02\n'),('pollbill','',1499178902,73.25,0,'ubuntu16-02\n'),('pollbill','',1499178902,73.27,0,'ubuntu16\n'),('poll','25',1499179202,4.36,1,'ubuntu16-02\n'),('poll','23',1499179202,4.36,1,'ubuntu16-02\n'),('poll','24',1499179202,4.37,1,'ubuntu16-02\n'),('poll','22',1499179202,6.49,1,'ubuntu16\n'),('poll','28',1499179202,23.71,1,'ubuntu16\n'),('poll','27',1499179202,27.65,1,'ubuntu16-02\n'),('pollbill','',1499179202,81.42,0,'ubuntu16\n'),('pollbill','',1499179201,85.66,0,'ubuntu16-02\n'),('poll','23',1499179502,1.21,1,'ubuntu16-02\n'),('poll','25',1499179502,1.21,1,'ubuntu16-02\n'),('poll','24',1499179502,1.24,1,'ubuntu16-02\n'),('poll','22',1499179502,1.08,1,'ubuntu16\n'),('poll','27',1499179502,4.05,1,'ubuntu16-02\n'),('poll','28',1499179502,3.86,1,'ubuntu16\n'),('pollbill','',1499179501,73.18,0,'ubuntu16-02\n'),('pollbill','',1499179502,72.93,0,'ubuntu16\n'),('poll','23',1499179802,1.24,1,'ubuntu16-02\n'),('poll','24',1499179802,1.16,1,'ubuntu16-02\n'),('poll','25',1499179802,1.14,1,'ubuntu16-02\n'),('poll','22',1499179802,1.80,1,'ubuntu16\n'),('poll','27',1499179802,4.13,1,'ubuntu16-02\n'),('poll','28',1499179802,4.61,1,'ubuntu16\n'),('pollbill','',1499179802,73.08,0,'ubuntu16-02\n'),('pollbill','',1499179802,73.16,0,'ubuntu16\n'),('poll','22',1499180102,1.08,1,'ubuntu16\n'),('poll','25',1499180102,1.22,1,'ubuntu16-02\n'),('poll','24',1499180102,1.23,1,'ubuntu16-02\n'),('poll','23',1499180102,1.24,1,'ubuntu16-02\n'),('poll','28',1499180102,3.84,1,'ubuntu16\n'),('poll','27',1499180102,3.76,1,'ubuntu16-02\n'),('pollbill','',1499180102,72.88,0,'ubuntu16-02\n'),('pollbill','',1499180102,73.23,0,'ubuntu16\n'),('poll','22',1499180402,1.03,1,'ubuntu16\n'),('poll','24',1499180402,1.16,1,'ubuntu16-02\n'),('poll','23',1499180402,1.15,1,'ubuntu16-02\n'),('poll','25',1499180402,1.16,1,'ubuntu16-02\n'),('poll','28',1499180402,3.82,1,'ubuntu16\n'),('poll','27',1499180402,3.81,1,'ubuntu16-02\n'),('pollbill','',1499180402,73.46,0,'ubuntu16\n'),('pollbill','',1499180402,73.04,0,'ubuntu16-02\n'),('poll','22',1499180702,1.07,1,'ubuntu16\n'),('poll','23',1499180702,1.18,1,'ubuntu16-02\n'),('poll','24',1499180702,1.17,1,'ubuntu16-02\n'),('poll','25',1499180702,1.18,1,'ubuntu16-02\n'),('poll','28',1499180702,3.94,1,'ubuntu16\n'),('poll','27',1499180702,3.92,1,'ubuntu16-02\n'),('pollbill','',1499180701,73.18,0,'ubuntu16-02\n'),('pollbill','',1499180702,72.96,0,'ubuntu16\n'),('poll','22',1499181002,1.05,1,'ubuntu16\n'),('poll','23',1499181002,1.18,1,'ubuntu16-02\n'),('poll','24',1499181002,1.17,1,'ubuntu16-02\n'),('poll','25',1499181002,1.14,1,'ubuntu16-02\n'),('poll','27',1499181002,3.97,1,'ubuntu16-02\n'),('poll','28',1499181002,3.89,1,'ubuntu16\n'),('pollbill','',1499181002,73.06,0,'ubuntu16-02\n'),('pollbill','',1499181002,73.08,0,'ubuntu16\n'),('poll','23',1499181302,1.12,1,'ubuntu16-02\n'),('poll','24',1499181302,1.16,1,'ubuntu16-02\n'),('poll','25',1499181302,1.16,1,'ubuntu16-02\n'),('poll','27',1499181302,3.94,1,'ubuntu16-02\n'),('pollbill','',1499181302,73.11,0,'ubuntu16-02\n'),('poll','23',1499181602,1.59,1,'ubuntu16-02\n'),('poll','25',1499181602,1.61,1,'ubuntu16-02\n'),('poll','24',1499181602,1.71,1,'ubuntu16-02\n'),('poll','27',1499181602,4.89,1,'ubuntu16-02\n'),('pollbill','',1499181601,74.39,0,'ubuntu16-02\n'),('poll','24',1499181902,1.09,1,'ubuntu16-02\n'),('poll','23',1499181902,1.09,1,'ubuntu16-02\n'),('pollbill','',1499181902,73.25,0,'ubuntu16-02\n'),('poll','25',1499182202,1.13,1,'ubuntu16-02\n'),('poll','23',1499182202,1.25,1,'ubuntu16-02\n'),('poll','24',1499182202,1.24,1,'ubuntu16-02\n'),('poll','27',1499182202,4.10,1,'ubuntu16-02\n'),('pollbill','',1499182201,73.49,0,'ubuntu16-02\n'),('poll','25',1499182502,1.37,1,'ubuntu16-02\n'),('poll','24',1499182502,1.43,1,'ubuntu16-02\n'),('poll','23',1499182502,1.56,1,'ubuntu16-02\n'),('poll','27',1499182502,4.26,1,'ubuntu16-02\n'),('pollbill','',1499182502,73.30,0,'ubuntu16-02\n'),('poll','23',1499182802,1.27,1,'ubuntu16-02\n'),('poll','25',1499182802,1.30,1,'ubuntu16-02\n'),('poll','24',1499182802,1.30,1,'ubuntu16-02\n'),('poll','27',1499182802,4.43,1,'ubuntu16-02\n'),('pollbill','',1499182802,73.22,0,'ubuntu16-02\n'),('poll','25',1499183103,1.21,1,'ubuntu16-02\n'),('poll','24',1499183103,1.27,1,'ubuntu16-02\n'),('poll','23',1499183103,1.40,1,'ubuntu16-02\n'),('poll','27',1499183103,4.64,1,'ubuntu16-02\n'),('pollbill','',1499183102,73.74,0,'ubuntu16-02\n'),('poll','25',1499183402,1.72,1,'ubuntu16-02\n'),('poll','23',1499183402,1.72,1,'ubuntu16-02\n'),('poll','24',1499183402,1.82,1,'ubuntu16-02\n'),('poll','27',1499183402,4.55,1,'ubuntu16-02\n'),('poll','28',1499183430,3.80,1,'ubuntu16\n'),('pollbill','',1499183402,73.85,0,'ubuntu16-02\n'),('poll','23',1499183702,1.21,1,'ubuntu16-02\n'),('poll','25',1499183702,1.29,1,'ubuntu16-02\n'),('poll','24',1499183702,1.20,1,'ubuntu16-02\n'),('poll','22',1499183703,1.01,1,'ubuntu16\n'),('poll','28',1499183702,3.81,1,'ubuntu16\n'),('poll','27',1499183702,4.04,1,'ubuntu16-02\n'),('pollbill','',1499183702,73.34,0,'ubuntu16-02\n'),('pollbill','',1499183702,73.08,0,'ubuntu16\n'),('poll','23',1499184002,1.27,1,'ubuntu16-02\n'),('poll','24',1499184002,1.30,1,'ubuntu16-02\n'),('poll','25',1499184002,1.30,1,'ubuntu16-02\n'),('poll','22',1499184002,1.00,1,'ubuntu16\n'),('poll','27',1499184002,4.05,1,'ubuntu16-02\n'),('poll','28',1499184002,3.65,1,'ubuntu16\n'),('pollbill','',1499184001,73.07,0,'ubuntu16-02\n'),('pollbill','',1499184002,72.98,0,'ubuntu16\n'),('poll','22',1499184302,1.01,1,'ubuntu16\n'),('poll','23',1499184302,1.15,1,'ubuntu16-02\n'),('poll','28',1499184302,1.37,1,'ubuntu16\n'),('poll','25',1499184302,1.15,1,'ubuntu16-02\n'),('poll','24',1499184302,1.15,1,'ubuntu16-02\n'),('poll','27',1499184302,3.73,1,'ubuntu16-02\n'),('pollbill','',1499184302,73.09,0,'ubuntu16\n'),('pollbill','',1499184302,73.01,0,'ubuntu16-02\n'),('poll','22',1499184602,1.00,1,'ubuntu16\n'),('poll','23',1499184603,1.14,1,'ubuntu16-02\n'),('poll','24',1499184603,1.14,1,'ubuntu16-02\n'),('poll','25',1499184603,1.21,1,'ubuntu16-02\n'),('poll','28',1499184602,3.97,1,'ubuntu16\n'),('poll','27',1499184603,4.03,1,'ubuntu16-02\n'),('pollbill','',1499184602,73.28,0,'ubuntu16-02\n'),('pollbill','',1499184602,73.04,0,'ubuntu16\n'),('poll','22',1499184902,1.02,1,'ubuntu16\n'),('poll','23',1499184902,1.22,1,'ubuntu16-02\n'),('poll','24',1499184902,1.23,1,'ubuntu16-02\n'),('poll','25',1499184902,1.22,1,'ubuntu16-02\n'),('poll','28',1499184902,3.72,1,'ubuntu16\n'),('poll','27',1499184902,3.92,1,'ubuntu16-02\n'),('pollbill','',1499184901,73.04,0,'ubuntu16-02\n'),('pollbill','',1499184902,73.12,0,'ubuntu16\n'),('poll','22',1499185202,1.02,1,'ubuntu16\n'),('poll','25',1499185202,1.17,1,'ubuntu16-02\n'),('poll','23',1499185202,1.16,1,'ubuntu16-02\n'),('poll','24',1499185202,1.53,1,'ubuntu16-02\n'),('poll','28',1499185202,4.66,1,'ubuntu16\n'),('poll','27',1499185202,5.06,1,'ubuntu16-02\n'),('pollbill','',1499185202,73.81,0,'ubuntu16\n'),('pollbill','',1499185202,74.84,0,'ubuntu16-02\n'),('poll','22',1499185502,1.02,1,'ubuntu16\n'),('poll','23',1499185502,1.18,1,'ubuntu16-02\n'),('poll','25',1499185502,1.17,1,'ubuntu16-02\n'),('poll','24',1499185502,1.19,1,'ubuntu16-02\n'),('poll','28',1499185502,4.07,1,'ubuntu16\n'),('poll','27',1499185502,3.77,1,'ubuntu16-02\n'),('pollbill','',1499185502,73.12,0,'ubuntu16\n'),('pollbill','',1499185502,74.56,0,'ubuntu16-02\n'),('poll','22',1499185802,1.02,1,'ubuntu16\n'),('poll','23',1499185802,1.26,1,'ubuntu16-02\n'),('poll','24',1499185802,1.24,1,'ubuntu16-02\n'),('poll','25',1499185802,1.25,1,'ubuntu16-02\n'),('poll','28',1499185802,4.99,1,'ubuntu16\n'),('poll','27',1499185802,5.05,1,'ubuntu16-02\n'),('pollbill','',1499185802,74.86,0,'ubuntu16\n'),('pollbill','',1499185802,75.28,0,'ubuntu16-02\n'),('poll','22',1499186102,1.02,1,'ubuntu16\n'),('poll','23',1499186102,1.21,1,'ubuntu16-02\n'),('poll','24',1499186102,1.22,1,'ubuntu16-02\n'),('poll','25',1499186102,1.22,1,'ubuntu16-02\n'),('poll','28',1499186102,3.95,1,'ubuntu16\n'),('poll','27',1499186102,3.85,1,'ubuntu16-02\n'),('pollbill','',1499186102,72.82,0,'ubuntu16\n'),('pollbill','',1499186102,73.21,0,'ubuntu16-02\n'),('poll','22',1499186402,1.02,1,'ubuntu16\n'),('poll','23',1499186402,1.26,1,'ubuntu16-02\n'),('poll','25',1499186402,1.28,1,'ubuntu16-02\n'),('poll','24',1499186402,1.21,1,'ubuntu16-02\n'),('poll','28',1499186402,3.84,1,'ubuntu16\n'),('poll','27',1499186402,4.14,1,'ubuntu16-02\n'),('pollbill','',1499186402,72.80,0,'ubuntu16-02\n'),('pollbill','',1499186402,72.79,0,'ubuntu16\n'),('poll','23',1499186707,2.47,1,'ubuntu16-02\n'),('poll','22',1499186702,7.35,1,'ubuntu16\n'),('poll','24',1499186707,2.47,1,'ubuntu16-02\n'),('poll','25',1499186707,3.50,1,'ubuntu16-02\n'),('poll','27',1499186707,14.14,1,'ubuntu16-02\n'),('poll','28',1499186702,19.01,1,'ubuntu16\n'),('pollbill','',1499186702,81.37,0,'ubuntu16-02\n'),('pollbill','',1499186701,84.05,0,'ubuntu16\n'),('poll','24',1499187012,1.38,1,'ubuntu16-02\n'),('poll','23',1499187012,1.35,1,'ubuntu16-02\n'),('poll','25',1499187012,1.62,1,'ubuntu16-02\n'),('poll','22',1499187014,1.02,1,'ubuntu16\n'),('poll','27',1499187012,6.09,1,'ubuntu16-02\n'),('poll','28',1499187014,5.14,1,'ubuntu16\n'),('pollbill','',1499187013,100.30,0,'ubuntu16\n'),('poll','22',1499187302,1.17,1,'ubuntu16\n'),('poll','28',1499187302,3.70,1,'ubuntu16\n'),('pollbill','',1499187302,97.56,0,'ubuntu16\n'),('poll','22',1499210705,1.90,1,'ubuntu16\n'),('poll','28',1499210705,13.26,1,'ubuntu16\n'),('pollbill','',1499210701,110.30,0,'ubuntu16\n'),('poll','22',1499211002,0.99,1,'ubuntu16\n'),('poll','28',1499211002,3.58,1,'ubuntu16\n'),('pollbill','',1499211002,97.11,0,'ubuntu16\n'),('poll','22',1499211302,1.00,1,'ubuntu16\n'),('poll','28',1499211302,3.62,1,'ubuntu16\n'),('pollbill','',1499211302,96.85,0,'ubuntu16\n'),('poll','22',1499211602,1.03,1,'ubuntu16\n'),('poll','28',1499211602,3.58,1,'ubuntu16\n'),('pollbill','',1499211602,97.07,0,'ubuntu16\n'),('poll','22',1499211902,0.99,1,'ubuntu16\n'),('poll','28',1499211902,3.63,1,'ubuntu16\n'),('pollbill','',1499211901,97.06,0,'ubuntu16\n'),('poll','22',1499212202,1.03,1,'ubuntu16\n'),('poll','28',1499212202,3.66,1,'ubuntu16\n'),('pollbill','',1499212202,97.16,0,'ubuntu16\n'),('poll','22',1499212502,0.99,1,'ubuntu16\n'),('poll','28',1499212502,3.57,1,'ubuntu16\n'),('pollbill','',1499212502,97.25,0,'ubuntu16\n'),('poll','22',1499212801,1.01,1,'ubuntu16\n'),('poll','28',1499212801,3.73,1,'ubuntu16\n'),('pollbill','',1499212801,97.19,0,'ubuntu16\n'),('poll','22',1499213102,1.08,1,'ubuntu16\n'),('poll','28',1499213102,3.73,1,'ubuntu16\n'),('pollbill','',1499213102,4372.00,0,'ubuntu16\n'),('poll','22',1499217602,1.07,1,'ubuntu16\n'),('poll','28',1499217602,2.16,1,'ubuntu16\n'),('pollbill','',1499217602,98.09,0,'ubuntu16\n'),('poll','22',1499217902,1.02,1,'ubuntu16\n'),('poll','28',1499217902,1.62,1,'ubuntu16\n'),('pollbill','',1499217902,97.55,0,'ubuntu16\n'),('poll','22',1499218202,1.06,1,'ubuntu16\n'),('poll','28',1499218202,1.62,1,'ubuntu16\n'),('pollbill','',1499218202,97.71,0,'ubuntu16\n'),('poll','22',1499218502,1.04,1,'ubuntu16\n'),('poll','28',1499218502,1.91,1,'ubuntu16\n'),('pollbill','',1499218501,97.54,0,'ubuntu16\n'),('poll','22',1499218802,1.03,1,'ubuntu16\n'),('poll','28',1499218802,1.87,1,'ubuntu16\n'),('pollbill','',1499218802,97.75,0,'ubuntu16\n'),('poll','22',1499219101,1.03,1,'ubuntu16\n'),('poll','28',1499219101,2.17,1,'ubuntu16\n'),('pollbill','',1499219101,97.67,0,'ubuntu16\n'),('poll','22',1499219402,1.01,1,'ubuntu16\n'),('poll','28',1499219402,1.54,1,'ubuntu16\n'),('pollbill','',1499219402,97.82,0,'ubuntu16\n'),('poll','22',1499219702,1.07,1,'ubuntu16\n'),('poll','28',1499219702,1.59,1,'ubuntu16\n'),('pollbill','',1499219702,97.65,0,'ubuntu16\n'),('poll','22',1499220002,1.04,1,'ubuntu16\n'),('poll','28',1499220002,1.66,1,'ubuntu16\n'),('pollbill','',1499220002,97.46,0,'ubuntu16\n'),('poll','22',1499220302,1.05,1,'ubuntu16\n'),('poll','28',1499220302,1.65,1,'ubuntu16\n'),('pollbill','',1499220302,97.62,0,'ubuntu16\n'),('poll','22',1499220602,1.05,1,'ubuntu16\n'),('poll','28',1499220602,1.75,1,'ubuntu16\n'),('pollbill','',1499220602,97.83,0,'ubuntu16\n'),('poll','22',1499220902,1.01,1,'ubuntu16\n'),('poll','28',1499220902,1.86,1,'ubuntu16\n'),('pollbill','',1499220902,97.69,0,'ubuntu16\n'),('poll','22',1499221202,1.01,1,'ubuntu16\n'),('poll','28',1499221202,1.90,1,'ubuntu16\n'),('pollbill','',1499221202,97.56,0,'ubuntu16\n'),('poll','22',1499221501,1.03,1,'ubuntu16\n'),('poll','28',1499221501,1.60,1,'ubuntu16\n'),('pollbill','',1499221501,97.65,0,'ubuntu16\n'),('poll','22',1499221802,1.08,1,'ubuntu16\n'),('poll','28',1499221802,1.56,1,'ubuntu16\n'),('pollbill','',1499221802,97.54,0,'ubuntu16\n'),('poll','22',1499222102,1.07,1,'ubuntu16\n'),('poll','28',1499222102,1.81,1,'ubuntu16\n'),('pollbill','',1499222102,97.57,0,'ubuntu16\n'),('poll','22',1499222401,1.00,1,'ubuntu16\n'),('poll','28',1499222401,1.82,1,'ubuntu16\n'),('pollbill','',1499222401,97.60,0,'ubuntu16\n'),('poll','22',1499222702,1.04,1,'ubuntu16\n'),('poll','28',1499222702,1.69,1,'ubuntu16\n'),('pollbill','',1499222702,97.52,0,'ubuntu16\n'),('poll','22',1499223002,1.08,1,'ubuntu16\n'),('poll','28',1499223002,1.52,1,'ubuntu16\n'),('pollbill','',1499223002,97.60,0,'ubuntu16\n'),('poll','22',1499223303,1.02,1,'ubuntu16\n'),('poll','28',1499223303,1.68,1,'ubuntu16\n'),('pollbill','',1499223302,97.70,0,'ubuntu16\n'),('poll','22',1499223602,1.00,1,'ubuntu16\n'),('poll','28',1499223602,1.40,1,'ubuntu16\n'),('pollbill','',1499223602,97.10,0,'ubuntu16\n'),('poll','22',1499223902,1.06,1,'ubuntu16\n'),('poll','28',1499223902,1.36,1,'ubuntu16\n'),('pollbill','',1499223902,97.04,0,'ubuntu16\n'),('poll','22',1499224202,1.08,1,'ubuntu16\n'),('poll','28',1499224202,1.34,1,'ubuntu16\n'),('pollbill','',1499224202,97.87,0,'ubuntu16\n'),('poll','22',1499224501,1.00,1,'ubuntu16\n'),('poll','28',1499224501,1.74,1,'ubuntu16\n'),('pollbill','',1499224501,97.45,0,'ubuntu16\n'),('poll','22',1499224802,0.99,1,'ubuntu16\n'),('poll','28',1499224802,1.41,1,'ubuntu16\n'),('pollbill','',1499224802,97.23,0,'ubuntu16\n'),('poll','22',1499225102,1.00,1,'ubuntu16\n'),('poll','28',1499225102,1.45,1,'ubuntu16\n'),('pollbill','',1499225102,97.16,0,'ubuntu16\n'),('poll','22',1499225402,1.01,1,'ubuntu16\n'),('poll','28',1499225402,2.82,1,'ubuntu16\n'),('pollbill','',1499225401,97.30,0,'ubuntu16\n'),('poll','22',1499225701,1.00,1,'ubuntu16\n'),('poll','28',1499225701,1.44,1,'ubuntu16\n'),('pollbill','',1499225701,97.26,0,'ubuntu16\n'),('poll','22',1499226002,1.08,1,'ubuntu16\n'),('poll','28',1499226002,1.64,1,'ubuntu16\n'),('pollbill','',1499226002,97.27,0,'ubuntu16\n'),('poll','22',1499226302,1.03,1,'ubuntu16\n'),('poll','28',1499226302,1.79,1,'ubuntu16\n'),('pollbill','',1499226302,97.29,0,'ubuntu16\n'),('poll','22',1499226602,0.99,1,'ubuntu16\n'),('poll','28',1499226602,1.33,1,'ubuntu16\n'),('pollbill','',1499226602,97.27,0,'ubuntu16\n'),('poll','22',1499226902,1.00,1,'ubuntu16\n'),('poll','28',1499226902,1.45,1,'ubuntu16\n'),('pollbill','',1499226901,97.36,0,'ubuntu16\n'),('poll','22',1499227202,1.02,1,'ubuntu16\n'),('poll','28',1499227202,1.48,1,'ubuntu16\n'),('pollbill','',1499227201,97.28,0,'ubuntu16\n'),('poll','22',1499227502,1.02,1,'ubuntu16\n'),('poll','28',1499227502,1.36,1,'ubuntu16\n'),('pollbill','',1499227502,97.34,0,'ubuntu16\n'),('poll','22',1499227802,1.02,1,'ubuntu16\n'),('poll','28',1499227802,1.37,1,'ubuntu16\n'),('pollbill','',1499227802,97.84,0,'ubuntu16\n'),('poll','22',1499228101,0.99,1,'ubuntu16\n'),('poll','28',1499228101,1.34,1,'ubuntu16\n'),('pollbill','',1499228101,97.42,0,'ubuntu16\n'),('poll','22',1499228401,1.05,1,'ubuntu16\n'),('poll','28',1499228401,1.52,1,'ubuntu16\n'),('pollbill','',1499228401,97.24,0,'ubuntu16\n'),('poll','22',1499228702,1.05,1,'ubuntu16\n'),('poll','28',1499228702,1.42,1,'ubuntu16\n'),('pollbill','',1499228702,97.15,0,'ubuntu16\n'),('poll','22',1499229002,1.00,1,'ubuntu16\n'),('poll','28',1499229002,1.53,1,'ubuntu16\n'),('pollbill','',1499229002,97.29,0,'ubuntu16\n'),('poll','22',1499229302,1.01,1,'ubuntu16\n'),('poll','28',1499229302,1.39,1,'ubuntu16\n'),('pollbill','',1499229301,97.56,0,'ubuntu16\n'),('poll','22',1499229602,1.03,1,'ubuntu16\n'),('poll','28',1499229602,1.61,1,'ubuntu16\n'),('pollbill','',1499229602,97.50,0,'ubuntu16\n'),('poll','22',1499229902,1.02,1,'ubuntu16\n'),('poll','28',1499229902,1.73,1,'ubuntu16\n'),('pollbill','',1499229902,97.49,0,'ubuntu16\n'),('poll','22',1499230203,1.06,1,'ubuntu16\n'),('poll','28',1499230203,3.10,1,'ubuntu16\n'),('pollbill','',1499230202,97.75,0,'ubuntu16\n'),('poll','22',1499230503,1.05,1,'ubuntu16\n'),('poll','28',1499230503,1.64,1,'ubuntu16\n'),('pollbill','',1499230503,97.79,0,'ubuntu16\n'),('poll','22',1499230802,1.12,1,'ubuntu16\n'),('poll','28',1499230802,1.77,1,'ubuntu16\n'),('pollbill','',1499230802,97.29,0,'ubuntu16\n'),('poll','22',1499231102,1.02,1,'ubuntu16\n'),('poll','28',1499231102,1.72,1,'ubuntu16\n'),('pollbill','',1499231102,97.75,0,'ubuntu16\n'),('poll','22',1499231402,1.00,1,'ubuntu16\n'),('poll','28',1499231402,1.79,1,'ubuntu16\n'),('pollbill','',1499231402,97.63,0,'ubuntu16\n'),('poll','22',1499231702,1.15,1,'ubuntu16\n'),('poll','28',1499231702,1.90,1,'ubuntu16\n'),('pollbill','',1499231702,98.08,0,'ubuntu16\n'),('poll','22',1499232002,1.01,1,'ubuntu16\n'),('poll','28',1499232002,2.19,1,'ubuntu16\n'),('pollbill','',1499232002,97.63,0,'ubuntu16\n'),('poll','22',1499232301,1.01,1,'ubuntu16\n'),('poll','28',1499232301,1.96,1,'ubuntu16\n'),('pollbill','',1499232301,98.02,0,'ubuntu16\n'),('poll','22',1499232602,1.07,1,'ubuntu16\n'),('poll','28',1499232602,1.93,1,'ubuntu16\n'),('pollbill','',1499232602,98.36,0,'ubuntu16\n'),('poll','22',1499232902,1.02,1,'ubuntu16\n'),('poll','28',1499232902,1.93,1,'ubuntu16\n'),('pollbill','',1499232902,97.64,0,'ubuntu16\n'),('poll','22',1499233202,1.02,1,'ubuntu16\n'),('poll','28',1499233202,1.53,1,'ubuntu16\n'),('pollbill','',1499233202,98.02,0,'ubuntu16\n'),('poll','22',1499233502,1.04,1,'ubuntu16\n'),('poll','28',1499233502,1.69,1,'ubuntu16\n'),('pollbill','',1499233501,98.11,0,'ubuntu16\n'),('poll','22',1499233801,1.00,1,'ubuntu16\n'),('poll','28',1499233801,1.70,1,'ubuntu16\n'),('pollbill','',1499233801,97.54,0,'ubuntu16\n'),('poll','22',1499234102,1.09,1,'ubuntu16\n'),('poll','28',1499234102,1.58,1,'ubuntu16\n'),('pollbill','',1499234102,97.59,0,'ubuntu16\n'),('poll','22',1499234402,1.01,1,'ubuntu16\n'),('poll','28',1499234402,1.69,1,'ubuntu16\n'),('pollbill','',1499234402,98.06,0,'ubuntu16\n'),('poll','22',1499234702,1.00,1,'ubuntu16\n'),('poll','28',1499234702,1.68,1,'ubuntu16\n'),('pollbill','',1499234702,97.69,0,'ubuntu16\n'),('poll','22',1499235002,1.00,1,'ubuntu16\n'),('poll','28',1499235002,1.74,1,'ubuntu16\n'),('pollbill','',1499235001,97.65,0,'ubuntu16\n'),('poll','22',1499235302,1.14,1,'ubuntu16\n'),('poll','28',1499235302,1.89,1,'ubuntu16\n'),('pollbill','',1499235302,97.48,0,'ubuntu16\n'),('pollbill','',1499235606,2.07,0,'ubuntu16\n'),('poll','22',1499235606,2.41,1,'ubuntu16\n'),('poll','28',1499235606,3.02,1,'ubuntu16\n'),('poll','25',1499235608,2.35,1,'ubuntu16-02\n'),('poll','24',1499235608,2.34,1,'ubuntu16-02\n'),('pollbill','',1499235605,10.71,0,'ubuntu16-02\n'),('poll','27',1499235608,9.12,1,'ubuntu16-02\n'),('poll','23',1499235608,9.17,1,'ubuntu16-02\n'),('pollbill','',1499235903,0.99,0,'ubuntu16\n'),('pollbill','',1499235902,2.34,0,'ubuntu16-02\n'),('poll','24',1499235903,1.77,1,'ubuntu16-02\n'),('poll','25',1499235903,2.08,1,'ubuntu16-02\n'),('poll','22',1499235903,1.94,1,'ubuntu16\n'),('poll','28',1499235903,2.20,1,'ubuntu16\n'),('poll','23',1499235903,3.22,1,'ubuntu16-02\n'),('poll','27',1499235903,3.29,1,'ubuntu16-02\n'),('pollbill','',1499236202,0.85,0,'ubuntu16\n'),('pollbill','',1499236202,1.91,0,'ubuntu16-02\n'),('poll','24',1499236202,1.83,1,'ubuntu16-02\n'),('poll','25',1499236202,1.85,1,'ubuntu16-02\n'),('poll','22',1499236203,1.84,1,'ubuntu16\n'),('poll','28',1499236202,2.01,1,'ubuntu16\n'),('poll','23',1499236203,2.94,1,'ubuntu16-02\n'),('poll','27',1499236202,3.06,1,'ubuntu16-02\n'),('pollbill','',1499236501,0.56,0,'ubuntu16\n'),('poll','22',1499236502,1.76,1,'ubuntu16\n'),('pollbill','',1499236502,1.45,0,'ubuntu16-02\n'),('poll','28',1499236502,2.13,1,'ubuntu16\n'),('poll','25',1499236503,1.50,1,'ubuntu16-02\n'),('poll','24',1499236503,1.52,1,'ubuntu16-02\n'),('poll','23',1499236503,3.03,1,'ubuntu16-02\n'),('poll','27',1499236503,3.10,1,'ubuntu16-02\n'),('pollbill','',1499236802,0.95,0,'ubuntu16\n'),('pollbill','',1499236802,1.95,0,'ubuntu16-02\n'),('poll','22',1499236802,2.04,1,'ubuntu16\n'),('poll','28',1499236802,2.25,1,'ubuntu16\n'),('poll','24',1499236803,1.64,1,'ubuntu16-02\n'),('poll','25',1499236803,1.68,1,'ubuntu16-02\n'),('poll','23',1499236803,2.49,1,'ubuntu16-02\n'),('poll','27',1499236804,2.99,1,'ubuntu16-02\n'),('pollbill','',1499237102,2.06,0,'ubuntu16-02\n'),('pollbill','',1499237103,1.06,0,'ubuntu16\n'),('poll','25',1499237103,1.82,1,'ubuntu16-02\n'),('poll','24',1499237103,1.84,1,'ubuntu16-02\n'),('poll','22',1499237103,1.78,1,'ubuntu16\n'),('poll','28',1499237103,2.26,1,'ubuntu16\n'),('poll','23',1499237103,3.00,1,'ubuntu16-02\n'),('poll','27',1499237103,3.31,1,'ubuntu16-02\n'),('pollbill','',1499237402,1.20,0,'ubuntu16\n'),('pollbill','',1499237401,1.77,0,'ubuntu16-02\n'),('poll','22',1499237403,1.70,1,'ubuntu16\n'),('poll','25',1499237403,1.59,1,'ubuntu16-02\n'),('poll','24',1499237403,1.62,1,'ubuntu16-02\n'),('poll','28',1499237403,1.86,1,'ubuntu16\n'),('poll','23',1499237403,2.42,1,'ubuntu16-02\n'),('poll','27',1499237403,2.70,1,'ubuntu16-02\n'),('pollbill','',1499237707,0.86,0,'ubuntu16\n'),('poll','22',1499237708,1.27,1,'ubuntu16\n'),('poll','28',1499237708,1.51,1,'ubuntu16\n'),('poll','24',1499237708,3.01,1,'ubuntu16-02\n'),('pollbill','',1499237707,8.94,0,'ubuntu16-02\n'),('poll','27',1499237708,8.31,1,'ubuntu16-02\n'),('poll','23',1499237708,17.54,1,'ubuntu16-02\n'),('poll','25',1499237708,18.51,1,'ubuntu16-02\n'),('poll','24',1499238016,10.46,1,'ubuntu16-02\n'),('poll','25',1499238022,4.66,1,'ubuntu16-02\n'),('pollbill','',1499238026,0.72,0,'ubuntu16\n'),('poll','23',1499238012,15.62,1,'ubuntu16-02\n'),('pollbill','',1499238006,21.95,0,'ubuntu16-02\n'),('poll','27',1499238022,6.55,1,'ubuntu16-02\n'),('poll','22',1499238027,1.81,1,'ubuntu16\n'),('poll','28',1499238027,2.06,1,'ubuntu16\n'),('poll','22',1499238308,5.48,1,'ubuntu16\n'),('poll','28',1499238313,1.37,1,'ubuntu16\n'),('poll','25',1499238306,10.54,1,'ubuntu16-02\n'),('poll','24',1499238304,12.65,1,'ubuntu16-02\n'),('pollbill','',1499238301,15.80,0,'ubuntu16\n'),('pollbill','',1499238302,15.34,0,'ubuntu16-02\n'),('poll','27',1499238304,14.06,1,'ubuntu16-02\n'),('poll','23',1499238308,10.86,1,'ubuntu16-02\n'),('poll','24',1499238602,1.99,1,'ubuntu16-02\n'),('poll','25',1499238602,1.92,1,'ubuntu16-02\n'),('poll','23',1499238602,5.26,1,'ubuntu16-02\n'),('poll','27',1499238602,5.33,1,'ubuntu16-02\n'),('pollbill','',1499238601,6.21,0,'ubuntu16-02\n'),('pollbill','',1499238607,1.32,0,'ubuntu16\n'),('poll','22',1499238607,2.10,1,'ubuntu16\n'),('poll','28',1499238607,1.99,1,'ubuntu16\n'),('pollbill','',1499238901,1.79,0,'ubuntu16\n'),('pollbill','',1499238902,1.68,0,'ubuntu16-02\n'),('poll','22',1499238902,1.80,1,'ubuntu16\n'),('poll','28',1499238902,1.97,1,'ubuntu16\n'),('poll','25',1499238903,1.35,1,'ubuntu16-02\n'),('poll','24',1499238903,1.35,1,'ubuntu16-02\n'),('poll','23',1499238903,2.39,1,'ubuntu16-02\n'),('poll','27',1499238903,2.46,1,'ubuntu16-02\n'),('pollbill','',1499239202,1.04,0,'ubuntu16\n'),('pollbill','',1499239201,1.56,0,'ubuntu16-02\n'),('poll','22',1499239202,1.66,1,'ubuntu16\n'),('poll','24',1499239202,1.66,1,'ubuntu16-02\n'),('poll','25',1499239202,1.70,1,'ubuntu16-02\n'),('poll','28',1499239202,2.04,1,'ubuntu16\n'),('poll','23',1499239202,2.73,1,'ubuntu16-02\n'),('poll','27',1499239202,3.00,1,'ubuntu16-02\n'),('pollbill','',1499239502,1.13,0,'ubuntu16\n'),('pollbill','',1499239502,1.43,0,'ubuntu16-02\n'),('poll','22',1499239502,1.63,1,'ubuntu16\n'),('poll','24',1499239503,1.46,1,'ubuntu16-02\n'),('poll','28',1499239502,2.12,1,'ubuntu16\n'),('poll','25',1499239503,1.35,1,'ubuntu16-02\n'),('poll','23',1499239503,1.83,1,'ubuntu16-02\n'),('poll','27',1499239503,2.19,1,'ubuntu16-02\n'),('pollbill','',1499239802,0.62,0,'ubuntu16\n'),('pollbill','',1499239802,1.24,0,'ubuntu16-02\n'),('poll','22',1499239802,1.30,1,'ubuntu16\n'),('poll','28',1499239802,1.58,1,'ubuntu16\n'),('poll','24',1499239802,1.38,1,'ubuntu16-02\n'),('poll','25',1499239802,1.38,1,'ubuntu16-02\n'),('poll','23',1499239803,2.00,1,'ubuntu16-02\n'),('poll','27',1499239802,2.20,1,'ubuntu16-02\n'),('pollbill','',1499240102,1.00,0,'ubuntu16\n'),('pollbill','',1499240102,1.82,0,'ubuntu16-02\n'),('poll','22',1499240102,2.35,1,'ubuntu16\n'),('poll','28',1499240102,2.45,1,'ubuntu16\n'),('poll','25',1499240103,1.73,1,'ubuntu16-02\n'),('poll','24',1499240103,2.05,1,'ubuntu16-02\n'),('poll','23',1499240104,2.11,1,'ubuntu16-02\n'),('poll','27',1499240104,2.10,1,'ubuntu16-02\n'),('pollbill','',1499240402,0.72,0,'ubuntu16\n'),('pollbill','',1499240402,1.14,0,'ubuntu16-02\n'),('poll','22',1499240402,1.63,1,'ubuntu16\n'),('poll','28',1499240402,1.79,1,'ubuntu16\n'),('poll','25',1499240403,1.51,1,'ubuntu16-02\n'),('poll','24',1499240403,1.48,1,'ubuntu16-02\n'),('poll','23',1499240403,1.91,1,'ubuntu16-02\n'),('poll','27',1499240403,2.19,1,'ubuntu16-02\n'),('pollbill','',1499240701,0.60,0,'ubuntu16\n'),('pollbill','',1499240702,1.16,0,'ubuntu16-02\n'),('poll','22',1499240702,1.78,1,'ubuntu16\n'),('poll','28',1499240702,1.91,1,'ubuntu16\n'),('poll','25',1499240703,1.51,1,'ubuntu16-02\n'),('poll','24',1499240703,1.49,1,'ubuntu16-02\n'),('poll','23',1499240703,1.80,1,'ubuntu16-02\n'),('poll','27',1499240703,2.14,1,'ubuntu16-02\n'),('poll','25',1499241008,1.92,1,'ubuntu16-02\n'),('poll','24',1499241008,2.19,1,'ubuntu16-02\n'),('pollbill','',1499241010,2.25,0,'ubuntu16\n'),('pollbill','',1499241004,7.81,0,'ubuntu16-02\n'),('poll','22',1499241010,2.95,1,'ubuntu16\n'),('poll','23',1499241008,5.24,1,'ubuntu16-02\n'),('poll','27',1499241008,5.38,1,'ubuntu16-02\n'),('poll','28',1499241011,2.65,1,'ubuntu16\n'),('pollbill','',1499241302,0.76,0,'ubuntu16\n'),('pollbill','',1499241302,1.41,0,'ubuntu16-02\n'),('poll','22',1499241303,1.41,1,'ubuntu16\n'),('poll','24',1499241303,1.40,1,'ubuntu16-02\n'),('poll','25',1499241303,1.46,1,'ubuntu16-02\n'),('poll','28',1499241303,1.85,1,'ubuntu16\n'),('poll','23',1499241303,2.13,1,'ubuntu16-02\n'),('poll','27',1499241303,2.35,1,'ubuntu16-02\n'),('pollbill','',1499241602,1.21,0,'ubuntu16-02\n'),('pollbill','',1499241602,0.90,0,'ubuntu16\n'),('poll','22',1499241602,1.34,1,'ubuntu16\n'),('poll','25',1499241602,1.50,1,'ubuntu16-02\n'),('poll','24',1499241603,1.39,1,'ubuntu16-02\n'),('poll','28',1499241602,1.84,1,'ubuntu16\n'),('poll','23',1499241603,2.21,1,'ubuntu16-02\n'),('poll','27',1499241602,2.48,1,'ubuntu16-02\n'),('pollbill','',1499241901,0.75,0,'ubuntu16\n'),('pollbill','',1499241902,1.17,0,'ubuntu16-02\n'),('poll','22',1499241902,2.09,1,'ubuntu16\n'),('poll','25',1499241902,1.57,1,'ubuntu16-02\n'),('poll','24',1499241902,1.54,1,'ubuntu16-02\n'),('poll','28',1499241902,2.35,1,'ubuntu16\n'),('poll','23',1499241902,2.62,1,'ubuntu16-02\n'),('poll','27',1499241902,2.79,1,'ubuntu16-02\n'),('pollbill','',1499242201,0.72,0,'ubuntu16\n'),('pollbill','',1499242201,1.00,0,'ubuntu16-02\n'),('poll','22',1499242202,1.74,1,'ubuntu16\n'),('poll','24',1499242202,1.41,1,'ubuntu16-02\n'),('poll','25',1499242202,1.44,1,'ubuntu16-02\n'),('poll','28',1499242202,2.03,1,'ubuntu16\n'),('poll','23',1499242202,2.20,1,'ubuntu16-02\n'),('poll','27',1499242202,2.37,1,'ubuntu16-02\n'),('pollbill','',1499242501,0.44,0,'ubuntu16\n'),('pollbill','',1499242502,1.07,0,'ubuntu16-02\n'),('poll','22',1499242501,1.53,1,'ubuntu16\n'),('poll','28',1499242501,1.83,1,'ubuntu16\n'),('poll','25',1499242503,1.47,1,'ubuntu16-02\n'),('poll','24',1499242503,1.37,1,'ubuntu16-02\n'),('poll','23',1499242503,1.91,1,'ubuntu16-02\n'),('poll','27',1499242503,2.07,1,'ubuntu16-02\n'),('pollbill','',1499242802,1.30,0,'ubuntu16-02\n'),('pollbill','',1499242803,0.79,0,'ubuntu16\n'),('poll','25',1499242802,1.40,1,'ubuntu16-02\n'),('poll','24',1499242802,1.47,1,'ubuntu16-02\n'),('poll','22',1499242803,1.36,1,'ubuntu16\n'),('poll','23',1499242802,2.25,1,'ubuntu16-02\n'),('poll','28',1499242803,1.73,1,'ubuntu16\n'),('poll','27',1499242802,2.53,1,'ubuntu16-02\n'),('pollbill','',1499243102,0.88,0,'ubuntu16\n'),('poll','22',1499243102,1.55,1,'ubuntu16\n'),('pollbill','',1499243102,1.29,0,'ubuntu16-02\n'),('poll','28',1499243102,2.00,1,'ubuntu16\n'),('poll','25',1499243103,9.48,1,'ubuntu16-02\n'),('poll','24',1499243103,9.41,1,'ubuntu16-02\n'),('poll','23',1499243103,10.31,1,'ubuntu16-02\n'),('poll','27',1499243103,10.99,1,'ubuntu16-02\n'),('poll','24',1499243420,16.64,1,'ubuntu16-02\n'),('poll','25',1499243420,18.21,1,'ubuntu16-02\n'),('poll','27',1499243421,35.84,1,'ubuntu16-02\n'),('poll','23',1499243420,48.54,1,'ubuntu16-02\n'),('pollbill','',1499243407,62.44,0,'ubuntu16-02\n'),('pollbill','',1499243439,31.54,0,'ubuntu16\n'),('poll','22',1499243441,29.52,1,'ubuntu16\n'),('poll','28',1499243439,31.87,1,'ubuntu16\n'),('pollbill','',1499243701,1.42,0,'ubuntu16-02\n'),('pollbill','',1499243702,0.84,0,'ubuntu16\n'),('poll','22',1499243702,1.74,1,'ubuntu16\n'),('poll','28',1499243702,1.74,1,'ubuntu16\n'),('pollbill','',1499244002,0.78,0,'ubuntu16\n'),('pollbill','',1499244002,0.87,0,'ubuntu16-02\n'),('poll','22',1499244002,1.45,1,'ubuntu16\n'),('poll','28',1499244002,1.87,1,'ubuntu16\n'),('poll','25',1499244003,1.40,1,'ubuntu16-02\n'),('poll','24',1499244003,1.50,1,'ubuntu16-02\n'),('poll','23',1499244003,1.71,1,'ubuntu16-02\n'),('poll','27',1499244003,3.24,1,'ubuntu16-02\n'),('poll','25',1499244303,2.01,1,'ubuntu16-02\n'),('poll','24',1499244303,2.42,1,'ubuntu16-02\n'),('poll','27',1499244303,4.28,1,'ubuntu16-02\n'),('poll','23',1499244303,5.02,1,'ubuntu16-02\n'),('pollbill','',1499244302,6.46,0,'ubuntu16-02\n'),('pollbill','',1499244305,3.64,0,'ubuntu16\n'),('poll','22',1499244308,1.43,1,'ubuntu16\n'),('poll','28',1499244305,4.61,1,'ubuntu16\n'),('pollbill','',1499244602,0.57,0,'ubuntu16\n'),('pollbill','',1499244602,1.31,0,'ubuntu16-02\n'),('poll','22',1499244602,1.77,1,'ubuntu16\n'),('poll','28',1499244602,1.94,1,'ubuntu16\n'),('poll','25',1499244603,1.48,1,'ubuntu16-02\n'),('poll','24',1499244603,1.46,1,'ubuntu16-02\n'),('poll','23',1499244603,1.96,1,'ubuntu16-02\n'),('poll','27',1499244603,2.18,1,'ubuntu16-02\n'),('pollbill','',1499244902,0.55,0,'ubuntu16\n'),('poll','22',1499244902,1.58,1,'ubuntu16\n'),('pollbill','',1499244902,1.40,0,'ubuntu16-02\n'),('poll','28',1499244902,1.74,1,'ubuntu16\n'),('poll','24',1499244903,1.26,1,'ubuntu16-02\n'),('poll','25',1499244904,0.99,1,'ubuntu16-02\n'),('poll','23',1499244903,1.98,1,'ubuntu16-02\n'),('poll','27',1499244903,2.21,1,'ubuntu16-02\n'),('pollbill','',1499245202,1.47,0,'ubuntu16-02\n'),('pollbill','',1499245203,0.92,0,'ubuntu16\n'),('poll','24',1499245202,1.38,1,'ubuntu16-02\n'),('poll','25',1499245202,1.36,1,'ubuntu16-02\n'),('poll','22',1499245203,2.05,1,'ubuntu16\n'),('poll','23',1499245202,2.55,1,'ubuntu16-02\n'),('poll','27',1499245202,2.50,1,'ubuntu16-02\n'),('poll','28',1499245203,2.04,1,'ubuntu16\n'),('pollbill','',1499245502,0.67,0,'ubuntu16\n'),('pollbill','',1499245502,1.37,0,'ubuntu16-02\n'),('poll','22',1499245502,1.62,1,'ubuntu16\n'),('poll','25',1499245503,1.43,1,'ubuntu16-02\n'),('poll','28',1499245502,1.94,1,'ubuntu16\n'),('poll','24',1499245503,1.46,1,'ubuntu16-02\n'),('poll','23',1499245502,2.03,1,'ubuntu16-02\n'),('poll','27',1499245503,2.34,1,'ubuntu16-02\n'),('pollbill','',1499245801,1.99,0,'ubuntu16-02\n'),('poll','24',1499245802,1.32,1,'ubuntu16-02\n'),('pollbill','',1499245802,1.23,0,'ubuntu16\n'),('poll','25',1499245802,1.34,1,'ubuntu16-02\n'),('poll','22',1499245803,1.59,1,'ubuntu16\n'),('poll','28',1499245802,2.48,1,'ubuntu16\n'),('poll','27',1499245802,2.76,1,'ubuntu16-02\n'),('poll','23',1499245802,2.83,1,'ubuntu16-02\n'),('pollbill','',1499246101,0.63,0,'ubuntu16\n'),('pollbill','',1499246102,0.87,0,'ubuntu16-02\n'),('poll','22',1499246102,1.51,1,'ubuntu16\n'),('poll','28',1499246102,1.76,1,'ubuntu16\n'),('poll','25',1499246103,1.29,1,'ubuntu16-02\n'),('poll','24',1499246103,1.31,1,'ubuntu16-02\n'),('poll','23',1499246103,2.14,1,'ubuntu16-02\n'),('poll','27',1499246103,2.16,1,'ubuntu16-02\n'),('pollbill','',1499246401,0.74,0,'ubuntu16\n'),('pollbill','',1499246401,1.14,0,'ubuntu16-02\n'),('poll','22',1499246402,1.54,1,'ubuntu16\n'),('poll','28',1499246402,1.86,1,'ubuntu16\n'),('poll','25',1499246402,1.40,1,'ubuntu16-02\n'),('poll','24',1499246402,1.45,1,'ubuntu16-02\n'),('poll','23',1499246402,2.10,1,'ubuntu16-02\n'),('poll','27',1499246402,2.23,1,'ubuntu16-02\n'),('pollbill','',1499246703,1.55,0,'ubuntu16\n'),('poll','25',1499246703,1.37,1,'ubuntu16-02\n'),('pollbill','',1499246703,2.27,0,'ubuntu16-02\n'),('poll','24',1499246704,1.12,1,'ubuntu16-02\n'),('poll','22',1499246704,1.78,1,'ubuntu16\n'),('poll','28',1499246704,2.78,1,'ubuntu16\n'),('poll','27',1499246703,3.18,1,'ubuntu16-02\n'),('poll','23',1499246703,3.12,1,'ubuntu16-02\n'),('pollbill','',1499247001,1.40,0,'ubuntu16-02\n'),('pollbill','',1499247002,1.30,0,'ubuntu16\n'),('poll','25',1499247002,1.28,1,'ubuntu16-02\n'),('poll','24',1499247002,1.38,1,'ubuntu16-02\n'),('poll','22',1499247003,1.90,1,'ubuntu16\n'),('poll','28',1499247002,2.45,1,'ubuntu16\n'),('poll','27',1499247002,2.95,1,'ubuntu16-02\n'),('poll','23',1499247002,2.84,1,'ubuntu16-02\n'),('pollbill','',1499247302,0.87,0,'ubuntu16\n'),('pollbill','',1499247302,1.35,0,'ubuntu16-02\n'),('poll','22',1499247302,1.60,1,'ubuntu16\n'),('poll','28',1499247302,2.00,1,'ubuntu16\n'),('poll','25',1499247303,1.57,1,'ubuntu16-02\n'),('poll','24',1499247303,1.58,1,'ubuntu16-02\n'),('poll','23',1499247303,2.13,1,'ubuntu16-02\n'),('poll','27',1499247303,2.36,1,'ubuntu16-02\n'),('pollbill','',1499247602,1.06,0,'ubuntu16\n'),('pollbill','',1499247602,1.38,0,'ubuntu16-02\n'),('poll','22',1499247602,1.53,1,'ubuntu16\n'),('poll','25',1499247603,1.44,1,'ubuntu16-02\n'),('poll','24',1499247603,1.50,1,'ubuntu16-02\n'),('poll','28',1499247602,2.15,1,'ubuntu16\n'),('poll','23',1499247603,2.03,1,'ubuntu16-02\n'),('poll','27',1499247603,2.22,1,'ubuntu16-02\n'),('pollbill','',1499247901,1.35,0,'ubuntu16-02\n'),('pollbill','',1499247902,0.93,0,'ubuntu16\n'),('poll','25',1499247902,1.46,1,'ubuntu16-02\n'),('poll','24',1499247902,1.47,1,'ubuntu16-02\n'),('poll','22',1499247903,1.44,1,'ubuntu16\n'),('poll','28',1499247902,1.63,1,'ubuntu16\n'),('poll','23',1499247902,2.36,1,'ubuntu16-02\n'),('poll','27',1499247902,2.50,1,'ubuntu16-02\n'),('poll','24',1499248203,1.88,1,'ubuntu16-02\n'),('pollbill','',1499248204,0.68,0,'ubuntu16\n'),('pollbill','',1499248202,2.85,0,'ubuntu16-02\n'),('poll','25',1499248204,1.18,1,'ubuntu16-02\n'),('poll','22',1499248204,1.28,1,'ubuntu16\n'),('poll','28',1499248204,1.59,1,'ubuntu16\n'),('poll','23',1499248204,2.19,1,'ubuntu16-02\n'),('poll','27',1499248204,2.53,1,'ubuntu16-02\n'),('pollbill','',1499248502,0.71,0,'ubuntu16\n'),('pollbill','',1499248501,1.36,0,'ubuntu16-02\n'),('poll','25',1499248502,1.31,1,'ubuntu16-02\n'),('poll','22',1499248502,1.48,1,'ubuntu16\n'),('poll','24',1499248502,1.44,1,'ubuntu16-02\n'),('poll','23',1499248502,1.85,1,'ubuntu16-02\n'),('poll','28',1499248502,2.76,1,'ubuntu16\n'),('poll','27',1499248502,3.22,1,'ubuntu16-02\n'),('poll','22',1499263503,2.04,1,'ubuntu16\n'),('poll','28',1499263503,10.74,1,'ubuntu16\n'),('pollbill','',1499263502,106.90,0,'ubuntu16\n'),('poll','22',1499263802,1.04,1,'ubuntu16\n'),('poll','28',1499263802,1.40,1,'ubuntu16\n'),('pollbill','',1499263802,96.93,0,'ubuntu16\n'),('poll','22',1499264101,1.07,1,'ubuntu16\n'),('poll','28',1499264101,1.33,1,'ubuntu16\n'),('pollbill','',1499264101,97.22,0,'ubuntu16\n'),('poll','22',1499264402,1.00,1,'ubuntu16\n'),('poll','28',1499264402,1.32,1,'ubuntu16\n'),('pollbill','',1499264402,39636.00,0,'ubuntu16\n'),('poll','22',1499304040,1.46,1,'ubuntu16\n'),('poll','28',1499304040,4.01,1,'ubuntu16\n'),('pollbill','',1499304039,98.26,0,'ubuntu16\n'),('poll','22',1499304302,1.06,1,'ubuntu16\n'),('poll','28',1499304302,1.62,1,'ubuntu16\n'),('pollbill','',1499304302,97.96,0,'ubuntu16\n'),('poll','22',1499304602,1.03,1,'ubuntu16\n'),('poll','28',1499304602,1.73,1,'ubuntu16\n'),('pollbill','',1499304602,97.59,0,'ubuntu16\n'),('poll','22',1499304902,1.03,1,'ubuntu16\n'),('poll','28',1499304902,1.89,1,'ubuntu16\n'),('pollbill','',1499304902,97.66,0,'ubuntu16\n'),('poll','22',1499305202,1.06,1,'ubuntu16\n'),('poll','28',1499305202,1.93,1,'ubuntu16\n'),('pollbill','',1499305201,97.61,0,'ubuntu16\n'),('poll','22',1499305502,1.01,1,'ubuntu16\n'),('poll','28',1499305502,1.88,1,'ubuntu16\n'),('pollbill','',1499305502,97.99,0,'ubuntu16\n'),('poll','22',1499305802,1.09,1,'ubuntu16\n'),('poll','28',1499305802,1.83,1,'ubuntu16\n'),('pollbill','',1499305801,97.49,0,'ubuntu16\n'),('poll','22',1499306102,1.02,1,'ubuntu16\n'),('poll','28',1499306102,2.08,1,'ubuntu16\n'),('pollbill','',1499306102,97.12,0,'ubuntu16\n'),('poll','22',1499306402,1.03,1,'ubuntu16\n'),('poll','28',1499306402,1.70,1,'ubuntu16\n'),('pollbill','',1499306401,97.09,0,'ubuntu16\n'),('poll','22',1499306702,1.04,1,'ubuntu16\n'),('poll','28',1499306702,1.92,1,'ubuntu16\n'),('pollbill','',1499306702,97.25,0,'ubuntu16\n'),('poll','22',1499307002,1.02,1,'ubuntu16\n'),('poll','28',1499307002,1.99,1,'ubuntu16\n'),('pollbill','',1499307002,97.56,0,'ubuntu16\n'),('poll','22',1499307302,1.00,1,'ubuntu16\n'),('poll','28',1499307302,2.43,1,'ubuntu16\n'),('pollbill','',1499307302,97.59,0,'ubuntu16\n'),('poll','22',1499307602,1.03,1,'ubuntu16\n'),('poll','28',1499307602,1.54,1,'ubuntu16\n'),('pollbill','',1499307601,97.58,0,'ubuntu16\n'),('poll','22',1499307902,1.13,1,'ubuntu16\n'),('poll','28',1499307902,1.82,1,'ubuntu16\n'),('pollbill','',1499307902,97.68,0,'ubuntu16\n'),('poll','22',1499308203,1.02,1,'ubuntu16\n'),('poll','28',1499308203,1.72,1,'ubuntu16\n'),('pollbill','',1499308202,97.65,0,'ubuntu16\n'),('poll','22',1499308502,1.27,1,'ubuntu16\n'),('poll','28',1499308502,2.13,1,'ubuntu16\n'),('pollbill','',1499308502,97.89,0,'ubuntu16\n'),('poll','22',1499308802,1.36,1,'ubuntu16\n'),('poll','28',1499308802,3.86,1,'ubuntu16\n'),('pollbill','',1499308802,97.48,0,'ubuntu16\n'),('poll','22',1499309102,1.03,1,'ubuntu16\n'),('poll','28',1499309102,3.04,1,'ubuntu16\n'),('pollbill','',1499309101,97.69,0,'ubuntu16\n'),('poll','22',1499309402,1.00,1,'ubuntu16\n'),('poll','28',1499309402,1.40,1,'ubuntu16\n'),('pollbill','',1499309402,97.12,0,'ubuntu16\n'),('poll','22',1499309702,0.99,1,'ubuntu16\n'),('poll','28',1499309702,1.36,1,'ubuntu16\n'),('pollbill','',1499309701,98.81,0,'ubuntu16\n'),('poll','22',1499310002,1.01,1,'ubuntu16\n'),('poll','28',1499310002,1.57,1,'ubuntu16\n'),('pollbill','',1499310002,97.24,0,'ubuntu16\n'),('poll','22',1499310302,0.99,1,'ubuntu16\n'),('poll','28',1499310302,1.43,1,'ubuntu16\n'),('pollbill','',1499310302,97.20,0,'ubuntu16\n'),('poll','22',1499310602,0.99,1,'ubuntu16\n'),('poll','28',1499310602,1.38,1,'ubuntu16\n'),('pollbill','',1499310602,97.21,0,'ubuntu16\n'),('poll','22',1499310902,1.08,1,'ubuntu16\n'),('poll','28',1499310902,1.46,1,'ubuntu16\n'),('pollbill','',1499310902,97.21,0,'ubuntu16\n'),('poll','22',1499311202,1.03,1,'ubuntu16\n'),('poll','28',1499311202,1.43,1,'ubuntu16\n'),('pollbill','',1499311202,97.12,0,'ubuntu16\n'),('poll','22',1499311503,1.19,1,'ubuntu16\n'),('poll','28',1499311503,1.40,1,'ubuntu16\n'),('pollbill','',1499311503,97.36,0,'ubuntu16\n'),('poll','22',1499311803,1.02,1,'ubuntu16\n'),('poll','28',1499311803,1.40,1,'ubuntu16\n'),('pollbill','',1499311803,97.65,0,'ubuntu16\n'),('poll','22',1499312102,1.00,1,'ubuntu16\n'),('poll','28',1499312102,1.33,1,'ubuntu16\n'),('pollbill','',1499312102,97.14,0,'ubuntu16\n'),('poll','22',1499312403,1.02,1,'ubuntu16\n'),('poll','28',1499312403,1.61,1,'ubuntu16\n'),('pollbill','',1499312402,97.23,0,'ubuntu16\n'),('poll','22',1499312703,1.59,1,'ubuntu16\n'),('poll','28',1499312703,1.60,1,'ubuntu16\n'),('pollbill','',1499312702,97.86,0,'ubuntu16\n'),('poll','22',1499313002,1.01,1,'ubuntu16\n'),('poll','28',1499313002,1.63,1,'ubuntu16\n'),('pollbill','',1499313002,97.42,0,'ubuntu16\n'),('poll','22',1499313302,0.98,1,'ubuntu16\n'),('poll','28',1499313302,1.43,1,'ubuntu16\n'),('pollbill','',1499313302,97.13,0,'ubuntu16\n'),('poll','22',1499313602,1.22,1,'ubuntu16\n'),('poll','28',1499313602,3.76,1,'ubuntu16\n'),('pollbill','',1499313602,97.40,0,'ubuntu16\n'),('poll','22',1499313902,0.99,1,'ubuntu16\n'),('poll','28',1499313902,1.49,1,'ubuntu16\n'),('pollbill','',1499313902,97.30,0,'ubuntu16\n'),('poll','22',1499314202,1.03,1,'ubuntu16\n'),('poll','28',1499314202,1.42,1,'ubuntu16\n'),('pollbill','',1499314202,97.45,0,'ubuntu16\n'),('poll','22',1499314503,0.99,1,'ubuntu16\n'),('poll','28',1499314503,1.36,1,'ubuntu16\n'),('pollbill','',1499314503,97.73,0,'ubuntu16\n'),('poll','22',1499314803,1.02,1,'ubuntu16\n'),('poll','28',1499314803,1.44,1,'ubuntu16\n'),('pollbill','',1499314803,97.24,0,'ubuntu16\n'),('poll','22',1499315102,1.03,1,'ubuntu16\n'),('poll','28',1499315102,1.38,1,'ubuntu16\n'),('pollbill','',1499315102,97.11,0,'ubuntu16\n'),('poll','22',1499315403,0.99,1,'ubuntu16\n'),('poll','28',1499315403,1.57,1,'ubuntu16\n'),('pollbill','',1499315403,97.33,0,'ubuntu16\n'),('poll','22',1499315702,1.04,1,'ubuntu16\n'),('poll','28',1499315702,1.33,1,'ubuntu16\n'),('pollbill','',1499315701,97.23,0,'ubuntu16\n'),('poll','22',1499316002,1.03,1,'ubuntu16\n'),('poll','28',1499316002,1.66,1,'ubuntu16\n'),('pollbill','',1499316002,97.34,0,'ubuntu16\n'),('poll','22',1499316302,1.00,1,'ubuntu16\n'),('poll','28',1499316302,1.50,1,'ubuntu16\n'),('pollbill','',1499316302,97.32,0,'ubuntu16\n'),('poll','22',1499316603,1.05,1,'ubuntu16\n'),('poll','28',1499316603,1.61,1,'ubuntu16\n'),('pollbill','',1499316602,97.25,0,'ubuntu16\n'),('poll','22',1499316903,1.00,1,'ubuntu16\n'),('poll','28',1499316902,1.77,1,'ubuntu16\n'),('pollbill','',1499316902,97.34,0,'ubuntu16\n'),('poll','22',1499317203,1.03,1,'ubuntu16\n'),('poll','28',1499317202,1.73,1,'ubuntu16\n'),('pollbill','',1499317203,97.21,0,'ubuntu16\n'),('poll','22',1499317502,1.00,1,'ubuntu16\n'),('poll','28',1499317502,1.41,1,'ubuntu16\n'),('pollbill','',1499317501,97.51,0,'ubuntu16\n'),('poll','22',1499317803,1.01,1,'ubuntu16\n'),('poll','28',1499317803,1.60,1,'ubuntu16\n'),('pollbill','',1499317803,97.11,0,'ubuntu16\n'),('poll','22',1499318103,1.08,1,'ubuntu16\n'),('poll','28',1499318103,1.46,1,'ubuntu16\n'),('pollbill','',1499318103,97.23,0,'ubuntu16\n'),('poll','22',1499318402,1.02,1,'ubuntu16\n'),('poll','28',1499318402,1.53,1,'ubuntu16\n'),('pollbill','',1499318402,97.12,0,'ubuntu16\n'),('poll','22',1499318704,1.00,1,'ubuntu16\n'),('poll','28',1499318704,1.43,1,'ubuntu16\n'),('pollbill','',1499318703,97.37,0,'ubuntu16\n'),('poll','22',1499319002,0.99,1,'ubuntu16\n'),('poll','28',1499319002,1.32,1,'ubuntu16\n'),('pollbill','',1499319002,97.16,0,'ubuntu16\n'),('poll','23',1499319447,1.39,1,'ubuntu16\n'),('poll','24',1499319452,1.42,1,'ubuntu16\n'),('poll','22',1499319601,1.02,1,'ubuntu16\n'),('poll','28',1499319601,1.69,1,'ubuntu16\n'),('pollbill','',1499319601,97.24,0,'ubuntu16\n'),('poll','22',1499319901,1.02,1,'ubuntu16\n'),('poll','28',1499319901,1.39,1,'ubuntu16\n'),('pollbill','',1499319901,97.29,0,'ubuntu16\n'),('poll','22',1499320202,1.00,1,'ubuntu16\n'),('poll','28',1499320202,1.62,1,'ubuntu16\n'),('pollbill','',1499320201,97.25,0,'ubuntu16\n'),('poll','22',1499320502,1.01,1,'ubuntu16\n'),('poll','28',1499320502,1.46,1,'ubuntu16\n'),('pollbill','',1499320502,97.26,0,'ubuntu16\n'),('poll','22',1499320802,1.03,1,'ubuntu16\n'),('poll','28',1499320802,2.86,1,'ubuntu16\n'),('pollbill','',1499320802,97.31,0,'ubuntu16\n'),('poll','22',1499321103,1.00,1,'ubuntu16\n'),('poll','28',1499321103,1.49,1,'ubuntu16\n'),('pollbill','',1499321102,97.19,0,'ubuntu16\n'),('poll','22',1499321402,1.00,1,'ubuntu16\n'),('poll','28',1499321402,1.35,1,'ubuntu16\n'),('pollbill','',1499321402,72.97,0,'ubuntu16\n'),('pollbill','',1499321702,10.73,0,'ubuntu16\n'),('poll','28',1499321706,9.36,1,'ubuntu16\n'),('poll','22',1499321705,9.93,1,'ubuntu16\n'),('pollbill','',1499321711,18.53,0,'ubuntu16-02\n'),('poll','25',1499321716,18.20,1,'ubuntu16-02\n'),('poll','24',1499321716,18.16,1,'ubuntu16-02\n'),('poll','23',1499321716,18.24,1,'ubuntu16-02\n'),('poll','27',1499321716,18.22,1,'ubuntu16-02\n'),('poll','22',1499322002,1.10,1,'ubuntu16\n'),('poll','24',1499322002,1.30,1,'ubuntu16-02\n'),('poll','23',1499322003,1.27,1,'ubuntu16-02\n'),('poll','25',1499322003,1.25,1,'ubuntu16-02\n'),('poll','28',1499322002,1.50,1,'ubuntu16\n'),('poll','27',1499322002,2.11,1,'ubuntu16-02\n'),('pollbill','',1499322002,73.18,0,'ubuntu16-02\n'),('pollbill','',1499322002,72.81,0,'ubuntu16\n'),('poll','22',1499322302,1.04,1,'ubuntu16\n'),('poll','23',1499322302,1.35,1,'ubuntu16-02\n'),('poll','28',1499322302,1.59,1,'ubuntu16\n'),('poll','25',1499322302,2.91,1,'ubuntu16-02\n'),('poll','27',1499322302,2.77,1,'ubuntu16-02\n'),('poll','24',1499322302,2.93,1,'ubuntu16-02\n'),('pollbill','',1499322301,73.22,0,'ubuntu16-02\n'),('pollbill','',1499322302,73.24,0,'ubuntu16\n'),('poll','22',1499322602,1.17,1,'ubuntu16\n'),('poll','23',1499322602,1.20,1,'ubuntu16-02\n'),('poll','28',1499322602,1.88,1,'ubuntu16\n'),('poll','24',1499322602,2.03,1,'ubuntu16-02\n'),('poll','25',1499322602,2.05,1,'ubuntu16-02\n'),('poll','27',1499322602,3.19,1,'ubuntu16-02\n'),('pollbill','',1499322601,48.92,0,'ubuntu16\n'),('pollbill','',1499322602,48.80,0,'ubuntu16-02\n'),('poll','23',1499322902,1.30,1,'ubuntu16-02\n'),('poll','22',1499322903,1.29,1,'ubuntu16\n'),('poll','28',1499322903,1.60,1,'ubuntu16\n'),('poll','25',1499322902,2.71,1,'ubuntu16-02\n'),('poll','24',1499322902,2.72,1,'ubuntu16-02\n'),('poll','27',1499322902,2.79,1,'ubuntu16-02\n'),('pollbill','',1499322902,49.26,0,'ubuntu16-02\n'),('pollbill','',1499322902,48.98,0,'ubuntu16\n'),('poll','23',1499323202,1.33,1,'ubuntu16-02\n'),('poll','22',1499323202,1.31,1,'ubuntu16\n'),('poll','28',1499323202,1.56,1,'ubuntu16\n'),('poll','25',1499323202,2.42,1,'ubuntu16-02\n'),('poll','24',1499323202,2.57,1,'ubuntu16-02\n'),('poll','27',1499323202,2.69,1,'ubuntu16-02\n'),('pollbill','',1499323201,48.87,0,'ubuntu16-02\n'),('pollbill','',1499323202,48.90,0,'ubuntu16\n'),('poll','22',1499323502,1.25,1,'ubuntu16\n'),('poll','28',1499323502,1.55,1,'ubuntu16\n'),('poll','23',1499323502,1.21,1,'ubuntu16-02\n'),('poll','25',1499323502,2.32,1,'ubuntu16-02\n'),('poll','24',1499323502,2.31,1,'ubuntu16-02\n'),('poll','27',1499323502,2.44,1,'ubuntu16-02\n'),('pollbill','',1499323501,40.75,0,'ubuntu16\n'),('pollbill','',1499323502,40.81,0,'ubuntu16-02\n'),('pollbill','',1499323802,0.83,0,'ubuntu16\n'),('pollbill','',1499323802,1.22,0,'ubuntu16-02\n'),('poll','22',1499323802,1.30,1,'ubuntu16\n'),('poll','28',1499323802,2.07,1,'ubuntu16\n'),('poll','25',1499323802,3.24,1,'ubuntu16-02\n'),('poll','24',1499323802,3.23,1,'ubuntu16-02\n'),('poll','23',1499323802,3.26,1,'ubuntu16-02\n'),('poll','27',1499323802,3.18,1,'ubuntu16-02\n'),('poll','28',1499324005,1.57,1,'ubuntu16\n'),('poll','22',1499324005,32.02,1,'ubuntu16\n'),('poll','28',1499324102,1.97,1,'ubuntu16\n'),('pollbill','',1499324102,2.64,0,'ubuntu16\n'),('poll','24',1499324102,2.93,1,'ubuntu16-02\n'),('poll','25',1499324102,2.93,1,'ubuntu16-02\n'),('poll','23',1499324102,3.03,1,'ubuntu16-02\n'),('poll','27',1499324102,3.25,1,'ubuntu16-02\n'),('pollbill','',1499324102,5.65,0,'ubuntu16-02\n'),('poll','22',1499324102,118.80,1,'ubuntu16\n'),('poll','23',1499324315,2.82,1,'ubuntu16-02\n'),('poll','24',1499324315,2.82,1,'ubuntu16-02\n'),('poll','25',1499324315,2.82,1,'ubuntu16-02\n'),('poll','27',1499324315,2.93,1,'ubuntu16-02\n'),('poll','28',1499324402,1.51,1,'ubuntu16\n'),('pollbill','',1499324402,1.76,0,'ubuntu16\n'),('pollbill','',1499324402,2.87,0,'ubuntu16-02\n'),('poll','25',1499324403,3.14,1,'ubuntu16-02\n'),('poll','24',1499324403,3.14,1,'ubuntu16-02\n'),('poll','23',1499324402,3.18,1,'ubuntu16-02\n'),('poll','27',1499324402,3.26,1,'ubuntu16-02\n'),('poll','22',1499324402,102.40,1,'ubuntu16\n'),('poll','28',1499324702,1.56,1,'ubuntu16\n'),('pollbill','',1499324702,2.59,0,'ubuntu16\n'),('pollbill','',1499324702,3.59,0,'ubuntu16-02\n'),('poll','24',1499324702,3.12,1,'ubuntu16-02\n'),('poll','25',1499324702,3.15,1,'ubuntu16-02\n'),('poll','23',1499324702,3.19,1,'ubuntu16-02\n'),('poll','27',1499324702,3.37,1,'ubuntu16-02\n'),('poll','22',1499324702,98.57,1,'ubuntu16\n'),('poll','22',1499325002,1.14,1,'ubuntu16\n'),('poll','28',1499325002,1.52,1,'ubuntu16\n'),('poll','24',1499325002,3.37,1,'ubuntu16-02\n'),('pollbill','',1499325001,3.93,0,'ubuntu16-02\n'),('poll','25',1499325002,3.52,1,'ubuntu16-02\n'),('poll','27',1499325002,3.62,1,'ubuntu16-02\n'),('poll','23',1499325002,3.53,1,'ubuntu16-02\n'),('pollbill','',1499325002,3.65,0,'ubuntu16\n'),('poll','22',1499325302,1.02,1,'ubuntu16\n'),('poll','28',1499325302,1.36,1,'ubuntu16\n'),('pollbill','',1499325301,2.15,0,'ubuntu16\n'),('poll','25',1499325302,2.85,1,'ubuntu16-02\n'),('poll','24',1499325302,2.84,1,'ubuntu16-02\n'),('poll','23',1499325302,2.91,1,'ubuntu16-02\n'),('poll','27',1499325302,2.99,1,'ubuntu16-02\n'),('pollbill','',1499325302,7.67,0,'ubuntu16-02\n'),('poll','28',1499325601,1.61,1,'ubuntu16\n'),('pollbill','',1499325602,1.77,0,'ubuntu16-02\n'),('pollbill','',1499325601,2.39,0,'ubuntu16\n'),('poll','25',1499325602,3.20,1,'ubuntu16-02\n'),('poll','24',1499325602,3.23,1,'ubuntu16-02\n'),('poll','23',1499325602,3.29,1,'ubuntu16-02\n'),('poll','27',1499325602,3.36,1,'ubuntu16-02\n'),('poll','22',1499325601,89.60,1,'ubuntu16\n'),('poll','28',1499325903,1.49,1,'ubuntu16\n'),('pollbill','',1499325902,2.62,0,'ubuntu16\n'),('pollbill','',1499325902,2.99,0,'ubuntu16-02\n'),('poll','25',1499325903,3.18,1,'ubuntu16-02\n'),('poll','24',1499325903,3.19,1,'ubuntu16-02\n'),('poll','23',1499325903,3.28,1,'ubuntu16-02\n'),('poll','27',1499325903,3.34,1,'ubuntu16-02\n'),('poll','22',1499325903,93.28,1,'ubuntu16\n'),('poll','22',1499326202,1.05,1,'ubuntu16\n'),('poll','28',1499326202,1.44,1,'ubuntu16\n'),('pollbill','',1499326201,2.72,0,'ubuntu16\n'),('pollbill','',1499326202,2.85,0,'ubuntu16-02\n'),('poll','25',1499326202,3.19,1,'ubuntu16-02\n'),('poll','24',1499326202,3.23,1,'ubuntu16-02\n'),('poll','23',1499326202,3.29,1,'ubuntu16-02\n'),('poll','27',1499326202,3.43,1,'ubuntu16-02\n'),('poll','22',1499326502,1.03,1,'ubuntu16\n'),('poll','28',1499326502,1.64,1,'ubuntu16\n'),('pollbill','',1499326501,2.70,0,'ubuntu16\n'),('poll','24',1499326502,3.18,1,'ubuntu16-02\n'),('poll','25',1499326502,3.20,1,'ubuntu16-02\n'),('poll','23',1499326502,3.31,1,'ubuntu16-02\n'),('poll','27',1499326502,3.40,1,'ubuntu16-02\n'),('pollbill','',1499326502,3.87,0,'ubuntu16-02\n'),('poll','22',1499326802,1.01,1,'ubuntu16\n'),('poll','28',1499326802,1.71,1,'ubuntu16\n'),('pollbill','',1499326802,3.08,0,'ubuntu16-02\n'),('poll','25',1499326802,3.27,1,'ubuntu16-02\n'),('poll','24',1499326802,3.33,1,'ubuntu16-02\n'),('poll','23',1499326802,3.40,1,'ubuntu16-02\n'),('poll','27',1499326802,3.49,1,'ubuntu16-02\n'),('pollbill','',1499326802,5.69,0,'ubuntu16\n'),('poll','28',1499327102,1.59,1,'ubuntu16\n'),('pollbill','',1499327102,2.15,0,'ubuntu16-02\n'),('poll','23',1499327102,3.83,1,'ubuntu16-02\n'),('poll','25',1499327102,3.98,1,'ubuntu16-02\n'),('poll','24',1499327102,4.01,1,'ubuntu16-02\n'),('poll','27',1499327102,4.04,1,'ubuntu16-02\n'),('pollbill','',1499327101,5.03,0,'ubuntu16\n'),('poll','22',1499327102,91.43,1,'ubuntu16\n'),('pollbill','',1499327401,1.64,0,'ubuntu16\n'),('poll','28',1499327402,1.43,1,'ubuntu16\n'),('poll','25',1499327402,2.81,1,'ubuntu16-02\n'),('poll','23',1499327402,2.78,1,'ubuntu16-02\n'),('poll','24',1499327402,2.80,1,'ubuntu16-02\n'),('poll','27',1499327402,2.96,1,'ubuntu16-02\n'),('pollbill','',1499327402,7.70,0,'ubuntu16-02\n'),('poll','22',1499327402,112.40,1,'ubuntu16\n'),('poll','28',1499327702,1.77,1,'ubuntu16\n'),('pollbill','',1499327702,2.79,0,'ubuntu16\n'),('pollbill','',1499327702,4.11,0,'ubuntu16-02\n'),('poll','25',1499327703,3.80,1,'ubuntu16-02\n'),('poll','24',1499327703,3.80,1,'ubuntu16-02\n'),('poll','23',1499327703,3.88,1,'ubuntu16-02\n'),('poll','27',1499327703,3.75,1,'ubuntu16-02\n'),('poll','22',1499327702,82.87,1,'ubuntu16\n'),('pollbill','',1499328001,1.97,0,'ubuntu16-02\n'),('pollbill','',1499328002,1.65,0,'ubuntu16\n'),('poll','28',1499328002,1.64,1,'ubuntu16\n'),('poll','25',1499328002,3.34,1,'ubuntu16-02\n'),('poll','24',1499328002,3.57,1,'ubuntu16-02\n'),('poll','23',1499328002,3.50,1,'ubuntu16-02\n'),('poll','27',1499328002,3.64,1,'ubuntu16-02\n'),('poll','22',1499328002,92.36,1,'ubuntu16\n'),('poll','28',1499328302,1.50,1,'ubuntu16\n'),('poll','23',1499328303,2.62,1,'ubuntu16-02\n'),('poll','25',1499328303,2.64,1,'ubuntu16-02\n'),('poll','24',1499328303,2.66,1,'ubuntu16-02\n'),('pollbill','',1499328302,3.63,0,'ubuntu16\n'),('poll','27',1499328303,3.47,1,'ubuntu16-02\n'),('pollbill','',1499328302,4.73,0,'ubuntu16-02\n'),('poll','22',1499328302,7.77,1,'ubuntu16\n'),('poll','22',1499328602,1.09,1,'ubuntu16\n'),('poll','28',1499328602,1.97,1,'ubuntu16\n'),('pollbill','',1499328601,3.99,0,'ubuntu16-02\n'),('poll','25',1499328602,3.94,1,'ubuntu16-02\n'),('poll','24',1499328602,3.95,1,'ubuntu16-02\n'),('poll','23',1499328602,4.02,1,'ubuntu16-02\n'),('poll','27',1499328602,4.13,1,'ubuntu16-02\n'),('pollbill','',1499328602,3.82,0,'ubuntu16\n'),('poll','22',1499328902,1.03,1,'ubuntu16\n'),('pollbill','',1499328902,1.96,0,'ubuntu16-02\n'),('poll','28',1499328902,1.93,1,'ubuntu16\n'),('poll','25',1499328902,3.08,1,'ubuntu16-02\n'),('poll','24',1499328902,3.08,1,'ubuntu16-02\n'),('poll','23',1499328902,3.08,1,'ubuntu16-02\n'),('poll','27',1499328902,3.38,1,'ubuntu16-02\n'),('pollbill','',1499328902,12.35,0,'ubuntu16\n'),('pollbill','',1499329202,1.76,0,'ubuntu16\n'),('poll','28',1499329202,1.74,1,'ubuntu16\n'),('poll','23',1499329202,3.82,1,'ubuntu16-02\n'),('poll','24',1499329202,3.76,1,'ubuntu16-02\n'),('poll','25',1499329202,3.81,1,'ubuntu16-02\n'),('poll','27',1499329202,4.02,1,'ubuntu16-02\n'),('pollbill','',1499329201,5.67,0,'ubuntu16-02\n'),('poll','22',1499329202,86.54,1,'ubuntu16\n'),('poll','28',1499329502,1.62,1,'ubuntu16\n'),('poll','24',1499329503,3.27,1,'ubuntu16-02\n'),('poll','25',1499329503,3.24,1,'ubuntu16-02\n'),('poll','23',1499329503,3.24,1,'ubuntu16-02\n'),('poll','27',1499329503,3.37,1,'ubuntu16-02\n'),('pollbill','',1499329502,4.83,0,'ubuntu16-02\n'),('pollbill','',1499329502,6.90,0,'ubuntu16\n'),('poll','22',1499329502,116.30,1,'ubuntu16\n'),('poll','22',1499329803,1.04,1,'ubuntu16\n'),('poll','28',1499329803,1.43,1,'ubuntu16\n'),('poll','24',1499329801,3.17,1,'ubuntu16-02\n'),('poll','25',1499329801,3.23,1,'ubuntu16-02\n'),('poll','23',1499329802,3.27,1,'ubuntu16-02\n'),('poll','27',1499329801,3.39,1,'ubuntu16-02\n'),('pollbill','',1499329801,5.61,0,'ubuntu16-02\n'),('pollbill','',1499329802,5.66,0,'ubuntu16\n'),('poll','22',1499330102,1.04,1,'ubuntu16\n'),('poll','28',1499330102,1.52,1,'ubuntu16\n'),('poll','23',1499330102,2.96,1,'ubuntu16-02\n'),('poll','24',1499330102,2.97,1,'ubuntu16-02\n'),('poll','25',1499330102,2.98,1,'ubuntu16-02\n'),('poll','27',1499330102,2.98,1,'ubuntu16-02\n'),('pollbill','',1499330102,5.77,0,'ubuntu16\n'),('pollbill','',1499330102,6.65,0,'ubuntu16-02\n'),('poll','22',1499330402,1.01,1,'ubuntu16\n'),('poll','28',1499330402,1.82,1,'ubuntu16\n'),('poll','25',1499330402,2.91,1,'ubuntu16-02\n'),('poll','24',1499330402,2.92,1,'ubuntu16-02\n'),('poll','23',1499330402,2.94,1,'ubuntu16-02\n'),('poll','27',1499330402,3.04,1,'ubuntu16-02\n'),('pollbill','',1499330402,5.82,0,'ubuntu16\n'),('pollbill','',1499330402,5.64,0,'ubuntu16-02\n'),('poll','28',1499330702,1.48,1,'ubuntu16\n'),('pollbill','',1499330702,2.62,0,'ubuntu16\n'),('poll','25',1499330703,2.97,1,'ubuntu16-02\n'),('poll','24',1499330703,2.98,1,'ubuntu16-02\n'),('poll','23',1499330703,3.05,1,'ubuntu16-02\n'),('poll','27',1499330703,3.19,1,'ubuntu16-02\n'),('pollbill','',1499330702,3.77,0,'ubuntu16-02\n'),('poll','22',1499330702,129.20,1,'ubuntu16\n'),('pollbill','',1499331001,1.33,0,'ubuntu16\n'),('poll','28',1499331002,1.67,1,'ubuntu16\n'),('pollbill','',1499331002,1.59,0,'ubuntu16-02\n'),('poll','25',1499331002,3.14,1,'ubuntu16-02\n'),('poll','23',1499331002,3.17,1,'ubuntu16-02\n'),('poll','24',1499331002,3.17,1,'ubuntu16-02\n'),('poll','27',1499331002,3.28,1,'ubuntu16-02\n'),('poll','22',1499331002,98.58,1,'ubuntu16\n'),('poll','22',1499331302,1.01,1,'ubuntu16\n'),('poll','28',1499331302,1.50,1,'ubuntu16\n'),('pollbill','',1499331301,1.74,0,'ubuntu16\n'),('pollbill','',1499331302,1.81,0,'ubuntu16-02\n'),('poll','24',1499331302,3.18,1,'ubuntu16-02\n'),('poll','23',1499331302,3.16,1,'ubuntu16-02\n'),('poll','25',1499331302,3.19,1,'ubuntu16-02\n'),('poll','27',1499331302,3.23,1,'ubuntu16-02\n'),('poll','28',1499331602,2.01,1,'ubuntu16\n'),('pollbill','',1499331602,2.66,0,'ubuntu16\n'),('pollbill','',1499331602,2.82,0,'ubuntu16-02\n'),('poll','24',1499331602,3.49,1,'ubuntu16-02\n'),('poll','23',1499331602,3.58,1,'ubuntu16-02\n'),('poll','25',1499331602,3.50,1,'ubuntu16-02\n'),('poll','27',1499331602,3.65,1,'ubuntu16-02\n'),('poll','22',1499331602,64.37,1,'ubuntu16\n'),('pollbill','',1499331901,1.26,0,'ubuntu16-02\n'),('pollbill','',1499331902,1.31,0,'ubuntu16\n'),('poll','28',1499331902,1.54,1,'ubuntu16\n'),('poll','25',1499331902,2.98,1,'ubuntu16-02\n'),('poll','24',1499331902,3.11,1,'ubuntu16-02\n'),('poll','23',1499331902,3.16,1,'ubuntu16-02\n'),('poll','27',1499331902,3.23,1,'ubuntu16-02\n'),('poll','22',1499331902,124.80,1,'ubuntu16\n'),('poll','28',1499332202,1.58,1,'ubuntu16\n'),('poll','23',1499332202,3.02,1,'ubuntu16-02\n'),('poll','24',1499332202,3.00,1,'ubuntu16-02\n'),('poll','25',1499332202,2.99,1,'ubuntu16-02\n'),('poll','27',1499332202,3.50,1,'ubuntu16-02\n'),('pollbill','',1499332202,3.71,0,'ubuntu16\n'),('pollbill','',1499332201,4.68,0,'ubuntu16-02\n'),('poll','22',1499332202,91.90,1,'ubuntu16\n'),('poll','22',1499332503,1.10,1,'ubuntu16\n'),('poll','28',1499332503,1.84,1,'ubuntu16\n'),('pollbill','',1499332502,2.99,0,'ubuntu16-02\n'),('poll','24',1499332503,3.28,1,'ubuntu16-02\n'),('poll','25',1499332503,3.24,1,'ubuntu16-02\n'),('poll','23',1499332503,3.34,1,'ubuntu16-02\n'),('poll','27',1499332503,3.42,1,'ubuntu16-02\n'),('pollbill','',1499332502,8.02,0,'ubuntu16\n'),('pollbill','',1499332802,1.78,0,'ubuntu16\n'),('poll','28',1499332802,1.93,1,'ubuntu16\n'),('poll','25',1499332803,3.07,1,'ubuntu16-02\n'),('poll','24',1499332803,3.06,1,'ubuntu16-02\n'),('poll','23',1499332803,3.14,1,'ubuntu16-02\n'),('poll','27',1499332803,3.28,1,'ubuntu16-02\n'),('pollbill','',1499332802,7.40,0,'ubuntu16-02\n'),('poll','22',1499332802,110.50,1,'ubuntu16\n'),('poll','28',1499333103,1.58,1,'ubuntu16\n'),('poll','24',1499333102,2.85,1,'ubuntu16-02\n'),('pollbill','',1499333102,2.68,0,'ubuntu16\n'),('poll','25',1499333102,3.43,1,'ubuntu16-02\n'),('poll','23',1499333102,3.43,1,'ubuntu16-02\n'),('pollbill','',1499333102,3.92,0,'ubuntu16-02\n'),('poll','27',1499333102,3.58,1,'ubuntu16-02\n'),('poll','22',1499333103,130.40,1,'ubuntu16\n'),('poll','22',1499333402,1.28,1,'ubuntu16\n'),('poll','28',1499333402,1.92,1,'ubuntu16\n'),('poll','24',1499333402,3.48,1,'ubuntu16-02\n'),('poll','27',1499333402,3.46,1,'ubuntu16-02\n'),('poll','25',1499333402,3.48,1,'ubuntu16-02\n'),('poll','23',1499333402,3.44,1,'ubuntu16-02\n'),('pollbill','',1499333402,5.06,0,'ubuntu16-02\n'),('pollbill','',1499333402,4.73,0,'ubuntu16\n'),('poll','22',1499333702,1.20,1,'ubuntu16\n'),('poll','28',1499333702,1.68,1,'ubuntu16\n'),('pollbill','',1499333702,1.92,0,'ubuntu16\n'),('pollbill','',1499333702,2.06,0,'ubuntu16-02\n'),('poll','25',1499333702,3.22,1,'ubuntu16-02\n'),('poll','24',1499333702,3.25,1,'ubuntu16-02\n'),('poll','23',1499333702,3.38,1,'ubuntu16-02\n'),('poll','27',1499333702,3.37,1,'ubuntu16-02\n'),('poll','28',1499334002,2.18,1,'ubuntu16\n'),('poll','25',1499334002,2.98,1,'ubuntu16-02\n'),('poll','24',1499334002,3.00,1,'ubuntu16-02\n'),('poll','23',1499334002,3.04,1,'ubuntu16-02\n'),('poll','27',1499334002,3.15,1,'ubuntu16-02\n'),('pollbill','',1499334002,3.72,0,'ubuntu16\n'),('pollbill','',1499334002,3.81,0,'ubuntu16-02\n'),('poll','22',1499334002,101.60,1,'ubuntu16\n'),('poll','28',1499334302,1.73,1,'ubuntu16\n'),('poll','27',1499334302,2.81,1,'ubuntu16-02\n'),('poll','24',1499334302,2.90,1,'ubuntu16-02\n'),('poll','23',1499334302,2.96,1,'ubuntu16-02\n'),('poll','25',1499334302,2.99,1,'ubuntu16-02\n'),('pollbill','',1499334301,4.68,0,'ubuntu16-02\n'),('pollbill','',1499334302,4.70,0,'ubuntu16\n'),('poll','22',1499334302,11.79,1,'ubuntu16\n'),('poll','28',1499334602,1.61,1,'ubuntu16\n'),('poll','23',1499334602,3.05,1,'ubuntu16-02\n'),('poll','24',1499334603,3.04,1,'ubuntu16-02\n'),('poll','25',1499334603,3.02,1,'ubuntu16-02\n'),('poll','27',1499334603,3.17,1,'ubuntu16-02\n'),('pollbill','',1499334602,4.78,0,'ubuntu16-02\n'),('pollbill','',1499334602,4.51,0,'ubuntu16\n'),('poll','22',1499334602,114.30,1,'ubuntu16\n'),('poll','22',1499334903,1.11,1,'ubuntu16\n'),('poll','28',1499334903,2.60,1,'ubuntu16\n'),('pollbill','',1499334902,97.28,0,'ubuntu16\n'),('poll','22',1499335203,1.00,1,'ubuntu16\n'),('poll','28',1499335203,1.39,1,'ubuntu16\n'),('pollbill','',1499335203,97.18,0,'ubuntu16\n'),('poll','22',1499335502,1.00,1,'ubuntu16\n'),('poll','28',1499335502,1.39,1,'ubuntu16\n'),('pollbill','',1499335502,97.06,0,'ubuntu16\n'),('poll','22',1499335803,1.03,1,'ubuntu16\n'),('poll','28',1499335803,1.36,1,'ubuntu16\n'),('pollbill','',1499335803,97.63,0,'ubuntu16\n'),('poll','22',1499340302,1.01,1,'ubuntu16\n'),('poll','28',1499340302,1.68,1,'ubuntu16\n'),('pollbill','',1499340302,94.17,0,'ubuntu16\n'),('poll','22',1499340602,1.06,1,'ubuntu16\n'),('poll','28',1499340602,1.73,1,'ubuntu16\n'),('pollbill','',1499340602,97.70,0,'ubuntu16\n'),('poll','22',1499340902,1.02,1,'ubuntu16\n'),('poll','28',1499340902,1.63,1,'ubuntu16\n'),('pollbill','',1499340902,97.74,0,'ubuntu16\n'),('poll','22',1499341203,1.06,1,'ubuntu16\n'),('poll','28',1499341203,1.73,1,'ubuntu16\n'),('pollbill','',1499341202,97.54,0,'ubuntu16\n'),('poll','22',1499341502,1.04,1,'ubuntu16\n'),('poll','28',1499341502,3.89,1,'ubuntu16\n'),('pollbill','',1499341502,97.64,0,'ubuntu16\n'),('poll','22',1499341803,1.05,1,'ubuntu16\n'),('poll','28',1499341803,3.91,1,'ubuntu16\n'),('pollbill','',1499341802,97.83,0,'ubuntu16\n'),('poll','22',1499342102,1.01,1,'ubuntu16\n'),('poll','28',1499342102,1.54,1,'ubuntu16\n'),('pollbill','',1499342102,97.79,0,'ubuntu16\n'),('poll','22',1499342402,1.07,1,'ubuntu16\n'),('poll','28',1499342402,1.80,1,'ubuntu16\n'),('pollbill','',1499342402,97.81,0,'ubuntu16\n'),('poll','22',1499342702,1.00,1,'ubuntu16\n'),('poll','28',1499342702,1.51,1,'ubuntu16\n'),('pollbill','',1499342702,97.83,0,'ubuntu16\n'),('poll','22',1499343002,1.14,1,'ubuntu16\n'),('poll','28',1499343002,1.72,1,'ubuntu16\n'),('pollbill','',1499343001,97.82,0,'ubuntu16\n'),('poll','22',1499343303,1.01,1,'ubuntu16\n'),('poll','28',1499343303,1.61,1,'ubuntu16\n'),('pollbill','',1499343302,97.27,0,'ubuntu16\n'),('poll','22',1499343602,1.05,1,'ubuntu16\n'),('poll','28',1499343602,1.30,1,'ubuntu16\n'),('pollbill','',1499343602,97.20,0,'ubuntu16\n'),('poll','22',1499343902,1.04,1,'ubuntu16\n'),('poll','28',1499343902,1.28,1,'ubuntu16\n'),('pollbill','',1499343902,97.09,0,'ubuntu16\n'),('poll','22',1499344202,1.03,1,'ubuntu16\n'),('poll','28',1499344202,1.81,1,'ubuntu16\n'),('pollbill','',1499344202,97.35,0,'ubuntu16\n'),('poll','22',1499344502,1.04,1,'ubuntu16\n'),('poll','28',1499344502,1.32,1,'ubuntu16\n'),('pollbill','',1499344502,97.04,0,'ubuntu16\n'),('poll','22',1499344802,1.03,1,'ubuntu16\n'),('poll','28',1499344802,1.47,1,'ubuntu16\n'),('pollbill','',1499344801,97.07,0,'ubuntu16\n'),('poll','22',1499345102,1.06,1,'ubuntu16\n'),('poll','28',1499345102,1.41,1,'ubuntu16\n'),('pollbill','',1499345102,97.06,0,'ubuntu16\n'),('poll','22',1499345402,1.04,1,'ubuntu16\n'),('poll','28',1499345402,1.28,1,'ubuntu16\n'),('pollbill','',1499345402,97.08,0,'ubuntu16\n'),('poll','22',1499345702,1.04,1,'ubuntu16\n'),('poll','28',1499345702,1.61,1,'ubuntu16\n'),('pollbill','',1499345701,97.12,0,'ubuntu16\n'),('poll','22',1499346002,1.02,1,'ubuntu16\n'),('poll','28',1499346002,1.29,1,'ubuntu16\n'),('pollbill','',1499346002,97.09,0,'ubuntu16\n'),('poll','22',1499346302,1.00,1,'ubuntu16\n'),('poll','28',1499346302,1.22,1,'ubuntu16\n'),('pollbill','',1499346302,97.06,0,'ubuntu16\n'),('poll','22',1499346602,1.01,1,'ubuntu16\n'),('poll','28',1499346602,1.51,1,'ubuntu16\n'),('pollbill','',1499346602,96.90,0,'ubuntu16\n'),('poll','22',1499346903,1.03,1,'ubuntu16\n'),('poll','28',1499346903,1.27,1,'ubuntu16\n'),('pollbill','',1499346902,97.05,0,'ubuntu16\n'),('poll','22',1499347202,1.04,1,'ubuntu16\n'),('poll','28',1499347202,1.63,1,'ubuntu16\n'),('pollbill','',1499347202,97.05,0,'ubuntu16\n'),('poll','22',1499347502,1.01,1,'ubuntu16\n'),('poll','28',1499347502,1.77,1,'ubuntu16\n'),('pollbill','',1499347502,97.13,0,'ubuntu16\n'),('poll','22',1499347802,1.02,1,'ubuntu16\n'),('poll','28',1499347802,1.66,1,'ubuntu16\n'),('pollbill','',1499347801,97.05,0,'ubuntu16\n'),('poll','22',1499348102,1.02,1,'ubuntu16\n'),('poll','28',1499348102,1.46,1,'ubuntu16\n'),('pollbill','',1499348102,96.98,0,'ubuntu16\n'),('poll','22',1499348402,1.20,1,'ubuntu16\n'),('poll','28',1499348402,1.66,1,'ubuntu16\n'),('pollbill','',1499348401,97.47,0,'ubuntu16\n'),('poll','22',1499348702,1.04,1,'ubuntu16\n'),('poll','28',1499348702,1.46,1,'ubuntu16\n'),('pollbill','',1499348702,97.15,0,'ubuntu16\n'),('poll','22',1499349002,0.99,1,'ubuntu16\n'),('poll','28',1499349002,1.32,1,'ubuntu16\n'),('pollbill','',1499349001,97.22,0,'ubuntu16\n'),('poll','22',1499349303,1.02,1,'ubuntu16\n'),('poll','28',1499349303,1.27,1,'ubuntu16\n'),('pollbill','',1499349303,96.97,0,'ubuntu16\n'),('poll','22',1499349602,1.03,1,'ubuntu16\n'),('poll','28',1499349602,1.18,1,'ubuntu16\n'),('pollbill','',1499349601,97.42,0,'ubuntu16\n'),('poll','22',1499349902,1.01,1,'ubuntu16\n'),('poll','28',1499349902,1.24,1,'ubuntu16\n'),('pollbill','',1499349902,97.44,0,'ubuntu16\n'),('poll','22',1499350202,1.02,1,'ubuntu16\n'),('poll','28',1499350202,1.41,1,'ubuntu16\n'),('pollbill','',1499350201,97.55,0,'ubuntu16\n'),('poll','22',1499350502,1.00,1,'ubuntu16\n'),('poll','28',1499350502,1.15,1,'ubuntu16\n'),('pollbill','',1499350502,97.22,0,'ubuntu16\n'),('poll','22',1499350802,1.04,1,'ubuntu16\n'),('poll','28',1499350802,1.20,1,'ubuntu16\n'),('pollbill','',1499350802,97.32,0,'ubuntu16\n'),('poll','22',1499390707,1.00,1,'ubuntu16\n'),('poll','28',1499390707,1.53,1,'ubuntu16\n'),('pollbill','',1499390703,101.40,0,'ubuntu16\n'),('poll','22',1499391003,1.05,1,'ubuntu16\n'),('poll','28',1499391003,1.67,1,'ubuntu16\n'),('pollbill','',1499391003,97.81,0,'ubuntu16\n'),('poll','22',1499391302,1.08,1,'ubuntu16\n'),('poll','28',1499391302,1.64,1,'ubuntu16\n'),('pollbill','',1499391302,97.79,0,'ubuntu16\n'),('poll','22',1499391602,3.07,1,'ubuntu16\n'),('poll','28',1499391602,3.98,1,'ubuntu16\n'),('pollbill','',1499391602,106.30,0,'ubuntu16\n'),('poll','22',1499391904,1.07,1,'ubuntu16\n'),('poll','28',1499391904,2.38,1,'ubuntu16\n'),('pollbill','',1499391903,99.78,0,'ubuntu16\n'),('poll','22',1499392202,1.16,1,'ubuntu16\n'),('poll','28',1499392202,2.39,1,'ubuntu16\n'),('pollbill','',1499392202,97.85,0,'ubuntu16\n'),('poll','22',1499392502,1.12,1,'ubuntu16\n'),('poll','28',1499392502,2.04,1,'ubuntu16\n'),('pollbill','',1499392501,97.56,0,'ubuntu16\n'),('poll','22',1499392806,1.34,1,'ubuntu16\n'),('poll','28',1499392806,2.35,1,'ubuntu16\n'),('pollbill','',1499392807,97.87,0,'ubuntu16\n'),('poll','22',1499393102,1.05,1,'ubuntu16\n'),('poll','28',1499393102,1.84,1,'ubuntu16\n'),('pollbill','',1499393102,97.76,0,'ubuntu16\n'),('poll','22',1499393402,1.03,1,'ubuntu16\n'),('poll','28',1499393402,3.99,1,'ubuntu16\n'),('pollbill','',1499393402,97.83,0,'ubuntu16\n'),('poll','22',1499393703,1.01,1,'ubuntu16\n'),('poll','28',1499393703,2.82,1,'ubuntu16\n'),('pollbill','',1499393702,98.29,0,'ubuntu16\n'),('poll','22',1499394003,1.04,1,'ubuntu16\n'),('poll','28',1499394003,3.17,1,'ubuntu16\n'),('pollbill','',1499394002,97.27,0,'ubuntu16\n'),('poll','22',1499394302,1.04,1,'ubuntu16\n'),('poll','28',1499394302,2.09,1,'ubuntu16\n'),('pollbill','',1499394301,97.87,0,'ubuntu16\n'),('poll','22',1499394602,1.04,1,'ubuntu16\n'),('poll','28',1499394602,1.64,1,'ubuntu16\n'),('pollbill','',1499394602,103.10,0,'ubuntu16\n'),('pollbill','',1499394902,0.87,0,'ubuntu16\n'),('poll','22',1499394902,2.56,1,'ubuntu16\n'),('poll','28',1499394902,3.09,1,'ubuntu16\n'),('pollbill','',1499394907,13.43,0,'ubuntu16-02\n'),('poll','23',1499394910,12.97,1,'ubuntu16-02\n'),('poll','27',1499394910,13.07,1,'ubuntu16-02\n'),('poll','24',1499394910,13.20,1,'ubuntu16-02\n'),('poll','25',1499394910,13.25,1,'ubuntu16-02\n'),('pollbill','',1499395202,1.16,0,'ubuntu16\n'),('pollbill','',1499395202,1.58,0,'ubuntu16-02\n'),('poll','22',1499395203,1.87,1,'ubuntu16\n'),('poll','28',1499395202,1.99,1,'ubuntu16\n'),('poll','23',1499395203,3.72,1,'ubuntu16-02\n'),('poll','25',1499395203,3.88,1,'ubuntu16-02\n'),('poll','24',1499395203,3.94,1,'ubuntu16-02\n'),('poll','27',1499395203,3.90,1,'ubuntu16-02\n'),('pollbill','',1499395505,1.28,0,'ubuntu16\n'),('pollbill','',1499395502,4.51,0,'ubuntu16-02\n'),('poll','22',1499395505,2.32,1,'ubuntu16\n'),('poll','28',1499395505,2.62,1,'ubuntu16\n'),('poll','23',1499395504,5.11,1,'ubuntu16-02\n'),('poll','24',1499395504,5.14,1,'ubuntu16-02\n'),('poll','27',1499395504,5.00,1,'ubuntu16-02\n'),('poll','25',1499395505,4.34,1,'ubuntu16-02\n'),('pollbill','',1499395803,0.98,0,'ubuntu16\n'),('pollbill','',1499395802,1.64,0,'ubuntu16-02\n'),('poll','22',1499395803,1.67,1,'ubuntu16\n'),('poll','28',1499395803,2.03,1,'ubuntu16\n'),('poll','25',1499395803,4.10,1,'ubuntu16-02\n'),('poll','24',1499395803,4.10,1,'ubuntu16-02\n'),('poll','23',1499395803,4.04,1,'ubuntu16-02\n'),('poll','27',1499395803,4.11,1,'ubuntu16-02\n'),('pollbill','',1499396102,0.95,0,'ubuntu16\n'),('pollbill','',1499396102,1.75,0,'ubuntu16-02\n'),('poll','22',1499396102,1.81,1,'ubuntu16\n'),('poll','28',1499396102,2.13,1,'ubuntu16\n'),('poll','24',1499396103,3.23,1,'ubuntu16-02\n'),('poll','25',1499396103,3.58,1,'ubuntu16-02\n'),('poll','23',1499396103,3.56,1,'ubuntu16-02\n'),('poll','27',1499396103,3.95,1,'ubuntu16-02\n'),('pollbill','',1499396402,0.96,0,'ubuntu16\n'),('pollbill','',1499396402,1.84,0,'ubuntu16-02\n'),('poll','22',1499396403,1.83,1,'ubuntu16\n'),('poll','28',1499396403,1.93,1,'ubuntu16\n'),('poll','23',1499396403,3.92,1,'ubuntu16-02\n'),('poll','24',1499396403,3.92,1,'ubuntu16-02\n'),('poll','25',1499396403,3.90,1,'ubuntu16-02\n'),('poll','27',1499396403,4.08,1,'ubuntu16-02\n'),('pollbill','',1499396701,2.37,0,'ubuntu16-02\n'),('pollbill','',1499396702,1.62,0,'ubuntu16\n'),('poll','22',1499396703,2.06,1,'ubuntu16\n'),('poll','28',1499396703,2.77,1,'ubuntu16\n'),('poll','24',1499396702,4.30,1,'ubuntu16-02\n'),('poll','25',1499396702,4.29,1,'ubuntu16-02\n'),('poll','27',1499396702,4.44,1,'ubuntu16-02\n'),('poll','23',1499396702,4.32,1,'ubuntu16-02\n'),('pollbill','',1499397001,1.04,0,'ubuntu16\n'),('pollbill','',1499397001,1.30,0,'ubuntu16-02\n'),('poll','22',1499397002,1.51,1,'ubuntu16\n'),('poll','28',1499397002,2.33,1,'ubuntu16\n'),('poll','25',1499397002,3.02,1,'ubuntu16-02\n'),('poll','24',1499397002,3.09,1,'ubuntu16-02\n'),('poll','23',1499397002,3.17,1,'ubuntu16-02\n'),('poll','27',1499397002,3.86,1,'ubuntu16-02\n'),('poll','28',1499397302,1.96,1,'ubuntu16\n'),('pollbill','',1499397302,3.36,0,'ubuntu16\n'),('pollbill','',1499397302,4.44,0,'ubuntu16-02\n'),('poll','25',1499397302,4.30,1,'ubuntu16-02\n'),('poll','24',1499397302,4.40,1,'ubuntu16-02\n'),('poll','23',1499397302,4.53,1,'ubuntu16-02\n'),('poll','27',1499397302,4.57,1,'ubuntu16-02\n'),('poll','22',1499397302,90.12,1,'ubuntu16\n'),('pollbill','',1499397602,2.40,0,'ubuntu16-02\n'),('poll','22',1499397603,1.16,1,'ubuntu16\n'),('poll','28',1499397603,2.23,1,'ubuntu16\n'),('poll','23',1499397602,4.07,1,'ubuntu16-02\n'),('poll','24',1499397602,4.12,1,'ubuntu16-02\n'),('poll','25',1499397602,4.14,1,'ubuntu16-02\n'),('poll','27',1499397602,4.38,1,'ubuntu16-02\n'),('pollbill','',1499397602,5.86,0,'ubuntu16\n'),('pollbill','',1499397901,2.98,0,'ubuntu16-02\n'),('pollbill','',1499397903,1.79,0,'ubuntu16\n'),('poll','28',1499397903,1.87,1,'ubuntu16\n'),('poll','23',1499397902,4.03,1,'ubuntu16-02\n'),('poll','24',1499397902,4.09,1,'ubuntu16-02\n'),('poll','27',1499397902,4.10,1,'ubuntu16-02\n'),('poll','25',1499397902,4.13,1,'ubuntu16-02\n'),('poll','22',1499397903,96.90,1,'ubuntu16\n'),('pollbill','',1499398201,3.16,0,'ubuntu16-02\n'),('poll','22',1499398204,1.24,1,'ubuntu16\n'),('poll','23',1499398202,3.52,1,'ubuntu16-02\n'),('poll','25',1499398202,3.85,1,'ubuntu16-02\n'),('poll','27',1499398202,4.06,1,'ubuntu16-02\n'),('poll','24',1499398202,4.29,1,'ubuntu16-02\n'),('poll','28',1499398204,2.16,1,'ubuntu16\n'),('pollbill','',1499398203,5.06,0,'ubuntu16\n'),('pollbill','',1499398502,2.53,0,'ubuntu16-02\n'),('poll','28',1499398503,1.59,1,'ubuntu16\n'),('pollbill','',1499398503,2.20,0,'ubuntu16\n'),('poll','24',1499398503,3.80,1,'ubuntu16-02\n'),('poll','27',1499398503,3.84,1,'ubuntu16-02\n'),('poll','23',1499398503,3.70,1,'ubuntu16-02\n'),('poll','25',1499398503,3.70,1,'ubuntu16-02\n'),('poll','22',1499398503,87.84,1,'ubuntu16\n'),('poll','22',1499398802,1.04,1,'ubuntu16\n'),('pollbill','',1499398802,1.99,0,'ubuntu16\n'),('poll','28',1499398802,2.07,1,'ubuntu16\n'),('poll','25',1499398803,3.24,1,'ubuntu16-02\n'),('poll','23',1499398803,3.30,1,'ubuntu16-02\n'),('poll','24',1499398803,3.30,1,'ubuntu16-02\n'),('poll','27',1499398803,3.33,1,'ubuntu16-02\n'),('pollbill','',1499398802,10.82,0,'ubuntu16-02\n'),('poll','23',1499399102,3.54,1,'ubuntu16-02\n'),('poll','28',1499399104,1.62,1,'ubuntu16\n'),('poll','24',1499399102,4.31,1,'ubuntu16-02\n'),('poll','25',1499399102,4.37,1,'ubuntu16-02\n'),('poll','27',1499399102,4.43,1,'ubuntu16-02\n'),('pollbill','',1499399101,4.97,0,'ubuntu16-02\n'),('pollbill','',1499399103,3.75,0,'ubuntu16\n'),('poll','22',1499399104,102.10,1,'ubuntu16\n'),('pollbill','',1499399402,1.73,0,'ubuntu16\n'),('poll','28',1499399402,2.32,1,'ubuntu16\n'),('poll','23',1499399402,3.33,1,'ubuntu16-02\n'),('poll','24',1499399402,3.38,1,'ubuntu16-02\n'),('poll','25',1499399402,3.54,1,'ubuntu16-02\n'),('pollbill','',1499399402,4.08,0,'ubuntu16-02\n'),('poll','27',1499399402,3.74,1,'ubuntu16-02\n'),('poll','22',1499399402,7.83,1,'ubuntu16\n'),('poll','28',1499399702,1.82,1,'ubuntu16\n'),('poll','25',1499399702,3.34,1,'ubuntu16-02\n'),('poll','24',1499399702,3.37,1,'ubuntu16-02\n'),('poll','23',1499399702,3.42,1,'ubuntu16-02\n'),('poll','27',1499399702,3.48,1,'ubuntu16-02\n'),('pollbill','',1499399702,8.84,0,'ubuntu16\n'),('pollbill','',1499399701,11.01,0,'ubuntu16-02\n'),('poll','22',1499399702,99.03,1,'ubuntu16\n'),('poll','28',1499400002,1.52,1,'ubuntu16\n'),('poll','27',1499400002,3.75,1,'ubuntu16-02\n'),('poll','23',1499400002,3.84,1,'ubuntu16-02\n'),('poll','24',1499400002,3.89,1,'ubuntu16-02\n'),('poll','25',1499400002,3.86,1,'ubuntu16-02\n'),('pollbill','',1499400002,3.94,0,'ubuntu16\n'),('pollbill','',1499400001,5.76,0,'ubuntu16-02\n'),('pollbill','',1499400054,3.75,0,'ubuntu16\n'),('pollbill','',1499400086,2.65,0,'ubuntu16\n'),('poll','22',1499400002,93.88,1,'ubuntu16\n'),('pollbill','',1499400135,2.66,0,'ubuntu16\n'),('poll','22',1499400303,1.06,1,'ubuntu16\n'),('poll','28',1499400303,1.64,1,'ubuntu16\n'),('pollbill','',1499400303,2.37,0,'ubuntu16\n'),('pollbill','',1499400302,3.47,0,'ubuntu16-02\n'),('poll','23',1499400303,3.63,1,'ubuntu16-02\n'),('poll','24',1499400303,3.59,1,'ubuntu16-02\n'),('poll','27',1499400303,3.71,1,'ubuntu16-02\n'),('poll','25',1499400303,3.72,1,'ubuntu16-02\n'),('pollbill','',1499400391,4.74,0,'ubuntu16\n'),('pollbill','',1499400602,2.29,0,'ubuntu16-02\n'),('poll','22',1499400603,1.18,1,'ubuntu16\n'),('poll','28',1499400603,1.60,1,'ubuntu16\n'),('poll','24',1499400602,3.32,1,'ubuntu16-02\n'),('poll','23',1499400602,3.66,1,'ubuntu16-02\n'),('poll','25',1499400602,3.56,1,'ubuntu16-02\n'),('poll','27',1499400602,3.69,1,'ubuntu16-02\n'),('pollbill','',1499400603,4.78,0,'ubuntu16\n'),('poll','22',1499400864,1.32,1,'ubuntu16\n'),('poll','28',1499400864,1.47,1,'ubuntu16\n'),('poll','23',1499400886,2.93,1,'ubuntu16-02\n'),('poll','25',1499400886,2.99,1,'ubuntu16-02\n'),('poll','24',1499400886,2.99,1,'ubuntu16-02\n'),('poll','27',1499400886,3.09,1,'ubuntu16-02\n'),('pollbill','',1499400901,0.43,0,'ubuntu16\n'),('pollbill','',1499400902,0.55,0,'ubuntu16-02\n'),('poll','22',1499400902,1.60,1,'ubuntu16\n'),('poll','28',1499400902,1.71,1,'ubuntu16\n'),('poll','27',1499400903,3.49,1,'ubuntu16-02\n'),('poll','25',1499400903,3.58,1,'ubuntu16-02\n'),('poll','24',1499400903,3.59,1,'ubuntu16-02\n'),('poll','23',1499400903,3.59,1,'ubuntu16-02\n'),('pollbill','',1499401202,0.79,0,'ubuntu16\n'),('pollbill','',1499401202,1.16,0,'ubuntu16-02\n'),('poll','22',1499401202,1.43,1,'ubuntu16\n'),('poll','28',1499401202,1.94,1,'ubuntu16\n'),('poll','24',1499401202,3.52,1,'ubuntu16-02\n'),('poll','25',1499401202,3.60,1,'ubuntu16-02\n'),('poll','27',1499401202,3.64,1,'ubuntu16-02\n'),('poll','23',1499401202,3.65,1,'ubuntu16-02\n'),('pollbill','',1499401253,0.44,0,'ubuntu16\n'),('poll','22',1499401459,1.43,1,'ubuntu16\n'),('poll','28',1499401459,1.91,1,'ubuntu16\n'),('pollbill','',1499401464,0.37,0,'ubuntu16\n'),('pollbill','',1499401502,0.91,0,'ubuntu16\n'),('pollbill','',1499401502,1.12,0,'ubuntu16-02\n'),('poll','22',1499401503,1.46,1,'ubuntu16\n'),('poll','28',1499401503,1.60,1,'ubuntu16\n'),('poll','24',1499401503,3.26,1,'ubuntu16-02\n'),('poll','23',1499401503,3.36,1,'ubuntu16-02\n'),('poll','25',1499401503,3.34,1,'ubuntu16-02\n'),('poll','27',1499401503,3.56,1,'ubuntu16-02\n'),('pollbill','',1499401802,1.49,0,'ubuntu16-02\n'),('pollbill','',1499401803,0.97,0,'ubuntu16\n'),('poll','22',1499401804,1.55,1,'ubuntu16\n'),('poll','28',1499401803,1.97,1,'ubuntu16\n'),('poll','24',1499401802,3.57,1,'ubuntu16-02\n'),('poll','23',1499401803,3.54,1,'ubuntu16-02\n'),('poll','25',1499401803,3.57,1,'ubuntu16-02\n'),('poll','27',1499401803,3.55,1,'ubuntu16-02\n'),('pollbill','',1499402102,0.82,0,'ubuntu16\n'),('pollbill','',1499402102,1.42,0,'ubuntu16-02\n'),('poll','22',1499402103,1.66,1,'ubuntu16\n'),('poll','28',1499402103,1.86,1,'ubuntu16\n'),('poll','23',1499402104,3.62,1,'ubuntu16-02\n'),('poll','27',1499402104,3.67,1,'ubuntu16-02\n'),('poll','24',1499402104,3.55,1,'ubuntu16-02\n'),('poll','25',1499402104,3.69,1,'ubuntu16-02\n'),('pollbill','',1499402403,0.94,0,'ubuntu16\n'),('pollbill','',1499402402,2.10,0,'ubuntu16-02\n'),('poll','22',1499402403,1.79,1,'ubuntu16\n'),('poll','28',1499402403,2.15,1,'ubuntu16\n'),('poll','24',1499402403,4.72,1,'ubuntu16-02\n'),('poll','23',1499402403,4.73,1,'ubuntu16-02\n'),('poll','25',1499402403,4.85,1,'ubuntu16-02\n'),('poll','27',1499402403,4.87,1,'ubuntu16-02\n'),('pollbill','',1499402702,0.77,0,'ubuntu16\n'),('pollbill','',1499402702,1.47,0,'ubuntu16-02\n'),('poll','22',1499402702,1.71,1,'ubuntu16\n'),('poll','28',1499402702,1.82,1,'ubuntu16\n'),('poll','25',1499402703,4.20,1,'ubuntu16-02\n'),('poll','24',1499402703,4.08,1,'ubuntu16-02\n'),('poll','27',1499402702,4.40,1,'ubuntu16-02\n'),('poll','23',1499402703,4.38,1,'ubuntu16-02\n'),('pollbill','',1499403001,1.10,0,'ubuntu16-02\n'),('pollbill','',1499403002,0.72,0,'ubuntu16\n'),('poll','22',1499403003,1.73,1,'ubuntu16\n'),('poll','28',1499403003,2.04,1,'ubuntu16\n'),('poll','23',1499403002,4.56,1,'ubuntu16-02\n'),('poll','27',1499403002,4.73,1,'ubuntu16-02\n'),('poll','24',1499403002,4.71,1,'ubuntu16-02\n'),('poll','25',1499403002,4.72,1,'ubuntu16-02\n'),('pollbill','',1499403302,1.24,0,'ubuntu16\n'),('pollbill','',1499403302,1.24,0,'ubuntu16-02\n'),('poll','22',1499403303,1.59,1,'ubuntu16\n'),('poll','28',1499403303,1.75,1,'ubuntu16\n'),('poll','27',1499403303,3.61,1,'ubuntu16-02\n'),('poll','24',1499403303,3.54,1,'ubuntu16-02\n'),('poll','23',1499403303,3.35,1,'ubuntu16-02\n'),('poll','25',1499403303,3.42,1,'ubuntu16-02\n'),('pollbill','',1499403602,0.89,0,'ubuntu16\n'),('pollbill','',1499403602,1.34,0,'ubuntu16-02\n'),('poll','22',1499403602,1.68,1,'ubuntu16\n'),('poll','28',1499403602,1.81,1,'ubuntu16\n'),('poll','25',1499403603,3.43,1,'ubuntu16-02\n'),('poll','23',1499403603,3.38,1,'ubuntu16-02\n'),('poll','24',1499403603,3.39,1,'ubuntu16-02\n'),('poll','27',1499403603,3.45,1,'ubuntu16-02\n'),('pollbill','',1499403902,1.42,0,'ubuntu16\n'),('pollbill','',1499403902,1.58,0,'ubuntu16-02\n'),('poll','22',1499403902,2.05,1,'ubuntu16\n'),('poll','28',1499403902,2.17,1,'ubuntu16\n'),('poll','24',1499403903,3.97,1,'ubuntu16-02\n'),('poll','23',1499403903,3.87,1,'ubuntu16-02\n'),('poll','25',1499403903,3.90,1,'ubuntu16-02\n'),('poll','27',1499403903,3.81,1,'ubuntu16-02\n'),('pollbill','',1499404202,0.80,0,'ubuntu16\n'),('pollbill','',1499404202,1.19,0,'ubuntu16-02\n'),('poll','22',1499404203,1.40,1,'ubuntu16\n'),('poll','28',1499404203,2.27,1,'ubuntu16\n'),('poll','27',1499404203,3.60,1,'ubuntu16-02\n'),('poll','23',1499404203,3.59,1,'ubuntu16-02\n'),('poll','24',1499404203,3.59,1,'ubuntu16-02\n'),('poll','25',1499404203,3.65,1,'ubuntu16-02\n'),('pollbill','',1499404503,0.76,0,'ubuntu16\n'),('pollbill','',1499404502,1.98,0,'ubuntu16-02\n'),('poll','22',1499404503,1.50,1,'ubuntu16\n'),('poll','28',1499404503,1.74,1,'ubuntu16\n'),('poll','24',1499404503,3.88,1,'ubuntu16-02\n'),('poll','25',1499404503,3.74,1,'ubuntu16-02\n'),('poll','23',1499404503,3.52,1,'ubuntu16-02\n'),('poll','27',1499404503,3.89,1,'ubuntu16-02\n'),('pollbill','',1499404802,0.85,0,'ubuntu16\n'),('pollbill','',1499404802,0.98,0,'ubuntu16-02\n'),('poll','22',1499404802,1.51,1,'ubuntu16\n'),('poll','28',1499404802,1.86,1,'ubuntu16\n'),('poll','24',1499404803,3.72,1,'ubuntu16-02\n'),('poll','23',1499404803,3.58,1,'ubuntu16-02\n'),('poll','25',1499404803,3.68,1,'ubuntu16-02\n'),('poll','27',1499404803,3.78,1,'ubuntu16-02\n'),('pollbill','',1499405101,1.44,0,'ubuntu16-02\n'),('pollbill','',1499405102,0.92,0,'ubuntu16\n'),('poll','22',1499405103,1.81,1,'ubuntu16\n'),('poll','28',1499405102,2.19,1,'ubuntu16\n'),('poll','27',1499405102,4.20,1,'ubuntu16-02\n'),('poll','24',1499405102,4.27,1,'ubuntu16-02\n'),('poll','23',1499405102,4.25,1,'ubuntu16-02\n'),('poll','25',1499405102,4.25,1,'ubuntu16-02\n'),('pollbill','',1499405403,0.77,0,'ubuntu16\n'),('pollbill','',1499405402,1.80,0,'ubuntu16-02\n'),('poll','28',1499405403,2.54,1,'ubuntu16\n'),('poll','22',1499405403,2.75,1,'ubuntu16\n'),('poll','23',1499405402,5.55,1,'ubuntu16-02\n'),('poll','25',1499405403,5.53,1,'ubuntu16-02\n'),('poll','27',1499405402,5.62,1,'ubuntu16-02\n'),('poll','24',1499405402,5.72,1,'ubuntu16-02\n'),('pollbill','',1499405701,1.69,0,'ubuntu16-02\n'),('pollbill','',1499405702,1.20,0,'ubuntu16\n'),('poll','22',1499405703,1.75,1,'ubuntu16\n'),('poll','28',1499405703,2.13,1,'ubuntu16\n'),('poll','24',1499405702,4.20,1,'ubuntu16-02\n'),('poll','23',1499405702,4.17,1,'ubuntu16-02\n'),('poll','25',1499405702,4.24,1,'ubuntu16-02\n'),('poll','27',1499405702,4.12,1,'ubuntu16-02\n'),('pollbill','',1499406002,1.07,0,'ubuntu16\n'),('pollbill','',1499406002,1.54,0,'ubuntu16-02\n'),('poll','22',1499406002,2.09,1,'ubuntu16\n'),('poll','28',1499406002,2.16,1,'ubuntu16\n'),('poll','24',1499406003,3.86,1,'ubuntu16-02\n'),('poll','25',1499406003,4.06,1,'ubuntu16-02\n'),('poll','27',1499406003,3.98,1,'ubuntu16-02\n'),('poll','23',1499406003,4.04,1,'ubuntu16-02\n'),('pollbill','',1499406303,0.93,0,'ubuntu16\n'),('pollbill','',1499406302,2.39,0,'ubuntu16-02\n'),('poll','22',1499406303,1.90,1,'ubuntu16\n'),('poll','28',1499406303,2.21,1,'ubuntu16\n'),('poll','23',1499406302,4.57,1,'ubuntu16-02\n'),('poll','24',1499406302,4.59,1,'ubuntu16-02\n'),('poll','27',1499406302,4.59,1,'ubuntu16-02\n'),('poll','25',1499406302,4.59,1,'ubuntu16-02\n'),('pollbill','',1499406601,0.98,0,'ubuntu16-02\n'),('pollbill','',1499406602,0.58,0,'ubuntu16\n'),('poll','22',1499406603,1.67,1,'ubuntu16\n'),('poll','28',1499406603,1.92,1,'ubuntu16\n'),('poll','23',1499406602,3.74,1,'ubuntu16-02\n'),('poll','25',1499406602,3.75,1,'ubuntu16-02\n'),('poll','24',1499406602,4.08,1,'ubuntu16-02\n'),('poll','27',1499406602,4.09,1,'ubuntu16-02\n'),('pollbill','',1499406902,0.77,0,'ubuntu16\n'),('pollbill','',1499406902,1.47,0,'ubuntu16-02\n'),('poll','22',1499406903,1.61,1,'ubuntu16\n'),('poll','28',1499406903,2.23,1,'ubuntu16\n'),('poll','25',1499406902,3.57,1,'ubuntu16-02\n'),('poll','23',1499406903,4.00,1,'ubuntu16-02\n'),('poll','27',1499406902,4.14,1,'ubuntu16-02\n'),('poll','24',1499406903,3.99,1,'ubuntu16-02\n'),('pollbill','',1499407203,3.10,0,'ubuntu16\n'),('pollbill','',1499407202,3.89,0,'ubuntu16-02\n'),('poll','22',1499407205,2.36,1,'ubuntu16\n'),('poll','28',1499407205,2.97,1,'ubuntu16\n'),('poll','23',1499407205,4.74,1,'ubuntu16-02\n'),('poll','25',1499407205,5.03,1,'ubuntu16-02\n'),('poll','24',1499407205,5.08,1,'ubuntu16-02\n'),('poll','27',1499407205,5.10,1,'ubuntu16-02\n'),('pollbill','',1499407502,0.92,0,'ubuntu16\n'),('pollbill','',1499407501,1.46,0,'ubuntu16-02\n'),('poll','22',1499407502,1.61,1,'ubuntu16\n'),('poll','28',1499407502,2.25,1,'ubuntu16\n'),('poll','27',1499407502,3.55,1,'ubuntu16-02\n'),('poll','24',1499407502,3.54,1,'ubuntu16-02\n'),('poll','23',1499407502,3.43,1,'ubuntu16-02\n'),('poll','25',1499407502,3.38,1,'ubuntu16-02\n'),('pollbill','',1499407802,0.74,0,'ubuntu16\n'),('pollbill','',1499407802,1.46,0,'ubuntu16-02\n'),('poll','22',1499407803,1.73,1,'ubuntu16\n'),('poll','28',1499407803,1.97,1,'ubuntu16\n'),('poll','25',1499407803,3.40,1,'ubuntu16-02\n'),('poll','24',1499407803,3.31,1,'ubuntu16-02\n'),('poll','23',1499407803,3.43,1,'ubuntu16-02\n'),('poll','27',1499407803,3.52,1,'ubuntu16-02\n'),('pollbill','',1499408101,1.98,0,'ubuntu16-02\n'),('pollbill','',1499408103,1.49,0,'ubuntu16\n'),('poll','22',1499408103,2.18,1,'ubuntu16\n'),('poll','28',1499408103,2.35,1,'ubuntu16\n'),('poll','23',1499408102,4.33,1,'ubuntu16-02\n'),('poll','27',1499408102,4.50,1,'ubuntu16-02\n'),('poll','25',1499408102,4.38,1,'ubuntu16-02\n'),('poll','24',1499408102,4.49,1,'ubuntu16-02\n'),('pollbill','',1499408402,1.11,0,'ubuntu16\n'),('pollbill','',1499408402,1.49,0,'ubuntu16-02\n'),('poll','22',1499408402,1.98,1,'ubuntu16\n'),('poll','28',1499408402,2.00,1,'ubuntu16\n'),('poll','24',1499408402,4.29,1,'ubuntu16-02\n'),('poll','23',1499408403,3.85,1,'ubuntu16-02\n'),('poll','25',1499408403,3.49,1,'ubuntu16-02\n'),('poll','27',1499408403,3.93,1,'ubuntu16-02\n'),('pollbill','',1499408702,1.03,0,'ubuntu16\n'),('pollbill','',1499408702,1.31,0,'ubuntu16-02\n'),('poll','22',1499408702,1.75,1,'ubuntu16\n'),('poll','28',1499408702,1.88,1,'ubuntu16\n'),('poll','25',1499408703,3.89,1,'ubuntu16-02\n'),('poll','24',1499408703,3.90,1,'ubuntu16-02\n'),('poll','27',1499408703,4.05,1,'ubuntu16-02\n'),('poll','23',1499408703,4.09,1,'ubuntu16-02\n'),('pollbill','',1499409002,0.71,0,'ubuntu16\n'),('pollbill','',1499409002,1.01,0,'ubuntu16-02\n'),('poll','22',1499409003,1.80,1,'ubuntu16\n'),('poll','28',1499409003,2.54,1,'ubuntu16\n'),('poll','23',1499409003,3.71,1,'ubuntu16-02\n'),('poll','25',1499409003,3.73,1,'ubuntu16-02\n'),('poll','27',1499409003,3.84,1,'ubuntu16-02\n'),('poll','24',1499409003,3.79,1,'ubuntu16-02\n'),('pollbill','',1499409302,1.18,0,'ubuntu16\n'),('pollbill','',1499409302,1.53,0,'ubuntu16-02\n'),('poll','22',1499409302,1.86,1,'ubuntu16\n'),('poll','28',1499409302,2.12,1,'ubuntu16\n'),('poll','23',1499409303,3.25,1,'ubuntu16-02\n'),('poll','27',1499409303,3.39,1,'ubuntu16-02\n'),('poll','24',1499409303,3.41,1,'ubuntu16-02\n'),('poll','25',1499409303,3.42,1,'ubuntu16-02\n'),('pollbill','',1499409602,0.82,0,'ubuntu16\n'),('pollbill','',1499409601,1.67,0,'ubuntu16-02\n'),('poll','22',1499409602,1.52,1,'ubuntu16\n'),('poll','28',1499409602,1.93,1,'ubuntu16\n'),('poll','23',1499409602,3.49,1,'ubuntu16-02\n'),('poll','25',1499409602,3.74,1,'ubuntu16-02\n'),('poll','27',1499409602,3.76,1,'ubuntu16-02\n'),('poll','24',1499409603,2.90,1,'ubuntu16-02\n'),('pollbill','',1499409903,1.04,0,'ubuntu16\n'),('pollbill','',1499409902,2.20,0,'ubuntu16-02\n'),('poll','22',1499409903,1.92,1,'ubuntu16\n'),('poll','28',1499409903,2.31,1,'ubuntu16\n'),('poll','25',1499409902,3.84,1,'ubuntu16-02\n'),('poll','23',1499409903,3.79,1,'ubuntu16-02\n'),('poll','27',1499409903,3.76,1,'ubuntu16-02\n'),('poll','24',1499409903,3.76,1,'ubuntu16-02\n'),('pollbill','',1499410201,0.97,0,'ubuntu16\n'),('pollbill','',1499410202,1.40,0,'ubuntu16-02\n'),('poll','22',1499410202,1.68,1,'ubuntu16\n'),('poll','28',1499410202,2.04,1,'ubuntu16\n'),('poll','24',1499410202,3.10,1,'ubuntu16-02\n'),('poll','25',1499410202,2.97,1,'ubuntu16-02\n'),('poll','23',1499410202,3.01,1,'ubuntu16-02\n'),('poll','27',1499410202,3.56,1,'ubuntu16-02\n'),('pollbill','',1499410501,0.92,0,'ubuntu16\n'),('pollbill','',1499410502,1.00,0,'ubuntu16-02\n'),('poll','22',1499410502,1.72,1,'ubuntu16\n'),('poll','28',1499410502,2.30,1,'ubuntu16\n'),('poll','25',1499410503,3.61,1,'ubuntu16-02\n'),('poll','24',1499410503,3.61,1,'ubuntu16-02\n'),('poll','23',1499410503,3.62,1,'ubuntu16-02\n'),('poll','27',1499410503,3.69,1,'ubuntu16-02\n'),('pollbill','',1499410807,0.83,0,'ubuntu16\n'),('pollbill','',1499410802,5.60,0,'ubuntu16-02\n'),('poll','22',1499410807,1.75,1,'ubuntu16\n'),('poll','28',1499410807,1.83,1,'ubuntu16\n'),('poll','23',1499410806,4.33,1,'ubuntu16-02\n'),('poll','27',1499410805,5.16,1,'ubuntu16-02\n'),('poll','25',1499410806,4.16,1,'ubuntu16-02\n'),('poll','24',1499410806,4.39,1,'ubuntu16-02\n'),('pollbill','',1499411102,0.78,0,'ubuntu16\n'),('pollbill','',1499411102,1.43,0,'ubuntu16-02\n'),('poll','22',1499411103,1.75,1,'ubuntu16\n'),('poll','28',1499411103,1.95,1,'ubuntu16\n'),('poll','24',1499411102,3.66,1,'ubuntu16-02\n'),('poll','23',1499411102,3.92,1,'ubuntu16-02\n'),('poll','25',1499411103,3.81,1,'ubuntu16-02\n'),('poll','27',1499411102,3.98,1,'ubuntu16-02\n'),('pollbill','',1499411402,0.96,0,'ubuntu16\n'),('pollbill','',1499411402,0.99,0,'ubuntu16-02\n'),('poll','22',1499411402,1.57,1,'ubuntu16\n'),('poll','28',1499411402,1.70,1,'ubuntu16\n'),('poll','23',1499411403,3.21,1,'ubuntu16-02\n'),('poll','24',1499411403,3.48,1,'ubuntu16-02\n'),('poll','25',1499411403,3.47,1,'ubuntu16-02\n'),('poll','27',1499411403,3.53,1,'ubuntu16-02\n'),('pollbill','',1499411701,4.83,0,'ubuntu16-02\n'),('poll','27',1499411702,5.88,1,'ubuntu16-02\n'),('poll','25',1499411702,6.32,1,'ubuntu16-02\n'),('poll','23',1499411702,6.32,1,'ubuntu16-02\n'),('poll','24',1499411702,6.54,1,'ubuntu16-02\n'),('pollbill','',1499412007,12.39,0,'ubuntu16-02\n'),('pollbill','',1499412007,12.66,0,'ubuntu16\n'),('poll','28',1499412007,12.75,1,'ubuntu16\n'),('poll','22',1499412007,12.69,1,'ubuntu16\n'),('poll','27',1499412008,12.55,1,'ubuntu16-02\n'),('poll','23',1499412008,12.58,1,'ubuntu16-02\n'),('poll','25',1499412008,12.58,1,'ubuntu16-02\n'),('poll','24',1499412008,12.61,1,'ubuntu16-02\n'),('pollbill','',1499412301,0.92,0,'ubuntu16\n'),('pollbill','',1499412302,1.92,0,'ubuntu16-02\n'),('poll','28',1499412302,2.91,1,'ubuntu16\n'),('poll','22',1499412302,2.96,1,'ubuntu16\n'),('poll','23',1499412303,5.36,1,'ubuntu16-02\n'),('poll','25',1499412303,5.36,1,'ubuntu16-02\n'),('poll','27',1499412303,5.18,1,'ubuntu16-02\n'),('poll','24',1499412303,5.10,1,'ubuntu16-02\n'),('pollbill','',1499412602,0.82,0,'ubuntu16\n'),('pollbill','',1499412601,1.48,0,'ubuntu16-02\n'),('poll','22',1499412602,1.96,1,'ubuntu16\n'),('poll','28',1499412602,2.43,1,'ubuntu16\n'),('poll','25',1499412602,4.42,1,'ubuntu16-02\n'),('poll','24',1499412603,4.54,1,'ubuntu16-02\n'),('poll','23',1499412602,4.73,1,'ubuntu16-02\n'),('poll','27',1499412603,4.43,1,'ubuntu16-02\n'),('pollbill','',1499412902,0.72,0,'ubuntu16\n'),('pollbill','',1499412902,1.14,0,'ubuntu16-02\n'),('poll','22',1499412903,1.55,1,'ubuntu16\n'),('poll','28',1499412903,1.81,1,'ubuntu16\n'),('poll','23',1499412903,3.45,1,'ubuntu16-02\n'),('poll','24',1499412903,3.42,1,'ubuntu16-02\n'),('poll','25',1499412903,3.61,1,'ubuntu16-02\n'),('poll','27',1499412903,3.75,1,'ubuntu16-02\n'),('pollbill','',1499413202,0.97,0,'ubuntu16\n'),('pollbill','',1499413202,1.81,0,'ubuntu16-02\n'),('poll','22',1499413203,1.76,1,'ubuntu16\n'),('poll','28',1499413203,2.33,1,'ubuntu16\n'),('poll','24',1499413203,3.39,1,'ubuntu16-02\n'),('poll','25',1499413203,3.67,1,'ubuntu16-02\n'),('poll','23',1499413203,3.69,1,'ubuntu16-02\n'),('poll','27',1499413203,3.76,1,'ubuntu16-02\n'),('pollbill','',1499413501,0.74,0,'ubuntu16-02\n'),('pollbill','',1499413502,0.75,0,'ubuntu16\n'),('poll','22',1499413503,1.64,1,'ubuntu16\n'),('poll','28',1499413503,1.86,1,'ubuntu16\n'),('poll','23',1499413502,4.08,1,'ubuntu16-02\n'),('poll','24',1499413502,4.11,1,'ubuntu16-02\n'),('poll','25',1499413502,4.18,1,'ubuntu16-02\n'),('poll','27',1499413502,4.29,1,'ubuntu16-02\n'),('pollbill','',1499413802,0.75,0,'ubuntu16\n'),('pollbill','',1499413802,1.56,0,'ubuntu16-02\n'),('poll','22',1499413802,1.45,1,'ubuntu16\n'),('poll','28',1499413802,2.02,1,'ubuntu16\n'),('poll','24',1499413802,3.18,1,'ubuntu16-02\n'),('poll','27',1499413802,3.38,1,'ubuntu16-02\n'),('poll','25',1499413802,3.75,1,'ubuntu16-02\n'),('poll','23',1499413802,3.74,1,'ubuntu16-02\n'),('pollbill','',1499414102,0.82,0,'ubuntu16\n'),('pollbill','',1499414102,0.99,0,'ubuntu16-02\n'),('poll','22',1499414102,1.47,1,'ubuntu16\n'),('poll','28',1499414102,1.76,1,'ubuntu16\n'),('poll','25',1499414103,3.40,1,'ubuntu16-02\n'),('poll','23',1499414103,3.53,1,'ubuntu16-02\n'),('poll','24',1499414103,3.50,1,'ubuntu16-02\n'),('poll','27',1499414103,3.43,1,'ubuntu16-02\n'),('pollbill','',1499414402,0.72,0,'ubuntu16\n'),('pollbill','',1499414401,1.30,0,'ubuntu16-02\n'),('poll','22',1499414402,1.54,1,'ubuntu16\n'),('poll','28',1499414402,1.94,1,'ubuntu16\n'),('poll','23',1499414402,3.56,1,'ubuntu16-02\n'),('poll','25',1499414402,3.59,1,'ubuntu16-02\n'),('poll','27',1499414402,3.63,1,'ubuntu16-02\n'),('poll','24',1499414402,3.61,1,'ubuntu16-02\n'),('pollbill','',1499414702,1.41,0,'ubuntu16-02\n'),('pollbill','',1499414702,0.73,0,'ubuntu16\n'),('poll','22',1499414703,1.72,1,'ubuntu16\n'),('poll','28',1499414703,1.87,1,'ubuntu16\n'),('poll','27',1499414702,3.46,1,'ubuntu16-02\n'),('poll','24',1499414702,3.54,1,'ubuntu16-02\n'),('poll','25',1499414702,3.62,1,'ubuntu16-02\n'),('poll','23',1499414702,3.64,1,'ubuntu16-02\n'),('pollbill','',1499415001,0.68,0,'ubuntu16-02\n'),('pollbill','',1499415002,0.82,0,'ubuntu16\n'),('poll','22',1499415003,1.57,1,'ubuntu16\n'),('poll','28',1499415003,2.18,1,'ubuntu16\n'),('poll','25',1499415002,3.75,1,'ubuntu16-02\n'),('poll','24',1499415002,3.84,1,'ubuntu16-02\n'),('poll','27',1499415002,3.91,1,'ubuntu16-02\n'),('poll','23',1499415002,3.92,1,'ubuntu16-02\n'),('pollbill','',1499415302,0.79,0,'ubuntu16\n'),('pollbill','',1499415302,1.54,0,'ubuntu16-02\n'),('poll','22',1499415303,1.72,1,'ubuntu16\n'),('poll','28',1499415303,1.88,1,'ubuntu16\n'),('poll','24',1499415303,3.69,1,'ubuntu16-02\n'),('poll','25',1499415302,3.82,1,'ubuntu16-02\n'),('poll','27',1499415303,3.81,1,'ubuntu16-02\n'),('poll','23',1499415303,3.68,1,'ubuntu16-02\n'),('pollbill','',1499415601,0.69,0,'ubuntu16\n'),('pollbill','',1499415602,1.34,0,'ubuntu16-02\n'),('poll','22',1499415602,1.92,1,'ubuntu16\n'),('poll','28',1499415602,1.98,1,'ubuntu16\n'),('poll','24',1499415603,3.09,1,'ubuntu16-02\n'),('poll','25',1499415603,3.21,1,'ubuntu16-02\n'),('poll','23',1499415603,3.19,1,'ubuntu16-02\n'),('poll','27',1499415603,3.36,1,'ubuntu16-02\n'),('pollbill','',1499415902,0.70,0,'ubuntu16\n'),('pollbill','',1499415902,1.22,0,'ubuntu16-02\n'),('poll','22',1499415902,1.64,1,'ubuntu16\n'),('poll','28',1499415902,1.94,1,'ubuntu16\n'),('poll','24',1499415902,3.61,1,'ubuntu16-02\n'),('poll','23',1499415902,3.62,1,'ubuntu16-02\n'),('poll','25',1499415902,3.65,1,'ubuntu16-02\n'),('poll','27',1499415902,3.67,1,'ubuntu16-02\n'),('pollbill','',1499416202,0.84,0,'ubuntu16\n'),('pollbill','',1499416202,0.94,0,'ubuntu16-02\n'),('poll','22',1499416202,1.74,1,'ubuntu16\n'),('poll','28',1499416202,1.76,1,'ubuntu16\n'),('poll','23',1499416203,3.83,1,'ubuntu16-02\n'),('poll','25',1499416203,3.69,1,'ubuntu16-02\n'),('poll','24',1499416203,3.74,1,'ubuntu16-02\n'),('poll','27',1499416203,3.78,1,'ubuntu16-02\n'),('pollbill','',1499416502,0.75,0,'ubuntu16\n'),('pollbill','',1499416502,0.98,0,'ubuntu16-02\n'),('poll','22',1499416502,1.56,1,'ubuntu16\n'),('poll','28',1499416502,2.18,1,'ubuntu16\n'),('poll','24',1499416502,3.30,1,'ubuntu16-02\n'),('poll','25',1499416503,3.26,1,'ubuntu16-02\n'),('poll','23',1499416502,3.48,1,'ubuntu16-02\n'),('poll','27',1499416503,3.49,1,'ubuntu16-02\n'),('pollbill','',1499416801,1.72,0,'ubuntu16-02\n'),('pollbill','',1499416802,0.88,0,'ubuntu16\n'),('poll','22',1499416803,1.91,1,'ubuntu16\n'),('poll','28',1499416803,2.23,1,'ubuntu16\n'),('poll','25',1499416802,4.08,1,'ubuntu16-02\n'),('poll','27',1499416802,4.37,1,'ubuntu16-02\n'),('poll','23',1499416802,4.59,1,'ubuntu16-02\n'),('poll','24',1499416802,4.43,1,'ubuntu16-02\n'),('pollbill','',1499417101,2.63,0,'ubuntu16\n'),('pollbill','',1499417102,2.63,0,'ubuntu16-02\n'),('poll','22',1499417102,3.25,1,'ubuntu16\n'),('poll','28',1499417102,3.48,1,'ubuntu16\n'),('poll','23',1499417103,3.95,1,'ubuntu16-02\n'),('poll','25',1499417104,3.69,1,'ubuntu16-02\n'),('poll','24',1499417103,4.08,1,'ubuntu16-02\n'),('poll','27',1499417103,4.07,1,'ubuntu16-02\n'),('pollbill','',1499417404,0.84,0,'ubuntu16\n'),('pollbill','',1499417403,1.76,0,'ubuntu16-02\n'),('poll','22',1499417404,1.66,1,'ubuntu16\n'),('poll','28',1499417404,1.81,1,'ubuntu16\n'),('poll','23',1499417404,3.43,1,'ubuntu16-02\n'),('poll','27',1499417404,3.63,1,'ubuntu16-02\n'),('poll','25',1499417405,3.11,1,'ubuntu16-02\n'),('poll','24',1499417405,3.17,1,'ubuntu16-02\n'),('pollbill','',1499417702,0.74,0,'ubuntu16\n'),('pollbill','',1499417702,1.30,0,'ubuntu16-02\n'),('poll','22',1499417702,1.57,1,'ubuntu16\n'),('poll','28',1499417702,1.80,1,'ubuntu16\n'),('poll','25',1499417703,3.16,1,'ubuntu16-02\n'),('poll','23',1499417703,3.25,1,'ubuntu16-02\n'),('poll','24',1499417703,3.16,1,'ubuntu16-02\n'),('poll','27',1499417703,3.44,1,'ubuntu16-02\n'),('pollbill','',1499418001,1.39,0,'ubuntu16-02\n'),('pollbill','',1499418002,0.95,0,'ubuntu16\n'),('poll','22',1499418003,1.83,1,'ubuntu16\n'),('poll','28',1499418003,2.00,1,'ubuntu16\n'),('poll','24',1499418002,3.62,1,'ubuntu16-02\n'),('poll','25',1499418002,3.74,1,'ubuntu16-02\n'),('poll','23',1499418002,3.84,1,'ubuntu16-02\n'),('poll','27',1499418002,3.95,1,'ubuntu16-02\n'),('pollbill','',1499418302,0.80,0,'ubuntu16\n'),('pollbill','',1499418301,1.29,0,'ubuntu16-02\n'),('poll','22',1499418302,1.47,1,'ubuntu16\n'),('poll','28',1499418302,2.96,1,'ubuntu16\n'),('poll','23',1499418302,3.76,1,'ubuntu16-02\n'),('poll','27',1499418302,3.75,1,'ubuntu16-02\n'),('poll','25',1499418302,3.81,1,'ubuntu16-02\n'),('poll','24',1499418302,3.80,1,'ubuntu16-02\n'),('pollbill','',1499418602,0.76,0,'ubuntu16\n'),('pollbill','',1499418602,0.79,0,'ubuntu16-02\n'),('poll','22',1499418602,1.44,1,'ubuntu16\n'),('poll','28',1499418602,1.69,1,'ubuntu16\n'),('poll','24',1499418603,3.32,1,'ubuntu16-02\n'),('poll','23',1499418603,3.31,1,'ubuntu16-02\n'),('poll','27',1499418603,3.36,1,'ubuntu16-02\n'),('poll','25',1499418603,3.37,1,'ubuntu16-02\n'),('pollbill','',1499418902,0.97,0,'ubuntu16\n'),('pollbill','',1499418902,0.98,0,'ubuntu16-02\n'),('poll','22',1499418902,1.50,1,'ubuntu16\n'),('poll','28',1499418902,1.95,1,'ubuntu16\n'),('poll','24',1499418903,3.62,1,'ubuntu16-02\n'),('poll','25',1499418903,3.57,1,'ubuntu16-02\n'),('poll','27',1499418903,3.69,1,'ubuntu16-02\n'),('poll','23',1499418903,3.67,1,'ubuntu16-02\n'),('pollbill','',1499419202,0.78,0,'ubuntu16\n'),('pollbill','',1499419201,1.48,0,'ubuntu16-02\n'),('poll','28',1499419202,2.01,1,'ubuntu16\n'),('poll','22',1499419202,1.93,1,'ubuntu16\n'),('poll','27',1499419202,3.83,1,'ubuntu16-02\n'),('poll','25',1499419202,3.92,1,'ubuntu16-02\n'),('poll','23',1499419202,3.92,1,'ubuntu16-02\n'),('poll','24',1499419203,3.70,1,'ubuntu16-02\n'),('pollbill','',1499419502,0.84,0,'ubuntu16\n'),('pollbill','',1499419502,1.22,0,'ubuntu16-02\n'),('poll','22',1499419503,1.68,1,'ubuntu16\n'),('poll','28',1499419503,1.98,1,'ubuntu16\n'),('poll','23',1499419503,3.08,1,'ubuntu16-02\n'),('poll','25',1499419503,3.27,1,'ubuntu16-02\n'),('poll','24',1499419503,3.38,1,'ubuntu16-02\n'),('poll','27',1499419503,3.48,1,'ubuntu16-02\n'),('pollbill','',1499419802,0.84,0,'ubuntu16\n'),('pollbill','',1499419802,1.64,0,'ubuntu16-02\n'),('poll','22',1499419803,1.54,1,'ubuntu16\n'),('poll','28',1499419803,1.97,1,'ubuntu16\n'),('poll','24',1499419803,3.67,1,'ubuntu16-02\n'),('poll','23',1499419803,3.70,1,'ubuntu16-02\n'),('poll','27',1499419803,3.72,1,'ubuntu16-02\n'),('poll','25',1499419803,3.71,1,'ubuntu16-02\n'),('pollbill','',1499420102,0.69,0,'ubuntu16\n'),('pollbill','',1499420102,1.06,0,'ubuntu16-02\n'),('poll','22',1499420102,1.34,1,'ubuntu16\n'),('poll','28',1499420102,2.44,1,'ubuntu16\n'),('poll','23',1499420102,3.23,1,'ubuntu16-02\n'),('poll','24',1499420102,3.30,1,'ubuntu16-02\n'),('poll','25',1499420103,3.28,1,'ubuntu16-02\n'),('poll','27',1499420102,3.68,1,'ubuntu16-02\n'),('pollbill','',1499420402,0.68,0,'ubuntu16\n'),('pollbill','',1499420402,1.01,0,'ubuntu16-02\n'),('poll','22',1499420402,1.81,1,'ubuntu16\n'),('poll','28',1499420402,2.18,1,'ubuntu16\n'),('poll','25',1499420403,2.71,1,'ubuntu16-02\n'),('poll','23',1499420403,3.29,1,'ubuntu16-02\n'),('poll','24',1499420403,3.29,1,'ubuntu16-02\n'),('poll','27',1499420403,3.31,1,'ubuntu16-02\n'),('pollbill','',1499420702,0.80,0,'ubuntu16\n'),('pollbill','',1499420702,1.13,0,'ubuntu16-02\n'),('poll','22',1499420702,1.56,1,'ubuntu16\n'),('poll','28',1499420702,1.82,1,'ubuntu16\n'),('poll','23',1499420703,3.47,1,'ubuntu16-02\n'),('poll','24',1499420703,3.58,1,'ubuntu16-02\n'),('poll','25',1499420703,3.62,1,'ubuntu16-02\n'),('poll','27',1499420703,3.59,1,'ubuntu16-02\n'),('pollbill','',1499421002,0.78,0,'ubuntu16\n'),('pollbill','',1499421001,1.32,0,'ubuntu16-02\n'),('poll','22',1499421002,1.58,1,'ubuntu16\n'),('poll','28',1499421002,1.95,1,'ubuntu16\n'),('poll','24',1499421002,3.53,1,'ubuntu16-02\n'),('poll','25',1499421002,3.84,1,'ubuntu16-02\n'),('poll','23',1499421002,3.67,1,'ubuntu16-02\n'),('poll','27',1499421002,3.85,1,'ubuntu16-02\n'),('pollbill','',1499421302,0.82,0,'ubuntu16\n'),('pollbill','',1499421302,1.12,0,'ubuntu16-02\n'),('poll','22',1499421302,1.53,1,'ubuntu16\n'),('poll','28',1499421302,1.92,1,'ubuntu16\n'),('poll','24',1499421303,4.10,1,'ubuntu16-02\n'),('poll','27',1499421303,4.29,1,'ubuntu16-02\n'),('poll','23',1499421303,4.23,1,'ubuntu16-02\n'),('poll','25',1499421303,4.02,1,'ubuntu16-02\n'),('pollbill','',1499421602,0.77,0,'ubuntu16\n'),('pollbill','',1499421602,1.70,0,'ubuntu16-02\n'),('poll','22',1499421603,1.58,1,'ubuntu16\n'),('poll','28',1499421603,3.08,1,'ubuntu16\n'),('poll','23',1499421602,4.21,1,'ubuntu16-02\n'),('poll','25',1499421603,4.22,1,'ubuntu16-02\n'),('poll','24',1499421603,4.09,1,'ubuntu16-02\n'),('poll','27',1499421603,4.45,1,'ubuntu16-02\n'),('pollbill','',1499421902,0.92,0,'ubuntu16\n'),('pollbill','',1499421902,1.13,0,'ubuntu16-02\n'),('poll','22',1499421902,1.64,1,'ubuntu16\n'),('poll','28',1499421902,2.21,1,'ubuntu16\n'),('poll','27',1499421902,3.21,1,'ubuntu16-02\n'),('poll','23',1499421902,3.56,1,'ubuntu16-02\n'),('poll','25',1499421902,3.51,1,'ubuntu16-02\n'),('poll','24',1499421902,3.53,1,'ubuntu16-02\n'),('poll','25',1499436010,4.30,1,'ubuntu16-02\n'),('poll','24',1499436010,4.32,1,'ubuntu16-02\n'),('pollbill','',1499436002,18.27,0,'ubuntu16\n'),('poll','22',1499436006,15.60,1,'ubuntu16\n'),('poll','28',1499436006,17.87,1,'ubuntu16\n'),('pollbill','',1499436009,18.45,0,'ubuntu16-02\n'),('poll','23',1499436010,19.34,1,'ubuntu16-02\n'),('poll','27',1499436010,20.96,1,'ubuntu16-02\n'),('pollbill','',1499436302,0.69,0,'ubuntu16\n'),('pollbill','',1499436302,1.15,0,'ubuntu16-02\n'),('poll','22',1499436302,1.38,1,'ubuntu16\n'),('poll','25',1499436303,1.25,1,'ubuntu16-02\n'),('poll','24',1499436303,1.33,1,'ubuntu16-02\n'),('poll','23',1499436303,1.65,1,'ubuntu16-02\n'),('poll','28',1499436302,4.30,1,'ubuntu16\n'),('poll','27',1499436303,4.15,1,'ubuntu16-02\n'),('pollbill','',1499436602,0.68,0,'ubuntu16\n'),('pollbill','',1499436602,1.40,0,'ubuntu16-02\n'),('poll','22',1499436603,1.40,1,'ubuntu16\n'),('poll','25',1499436603,1.40,1,'ubuntu16-02\n'),('poll','24',1499436603,1.29,1,'ubuntu16-02\n'),('poll','23',1499436603,1.64,1,'ubuntu16-02\n'),('poll','28',1499436603,4.04,1,'ubuntu16\n'),('poll','27',1499436603,4.46,1,'ubuntu16-02\n'),('pollbill','',1499436902,0.68,0,'ubuntu16\n'),('pollbill','',1499436902,1.09,0,'ubuntu16-02\n'),('poll','22',1499436902,1.51,1,'ubuntu16\n'),('poll','28',1499436902,1.77,1,'ubuntu16\n'),('poll','25',1499436903,1.32,1,'ubuntu16-02\n'),('poll','24',1499436903,1.34,1,'ubuntu16-02\n'),('poll','23',1499436903,2.26,1,'ubuntu16-02\n'),('poll','27',1499436903,2.36,1,'ubuntu16-02\n'),('pollbill','',1499437201,0.95,0,'ubuntu16-02\n'),('pollbill','',1499437202,0.60,0,'ubuntu16\n'),('poll','25',1499437202,1.55,1,'ubuntu16-02\n'),('poll','24',1499437202,1.56,1,'ubuntu16-02\n'),('poll','22',1499437203,1.46,1,'ubuntu16\n'),('poll','23',1499437202,2.51,1,'ubuntu16-02\n'),('poll','27',1499437202,2.63,1,'ubuntu16-02\n'),('poll','28',1499437203,4.05,1,'ubuntu16\n'),('pollbill','',1499437502,0.81,0,'ubuntu16\n'),('pollbill','',1499437502,1.09,0,'ubuntu16-02\n'),('poll','22',1499437502,1.56,1,'ubuntu16\n'),('poll','25',1499437503,1.34,1,'ubuntu16-02\n'),('poll','24',1499437503,1.34,1,'ubuntu16-02\n'),('poll','23',1499437503,1.94,1,'ubuntu16-02\n'),('poll','28',1499437502,4.17,1,'ubuntu16\n'),('poll','27',1499437503,4.49,1,'ubuntu16-02\n'),('pollbill','',1499437802,1.50,0,'ubuntu16-02\n'),('pollbill','',1499437802,0.87,0,'ubuntu16\n'),('poll','25',1499437803,1.52,1,'ubuntu16-02\n'),('poll','24',1499437803,1.52,1,'ubuntu16-02\n'),('poll','22',1499437803,1.57,1,'ubuntu16\n'),('poll','23',1499437803,1.98,1,'ubuntu16-02\n'),('poll','28',1499437803,4.01,1,'ubuntu16\n'),('poll','27',1499437803,4.48,1,'ubuntu16-02\n'),('pollbill','',1499438102,0.71,0,'ubuntu16\n'),('pollbill','',1499438101,1.56,0,'ubuntu16-02\n'),('poll','22',1499438102,1.55,1,'ubuntu16\n'),('poll','24',1499438103,1.51,1,'ubuntu16-02\n'),('poll','25',1499438103,1.33,1,'ubuntu16-02\n'),('poll','23',1499438103,1.81,1,'ubuntu16-02\n'),('poll','28',1499438102,4.09,1,'ubuntu16\n'),('poll','27',1499438103,4.35,1,'ubuntu16-02\n'),('pollbill','',1499438401,1.38,0,'ubuntu16-02\n'),('pollbill','',1499438403,0.78,0,'ubuntu16\n'),('poll','24',1499438402,1.68,1,'ubuntu16-02\n'),('poll','25',1499438403,1.24,1,'ubuntu16-02\n'),('poll','22',1499438403,1.53,1,'ubuntu16\n'),('poll','23',1499438403,1.99,1,'ubuntu16-02\n'),('poll','28',1499438403,4.24,1,'ubuntu16\n'),('poll','27',1499438403,4.61,1,'ubuntu16-02\n'),('pollbill','',1499438702,0.69,0,'ubuntu16\n'),('pollbill','',1499438702,0.95,0,'ubuntu16-02\n'),('poll','22',1499438702,1.47,1,'ubuntu16\n'),('poll','28',1499438702,1.84,1,'ubuntu16\n'),('poll','24',1499438703,1.24,1,'ubuntu16-02\n'),('poll','25',1499438703,1.27,1,'ubuntu16-02\n'),('poll','23',1499438703,1.64,1,'ubuntu16-02\n'),('poll','27',1499438703,4.17,1,'ubuntu16-02\n'),('pollbill','',1499439002,1.11,0,'ubuntu16\n'),('pollbill','',1499439002,1.14,0,'ubuntu16-02\n'),('poll','22',1499439002,1.73,1,'ubuntu16\n'),('poll','24',1499439003,1.35,1,'ubuntu16-02\n'),('poll','25',1499439003,1.27,1,'ubuntu16-02\n'),('poll','23',1499439003,2.06,1,'ubuntu16-02\n'),('poll','27',1499439003,2.27,1,'ubuntu16-02\n'),('poll','28',1499439002,4.10,1,'ubuntu16\n'),('pollbill','',1499439302,0.71,0,'ubuntu16\n'),('pollbill','',1499439302,1.29,0,'ubuntu16-02\n'),('poll','25',1499439303,1.36,1,'ubuntu16-02\n'),('poll','24',1499439303,1.23,1,'ubuntu16-02\n'),('poll','22',1499439303,1.40,1,'ubuntu16\n'),('poll','23',1499439303,1.78,1,'ubuntu16-02\n'),('poll','28',1499439303,4.00,1,'ubuntu16\n'),('poll','27',1499439303,4.22,1,'ubuntu16-02\n'),('pollbill','',1499439601,1.12,0,'ubuntu16-02\n'),('pollbill','',1499439602,0.63,0,'ubuntu16\n'),('poll','22',1499439602,1.27,1,'ubuntu16\n'),('poll','24',1499439602,1.29,1,'ubuntu16-02\n'),('poll','25',1499439602,1.27,1,'ubuntu16-02\n'),('poll','23',1499439602,1.48,1,'ubuntu16-02\n'),('poll','28',1499439602,3.64,1,'ubuntu16\n'),('poll','27',1499439602,3.87,1,'ubuntu16-02\n'),('pollbill','',1499439902,0.60,0,'ubuntu16\n'),('pollbill','',1499439902,0.83,0,'ubuntu16-02\n'),('poll','22',1499439902,1.38,1,'ubuntu16\n'),('poll','25',1499439903,1.24,1,'ubuntu16-02\n'),('poll','24',1499439903,1.27,1,'ubuntu16-02\n'),('poll','23',1499439903,1.51,1,'ubuntu16-02\n'),('poll','28',1499439902,3.71,1,'ubuntu16\n'),('poll','27',1499439903,3.99,1,'ubuntu16-02\n'),('pollbill','',1499440202,0.57,0,'ubuntu16\n'),('pollbill','',1499440202,0.78,0,'ubuntu16-02\n'),('poll','22',1499440202,1.34,1,'ubuntu16\n'),('poll','24',1499440203,1.28,1,'ubuntu16-02\n'),('poll','25',1499440203,1.28,1,'ubuntu16-02\n'),('poll','23',1499440203,1.44,1,'ubuntu16-02\n'),('poll','28',1499440202,3.79,1,'ubuntu16\n'),('poll','27',1499440203,4.02,1,'ubuntu16-02\n'),('pollbill','',1499440502,0.51,0,'ubuntu16\n'),('pollbill','',1499440501,1.09,0,'ubuntu16-02\n'),('poll','22',1499440502,1.20,1,'ubuntu16\n'),('poll','24',1499440502,1.27,1,'ubuntu16-02\n'),('poll','25',1499440502,1.29,1,'ubuntu16-02\n'),('poll','23',1499440503,1.36,1,'ubuntu16-02\n'),('poll','28',1499440502,3.72,1,'ubuntu16\n'),('poll','27',1499440502,3.91,1,'ubuntu16-02\n'),('pollbill','',1499440802,0.52,0,'ubuntu16\n'),('pollbill','',1499440802,0.70,0,'ubuntu16-02\n'),('poll','22',1499440802,1.32,1,'ubuntu16\n'),('poll','24',1499440803,1.22,1,'ubuntu16-02\n'),('poll','25',1499440803,1.21,1,'ubuntu16-02\n'),('poll','23',1499440803,1.42,1,'ubuntu16-02\n'),('poll','28',1499440802,3.74,1,'ubuntu16\n'),('poll','27',1499440803,4.32,1,'ubuntu16-02\n'),('pollbill','',1499441102,0.60,0,'ubuntu16\n'),('pollbill','',1499441102,0.72,0,'ubuntu16-02\n'),('poll','22',1499441102,1.24,1,'ubuntu16\n'),('poll','25',1499441103,1.26,1,'ubuntu16-02\n'),('poll','24',1499441103,1.28,1,'ubuntu16-02\n'),('poll','23',1499441103,1.46,1,'ubuntu16-02\n'),('poll','28',1499441102,3.68,1,'ubuntu16\n'),('poll','27',1499441103,4.07,1,'ubuntu16-02\n'),('pollbill','',1499441402,0.42,0,'ubuntu16\n'),('pollbill','',1499441402,0.63,0,'ubuntu16-02\n'),('poll','22',1499441402,1.25,1,'ubuntu16\n'),('poll','24',1499441403,1.17,1,'ubuntu16-02\n'),('poll','25',1499441403,1.19,1,'ubuntu16-02\n'),('poll','23',1499441403,1.34,1,'ubuntu16-02\n'),('poll','28',1499441402,3.63,1,'ubuntu16\n'),('poll','27',1499441403,4.24,1,'ubuntu16-02\n'),('pollbill','',1499441702,0.50,0,'ubuntu16\n'),('pollbill','',1499441702,0.65,0,'ubuntu16-02\n'),('poll','22',1499441702,1.23,1,'ubuntu16\n'),('poll','25',1499441702,1.21,1,'ubuntu16-02\n'),('poll','24',1499441702,1.21,1,'ubuntu16-02\n'),('poll','23',1499441702,1.37,1,'ubuntu16-02\n'),('poll','28',1499441702,3.68,1,'ubuntu16\n'),('poll','27',1499441702,3.88,1,'ubuntu16-02\n'),('pollbill','',1499442002,0.70,0,'ubuntu16\n'),('pollbill','',1499442002,0.92,0,'ubuntu16-02\n'),('poll','22',1499442002,1.34,1,'ubuntu16\n'),('poll','24',1499442003,1.38,1,'ubuntu16-02\n'),('poll','25',1499442003,1.29,1,'ubuntu16-02\n'),('poll','23',1499442003,1.45,1,'ubuntu16-02\n'),('poll','28',1499442002,3.86,1,'ubuntu16\n'),('poll','27',1499442003,4.04,1,'ubuntu16-02\n'),('pollbill','',1499442302,0.67,0,'ubuntu16\n'),('pollbill','',1499442301,1.50,0,'ubuntu16-02\n'),('poll','22',1499442302,1.31,1,'ubuntu16\n'),('poll','24',1499442302,1.37,1,'ubuntu16-02\n'),('poll','25',1499442302,1.29,1,'ubuntu16-02\n'),('poll','23',1499442302,1.76,1,'ubuntu16-02\n'),('poll','28',1499442302,3.89,1,'ubuntu16\n'),('poll','27',1499442302,4.30,1,'ubuntu16-02\n'),('pollbill','',1499442601,0.69,0,'ubuntu16\n'),('pollbill','',1499442602,0.80,0,'ubuntu16-02\n'),('poll','22',1499442602,1.32,1,'ubuntu16\n'),('poll','25',1499442603,1.26,1,'ubuntu16-02\n'),('poll','24',1499442603,1.30,1,'ubuntu16-02\n'),('poll','23',1499442603,1.51,1,'ubuntu16-02\n'),('poll','28',1499442602,4.04,1,'ubuntu16\n'),('poll','27',1499442603,4.11,1,'ubuntu16-02\n'),('pollbill','',1499442902,0.60,0,'ubuntu16\n'),('pollbill','',1499442902,0.76,0,'ubuntu16-02\n'),('poll','22',1499442902,1.22,1,'ubuntu16\n'),('poll','25',1499442902,1.19,1,'ubuntu16-02\n'),('poll','24',1499442902,1.20,1,'ubuntu16-02\n'),('poll','23',1499442902,1.64,1,'ubuntu16-02\n'),('poll','27',1499442902,1.84,1,'ubuntu16-02\n'),('poll','28',1499442902,3.70,1,'ubuntu16\n'),('pollbill','',1499443201,0.52,0,'ubuntu16\n'),('pollbill','',1499443202,0.77,0,'ubuntu16-02\n'),('poll','22',1499443202,1.40,1,'ubuntu16\n'),('poll','24',1499443203,1.20,1,'ubuntu16-02\n'),('poll','25',1499443203,1.21,1,'ubuntu16-02\n'),('poll','23',1499443203,1.49,1,'ubuntu16-02\n'),('poll','28',1499443202,3.73,1,'ubuntu16\n'),('poll','27',1499443203,3.96,1,'ubuntu16-02\n'),('pollbill','',1499443502,0.58,0,'ubuntu16\n'),('pollbill','',1499443502,0.73,0,'ubuntu16-02\n'),('poll','22',1499443502,1.37,1,'ubuntu16\n'),('poll','24',1499443503,1.23,1,'ubuntu16-02\n'),('poll','25',1499443503,1.21,1,'ubuntu16-02\n'),('poll','23',1499443503,1.54,1,'ubuntu16-02\n'),('poll','28',1499443502,4.29,1,'ubuntu16\n'),('poll','27',1499443503,4.03,1,'ubuntu16-02\n'),('pollbill','',1499443802,0.86,0,'ubuntu16\n'),('pollbill','',1499443802,1.54,0,'ubuntu16-02\n'),('poll','22',1499443802,1.82,1,'ubuntu16\n'),('poll','25',1499443803,1.40,1,'ubuntu16-02\n'),('poll','24',1499443803,1.50,1,'ubuntu16-02\n'),('poll','23',1499443803,1.90,1,'ubuntu16-02\n'),('poll','28',1499443802,4.17,1,'ubuntu16\n'),('poll','27',1499443803,4.31,1,'ubuntu16-02\n'),('pollbill','',1499444102,0.64,0,'ubuntu16\n'),('pollbill','',1499444102,1.15,0,'ubuntu16-02\n'),('poll','22',1499444102,1.36,1,'ubuntu16\n'),('poll','25',1499444103,1.26,1,'ubuntu16-02\n'),('poll','24',1499444103,1.22,1,'ubuntu16-02\n'),('poll','23',1499444103,1.60,1,'ubuntu16-02\n'),('poll','28',1499444102,3.85,1,'ubuntu16\n'),('poll','27',1499444103,4.02,1,'ubuntu16-02\n'),('pollbill','',1499444401,1.36,0,'ubuntu16-02\n'),('pollbill','',1499444402,0.69,0,'ubuntu16\n'),('poll','24',1499444402,1.51,1,'ubuntu16-02\n'),('poll','25',1499444402,1.76,1,'ubuntu16-02\n'),('poll','22',1499444402,1.64,1,'ubuntu16\n'),('poll','28',1499444402,1.90,1,'ubuntu16\n'),('poll','23',1499444402,2.62,1,'ubuntu16-02\n'),('poll','27',1499444402,2.88,1,'ubuntu16-02\n'),('pollbill','',1499444702,0.64,0,'ubuntu16\n'),('pollbill','',1499444702,1.14,0,'ubuntu16-02\n'),('poll','22',1499444702,1.32,1,'ubuntu16\n'),('poll','25',1499444703,1.24,1,'ubuntu16-02\n'),('poll','24',1499444703,1.21,1,'ubuntu16-02\n'),('poll','23',1499444703,1.58,1,'ubuntu16-02\n'),('poll','28',1499444702,3.86,1,'ubuntu16\n'),('poll','27',1499444703,3.97,1,'ubuntu16-02\n'),('pollbill','',1499445001,0.89,0,'ubuntu16-02\n'),('pollbill','',1499445002,0.42,0,'ubuntu16\n'),('poll','24',1499445002,1.55,1,'ubuntu16-02\n'),('poll','25',1499445002,1.54,1,'ubuntu16-02\n'),('poll','23',1499445002,1.82,1,'ubuntu16-02\n'),('poll','22',1499445002,1.25,1,'ubuntu16\n'),('poll','27',1499445002,4.09,1,'ubuntu16-02\n'),('poll','28',1499445002,3.71,1,'ubuntu16\n'),('pollbill','',1499445302,0.97,0,'ubuntu16\n'),('pollbill','',1499445302,1.59,0,'ubuntu16-02\n'),('poll','25',1499445303,1.15,1,'ubuntu16-02\n'),('poll','22',1499445302,1.52,1,'ubuntu16\n'),('poll','24',1499445303,1.43,1,'ubuntu16-02\n'),('poll','23',1499445303,1.63,1,'ubuntu16-02\n'),('poll','28',1499445302,3.96,1,'ubuntu16\n'),('poll','27',1499445303,4.03,1,'ubuntu16-02\n'),('pollbill','',1499445601,0.87,0,'ubuntu16\n'),('pollbill','',1499445602,0.93,0,'ubuntu16-02\n'),('poll','22',1499445602,1.38,1,'ubuntu16\n'),('poll','25',1499445603,1.34,1,'ubuntu16-02\n'),('poll','24',1499445603,1.35,1,'ubuntu16-02\n'),('poll','23',1499445603,1.60,1,'ubuntu16-02\n'),('poll','28',1499445602,3.80,1,'ubuntu16\n'),('poll','27',1499445603,4.03,1,'ubuntu16-02\n'),('poll','25',1499445907,16.80,1,'ubuntu16-02\n'),('poll','24',1499445909,16.15,1,'ubuntu16-02\n'),('poll','27',1499445910,23.31,1,'ubuntu16-02\n'),('poll','23',1499445918,16.68,1,'ubuntu16-02\n'),('pollbill','',1499445902,38.18,0,'ubuntu16-02\n'),('pollbill','',1499445938,2.77,0,'ubuntu16\n'),('poll','22',1499445941,1.25,1,'ubuntu16\n'),('poll','28',1499445941,3.98,1,'ubuntu16\n'),('pollbill','',1499446201,1.17,0,'ubuntu16\n'),('pollbill','',1499446202,1.33,0,'ubuntu16-02\n'),('poll','22',1499446202,1.94,1,'ubuntu16\n'),('poll','24',1499446203,1.33,1,'ubuntu16-02\n'),('poll','25',1499446203,1.47,1,'ubuntu16-02\n'),('poll','23',1499446203,2.08,1,'ubuntu16-02\n'),('poll','28',1499446202,4.06,1,'ubuntu16\n'),('poll','27',1499446203,4.16,1,'ubuntu16-02\n'),('poll','24',1499446505,6.69,1,'ubuntu16-02\n'),('poll','25',1499446506,12.16,1,'ubuntu16-02\n'),('poll','23',1499446506,22.39,1,'ubuntu16-02\n'),('poll','27',1499446506,21.68,1,'ubuntu16-02\n'),('pollbill','',1499446504,24.04,0,'ubuntu16-02\n'),('pollbill','',1499446504,24.05,0,'ubuntu16\n'),('poll','22',1499446528,1.53,1,'ubuntu16\n'),('poll','28',1499446527,4.54,1,'ubuntu16\n'),('poll','24',1499446806,4.29,1,'ubuntu16-02\n'),('poll','25',1499446804,6.55,1,'ubuntu16-02\n'),('poll','23',1499446805,10.59,1,'ubuntu16-02\n'),('poll','27',1499446804,11.47,1,'ubuntu16-02\n'),('pollbill','',1499446802,13.42,0,'ubuntu16-02\n'),('pollbill','',1499446802,13.77,0,'ubuntu16\n'),('poll','22',1499446803,13.71,1,'ubuntu16\n'),('poll','28',1499446803,16.23,1,'ubuntu16\n'),('pollbill','',1499447102,0.70,0,'ubuntu16\n'),('pollbill','',1499447101,1.58,0,'ubuntu16-02\n'),('poll','24',1499447102,1.55,1,'ubuntu16-02\n'),('poll','25',1499447102,1.44,1,'ubuntu16-02\n'),('poll','22',1499447102,1.62,1,'ubuntu16\n'),('poll','23',1499447102,1.88,1,'ubuntu16-02\n'),('poll','27',1499447102,2.14,1,'ubuntu16-02\n'),('poll','28',1499447102,3.82,1,'ubuntu16\n'),('poll','25',1499447410,7.38,1,'ubuntu16-02\n'),('poll','24',1499447411,11.00,1,'ubuntu16-02\n'),('poll','27',1499447410,19.99,1,'ubuntu16-02\n'),('poll','23',1499447418,16.95,1,'ubuntu16-02\n'),('pollbill','',1499447403,32.48,0,'ubuntu16-02\n'),('pollbill','',1499447420,16.58,0,'ubuntu16\n'),('poll','22',1499447436,1.38,1,'ubuntu16\n'),('poll','28',1499447435,4.67,1,'ubuntu16\n'),('pollbill','',1499447701,5.09,0,'ubuntu16-02\n'),('pollbill','',1499447702,4.77,0,'ubuntu16\n'),('poll','22',1499447702,5.51,1,'ubuntu16\n'),('poll','28',1499447702,8.32,1,'ubuntu16\n'),('poll','25',1499448003,19.14,1,'ubuntu16-02\n'),('poll','24',1499448004,32.05,1,'ubuntu16-02\n'),('poll','23',1499448004,53.24,1,'ubuntu16-02\n'),('poll','27',1499448004,53.49,1,'ubuntu16-02\n'),('pollbill','',1499448002,55.35,0,'ubuntu16-02\n'),('pollbill','',1499448045,12.26,0,'ubuntu16\n'),('poll','22',1499448057,1.29,1,'ubuntu16\n'),('poll','28',1499448057,3.96,1,'ubuntu16\n'),('pollbill','',1499448302,0.62,0,'ubuntu16\n'),('pollbill','',1499448302,1.16,0,'ubuntu16-02\n'),('poll','22',1499448302,1.66,1,'ubuntu16\n'),('poll','24',1499448303,1.42,1,'ubuntu16-02\n'),('poll','25',1499448303,1.33,1,'ubuntu16-02\n'),('poll','23',1499448303,1.69,1,'ubuntu16-02\n'),('poll','28',1499448302,4.06,1,'ubuntu16\n'),('poll','27',1499448303,4.17,1,'ubuntu16-02\n'),('pollbill','',1499448602,0.72,0,'ubuntu16\n'),('pollbill','',1499448602,1.38,0,'ubuntu16-02\n'),('poll','22',1499448602,1.49,1,'ubuntu16\n'),('poll','24',1499448603,1.44,1,'ubuntu16-02\n'),('poll','25',1499448603,1.45,1,'ubuntu16-02\n'),('poll','23',1499448603,1.84,1,'ubuntu16-02\n'),('poll','28',1499448602,3.95,1,'ubuntu16\n'),('poll','27',1499448603,4.34,1,'ubuntu16-02\n'),('poll','24',1499448902,1.68,1,'ubuntu16-02\n'),('poll','25',1499448902,1.62,1,'ubuntu16-02\n'),('pollbill','',1499448901,2.86,0,'ubuntu16-02\n'),('poll','23',1499448902,2.48,1,'ubuntu16-02\n'),('pollbill','',1499448904,0.56,0,'ubuntu16\n'),('poll','22',1499448904,1.62,1,'ubuntu16\n'),('poll','28',1499448904,1.84,1,'ubuntu16\n'),('poll','27',1499448902,4.67,1,'ubuntu16-02\n'),('poll','24',1499449206,2.86,1,'ubuntu16-02\n'),('poll','25',1499449206,2.89,1,'ubuntu16-02\n'),('poll','22',1499449202,7.98,1,'ubuntu16\n'),('poll','23',1499449206,4.23,1,'ubuntu16-02\n'),('pollbill','',1499449203,6.93,0,'ubuntu16-02\n'),('pollbill','',1499449202,8.35,0,'ubuntu16\n'),('poll','27',1499449204,6.80,1,'ubuntu16-02\n'),('poll','28',1499449202,10.91,1,'ubuntu16\n'),('pollbill','',1499449502,0.88,0,'ubuntu16\n'),('pollbill','',1499449501,1.65,0,'ubuntu16-02\n'),('poll','22',1499449502,1.54,1,'ubuntu16\n'),('poll','24',1499449502,1.56,1,'ubuntu16-02\n'),('poll','25',1499449503,1.37,1,'ubuntu16-02\n'),('poll','23',1499449502,2.20,1,'ubuntu16-02\n'),('poll','28',1499449502,4.46,1,'ubuntu16\n'),('poll','27',1499449502,4.47,1,'ubuntu16-02\n'),('poll','25',1499449806,8.13,1,'ubuntu16-02\n'),('poll','24',1499449809,5.68,1,'ubuntu16-02\n'),('poll','27',1499449805,11.83,1,'ubuntu16-02\n'),('poll','23',1499449807,10.54,1,'ubuntu16-02\n'),('pollbill','',1499449802,15.33,0,'ubuntu16-02\n'),('pollbill','',1499449817,1.27,0,'ubuntu16\n'),('poll','22',1499449817,2.12,1,'ubuntu16\n'),('poll','28',1499449817,4.62,1,'ubuntu16\n'),('pollbill','',1499450102,1.46,0,'ubuntu16-02\n'),('pollbill','',1499450102,0.90,0,'ubuntu16\n'),('poll','25',1499450102,1.62,1,'ubuntu16-02\n'),('poll','24',1499450102,1.58,1,'ubuntu16-02\n'),('poll','22',1499450103,1.36,1,'ubuntu16\n'),('poll','23',1499450102,1.86,1,'ubuntu16-02\n'),('poll','28',1499450103,3.98,1,'ubuntu16\n'),('poll','27',1499450102,4.46,1,'ubuntu16-02\n'),('pollbill','',1499450402,0.56,0,'ubuntu16\n'),('pollbill','',1499450402,0.83,0,'ubuntu16-02\n'),('poll','22',1499450402,1.43,1,'ubuntu16\n'),('poll','24',1499450403,1.27,1,'ubuntu16-02\n'),('poll','25',1499450403,1.26,1,'ubuntu16-02\n'),('poll','23',1499450403,1.57,1,'ubuntu16-02\n'),('poll','28',1499450402,3.79,1,'ubuntu16\n'),('poll','27',1499450403,4.04,1,'ubuntu16-02\n'),('pollbill','',1499450702,0.60,0,'ubuntu16\n'),('pollbill','',1499450701,1.40,0,'ubuntu16-02\n'),('poll','22',1499450702,1.33,1,'ubuntu16\n'),('poll','25',1499450702,1.59,1,'ubuntu16-02\n'),('poll','24',1499450702,1.59,1,'ubuntu16-02\n'),('poll','23',1499450703,1.52,1,'ubuntu16-02\n'),('poll','28',1499450702,3.81,1,'ubuntu16\n'),('poll','27',1499450702,4.38,1,'ubuntu16-02\n'),('pollbill','',1499451002,0.59,0,'ubuntu16\n'),('pollbill','',1499451002,1.08,0,'ubuntu16-02\n'),('poll','22',1499451002,1.41,1,'ubuntu16\n'),('poll','24',1499451003,1.43,1,'ubuntu16-02\n'),('poll','25',1499451003,1.27,1,'ubuntu16-02\n'),('poll','23',1499451003,1.78,1,'ubuntu16-02\n'),('poll','28',1499451002,3.85,1,'ubuntu16\n'),('poll','27',1499451003,4.16,1,'ubuntu16-02\n'),('poll','25',1499451304,9.66,1,'ubuntu16-02\n'),('poll','24',1499451305,9.83,1,'ubuntu16-02\n'),('poll','27',1499451304,19.46,1,'ubuntu16-02\n'),('pollbill','',1499451301,21.56,0,'ubuntu16-02\n'),('poll','23',1499451304,19.69,1,'ubuntu16-02\n'),('pollbill','',1499451322,2.01,0,'ubuntu16\n'),('poll','22',1499451319,4.63,1,'ubuntu16\n'),('poll','28',1499451323,4.73,1,'ubuntu16\n'),('poll','24',1499451604,1.56,1,'ubuntu16-02\n'),('poll','25',1499451604,1.49,1,'ubuntu16-02\n'),('pollbill','',1499451602,5.20,0,'ubuntu16\n'),('pollbill','',1499451603,4.61,0,'ubuntu16-02\n'),('poll','22',1499451602,6.12,1,'ubuntu16\n'),('poll','23',1499451604,3.90,1,'ubuntu16-02\n'),('poll','28',1499451602,6.99,1,'ubuntu16\n'),('poll','27',1499451604,5.34,1,'ubuntu16-02\n'),('poll','24',1499451903,1.62,1,'ubuntu16-02\n'),('poll','25',1499451903,1.62,1,'ubuntu16-02\n'),('pollbill','',1499451902,3.63,0,'ubuntu16-02\n'),('poll','23',1499451903,3.11,1,'ubuntu16-02\n'),('pollbill','',1499451906,0.70,0,'ubuntu16\n'),('poll','22',1499451906,1.50,1,'ubuntu16\n'),('poll','27',1499451903,4.52,1,'ubuntu16-02\n'),('poll','28',1499451906,4.44,1,'ubuntu16\n'),('pollbill','',1499452202,0.66,0,'ubuntu16\n'),('pollbill','',1499452202,1.31,0,'ubuntu16-02\n'),('poll','22',1499452203,1.33,1,'ubuntu16\n'),('poll','24',1499452203,1.36,1,'ubuntu16-02\n'),('poll','25',1499452203,1.37,1,'ubuntu16-02\n'),('poll','23',1499452203,1.77,1,'ubuntu16-02\n'),('poll','28',1499452203,3.80,1,'ubuntu16\n'),('poll','27',1499452203,4.20,1,'ubuntu16-02\n'),('pollbill','',1499452501,1.19,0,'ubuntu16-02\n'),('pollbill','',1499452502,0.51,0,'ubuntu16\n'),('poll','24',1499452502,1.58,1,'ubuntu16-02\n'),('poll','25',1499452502,1.67,1,'ubuntu16-02\n'),('poll','22',1499452502,1.62,1,'ubuntu16\n'),('poll','23',1499452502,1.89,1,'ubuntu16-02\n'),('poll','28',1499452502,3.80,1,'ubuntu16\n'),('poll','27',1499452502,4.19,1,'ubuntu16-02\n'),('poll','24',1499452807,16.75,1,'ubuntu16-02\n'),('poll','25',1499452808,16.08,1,'ubuntu16-02\n'),('poll','27',1499452812,27.11,1,'ubuntu16-02\n'),('poll','23',1499452815,25.34,1,'ubuntu16-02\n'),('pollbill','',1499452801,45.03,0,'ubuntu16-02\n'),('pollbill','',1499452837,17.02,0,'ubuntu16\n'),('poll','22',1499452843,13.28,1,'ubuntu16\n'),('poll','28',1499452854,5.20,1,'ubuntu16\n'),('poll','25',1499453108,10.01,1,'ubuntu16-02\n'),('poll','24',1499453107,14.30,1,'ubuntu16-02\n'),('poll','23',1499453106,27.99,1,'ubuntu16-02\n'),('poll','27',1499453106,28.76,1,'ubuntu16-02\n'),('pollbill','',1499453102,35.70,0,'ubuntu16-02\n'),('pollbill','',1499453123,15.36,0,'ubuntu16\n'),('poll','22',1499453127,12.00,1,'ubuntu16\n'),('poll','28',1499453127,14.28,1,'ubuntu16\n'),('poll','22',1499472603,9.95,1,'ubuntu16\n'),('poll','28',1499472603,12.13,1,'ubuntu16\n'),('pollbill','',1499472602,33.92,0,'ubuntu16\n'),('poll','22',1499472903,1.78,1,'ubuntu16\n'),('poll','28',1499472903,4.50,1,'ubuntu16\n'),('pollbill','',1499472902,24.68,0,'ubuntu16\n'),('pollbill','',1499473202,0.52,0,'ubuntu16\n'),('poll','22',1499473203,1.38,1,'ubuntu16\n'),('poll','28',1499473203,2.23,1,'ubuntu16\n'),('poll','25',1499473206,3.07,1,'ubuntu16-02\n'),('poll','24',1499473206,3.10,1,'ubuntu16-02\n'),('pollbill','',1499473203,10.14,0,'ubuntu16-02\n'),('poll','23',1499473206,8.61,1,'ubuntu16-02\n'),('poll','27',1499473206,10.88,1,'ubuntu16-02\n'),('pollbill','',1499473502,0.88,0,'ubuntu16\n'),('pollbill','',1499473502,1.10,0,'ubuntu16-02\n'),('poll','22',1499473502,1.69,1,'ubuntu16\n'),('poll','25',1499473503,1.41,1,'ubuntu16-02\n'),('poll','24',1499473503,1.49,1,'ubuntu16-02\n'),('poll','23',1499473503,1.89,1,'ubuntu16-02\n'),('poll','28',1499473502,3.96,1,'ubuntu16\n'),('poll','27',1499473503,5.18,1,'ubuntu16-02\n'),('pollbill','',1499473802,1.15,0,'ubuntu16\n'),('pollbill','',1499473802,1.73,0,'ubuntu16-02\n'),('poll','25',1499473803,1.56,1,'ubuntu16-02\n'),('poll','22',1499473803,1.74,1,'ubuntu16\n'),('poll','24',1499473803,1.30,1,'ubuntu16-02\n'),('poll','23',1499473803,2.60,1,'ubuntu16-02\n'),('poll','28',1499473803,4.18,1,'ubuntu16\n'),('poll','27',1499473803,5.03,1,'ubuntu16-02\n'),('pollbill','',1499474101,0.69,0,'ubuntu16\n'),('pollbill','',1499474102,0.93,0,'ubuntu16-02\n'),('poll','22',1499474102,1.75,1,'ubuntu16\n'),('poll','24',1499474103,1.30,1,'ubuntu16-02\n'),('poll','25',1499474103,1.32,1,'ubuntu16-02\n'),('poll','23',1499474103,1.78,1,'ubuntu16-02\n'),('poll','28',1499474102,4.06,1,'ubuntu16\n'),('poll','27',1499474103,4.62,1,'ubuntu16-02\n'),('pollbill','',1499474402,0.87,0,'ubuntu16\n'),('pollbill','',1499474402,1.04,0,'ubuntu16-02\n'),('poll','22',1499474402,1.33,1,'ubuntu16\n'),('poll','24',1499474403,1.45,1,'ubuntu16-02\n'),('poll','25',1499474403,1.39,1,'ubuntu16-02\n'),('poll','23',1499474403,1.75,1,'ubuntu16-02\n'),('poll','28',1499474402,4.22,1,'ubuntu16\n'),('poll','27',1499474403,4.27,1,'ubuntu16-02\n'),('pollbill','',1499474702,1.50,0,'ubuntu16\n'),('pollbill','',1499474701,2.34,0,'ubuntu16-02\n'),('poll','24',1499474702,1.65,1,'ubuntu16-02\n'),('poll','25',1499474702,1.61,1,'ubuntu16-02\n'),('poll','22',1499474702,1.99,1,'ubuntu16\n'),('poll','23',1499474702,2.26,1,'ubuntu16-02\n'),('poll','28',1499474702,5.64,1,'ubuntu16\n'),('poll','27',1499474702,5.89,1,'ubuntu16-02\n'),('pollbill','',1499475002,0.76,0,'ubuntu16\n'),('pollbill','',1499475002,1.09,0,'ubuntu16-02\n'),('poll','22',1499475002,1.35,1,'ubuntu16\n'),('poll','25',1499475003,1.35,1,'ubuntu16-02\n'),('poll','24',1499475003,1.44,1,'ubuntu16-02\n'),('poll','23',1499475003,1.46,1,'ubuntu16-02\n'),('poll','28',1499475002,3.84,1,'ubuntu16\n'),('poll','27',1499475003,3.96,1,'ubuntu16-02\n'),('pollbill','',1499475302,0.81,0,'ubuntu16\n'),('pollbill','',1499475302,1.60,0,'ubuntu16-02\n'),('poll','24',1499475302,1.80,1,'ubuntu16-02\n'),('poll','25',1499475302,1.78,1,'ubuntu16-02\n'),('poll','22',1499475303,1.65,1,'ubuntu16\n'),('poll','23',1499475303,2.06,1,'ubuntu16-02\n'),('poll','28',1499475303,3.83,1,'ubuntu16\n'),('poll','27',1499475302,4.63,1,'ubuntu16-02\n'),('pollbill','',1499475602,0.71,0,'ubuntu16\n'),('pollbill','',1499475602,0.89,0,'ubuntu16-02\n'),('poll','22',1499475603,1.40,1,'ubuntu16\n'),('poll','24',1499475603,1.33,1,'ubuntu16-02\n'),('poll','25',1499475603,1.30,1,'ubuntu16-02\n'),('poll','23',1499475603,1.70,1,'ubuntu16-02\n'),('poll','28',1499475602,3.92,1,'ubuntu16\n'),('poll','27',1499475603,4.09,1,'ubuntu16-02\n'),('pollbill','',1499475902,0.57,0,'ubuntu16\n'),('pollbill','',1499475902,1.03,0,'ubuntu16-02\n'),('poll','22',1499475902,1.70,1,'ubuntu16\n'),('poll','24',1499475903,1.28,1,'ubuntu16-02\n'),('poll','25',1499475903,1.24,1,'ubuntu16-02\n'),('poll','23',1499475903,1.52,1,'ubuntu16-02\n'),('poll','28',1499475902,3.82,1,'ubuntu16\n'),('poll','27',1499475903,4.05,1,'ubuntu16-02\n'),('pollbill','',1499476202,0.63,0,'ubuntu16\n'),('pollbill','',1499476202,0.84,0,'ubuntu16-02\n'),('poll','22',1499476202,1.38,1,'ubuntu16\n'),('poll','25',1499476203,1.28,1,'ubuntu16-02\n'),('poll','24',1499476203,1.24,1,'ubuntu16-02\n'),('poll','23',1499476203,1.46,1,'ubuntu16-02\n'),('poll','28',1499476202,3.87,1,'ubuntu16\n'),('poll','27',1499476203,4.02,1,'ubuntu16-02\n'),('poll','24',1499589635,13.98,1,'ubuntu16-02\n'),('poll','25',1499589635,13.97,1,'ubuntu16-02\n'),('poll','23',1499589635,45.16,1,'ubuntu16-02\n'),('poll','27',1499589635,47.12,1,'ubuntu16-02\n'),('pollbill','',1499589902,1.01,0,'ubuntu16-02\n'),('pollbill','',1499589902,8.43,0,'ubuntu16\n'),('poll','22',1499589903,9.18,1,'ubuntu16\n'),('poll','28',1499589903,11.22,1,'ubuntu16\n'),('pollbill','',1499590202,0.77,0,'ubuntu16\n'),('pollbill','',1499590202,1.14,0,'ubuntu16-02\n'),('poll','22',1499590202,1.66,1,'ubuntu16\n'),('poll','24',1499590203,1.38,1,'ubuntu16-02\n'),('poll','25',1499590203,1.33,1,'ubuntu16-02\n'),('poll','23',1499590203,2.03,1,'ubuntu16-02\n'),('poll','28',1499590202,4.17,1,'ubuntu16\n'),('poll','27',1499590203,4.80,1,'ubuntu16-02\n'),('pollbill','',1499590502,1.11,0,'ubuntu16\n'),('pollbill','',1499590503,1.34,0,'ubuntu16-02\n'),('poll','22',1499590503,1.80,1,'ubuntu16\n'),('poll','24',1499590504,1.48,1,'ubuntu16-02\n'),('poll','25',1499590504,1.36,1,'ubuntu16-02\n'),('poll','23',1499590504,1.58,1,'ubuntu16-02\n'),('poll','28',1499590503,4.16,1,'ubuntu16\n'),('poll','27',1499590504,4.37,1,'ubuntu16-02\n'),('pollbill','',1499590802,0.81,0,'ubuntu16\n'),('pollbill','',1499590802,1.00,0,'ubuntu16-02\n'),('poll','22',1499590802,1.66,1,'ubuntu16\n'),('poll','24',1499590803,1.27,1,'ubuntu16-02\n'),('poll','25',1499590803,1.27,1,'ubuntu16-02\n'),('poll','23',1499590803,2.03,1,'ubuntu16-02\n'),('poll','28',1499590802,4.12,1,'ubuntu16\n'),('poll','27',1499590803,5.18,1,'ubuntu16-02\n'),('pollbill','',1499591102,0.79,0,'ubuntu16\n'),('pollbill','',1499591102,1.20,0,'ubuntu16-02\n'),('poll','22',1499591102,1.90,1,'ubuntu16\n'),('poll','25',1499591104,1.50,1,'ubuntu16-02\n'),('poll','24',1499591104,1.55,1,'ubuntu16-02\n'),('poll','23',1499591103,2.43,1,'ubuntu16-02\n'),('poll','27',1499591104,2.98,1,'ubuntu16-02\n'),('poll','28',1499591102,4.43,1,'ubuntu16\n'),('pollbill','',1499591402,1.11,0,'ubuntu16\n'),('pollbill','',1499591401,1.64,0,'ubuntu16-02\n'),('poll','22',1499591402,1.93,1,'ubuntu16\n'),('poll','25',1499591403,1.42,1,'ubuntu16-02\n'),('poll','24',1499591403,1.42,1,'ubuntu16-02\n'),('poll','23',1499591403,1.94,1,'ubuntu16-02\n'),('poll','28',1499591402,4.27,1,'ubuntu16\n'),('poll','27',1499591403,4.38,1,'ubuntu16-02\n'),('pollbill','',1499591702,0.76,0,'ubuntu16\n'),('pollbill','',1499591702,1.07,0,'ubuntu16-02\n'),('poll','22',1499591703,1.60,1,'ubuntu16\n'),('poll','24',1499591703,1.38,1,'ubuntu16-02\n'),('poll','25',1499591703,1.40,1,'ubuntu16-02\n'),('poll','23',1499591703,1.72,1,'ubuntu16-02\n'),('poll','28',1499591703,4.49,1,'ubuntu16\n'),('poll','27',1499591703,4.62,1,'ubuntu16-02\n'),('pollbill','',1499592002,0.96,0,'ubuntu16\n'),('pollbill','',1499592002,1.46,0,'ubuntu16-02\n'),('poll','22',1499592002,1.87,1,'ubuntu16\n'),('poll','25',1499592003,1.43,1,'ubuntu16-02\n'),('poll','24',1499592003,1.54,1,'ubuntu16-02\n'),('poll','23',1499592003,1.95,1,'ubuntu16-02\n'),('poll','28',1499592002,4.20,1,'ubuntu16\n'),('poll','27',1499592003,4.81,1,'ubuntu16-02\n'),('pollbill','',1499592301,0.69,0,'ubuntu16\n'),('pollbill','',1499592302,1.06,0,'ubuntu16-02\n'),('poll','22',1499592302,1.94,1,'ubuntu16\n'),('poll','28',1499592302,2.07,1,'ubuntu16\n'),('poll','24',1499592303,1.42,1,'ubuntu16-02\n'),('poll','25',1499592303,1.36,1,'ubuntu16-02\n'),('poll','23',1499592303,2.01,1,'ubuntu16-02\n'),('poll','27',1499592303,4.76,1,'ubuntu16-02\n'),('pollbill','',1499592602,0.78,0,'ubuntu16\n'),('pollbill','',1499592602,1.33,0,'ubuntu16-02\n'),('poll','22',1499592602,1.64,1,'ubuntu16\n'),('poll','24',1499592603,1.40,1,'ubuntu16-02\n'),('poll','25',1499592603,1.40,1,'ubuntu16-02\n'),('poll','23',1499592603,1.98,1,'ubuntu16-02\n'),('poll','28',1499592602,4.05,1,'ubuntu16\n'),('poll','27',1499592603,4.68,1,'ubuntu16-02\n'),('pollbill','',1499592902,1.12,0,'ubuntu16\n'),('pollbill','',1499592902,1.60,0,'ubuntu16-02\n'),('poll','22',1499592902,1.90,1,'ubuntu16\n'),('poll','24',1499592903,1.68,1,'ubuntu16-02\n'),('poll','25',1499592903,1.57,1,'ubuntu16-02\n'),('poll','23',1499592903,2.18,1,'ubuntu16-02\n'),('poll','28',1499592902,4.42,1,'ubuntu16\n'),('poll','27',1499592903,4.84,1,'ubuntu16-02\n'),('pollbill','',1499593202,0.72,0,'ubuntu16\n'),('pollbill','',1499593201,1.55,0,'ubuntu16-02\n'),('poll','22',1499593203,1.44,1,'ubuntu16\n'),('poll','24',1499593203,1.54,1,'ubuntu16-02\n'),('poll','25',1499593203,2.01,1,'ubuntu16-02\n'),('poll','23',1499593203,1.99,1,'ubuntu16-02\n'),('poll','28',1499593203,4.00,1,'ubuntu16\n'),('poll','27',1499593203,5.13,1,'ubuntu16-02\n'),('pollbill','',1499593502,0.99,0,'ubuntu16\n'),('pollbill','',1499593502,1.07,0,'ubuntu16-02\n'),('poll','22',1499593502,1.66,1,'ubuntu16\n'),('poll','24',1499593503,1.29,1,'ubuntu16-02\n'),('poll','25',1499593503,1.31,1,'ubuntu16-02\n'),('poll','23',1499593503,2.13,1,'ubuntu16-02\n'),('poll','28',1499593502,4.54,1,'ubuntu16\n'),('poll','27',1499593503,4.94,1,'ubuntu16-02\n'),('pollbill','',1499593802,0.79,0,'ubuntu16\n'),('pollbill','',1499593802,1.26,0,'ubuntu16-02\n'),('poll','22',1499593803,1.56,1,'ubuntu16\n'),('poll','25',1499593803,1.41,1,'ubuntu16-02\n'),('poll','24',1499593803,1.54,1,'ubuntu16-02\n'),('poll','23',1499593803,1.93,1,'ubuntu16-02\n'),('poll','28',1499593803,4.24,1,'ubuntu16\n'),('poll','27',1499593803,4.36,1,'ubuntu16-02\n'),('pollbill','',1499594101,1.14,0,'ubuntu16-02\n'),('pollbill','',1499594102,0.72,0,'ubuntu16\n'),('poll','24',1499594102,1.47,1,'ubuntu16-02\n'),('poll','25',1499594102,1.47,1,'ubuntu16-02\n'),('poll','22',1499594103,1.40,1,'ubuntu16\n'),('poll','23',1499594102,1.89,1,'ubuntu16-02\n'),('poll','27',1499594102,2.42,1,'ubuntu16-02\n'),('poll','28',1499594103,3.73,1,'ubuntu16\n'),('pollbill','',1499594401,0.71,0,'ubuntu16\n'),('pollbill','',1499594402,0.80,0,'ubuntu16-02\n'),('poll','22',1499594402,1.43,1,'ubuntu16\n'),('poll','24',1499594403,1.28,1,'ubuntu16-02\n'),('poll','25',1499594403,1.21,1,'ubuntu16-02\n'),('poll','23',1499594403,1.41,1,'ubuntu16-02\n'),('poll','28',1499594402,3.85,1,'ubuntu16\n'),('poll','27',1499594403,3.96,1,'ubuntu16-02\n'),('pollbill','',1499594702,0.52,0,'ubuntu16\n'),('pollbill','',1499594702,0.86,0,'ubuntu16-02\n'),('poll','22',1499594702,1.41,1,'ubuntu16\n'),('poll','24',1499594703,1.18,1,'ubuntu16-02\n'),('poll','25',1499594703,1.15,1,'ubuntu16-02\n'),('poll','23',1499594703,1.53,1,'ubuntu16-02\n'),('poll','28',1499594702,3.82,1,'ubuntu16\n'),('poll','27',1499594703,3.95,1,'ubuntu16-02\n'),('pollbill','',1499595001,0.57,0,'ubuntu16\n'),('pollbill','',1499595002,0.70,0,'ubuntu16-02\n'),('poll','22',1499595002,1.28,1,'ubuntu16\n'),('poll','25',1499595003,1.19,1,'ubuntu16-02\n'),('poll','24',1499595003,1.20,1,'ubuntu16-02\n'),('poll','23',1499595003,1.44,1,'ubuntu16-02\n'),('poll','28',1499595002,3.76,1,'ubuntu16\n'),('poll','27',1499595003,4.04,1,'ubuntu16-02\n'),('pollbill','',1499595302,0.71,0,'ubuntu16\n'),('pollbill','',1499595302,0.91,0,'ubuntu16-02\n'),('poll','22',1499595302,1.30,1,'ubuntu16\n'),('poll','24',1499595302,1.26,1,'ubuntu16-02\n'),('poll','25',1499595303,1.25,1,'ubuntu16-02\n'),('poll','23',1499595303,1.42,1,'ubuntu16-02\n'),('poll','28',1499595302,3.80,1,'ubuntu16\n'),('poll','27',1499595302,4.06,1,'ubuntu16-02\n'),('pollbill','',1499595601,0.44,0,'ubuntu16\n'),('pollbill','',1499595602,0.87,0,'ubuntu16-02\n'),('poll','22',1499595602,1.48,1,'ubuntu16\n'),('poll','24',1499595603,1.28,1,'ubuntu16-02\n'),('poll','25',1499595603,1.25,1,'ubuntu16-02\n'),('poll','23',1499595603,1.47,1,'ubuntu16-02\n'),('poll','28',1499595602,3.90,1,'ubuntu16\n'),('poll','27',1499595603,4.07,1,'ubuntu16-02\n'),('pollbill','',1499595901,0.69,0,'ubuntu16\n'),('pollbill','',1499595902,0.93,0,'ubuntu16-02\n'),('poll','22',1499595902,1.31,1,'ubuntu16\n'),('poll','24',1499595903,1.22,1,'ubuntu16-02\n'),('poll','25',1499595903,1.21,1,'ubuntu16-02\n'),('poll','23',1499595903,1.76,1,'ubuntu16-02\n'),('poll','28',1499595902,3.79,1,'ubuntu16\n'),('poll','27',1499595903,4.10,1,'ubuntu16-02\n'),('pollbill','',1499596201,0.84,0,'ubuntu16\n'),('pollbill','',1499596201,1.03,0,'ubuntu16-02\n'),('poll','22',1499596202,1.36,1,'ubuntu16\n'),('poll','24',1499596202,1.29,1,'ubuntu16-02\n'),('poll','25',1499596202,1.28,1,'ubuntu16-02\n'),('poll','23',1499596202,1.57,1,'ubuntu16-02\n'),('poll','28',1499596202,3.75,1,'ubuntu16\n'),('poll','27',1499596202,4.08,1,'ubuntu16-02\n'),('pollbill','',1499596502,0.66,0,'ubuntu16\n'),('pollbill','',1499596502,0.89,0,'ubuntu16-02\n'),('poll','22',1499596502,1.58,1,'ubuntu16\n'),('poll','25',1499596503,1.23,1,'ubuntu16-02\n'),('poll','24',1499596503,1.33,1,'ubuntu16-02\n'),('poll','23',1499596503,1.59,1,'ubuntu16-02\n'),('poll','28',1499596502,3.84,1,'ubuntu16\n'),('poll','27',1499596503,4.24,1,'ubuntu16-02\n'),('pollbill','',1499596802,0.69,0,'ubuntu16\n'),('pollbill','',1499596802,0.73,0,'ubuntu16-02\n'),('poll','22',1499596802,1.41,1,'ubuntu16\n'),('poll','24',1499596803,1.25,1,'ubuntu16-02\n'),('poll','25',1499596803,1.25,1,'ubuntu16-02\n'),('poll','23',1499596803,1.51,1,'ubuntu16-02\n'),('poll','28',1499596802,3.75,1,'ubuntu16\n'),('poll','27',1499596803,4.03,1,'ubuntu16-02\n'),('pollbill','',1499597102,0.52,0,'ubuntu16\n'),('pollbill','',1499597101,1.20,0,'ubuntu16-02\n'),('poll','25',1499597102,1.35,1,'ubuntu16-02\n'),('poll','24',1499597102,1.26,1,'ubuntu16-02\n'),('poll','22',1499597102,1.60,1,'ubuntu16\n'),('poll','23',1499597102,1.66,1,'ubuntu16-02\n'),('poll','28',1499597102,3.96,1,'ubuntu16\n'),('poll','27',1499597102,4.33,1,'ubuntu16-02\n'),('pollbill','',1499597402,0.75,0,'ubuntu16\n'),('pollbill','',1499597402,0.89,0,'ubuntu16-02\n'),('poll','22',1499597402,1.53,1,'ubuntu16\n'),('poll','25',1499597403,1.25,1,'ubuntu16-02\n'),('poll','24',1499597403,1.27,1,'ubuntu16-02\n'),('poll','23',1499597403,1.66,1,'ubuntu16-02\n'),('poll','28',1499597402,3.84,1,'ubuntu16\n'),('poll','27',1499597403,4.09,1,'ubuntu16-02\n'),('pollbill','',1499597701,0.67,0,'ubuntu16\n'),('pollbill','',1499597702,0.96,0,'ubuntu16-02\n'),('poll','22',1499597702,1.42,1,'ubuntu16\n'),('poll','25',1499597703,1.25,1,'ubuntu16-02\n'),('poll','24',1499597703,1.24,1,'ubuntu16-02\n'),('poll','23',1499597703,1.56,1,'ubuntu16-02\n'),('poll','28',1499597702,3.85,1,'ubuntu16\n'),('poll','27',1499597702,4.12,1,'ubuntu16-02\n'),('pollbill','',1499598002,0.82,0,'ubuntu16\n'),('pollbill','',1499598002,1.04,0,'ubuntu16-02\n'),('poll','22',1499598003,1.49,1,'ubuntu16\n'),('poll','24',1499598003,1.21,1,'ubuntu16-02\n'),('poll','25',1499598003,1.24,1,'ubuntu16-02\n'),('poll','23',1499598003,1.63,1,'ubuntu16-02\n'),('poll','28',1499598003,4.09,1,'ubuntu16\n'),('poll','27',1499598003,4.10,1,'ubuntu16-02\n'),('pollbill','',1499598302,0.55,0,'ubuntu16\n'),('pollbill','',1499598301,1.09,0,'ubuntu16-02\n'),('poll','22',1499598302,1.21,1,'ubuntu16\n'),('poll','25',1499598302,1.28,1,'ubuntu16-02\n'),('poll','24',1499598302,1.26,1,'ubuntu16-02\n'),('poll','23',1499598302,1.49,1,'ubuntu16-02\n'),('poll','28',1499598302,3.73,1,'ubuntu16\n'),('poll','27',1499598302,3.93,1,'ubuntu16-02\n'),('pollbill','',1499598602,0.41,0,'ubuntu16\n'),('pollbill','',1499598602,0.93,0,'ubuntu16-02\n'),('poll','22',1499598602,1.29,1,'ubuntu16\n'),('poll','24',1499598603,1.12,1,'ubuntu16-02\n'),('poll','25',1499598603,1.16,1,'ubuntu16-02\n'),('poll','23',1499598603,1.07,1,'ubuntu16-02\n'),('poll','28',1499598602,3.77,1,'ubuntu16\n'),('poll','27',1499598603,3.73,1,'ubuntu16-02\n'),('pollbill','',1499598902,0.56,0,'ubuntu16\n'),('pollbill','',1499598902,0.74,0,'ubuntu16-02\n'),('poll','22',1499598902,1.24,1,'ubuntu16\n'),('poll','24',1499598902,1.29,1,'ubuntu16-02\n'),('poll','25',1499598903,1.27,1,'ubuntu16-02\n'),('poll','23',1499598903,1.54,1,'ubuntu16-02\n'),('poll','28',1499598902,3.84,1,'ubuntu16\n'),('poll','27',1499598903,3.98,1,'ubuntu16-02\n'),('pollbill','',1499599202,0.58,0,'ubuntu16\n'),('pollbill','',1499599201,1.00,0,'ubuntu16-02\n'),('poll','22',1499599202,1.19,1,'ubuntu16\n'),('poll','24',1499599202,1.24,1,'ubuntu16-02\n'),('poll','25',1499599202,1.27,1,'ubuntu16-02\n'),('poll','23',1499599202,1.44,1,'ubuntu16-02\n'),('poll','28',1499599202,3.71,1,'ubuntu16\n'),('poll','27',1499599202,3.93,1,'ubuntu16-02\n'),('pollbill','',1499599502,0.45,0,'ubuntu16\n'),('pollbill','',1499599502,0.68,0,'ubuntu16-02\n'),('poll','22',1499599502,1.30,1,'ubuntu16\n'),('poll','25',1499599503,1.19,1,'ubuntu16-02\n'),('poll','24',1499599503,1.19,1,'ubuntu16-02\n'),('poll','23',1499599503,1.35,1,'ubuntu16-02\n'),('poll','28',1499599502,3.77,1,'ubuntu16\n'),('poll','27',1499599503,3.91,1,'ubuntu16-02\n'),('pollbill','',1499599801,0.57,0,'ubuntu16\n'),('pollbill','',1499599802,0.71,0,'ubuntu16-02\n'),('poll','22',1499599802,1.25,1,'ubuntu16\n'),('poll','25',1499599802,1.20,1,'ubuntu16-02\n'),('poll','24',1499599802,1.20,1,'ubuntu16-02\n'),('poll','23',1499599802,1.33,1,'ubuntu16-02\n'),('poll','28',1499599802,3.71,1,'ubuntu16\n'),('poll','27',1499599802,3.80,1,'ubuntu16-02\n'),('pollbill','',1499600101,0.39,0,'ubuntu16\n'),('pollbill','',1499600102,0.78,0,'ubuntu16-02\n'),('poll','22',1499600102,1.36,1,'ubuntu16\n'),('poll','25',1499600103,1.15,1,'ubuntu16-02\n'),('poll','24',1499600103,1.16,1,'ubuntu16-02\n'),('poll','23',1499600103,1.47,1,'ubuntu16-02\n'),('poll','28',1499600102,3.75,1,'ubuntu16\n'),('poll','27',1499600103,3.94,1,'ubuntu16-02\n'),('pollbill','',1499600402,0.55,0,'ubuntu16\n'),('pollbill','',1499600402,1.22,0,'ubuntu16-02\n'),('poll','24',1499600402,1.52,1,'ubuntu16-02\n'),('poll','25',1499600402,1.50,1,'ubuntu16-02\n'),('poll','22',1499600403,1.25,1,'ubuntu16\n'),('poll','23',1499600403,1.72,1,'ubuntu16-02\n'),('poll','28',1499600403,3.80,1,'ubuntu16\n'),('poll','27',1499600403,4.14,1,'ubuntu16-02\n'),('pollbill','',1499600701,0.56,0,'ubuntu16\n'),('poll','22',1499600702,1.23,1,'ubuntu16\n'),('pollbill','',1499600702,0.66,0,'ubuntu16-02\n'),('poll','24',1499600703,1.19,1,'ubuntu16-02\n'),('poll','25',1499600703,1.20,1,'ubuntu16-02\n'),('poll','23',1499600703,1.36,1,'ubuntu16-02\n'),('poll','28',1499600702,3.69,1,'ubuntu16\n'),('poll','27',1499600703,3.95,1,'ubuntu16-02\n'),('pollbill','',1499601002,1.14,0,'ubuntu16-02\n'),('pollbill','',1499601002,0.58,0,'ubuntu16\n'),('poll','24',1499601002,1.47,1,'ubuntu16-02\n'),('poll','22',1499601003,1.28,1,'ubuntu16\n'),('poll','25',1499601002,1.36,1,'ubuntu16-02\n'),('poll','23',1499601003,1.66,1,'ubuntu16-02\n'),('poll','28',1499601003,3.74,1,'ubuntu16\n'),('poll','27',1499601003,4.15,1,'ubuntu16-02\n'),('pollbill','',1499601302,0.61,0,'ubuntu16\n'),('pollbill','',1499601302,0.82,0,'ubuntu16-02\n'),('poll','22',1499601303,1.29,1,'ubuntu16\n'),('poll','25',1499601303,1.27,1,'ubuntu16-02\n'),('poll','24',1499601303,1.20,1,'ubuntu16-02\n'),('poll','23',1499601303,1.47,1,'ubuntu16-02\n'),('poll','28',1499601303,3.83,1,'ubuntu16\n'),('poll','27',1499601303,3.92,1,'ubuntu16-02\n'),('pollbill','',1499601602,0.68,0,'ubuntu16\n'),('pollbill','',1499601602,1.37,0,'ubuntu16-02\n'),('poll','22',1499601603,1.26,1,'ubuntu16\n'),('poll','25',1499601603,1.40,1,'ubuntu16-02\n'),('poll','24',1499601603,1.41,1,'ubuntu16-02\n'),('poll','23',1499601603,1.59,1,'ubuntu16-02\n'),('poll','28',1499601603,3.75,1,'ubuntu16\n'),('poll','27',1499601603,4.30,1,'ubuntu16-02\n'),('pollbill','',1499601902,0.68,0,'ubuntu16\n'),('pollbill','',1499601902,1.12,0,'ubuntu16-02\n'),('poll','22',1499601902,1.38,1,'ubuntu16\n'),('poll','25',1499601903,1.30,1,'ubuntu16-02\n'),('poll','24',1499601903,1.28,1,'ubuntu16-02\n'),('poll','23',1499601903,1.53,1,'ubuntu16-02\n'),('poll','28',1499601902,4.08,1,'ubuntu16\n'),('poll','27',1499601903,4.31,1,'ubuntu16-02\n'),('pollbill','',1499602202,0.53,0,'ubuntu16\n'),('pollbill','',1499602201,1.12,0,'ubuntu16-02\n'),('poll','22',1499602202,1.17,1,'ubuntu16\n'),('poll','25',1499602202,1.31,1,'ubuntu16-02\n'),('poll','24',1499602202,1.23,1,'ubuntu16-02\n'),('poll','23',1499602202,1.54,1,'ubuntu16-02\n'),('poll','28',1499602202,3.70,1,'ubuntu16\n'),('poll','27',1499602202,3.89,1,'ubuntu16-02\n'),('pollbill','',1499602502,0.68,0,'ubuntu16\n'),('pollbill','',1499602502,0.92,0,'ubuntu16-02\n'),('poll','22',1499602502,1.43,1,'ubuntu16\n'),('poll','28',1499602502,1.73,1,'ubuntu16\n'),('poll','24',1499602503,1.25,1,'ubuntu16-02\n'),('poll','25',1499602503,1.20,1,'ubuntu16-02\n'),('poll','23',1499602503,1.52,1,'ubuntu16-02\n'),('poll','27',1499602503,4.17,1,'ubuntu16-02\n'),('pollbill','',1499602802,0.65,0,'ubuntu16\n'),('pollbill','',1499602801,1.32,0,'ubuntu16-02\n'),('poll','22',1499602802,1.41,1,'ubuntu16\n'),('poll','24',1499602803,1.30,1,'ubuntu16-02\n'),('poll','25',1499602803,1.14,1,'ubuntu16-02\n'),('poll','23',1499602802,1.80,1,'ubuntu16-02\n'),('poll','28',1499602802,3.84,1,'ubuntu16\n'),('poll','27',1499602802,4.37,1,'ubuntu16-02\n'),('pollbill','',1499603102,0.85,0,'ubuntu16\n'),('pollbill','',1499603102,1.33,0,'ubuntu16-02\n'),('poll','22',1499603103,1.59,1,'ubuntu16\n'),('poll','25',1499603104,1.36,1,'ubuntu16-02\n'),('poll','24',1499603104,1.36,1,'ubuntu16-02\n'),('poll','23',1499603104,1.81,1,'ubuntu16-02\n'),('poll','28',1499603103,4.43,1,'ubuntu16\n'),('poll','27',1499603104,4.38,1,'ubuntu16-02\n'),('pollbill','',1499603402,0.52,0,'ubuntu16\n'),('pollbill','',1499603402,0.88,0,'ubuntu16-02\n'),('poll','22',1499603402,1.57,1,'ubuntu16\n'),('poll','25',1499603403,1.26,1,'ubuntu16-02\n'),('poll','24',1499603403,1.29,1,'ubuntu16-02\n'),('poll','23',1499603403,1.76,1,'ubuntu16-02\n'),('poll','28',1499603402,4.08,1,'ubuntu16\n'),('poll','27',1499603403,4.34,1,'ubuntu16-02\n'),('pollbill','',1499603702,0.68,0,'ubuntu16\n'),('pollbill','',1499603702,1.29,0,'ubuntu16-02\n'),('poll','22',1499603702,1.38,1,'ubuntu16\n'),('poll','24',1499603703,1.32,1,'ubuntu16-02\n'),('poll','25',1499603703,1.28,1,'ubuntu16-02\n'),('poll','23',1499603703,1.61,1,'ubuntu16-02\n'),('poll','28',1499603702,4.32,1,'ubuntu16\n'),('poll','27',1499603703,4.16,1,'ubuntu16-02\n'),('pollbill','',1499604002,0.80,0,'ubuntu16\n'),('pollbill','',1499604002,1.06,0,'ubuntu16-02\n'),('poll','22',1499604002,1.56,1,'ubuntu16\n'),('poll','24',1499604003,1.41,1,'ubuntu16-02\n'),('poll','25',1499604003,1.32,1,'ubuntu16-02\n'),('poll','23',1499604003,1.69,1,'ubuntu16-02\n'),('poll','28',1499604002,4.06,1,'ubuntu16\n'),('poll','27',1499604003,4.17,1,'ubuntu16-02\n'),('pollbill','',1499604301,2.30,0,'ubuntu16-02\n'),('poll','24',1499604302,1.49,1,'ubuntu16-02\n'),('poll','25',1499604302,1.75,1,'ubuntu16-02\n'),('poll','23',1499604302,2.36,1,'ubuntu16-02\n'),('pollbill','',1499604304,0.73,0,'ubuntu16\n'),('poll','22',1499604305,1.37,1,'ubuntu16\n'),('poll','27',1499604302,4.96,1,'ubuntu16-02\n'),('poll','28',1499604304,4.07,1,'ubuntu16\n'),('poll','25',1499604602,1.74,1,'ubuntu16-02\n'),('poll','24',1499604602,1.72,1,'ubuntu16-02\n'),('pollbill','',1499604603,1.01,0,'ubuntu16\n'),('pollbill','',1499604602,3.24,0,'ubuntu16-02\n'),('poll','22',1499604603,1.97,1,'ubuntu16\n'),('poll','23',1499604602,3.56,1,'ubuntu16-02\n'),('poll','27',1499604603,3.51,1,'ubuntu16-02\n'),('poll','28',1499604603,4.29,1,'ubuntu16\n'),('pollbill','',1499604901,0.94,0,'ubuntu16\n'),('pollbill','',1499604902,1.19,0,'ubuntu16-02\n'),('poll','22',1499604902,1.60,1,'ubuntu16\n'),('poll','24',1499604903,1.33,1,'ubuntu16-02\n'),('poll','25',1499604903,1.32,1,'ubuntu16-02\n'),('poll','23',1499604903,1.66,1,'ubuntu16-02\n'),('poll','28',1499604902,4.14,1,'ubuntu16\n'),('poll','27',1499604903,4.20,1,'ubuntu16-02\n'),('pollbill','',1499605201,1.85,0,'ubuntu16-02\n'),('poll','25',1499605202,1.48,1,'ubuntu16-02\n'),('poll','24',1499605202,1.62,1,'ubuntu16-02\n'),('pollbill','',1499605204,1.89,0,'ubuntu16\n'),('poll','23',1499605202,3.81,1,'ubuntu16-02\n'),('poll','22',1499605205,2.76,1,'ubuntu16\n'),('poll','27',1499605203,5.36,1,'ubuntu16-02\n'),('poll','28',1499605205,7.18,1,'ubuntu16\n'),('pollbill','',1499605502,0.63,0,'ubuntu16\n'),('pollbill','',1499605502,1.09,0,'ubuntu16-02\n'),('poll','22',1499605502,1.61,1,'ubuntu16\n'),('poll','25',1499605503,1.50,1,'ubuntu16-02\n'),('poll','24',1499605503,1.51,1,'ubuntu16-02\n'),('poll','23',1499605503,2.06,1,'ubuntu16-02\n'),('poll','28',1499605502,4.07,1,'ubuntu16\n'),('poll','27',1499605503,4.77,1,'ubuntu16-02\n'),('pollbill','',1499605802,0.66,0,'ubuntu16\n'),('pollbill','',1499605802,0.96,0,'ubuntu16-02\n'),('poll','22',1499605802,1.67,1,'ubuntu16\n'),('poll','25',1499605803,1.28,1,'ubuntu16-02\n'),('poll','24',1499605803,1.35,1,'ubuntu16-02\n'),('poll','23',1499605803,2.49,1,'ubuntu16-02\n'),('poll','27',1499605803,2.70,1,'ubuntu16-02\n'),('poll','28',1499605802,4.01,1,'ubuntu16\n'),('poll','25',1499606103,1.83,1,'ubuntu16-02\n'),('poll','24',1499606103,1.57,1,'ubuntu16-02\n'),('pollbill','',1499606102,3.12,0,'ubuntu16-02\n'),('pollbill','',1499606104,0.84,0,'ubuntu16\n'),('poll','23',1499606103,3.11,1,'ubuntu16-02\n'),('poll','22',1499606105,1.88,1,'ubuntu16\n'),('poll','27',1499606103,5.25,1,'ubuntu16-02\n'),('poll','28',1499606105,4.32,1,'ubuntu16\n'),('pollbill','',1499606402,0.70,0,'ubuntu16\n'),('pollbill','',1499606401,1.71,0,'ubuntu16-02\n'),('poll','25',1499606403,1.69,1,'ubuntu16-02\n'),('poll','22',1499606403,1.68,1,'ubuntu16\n'),('poll','24',1499606403,1.40,1,'ubuntu16-02\n'),('poll','23',1499606403,2.16,1,'ubuntu16-02\n'),('poll','28',1499606403,4.28,1,'ubuntu16\n'),('poll','27',1499606403,4.53,1,'ubuntu16-02\n'),('pollbill','',1499606702,2.18,0,'ubuntu16-02\n'),('pollbill','',1499606704,0.60,0,'ubuntu16\n'),('poll','24',1499606703,1.23,1,'ubuntu16-02\n'),('poll','25',1499606703,1.36,1,'ubuntu16-02\n'),('poll','23',1499606703,1.94,1,'ubuntu16-02\n'),('poll','22',1499606704,1.48,1,'ubuntu16\n'),('poll','27',1499606703,4.63,1,'ubuntu16-02\n'),('poll','28',1499606704,4.07,1,'ubuntu16\n'),('pollbill','',1499607002,0.96,0,'ubuntu16\n'),('pollbill','',1499607002,1.55,0,'ubuntu16-02\n'),('poll','22',1499607002,1.99,1,'ubuntu16\n'),('poll','24',1499607004,1.48,1,'ubuntu16-02\n'),('poll','25',1499607004,1.19,1,'ubuntu16-02\n'),('poll','23',1499607004,2.10,1,'ubuntu16-02\n'),('poll','28',1499607002,4.39,1,'ubuntu16\n'),('poll','27',1499607004,4.46,1,'ubuntu16-02\n'),('pollbill','',1499607302,0.83,0,'ubuntu16\n'),('pollbill','',1499607302,1.68,0,'ubuntu16-02\n'),('poll','22',1499607303,1.70,1,'ubuntu16\n'),('poll','24',1499607303,1.51,1,'ubuntu16-02\n'),('poll','25',1499607304,1.45,1,'ubuntu16-02\n'),('poll','23',1499607303,2.08,1,'ubuntu16-02\n'),('poll','28',1499607303,4.14,1,'ubuntu16\n'),('poll','27',1499607303,4.73,1,'ubuntu16-02\n'),('poll','24',1499607605,2.04,1,'ubuntu16-02\n'),('poll','25',1499607605,2.04,1,'ubuntu16-02\n'),('pollbill','',1499607602,4.88,0,'ubuntu16-02\n'),('poll','23',1499607605,2.64,1,'ubuntu16-02\n'),('pollbill','',1499607607,1.05,0,'ubuntu16\n'),('poll','22',1499607607,2.49,1,'ubuntu16\n'),('poll','27',1499607605,5.04,1,'ubuntu16-02\n'),('poll','28',1499607607,4.63,1,'ubuntu16\n'),('pollbill','',1499607903,1.31,0,'ubuntu16\n'),('pollbill','',1499607902,3.06,0,'ubuntu16-02\n'),('poll','25',1499607903,1.94,1,'ubuntu16-02\n'),('poll','24',1499607904,1.93,1,'ubuntu16-02\n'),('poll','22',1499607903,2.28,1,'ubuntu16\n'),('poll','23',1499607904,2.50,1,'ubuntu16-02\n'),('poll','28',1499607903,4.50,1,'ubuntu16\n'),('poll','27',1499607903,5.08,1,'ubuntu16-02\n'),('pollbill','',1499608202,2.33,0,'ubuntu16-02\n'),('poll','24',1499608203,1.53,1,'ubuntu16-02\n'),('poll','25',1499608203,1.56,1,'ubuntu16-02\n'),('pollbill','',1499608204,1.25,0,'ubuntu16\n'),('poll','23',1499608203,2.44,1,'ubuntu16-02\n'),('poll','22',1499608204,1.95,1,'ubuntu16\n'),('poll','27',1499608203,4.66,1,'ubuntu16-02\n'),('poll','28',1499608204,4.96,1,'ubuntu16\n'),('pollbill','',1499608503,0.84,0,'ubuntu16\n'),('pollbill','',1499608502,2.03,0,'ubuntu16-02\n'),('poll','22',1499608503,1.69,1,'ubuntu16\n'),('poll','25',1499608503,1.71,1,'ubuntu16-02\n'),('poll','24',1499608503,1.66,1,'ubuntu16-02\n'),('poll','23',1499608503,2.19,1,'ubuntu16-02\n'),('poll','28',1499608503,4.20,1,'ubuntu16\n'),('poll','27',1499608503,4.87,1,'ubuntu16-02\n'),('pollbill','',1499608802,1.61,0,'ubuntu16\n'),('pollbill','',1499608802,2.57,0,'ubuntu16-02\n'),('poll','25',1499608803,2.19,1,'ubuntu16-02\n'),('poll','22',1499608803,2.96,1,'ubuntu16\n'),('poll','28',1499608802,3.15,1,'ubuntu16\n'),('poll','24',1499608804,1.89,1,'ubuntu16-02\n'),('poll','23',1499608804,2.89,1,'ubuntu16-02\n'),('poll','27',1499608804,4.98,1,'ubuntu16-02\n'),('poll','25',1499609103,1.46,1,'ubuntu16-02\n'),('poll','24',1499609102,1.67,1,'ubuntu16-02\n'),('pollbill','',1499609101,3.16,0,'ubuntu16-02\n'),('pollbill','',1499609104,1.02,0,'ubuntu16\n'),('poll','23',1499609103,3.12,1,'ubuntu16-02\n'),('poll','22',1499609104,2.01,1,'ubuntu16\n'),('poll','27',1499609103,5.58,1,'ubuntu16-02\n'),('poll','28',1499609104,4.51,1,'ubuntu16\n'),('pollbill','',1499609402,0.86,0,'ubuntu16\n'),('pollbill','',1499609402,1.12,0,'ubuntu16-02\n'),('poll','22',1499609402,1.70,1,'ubuntu16\n'),('poll','25',1499609403,1.34,1,'ubuntu16-02\n'),('poll','24',1499609403,1.35,1,'ubuntu16-02\n'),('poll','23',1499609403,1.85,1,'ubuntu16-02\n'),('poll','28',1499609402,4.11,1,'ubuntu16\n'),('poll','27',1499609403,4.74,1,'ubuntu16-02\n'),('pollbill','',1499609702,0.78,0,'ubuntu16\n'),('pollbill','',1499609702,1.66,0,'ubuntu16-02\n'),('poll','22',1499609703,1.53,1,'ubuntu16\n'),('poll','24',1499609703,1.43,1,'ubuntu16-02\n'),('poll','25',1499609703,1.36,1,'ubuntu16-02\n'),('poll','23',1499609703,1.94,1,'ubuntu16-02\n'),('poll','28',1499609703,4.64,1,'ubuntu16\n'),('poll','27',1499609703,4.42,1,'ubuntu16-02\n'),('pollbill','',1499610001,1.52,0,'ubuntu16-02\n'),('poll','25',1499610002,1.47,1,'ubuntu16-02\n'),('poll','24',1499610002,1.46,1,'ubuntu16-02\n'),('poll','27',1499610002,6.96,1,'ubuntu16-02\n'),('poll','23',1499610002,7.11,1,'ubuntu16-02\n'),('pollbill','',1499610004,6.36,0,'ubuntu16\n'),('poll','22',1499610007,4.52,1,'ubuntu16\n'),('poll','28',1499610005,8.57,1,'ubuntu16\n'),('poll','25',1499610303,7.84,1,'ubuntu16-02\n'),('poll','24',1499610303,7.25,1,'ubuntu16-02\n'),('poll','27',1499610303,27.05,1,'ubuntu16-02\n'),('poll','23',1499610303,27.62,1,'ubuntu16-02\n'),('poll','22',1499610303,27.82,1,'ubuntu16\n'),('poll','28',1499610303,27.84,1,'ubuntu16\n'),('pollbill','',1499610302,29.95,0,'ubuntu16-02\n'),('pollbill','',1499610303,28.63,0,'ubuntu16\n'),('pollbill','',1499610601,1.52,0,'ubuntu16-02\n'),('poll','24',1499610602,1.35,1,'ubuntu16-02\n'),('poll','25',1499610602,1.37,1,'ubuntu16-02\n'),('poll','23',1499610602,2.21,1,'ubuntu16-02\n'),('pollbill','',1499610605,0.91,0,'ubuntu16\n'),('poll','27',1499610602,4.22,1,'ubuntu16-02\n'),('poll','22',1499610606,1.27,1,'ubuntu16\n'),('poll','28',1499610606,3.74,1,'ubuntu16\n'),('pollbill','',1499610903,0.78,0,'ubuntu16\n'),('pollbill','',1499610902,1.90,0,'ubuntu16-02\n'),('poll','24',1499610903,1.43,1,'ubuntu16-02\n'),('poll','25',1499610903,1.60,1,'ubuntu16-02\n'),('poll','22',1499610903,1.73,1,'ubuntu16\n'),('poll','28',1499610903,2.32,1,'ubuntu16\n'),('poll','23',1499610903,2.48,1,'ubuntu16-02\n'),('poll','27',1499610903,4.58,1,'ubuntu16-02\n'),('pollbill','',1499611202,2.56,0,'ubuntu16\n'),('poll','22',1499611203,2.19,1,'ubuntu16\n'),('poll','25',1499611205,1.98,1,'ubuntu16-02\n'),('poll','24',1499611205,2.16,1,'ubuntu16-02\n'),('pollbill','',1499611204,2.76,0,'ubuntu16-02\n'),('poll','28',1499611203,5.24,1,'ubuntu16\n'),('poll','23',1499611205,3.42,1,'ubuntu16-02\n'),('poll','27',1499611205,6.05,1,'ubuntu16-02\n'),('poll','24',1499611503,6.29,1,'ubuntu16-02\n'),('poll','25',1499611503,5.83,1,'ubuntu16-02\n'),('poll','23',1499611503,7.93,1,'ubuntu16-02\n'),('poll','27',1499611503,8.75,1,'ubuntu16-02\n'),('pollbill','',1499611502,9.84,0,'ubuntu16-02\n'),('pollbill','',1499611510,1.34,0,'ubuntu16\n'),('poll','22',1499611511,1.62,1,'ubuntu16\n'),('poll','28',1499611511,4.29,1,'ubuntu16\n'),('pollbill','',1499611802,0.91,0,'ubuntu16\n'),('pollbill','',1499611802,1.54,0,'ubuntu16-02\n'),('poll','25',1499611802,1.31,1,'ubuntu16-02\n'),('poll','24',1499611802,1.58,1,'ubuntu16-02\n'),('poll','22',1499611802,1.53,1,'ubuntu16\n'),('poll','23',1499611802,1.87,1,'ubuntu16-02\n'),('poll','28',1499611803,3.66,1,'ubuntu16\n'),('poll','27',1499611802,4.55,1,'ubuntu16-02\n'),('pollbill','',1499612102,1.01,0,'ubuntu16\n'),('pollbill','',1499612102,1.29,0,'ubuntu16-02\n'),('poll','22',1499612103,1.40,1,'ubuntu16\n'),('poll','25',1499612103,1.44,1,'ubuntu16-02\n'),('poll','24',1499612103,1.45,1,'ubuntu16-02\n'),('poll','23',1499612103,1.83,1,'ubuntu16-02\n'),('poll','28',1499612103,3.92,1,'ubuntu16\n'),('poll','27',1499612103,4.31,1,'ubuntu16-02\n'),('poll','24',1499612403,1.48,1,'ubuntu16-02\n'),('poll','25',1499612403,1.27,1,'ubuntu16-02\n'),('pollbill','',1499612402,2.99,0,'ubuntu16-02\n'),('poll','23',1499612403,2.06,1,'ubuntu16-02\n'),('pollbill','',1499612405,0.48,0,'ubuntu16\n'),('poll','22',1499612405,1.46,1,'ubuntu16\n'),('poll','27',1499612403,4.13,1,'ubuntu16-02\n'),('poll','28',1499612405,3.96,1,'ubuntu16\n'),('pollbill','',1499612702,1.35,0,'ubuntu16-02\n'),('pollbill','',1499612703,0.73,0,'ubuntu16\n'),('poll','24',1499612703,1.31,1,'ubuntu16-02\n'),('poll','25',1499612703,1.34,1,'ubuntu16-02\n'),('poll','23',1499612703,1.75,1,'ubuntu16-02\n'),('poll','22',1499612703,1.42,1,'ubuntu16\n'),('poll','27',1499612703,4.33,1,'ubuntu16-02\n'),('poll','28',1499612703,4.35,1,'ubuntu16\n'),('poll','24',1499613002,1.99,1,'ubuntu16-02\n'),('poll','25',1499613002,1.89,1,'ubuntu16-02\n'),('pollbill','',1499613001,3.55,0,'ubuntu16-02\n'),('poll','23',1499613002,2.97,1,'ubuntu16-02\n'),('pollbill','',1499613006,0.69,0,'ubuntu16\n'),('poll','27',1499613002,4.92,1,'ubuntu16-02\n'),('poll','22',1499613006,1.68,1,'ubuntu16\n'),('poll','28',1499613006,4.52,1,'ubuntu16\n'),('pollbill','',1499613301,1.56,0,'ubuntu16-02\n'),('pollbill','',1499613302,0.82,0,'ubuntu16\n'),('poll','24',1499613302,1.25,1,'ubuntu16-02\n'),('poll','25',1499613302,1.30,1,'ubuntu16-02\n'),('poll','23',1499613302,1.92,1,'ubuntu16-02\n'),('poll','22',1499613303,1.44,1,'ubuntu16\n'),('poll','28',1499613303,3.95,1,'ubuntu16\n'),('poll','27',1499613302,4.67,1,'ubuntu16-02\n'),('pollbill','',1499613602,0.91,0,'ubuntu16\n'),('pollbill','',1499613602,1.04,0,'ubuntu16-02\n'),('poll','22',1499613602,1.53,1,'ubuntu16\n'),('poll','24',1499613603,1.48,1,'ubuntu16-02\n'),('poll','25',1499613603,1.41,1,'ubuntu16-02\n'),('poll','23',1499613603,1.64,1,'ubuntu16-02\n'),('poll','28',1499613602,3.95,1,'ubuntu16\n'),('poll','27',1499613603,4.20,1,'ubuntu16-02\n'),('poll','24',1499613902,1.52,1,'ubuntu16-02\n'),('poll','25',1499613902,1.56,1,'ubuntu16-02\n'),('pollbill','',1499613901,3.02,0,'ubuntu16-02\n'),('poll','23',1499613902,2.56,1,'ubuntu16-02\n'),('pollbill','',1499613905,0.67,0,'ubuntu16\n'),('poll','22',1499613905,1.56,1,'ubuntu16\n'),('poll','27',1499613902,4.26,1,'ubuntu16-02\n'),('poll','28',1499613905,3.78,1,'ubuntu16\n'),('pollbill','',1499614202,0.88,0,'ubuntu16\n'),('pollbill','',1499614201,1.40,0,'ubuntu16-02\n'),('poll','22',1499614202,1.35,1,'ubuntu16\n'),('poll','24',1499614202,1.42,1,'ubuntu16-02\n'),('poll','25',1499614203,1.36,1,'ubuntu16-02\n'),('poll','23',1499614203,1.84,1,'ubuntu16-02\n'),('poll','28',1499614202,4.15,1,'ubuntu16\n'),('poll','27',1499614203,4.32,1,'ubuntu16-02\n'),('pollbill','',1499614502,1.43,0,'ubuntu16\n'),('pollbill','',1499614502,1.65,0,'ubuntu16-02\n'),('poll','22',1499614502,1.73,1,'ubuntu16\n'),('poll','25',1499614503,1.74,1,'ubuntu16-02\n'),('poll','24',1499614503,1.77,1,'ubuntu16-02\n'),('poll','23',1499614503,2.11,1,'ubuntu16-02\n'),('poll','28',1499614502,4.66,1,'ubuntu16\n'),('poll','27',1499614503,4.96,1,'ubuntu16-02\n'),('poll','25',1499614802,1.83,1,'ubuntu16-02\n'),('poll','24',1499614802,1.92,1,'ubuntu16-02\n'),('pollbill','',1499614802,2.69,0,'ubuntu16-02\n'),('pollbill','',1499614804,1.31,0,'ubuntu16\n'),('poll','23',1499614802,3.00,1,'ubuntu16-02\n'),('poll','27',1499614802,3.21,1,'ubuntu16-02\n'),('poll','22',1499614805,1.35,1,'ubuntu16\n'),('poll','28',1499614805,3.88,1,'ubuntu16\n'),('pollbill','',1499615101,0.65,0,'ubuntu16\n'),('pollbill','',1499615102,0.95,0,'ubuntu16-02\n'),('poll','22',1499615102,1.51,1,'ubuntu16\n'),('poll','24',1499615103,1.18,1,'ubuntu16-02\n'),('poll','25',1499615103,1.24,1,'ubuntu16-02\n'),('poll','23',1499615103,1.55,1,'ubuntu16-02\n'),('poll','28',1499615102,3.86,1,'ubuntu16\n'),('poll','27',1499615103,4.21,1,'ubuntu16-02\n'),('pollbill','',1499615402,0.74,0,'ubuntu16\n'),('pollbill','',1499615402,1.04,0,'ubuntu16-02\n'),('poll','22',1499615402,1.55,1,'ubuntu16\n'),('poll','25',1499615403,1.28,1,'ubuntu16-02\n'),('poll','24',1499615403,1.29,1,'ubuntu16-02\n'),('poll','23',1499615403,1.72,1,'ubuntu16-02\n'),('poll','28',1499615402,4.33,1,'ubuntu16\n'),('poll','27',1499615403,4.00,1,'ubuntu16-02\n'),('poll','25',1499615702,1.27,1,'ubuntu16-02\n'),('pollbill','',1499615702,2.25,0,'ubuntu16-02\n'),('poll','24',1499615703,1.40,1,'ubuntu16-02\n'),('pollbill','',1499615702,1.88,0,'ubuntu16\n'),('poll','23',1499615703,1.99,1,'ubuntu16-02\n'),('poll','22',1499615704,1.49,1,'ubuntu16\n'),('poll','27',1499615702,3.92,1,'ubuntu16-02\n'),('poll','28',1499615704,3.96,1,'ubuntu16\n'),('poll','25',1499616003,1.59,1,'ubuntu16-02\n'),('pollbill','',1499616004,1.29,0,'ubuntu16\n'),('poll','24',1499616003,1.77,1,'ubuntu16-02\n'),('pollbill','',1499616002,3.28,0,'ubuntu16-02\n'),('poll','23',1499616003,3.09,1,'ubuntu16-02\n'),('poll','22',1499616004,2.48,1,'ubuntu16\n'),('poll','27',1499616003,4.75,1,'ubuntu16-02\n'),('poll','28',1499616004,4.43,1,'ubuntu16\n'),('pollbill','',1499616302,1.24,0,'ubuntu16\n'),('pollbill','',1499616302,1.96,0,'ubuntu16-02\n'),('poll','25',1499616303,1.70,1,'ubuntu16-02\n'),('poll','22',1499616303,1.92,1,'ubuntu16\n'),('poll','24',1499616303,1.64,1,'ubuntu16-02\n'),('poll','23',1499616303,2.39,1,'ubuntu16-02\n'),('poll','28',1499616303,4.23,1,'ubuntu16\n'),('poll','27',1499616303,5.07,1,'ubuntu16-02\n'),('pollbill','',1499616602,0.91,0,'ubuntu16\n'),('pollbill','',1499616602,1.29,0,'ubuntu16-02\n'),('poll','22',1499616602,1.87,1,'ubuntu16\n'),('poll','24',1499616603,1.45,1,'ubuntu16-02\n'),('poll','25',1499616604,1.45,1,'ubuntu16-02\n'),('poll','23',1499616604,2.25,1,'ubuntu16-02\n'),('poll','27',1499616603,2.52,1,'ubuntu16-02\n'),('poll','28',1499616602,4.28,1,'ubuntu16\n'),('pollbill','',1499616902,1.18,0,'ubuntu16\n'),('pollbill','',1499616903,1.61,0,'ubuntu16-02\n'),('poll','24',1499616903,1.81,1,'ubuntu16-02\n'),('poll','25',1499616904,1.86,1,'ubuntu16-02\n'),('poll','22',1499616903,2.68,1,'ubuntu16\n'),('poll','28',1499616903,2.75,1,'ubuntu16\n'),('poll','23',1499616903,2.48,1,'ubuntu16-02\n'),('poll','27',1499616903,4.72,1,'ubuntu16-02\n'),('poll','25',1499617203,1.31,1,'ubuntu16-02\n'),('poll','24',1499617203,1.37,1,'ubuntu16-02\n'),('pollbill','',1499617204,0.89,0,'ubuntu16\n'),('pollbill','',1499617202,2.58,0,'ubuntu16-02\n'),('poll','23',1499617203,2.56,1,'ubuntu16-02\n'),('poll','22',1499617204,1.91,1,'ubuntu16\n'),('poll','27',1499617203,4.78,1,'ubuntu16-02\n'),('poll','28',1499617204,4.10,1,'ubuntu16\n'),('poll','25',1499617509,2.89,1,'ubuntu16-02\n'),('poll','24',1499617509,4.34,1,'ubuntu16-02\n'),('poll','23',1499617509,5.70,1,'ubuntu16-02\n'),('pollbill','',1499617505,9.77,0,'ubuntu16-02\n'),('pollbill','',1499617504,11.24,0,'ubuntu16\n'),('poll','27',1499617509,6.54,1,'ubuntu16-02\n'),('poll','22',1499617511,6.06,1,'ubuntu16\n'),('poll','28',1499617510,10.87,1,'ubuntu16\n'),('poll','27',1499617836,21.59,1,'ubuntu16-02\n'),('pollbill','',1499617833,24.74,0,'ubuntu16\n'),('pollbill','',1499617801,56.43,0,'ubuntu16-02\n'),('poll','22',1499617856,3.14,1,'ubuntu16\n'),('poll','28',1499617856,5.39,1,'ubuntu16\n'),('pollbill','',1499618102,1.47,0,'ubuntu16\n'),('pollbill','',1499618102,1.64,0,'ubuntu16-02\n'),('poll','22',1499618102,2.22,1,'ubuntu16\n'),('poll','24',1499618103,1.45,1,'ubuntu16-02\n'),('poll','25',1499618103,1.43,1,'ubuntu16-02\n'),('poll','23',1499618103,1.74,1,'ubuntu16-02\n'),('poll','28',1499618102,4.63,1,'ubuntu16\n'),('poll','27',1499618103,4.24,1,'ubuntu16-02\n'),('pollbill','',1499618402,1.00,0,'ubuntu16\n'),('pollbill','',1499618402,1.60,0,'ubuntu16-02\n'),('poll','24',1499618403,1.43,1,'ubuntu16-02\n'),('poll','25',1499618403,1.52,1,'ubuntu16-02\n'),('poll','22',1499618403,1.60,1,'ubuntu16\n'),('poll','23',1499618403,1.93,1,'ubuntu16-02\n'),('poll','28',1499618403,4.17,1,'ubuntu16\n'),('poll','27',1499618403,4.40,1,'ubuntu16-02\n'),('poll','25',1499618702,1.95,1,'ubuntu16-02\n'),('poll','24',1499618702,2.02,1,'ubuntu16-02\n'),('pollbill','',1499618701,3.82,0,'ubuntu16-02\n'),('poll','23',1499618703,2.59,1,'ubuntu16-02\n'),('pollbill','',1499618704,1.32,0,'ubuntu16\n'),('poll','22',1499618705,1.34,1,'ubuntu16\n'),('poll','27',1499618702,4.92,1,'ubuntu16-02\n'),('poll','28',1499618705,4.41,1,'ubuntu16\n'),('poll','22',1499658905,1.50,1,'ubuntu16\n'),('poll','28',1499658905,7.52,1,'ubuntu16\n'),('pollbill','',1499658902,81.10,0,'ubuntu16\n'),('poll','22',1499659202,1.01,1,'ubuntu16\n'),('poll','28',1499659202,3.74,1,'ubuntu16\n'),('pollbill','',1499659202,72.79,0,'ubuntu16\n'),('poll','22',1499659502,1.03,1,'ubuntu16\n'),('poll','28',1499659502,1.48,1,'ubuntu16\n'),('pollbill','',1499659502,72.84,0,'ubuntu16\n'),('poll','22',1499659802,1.01,1,'ubuntu16\n'),('poll','28',1499659802,1.36,1,'ubuntu16\n'),('pollbill','',1499659802,72.67,0,'ubuntu16\n'),('poll','22',1499660102,1.03,1,'ubuntu16\n'),('poll','28',1499660102,1.47,1,'ubuntu16\n'),('pollbill','',1499660101,72.74,0,'ubuntu16\n'),('poll','22',1499660402,1.02,1,'ubuntu16\n'),('poll','28',1499660402,1.55,1,'ubuntu16\n'),('pollbill','',1499660402,72.88,0,'ubuntu16\n'),('poll','22',1499660702,1.02,1,'ubuntu16\n'),('poll','28',1499660702,1.55,1,'ubuntu16\n'),('pollbill','',1499660701,72.83,0,'ubuntu16\n'),('poll','22',1499661002,1.02,1,'ubuntu16\n'),('poll','28',1499661002,1.57,1,'ubuntu16\n'),('pollbill','',1499661002,72.91,0,'ubuntu16\n'),('poll','22',1499661303,1.02,1,'ubuntu16\n'),('poll','28',1499661303,1.53,1,'ubuntu16\n'),('pollbill','',1499661302,72.73,0,'ubuntu16\n'),('poll','22',1499661602,1.01,1,'ubuntu16\n'),('poll','28',1499661602,1.38,1,'ubuntu16\n'),('pollbill','',1499661602,72.90,0,'ubuntu16\n'),('poll','22',1499661903,1.05,1,'ubuntu16\n'),('poll','28',1499661903,1.44,1,'ubuntu16\n'),('pollbill','',1499661902,72.92,0,'ubuntu16\n'),('poll','22',1499662202,0.99,1,'ubuntu16\n'),('poll','28',1499662202,1.30,1,'ubuntu16\n'),('pollbill','',1499662202,72.82,0,'ubuntu16\n'),('poll','22',1499662502,1.03,1,'ubuntu16\n'),('poll','28',1499662502,1.71,1,'ubuntu16\n'),('pollbill','',1499662502,72.79,0,'ubuntu16\n'),('poll','22',1499662802,0.98,1,'ubuntu16\n'),('poll','28',1499662802,1.39,1,'ubuntu16\n'),('pollbill','',1499662802,72.86,0,'ubuntu16\n'),('poll','22',1499663102,1.01,1,'ubuntu16\n'),('poll','28',1499663102,1.59,1,'ubuntu16\n'),('pollbill','',1499663102,72.81,0,'ubuntu16\n'),('poll','22',1499663402,1.00,1,'ubuntu16\n'),('poll','28',1499663402,1.36,1,'ubuntu16\n'),('pollbill','',1499663402,72.96,0,'ubuntu16\n'),('poll','22',1499663702,0.99,1,'ubuntu16\n'),('poll','28',1499663702,1.61,1,'ubuntu16\n'),('pollbill','',1499663702,72.93,0,'ubuntu16\n'),('poll','22',1499664002,1.00,1,'ubuntu16\n'),('poll','28',1499664002,1.51,1,'ubuntu16\n'),('pollbill','',1499664002,72.94,0,'ubuntu16\n'),('poll','22',1499664302,1.00,1,'ubuntu16\n'),('poll','28',1499664302,1.32,1,'ubuntu16\n'),('pollbill','',1499664302,72.86,0,'ubuntu16\n'),('poll','22',1499664602,1.01,1,'ubuntu16\n'),('poll','28',1499664602,1.36,1,'ubuntu16\n'),('pollbill','',1499664602,72.97,0,'ubuntu16\n'),('poll','22',1499664902,0.99,1,'ubuntu16\n'),('poll','28',1499664902,1.38,1,'ubuntu16\n'),('pollbill','',1499664902,72.99,0,'ubuntu16\n'),('poll','22',1499665202,1.01,1,'ubuntu16\n'),('poll','28',1499665202,1.41,1,'ubuntu16\n'),('pollbill','',1499665202,72.90,0,'ubuntu16\n'),('poll','22',1499665502,1.06,1,'ubuntu16\n'),('poll','28',1499665502,1.36,1,'ubuntu16\n'),('pollbill','',1499665502,72.87,0,'ubuntu16\n'),('poll','22',1499665802,0.99,1,'ubuntu16\n'),('poll','28',1499665802,1.37,1,'ubuntu16\n'),('pollbill','',1499665802,73.01,0,'ubuntu16\n'),('poll','22',1499666102,1.00,1,'ubuntu16\n'),('poll','28',1499666102,2.59,1,'ubuntu16\n'),('pollbill','',1499666102,72.90,0,'ubuntu16\n'),('poll','22',1499666402,1.02,1,'ubuntu16\n'),('poll','28',1499666402,1.29,1,'ubuntu16\n'),('pollbill','',1499666402,72.91,0,'ubuntu16\n'),('poll','22',1499666702,1.03,1,'ubuntu16\n'),('poll','28',1499666702,1.57,1,'ubuntu16\n'),('pollbill','',1499666702,72.80,0,'ubuntu16\n'),('poll','22',1499667002,1.00,1,'ubuntu16\n'),('poll','28',1499667002,1.36,1,'ubuntu16\n'),('pollbill','',1499667002,73.09,0,'ubuntu16\n'),('poll','22',1499667302,1.01,1,'ubuntu16\n'),('poll','28',1499667302,1.35,1,'ubuntu16\n'),('pollbill','',1499667302,72.92,0,'ubuntu16\n'),('poll','22',1499667603,1.06,1,'ubuntu16\n'),('poll','28',1499667603,1.38,1,'ubuntu16\n'),('pollbill','',1499667602,72.83,0,'ubuntu16\n'),('poll','22',1499667902,1.00,1,'ubuntu16\n'),('poll','28',1499667902,1.35,1,'ubuntu16\n'),('pollbill','',1499667902,72.71,0,'ubuntu16\n'),('poll','22',1499668202,1.00,1,'ubuntu16\n'),('poll','28',1499668202,1.31,1,'ubuntu16\n'),('pollbill','',1499668202,72.86,0,'ubuntu16\n'),('poll','22',1499668502,1.07,1,'ubuntu16\n'),('poll','28',1499668502,1.46,1,'ubuntu16\n'),('pollbill','',1499668501,72.89,0,'ubuntu16\n'),('poll','22',1499668802,1.03,1,'ubuntu16\n'),('poll','28',1499668802,1.30,1,'ubuntu16\n'),('pollbill','',1499668802,72.99,0,'ubuntu16\n'),('poll','22',1499669102,1.03,1,'ubuntu16\n'),('poll','28',1499669102,1.34,1,'ubuntu16\n'),('pollbill','',1499669102,72.83,0,'ubuntu16\n'),('poll','22',1499669402,1.04,1,'ubuntu16\n'),('poll','28',1499669402,1.43,1,'ubuntu16\n'),('pollbill','',1499669402,72.97,0,'ubuntu16\n'),('poll','22',1499669702,1.02,1,'ubuntu16\n'),('poll','28',1499669702,1.47,1,'ubuntu16\n'),('pollbill','',1499669702,72.90,0,'ubuntu16\n'),('poll','22',1499670002,1.04,1,'ubuntu16\n'),('poll','28',1499670002,1.44,1,'ubuntu16\n'),('pollbill','',1499670002,72.81,0,'ubuntu16\n'),('poll','22',1499670302,1.07,1,'ubuntu16\n'),('poll','28',1499670302,1.39,1,'ubuntu16\n'),('pollbill','',1499670302,72.90,0,'ubuntu16\n'),('poll','22',1499670602,1.02,1,'ubuntu16\n'),('poll','28',1499670602,1.83,1,'ubuntu16\n'),('pollbill','',1499670602,72.95,0,'ubuntu16\n'),('poll','22',1499670902,1.01,1,'ubuntu16\n'),('poll','28',1499670902,1.34,1,'ubuntu16\n'),('pollbill','',1499670902,72.75,0,'ubuntu16\n'),('poll','22',1499671203,0.99,1,'ubuntu16\n'),('poll','28',1499671203,1.33,1,'ubuntu16\n'),('pollbill','',1499671202,72.91,0,'ubuntu16\n'),('poll','22',1499671502,1.00,1,'ubuntu16\n'),('poll','28',1499671502,1.79,1,'ubuntu16\n'),('pollbill','',1499671502,72.78,0,'ubuntu16\n'),('poll','22',1499671802,1.02,1,'ubuntu16\n'),('poll','28',1499671802,1.43,1,'ubuntu16\n'),('pollbill','',1499671802,72.72,0,'ubuntu16\n'),('poll','22',1499672102,1.04,1,'ubuntu16\n'),('poll','28',1499672102,1.40,1,'ubuntu16\n'),('pollbill','',1499672101,72.83,0,'ubuntu16\n'),('poll','22',1499672402,1.01,1,'ubuntu16\n'),('poll','28',1499672402,1.53,1,'ubuntu16\n'),('pollbill','',1499672402,72.94,0,'ubuntu16\n'),('poll','22',1499672702,1.02,1,'ubuntu16\n'),('poll','28',1499672702,1.41,1,'ubuntu16\n'),('pollbill','',1499672701,72.84,0,'ubuntu16\n'),('poll','22',1499673002,1.04,1,'ubuntu16\n'),('poll','28',1499673002,2.56,1,'ubuntu16\n'),('pollbill','',1499673002,72.89,0,'ubuntu16\n'),('poll','22',1499673302,1.01,1,'ubuntu16\n'),('poll','28',1499673302,1.48,1,'ubuntu16\n'),('pollbill','',1499673301,72.85,0,'ubuntu16\n'),('poll','22',1499673602,1.03,1,'ubuntu16\n'),('poll','28',1499673602,2.56,1,'ubuntu16\n'),('pollbill','',1499673602,72.89,0,'ubuntu16\n'),('poll','22',1499673903,1.02,1,'ubuntu16\n'),('poll','28',1499673903,1.34,1,'ubuntu16\n'),('pollbill','',1499673902,72.89,0,'ubuntu16\n'),('poll','22',1499674202,1.03,1,'ubuntu16\n'),('poll','28',1499674202,1.55,1,'ubuntu16\n'),('pollbill','',1499674202,72.94,0,'ubuntu16\n'),('poll','22',1499674503,1.03,1,'ubuntu16\n'),('poll','28',1499674503,1.71,1,'ubuntu16\n'),('pollbill','',1499674502,73.01,0,'ubuntu16\n'),('poll','22',1499674802,1.01,1,'ubuntu16\n'),('poll','28',1499674802,1.29,1,'ubuntu16\n'),('pollbill','',1499674802,72.82,0,'ubuntu16\n'),('poll','22',1499675102,1.00,1,'ubuntu16\n'),('poll','28',1499675102,1.59,1,'ubuntu16\n'),('pollbill','',1499675101,72.90,0,'ubuntu16\n'),('poll','22',1499748003,2.27,1,'ubuntu16\n'),('poll','28',1499748003,9.96,1,'ubuntu16\n'),('pollbill','',1499748002,81.90,0,'ubuntu16\n'),('poll','22',1499748302,1.03,1,'ubuntu16\n'),('poll','28',1499748302,1.81,1,'ubuntu16\n'),('pollbill','',1499748302,73.13,0,'ubuntu16\n'),('poll','22',1499748603,1.18,1,'ubuntu16\n'),('poll','28',1499748603,2.72,1,'ubuntu16\n'),('pollbill','',1499748602,73.41,0,'ubuntu16\n'),('poll','22',1499748902,1.04,1,'ubuntu16\n'),('poll','28',1499748902,1.57,1,'ubuntu16\n'),('pollbill','',1499748902,73.30,0,'ubuntu16\n'),('poll','22',1499749202,1.06,1,'ubuntu16\n'),('poll','28',1499749202,1.45,1,'ubuntu16\n'),('pollbill','',1499749202,73.31,0,'ubuntu16\n'),('poll','22',1499749502,1.03,1,'ubuntu16\n'),('poll','28',1499749502,1.51,1,'ubuntu16\n'),('pollbill','',1499749502,73.22,0,'ubuntu16\n'),('poll','22',1499749802,1.04,1,'ubuntu16\n'),('poll','28',1499749802,1.62,1,'ubuntu16\n'),('pollbill','',1499749802,73.24,0,'ubuntu16\n'),('poll','22',1499750102,1.03,1,'ubuntu16\n'),('poll','28',1499750102,1.84,1,'ubuntu16\n'),('pollbill','',1499750102,73.23,0,'ubuntu16\n'),('poll','22',1499750402,1.03,1,'ubuntu16\n'),('poll','28',1499750402,1.98,1,'ubuntu16\n'),('pollbill','',1499750402,73.35,0,'ubuntu16\n'),('poll','22',1499750702,1.02,1,'ubuntu16\n'),('poll','28',1499750702,1.61,1,'ubuntu16\n'),('pollbill','',1499750702,73.16,0,'ubuntu16\n'),('poll','22',1499751002,1.04,1,'ubuntu16\n'),('poll','28',1499751002,1.72,1,'ubuntu16\n'),('pollbill','',1499751002,73.43,0,'ubuntu16\n'),('poll','22',1499751302,1.05,1,'ubuntu16\n'),('poll','28',1499751302,1.67,1,'ubuntu16\n'),('pollbill','',1499751302,73.25,0,'ubuntu16\n'),('poll','22',1499751602,1.03,1,'ubuntu16\n'),('poll','28',1499751602,2.08,1,'ubuntu16\n'),('pollbill','',1499751602,73.26,0,'ubuntu16\n'),('poll','22',1499751902,1.05,1,'ubuntu16\n'),('poll','28',1499751902,1.57,1,'ubuntu16\n'),('pollbill','',1499751902,73.26,0,'ubuntu16\n'),('poll','22',1499752202,1.01,1,'ubuntu16\n'),('poll','28',1499752202,1.90,1,'ubuntu16\n'),('pollbill','',1499752201,73.11,0,'ubuntu16\n'),('poll','22',1499752503,1.05,1,'ubuntu16\n'),('poll','28',1499752503,1.96,1,'ubuntu16\n'),('pollbill','',1499752502,73.24,0,'ubuntu16\n'),('poll','22',1499752802,1.03,1,'ubuntu16\n'),('poll','28',1499752802,1.78,1,'ubuntu16\n'),('pollbill','',1499752801,73.15,0,'ubuntu16\n'),('poll','22',1499753102,1.00,1,'ubuntu16\n'),('poll','28',1499753102,1.87,1,'ubuntu16\n'),('pollbill','',1499753101,73.13,0,'ubuntu16\n'),('poll','22',1499753402,1.02,1,'ubuntu16\n'),('poll','28',1499753402,1.70,1,'ubuntu16\n'),('pollbill','',1499753402,73.32,0,'ubuntu16\n'),('poll','22',1499753702,1.02,1,'ubuntu16\n'),('poll','28',1499753702,1.85,1,'ubuntu16\n'),('pollbill','',1499753702,73.30,0,'ubuntu16\n'),('poll','22',1499754002,1.01,1,'ubuntu16\n'),('poll','28',1499754002,2.03,1,'ubuntu16\n'),('pollbill','',1499754002,73.48,0,'ubuntu16\n'),('poll','22',1499754306,1.52,1,'ubuntu16\n'),('poll','28',1499754306,2.17,1,'ubuntu16\n'),('pollbill','',1499754306,74.07,0,'ubuntu16\n'),('poll','22',1499754602,1.00,1,'ubuntu16\n'),('poll','28',1499754602,1.33,1,'ubuntu16\n'),('pollbill','',1499754602,72.96,0,'ubuntu16\n'),('poll','22',1499754902,1.06,1,'ubuntu16\n'),('poll','28',1499754902,1.74,1,'ubuntu16\n'),('pollbill','',1499754901,73.23,0,'ubuntu16\n'),('poll','22',1499755204,1.04,1,'ubuntu16\n'),('poll','28',1499755204,1.39,1,'ubuntu16\n'),('pollbill','',1499755204,72.86,0,'ubuntu16\n'),('poll','22',1499755502,1.05,1,'ubuntu16\n'),('poll','28',1499755502,1.39,1,'ubuntu16\n'),('pollbill','',1499755502,72.99,0,'ubuntu16\n'),('poll','22',1499755804,1.05,1,'ubuntu16\n'),('poll','28',1499755804,1.76,1,'ubuntu16\n'),('pollbill','',1499755804,72.78,0,'ubuntu16\n'),('poll','22',1499756105,1.00,1,'ubuntu16\n'),('poll','28',1499756105,1.38,1,'ubuntu16\n'),('pollbill','',1499756105,72.98,0,'ubuntu16\n'),('poll','22',1499756402,1.20,1,'ubuntu16\n'),('poll','28',1499756402,1.71,1,'ubuntu16\n'),('pollbill','',1499756402,73.05,0,'ubuntu16\n'),('poll','22',1499756703,1.05,1,'ubuntu16\n'),('poll','28',1499756703,1.40,1,'ubuntu16\n'),('pollbill','',1499756703,73.91,0,'ubuntu16\n'),('poll','22',1499757002,1.02,1,'ubuntu16\n'),('poll','28',1499757002,1.51,1,'ubuntu16\n'),('pollbill','',1499757002,72.95,0,'ubuntu16\n'),('poll','22',1499757304,1.46,1,'ubuntu16\n'),('poll','28',1499757304,1.79,1,'ubuntu16\n'),('pollbill','',1499757304,72.93,0,'ubuntu16\n'),('poll','22',1499757603,1.01,1,'ubuntu16\n'),('poll','28',1499757603,1.25,1,'ubuntu16\n'),('pollbill','',1499757603,73.03,0,'ubuntu16\n'),('poll','22',1499757903,1.03,1,'ubuntu16\n'),('poll','28',1499757903,1.90,1,'ubuntu16\n'),('pollbill','',1499757902,73.24,0,'ubuntu16\n'),('poll','22',1499758204,1.15,1,'ubuntu16\n'),('poll','28',1499758204,3.13,1,'ubuntu16\n'),('pollbill','',1499758204,73.21,0,'ubuntu16\n'),('poll','22',1499758502,1.01,1,'ubuntu16\n'),('poll','28',1499758502,2.64,1,'ubuntu16\n'),('pollbill','',1499758501,72.98,0,'ubuntu16\n'),('poll','22',1499758802,1.00,1,'ubuntu16\n'),('poll','28',1499758802,1.60,1,'ubuntu16\n'),('pollbill','',1499758802,72.93,0,'ubuntu16\n'),('poll','22',1499759104,1.02,1,'ubuntu16\n'),('poll','28',1499759104,1.36,1,'ubuntu16\n'),('pollbill','',1499759104,72.94,0,'ubuntu16\n'),('poll','22',1499759403,1.02,1,'ubuntu16\n'),('poll','28',1499759403,1.38,1,'ubuntu16\n'),('pollbill','',1499759403,73.05,0,'ubuntu16\n'),('poll','22',1499759705,1.04,1,'ubuntu16\n'),('poll','28',1499759705,1.70,1,'ubuntu16\n'),('pollbill','',1499759705,72.95,0,'ubuntu16\n'),('poll','22',1499760002,1.12,1,'ubuntu16\n'),('poll','28',1499760002,1.36,1,'ubuntu16\n'),('pollbill','',1499760002,73.14,0,'ubuntu16\n'),('poll','22',1499760302,1.03,1,'ubuntu16\n'),('poll','28',1499760302,1.59,1,'ubuntu16\n'),('pollbill','',1499760302,73.04,0,'ubuntu16\n'),('poll','22',1499760605,1.00,1,'ubuntu16\n'),('poll','28',1499760605,1.69,1,'ubuntu16\n'),('pollbill','',1499760604,72.90,0,'ubuntu16\n'),('poll','22',1499760902,1.00,1,'ubuntu16\n'),('poll','28',1499760902,3.61,1,'ubuntu16\n'),('pollbill','',1499760902,73.01,0,'ubuntu16\n'),('poll','22',1499761203,1.12,1,'ubuntu16\n'),('poll','28',1499761203,1.50,1,'ubuntu16\n'),('pollbill','',1499761203,73.06,0,'ubuntu16\n'),('poll','22',1499761504,1.02,1,'ubuntu16\n'),('poll','28',1499761504,1.65,1,'ubuntu16\n'),('pollbill','',1499761503,73.06,0,'ubuntu16\n'),('poll','22',1499761802,1.02,1,'ubuntu16\n'),('poll','28',1499761802,1.53,1,'ubuntu16\n'),('pollbill','',1499761802,73.02,0,'ubuntu16\n'),('poll','22',1499762105,1.09,1,'ubuntu16\n'),('poll','28',1499762105,1.44,1,'ubuntu16\n'),('pollbill','',1499762105,73.00,0,'ubuntu16\n'),('poll','22',1499762402,0.99,1,'ubuntu16\n'),('poll','28',1499762402,2.63,1,'ubuntu16\n'),('pollbill','',1499762402,72.85,0,'ubuntu16\n'),('poll','22',1499762703,1.01,1,'ubuntu16\n'),('poll','28',1499762703,1.48,1,'ubuntu16\n'),('pollbill','',1499762703,73.04,0,'ubuntu16\n'),('poll','22',1499763004,0.99,1,'ubuntu16\n'),('poll','28',1499763004,1.39,1,'ubuntu16\n'),('pollbill','',1499763004,73.01,0,'ubuntu16\n'),('poll','22',1499763303,1.01,1,'ubuntu16\n'),('poll','28',1499763303,1.79,1,'ubuntu16\n'),('pollbill','',1499763302,73.03,0,'ubuntu16\n'),('poll','22',1499763604,1.42,1,'ubuntu16\n'),('poll','28',1499763605,1.85,1,'ubuntu16\n'),('pollbill','',1499763604,73.28,0,'ubuntu16\n'),('poll','22',1499763902,1.00,1,'ubuntu16\n'),('poll','28',1499763902,1.34,1,'ubuntu16\n'),('pollbill','',1499763902,73.03,0,'ubuntu16\n'),('poll','22',1499764202,1.15,1,'ubuntu16\n'),('poll','28',1499764202,1.85,1,'ubuntu16\n'),('pollbill','',1499764202,73.08,0,'ubuntu16\n'),('poll','22',1499764505,1.00,1,'ubuntu16\n'),('poll','28',1499764505,1.47,1,'ubuntu16\n'),('pollbill','',1499764505,73.07,0,'ubuntu16\n'),('poll','22',1499764802,1.00,1,'ubuntu16\n'),('poll','28',1499764802,1.48,1,'ubuntu16\n'),('pollbill','',1499764802,73.03,0,'ubuntu16\n'),('poll','22',1499765105,1.03,1,'ubuntu16\n'),('poll','28',1499765105,1.46,1,'ubuntu16\n'),('pollbill','',1499765105,73.06,0,'ubuntu16\n'),('poll','22',1499765402,1.01,1,'ubuntu16\n'),('poll','28',1499765402,1.50,1,'ubuntu16\n'),('pollbill','',1499765402,73.09,0,'ubuntu16\n'),('poll','22',1499765703,1.03,1,'ubuntu16\n'),('poll','28',1499765703,1.62,1,'ubuntu16\n'),('pollbill','',1499765702,72.95,0,'ubuntu16\n'),('poll','22',1499766004,1.00,1,'ubuntu16\n'),('poll','28',1499766004,1.36,1,'ubuntu16\n'),('pollbill','',1499766004,72.94,0,'ubuntu16\n'),('poll','22',1499766302,1.03,1,'ubuntu16\n'),('poll','28',1499766302,1.42,1,'ubuntu16\n'),('pollbill','',1499766302,72.87,0,'ubuntu16\n'),('poll','22',1499766602,1.08,1,'ubuntu16\n'),('poll','28',1499766602,1.37,1,'ubuntu16\n'),('pollbill','',1499766602,73.05,0,'ubuntu16\n'),('poll','22',1499985003,3.44,1,'ubuntu16\n'),('poll','28',1499985003,13.13,1,'ubuntu16\n'),('pollbill','',1499985002,83.75,0,'ubuntu16\n'),('poll','22',1499985308,2.11,1,'ubuntu16\n'),('poll','28',1499985308,4.82,1,'ubuntu16\n'),('pollbill','',1499985307,73.67,0,'ubuntu16\n'),('poll','22',1499985604,1.29,1,'ubuntu16\n'),('poll','28',1499985604,4.21,1,'ubuntu16\n'),('pollbill','',1499985604,74.19,0,'ubuntu16\n'),('poll','22',1499985907,5.05,1,'ubuntu16\n'),('poll','28',1499985911,3.71,1,'ubuntu16\n'),('pollbill','',1499985911,73.44,0,'ubuntu16\n'),('poll','22',1499986202,1.44,1,'ubuntu16\n'),('poll','28',1499986202,10.67,1,'ubuntu16\n'),('pollbill','',1499986202,79.06,0,'ubuntu16\n'),('poll','22',1499986504,1.18,1,'ubuntu16\n'),('poll','28',1499986504,5.01,1,'ubuntu16\n'),('pollbill','',1499986505,74.34,0,'ubuntu16\n'),('poll','22',1499986802,0.99,1,'ubuntu16\n'),('poll','28',1499986802,1.77,1,'ubuntu16\n'),('pollbill','',1499986802,73.36,0,'ubuntu16\n'),('poll','22',1499987104,1.10,1,'ubuntu16\n'),('poll','28',1499987102,5.40,1,'ubuntu16\n'),('pollbill','',1499987103,74.07,0,'ubuntu16\n'),('poll','22',1499987402,1.03,1,'ubuntu16\n'),('poll','28',1499987402,3.68,1,'ubuntu16\n'),('pollbill','',1499987401,73.20,0,'ubuntu16\n'),('poll','22',1499987703,1.08,1,'ubuntu16\n'),('poll','28',1499987703,4.01,1,'ubuntu16\n'),('pollbill','',1499987702,73.69,0,'ubuntu16\n'),('poll','22',1499988004,1.00,1,'ubuntu16\n'),('poll','28',1499988004,3.79,1,'ubuntu16\n'),('pollbill','',1499988004,73.34,0,'ubuntu16\n'),('poll','22',1499988302,1.71,1,'ubuntu16\n'),('poll','28',1499988302,4.78,1,'ubuntu16\n'),('pollbill','',1499988302,73.65,0,'ubuntu16\n'),('poll','22',1499988608,1.09,1,'ubuntu16\n'),('poll','28',1499988608,3.76,1,'ubuntu16\n'),('pollbill','',1499988607,73.06,0,'ubuntu16\n'),('poll','22',1499988902,1.07,1,'ubuntu16\n'),('poll','28',1499988902,3.59,1,'ubuntu16\n'),('pollbill','',1499988902,73.29,0,'ubuntu16\n'),('poll','22',1499989202,1.00,1,'ubuntu16\n'),('poll','28',1499989202,3.65,1,'ubuntu16\n'),('pollbill','',1499989202,73.46,0,'ubuntu16\n'),('poll','22',1499989503,1.10,1,'ubuntu16\n'),('poll','28',1499989503,3.67,1,'ubuntu16\n'),('pollbill','',1499989503,73.64,0,'ubuntu16\n'),('poll','22',1499989802,1.07,1,'ubuntu16\n'),('poll','28',1499989802,3.69,1,'ubuntu16\n'),('pollbill','',1499989802,73.37,0,'ubuntu16\n'),('poll','22',1499990103,1.05,1,'ubuntu16\n'),('poll','28',1499990103,1.37,1,'ubuntu16\n'),('pollbill','',1499990102,73.57,0,'ubuntu16\n'),('poll','22',1499990402,1.04,1,'ubuntu16\n'),('poll','28',1499990402,4.20,1,'ubuntu16\n'),('pollbill','',1499990402,74.09,0,'ubuntu16\n'),('poll','22',1499990702,1.10,1,'ubuntu16\n'),('poll','28',1499990702,3.70,1,'ubuntu16\n'),('pollbill','',1499990702,74.07,0,'ubuntu16\n'),('poll','22',1499991003,1.05,1,'ubuntu16\n'),('poll','28',1499991003,3.60,1,'ubuntu16\n'),('pollbill','',1499991003,73.65,0,'ubuntu16\n'),('poll','22',1499991302,1.01,1,'ubuntu16\n'),('poll','28',1499991302,3.62,1,'ubuntu16\n'),('pollbill','',1499991302,73.39,0,'ubuntu16\n'),('poll','22',1499991604,1.10,1,'ubuntu16\n'),('poll','28',1499991604,3.79,1,'ubuntu16\n'),('pollbill','',1499991603,73.85,0,'ubuntu16\n'),('poll','22',1499995202,1.10,1,'ubuntu16\n'),('poll','28',1499995202,3.69,1,'ubuntu16\n'),('pollbill','',1499995202,74.02,0,'ubuntu16\n'),('poll','22',1499995502,1.05,1,'ubuntu16\n'),('poll','28',1499995502,3.79,1,'ubuntu16\n'),('pollbill','',1499995501,73.60,0,'ubuntu16\n'),('poll','22',1499995802,1.04,1,'ubuntu16\n'),('poll','28',1499995802,3.91,1,'ubuntu16\n'),('pollbill','',1499995802,73.44,0,'ubuntu16\n'),('poll','22',1499996102,1.04,1,'ubuntu16\n'),('poll','28',1499996102,3.84,1,'ubuntu16\n'),('pollbill','',1499996102,73.81,0,'ubuntu16\n'),('poll','22',1499996403,1.50,1,'ubuntu16\n'),('poll','28',1499996403,1.65,1,'ubuntu16\n'),('pollbill','',1499996403,73.97,0,'ubuntu16\n'),('poll','22',1499996702,1.01,1,'ubuntu16\n'),('poll','28',1499996702,1.99,1,'ubuntu16\n'),('pollbill','',1499996701,73.49,0,'ubuntu16\n'),('poll','22',1499997002,1.22,1,'ubuntu16\n'),('poll','28',1499997002,1.86,1,'ubuntu16\n'),('pollbill','',1499997002,73.43,0,'ubuntu16\n'),('poll','22',1499997302,1.02,1,'ubuntu16\n'),('poll','28',1499997302,1.80,1,'ubuntu16\n'),('pollbill','',1499997302,73.46,0,'ubuntu16\n'),('poll','22',1499997602,1.01,1,'ubuntu16\n'),('poll','28',1499997602,1.60,1,'ubuntu16\n'),('pollbill','',1499997602,74.02,0,'ubuntu16\n'),('poll','22',1499997903,1.07,1,'ubuntu16\n'),('poll','28',1499997903,1.91,1,'ubuntu16\n'),('pollbill','',1499997902,73.70,0,'ubuntu16\n'),('poll','22',1499998202,1.01,1,'ubuntu16\n'),('poll','28',1499998202,2.03,1,'ubuntu16\n'),('pollbill','',1499998202,73.70,0,'ubuntu16\n'),('poll','22',1499998502,1.01,1,'ubuntu16\n'),('poll','28',1499998502,1.45,1,'ubuntu16\n'),('pollbill','',1499998501,73.84,0,'ubuntu16\n'),('poll','22',1499998803,1.02,1,'ubuntu16\n'),('poll','28',1499998803,3.90,1,'ubuntu16\n'),('pollbill','',1499998803,73.81,0,'ubuntu16\n'),('poll','22',1499999102,1.26,1,'ubuntu16\n'),('poll','28',1499999102,2.56,1,'ubuntu16\n'),('pollbill','',1499999101,73.65,0,'ubuntu16\n'),('poll','22',1499999402,1.00,1,'ubuntu16\n'),('poll','28',1499999402,1.99,1,'ubuntu16\n'),('pollbill','',1499999402,73.53,0,'ubuntu16\n'),('poll','22',1499999703,1.08,1,'ubuntu16\n'),('poll','28',1499999703,1.73,1,'ubuntu16\n'),('pollbill','',1499999703,73.63,0,'ubuntu16\n'),('poll','22',1500000002,1.06,1,'ubuntu16\n'),('poll','28',1500000002,2.29,1,'ubuntu16\n'),('pollbill','',1500000002,73.91,0,'ubuntu16\n'),('poll','22',1500000305,1.01,1,'ubuntu16\n'),('poll','28',1500000304,4.88,1,'ubuntu16\n'),('pollbill','',1500000302,74.90,0,'ubuntu16\n'),('poll','22',1500000602,1.14,1,'ubuntu16\n'),('poll','28',1500000602,2.34,1,'ubuntu16\n'),('pollbill','',1500000602,73.77,0,'ubuntu16\n'),('poll','22',1500000902,1.10,1,'ubuntu16\n'),('poll','28',1500000902,1.74,1,'ubuntu16\n'),('pollbill','',1500000902,73.56,0,'ubuntu16\n'),('poll','22',1500001202,1.14,1,'ubuntu16\n'),('poll','28',1500001202,1.91,1,'ubuntu16\n'),('pollbill','',1500001201,73.73,0,'ubuntu16\n'),('poll','22',1500001502,1.03,1,'ubuntu16\n'),('poll','28',1500001502,1.69,1,'ubuntu16\n'),('pollbill','',1500001502,73.27,0,'ubuntu16\n'),('poll','22',1500001802,1.40,1,'ubuntu16\n'),('poll','28',1500001803,2.02,1,'ubuntu16\n'),('pollbill','',1500001802,74.12,0,'ubuntu16\n'),('poll','22',1500002102,1.03,1,'ubuntu16\n'),('poll','28',1500002102,1.59,1,'ubuntu16\n'),('pollbill','',1500002102,73.49,0,'ubuntu16\n'),('poll','22',1500002403,1.03,1,'ubuntu16\n'),('poll','28',1500002403,1.79,1,'ubuntu16\n'),('pollbill','',1500002402,73.85,0,'ubuntu16\n'),('poll','22',1500002702,1.01,1,'ubuntu16\n'),('poll','28',1500002702,2.21,1,'ubuntu16\n'),('pollbill','',1500002702,73.57,0,'ubuntu16\n'),('poll','22',1500003002,1.04,1,'ubuntu16\n'),('poll','28',1500003002,1.94,1,'ubuntu16\n'),('pollbill','',1500003001,74.27,0,'ubuntu16\n'),('poll','22',1500003302,1.02,1,'ubuntu16\n'),('poll','28',1500003302,1.47,1,'ubuntu16\n'),('pollbill','',1500003302,73.88,0,'ubuntu16\n'),('poll','22',1500003602,1.08,1,'ubuntu16\n'),('poll','28',1500003602,1.68,1,'ubuntu16\n'),('pollbill','',1500003602,73.95,0,'ubuntu16\n'),('poll','22',1500003902,1.01,1,'ubuntu16\n'),('poll','28',1500003902,1.57,1,'ubuntu16\n'),('pollbill','',1500003902,73.51,0,'ubuntu16\n'),('poll','22',1500004202,1.13,1,'ubuntu16\n'),('poll','28',1500004202,1.54,1,'ubuntu16\n'),('pollbill','',1500004202,73.94,0,'ubuntu16\n'),('poll','22',1500004502,1.08,1,'ubuntu16\n'),('poll','28',1500004502,1.48,1,'ubuntu16\n'),('pollbill','',1500004502,73.65,0,'ubuntu16\n'),('poll','22',1500004803,1.11,1,'ubuntu16\n'),('poll','28',1500004803,1.38,1,'ubuntu16\n'),('pollbill','',1500004803,73.41,0,'ubuntu16\n'),('poll','22',1500005102,1.07,1,'ubuntu16\n'),('poll','28',1500005102,1.45,1,'ubuntu16\n'),('pollbill','',1500005102,73.60,0,'ubuntu16\n'),('poll','22',1500005403,1.01,1,'ubuntu16\n'),('poll','28',1500005403,4.33,1,'ubuntu16\n'),('pollbill','',1500005402,73.86,0,'ubuntu16\n'),('poll','22',1500005702,1.04,1,'ubuntu16\n'),('poll','28',1500005702,1.62,1,'ubuntu16\n'),('pollbill','',1500005702,73.56,0,'ubuntu16\n'),('poll','22',1500006002,1.05,1,'ubuntu16\n'),('poll','28',1500006002,1.62,1,'ubuntu16\n'),('pollbill','',1500006002,78.41,0,'ubuntu16\n'),('poll','22',1500006303,1.02,1,'ubuntu16\n'),('poll','28',1500006303,1.88,1,'ubuntu16\n'),('pollbill','',1500006303,73.19,0,'ubuntu16\n'),('poll','22',1500006602,1.01,1,'ubuntu16\n'),('poll','28',1500006602,1.34,1,'ubuntu16\n'),('pollbill','',1500006602,73.17,0,'ubuntu16\n'),('poll','22',1500006903,1.21,1,'ubuntu16\n'),('poll','28',1500006903,1.76,1,'ubuntu16\n'),('pollbill','',1500006902,73.63,0,'ubuntu16\n'),('poll','22',1500007202,1.04,1,'ubuntu16\n'),('poll','28',1500007202,1.50,1,'ubuntu16\n'),('pollbill','',1500007202,73.00,0,'ubuntu16\n'),('poll','22',1500007502,1.08,1,'ubuntu16\n'),('poll','28',1500007502,1.72,1,'ubuntu16\n'),('pollbill','',1500007501,73.06,0,'ubuntu16\n'),('poll','22',1500007803,1.15,1,'ubuntu16\n'),('poll','28',1500007803,2.20,1,'ubuntu16\n'),('pollbill','',1500007803,73.36,0,'ubuntu16\n'),('poll','22',1500008102,1.00,1,'ubuntu16\n'),('poll','28',1500008102,1.99,1,'ubuntu16\n'),('pollbill','',1500008102,73.00,0,'ubuntu16\n'),('poll','22',1500008402,1.05,1,'ubuntu16\n'),('poll','28',1500008402,1.66,1,'ubuntu16\n'),('pollbill','',1500008401,73.03,0,'ubuntu16\n'),('poll','22',1500008703,1.09,1,'ubuntu16\n'),('poll','28',1500008703,1.73,1,'ubuntu16\n'),('pollbill','',1500008702,73.05,0,'ubuntu16\n'),('poll','22',1500009003,1.06,1,'ubuntu16\n'),('poll','28',1500009003,2.12,1,'ubuntu16\n'),('pollbill','',1500009002,73.28,0,'ubuntu16\n'),('poll','22',1500009303,1.03,1,'ubuntu16\n'),('poll','28',1500009303,3.87,1,'ubuntu16\n'),('pollbill','',1500009303,73.43,0,'ubuntu16\n'),('poll','22',1500009602,1.03,1,'ubuntu16\n'),('poll','28',1500009602,1.58,1,'ubuntu16\n'),('pollbill','',1500009602,73.32,0,'ubuntu16\n'),('poll','22',1500009903,1.10,1,'ubuntu16\n'),('poll','28',1500009903,1.40,1,'ubuntu16\n'),('pollbill','',1500009903,73.59,0,'ubuntu16\n'),('poll','22',1500010202,1.01,1,'ubuntu16\n'),('poll','28',1500010202,1.68,1,'ubuntu16\n'),('pollbill','',1500010202,73.03,0,'ubuntu16\n'),('poll','22',1500010502,0.99,1,'ubuntu16\n'),('poll','28',1500010502,1.46,1,'ubuntu16\n'),('pollbill','',1500010502,73.36,0,'ubuntu16\n'),('poll','22',1500010802,3.01,1,'ubuntu16\n'),('poll','28',1500010804,1.62,1,'ubuntu16\n'),('pollbill','',1500010804,73.84,0,'ubuntu16\n'),('poll','22',1500011103,1.08,1,'ubuntu16\n'),('poll','28',1500011103,1.48,1,'ubuntu16\n'),('pollbill','',1500011102,73.42,0,'ubuntu16\n'),('poll','22',1500011403,1.03,1,'ubuntu16\n'),('poll','28',1500011403,1.46,1,'ubuntu16\n'),('pollbill','',1500011403,73.15,0,'ubuntu16\n'),('poll','22',1500011702,1.01,1,'ubuntu16\n'),('poll','28',1500011702,1.70,1,'ubuntu16\n'),('pollbill','',1500011702,73.61,0,'ubuntu16\n'),('poll','22',1500012002,1.06,1,'ubuntu16\n'),('poll','28',1500012002,2.81,1,'ubuntu16\n'),('pollbill','',1500012001,73.46,0,'ubuntu16\n'),('poll','22',1500012303,1.03,1,'ubuntu16\n'),('poll','28',1500012303,1.40,1,'ubuntu16\n'),('pollbill','',1500012303,73.07,0,'ubuntu16\n'),('poll','22',1500012602,1.02,1,'ubuntu16\n'),('poll','28',1500012602,1.71,1,'ubuntu16\n'),('pollbill','',1500012602,73.26,0,'ubuntu16\n'),('poll','22',1500012902,1.20,1,'ubuntu16\n'),('poll','28',1500012902,1.30,1,'ubuntu16\n'),('pollbill','',1500012902,73.26,0,'ubuntu16\n'),('poll','22',1500013202,1.06,1,'ubuntu16\n'),('poll','28',1500013202,2.22,1,'ubuntu16\n'),('pollbill','',1500013202,73.78,0,'ubuntu16\n'),('poll','22',1500013502,1.05,1,'ubuntu16\n'),('poll','28',1500013502,2.08,1,'ubuntu16\n'),('pollbill','',1500013502,73.52,0,'ubuntu16\n'),('poll','22',1500013803,1.06,1,'ubuntu16\n'),('poll','28',1500013803,1.96,1,'ubuntu16\n'),('pollbill','',1500013803,73.98,0,'ubuntu16\n'),('poll','22',1500014102,1.07,1,'ubuntu16\n'),('poll','28',1500014102,1.69,1,'ubuntu16\n'),('pollbill','',1500014102,73.68,0,'ubuntu16\n'),('poll','22',1500014402,1.05,1,'ubuntu16\n'),('poll','28',1500014402,1.70,1,'ubuntu16\n'),('pollbill','',1500014402,73.60,0,'ubuntu16\n'),('poll','22',1500014702,1.00,1,'ubuntu16\n'),('poll','28',1500014702,1.50,1,'ubuntu16\n'),('pollbill','',1500014702,73.27,0,'ubuntu16\n'),('poll','22',1500015002,1.01,1,'ubuntu16\n'),('poll','28',1500015002,1.40,1,'ubuntu16\n'),('pollbill','',1500015002,73.05,0,'ubuntu16\n'),('poll','22',1500015304,1.01,1,'ubuntu16\n'),('poll','28',1500015304,1.42,1,'ubuntu16\n'),('pollbill','',1500015303,73.40,0,'ubuntu16\n'),('poll','22',1500015602,1.04,1,'ubuntu16\n'),('poll','28',1500015602,2.68,1,'ubuntu16\n'),('pollbill','',1500015602,73.18,0,'ubuntu16\n'),('poll','22',1500015903,1.01,1,'ubuntu16\n'),('poll','28',1500015903,1.43,1,'ubuntu16\n'),('pollbill','',1500015902,73.47,0,'ubuntu16\n'),('poll','22',1500016202,1.02,1,'ubuntu16\n'),('poll','28',1500016202,1.49,1,'ubuntu16\n'),('pollbill','',1500016202,73.29,0,'ubuntu16\n'),('poll','22',1500016502,1.07,1,'ubuntu16\n'),('poll','28',1500016502,1.50,1,'ubuntu16\n'),('pollbill','',1500016502,73.32,0,'ubuntu16\n'),('poll','22',1500016802,1.01,1,'ubuntu16\n'),('poll','28',1500016802,1.81,1,'ubuntu16\n'),('pollbill','',1500016802,73.43,0,'ubuntu16\n'),('poll','22',1500017102,1.00,1,'ubuntu16\n'),('poll','28',1500017102,1.54,1,'ubuntu16\n'),('pollbill','',1500017101,72.87,0,'ubuntu16\n'),('poll','22',1500017403,1.02,1,'ubuntu16\n'),('poll','28',1500017403,1.43,1,'ubuntu16\n'),('pollbill','',1500017403,73.09,0,'ubuntu16\n'),('poll','22',1500017703,1.00,1,'ubuntu16\n'),('poll','28',1500017703,1.33,1,'ubuntu16\n'),('pollbill','',1500017702,72.91,0,'ubuntu16\n'),('poll','22',1500018003,1.04,1,'ubuntu16\n'),('poll','28',1500018003,2.37,1,'ubuntu16\n'),('pollbill','',1500018003,72.96,0,'ubuntu16\n'),('poll','22',1500018303,1.03,1,'ubuntu16\n'),('poll','28',1500018303,1.46,1,'ubuntu16\n'),('pollbill','',1500018303,72.94,0,'ubuntu16\n'),('poll','22',1500018602,1.16,1,'ubuntu16\n'),('poll','28',1500018603,1.38,1,'ubuntu16\n'),('pollbill','',1500018602,73.20,0,'ubuntu16\n'),('poll','22',1500018902,1.02,1,'ubuntu16\n'),('poll','28',1500018902,1.53,1,'ubuntu16\n'),('pollbill','',1500018902,72.93,0,'ubuntu16\n'),('poll','22',1500019202,1.02,1,'ubuntu16\n'),('poll','28',1500019202,1.39,1,'ubuntu16\n'),('pollbill','',1500019201,73.07,0,'ubuntu16\n'),('poll','22',1500019503,1.01,1,'ubuntu16\n'),('poll','28',1500019503,1.48,1,'ubuntu16\n'),('pollbill','',1500019502,72.98,0,'ubuntu16\n'),('poll','22',1500019802,1.00,1,'ubuntu16\n'),('poll','28',1500019802,1.36,1,'ubuntu16\n'),('pollbill','',1500019802,73.13,0,'ubuntu16\n'),('poll','22',1500020103,1.00,1,'ubuntu16\n'),('poll','28',1500020103,1.42,1,'ubuntu16\n'),('pollbill','',1500020102,73.75,0,'ubuntu16\n'),('poll','22',1500020402,1.04,1,'ubuntu16\n'),('poll','28',1500020402,1.47,1,'ubuntu16\n'),('pollbill','',1500020402,73.42,0,'ubuntu16\n'),('poll','22',1500020741,1.27,1,'ubuntu16\n'),('poll','28',1500020713,29.01,1,'ubuntu16\n'),('pollbill','',1500020739,75.80,0,'ubuntu16\n'),('poll','22',1500021002,1.03,1,'ubuntu16\n'),('poll','28',1500021002,1.36,1,'ubuntu16\n'),('pollbill','',1500021002,73.14,0,'ubuntu16\n'),('poll','22',1500021347,1.10,1,'ubuntu16\n'),('poll','28',1500021347,1.62,1,'ubuntu16\n'),('pollbill','',1500021346,74.08,0,'ubuntu16\n'),('poll','22',1500021602,1.08,1,'ubuntu16\n'),('poll','28',1500021602,1.53,1,'ubuntu16\n'),('pollbill','',1500021601,73.58,0,'ubuntu16\n'),('poll','22',1500021902,1.14,1,'ubuntu16\n'),('poll','28',1500021902,1.61,1,'ubuntu16\n'),('pollbill','',1500021902,74.05,0,'ubuntu16\n'),('poll','22',1500022202,1.10,1,'ubuntu16\n'),('poll','28',1500022202,1.47,1,'ubuntu16\n'),('pollbill','',1500022202,73.55,0,'ubuntu16\n'),('poll','22',1500022502,1.10,1,'ubuntu16\n'),('poll','28',1500022502,1.64,1,'ubuntu16\n'),('pollbill','',1500022502,73.21,0,'ubuntu16\n'),('poll','22',1500022802,1.02,1,'ubuntu16\n'),('poll','28',1500022802,1.80,1,'ubuntu16\n'),('pollbill','',1500022802,73.22,0,'ubuntu16\n'),('poll','22',1500023102,1.05,1,'ubuntu16\n'),('poll','28',1500023102,1.55,1,'ubuntu16\n'),('pollbill','',1500023102,73.19,0,'ubuntu16\n'),('poll','22',1500023402,1.04,1,'ubuntu16\n'),('poll','28',1500023402,1.53,1,'ubuntu16\n'),('pollbill','',1500023402,73.39,0,'ubuntu16\n'),('poll','22',1500023702,1.06,1,'ubuntu16\n'),('poll','28',1500023702,1.84,1,'ubuntu16\n'),('pollbill','',1500023702,73.08,0,'ubuntu16\n'),('poll','22',1500024002,1.01,1,'ubuntu16\n'),('poll','28',1500024002,1.46,1,'ubuntu16\n'),('pollbill','',1500024002,73.29,0,'ubuntu16\n'),('poll','22',1500024303,1.03,1,'ubuntu16\n'),('poll','28',1500024302,1.92,1,'ubuntu16\n'),('pollbill','',1500024302,73.40,0,'ubuntu16\n'),('poll','22',1500024602,1.03,1,'ubuntu16\n'),('poll','28',1500024602,1.34,1,'ubuntu16\n'),('pollbill','',1500024602,73.25,0,'ubuntu16\n'),('poll','22',1500024903,1.11,1,'ubuntu16\n'),('poll','28',1500024903,2.05,1,'ubuntu16\n'),('pollbill','',1500024902,73.07,0,'ubuntu16\n'),('poll','22',1500025202,1.05,1,'ubuntu16\n'),('poll','28',1500025202,1.51,1,'ubuntu16\n'),('pollbill','',1500025202,72.79,0,'ubuntu16\n'),('poll','22',1500025503,1.07,1,'ubuntu16\n'),('poll','28',1500025503,1.70,1,'ubuntu16\n'),('pollbill','',1500025502,72.98,0,'ubuntu16\n'),('poll','22',1500025803,1.09,1,'ubuntu16\n'),('poll','28',1500025803,1.57,1,'ubuntu16\n'),('pollbill','',1500025803,72.89,0,'ubuntu16\n'),('poll','22',1500026102,1.02,1,'ubuntu16\n'),('poll','28',1500026102,1.41,1,'ubuntu16\n'),('pollbill','',1500026102,73.06,0,'ubuntu16\n'),('poll','22',1500026402,1.13,1,'ubuntu16\n'),('poll','28',1500026402,1.93,1,'ubuntu16\n'),('pollbill','',1500026402,73.04,0,'ubuntu16\n'),('poll','22',1500026702,1.40,1,'ubuntu16\n'),('poll','28',1500026702,2.02,1,'ubuntu16\n'),('pollbill','',1500026702,84.52,0,'ubuntu16\n'),('poll','22',1500074404,1.06,1,'ubuntu16\n'),('poll','28',1500074404,4.25,1,'ubuntu16\n'),('pollbill','',1500074403,73.96,0,'ubuntu16\n'),('poll','22',1500074704,4.03,1,'ubuntu16\n'),('poll','28',1500074704,13.46,1,'ubuntu16\n'),('pollbill','',1500074702,78.86,0,'ubuntu16\n'),('poll','22',1500075015,1.09,1,'ubuntu16\n'),('poll','28',1500075015,3.87,1,'ubuntu16\n'),('pollbill','',1500075012,76.67,0,'ubuntu16\n'),('poll','22',1500075303,1.16,1,'ubuntu16\n'),('poll','28',1500075303,1.95,1,'ubuntu16\n'),('pollbill','',1500075303,73.38,0,'ubuntu16\n'),('poll','22',1500075602,5.74,1,'ubuntu16\n'),('poll','28',1500075602,5.87,1,'ubuntu16\n'),('pollbill','',1500075602,73.51,0,'ubuntu16\n'),('poll','22',1500075902,1.15,1,'ubuntu16\n'),('poll','28',1500075903,4.00,1,'ubuntu16\n'),('pollbill','',1500075902,73.86,0,'ubuntu16\n'),('poll','22',1500076202,1.63,1,'ubuntu16\n'),('poll','28',1500076202,5.37,1,'ubuntu16\n'),('pollbill','',1500076202,74.92,0,'ubuntu16\n'),('poll','22',1500076502,1.05,1,'ubuntu16\n'),('poll','28',1500076502,4.04,1,'ubuntu16\n'),('pollbill','',1500076502,73.62,0,'ubuntu16\n'),('poll','22',1500076802,1.46,1,'ubuntu16\n'),('poll','28',1500076802,5.85,1,'ubuntu16\n'),('pollbill','',1500076802,74.31,0,'ubuntu16\n'),('poll','22',1500077405,2.19,1,'ubuntu16\n'),('poll','28',1500077405,14.41,1,'ubuntu16\n'),('pollbill','',1500077402,86.03,0,'ubuntu16\n'),('poll','22',1500077703,1.06,1,'ubuntu16\n'),('poll','28',1500077703,4.08,1,'ubuntu16\n'),('pollbill','',1500077702,73.75,0,'ubuntu16\n'),('poll','22',1500078002,1.03,1,'ubuntu16\n'),('poll','28',1500078002,3.76,1,'ubuntu16\n'),('pollbill','',1500078002,73.44,0,'ubuntu16\n'),('poll','22',1500078302,1.01,1,'ubuntu16\n'),('poll','28',1500078302,3.99,1,'ubuntu16\n'),('pollbill','',1500078302,73.34,0,'ubuntu16\n'),('poll','22',1500078602,1.02,1,'ubuntu16\n'),('poll','28',1500078602,4.07,1,'ubuntu16\n'),('pollbill','',1500078602,73.24,0,'ubuntu16\n'),('poll','22',1500078902,1.08,1,'ubuntu16\n'),('poll','28',1500078902,3.82,1,'ubuntu16\n'),('pollbill','',1500078902,73.26,0,'ubuntu16\n'),('poll','22',1500079202,1.08,1,'ubuntu16\n'),('poll','28',1500079202,3.85,1,'ubuntu16\n'),('pollbill','',1500079202,73.29,0,'ubuntu16\n'),('poll','22',1500079503,1.00,1,'ubuntu16\n'),('poll','28',1500079503,3.66,1,'ubuntu16\n'),('pollbill','',1500079503,73.18,0,'ubuntu16\n'),('poll','22',1500079802,1.17,1,'ubuntu16\n'),('poll','28',1500079802,3.78,1,'ubuntu16\n'),('pollbill','',1500079802,73.93,0,'ubuntu16\n'),('poll','22',1500080102,1.00,1,'ubuntu16\n'),('poll','28',1500080102,3.59,1,'ubuntu16\n'),('pollbill','',1500080101,73.06,0,'ubuntu16\n'),('poll','22',1500080403,1.04,1,'ubuntu16\n'),('poll','28',1500080403,3.80,1,'ubuntu16\n'),('pollbill','',1500080403,73.25,0,'ubuntu16\n'),('poll','22',1500080702,1.00,1,'ubuntu16\n'),('poll','28',1500080702,3.57,1,'ubuntu16\n'),('pollbill','',1500080701,73.14,0,'ubuntu16\n'),('poll','22',1500081003,1.05,1,'ubuntu16\n'),('poll','28',1500081003,3.66,1,'ubuntu16\n'),('pollbill','',1500081003,73.06,0,'ubuntu16\n'),('poll','22',1500081302,1.01,1,'ubuntu16\n'),('poll','28',1500081302,3.56,1,'ubuntu16\n'),('pollbill','',1500081302,73.06,0,'ubuntu16\n'),('poll','22',1500081602,1.00,1,'ubuntu16\n'),('poll','28',1500081602,3.65,1,'ubuntu16\n'),('pollbill','',1500081602,73.48,0,'ubuntu16\n'),('poll','22',1500081902,1.06,1,'ubuntu16\n'),('poll','28',1500081902,3.64,1,'ubuntu16\n'),('pollbill','',1500081902,73.15,0,'ubuntu16\n'),('poll','22',1500082203,1.02,1,'ubuntu16\n'),('poll','28',1500082202,3.59,1,'ubuntu16\n'),('pollbill','',1500082202,73.14,0,'ubuntu16\n'),('poll','22',1500082502,1.01,1,'ubuntu16\n'),('poll','28',1500082502,3.62,1,'ubuntu16\n'),('pollbill','',1500082502,73.21,0,'ubuntu16\n'),('poll','22',1500082802,1.04,1,'ubuntu16\n'),('poll','28',1500082802,3.60,1,'ubuntu16\n'),('pollbill','',1500082802,73.10,0,'ubuntu16\n'),('poll','22',1500083102,0.99,1,'ubuntu16\n'),('poll','28',1500083102,3.73,1,'ubuntu16\n'),('pollbill','',1500083102,73.23,0,'ubuntu16\n'),('poll','22',1500083402,1.00,1,'ubuntu16\n'),('poll','28',1500083402,3.57,1,'ubuntu16\n'),('pollbill','',1500083402,73.13,0,'ubuntu16\n'),('poll','22',1500083702,1.22,1,'ubuntu16\n'),('poll','28',1500083702,3.95,1,'ubuntu16\n'),('pollbill','',1500083702,73.26,0,'ubuntu16\n'),('poll','22',1500084002,1.01,1,'ubuntu16\n'),('poll','28',1500084002,3.78,1,'ubuntu16\n'),('pollbill','',1500084002,72.82,0,'ubuntu16\n'),('poll','22',1500084302,1.04,1,'ubuntu16\n'),('poll','28',1500084302,3.70,1,'ubuntu16\n'),('pollbill','',1500084301,72.92,0,'ubuntu16\n'),('poll','22',1500084602,1.06,1,'ubuntu16\n'),('poll','28',1500084602,1.62,1,'ubuntu16\n'),('pollbill','',1500084602,73.03,0,'ubuntu16\n'),('poll','22',1500084902,1.00,1,'ubuntu16\n'),('poll','28',1500084902,3.73,1,'ubuntu16\n'),('pollbill','',1500084902,73.49,0,'ubuntu16\n'),('poll','22',1500085202,1.04,1,'ubuntu16\n'),('poll','28',1500085202,3.58,1,'ubuntu16\n'),('pollbill','',1500085202,73.23,0,'ubuntu16\n'),('poll','22',1500085502,0.98,1,'ubuntu16\n'),('poll','28',1500085502,3.60,1,'ubuntu16\n'),('pollbill','',1500085502,73.31,0,'ubuntu16\n'),('poll','22',1500085802,1.01,1,'ubuntu16\n'),('poll','28',1500085802,3.58,1,'ubuntu16\n'),('pollbill','',1500085801,73.23,0,'ubuntu16\n'),('poll','22',1500086102,1.06,1,'ubuntu16\n'),('poll','28',1500086102,3.65,1,'ubuntu16\n'),('pollbill','',1500086102,73.64,0,'ubuntu16\n'),('poll','22',1500086402,1.00,1,'ubuntu16\n'),('poll','28',1500086402,3.56,1,'ubuntu16\n'),('pollbill','',1500086401,72.97,0,'ubuntu16\n'),('poll','22',1500086703,0.99,1,'ubuntu16\n'),('poll','28',1500086702,4.05,1,'ubuntu16\n'),('pollbill','',1500086703,73.07,0,'ubuntu16\n'),('poll','22',1500087003,1.02,1,'ubuntu16\n'),('poll','28',1500087003,3.61,1,'ubuntu16\n'),('pollbill','',1500087002,73.31,0,'ubuntu16\n'),('poll','22',1500087302,1.03,1,'ubuntu16\n'),('poll','28',1500087302,3.74,1,'ubuntu16\n'),('pollbill','',1500087302,73.59,0,'ubuntu16\n'),('poll','22',1500087602,1.03,1,'ubuntu16\n'),('poll','28',1500087602,3.96,1,'ubuntu16\n'),('pollbill','',1500087601,73.80,0,'ubuntu16\n'),('poll','22',1500087904,1.03,1,'ubuntu16\n'),('poll','28',1500087904,3.82,1,'ubuntu16\n'),('pollbill','',1500087903,73.58,0,'ubuntu16\n'),('poll','22',1500098407,3.21,1,'ubuntu16\n'),('poll','28',1500098407,11.78,1,'ubuntu16\n'),('pollbill','',1500098407,81.28,0,'ubuntu16\n'),('poll','22',1500098702,1.00,1,'ubuntu16\n'),('poll','28',1500098702,4.27,1,'ubuntu16\n'),('pollbill','',1500098702,73.29,0,'ubuntu16\n'),('poll','22',1500099003,1.05,1,'ubuntu16\n'),('poll','28',1500099003,4.02,1,'ubuntu16\n'),('pollbill','',1500099002,73.51,0,'ubuntu16\n'),('poll','22',1500099302,1.06,1,'ubuntu16\n'),('poll','28',1500099302,4.06,1,'ubuntu16\n'),('pollbill','',1500099302,73.26,0,'ubuntu16\n'),('poll','22',1500099602,1.04,1,'ubuntu16\n'),('poll','28',1500099602,3.84,1,'ubuntu16\n'),('pollbill','',1500099602,73.18,0,'ubuntu16\n'),('poll','22',1500099902,1.03,1,'ubuntu16\n'),('poll','28',1500099902,3.56,1,'ubuntu16\n'),('pollbill','',1500099901,73.38,0,'ubuntu16\n'),('poll','22',1500100202,1.03,1,'ubuntu16\n'),('poll','28',1500100202,3.64,1,'ubuntu16\n'),('pollbill','',1500100201,73.23,0,'ubuntu16\n'),('poll','22',1500100502,1.02,1,'ubuntu16\n'),('poll','28',1500100502,3.60,1,'ubuntu16\n'),('pollbill','',1500100502,73.13,0,'ubuntu16\n'),('poll','22',1500100802,0.99,1,'ubuntu16\n'),('poll','28',1500100802,3.54,1,'ubuntu16\n'),('pollbill','',1500100802,73.06,0,'ubuntu16\n'),('poll','22',1500101102,1.01,1,'ubuntu16\n'),('poll','28',1500101102,3.57,1,'ubuntu16\n'),('pollbill','',1500101102,73.29,0,'ubuntu16\n'),('poll','22',1500101402,1.04,1,'ubuntu16\n'),('poll','28',1500101402,3.55,1,'ubuntu16\n'),('pollbill','',1500101402,72.84,0,'ubuntu16\n'),('poll','22',1500101702,1.00,1,'ubuntu16\n'),('poll','28',1500101702,3.55,1,'ubuntu16\n'),('pollbill','',1500101702,73.11,0,'ubuntu16\n'),('poll','22',1500102002,1.00,1,'ubuntu16\n'),('poll','28',1500102002,3.59,1,'ubuntu16\n'),('pollbill','',1500102002,73.25,0,'ubuntu16\n'),('poll','22',1500102302,1.00,1,'ubuntu16\n'),('poll','28',1500102302,3.64,1,'ubuntu16\n'),('pollbill','',1500102301,73.50,0,'ubuntu16\n'),('poll','22',1500102602,1.00,1,'ubuntu16\n'),('poll','28',1500102602,3.58,1,'ubuntu16\n'),('pollbill','',1500102602,73.19,0,'ubuntu16\n'),('poll','22',1500102902,1.02,1,'ubuntu16\n'),('poll','28',1500102902,3.62,1,'ubuntu16\n'),('pollbill','',1500102901,73.35,0,'ubuntu16\n'),('poll','22',1500103202,1.00,1,'ubuntu16\n'),('poll','28',1500103202,3.65,1,'ubuntu16\n'),('pollbill','',1500103202,73.30,0,'ubuntu16\n'),('poll','22',1500103502,0.99,1,'ubuntu16\n'),('poll','28',1500103502,3.58,1,'ubuntu16\n'),('pollbill','',1500103502,72.76,0,'ubuntu16\n'),('poll','22',1500103802,0.99,1,'ubuntu16\n'),('poll','28',1500103802,3.68,1,'ubuntu16\n'),('pollbill','',1500103801,72.84,0,'ubuntu16\n'),('poll','22',1500104102,1.03,1,'ubuntu16\n'),('poll','28',1500104102,3.58,1,'ubuntu16\n'),('pollbill','',1500104102,72.89,0,'ubuntu16\n'),('poll','22',1500104402,1.00,1,'ubuntu16\n'),('poll','28',1500104402,3.69,1,'ubuntu16\n'),('pollbill','',1500104402,72.91,0,'ubuntu16\n'),('poll','22',1500104702,1.02,1,'ubuntu16\n'),('poll','28',1500104702,3.77,1,'ubuntu16\n'),('poll','22',1500110707,4.74,1,'ubuntu16\n'),('poll','28',1500110707,23.93,1,'ubuntu16\n'),('pollbill','',1500110703,96.50,0,'ubuntu16\n'),('poll','22',1500111002,1.01,1,'ubuntu16\n'),('poll','28',1500111002,3.71,1,'ubuntu16\n'),('pollbill','',1500111002,73.00,0,'ubuntu16\n'),('poll','22',1500111302,1.01,1,'ubuntu16\n'),('poll','28',1500111302,3.64,1,'ubuntu16\n'),('pollbill','',1500111302,73.09,0,'ubuntu16\n'),('poll','22',1500111602,1.00,1,'ubuntu16\n'),('poll','28',1500111602,3.55,1,'ubuntu16\n'),('pollbill','',1500111601,73.28,0,'ubuntu16\n'),('poll','22',1500111901,1.03,1,'ubuntu16\n'),('poll','28',1500111902,3.55,1,'ubuntu16\n'),('pollbill','',1500111901,73.06,0,'ubuntu16\n'),('poll','22',1500112202,1.04,1,'ubuntu16\n'),('poll','28',1500112202,3.94,1,'ubuntu16\n'),('pollbill','',1500112202,72.94,0,'ubuntu16\n'),('poll','22',1500112502,1.03,1,'ubuntu16\n'),('poll','28',1500112502,3.61,1,'ubuntu16\n'),('pollbill','',1500112502,72.91,0,'ubuntu16\n'),('poll','22',1500112802,1.08,1,'ubuntu16\n'),('poll','28',1500112802,4.09,1,'ubuntu16\n'),('pollbill','',1500112802,72.92,0,'ubuntu16\n'),('poll','22',1500113102,1.02,1,'ubuntu16\n'),('poll','28',1500113102,3.72,1,'ubuntu16\n'),('pollbill','',1500113102,72.90,0,'ubuntu16\n'),('poll','22',1500113402,1.04,1,'ubuntu16\n'),('poll','28',1500113402,3.61,1,'ubuntu16\n'),('pollbill','',1500113402,72.91,0,'ubuntu16\n'),('poll','22',1500113702,1.04,1,'ubuntu16\n'),('poll','28',1500113702,4.07,1,'ubuntu16\n'),('pollbill','',1500113702,73.07,0,'ubuntu16\n'),('poll','22',1500114002,1.00,1,'ubuntu16\n'),('poll','28',1500114002,3.63,1,'ubuntu16\n'),('pollbill','',1500114002,73.29,0,'ubuntu16\n'),('poll','22',1500114302,1.05,1,'ubuntu16\n'),('poll','28',1500114302,3.71,1,'ubuntu16\n'),('pollbill','',1500114302,73.08,0,'ubuntu16\n'),('poll','22',1500114602,1.08,1,'ubuntu16\n'),('poll','28',1500114602,4.08,1,'ubuntu16\n'),('pollbill','',1500114602,73.13,0,'ubuntu16\n'),('poll','22',1500114902,1.00,1,'ubuntu16\n'),('poll','28',1500114902,3.77,1,'ubuntu16\n'),('pollbill','',1500114902,72.98,0,'ubuntu16\n'),('poll','22',1500115202,1.03,1,'ubuntu16\n'),('poll','28',1500115202,3.60,1,'ubuntu16\n'),('pollbill','',1500115201,72.99,0,'ubuntu16\n'),('poll','22',1500115502,1.02,1,'ubuntu16\n'),('poll','28',1500115502,3.57,1,'ubuntu16\n'),('pollbill','',1500115502,73.02,0,'ubuntu16\n'),('poll','22',1500115802,1.01,1,'ubuntu16\n'),('poll','28',1500115802,3.78,1,'ubuntu16\n'),('pollbill','',1500115802,72.88,0,'ubuntu16\n'),('poll','22',1500116103,1.06,1,'ubuntu16\n'),('poll','28',1500116103,3.70,1,'ubuntu16\n'),('pollbill','',1500116102,73.13,0,'ubuntu16\n'),('poll','22',1500116402,1.07,1,'ubuntu16\n'),('poll','28',1500116402,3.70,1,'ubuntu16\n'),('pollbill','',1500116402,73.04,0,'ubuntu16\n'),('poll','22',1500116702,1.03,1,'ubuntu16\n'),('poll','28',1500116702,3.71,1,'ubuntu16\n'),('pollbill','',1500116702,73.04,0,'ubuntu16\n'),('poll','22',1500117002,1.01,1,'ubuntu16\n'),('poll','28',1500117002,3.65,1,'ubuntu16\n'),('pollbill','',1500117002,73.34,0,'ubuntu16\n'),('poll','22',1500117302,1.03,1,'ubuntu16\n'),('poll','28',1500117302,3.62,1,'ubuntu16\n'),('pollbill','',1500117302,73.28,0,'ubuntu16\n'),('poll','22',1500117602,1.00,1,'ubuntu16\n'),('poll','28',1500117602,3.64,1,'ubuntu16\n'),('pollbill','',1500117602,73.01,0,'ubuntu16\n'),('poll','22',1500117902,1.00,1,'ubuntu16\n'),('poll','28',1500117902,3.57,1,'ubuntu16\n'),('pollbill','',1500117902,72.96,0,'ubuntu16\n'),('poll','22',1500118202,1.03,1,'ubuntu16\n'),('poll','28',1500118202,3.62,1,'ubuntu16\n'),('pollbill','',1500118202,73.03,0,'ubuntu16\n'),('poll','22',1500118502,1.01,1,'ubuntu16\n'),('poll','28',1500118502,3.58,1,'ubuntu16\n'),('pollbill','',1500118502,72.97,0,'ubuntu16\n'),('poll','22',1500118802,1.00,1,'ubuntu16\n'),('poll','28',1500118802,3.63,1,'ubuntu16\n'),('pollbill','',1500118801,72.96,0,'ubuntu16\n'),('poll','22',1500119102,1.02,1,'ubuntu16\n'),('poll','28',1500119102,3.58,1,'ubuntu16\n'),('pollbill','',1500119102,72.93,0,'ubuntu16\n'),('poll','22',1500119402,1.01,1,'ubuntu16\n'),('poll','28',1500119402,3.53,1,'ubuntu16\n'),('pollbill','',1500119401,72.96,0,'ubuntu16\n'),('poll','22',1500119702,1.03,1,'ubuntu16\n'),('poll','28',1500119702,3.63,1,'ubuntu16\n'),('pollbill','',1500119702,73.33,0,'ubuntu16\n'),('poll','22',1500120002,1.03,1,'ubuntu16\n'),('poll','28',1500120002,3.54,1,'ubuntu16\n'),('pollbill','',1500120002,73.07,0,'ubuntu16\n'),('poll','22',1500120302,1.04,1,'ubuntu16\n'),('poll','28',1500120302,3.60,1,'ubuntu16\n'),('pollbill','',1500120302,73.34,0,'ubuntu16\n'),('poll','22',1500120602,1.02,1,'ubuntu16\n'),('poll','28',1500120602,3.59,1,'ubuntu16\n'),('pollbill','',1500120601,73.19,0,'ubuntu16\n'),('poll','22',1500120902,1.03,1,'ubuntu16\n'),('poll','28',1500120902,3.63,1,'ubuntu16\n'),('pollbill','',1500120902,73.01,0,'ubuntu16\n'),('poll','22',1500121202,1.03,1,'ubuntu16\n'),('poll','28',1500121202,3.61,1,'ubuntu16\n'),('pollbill','',1500121202,73.09,0,'ubuntu16\n'),('poll','22',1500121502,1.00,1,'ubuntu16\n'),('poll','28',1500121502,3.57,1,'ubuntu16\n'),('pollbill','',1500121502,73.12,0,'ubuntu16\n'),('poll','22',1500121802,1.01,1,'ubuntu16\n'),('poll','28',1500121802,3.61,1,'ubuntu16\n'),('poll','22',1500195904,1.36,1,'ubuntu16\n'),('poll','28',1500195904,10.12,1,'ubuntu16\n'),('pollbill','',1500195903,80.58,0,'ubuntu16\n'),('poll','22',1500196202,1.01,1,'ubuntu16\n'),('poll','28',1500196202,4.12,1,'ubuntu16\n'),('pollbill','',1500196202,73.43,0,'ubuntu16\n'),('poll','22',1500196503,1.05,1,'ubuntu16\n'),('poll','28',1500196503,3.83,1,'ubuntu16\n'),('pollbill','',1500196503,73.27,0,'ubuntu16\n'),('poll','22',1500196802,1.11,1,'ubuntu16\n'),('poll','28',1500196802,3.97,1,'ubuntu16\n'),('pollbill','',1500196802,73.52,0,'ubuntu16\n'),('poll','22',1500197102,1.02,1,'ubuntu16\n'),('poll','28',1500197103,4.15,1,'ubuntu16\n'),('pollbill','',1500197102,73.50,0,'ubuntu16\n'),('poll','22',1500197402,1.03,1,'ubuntu16\n'),('poll','28',1500197402,4.02,1,'ubuntu16\n'),('pollbill','',1500197402,73.54,0,'ubuntu16\n'),('poll','22',1500197702,1.00,1,'ubuntu16\n'),('poll','28',1500197702,1.65,1,'ubuntu16\n'),('pollbill','',1500197702,73.45,0,'ubuntu16\n'),('poll','22',1500198002,1.02,1,'ubuntu16\n'),('poll','28',1500198002,4.01,1,'ubuntu16\n'),('pollbill','',1500198002,73.40,0,'ubuntu16\n'),('poll','22',1500198302,1.01,1,'ubuntu16\n'),('poll','28',1500198302,4.05,1,'ubuntu16\n'),('pollbill','',1500198301,73.53,0,'ubuntu16\n'),('poll','22',1500198602,1.06,1,'ubuntu16\n'),('poll','28',1500198602,4.02,1,'ubuntu16\n'),('pollbill','',1500198602,73.13,0,'ubuntu16\n'),('poll','22',1500198902,1.03,1,'ubuntu16\n'),('poll','28',1500198902,3.98,1,'ubuntu16\n'),('pollbill','',1500198902,73.52,0,'ubuntu16\n'),('poll','22',1500199202,1.05,1,'ubuntu16\n'),('poll','28',1500199202,4.04,1,'ubuntu16\n'),('pollbill','',1500199202,73.51,0,'ubuntu16\n'),('poll','22',1500199502,1.04,1,'ubuntu16\n'),('poll','28',1500199502,4.10,1,'ubuntu16\n'),('pollbill','',1500199501,73.35,0,'ubuntu16\n'),('poll','22',1500199802,1.04,1,'ubuntu16\n'),('poll','28',1500199802,4.08,1,'ubuntu16\n'),('pollbill','',1500199802,73.46,0,'ubuntu16\n'),('poll','22',1500200102,1.02,1,'ubuntu16\n'),('poll','28',1500200102,3.89,1,'ubuntu16\n'),('pollbill','',1500200102,73.33,0,'ubuntu16\n'),('poll','22',1500200402,1.00,1,'ubuntu16\n'),('poll','28',1500200402,3.83,1,'ubuntu16\n'),('pollbill','',1500200402,73.40,0,'ubuntu16\n'),('poll','22',1500200703,1.01,1,'ubuntu16\n'),('poll','28',1500200703,3.90,1,'ubuntu16\n'),('pollbill','',1500200702,73.78,0,'ubuntu16\n'),('poll','22',1500201002,1.02,1,'ubuntu16\n'),('poll','28',1500201002,3.94,1,'ubuntu16\n'),('pollbill','',1500201002,73.50,0,'ubuntu16\n'),('poll','22',1500201302,1.05,1,'ubuntu16\n'),('poll','28',1500201302,4.08,1,'ubuntu16\n'),('pollbill','',1500201302,73.42,0,'ubuntu16\n'),('poll','22',1500201602,1.03,1,'ubuntu16\n'),('poll','28',1500201602,4.00,1,'ubuntu16\n'),('pollbill','',1500201602,73.35,0,'ubuntu16\n'),('poll','22',1500201902,1.05,1,'ubuntu16\n'),('poll','28',1500201902,3.94,1,'ubuntu16\n'),('pollbill','',1500201902,73.37,0,'ubuntu16\n'),('poll','22',1500202202,1.02,1,'ubuntu16\n'),('poll','28',1500202202,3.91,1,'ubuntu16\n'),('pollbill','',1500202202,73.48,0,'ubuntu16\n'),('poll','22',1500202502,1.02,1,'ubuntu16\n'),('poll','28',1500202502,3.92,1,'ubuntu16\n'),('pollbill','',1500202502,73.39,0,'ubuntu16\n'),('poll','22',1500202802,1.03,1,'ubuntu16\n'),('poll','28',1500202802,3.85,1,'ubuntu16\n'),('pollbill','',1500202802,73.70,0,'ubuntu16\n'),('poll','22',1500203102,1.03,1,'ubuntu16\n'),('poll','28',1500203102,4.10,1,'ubuntu16\n'),('pollbill','',1500203102,73.46,0,'ubuntu16\n'),('poll','22',1500203402,1.05,1,'ubuntu16\n'),('poll','28',1500203402,4.05,1,'ubuntu16\n'),('pollbill','',1500203402,73.34,0,'ubuntu16\n'),('poll','22',1500203702,1.03,1,'ubuntu16\n'),('poll','28',1500203702,3.83,1,'ubuntu16\n'),('pollbill','',1500203702,73.50,0,'ubuntu16\n'),('poll','22',1500204002,1.01,1,'ubuntu16\n'),('poll','28',1500204002,3.93,1,'ubuntu16\n'),('pollbill','',1500204001,73.29,0,'ubuntu16\n'),('poll','22',1500204302,1.02,1,'ubuntu16\n'),('poll','28',1500204302,4.11,1,'ubuntu16\n'),('pollbill','',1500204301,73.35,0,'ubuntu16\n'),('poll','22',1500204602,1.04,1,'ubuntu16\n'),('poll','28',1500204602,4.10,1,'ubuntu16\n'),('pollbill','',1500204602,73.39,0,'ubuntu16\n'),('poll','22',1500204902,1.04,1,'ubuntu16\n'),('poll','28',1500204902,4.01,1,'ubuntu16\n'),('pollbill','',1500204902,73.38,0,'ubuntu16\n'),('poll','22',1500205202,1.02,1,'ubuntu16\n'),('poll','28',1500205202,4.04,1,'ubuntu16\n'),('pollbill','',1500205202,73.42,0,'ubuntu16\n'),('poll','22',1500205502,1.04,1,'ubuntu16\n'),('poll','28',1500205502,3.90,1,'ubuntu16\n'),('pollbill','',1500205502,73.53,0,'ubuntu16\n'),('poll','22',1500205802,1.04,1,'ubuntu16\n'),('poll','28',1500205802,3.90,1,'ubuntu16\n'),('pollbill','',1500205801,73.44,0,'ubuntu16\n'),('poll','22',1500206102,1.03,1,'ubuntu16\n'),('poll','28',1500206102,3.91,1,'ubuntu16\n'),('pollbill','',1500206102,73.58,0,'ubuntu16\n'),('poll','22',1500206402,1.07,1,'ubuntu16\n'),('poll','28',1500206402,3.94,1,'ubuntu16\n'),('pollbill','',1500206402,73.26,0,'ubuntu16\n'),('poll','22',1500206702,1.01,1,'ubuntu16\n'),('poll','28',1500206702,3.64,1,'ubuntu16\n'),('pollbill','',1500206702,73.49,0,'ubuntu16\n'),('poll','22',1500207002,1.04,1,'ubuntu16\n'),('poll','28',1500207002,3.64,1,'ubuntu16\n'),('pollbill','',1500207002,73.25,0,'ubuntu16\n'),('poll','22',1500207302,1.02,1,'ubuntu16\n'),('poll','28',1500207302,3.77,1,'ubuntu16\n'),('pollbill','',1500207302,72.80,0,'ubuntu16\n'),('poll','22',1500207605,1.04,1,'ubuntu16\n'),('poll','28',1500207605,3.59,1,'ubuntu16\n'),('pollbill','',1500207605,72.95,0,'ubuntu16\n'),('poll','22',1500207902,1.05,1,'ubuntu16\n'),('poll','28',1500207902,3.69,1,'ubuntu16\n'),('pollbill','',1500207902,73.08,0,'ubuntu16\n'),('poll','22',1500208204,1.07,1,'ubuntu16\n'),('poll','28',1500208204,4.11,1,'ubuntu16\n'),('pollbill','',1500208204,73.37,0,'ubuntu16\n'),('poll','22',1500208502,1.02,1,'ubuntu16\n'),('poll','28',1500208502,3.63,1,'ubuntu16\n'),('pollbill','',1500208502,72.88,0,'ubuntu16\n'),('poll','22',1500208820,1.25,1,'ubuntu16\n'),('poll','28',1500208819,3.98,1,'ubuntu16\n'),('pollbill','',1500208819,73.13,0,'ubuntu16\n'),('poll','22',1500209102,1.00,1,'ubuntu16\n'),('poll','28',1500209102,3.61,1,'ubuntu16\n'),('pollbill','',1500209102,73.25,0,'ubuntu16\n'),('poll','22',1500209402,1.03,1,'ubuntu16\n'),('poll','28',1500209402,3.58,1,'ubuntu16\n'),('pollbill','',1500209402,72.74,0,'ubuntu16\n'),('poll','22',1500209704,1.02,1,'ubuntu16\n'),('poll','28',1500209704,3.64,1,'ubuntu16\n'),('pollbill','',1500209703,73.03,0,'ubuntu16\n'),('poll','22',1500210002,1.00,1,'ubuntu16\n'),('poll','28',1500210002,2.12,1,'ubuntu16\n'),('pollbill','',1500210002,72.92,0,'ubuntu16\n'),('poll','22',1500210302,1.56,1,'ubuntu16\n'),('poll','28',1500210303,3.62,1,'ubuntu16\n'),('pollbill','',1500210303,72.96,0,'ubuntu16\n'),('poll','22',1500210602,1.03,1,'ubuntu16\n'),('poll','28',1500210602,3.63,1,'ubuntu16\n'),('pollbill','',1500210601,72.99,0,'ubuntu16\n'),('poll','22',1500210902,1.01,1,'ubuntu16\n'),('poll','28',1500210902,3.59,1,'ubuntu16\n'),('pollbill','',1500210902,73.03,0,'ubuntu16\n'),('poll','22',1500211203,1.02,1,'ubuntu16\n'),('poll','28',1500211203,3.64,1,'ubuntu16\n'),('pollbill','',1500211202,73.05,0,'ubuntu16\n'),('poll','22',1500211502,1.00,1,'ubuntu16\n'),('poll','28',1500211502,3.69,1,'ubuntu16\n'),('pollbill','',1500211502,73.09,0,'ubuntu16\n'),('poll','22',1500211802,1.01,1,'ubuntu16\n'),('poll','28',1500211802,3.63,1,'ubuntu16\n'),('pollbill','',1500211802,72.76,0,'ubuntu16\n'),('poll','22',1500212103,1.00,1,'ubuntu16\n'),('poll','28',1500212103,3.65,1,'ubuntu16\n'),('pollbill','',1500212102,72.78,0,'ubuntu16\n'),('poll','22',1500212402,0.99,1,'ubuntu16\n'),('poll','28',1500212402,3.68,1,'ubuntu16\n'),('pollbill','',1500212402,72.83,0,'ubuntu16\n'),('poll','22',1500212703,1.23,1,'ubuntu16\n'),('poll','28',1500212703,3.64,1,'ubuntu16\n'),('pollbill','',1500212703,72.94,0,'ubuntu16\n'),('poll','22',1500213003,1.00,1,'ubuntu16\n'),('poll','28',1500213003,3.61,1,'ubuntu16\n'),('pollbill','',1500213002,73.01,0,'ubuntu16\n'),('poll','22',1500213303,1.00,1,'ubuntu16\n'),('poll','28',1500213303,3.66,1,'ubuntu16\n'),('pollbill','',1500213302,73.27,0,'ubuntu16\n'),('poll','22',1500213603,1.03,1,'ubuntu16\n'),('poll','28',1500213603,3.61,1,'ubuntu16\n'),('pollbill','',1500213602,73.04,0,'ubuntu16\n'),('poll','22',1500213902,1.07,1,'ubuntu16\n'),('poll','28',1500213902,3.61,1,'ubuntu16\n'),('pollbill','',1500213902,72.79,0,'ubuntu16\n'),('poll','22',1500214203,1.14,1,'ubuntu16\n'),('poll','28',1500214203,3.74,1,'ubuntu16\n'),('pollbill','',1500214203,73.04,0,'ubuntu16\n'),('poll','22',1500539405,2.22,1,'ubuntu16\n'),('poll','28',1500539405,9.92,1,'ubuntu16\n'),('pollbill','',1500539402,82.73,0,'ubuntu16\n'),('poll','22',1500539702,1.03,1,'ubuntu16\n'),('poll','28',1500539702,1.45,1,'ubuntu16\n'),('pollbill','',1500539702,72.96,0,'ubuntu16\n'),('poll','22',1500540002,1.02,1,'ubuntu16\n'),('poll','28',1500540002,1.54,1,'ubuntu16\n'),('pollbill','',1500540002,73.11,0,'ubuntu16\n'),('poll','22',1500540303,1.06,1,'ubuntu16\n'),('poll','28',1500540302,1.76,1,'ubuntu16\n'),('pollbill','',1500540302,73.19,0,'ubuntu16\n'),('poll','22',1500540602,1.09,1,'ubuntu16\n'),('poll','28',1500540602,3.79,1,'ubuntu16\n'),('pollbill','',1500540602,73.15,0,'ubuntu16\n'),('poll','22',1500540902,1.05,1,'ubuntu16\n'),('poll','28',1500540902,3.15,1,'ubuntu16\n'),('pollbill','',1500540902,73.17,0,'ubuntu16\n'),('poll','22',1500541202,1.10,1,'ubuntu16\n'),('poll','28',1500541202,1.79,1,'ubuntu16\n'),('pollbill','',1500541202,73.04,0,'ubuntu16\n'),('poll','22',1500541503,1.04,1,'ubuntu16\n'),('poll','28',1500541503,3.67,1,'ubuntu16\n'),('pollbill','',1500541502,81.25,0,'ubuntu16\n'),('poll','22',1500541802,2.09,1,'ubuntu16\n'),('poll','28',1500541802,5.80,1,'ubuntu16\n'),('pollbill','',1500541801,78.03,0,'ubuntu16\n'),('poll','22',1500542131,1.77,1,'ubuntu16\n'),('poll','28',1500542131,4.49,1,'ubuntu16\n'),('pollbill','',1500542131,75.29,0,'ubuntu16\n'),('poll','22',1500542402,6.58,1,'ubuntu16\n'),('poll','28',1500542402,7.71,1,'ubuntu16\n'),('pollbill','',1500542402,84.23,0,'ubuntu16\n'),('poll','22',1500542707,1.43,1,'ubuntu16\n'),('poll','28',1500542703,9.88,1,'ubuntu16\n'),('pollbill','',1500542704,79.13,0,'ubuntu16\n'),('poll','22',1500543050,3.88,1,'ubuntu16\n'),('poll','28',1500543051,14.25,1,'ubuntu16\n'),('poll','22',1500615604,2.82,1,'ubuntu16\n'),('poll','28',1500615604,14.28,1,'ubuntu16\n'),('pollbill','',1500615602,87.01,0,'ubuntu16\n'),('poll','22',1500615902,1.01,1,'ubuntu16\n'),('poll','28',1500615902,1.82,1,'ubuntu16\n'),('pollbill','',1500615902,72.88,0,'ubuntu16\n'),('poll','22',1500616202,1.01,1,'ubuntu16\n'),('poll','28',1500616202,1.51,1,'ubuntu16\n'),('pollbill','',1500616202,72.98,0,'ubuntu16\n'),('poll','22',1500616502,1.00,1,'ubuntu16\n'),('poll','28',1500616502,1.40,1,'ubuntu16\n'),('pollbill','',1500616502,72.99,0,'ubuntu16\n'),('poll','22',1500616802,1.00,1,'ubuntu16\n'),('poll','28',1500616802,1.66,1,'ubuntu16\n'),('pollbill','',1500616801,72.97,0,'ubuntu16\n'),('poll','22',1500617102,1.02,1,'ubuntu16\n'),('poll','28',1500617102,1.42,1,'ubuntu16\n'),('pollbill','',1500617102,73.03,0,'ubuntu16\n'),('poll','22',1500617402,1.02,1,'ubuntu16\n'),('poll','28',1500617402,1.55,1,'ubuntu16\n'),('pollbill','',1500617402,72.93,0,'ubuntu16\n'),('poll','22',1500617703,1.09,1,'ubuntu16\n'),('poll','28',1500617703,2.45,1,'ubuntu16\n'),('pollbill','',1500617702,73.03,0,'ubuntu16\n'),('poll','22',1500618002,1.07,1,'ubuntu16\n'),('poll','28',1500618002,3.06,1,'ubuntu16\n'),('pollbill','',1500618002,73.06,0,'ubuntu16\n'),('poll','22',1500618302,0.99,1,'ubuntu16\n'),('poll','28',1500618302,1.52,1,'ubuntu16\n'),('pollbill','',1500618301,72.96,0,'ubuntu16\n'),('poll','22',1500618602,1.00,1,'ubuntu16\n'),('poll','28',1500618602,3.67,1,'ubuntu16\n'),('pollbill','',1500618602,73.08,0,'ubuntu16\n'),('poll','22',1500618902,1.11,1,'ubuntu16\n'),('poll','28',1500618902,2.23,1,'ubuntu16\n'),('pollbill','',1500618902,73.17,0,'ubuntu16\n'),('poll','22',1500619202,1.02,1,'ubuntu16\n'),('poll','28',1500619202,1.49,1,'ubuntu16\n'),('pollbill','',1500619201,72.99,0,'ubuntu16\n'),('poll','22',1500619502,1.07,1,'ubuntu16\n'),('poll','28',1500619502,1.77,1,'ubuntu16\n'),('pollbill','',1500619502,73.00,0,'ubuntu16\n'),('poll','22',1500619802,1.02,1,'ubuntu16\n'),('poll','28',1500619802,1.99,1,'ubuntu16\n'),('pollbill','',1500619802,73.14,0,'ubuntu16\n'),('poll','22',1500620102,1.02,1,'ubuntu16\n'),('poll','28',1500620102,1.77,1,'ubuntu16\n'),('pollbill','',1500620101,72.91,0,'ubuntu16\n'),('poll','22',1500620402,1.09,1,'ubuntu16\n'),('poll','28',1500620402,2.43,1,'ubuntu16\n'),('pollbill','',1500620402,73.03,0,'ubuntu16\n'),('poll','22',1500620702,1.06,1,'ubuntu16\n'),('poll','28',1500620702,1.32,1,'ubuntu16\n'),('pollbill','',1500620702,73.20,0,'ubuntu16\n'),('poll','22',1500621002,1.02,1,'ubuntu16\n'),('poll','28',1500621002,1.36,1,'ubuntu16\n'),('pollbill','',1500621002,73.11,0,'ubuntu16\n'),('poll','22',1500621302,1.03,1,'ubuntu16\n'),('poll','28',1500621302,2.01,1,'ubuntu16\n'),('pollbill','',1500621301,73.15,0,'ubuntu16\n'),('poll','22',1500625204,1.30,1,'ubuntu16\n'),('poll','28',1500625204,8.70,1,'ubuntu16\n'),('pollbill','',1500625202,81.40,0,'ubuntu16\n'),('poll','22',1502099705,1.62,1,'ubuntu16\n'),('poll','28',1502099705,7.56,1,'ubuntu16\n'),('pollbill','',1502099702,81.83,0,'ubuntu16\n'),('poll','22',1502112303,2.28,1,'ubuntu16\n'),('poll','28',1502112303,11.28,1,'ubuntu16\n'),('pollbill','',1502112303,80.93,0,'ubuntu16\n'),('poll','22',1502112602,1.04,1,'ubuntu16\n'),('poll','28',1502112602,4.02,1,'ubuntu16\n'),('pollbill','',1502112602,73.13,0,'ubuntu16\n'),('poll','22',1502112902,1.02,1,'ubuntu16\n'),('poll','28',1502112902,3.82,1,'ubuntu16\n'),('pollbill','',1502112902,73.23,0,'ubuntu16\n'),('poll','22',1502113203,1.09,1,'ubuntu16\n'),('poll','28',1502113203,4.28,1,'ubuntu16\n'),('pollbill','',1502113202,73.70,0,'ubuntu16\n'),('poll','22',1502113502,1.14,1,'ubuntu16\n'),('poll','28',1502113502,4.30,1,'ubuntu16\n'),('pollbill','',1502113502,75.48,0,'ubuntu16\n'),('poll','22',1502113808,1.94,1,'ubuntu16\n'),('poll','28',1502113808,5.29,1,'ubuntu16\n'),('pollbill','',1502113807,74.10,0,'ubuntu16\n'),('poll','22',1502114109,1.07,1,'ubuntu16\n'),('poll','28',1502114109,4.92,1,'ubuntu16\n'),('pollbill','',1502114108,74.47,0,'ubuntu16\n'),('poll','all',1504673998,19.51,6,'ubuntu16\n'),('poll','all',1504674117,6.87,6,'ubuntu16\n'),('pollbill','',1504689305,73.18,0,'ubuntu16\n'),('poll','22',1504689386,1.04,1,'ubuntu16\n'),('poll','28',1504689386,1.46,1,'ubuntu16\n'),('poll','22',1504689603,1.04,1,'ubuntu16\n'),('poll','28',1504689603,1.42,1,'ubuntu16\n'),('pollbill','',1504689603,73.16,0,'ubuntu16\n'),('poll','22',1504689903,1.03,1,'ubuntu16\n'),('poll','28',1504689903,3.75,1,'ubuntu16\n'),('pollbill','',1504689903,73.16,0,'ubuntu16\n'),('poll','22',1504690202,1.06,1,'ubuntu16\n'),('poll','28',1504690202,3.64,1,'ubuntu16\n'),('pollbill','',1504690202,73.22,0,'ubuntu16\n'),('poll','22',1504690507,1.02,1,'ubuntu16\n'),('poll','28',1504690507,3.02,1,'ubuntu16\n'),('pollbill','',1504690507,73.06,0,'ubuntu16\n'),('poll','22',1504690802,1.02,1,'ubuntu16\n'),('poll','28',1504690802,1.97,1,'ubuntu16\n'),('pollbill','',1504690802,73.07,0,'ubuntu16\n'),('poll','22',1504691103,1.10,1,'ubuntu16\n'),('poll','28',1504691103,1.84,1,'ubuntu16\n'),('pollbill','',1504691102,73.44,0,'ubuntu16\n'),('poll','22',1504691405,1.01,1,'ubuntu16\n'),('poll','28',1504691405,2.78,1,'ubuntu16\n'),('pollbill','',1504691405,73.17,0,'ubuntu16\n'),('poll','22',1504691702,1.02,1,'ubuntu16\n'),('poll','28',1504691702,1.41,1,'ubuntu16\n'),('pollbill','',1504691701,73.14,0,'ubuntu16\n'),('poll','22',1504692002,1.07,1,'ubuntu16\n'),('poll','28',1504692002,1.45,1,'ubuntu16\n'),('pollbill','',1504692002,73.13,0,'ubuntu16\n'),('poll','22',1504696502,1.03,1,'ubuntu16\n'),('poll','28',1504696502,1.56,1,'ubuntu16\n'),('pollbill','',1504696502,73.59,0,'ubuntu16\n'),('poll','22',1504696802,1.00,1,'ubuntu16\n'),('poll','28',1504696802,1.46,1,'ubuntu16\n'),('pollbill','',1504696802,73.34,0,'ubuntu16\n'),('poll','22',1504697102,1.06,1,'ubuntu16\n'),('poll','28',1504697102,1.54,1,'ubuntu16\n'),('pollbill','',1504697101,73.57,0,'ubuntu16\n'),('poll','22',1504697402,1.12,1,'ubuntu16\n'),('poll','28',1504697402,1.94,1,'ubuntu16\n'),('pollbill','',1504697402,73.48,0,'ubuntu16\n'),('poll','22',1504697704,1.02,1,'ubuntu16\n'),('poll','28',1504697704,1.53,1,'ubuntu16\n'),('pollbill','',1504697703,73.42,0,'ubuntu16\n'),('poll','22',1504698002,1.08,1,'ubuntu16\n'),('poll','28',1504698002,1.65,1,'ubuntu16\n'),('pollbill','',1504698002,73.67,0,'ubuntu16\n'),('poll','22',1504698302,1.05,1,'ubuntu16\n'),('poll','28',1504698302,1.67,1,'ubuntu16\n'),('pollbill','',1504698302,74.54,0,'ubuntu16\n'),('poll','22',1504698603,1.07,1,'ubuntu16\n'),('poll','28',1504698603,1.67,1,'ubuntu16\n'),('pollbill','',1504698602,73.34,0,'ubuntu16\n'),('poll','22',1504698902,1.01,1,'ubuntu16\n'),('poll','28',1504698902,1.57,1,'ubuntu16\n'),('pollbill','',1504698901,73.56,0,'ubuntu16\n'),('poll','22',1504699202,1.02,1,'ubuntu16\n'),('poll','28',1504699202,1.84,1,'ubuntu16\n'),('pollbill','',1504699202,73.42,0,'ubuntu16\n'),('poll','22',1504699504,1.07,1,'ubuntu16\n'),('poll','28',1504699504,3.16,1,'ubuntu16\n'),('pollbill','',1504699503,73.32,0,'ubuntu16\n'),('poll','22',1504699802,1.01,1,'ubuntu16\n'),('poll','28',1504699802,1.98,1,'ubuntu16\n'),('pollbill','',1504699802,73.37,0,'ubuntu16\n'),('poll','22',1504700102,1.01,1,'ubuntu16\n'),('poll','28',1504700102,2.16,1,'ubuntu16\n'),('pollbill','',1504700102,73.15,0,'ubuntu16\n'),('poll','22',1504700402,1.03,1,'ubuntu16\n'),('poll','28',1504700402,2.53,1,'ubuntu16\n'),('pollbill','',1504700402,73.36,0,'ubuntu16\n'),('poll','22',1504700703,1.02,1,'ubuntu16\n'),('poll','28',1504700703,2.00,1,'ubuntu16\n'),('pollbill','',1504700703,73.33,0,'ubuntu16\n'),('poll','22',1504701002,1.03,1,'ubuntu16\n'),('poll','28',1504701002,2.09,1,'ubuntu16\n'),('pollbill','',1504701002,73.26,0,'ubuntu16\n'),('poll','22',1504701302,1.08,1,'ubuntu16\n'),('poll','28',1504701302,3.78,1,'ubuntu16\n'),('pollbill','',1504701301,73.36,0,'ubuntu16\n'),('poll','22',1504701602,1.00,1,'ubuntu16\n'),('poll','28',1504701602,3.09,1,'ubuntu16\n'),('pollbill','',1504701602,73.47,0,'ubuntu16\n'),('poll','22',1504701902,1.04,1,'ubuntu16\n'),('poll','28',1504701902,1.90,1,'ubuntu16\n'),('pollbill','',1504701902,73.16,0,'ubuntu16\n'),('poll','22',1504702202,1.04,1,'ubuntu16\n'),('poll','28',1504702202,2.15,1,'ubuntu16\n'),('pollbill','',1504702202,73.47,0,'ubuntu16\n'),('poll','22',1504702502,1.05,1,'ubuntu16\n'),('poll','28',1504702502,2.10,1,'ubuntu16\n'),('pollbill','',1504702502,73.42,0,'ubuntu16\n'),('poll','22',1504702802,1.00,1,'ubuntu16\n'),('poll','28',1504702802,2.22,1,'ubuntu16\n'),('pollbill','',1504702802,73.17,0,'ubuntu16\n'),('poll','22',1504703102,0.99,1,'ubuntu16\n'),('poll','28',1504703102,2.08,1,'ubuntu16\n'),('pollbill','',1504703102,73.89,0,'ubuntu16\n'),('poll','22',1504703402,1.00,1,'ubuntu16\n'),('poll','28',1504703402,2.06,1,'ubuntu16\n'),('pollbill','',1504703402,73.27,0,'ubuntu16\n'),('poll','22',1504703702,1.01,1,'ubuntu16\n'),('poll','28',1504703702,2.09,1,'ubuntu16\n'),('pollbill','',1504703702,73.27,0,'ubuntu16\n'),('poll','22',1504704002,0.99,1,'ubuntu16\n'),('poll','28',1504704002,3.54,1,'ubuntu16\n'),('pollbill','',1504704002,73.28,0,'ubuntu16\n'),('poll','22',1504704302,1.00,1,'ubuntu16\n'),('poll','28',1504704302,2.29,1,'ubuntu16\n'),('pollbill','',1504704302,73.11,0,'ubuntu16\n'),('poll','22',1504704602,1.01,1,'ubuntu16\n'),('poll','28',1504704602,2.06,1,'ubuntu16\n'),('pollbill','',1504704601,73.28,0,'ubuntu16\n'),('poll','22',1504704901,1.00,1,'ubuntu16\n'),('poll','28',1504704901,2.61,1,'ubuntu16\n'),('pollbill','',1504704901,73.04,0,'ubuntu16\n'),('poll','22',1504705202,1.02,1,'ubuntu16\n'),('poll','28',1504705202,2.06,1,'ubuntu16\n'),('pollbill','',1504705202,73.25,0,'ubuntu16\n'),('poll','22',1504705502,1.02,1,'ubuntu16\n'),('poll','28',1504705502,1.94,1,'ubuntu16\n'),('pollbill','',1504705502,73.39,0,'ubuntu16\n'),('poll','22',1504705802,1.05,1,'ubuntu16\n'),('poll','28',1504705802,3.68,1,'ubuntu16\n'),('pollbill','',1504705802,73.28,0,'ubuntu16\n'),('poll','22',1504706102,1.12,1,'ubuntu16\n'),('poll','28',1504706102,3.94,1,'ubuntu16\n'),('pollbill','',1504706102,74.17,0,'ubuntu16\n'),('poll','22',1504706403,41337.00,1,'ubuntu16\n'),('poll','28',1504706403,41341.00,1,'ubuntu16\n'),('poll','22',1504747802,1.12,1,'ubuntu16\n'),('poll','28',1504747802,1.80,1,'ubuntu16\n'),('pollbill','',1504706403,41409.00,0,'ubuntu16\n'),('pollbill','',1504747802,73.21,0,'ubuntu16\n'),('poll','22',1504748102,1.04,1,'ubuntu16\n'),('poll','28',1504748102,1.72,1,'ubuntu16\n'),('pollbill','',1504748102,73.21,0,'ubuntu16\n'),('poll','22',1504748402,1.04,1,'ubuntu16\n'),('poll','28',1504748402,1.57,1,'ubuntu16\n'),('pollbill','',1504748402,73.29,0,'ubuntu16\n'),('poll','22',1504748702,1.03,1,'ubuntu16\n'),('poll','28',1504748702,1.31,1,'ubuntu16\n'),('pollbill','',1504748702,73.22,0,'ubuntu16\n'),('poll','22',1504749002,1.05,1,'ubuntu16\n'),('poll','28',1504749002,1.36,1,'ubuntu16\n'),('pollbill','',1504749002,73.14,0,'ubuntu16\n'),('poll','22',1504749302,0.99,1,'ubuntu16\n'),('poll','28',1504749302,1.35,1,'ubuntu16\n'),('pollbill','',1504749302,73.21,0,'ubuntu16\n'),('poll','22',1504749602,1.01,1,'ubuntu16\n'),('poll','28',1504749602,1.43,1,'ubuntu16\n'),('pollbill','',1504749602,73.22,0,'ubuntu16\n'),('poll','22',1504749902,1.04,1,'ubuntu16\n'),('poll','28',1504749902,1.47,1,'ubuntu16\n'),('pollbill','',1504749902,73.31,0,'ubuntu16\n'),('poll','22',1504750202,1.00,1,'ubuntu16\n'),('poll','28',1504750202,1.44,1,'ubuntu16\n'),('pollbill','',1504750201,73.31,0,'ubuntu16\n'),('poll','22',1504750502,1.01,1,'ubuntu16\n'),('poll','28',1504750502,1.37,1,'ubuntu16\n'),('pollbill','',1504750501,73.03,0,'ubuntu16\n'),('poll','22',1504750802,1.00,1,'ubuntu16\n'),('poll','28',1504750802,1.40,1,'ubuntu16\n'),('pollbill','',1504750802,73.04,0,'ubuntu16\n'),('poll','22',1504751102,1.01,1,'ubuntu16\n'),('poll','28',1504751102,1.85,1,'ubuntu16\n'),('pollbill','',1504751102,73.26,0,'ubuntu16\n'),('poll','22',1504751402,1.00,1,'ubuntu16\n'),('poll','28',1504751402,1.33,1,'ubuntu16\n'),('pollbill','',1504751402,73.26,0,'ubuntu16\n'),('poll','22',1504751702,1.01,1,'ubuntu16\n'),('poll','28',1504751702,1.40,1,'ubuntu16\n'),('pollbill','',1504751701,73.44,0,'ubuntu16\n'),('poll','22',1504752002,1.04,1,'ubuntu16\n'),('poll','28',1504752002,1.47,1,'ubuntu16\n'),('pollbill','',1504752001,73.32,0,'ubuntu16\n'),('poll','22',1504752302,1.04,1,'ubuntu16\n'),('poll','28',1504752302,1.77,1,'ubuntu16\n'),('pollbill','',1504752301,73.43,0,'ubuntu16\n'),('poll','22',1504752602,1.02,1,'ubuntu16\n'),('poll','28',1504752602,1.30,1,'ubuntu16\n'),('pollbill','',1504752601,73.46,0,'ubuntu16\n'),('poll','22',1504752902,1.00,1,'ubuntu16\n'),('poll','28',1504752902,1.41,1,'ubuntu16\n'),('pollbill','',1504752901,73.20,0,'ubuntu16\n'),('poll','22',1504753203,1.04,1,'ubuntu16\n'),('poll','28',1504753203,1.49,1,'ubuntu16\n'),('pollbill','',1504753202,73.45,0,'ubuntu16\n'),('poll','22',1504753502,1.03,1,'ubuntu16\n'),('poll','28',1504753502,3.96,1,'ubuntu16\n'),('pollbill','',1504753502,73.56,0,'ubuntu16\n'),('poll','22',1504753802,1.00,1,'ubuntu16\n'),('poll','28',1504753802,3.57,1,'ubuntu16\n'),('pollbill','',1504753802,73.55,0,'ubuntu16\n'),('poll','22',1504754102,1.03,1,'ubuntu16\n'),('poll','28',1504754102,1.64,1,'ubuntu16\n'),('pollbill','',1504754102,73.62,0,'ubuntu16\n'),('poll','22',1504754402,1.01,1,'ubuntu16\n'),('poll','28',1504754402,2.01,1,'ubuntu16\n'),('pollbill','',1504754402,73.65,0,'ubuntu16\n'),('poll','22',1504754702,1.03,1,'ubuntu16\n'),('poll','28',1504754702,2.23,1,'ubuntu16\n'),('pollbill','',1504754701,73.44,0,'ubuntu16\n'),('poll','22',1504755002,1.01,1,'ubuntu16\n'),('poll','28',1504755002,2.33,1,'ubuntu16\n'),('pollbill','',1504755002,73.58,0,'ubuntu16\n'),('poll','22',1504755302,1.08,1,'ubuntu16\n'),('poll','28',1504755302,1.92,1,'ubuntu16\n'),('pollbill','',1504755302,73.57,0,'ubuntu16\n'),('poll','22',1504755602,1.03,1,'ubuntu16\n'),('poll','28',1504755602,1.97,1,'ubuntu16\n'),('pollbill','',1504755602,73.51,0,'ubuntu16\n'),('poll','22',1504755902,1.01,1,'ubuntu16\n'),('poll','28',1504755902,1.90,1,'ubuntu16\n'),('pollbill','',1504755902,73.65,0,'ubuntu16\n'),('poll','22',1504756202,1.08,1,'ubuntu16\n'),('poll','28',1504756202,1.89,1,'ubuntu16\n'),('pollbill','',1504756202,73.64,0,'ubuntu16\n'),('poll','22',1504756502,1.05,1,'ubuntu16\n'),('poll','28',1504756502,2.25,1,'ubuntu16\n'),('pollbill','',1504756502,73.51,0,'ubuntu16\n'),('poll','22',1504756802,1.01,1,'ubuntu16\n'),('poll','28',1504756802,1.87,1,'ubuntu16\n'),('pollbill','',1504756801,73.41,0,'ubuntu16\n'),('poll','22',1504757102,1.02,1,'ubuntu16\n'),('poll','28',1504757102,1.93,1,'ubuntu16\n'),('pollbill','',1504757102,73.52,0,'ubuntu16\n'),('poll','22',1504757402,1.08,1,'ubuntu16\n'),('poll','28',1504757402,1.73,1,'ubuntu16\n'),('pollbill','',1504757402,73.86,0,'ubuntu16\n'),('poll','22',1504757702,1.01,1,'ubuntu16\n'),('poll','28',1504757702,2.53,1,'ubuntu16\n'),('pollbill','',1504757702,73.68,0,'ubuntu16\n'),('poll','22',1504758002,1.07,1,'ubuntu16\n'),('poll','28',1504758002,2.82,1,'ubuntu16\n'),('pollbill','',1504758002,73.72,0,'ubuntu16\n'),('poll','22',1504758302,1.01,1,'ubuntu16\n'),('poll','28',1504758302,4.32,1,'ubuntu16\n'),('pollbill','',1504758302,73.53,0,'ubuntu16\n'),('poll','22',1504758602,1.05,1,'ubuntu16\n'),('poll','28',1504758602,1.94,1,'ubuntu16\n'),('pollbill','',1504758602,73.59,0,'ubuntu16\n'),('poll','22',1504758902,1.02,1,'ubuntu16\n'),('poll','28',1504758902,3.39,1,'ubuntu16\n'),('pollbill','',1504758902,73.42,0,'ubuntu16\n'),('poll','22',1504759202,1.03,1,'ubuntu16\n'),('poll','28',1504759202,2.06,1,'ubuntu16\n'),('pollbill','',1504759202,73.57,0,'ubuntu16\n'),('poll','22',1504759502,1.02,1,'ubuntu16\n'),('poll','28',1504759502,3.89,1,'ubuntu16\n'),('pollbill','',1504759502,73.39,0,'ubuntu16\n'),('poll','22',1504759802,1.02,1,'ubuntu16\n'),('poll','28',1504759802,1.94,1,'ubuntu16\n'),('pollbill','',1504759801,73.47,0,'ubuntu16\n'),('poll','22',1504760102,1.14,1,'ubuntu16\n'),('poll','28',1504760102,1.64,1,'ubuntu16\n'),('pollbill','',1504760102,73.64,0,'ubuntu16\n'),('poll','22',1504760402,1.06,1,'ubuntu16\n'),('poll','28',1504760402,3.05,1,'ubuntu16\n'),('pollbill','',1504760402,73.60,0,'ubuntu16\n'),('poll','22',1504760702,1.04,1,'ubuntu16\n'),('poll','28',1504760702,2.19,1,'ubuntu16\n'),('pollbill','',1504760702,73.47,0,'ubuntu16\n'),('poll','22',1504761002,1.06,1,'ubuntu16\n'),('poll','28',1504761002,3.46,1,'ubuntu16\n'),('pollbill','',1504761002,73.80,0,'ubuntu16\n'),('poll','22',1504761302,1.01,1,'ubuntu16\n'),('poll','28',1504761302,2.56,1,'ubuntu16\n'),('pollbill','',1504761302,73.32,0,'ubuntu16\n'),('poll','22',1504761602,1.03,1,'ubuntu16\n'),('poll','28',1504761602,2.09,1,'ubuntu16\n'),('pollbill','',1504761602,73.63,0,'ubuntu16\n'),('poll','22',1504761902,1.02,1,'ubuntu16\n'),('poll','28',1504761902,3.40,1,'ubuntu16\n'),('pollbill','',1504761901,459.80,0,'ubuntu16\n'),('poll','22',1504762502,1.00,1,'ubuntu16\n'),('poll','28',1504762502,1.39,1,'ubuntu16\n'),('pollbill','',1504762502,73.34,0,'ubuntu16\n'),('poll','22',1504762802,1.04,1,'ubuntu16\n'),('poll','28',1504762802,1.43,1,'ubuntu16\n'),('pollbill','',1504762802,73.29,0,'ubuntu16\n'),('poll','22',1504763102,1.02,1,'ubuntu16\n'),('poll','28',1504763102,1.34,1,'ubuntu16\n'),('pollbill','',1504763102,73.54,0,'ubuntu16\n'),('poll','22',1504763402,1.00,1,'ubuntu16\n'),('poll','28',1504763402,1.38,1,'ubuntu16\n'),('pollbill','',1504763402,73.38,0,'ubuntu16\n'),('poll','22',1504763702,1.03,1,'ubuntu16\n'),('poll','28',1504763702,2.06,1,'ubuntu16\n'),('pollbill','',1504763702,73.33,0,'ubuntu16\n'),('poll','22',1504764002,1.09,1,'ubuntu16\n'),('poll','28',1504764002,1.48,1,'ubuntu16\n'),('pollbill','',1504764002,73.48,0,'ubuntu16\n'),('poll','22',1504764302,1.03,1,'ubuntu16\n'),('poll','28',1504764302,1.83,1,'ubuntu16\n'),('pollbill','',1504764302,73.43,0,'ubuntu16\n'),('poll','22',1504764602,1.00,1,'ubuntu16\n'),('poll','28',1504764602,1.42,1,'ubuntu16\n'),('pollbill','',1504764602,73.24,0,'ubuntu16\n'),('poll','22',1504764902,1.03,1,'ubuntu16\n'),('poll','28',1504764902,1.61,1,'ubuntu16\n'),('pollbill','',1504764902,73.30,0,'ubuntu16\n'),('poll','22',1504765202,1.02,1,'ubuntu16\n'),('poll','28',1504765202,1.47,1,'ubuntu16\n'),('pollbill','',1504765202,73.98,0,'ubuntu16\n'),('poll','22',1504765502,1.02,1,'ubuntu16\n'),('poll','28',1504765502,1.43,1,'ubuntu16\n'),('pollbill','',1504765501,73.31,0,'ubuntu16\n'),('poll','22',1504765802,1.04,1,'ubuntu16\n'),('poll','28',1504765802,1.41,1,'ubuntu16\n'),('pollbill','',1504765801,73.45,0,'ubuntu16\n'),('poll','22',1504766102,1.04,1,'ubuntu16\n'),('poll','28',1504766102,3.02,1,'ubuntu16\n'),('pollbill','',1504766102,73.48,0,'ubuntu16\n'),('poll','22',1504766402,1.02,1,'ubuntu16\n'),('poll','28',1504766402,1.34,1,'ubuntu16\n'),('pollbill','',1504766402,73.21,0,'ubuntu16\n'),('poll','22',1504766702,1.06,1,'ubuntu16\n'),('poll','28',1504766702,1.37,1,'ubuntu16\n'),('pollbill','',1504766702,73.25,0,'ubuntu16\n'),('poll','22',1504767002,1.01,1,'ubuntu16\n'),('poll','28',1504767002,1.51,1,'ubuntu16\n'),('pollbill','',1504767002,73.34,0,'ubuntu16\n'),('poll','22',1504767302,1.01,1,'ubuntu16\n'),('poll','28',1504767302,1.50,1,'ubuntu16\n'),('pollbill','',1504767302,73.19,0,'ubuntu16\n'),('poll','22',1504767602,0.99,1,'ubuntu16\n'),('poll','28',1504767602,1.40,1,'ubuntu16\n'),('pollbill','',1504767601,73.41,0,'ubuntu16\n'),('poll','22',1504767902,1.03,1,'ubuntu16\n'),('poll','28',1504767902,2.87,1,'ubuntu16\n'),('pollbill','',1504767902,73.50,0,'ubuntu16\n'),('poll','22',1504768202,1.02,1,'ubuntu16\n'),('poll','28',1504768202,1.54,1,'ubuntu16\n'),('pollbill','',1504768202,73.53,0,'ubuntu16\n'),('poll','22',1504768502,1.08,1,'ubuntu16\n'),('poll','28',1504768502,3.06,1,'ubuntu16\n'),('pollbill','',1504768502,73.34,0,'ubuntu16\n'),('poll','22',1504768802,1.02,1,'ubuntu16\n'),('poll','28',1504768802,1.48,1,'ubuntu16\n'),('pollbill','',1504768802,73.37,0,'ubuntu16\n'),('poll','22',1504769102,1.05,1,'ubuntu16\n'),('poll','28',1504769102,1.49,1,'ubuntu16\n'),('pollbill','',1504769102,73.27,0,'ubuntu16\n'),('poll','22',1504769402,1.01,1,'ubuntu16\n'),('poll','28',1504769402,3.67,1,'ubuntu16\n'),('pollbill','',1504769402,73.08,0,'ubuntu16\n'),('poll','22',1504769702,1.07,1,'ubuntu16\n'),('poll','28',1504769702,1.66,1,'ubuntu16\n'),('pollbill','',1504769702,72.92,0,'ubuntu16\n'),('poll','22',1504770002,1.02,1,'ubuntu16\n'),('poll','28',1504770002,2.87,1,'ubuntu16\n'),('pollbill','',1504770002,73.02,0,'ubuntu16\n'),('poll','22',1504770302,1.02,1,'ubuntu16\n'),('poll','28',1504770302,1.42,1,'ubuntu16\n'),('pollbill','',1504770301,73.00,0,'ubuntu16\n'),('poll','22',1504770602,1.02,1,'ubuntu16\n'),('poll','28',1504770602,3.07,1,'ubuntu16\n'),('pollbill','',1504770602,72.82,0,'ubuntu16\n'),('poll','22',1504770902,1.00,1,'ubuntu16\n'),('poll','28',1504770902,1.38,1,'ubuntu16\n'),('pollbill','',1504770902,72.85,0,'ubuntu16\n'),('poll','22',1504771202,1.00,1,'ubuntu16\n'),('poll','28',1504771202,1.36,1,'ubuntu16\n'),('pollbill','',1504771202,73.09,0,'ubuntu16\n'),('poll','22',1504771503,1.03,1,'ubuntu16\n'),('poll','28',1504771503,2.96,1,'ubuntu16\n'),('pollbill','',1504771502,73.37,0,'ubuntu16\n'),('poll','22',1504771802,1.05,1,'ubuntu16\n'),('poll','28',1504771802,2.35,1,'ubuntu16\n'),('pollbill','',1504771801,73.54,0,'ubuntu16\n'),('poll','22',1504772102,1.02,1,'ubuntu16\n'),('poll','28',1504772102,2.24,1,'ubuntu16\n'),('pollbill','',1504772102,73.33,0,'ubuntu16\n'),('poll','22',1504772402,1.01,1,'ubuntu16\n'),('poll','28',1504772402,2.73,1,'ubuntu16\n'),('pollbill','',1504772402,73.23,0,'ubuntu16\n'),('poll','22',1504772702,1.00,1,'ubuntu16\n'),('poll','28',1504772702,1.65,1,'ubuntu16\n'),('pollbill','',1504772702,73.14,0,'ubuntu16\n'),('poll','22',1504773002,1.04,1,'ubuntu16\n'),('poll','28',1504773002,1.57,1,'ubuntu16\n'),('pollbill','',1504773001,73.45,0,'ubuntu16\n'),('poll','22',1504773302,1.04,1,'ubuntu16\n'),('poll','28',1504773302,1.55,1,'ubuntu16\n'),('pollbill','',1504773301,73.43,0,'ubuntu16\n'),('poll','22',1504773602,1.02,1,'ubuntu16\n'),('poll','28',1504773602,1.45,1,'ubuntu16\n'),('pollbill','',1504773601,73.44,0,'ubuntu16\n'),('poll','22',1504773902,1.03,1,'ubuntu16\n'),('poll','28',1504773902,1.45,1,'ubuntu16\n'),('pollbill','',1504773902,73.44,0,'ubuntu16\n'),('poll','22',1504774202,1.03,1,'ubuntu16\n'),('poll','28',1504774202,1.98,1,'ubuntu16\n'),('pollbill','',1504774202,73.21,0,'ubuntu16\n'),('poll','22',1504774502,1.03,1,'ubuntu16\n'),('poll','28',1504774502,1.68,1,'ubuntu16\n'),('pollbill','',1504774501,73.46,0,'ubuntu16\n'),('poll','22',1504774802,1.02,1,'ubuntu16\n'),('poll','28',1504774802,1.43,1,'ubuntu16\n'),('pollbill','',1504774801,73.33,0,'ubuntu16\n'),('poll','22',1504775102,1.03,1,'ubuntu16\n'),('poll','28',1504775102,1.43,1,'ubuntu16\n'),('pollbill','',1504775101,73.20,0,'ubuntu16\n'),('poll','22',1504775402,1.01,1,'ubuntu16\n'),('poll','28',1504775402,1.47,1,'ubuntu16\n'),('pollbill','',1504775401,73.23,0,'ubuntu16\n'),('poll','22',1504775702,1.01,1,'ubuntu16\n'),('poll','28',1504775702,1.58,1,'ubuntu16\n'),('pollbill','',1504775702,73.35,0,'ubuntu16\n'),('poll','22',1504776002,1.02,1,'ubuntu16\n'),('poll','28',1504776002,1.32,1,'ubuntu16\n'),('pollbill','',1504776002,73.19,0,'ubuntu16\n'),('poll','22',1504776302,1.04,1,'ubuntu16\n'),('poll','28',1504776302,1.41,1,'ubuntu16\n'),('pollbill','',1504776302,73.18,0,'ubuntu16\n'),('poll','22',1504776602,1.02,1,'ubuntu16\n'),('poll','28',1504776602,2.81,1,'ubuntu16\n'),('pollbill','',1504776602,73.24,0,'ubuntu16\n'),('poll','22',1504776902,1.03,1,'ubuntu16\n'),('poll','28',1504776902,1.57,1,'ubuntu16\n'),('pollbill','',1504776902,73.34,0,'ubuntu16\n'),('poll','22',1504777202,1.06,1,'ubuntu16\n'),('poll','28',1504777202,1.54,1,'ubuntu16\n'),('pollbill','',1504777202,73.34,0,'ubuntu16\n'),('poll','22',1504777502,1.02,1,'ubuntu16\n'),('poll','28',1504777502,1.39,1,'ubuntu16\n'),('pollbill','',1504777502,73.42,0,'ubuntu16\n'),('poll','22',1504777802,1.04,1,'ubuntu16\n'),('poll','28',1504777802,1.60,1,'ubuntu16\n'),('pollbill','',1504777802,73.33,0,'ubuntu16\n'),('poll','22',1504778102,1.00,1,'ubuntu16\n'),('poll','28',1504778102,1.43,1,'ubuntu16\n'),('pollbill','',1504778102,73.29,0,'ubuntu16\n'),('poll','22',1504778402,1.01,1,'ubuntu16\n'),('poll','28',1504778402,1.90,1,'ubuntu16\n'),('pollbill','',1504778402,73.32,0,'ubuntu16\n'),('poll','22',1504784705,1.04,1,'ubuntu16\n'),('poll','28',1504784705,3.84,1,'ubuntu16\n'),('pollbill','',1504784705,68.71,0,'ubuntu16\n'),('poll','22',1504785002,1.01,1,'ubuntu16\n'),('poll','28',1504785002,3.88,1,'ubuntu16\n'),('pollbill','',1504785002,73.24,0,'ubuntu16\n'),('poll','22',1504785302,1.02,1,'ubuntu16\n'),('poll','28',1504785302,1.62,1,'ubuntu16\n'),('pollbill','',1504785302,73.63,0,'ubuntu16\n'),('poll','22',1504785602,1.01,1,'ubuntu16\n'),('poll','28',1504785602,1.81,1,'ubuntu16\n'),('pollbill','',1504785601,73.29,0,'ubuntu16\n'),('poll','22',1504785902,1.04,1,'ubuntu16\n'),('poll','28',1504785902,1.78,1,'ubuntu16\n'),('pollbill','',1504785901,73.13,0,'ubuntu16\n'),('poll','22',1504786202,1.05,1,'ubuntu16\n'),('poll','28',1504786202,1.71,1,'ubuntu16\n'),('pollbill','',1504786201,73.15,0,'ubuntu16\n'),('poll','22',1504786502,1.00,1,'ubuntu16\n'),('poll','28',1504786502,1.76,1,'ubuntu16\n'),('pollbill','',1504786502,73.24,0,'ubuntu16\n'),('poll','22',1504786803,1.03,1,'ubuntu16\n'),('poll','28',1504786803,1.89,1,'ubuntu16\n'),('pollbill','',1504786803,73.19,0,'ubuntu16\n'),('poll','22',1504787102,1.00,1,'ubuntu16\n'),('poll','28',1504787102,1.58,1,'ubuntu16\n'),('pollbill','',1504787102,73.24,0,'ubuntu16\n'),('poll','22',1504787402,1.01,1,'ubuntu16\n'),('poll','28',1504787402,1.89,1,'ubuntu16\n'),('pollbill','',1504787402,73.27,0,'ubuntu16\n'),('poll','22',1504787702,1.01,1,'ubuntu16\n'),('poll','28',1504787702,1.76,1,'ubuntu16\n'),('pollbill','',1504787702,73.24,0,'ubuntu16\n'),('poll','22',1504788002,1.02,1,'ubuntu16\n'),('poll','28',1504788002,1.97,1,'ubuntu16\n'),('pollbill','',1504788002,73.21,0,'ubuntu16\n'),('poll','22',1504788302,1.03,1,'ubuntu16\n'),('poll','28',1504788302,1.79,1,'ubuntu16\n'),('pollbill','',1504788302,73.25,0,'ubuntu16\n'),('poll','22',1504788602,1.00,1,'ubuntu16\n'),('poll','28',1504788602,1.85,1,'ubuntu16\n'),('pollbill','',1504788602,73.18,0,'ubuntu16\n'),('poll','22',1504788902,1.01,1,'ubuntu16\n'),('poll','28',1504788902,1.52,1,'ubuntu16\n'),('pollbill','',1504788902,73.26,0,'ubuntu16\n'),('poll','22',1504789202,1.01,1,'ubuntu16\n'),('poll','28',1504789202,1.67,1,'ubuntu16\n'),('pollbill','',1504789202,73.19,0,'ubuntu16\n'),('poll','22',1504789502,1.04,1,'ubuntu16\n'),('poll','28',1504789502,1.84,1,'ubuntu16\n'),('pollbill','',1504789502,73.40,0,'ubuntu16\n'),('poll','22',1504789802,1.02,1,'ubuntu16\n'),('poll','28',1504789802,1.91,1,'ubuntu16\n'),('pollbill','',1504789802,73.28,0,'ubuntu16\n'),('poll','22',1504790102,1.11,1,'ubuntu16\n'),('poll','28',1504790102,2.23,1,'ubuntu16\n'),('pollbill','',1504790102,73.30,0,'ubuntu16\n'),('poll','22',1504790402,1.01,1,'ubuntu16\n'),('poll','28',1504790402,2.31,1,'ubuntu16\n'),('pollbill','',1504790402,73.34,0,'ubuntu16\n'),('poll','22',1504790702,1.04,1,'ubuntu16\n'),('poll','28',1504790702,3.10,1,'ubuntu16\n'),('pollbill','',1504790701,73.08,0,'ubuntu16\n'),('poll','22',1504791002,1.01,1,'ubuntu16\n'),('poll','28',1504791002,1.81,1,'ubuntu16\n'),('pollbill','',1504791001,73.57,0,'ubuntu16\n'),('poll','22',1504791302,1.60,1,'ubuntu16\n'),('poll','28',1504791302,2.27,1,'ubuntu16\n'),('pollbill','',1504791302,48.91,0,'ubuntu16\n'),('poll','22',1504791602,1.48,1,'ubuntu16\n'),('poll','28',1504791602,2.15,1,'ubuntu16\n'),('pollbill','',1504791602,48.81,0,'ubuntu16\n'),('poll','22',1504791903,1.46,1,'ubuntu16\n'),('poll','28',1504791903,2.00,1,'ubuntu16\n'),('pollbill','',1504791902,49.03,0,'ubuntu16\n'),('poll','22',1504792203,1.78,1,'ubuntu16\n'),('poll','28',1504792203,2.67,1,'ubuntu16\n'),('pollbill','',1504792202,54.90,0,'ubuntu16\n'),('poll','22',1504792503,2.04,1,'ubuntu16\n'),('poll','28',1504792503,4.50,1,'ubuntu16\n'),('pollbill','',1504792502,49.03,0,'ubuntu16\n'),('poll','28',1504835124,18.10,1,'ubuntu16\n'),('poll','22',1504835124,23.19,1,'ubuntu16\n'),('pollbill','',1504835121,58.82,0,'ubuntu16\n'),('poll','22',1504876204,1.80,1,'ubuntu16\n'),('poll','28',1504876204,10.10,1,'ubuntu16\n'),('pollbill','',1504876202,81.21,0,'ubuntu16\n'),('poll','22',1504876502,1.00,1,'ubuntu16\n'),('poll','28',1504876502,2.73,1,'ubuntu16\n'),('pollbill','',1504876502,73.38,0,'ubuntu16\n'),('poll','22',1504876802,1.00,1,'ubuntu16\n'),('poll','28',1504876802,2.76,1,'ubuntu16\n'),('pollbill','',1504876802,73.44,0,'ubuntu16\n'),('poll','22',1504877102,1.07,1,'ubuntu16\n'),('poll','28',1504877102,3.10,1,'ubuntu16\n'),('pollbill','',1504877101,73.30,0,'ubuntu16\n'),('poll','22',1504877402,1.04,1,'ubuntu16\n'),('poll','28',1504877402,3.83,1,'ubuntu16\n'),('pollbill','',1504877402,73.21,0,'ubuntu16\n'),('poll','22',1504877702,1.02,1,'ubuntu16\n'),('poll','28',1504877702,3.88,1,'ubuntu16\n'),('pollbill','',1504877701,73.48,0,'ubuntu16\n'),('poll','22',1504878002,1.02,1,'ubuntu16\n'),('poll','28',1504878002,3.76,1,'ubuntu16\n'),('pollbill','',1504878002,73.49,0,'ubuntu16\n'),('poll','22',1504878302,1.00,1,'ubuntu16\n'),('poll','28',1504878302,3.71,1,'ubuntu16\n'),('pollbill','',1504878302,73.27,0,'ubuntu16\n'),('poll','22',1504878602,1.05,1,'ubuntu16\n'),('poll','28',1504878602,3.77,1,'ubuntu16\n'),('pollbill','',1504878602,73.51,0,'ubuntu16\n'),('poll','22',1504878902,1.01,1,'ubuntu16\n'),('poll','28',1504878902,4.10,1,'ubuntu16\n'),('pollbill','',1504878902,73.52,0,'ubuntu16\n'),('poll','22',1504879202,1.01,1,'ubuntu16\n'),('poll','28',1504879202,3.93,1,'ubuntu16\n'),('pollbill','',1504879201,73.42,0,'ubuntu16\n'),('poll','22',1504882202,1.13,1,'ubuntu16\n'),('poll','28',1504882202,4.12,1,'ubuntu16\n'),('pollbill','',1504882202,73.81,0,'ubuntu16\n'),('poll','22',1504882502,1.05,1,'ubuntu16\n'),('poll','28',1504882502,4.91,1,'ubuntu16\n'),('pollbill','',1504882502,73.36,0,'ubuntu16\n'),('poll','22',1504882802,1.02,1,'ubuntu16\n'),('poll','28',1504882802,4.14,1,'ubuntu16\n'),('pollbill','',1504882802,73.59,0,'ubuntu16\n'),('poll','22',1504883102,1.02,1,'ubuntu16\n'),('poll','28',1504883102,4.29,1,'ubuntu16\n'),('pollbill','',1504883102,73.39,0,'ubuntu16\n'),('poll','22',1504883402,1.04,1,'ubuntu16\n'),('poll','28',1504883402,3.92,1,'ubuntu16\n'),('pollbill','',1504883402,73.43,0,'ubuntu16\n'),('poll','22',1504883702,1.01,1,'ubuntu16\n'),('poll','28',1504883702,3.84,1,'ubuntu16\n'),('pollbill','',1504883701,73.40,0,'ubuntu16\n'),('poll','22',1504884002,1.00,1,'ubuntu16\n'),('poll','28',1504884002,3.88,1,'ubuntu16\n'),('pollbill','',1504884002,73.33,0,'ubuntu16\n'),('poll','22',1504884304,1.02,1,'ubuntu16\n'),('poll','28',1504884303,4.40,1,'ubuntu16\n'),('pollbill','',1504884302,73.91,0,'ubuntu16\n'),('poll','22',1504884602,1.06,1,'ubuntu16\n'),('poll','28',1504884602,3.94,1,'ubuntu16\n'),('pollbill','',1504884601,73.48,0,'ubuntu16\n'),('poll','22',1504884902,1.02,1,'ubuntu16\n'),('poll','28',1504884902,4.08,1,'ubuntu16\n'),('pollbill','',1504884902,73.36,0,'ubuntu16\n'),('poll','22',1504885202,1.02,1,'ubuntu16\n'),('poll','28',1504885202,3.89,1,'ubuntu16\n'),('pollbill','',1504885202,73.49,0,'ubuntu16\n'),('poll','22',1504885503,1.70,1,'ubuntu16\n'),('poll','28',1504885503,4.43,1,'ubuntu16\n'),('pollbill','',1504885503,73.32,0,'ubuntu16\n'),('poll','22',1504885802,1.01,1,'ubuntu16\n'),('poll','28',1504885802,3.96,1,'ubuntu16\n'),('pollbill','',1504885802,73.28,0,'ubuntu16\n'),('poll','22',1504886102,1.05,1,'ubuntu16\n'),('poll','28',1504886102,3.87,1,'ubuntu16\n'),('pollbill','',1504886102,73.74,0,'ubuntu16\n'),('poll','22',1504886402,1.00,1,'ubuntu16\n'),('poll','28',1504886402,3.73,1,'ubuntu16\n'),('pollbill','',1504886401,73.41,0,'ubuntu16\n'),('poll','22',1504886702,1.05,1,'ubuntu16\n'),('poll','28',1504886702,4.02,1,'ubuntu16\n'),('pollbill','',1504886702,7115.00,0,'ubuntu16\n'),('poll','22',1504893908,1.01,1,'ubuntu16\n'),('poll','28',1504893908,3.81,1,'ubuntu16\n'),('pollbill','',1504893908,73.42,0,'ubuntu16\n'),('poll','22',1504911902,1.19,1,'ubuntu16\n'),('poll','28',1504911902,0.31,1,'ubuntu16\n'),('pollbill','',1504911901,70.25,0,'ubuntu16\n'),('poll','22',1504912203,0.98,1,'ubuntu16\n'),('poll','28',1504912203,3.70,1,'ubuntu16\n'),('pollbill','',1504912203,72.97,0,'ubuntu16\n'),('poll','22',1504912511,1.02,1,'ubuntu16\n'),('poll','28',1504912511,1.38,1,'ubuntu16\n'),('pollbill','',1504912511,72.75,0,'ubuntu16\n'),('poll','22',1504912803,1.00,1,'ubuntu16\n'),('poll','28',1504912803,3.69,1,'ubuntu16\n'),('pollbill','',1504912802,73.29,0,'ubuntu16\n'),('poll','22',1504913110,1.01,1,'ubuntu16\n'),('poll','28',1504913110,3.78,1,'ubuntu16\n'),('pollbill','',1504913110,72.94,0,'ubuntu16\n'),('poll','22',1504913406,1.04,1,'ubuntu16\n'),('poll','28',1504913406,3.71,1,'ubuntu16\n'),('pollbill','',1504913405,75.09,0,'ubuntu16\n'),('poll','22',1504913708,1.11,1,'ubuntu16\n'),('poll','28',1504913708,3.65,1,'ubuntu16\n'),('pollbill','',1504913708,73.03,0,'ubuntu16\n'),('poll','22',1504914011,1.00,1,'ubuntu16\n'),('poll','28',1504914011,3.88,1,'ubuntu16\n'),('pollbill','',1504914011,73.34,0,'ubuntu16\n'),('poll','22',1504914304,1.02,1,'ubuntu16\n'),('poll','28',1504914304,3.68,1,'ubuntu16\n'),('pollbill','',1504914304,73.03,0,'ubuntu16\n'),('poll','22',1504914602,1.02,1,'ubuntu16\n'),('poll','28',1504914602,3.84,1,'ubuntu16\n'),('pollbill','',1504914601,73.50,0,'ubuntu16\n'),('poll','22',1504914911,1.01,1,'ubuntu16\n'),('poll','28',1504914911,3.68,1,'ubuntu16\n'),('pollbill','',1504914911,72.99,0,'ubuntu16\n'),('poll','22',1504915202,0.99,1,'ubuntu16\n'),('poll','28',1504915202,3.97,1,'ubuntu16\n'),('pollbill','',1504915201,72.93,0,'ubuntu16\n'),('poll','22',1504915502,1.02,1,'ubuntu16\n'),('poll','28',1504915502,3.73,1,'ubuntu16\n'),('pollbill','',1504915502,73.67,0,'ubuntu16\n'),('poll','22',1504915805,1.00,1,'ubuntu16\n'),('poll','28',1504915805,3.70,1,'ubuntu16\n'),('pollbill','',1504915805,73.16,0,'ubuntu16\n'),('poll','22',1504916102,1.09,1,'ubuntu16\n'),('poll','28',1504916102,3.77,1,'ubuntu16\n'),('pollbill','',1504916102,72.91,0,'ubuntu16\n'),('poll','22',1504916403,1.03,1,'ubuntu16\n'),('poll','28',1504916403,3.67,1,'ubuntu16\n'),('pollbill','',1504916402,73.16,0,'ubuntu16\n'),('poll','22',1504916705,1.02,1,'ubuntu16\n'),('poll','28',1504916705,3.67,1,'ubuntu16\n'),('pollbill','',1504916704,73.22,0,'ubuntu16\n'),('poll','22',1504917002,1.01,1,'ubuntu16\n'),('poll','28',1504917002,3.65,1,'ubuntu16\n'),('pollbill','',1504917002,73.00,0,'ubuntu16\n'),('poll','22',1504917302,1.03,1,'ubuntu16\n'),('poll','28',1504917302,3.70,1,'ubuntu16\n'),('pollbill','',1504917302,73.43,0,'ubuntu16\n'),('poll','22',1504917605,1.04,1,'ubuntu16\n'),('poll','28',1504917605,3.95,1,'ubuntu16\n'),('pollbill','',1504917605,72.92,0,'ubuntu16\n'),('poll','22',1504917902,1.05,1,'ubuntu16\n'),('poll','28',1504917902,3.75,1,'ubuntu16\n'),('pollbill','',1504917902,72.69,0,'ubuntu16\n'),('poll','22',1504918202,1.01,1,'ubuntu16\n'),('poll','28',1504918202,3.80,1,'ubuntu16\n'),('pollbill','',1504918201,72.82,0,'ubuntu16\n'),('poll','22',1504918505,1.00,1,'ubuntu16\n'),('poll','28',1504918505,3.73,1,'ubuntu16\n'),('pollbill','',1504918505,72.86,0,'ubuntu16\n'),('poll','22',1504918802,1.04,1,'ubuntu16\n'),('poll','28',1504918802,3.66,1,'ubuntu16\n'),('pollbill','',1504918802,73.17,0,'ubuntu16\n'),('poll','22',1504919102,1.04,1,'ubuntu16\n'),('poll','28',1504919102,3.73,1,'ubuntu16\n'),('pollbill','',1504919102,73.23,0,'ubuntu16\n'),('poll','22',1504919403,1.02,1,'ubuntu16\n'),('poll','28',1504919403,3.65,1,'ubuntu16\n'),('pollbill','',1504919403,73.11,0,'ubuntu16\n'),('poll','22',1504919702,1.00,1,'ubuntu16\n'),('poll','28',1504919702,3.66,1,'ubuntu16\n'),('pollbill','',1504919702,73.12,0,'ubuntu16\n'),('poll','22',1504920007,0.99,1,'ubuntu16\n'),('poll','28',1504920006,4.00,1,'ubuntu16\n'),('pollbill','',1504920005,73.54,0,'ubuntu16\n'),('poll','22',1505030103,2.66,1,'ubuntu16\n'),('poll','28',1505030103,10.77,1,'ubuntu16\n'),('pollbill','',1505030102,81.34,0,'ubuntu16\n'),('poll','22',1505030402,1.01,1,'ubuntu16\n'),('poll','28',1505030402,3.65,1,'ubuntu16\n'),('pollbill','',1505030402,72.93,0,'ubuntu16\n'),('poll','22',1505030702,1.01,1,'ubuntu16\n'),('poll','28',1505030702,3.61,1,'ubuntu16\n'),('pollbill','',1505030701,72.87,0,'ubuntu16\n'),('poll','22',1505031002,1.03,1,'ubuntu16\n'),('poll','28',1505031002,3.75,1,'ubuntu16\n'),('pollbill','',1505031002,73.10,0,'ubuntu16\n'),('poll','22',1505031302,0.99,1,'ubuntu16\n'),('poll','28',1505031302,3.58,1,'ubuntu16\n'),('pollbill','',1505031302,73.14,0,'ubuntu16\n'),('poll','22',1505031602,1.03,1,'ubuntu16\n'),('poll','28',1505031602,3.72,1,'ubuntu16\n'),('pollbill','',1505031602,73.12,0,'ubuntu16\n'),('poll','22',1505031902,1.01,1,'ubuntu16\n'),('poll','28',1505031902,3.91,1,'ubuntu16\n'),('pollbill','',1505031901,56.81,0,'ubuntu16\n'),('pollbill','',1505032202,0.79,0,'ubuntu16\n'),('poll','22',1505032202,1.30,1,'ubuntu16\n'),('poll','28',1505032202,3.73,1,'ubuntu16\n'),('pollbill','',1505032224,8.67,0,'ubuntu16-02\n'),('poll','25',1505032246,2.31,1,'ubuntu16-02\n'),('poll','24',1505032246,2.31,1,'ubuntu16-02\n'),('poll','23',1505032246,3.09,1,'ubuntu16-02\n'),('poll','27',1505032246,5.21,1,'ubuntu16-02\n'),('pollbill','',1505032502,0.48,0,'ubuntu16\n'),('poll','22',1505032502,1.25,1,'ubuntu16\n'),('poll','28',1505032502,3.67,1,'ubuntu16\n'),('pollbill','',1505032522,0.63,0,'ubuntu16-02\n'),('poll','25',1505032542,1.24,1,'ubuntu16-02\n'),('poll','24',1505032542,1.24,1,'ubuntu16-02\n'),('poll','23',1505032542,1.45,1,'ubuntu16-02\n'),('poll','27',1505032542,3.92,1,'ubuntu16-02\n'),('pollbill','',1505032802,0.36,0,'ubuntu16\n'),('poll','22',1505032802,1.08,1,'ubuntu16\n'),('poll','28',1505032802,3.60,1,'ubuntu16\n'),('pollbill','',1505032822,0.53,0,'ubuntu16-02\n'),('poll','25',1505032842,1.16,1,'ubuntu16-02\n'),('poll','24',1505032842,1.32,1,'ubuntu16-02\n'),('poll','23',1505032842,1.53,1,'ubuntu16-02\n'),('poll','27',1505032842,3.94,1,'ubuntu16-02\n'),('pollbill','',1505033102,0.38,0,'ubuntu16\n'),('poll','22',1505033102,1.04,1,'ubuntu16\n'),('poll','28',1505033102,3.64,1,'ubuntu16\n'),('pollbill','',1505033121,0.37,0,'ubuntu16-02\n'),('pollbill','',1505033402,0.76,0,'ubuntu16\n'),('poll','22',1505033402,1.50,1,'ubuntu16\n'),('poll','28',1505033402,4.00,1,'ubuntu16\n'),('pollbill','',1505033422,0.79,0,'ubuntu16-02\n'),('poll','25',1505033442,1.17,1,'ubuntu16-02\n'),('poll','24',1505033442,1.19,1,'ubuntu16-02\n'),('poll','23',1505033442,1.64,1,'ubuntu16-02\n'),('poll','27',1505033442,3.85,1,'ubuntu16-02\n'),('pollbill','',1505033702,0.56,0,'ubuntu16\n'),('poll','22',1505033702,1.47,1,'ubuntu16\n'),('poll','28',1505033702,3.88,1,'ubuntu16\n'),('pollbill','',1505033722,0.67,0,'ubuntu16-02\n'),('poll','25',1505033743,1.20,1,'ubuntu16-02\n'),('poll','24',1505033743,1.20,1,'ubuntu16-02\n'),('poll','23',1505033743,1.54,1,'ubuntu16-02\n'),('poll','27',1505033743,3.86,1,'ubuntu16-02\n'),('pollbill','',1505034002,0.45,0,'ubuntu16\n'),('poll','22',1505034002,1.25,1,'ubuntu16\n'),('poll','28',1505034002,3.72,1,'ubuntu16\n'),('pollbill','',1505034022,0.58,0,'ubuntu16-02\n'),('poll','25',1505034042,1.17,1,'ubuntu16-02\n'),('poll','24',1505034042,1.25,1,'ubuntu16-02\n'),('poll','23',1505034042,1.41,1,'ubuntu16-02\n'),('poll','27',1505034042,3.95,1,'ubuntu16-02\n'),('pollbill','',1505034302,0.45,0,'ubuntu16\n'),('poll','22',1505034302,1.30,1,'ubuntu16\n'),('poll','28',1505034302,3.77,1,'ubuntu16\n'),('pollbill','',1505034322,0.63,0,'ubuntu16-02\n'),('poll','24',1505034343,1.06,1,'ubuntu16-02\n'),('poll','25',1505034343,1.15,1,'ubuntu16-02\n'),('poll','23',1505034343,1.52,1,'ubuntu16-02\n'),('poll','27',1505034343,3.98,1,'ubuntu16-02\n'),('pollbill','',1505034602,0.43,0,'ubuntu16\n'),('poll','22',1505034602,1.24,1,'ubuntu16\n'),('poll','28',1505034602,3.80,1,'ubuntu16\n'),('pollbill','',1505034622,0.58,0,'ubuntu16-02\n'),('poll','25',1505034642,1.17,1,'ubuntu16-02\n'),('poll','24',1505034642,1.19,1,'ubuntu16-02\n'),('poll','23',1505034642,1.42,1,'ubuntu16-02\n'),('poll','27',1505034642,3.97,1,'ubuntu16-02\n'),('pollbill','',1505034902,0.39,0,'ubuntu16\n'),('poll','22',1505034902,1.19,1,'ubuntu16\n'),('poll','28',1505034902,3.65,1,'ubuntu16\n'),('pollbill','',1505034921,0.67,0,'ubuntu16-02\n'),('poll','24',1505034942,1.16,1,'ubuntu16-02\n'),('poll','25',1505034942,1.16,1,'ubuntu16-02\n'),('poll','23',1505034942,1.49,1,'ubuntu16-02\n'),('poll','27',1505034942,3.91,1,'ubuntu16-02\n'),('pollbill','',1505035202,0.54,0,'ubuntu16\n'),('poll','22',1505035202,1.38,1,'ubuntu16\n'),('poll','28',1505035202,3.91,1,'ubuntu16\n'),('pollbill','',1505035222,0.68,0,'ubuntu16-02\n'),('poll','25',1505035243,1.24,1,'ubuntu16-02\n'),('poll','24',1505035243,1.31,1,'ubuntu16-02\n'),('poll','23',1505035243,1.65,1,'ubuntu16-02\n'),('poll','27',1505035243,4.13,1,'ubuntu16-02\n'),('pollbill','',1505035501,0.65,0,'ubuntu16\n'),('poll','22',1505035502,1.38,1,'ubuntu16\n'),('poll','28',1505035502,3.90,1,'ubuntu16\n'),('pollbill','',1505035522,0.86,0,'ubuntu16-02\n'),('poll','25',1505035543,1.21,1,'ubuntu16-02\n'),('poll','24',1505035543,1.22,1,'ubuntu16-02\n'),('poll','23',1505035543,1.59,1,'ubuntu16-02\n'),('poll','27',1505035543,4.11,1,'ubuntu16-02\n'),('pollbill','',1505035802,0.44,0,'ubuntu16\n'),('poll','22',1505035802,1.30,1,'ubuntu16\n'),('poll','28',1505035802,4.17,1,'ubuntu16\n'),('pollbill','',1505035822,0.70,0,'ubuntu16-02\n'),('poll','25',1505035842,1.27,1,'ubuntu16-02\n'),('poll','24',1505035842,1.27,1,'ubuntu16-02\n'),('poll','23',1505035842,2.44,1,'ubuntu16-02\n'),('poll','27',1505035842,4.71,1,'ubuntu16-02\n'),('pollbill','',1505036102,0.56,0,'ubuntu16\n'),('poll','22',1505036102,1.24,1,'ubuntu16\n'),('poll','28',1505036102,4.14,1,'ubuntu16\n'),('pollbill','',1505036122,0.66,0,'ubuntu16-02\n'),('poll','25',1505036142,1.24,1,'ubuntu16-02\n'),('poll','24',1505036142,1.25,1,'ubuntu16-02\n'),('poll','23',1505036142,1.73,1,'ubuntu16-02\n'),('poll','27',1505036142,4.19,1,'ubuntu16-02\n'),('pollbill','',1505036402,0.54,0,'ubuntu16\n'),('poll','22',1505036402,1.35,1,'ubuntu16\n'),('poll','28',1505036402,4.04,1,'ubuntu16\n'),('pollbill','',1505036422,0.86,0,'ubuntu16-02\n'),('poll','24',1505036442,1.20,1,'ubuntu16-02\n'),('poll','25',1505036442,1.23,1,'ubuntu16-02\n'),('poll','23',1505036443,1.60,1,'ubuntu16-02\n'),('poll','27',1505036442,4.08,1,'ubuntu16-02\n'),('pollbill','',1505036702,0.39,0,'ubuntu16\n'),('poll','22',1505036702,1.56,1,'ubuntu16\n'),('poll','28',1505036702,4.08,1,'ubuntu16\n'),('pollbill','',1505036721,0.61,0,'ubuntu16-02\n'),('poll','24',1505036742,1.28,1,'ubuntu16-02\n'),('poll','25',1505036742,1.29,1,'ubuntu16-02\n'),('poll','23',1505036742,1.63,1,'ubuntu16-02\n'),('poll','27',1505036742,4.09,1,'ubuntu16-02\n'),('pollbill','',1505037002,0.52,0,'ubuntu16\n'),('poll','22',1505037003,1.47,1,'ubuntu16\n'),('poll','28',1505037003,4.15,1,'ubuntu16\n'),('pollbill','',1505037022,0.72,0,'ubuntu16-02\n'),('poll','25',1505037043,1.28,1,'ubuntu16-02\n'),('poll','24',1505037043,1.28,1,'ubuntu16-02\n'),('poll','23',1505037043,1.61,1,'ubuntu16-02\n'),('poll','27',1505037043,4.19,1,'ubuntu16-02\n'),('pollbill','',1505037302,0.54,0,'ubuntu16\n'),('pollbill','',1505037322,1.21,0,'ubuntu16-02\n'),('poll','24',1505037343,1.21,1,'ubuntu16-02\n'),('poll','25',1505037343,1.19,1,'ubuntu16-02\n'),('poll','23',1505037343,1.52,1,'ubuntu16-02\n'),('poll','27',1505037343,4.14,1,'ubuntu16-02\n'),('pollbill','',1505037602,0.40,0,'ubuntu16\n'),('poll','22',1505037602,1.31,1,'ubuntu16\n'),('poll','28',1505037602,4.07,1,'ubuntu16\n'),('pollbill','',1505037622,0.90,0,'ubuntu16-02\n'),('poll','24',1505037643,1.18,1,'ubuntu16-02\n'),('poll','25',1505037643,1.25,1,'ubuntu16-02\n'),('poll','23',1505037643,1.92,1,'ubuntu16-02\n'),('poll','27',1505037643,4.43,1,'ubuntu16-02\n'),('pollbill','',1505037902,0.81,0,'ubuntu16\n'),('poll','22',1505037902,1.38,1,'ubuntu16\n'),('poll','28',1505037902,3.95,1,'ubuntu16\n'),('pollbill','',1505037922,0.69,0,'ubuntu16-02\n'),('poll','24',1505037943,1.25,1,'ubuntu16-02\n'),('poll','25',1505037943,1.27,1,'ubuntu16-02\n'),('poll','23',1505037943,1.61,1,'ubuntu16-02\n'),('poll','27',1505037943,4.25,1,'ubuntu16-02\n'),('pollbill','',1505038202,0.43,0,'ubuntu16\n'),('poll','22',1505038202,1.23,1,'ubuntu16\n'),('poll','28',1505038202,4.00,1,'ubuntu16\n'),('pollbill','',1505038222,0.89,0,'ubuntu16-02\n'),('poll','25',1505038243,1.19,1,'ubuntu16-02\n'),('poll','24',1505038243,1.18,1,'ubuntu16-02\n'),('poll','23',1505038243,1.53,1,'ubuntu16-02\n'),('poll','27',1505038243,4.15,1,'ubuntu16-02\n'),('pollbill','',1505038501,0.65,0,'ubuntu16\n'),('poll','22',1505038502,1.45,1,'ubuntu16\n'),('poll','28',1505038502,3.96,1,'ubuntu16\n'),('pollbill','',1505038522,0.79,0,'ubuntu16-02\n'),('poll','24',1505038542,1.25,1,'ubuntu16-02\n'),('poll','25',1505038543,1.22,1,'ubuntu16-02\n'),('poll','23',1505038543,1.65,1,'ubuntu16-02\n'),('poll','27',1505038542,4.32,1,'ubuntu16-02\n'),('pollbill','',1505038802,0.50,0,'ubuntu16\n'),('poll','22',1505038802,1.41,1,'ubuntu16\n'),('poll','28',1505038802,3.96,1,'ubuntu16\n'),('pollbill','',1505038822,0.72,0,'ubuntu16-02\n'),('poll','24',1505038842,1.27,1,'ubuntu16-02\n'),('poll','25',1505038842,1.25,1,'ubuntu16-02\n'),('poll','23',1505038842,1.97,1,'ubuntu16-02\n'),('poll','27',1505038842,4.46,1,'ubuntu16-02\n'),('pollbill','',1505039102,0.58,0,'ubuntu16\n'),('poll','22',1505039103,1.26,1,'ubuntu16\n'),('poll','28',1505039103,4.22,1,'ubuntu16\n'),('pollbill','',1505039122,0.80,0,'ubuntu16-02\n'),('poll','24',1505039142,1.22,1,'ubuntu16-02\n'),('poll','25',1505039142,1.22,1,'ubuntu16-02\n'),('poll','23',1505039142,2.15,1,'ubuntu16-02\n'),('poll','27',1505039142,4.43,1,'ubuntu16-02\n'),('pollbill','',1505039402,0.57,0,'ubuntu16\n'),('pollbill','',1505039422,0.73,0,'ubuntu16-02\n'),('poll','24',1505039443,1.35,1,'ubuntu16-02\n'),('poll','25',1505039443,1.36,1,'ubuntu16-02\n'),('poll','23',1505039443,1.77,1,'ubuntu16-02\n'),('poll','27',1505039443,4.40,1,'ubuntu16-02\n'),('pollbill','',1505039702,0.51,0,'ubuntu16\n'),('poll','22',1505039702,1.36,1,'ubuntu16\n'),('poll','28',1505039702,3.86,1,'ubuntu16\n'),('pollbill','',1505039722,0.93,0,'ubuntu16-02\n'),('poll','25',1505039743,1.43,1,'ubuntu16-02\n'),('poll','24',1505039743,1.41,1,'ubuntu16-02\n'),('poll','23',1505039743,2.37,1,'ubuntu16-02\n'),('poll','27',1505039742,4.84,1,'ubuntu16-02\n'),('pollbill','',1505040002,0.78,0,'ubuntu16\n'),('poll','22',1505040002,1.55,1,'ubuntu16\n'),('poll','28',1505040002,3.96,1,'ubuntu16\n'),('pollbill','',1505040022,0.91,0,'ubuntu16-02\n'),('poll','25',1505040043,1.52,1,'ubuntu16-02\n'),('poll','24',1505040043,1.54,1,'ubuntu16-02\n'),('poll','23',1505040043,2.02,1,'ubuntu16-02\n'),('poll','27',1505040043,4.73,1,'ubuntu16-02\n'),('pollbill','',1505040302,0.47,0,'ubuntu16\n'),('poll','22',1505040302,1.41,1,'ubuntu16\n'),('poll','28',1505040302,4.10,1,'ubuntu16\n'),('pollbill','',1505040322,0.61,0,'ubuntu16-02\n'),('poll','25',1505040342,1.21,1,'ubuntu16-02\n'),('poll','24',1505040342,1.24,1,'ubuntu16-02\n'),('poll','23',1505040342,1.83,1,'ubuntu16-02\n'),('poll','27',1505040342,4.50,1,'ubuntu16-02\n'),('pollbill','',1505040601,0.57,0,'ubuntu16\n'),('pollbill','',1505040621,0.41,0,'ubuntu16-02\n'),('pollbill','',1505040902,0.47,0,'ubuntu16\n'),('poll','22',1505040903,1.32,1,'ubuntu16\n'),('poll','28',1505040903,3.94,1,'ubuntu16\n'),('pollbill','',1505040922,0.80,0,'ubuntu16-02\n'),('poll','25',1505040943,1.25,1,'ubuntu16-02\n'),('poll','24',1505040943,1.24,1,'ubuntu16-02\n'),('poll','23',1505040943,1.68,1,'ubuntu16-02\n'),('poll','27',1505040943,4.59,1,'ubuntu16-02\n'),('pollbill','',1505041202,0.63,0,'ubuntu16\n'),('poll','22',1505041202,1.28,1,'ubuntu16\n'),('poll','28',1505041202,4.09,1,'ubuntu16\n'),('pollbill','',1505041222,0.85,0,'ubuntu16-02\n'),('poll','24',1505041243,1.51,1,'ubuntu16-02\n'),('poll','25',1505041243,1.55,1,'ubuntu16-02\n'),('poll','23',1505041243,1.86,1,'ubuntu16-02\n'),('poll','27',1505041243,4.70,1,'ubuntu16-02\n'),('pollbill','',1505041502,0.40,0,'ubuntu16\n'),('poll','22',1505041503,1.31,1,'ubuntu16\n'),('poll','28',1505041503,4.09,1,'ubuntu16\n'),('pollbill','',1505041522,0.71,0,'ubuntu16-02\n'),('poll','25',1505041543,1.36,1,'ubuntu16-02\n'),('poll','24',1505041543,1.37,1,'ubuntu16-02\n'),('poll','23',1505041543,2.01,1,'ubuntu16-02\n'),('poll','27',1505041543,4.55,1,'ubuntu16-02\n'),('pollbill','',1505041802,0.46,0,'ubuntu16\n'),('pollbill','',1505041822,1.19,0,'ubuntu16-02\n'),('poll','25',1505041843,1.36,1,'ubuntu16-02\n'),('poll','24',1505041843,1.36,1,'ubuntu16-02\n'),('poll','23',1505041843,1.98,1,'ubuntu16-02\n'),('poll','27',1505041843,4.54,1,'ubuntu16-02\n'),('pollbill','',1505042102,0.87,0,'ubuntu16\n'),('poll','22',1505042102,1.31,1,'ubuntu16\n'),('poll','28',1505042102,3.88,1,'ubuntu16\n'),('pollbill','',1505042122,0.39,0,'ubuntu16-02\n'),('pollbill','',1505042403,0.50,0,'ubuntu16\n'),('poll','22',1505042403,1.24,1,'ubuntu16\n'),('poll','28',1505042403,3.86,1,'ubuntu16\n'),('pollbill','',1505042421,0.68,0,'ubuntu16-02\n'),('poll','25',1505042442,1.28,1,'ubuntu16-02\n'),('poll','24',1505042442,1.28,1,'ubuntu16-02\n'),('poll','23',1505042442,1.89,1,'ubuntu16-02\n'),('poll','27',1505042442,4.22,1,'ubuntu16-02\n'),('pollbill','',1505042702,0.41,0,'ubuntu16\n'),('poll','22',1505042703,1.38,1,'ubuntu16\n'),('poll','28',1505042703,4.21,1,'ubuntu16\n'),('pollbill','',1505042721,0.74,0,'ubuntu16-02\n'),('poll','24',1505042742,1.44,1,'ubuntu16-02\n'),('poll','25',1505042742,1.42,1,'ubuntu16-02\n'),('poll','23',1505042742,2.04,1,'ubuntu16-02\n'),('poll','27',1505042742,4.56,1,'ubuntu16-02\n'),('pollbill','',1505043002,0.66,0,'ubuntu16\n'),('pollbill','',1505043022,0.89,0,'ubuntu16-02\n'),('poll','24',1505043043,1.28,1,'ubuntu16-02\n'),('poll','25',1505043043,1.27,1,'ubuntu16-02\n'),('poll','23',1505043043,2.05,1,'ubuntu16-02\n'),('poll','27',1505043043,4.42,1,'ubuntu16-02\n'),('pollbill','',1505043302,0.75,0,'ubuntu16\n'),('poll','22',1505043302,1.30,1,'ubuntu16\n'),('poll','28',1505043302,4.17,1,'ubuntu16\n'),('pollbill','',1505043322,0.87,0,'ubuntu16-02\n'),('poll','24',1505043343,1.41,1,'ubuntu16-02\n'),('poll','25',1505043343,1.43,1,'ubuntu16-02\n'),('poll','23',1505043343,2.19,1,'ubuntu16-02\n'),('poll','27',1505043343,5.20,1,'ubuntu16-02\n'),('pollbill','',1505043602,0.53,0,'ubuntu16\n'),('poll','22',1505043603,1.37,1,'ubuntu16\n'),('poll','28',1505043603,4.16,1,'ubuntu16\n'),('pollbill','',1505043622,0.80,0,'ubuntu16-02\n'),('poll','24',1505043643,1.43,1,'ubuntu16-02\n'),('poll','25',1505043643,1.45,1,'ubuntu16-02\n'),('poll','23',1505043643,2.36,1,'ubuntu16-02\n'),('poll','27',1505043643,4.92,1,'ubuntu16-02\n'),('pollbill','',1505043903,0.42,0,'ubuntu16\n'),('poll','22',1505043903,1.27,1,'ubuntu16\n'),('poll','28',1505043903,3.87,1,'ubuntu16\n'),('pollbill','',1505043922,0.60,0,'ubuntu16-02\n'),('poll','25',1505043943,1.47,1,'ubuntu16-02\n'),('poll','24',1505043943,1.48,1,'ubuntu16-02\n'),('poll','23',1505043943,1.86,1,'ubuntu16-02\n'),('poll','27',1505043943,4.64,1,'ubuntu16-02\n'),('pollbill','',1505044202,0.46,0,'ubuntu16\n'),('pollbill','',1505044222,0.48,0,'ubuntu16-02\n'),('pollbill','',1505044502,0.47,0,'ubuntu16\n'),('poll','22',1505044502,1.30,1,'ubuntu16\n'),('poll','28',1505044502,4.24,1,'ubuntu16\n'),('pollbill','',1505044522,0.76,0,'ubuntu16-02\n'),('poll','24',1505044542,1.41,1,'ubuntu16-02\n'),('poll','25',1505044542,1.40,1,'ubuntu16-02\n'),('poll','23',1505044542,2.36,1,'ubuntu16-02\n'),('poll','27',1505044542,4.86,1,'ubuntu16-02\n'),('pollbill','',1505044804,0.58,0,'ubuntu16\n'),('poll','22',1505044804,1.40,1,'ubuntu16\n'),('poll','28',1505044804,4.41,1,'ubuntu16\n'),('pollbill','',1505044822,0.97,0,'ubuntu16-02\n'),('poll','24',1505044844,1.30,1,'ubuntu16-02\n'),('poll','25',1505044844,1.29,1,'ubuntu16-02\n'),('poll','23',1505044844,1.83,1,'ubuntu16-02\n'),('poll','27',1505044844,4.54,1,'ubuntu16-02\n'),('pollbill','',1505045102,0.36,0,'ubuntu16\n'),('pollbill','',1505045121,0.48,0,'ubuntu16-02\n'),('pollbill','',1505045403,0.50,0,'ubuntu16\n'),('poll','22',1505045403,1.36,1,'ubuntu16\n'),('poll','28',1505045403,3.79,1,'ubuntu16\n'),('pollbill','',1505045422,0.68,0,'ubuntu16-02\n'),('poll','25',1505045443,1.25,1,'ubuntu16-02\n'),('poll','24',1505045443,1.26,1,'ubuntu16-02\n'),('poll','23',1505045443,2.31,1,'ubuntu16-02\n'),('poll','27',1505045443,4.54,1,'ubuntu16-02\n'),('pollbill','',1505045702,1.40,0,'ubuntu16\n'),('pollbill','',1505045722,0.77,0,'ubuntu16-02\n'),('poll','24',1505045743,1.37,1,'ubuntu16-02\n'),('poll','25',1505045743,1.37,1,'ubuntu16-02\n'),('poll','23',1505045743,1.79,1,'ubuntu16-02\n'),('poll','27',1505045743,4.52,1,'ubuntu16-02\n'),('pollbill','',1505046002,0.46,0,'ubuntu16\n'),('poll','22',1505046002,1.30,1,'ubuntu16\n'),('poll','28',1505046002,4.05,1,'ubuntu16\n'),('pollbill','',1505046022,0.76,0,'ubuntu16-02\n'),('poll','25',1505046043,1.73,1,'ubuntu16-02\n'),('poll','24',1505046043,1.73,1,'ubuntu16-02\n'),('poll','23',1505046043,1.86,1,'ubuntu16-02\n'),('poll','27',1505046043,4.22,1,'ubuntu16-02\n'),('pollbill','',1505050201,0.61,0,'ubuntu16-02\n'),('pollbill','',1505050202,0.55,0,'ubuntu16\n'),('poll','22',1505050202,1.53,1,'ubuntu16\n'),('poll','28',1505050202,4.01,1,'ubuntu16\n'),('poll','24',1505050502,1.64,1,'ubuntu16-02\n'),('poll','25',1505050502,1.52,1,'ubuntu16-02\n'),('pollbill','',1505050501,2.74,0,'ubuntu16-02\n'),('poll','23',1505050502,2.57,1,'ubuntu16-02\n'),('pollbill','',1505050504,0.38,0,'ubuntu16\n'),('poll','22',1505050505,1.21,1,'ubuntu16\n'),('poll','27',1505050502,4.64,1,'ubuntu16-02\n'),('poll','28',1505050505,3.75,1,'ubuntu16\n'),('pollbill','',1505050801,0.43,0,'ubuntu16\n'),('pollbill','',1505050802,0.65,0,'ubuntu16-02\n'),('poll','22',1505050802,1.21,1,'ubuntu16\n'),('poll','25',1505050803,1.27,1,'ubuntu16-02\n'),('poll','24',1505050803,1.25,1,'ubuntu16-02\n'),('poll','23',1505050803,1.39,1,'ubuntu16-02\n'),('poll','28',1505050802,3.66,1,'ubuntu16\n'),('poll','27',1505050803,3.90,1,'ubuntu16-02\n'),('pollbill','',1505051101,1.70,0,'ubuntu16-02\n'),('poll','24',1505051102,1.39,1,'ubuntu16-02\n'),('poll','25',1505051103,1.21,1,'ubuntu16-02\n'),('poll','23',1505051102,2.44,1,'ubuntu16-02\n'),('pollbill','',1505051104,0.65,0,'ubuntu16\n'),('poll','27',1505051102,3.07,1,'ubuntu16-02\n'),('poll','22',1505051104,1.50,1,'ubuntu16\n'),('poll','28',1505051104,3.87,1,'ubuntu16\n'),('pollbill','',1505051402,0.70,0,'ubuntu16\n'),('pollbill','',1505051402,0.88,0,'ubuntu16-02\n'),('poll','22',1505051402,1.42,1,'ubuntu16\n'),('poll','24',1505051403,1.31,1,'ubuntu16-02\n'),('poll','25',1505051403,1.33,1,'ubuntu16-02\n'),('poll','23',1505051403,1.70,1,'ubuntu16-02\n'),('poll','28',1505051402,4.00,1,'ubuntu16\n'),('poll','27',1505051403,4.19,1,'ubuntu16-02\n'),('pollbill','',1505051702,0.62,0,'ubuntu16\n'),('pollbill','',1505051702,1.05,0,'ubuntu16-02\n'),('poll','22',1505051702,1.37,1,'ubuntu16\n'),('poll','24',1505051703,1.31,1,'ubuntu16-02\n'),('poll','25',1505051703,1.29,1,'ubuntu16-02\n'),('poll','23',1505051703,1.68,1,'ubuntu16-02\n'),('poll','28',1505051702,3.86,1,'ubuntu16\n'),('poll','27',1505051703,4.15,1,'ubuntu16-02\n'),('pollbill','',1505052001,2.29,0,'ubuntu16-02\n'),('poll','24',1505052003,1.59,1,'ubuntu16-02\n'),('poll','25',1505052003,1.38,1,'ubuntu16-02\n'),('poll','23',1505052003,2.58,1,'ubuntu16-02\n'),('pollbill','',1505052005,0.38,0,'ubuntu16\n'),('poll','22',1505052005,1.10,1,'ubuntu16\n'),('poll','27',1505052003,4.56,1,'ubuntu16-02\n'),('poll','28',1505052005,3.73,1,'ubuntu16\n'),('pollbill','',1505052302,1.23,0,'ubuntu16-02\n'),('pollbill','',1505052302,0.71,0,'ubuntu16\n'),('poll','24',1505052302,1.60,1,'ubuntu16-02\n'),('poll','25',1505052302,1.40,1,'ubuntu16-02\n'),('poll','22',1505052303,1.38,1,'ubuntu16\n'),('poll','23',1505052302,2.00,1,'ubuntu16-02\n'),('poll','28',1505052303,3.96,1,'ubuntu16\n'),('poll','27',1505052302,4.63,1,'ubuntu16-02\n'),('pollbill','',1505052601,0.75,0,'ubuntu16-02\n'),('pollbill','',1505052602,0.42,0,'ubuntu16\n'),('poll','25',1505052602,1.32,1,'ubuntu16-02\n'),('poll','24',1505052602,1.43,1,'ubuntu16-02\n'),('poll','22',1505052603,1.53,1,'ubuntu16\n'),('poll','23',1505052602,2.54,1,'ubuntu16-02\n'),('poll','27',1505052602,2.79,1,'ubuntu16-02\n'),('poll','28',1505052603,3.88,1,'ubuntu16\n'),('poll','24',1505052903,2.83,1,'ubuntu16-02\n'),('poll','25',1505052903,2.58,1,'ubuntu16-02\n'),('poll','23',1505052903,4.49,1,'ubuntu16-02\n'),('pollbill','',1505052902,5.52,0,'ubuntu16-02\n'),('poll','27',1505052902,5.22,1,'ubuntu16-02\n'),('pollbill','',1505052907,0.64,0,'ubuntu16\n'),('poll','22',1505052907,1.26,1,'ubuntu16\n'),('poll','28',1505052907,3.78,1,'ubuntu16\n'),('pollbill','',1505053202,0.69,0,'ubuntu16\n'),('pollbill','',1505053202,1.25,0,'ubuntu16-02\n'),('poll','22',1505053202,1.43,1,'ubuntu16\n'),('poll','24',1505053203,1.37,1,'ubuntu16-02\n'),('poll','25',1505053203,1.34,1,'ubuntu16-02\n'),('poll','23',1505053203,1.97,1,'ubuntu16-02\n'),('poll','28',1505053202,3.88,1,'ubuntu16\n'),('poll','27',1505053203,4.30,1,'ubuntu16-02\n'),('poll','25',1505053502,2.61,1,'ubuntu16-02\n'),('pollbill','',1505053501,3.40,0,'ubuntu16-02\n'),('poll','24',1505053504,1.38,1,'ubuntu16-02\n'),('pollbill','',1505053506,1.11,0,'ubuntu16\n'),('poll','23',1505053504,3.17,1,'ubuntu16-02\n'),('poll','27',1505053504,3.79,1,'ubuntu16-02\n'),('poll','22',1505053506,1.79,1,'ubuntu16\n'),('poll','28',1505053506,4.15,1,'ubuntu16\n'),('pollbill','',1505094902,1.26,0,'ubuntu16\n'),('pollbill','',1505094902,1.44,0,'ubuntu16-02\n'),('poll','22',1505094902,2.11,1,'ubuntu16\n'),('poll','24',1505094903,1.43,1,'ubuntu16-02\n'),('poll','25',1505094903,1.40,1,'ubuntu16-02\n'),('poll','23',1505094903,2.28,1,'ubuntu16-02\n'),('poll','28',1505094902,4.44,1,'ubuntu16\n'),('poll','27',1505094903,4.59,1,'ubuntu16-02\n'),('pollbill','',1505095203,0.58,0,'ubuntu16\n'),('pollbill','',1505095202,1.74,0,'ubuntu16-02\n'),('poll','22',1505095203,1.63,1,'ubuntu16\n'),('poll','25',1505095203,1.41,1,'ubuntu16-02\n'),('poll','24',1505095203,1.36,1,'ubuntu16-02\n'),('poll','23',1505095203,1.98,1,'ubuntu16-02\n'),('poll','28',1505095203,2.57,1,'ubuntu16\n'),('poll','27',1505095203,4.75,1,'ubuntu16-02\n'),('poll','25',1505095504,13.65,1,'ubuntu16-02\n'),('poll','24',1505095502,19.24,1,'ubuntu16-02\n'),('poll','27',1505095512,18.09,1,'ubuntu16-02\n'),('poll','23',1505095513,20.54,1,'ubuntu16-02\n'),('pollbill','',1505095502,35.91,0,'ubuntu16-02\n'),('pollbill','',1505095534,3.72,0,'ubuntu16\n'),('poll','22',1505095537,1.50,1,'ubuntu16\n'),('poll','28',1505095537,2.30,1,'ubuntu16\n'),('pollbill','',1505095802,0.66,0,'ubuntu16\n'),('poll','22',1505095803,1.42,1,'ubuntu16\n'),('pollbill','',1505095802,0.79,0,'ubuntu16-02\n'),('poll','24',1505095802,1.31,1,'ubuntu16-02\n'),('poll','25',1505095802,1.30,1,'ubuntu16-02\n'),('poll','23',1505095802,1.68,1,'ubuntu16-02\n'),('poll','28',1505095803,4.01,1,'ubuntu16\n'),('poll','27',1505095802,4.25,1,'ubuntu16-02\n'),('poll','22',1505183705,1.73,1,'ubuntu16\n'),('poll','28',1505183705,11.91,1,'ubuntu16\n'),('pollbill','',1505183702,83.57,0,'ubuntu16\n');
/*!40000 ALTER TABLE `perf_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `plugin_id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(60) NOT NULL,
  `plugin_active` int(11) NOT NULL,
  PRIMARY KEY (`plugin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (1,'Test',0);
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poller_groups`
--

DROP TABLE IF EXISTS `poller_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poller_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `descr` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poller_groups`
--

LOCK TABLES `poller_groups` WRITE;
/*!40000 ALTER TABLE `poller_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `poller_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pollers`
--

DROP TABLE IF EXISTS `pollers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poller_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `last_polled` datetime NOT NULL,
  `devices` int(11) NOT NULL,
  `time_taken` double NOT NULL,
  PRIMARY KEY (`poller_name`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pollers`
--

LOCK TABLES `pollers` WRITE;
/*!40000 ALTER TABLE `pollers` DISABLE KEYS */;
INSERT INTO `pollers` VALUES (1,'idempiere-devel\n','2017-06-09 17:00:08',6,5),(3,'ubuntu14-vmware\n','2017-06-09 17:05:06',6,5),(4,'ubuntu14vm\n','2017-06-12 14:20:05',1,2),(2,'ubuntu16\n','2017-09-12 09:35:17',2,14),(5,'ubuntu16-02\n','2017-09-11 09:10:09',4,5);
/*!40000 ALTER TABLE `pollers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `port_association_mode`
--

DROP TABLE IF EXISTS `port_association_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `port_association_mode` (
  `pom_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  PRIMARY KEY (`pom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `port_association_mode`
--

LOCK TABLES `port_association_mode` WRITE;
/*!40000 ALTER TABLE `port_association_mode` DISABLE KEYS */;
INSERT INTO `port_association_mode` VALUES (1,'ifIndex'),(2,'ifName'),(3,'ifDescr'),(4,'ifAlias');
/*!40000 ALTER TABLE `port_association_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports`
--

DROP TABLE IF EXISTS `ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports` (
  `port_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `port_descr_type` varchar(255) DEFAULT NULL,
  `port_descr_descr` varchar(255) DEFAULT NULL,
  `port_descr_circuit` varchar(255) DEFAULT NULL,
  `port_descr_speed` varchar(32) DEFAULT NULL,
  `port_descr_notes` varchar(255) DEFAULT NULL,
  `ifDescr` varchar(255) DEFAULT NULL,
  `ifName` varchar(255) DEFAULT NULL,
  `portName` varchar(128) DEFAULT NULL,
  `ifIndex` bigint(20) DEFAULT '0',
  `ifSpeed` bigint(20) DEFAULT NULL,
  `ifConnectorPresent` varchar(12) DEFAULT NULL,
  `ifPromiscuousMode` varchar(12) DEFAULT NULL,
  `ifHighSpeed` int(11) DEFAULT NULL,
  `ifOperStatus` varchar(16) DEFAULT NULL,
  `ifOperStatus_prev` varchar(16) DEFAULT NULL,
  `ifAdminStatus` varchar(16) DEFAULT NULL,
  `ifAdminStatus_prev` varchar(16) DEFAULT NULL,
  `ifDuplex` varchar(12) DEFAULT NULL,
  `ifMtu` int(11) DEFAULT NULL,
  `ifType` text,
  `ifAlias` text,
  `ifPhysAddress` text,
  `ifHardType` varchar(64) DEFAULT NULL,
  `ifLastChange` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ifVlan` varchar(8) NOT NULL DEFAULT '',
  `ifTrunk` varchar(8) DEFAULT '',
  `ifVrf` int(11) NOT NULL DEFAULT '0',
  `counter_in` int(11) DEFAULT NULL,
  `counter_out` int(11) DEFAULT NULL,
  `ignore` tinyint(1) NOT NULL DEFAULT '0',
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  `detailed` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `pagpOperationMode` varchar(32) DEFAULT NULL,
  `pagpPortState` varchar(16) DEFAULT NULL,
  `pagpPartnerDeviceId` varchar(48) DEFAULT NULL,
  `pagpPartnerLearnMethod` varchar(16) DEFAULT NULL,
  `pagpPartnerIfIndex` int(11) DEFAULT NULL,
  `pagpPartnerGroupIfIndex` int(11) DEFAULT NULL,
  `pagpPartnerDeviceName` varchar(128) DEFAULT NULL,
  `pagpEthcOperationMode` varchar(16) DEFAULT NULL,
  `pagpDeviceId` varchar(48) DEFAULT NULL,
  `pagpGroupIfIndex` int(11) DEFAULT NULL,
  `ifInUcastPkts` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInUcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutUcastPkts` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutUcastPkts_rate` int(11) DEFAULT NULL,
  `ifInErrors` bigint(20) DEFAULT NULL,
  `ifInErrors_prev` bigint(20) DEFAULT NULL,
  `ifInErrors_delta` bigint(20) DEFAULT NULL,
  `ifInErrors_rate` int(11) DEFAULT NULL,
  `ifOutErrors` bigint(20) DEFAULT NULL,
  `ifOutErrors_prev` bigint(20) DEFAULT NULL,
  `ifOutErrors_delta` bigint(20) DEFAULT NULL,
  `ifOutErrors_rate` int(11) DEFAULT NULL,
  `ifInOctets` bigint(20) DEFAULT NULL,
  `ifInOctets_prev` bigint(20) DEFAULT NULL,
  `ifInOctets_delta` bigint(20) DEFAULT NULL,
  `ifInOctets_rate` bigint(20) DEFAULT NULL,
  `ifOutOctets` bigint(20) DEFAULT NULL,
  `ifOutOctets_prev` bigint(20) DEFAULT NULL,
  `ifOutOctets_delta` bigint(20) DEFAULT NULL,
  `ifOutOctets_rate` bigint(20) DEFAULT NULL,
  `poll_time` int(11) DEFAULT NULL,
  `poll_prev` int(11) DEFAULT NULL,
  `poll_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`port_id`),
  UNIQUE KEY `device_ifIndex` (`device_id`,`ifIndex`),
  KEY `if_2` (`ifDescr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports`
--

LOCK TABLES `ports` WRITE;
/*!40000 ALTER TABLE `ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_adsl`
--

DROP TABLE IF EXISTS `ports_adsl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_adsl` (
  `port_id` int(11) NOT NULL,
  `port_adsl_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `adslLineCoding` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslLineType` varchar(16) COLLATE utf8_bin NOT NULL,
  `adslAtucInvVendorID` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAtucInvVersionNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAtucCurrSnrMgn` decimal(5,1) NOT NULL,
  `adslAtucCurrAtn` decimal(5,1) NOT NULL,
  `adslAtucCurrOutputPwr` decimal(5,1) NOT NULL,
  `adslAtucCurrAttainableRate` int(11) NOT NULL,
  `adslAtucChanCurrTxRate` int(11) NOT NULL,
  `adslAturInvSerialNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturInvVendorID` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturInvVersionNumber` varchar(8) COLLATE utf8_bin NOT NULL,
  `adslAturChanCurrTxRate` int(11) NOT NULL,
  `adslAturCurrSnrMgn` decimal(5,1) NOT NULL,
  `adslAturCurrAtn` decimal(5,1) NOT NULL,
  `adslAturCurrOutputPwr` decimal(5,1) NOT NULL,
  `adslAturCurrAttainableRate` int(11) NOT NULL,
  UNIQUE KEY `interface_id` (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_adsl`
--

LOCK TABLES `ports_adsl` WRITE;
/*!40000 ALTER TABLE `ports_adsl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_adsl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_perms`
--

DROP TABLE IF EXISTS `ports_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_perms` (
  `user_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `access_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_perms`
--

LOCK TABLES `ports_perms` WRITE;
/*!40000 ALTER TABLE `ports_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_stack`
--

DROP TABLE IF EXISTS `ports_stack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_stack` (
  `device_id` int(11) NOT NULL,
  `port_id_high` int(11) NOT NULL,
  `port_id_low` int(11) NOT NULL,
  `ifStackStatus` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `device_id` (`device_id`,`port_id_high`,`port_id_low`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_stack`
--

LOCK TABLES `ports_stack` WRITE;
/*!40000 ALTER TABLE `ports_stack` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_stack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_statistics`
--

DROP TABLE IF EXISTS `ports_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_statistics` (
  `port_id` int(11) NOT NULL,
  `ifInNUcastPkts` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInNUcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutNUcastPkts` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutNUcastPkts_rate` int(11) DEFAULT NULL,
  `ifInDiscards` bigint(20) DEFAULT NULL,
  `ifInDiscards_prev` bigint(20) DEFAULT NULL,
  `ifInDiscards_delta` bigint(20) DEFAULT NULL,
  `ifInDiscards_rate` int(11) DEFAULT NULL,
  `ifOutDiscards` bigint(20) DEFAULT NULL,
  `ifOutDiscards_prev` bigint(20) DEFAULT NULL,
  `ifOutDiscards_delta` bigint(20) DEFAULT NULL,
  `ifOutDiscards_rate` int(11) DEFAULT NULL,
  `ifInUnknownProtos` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_prev` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_delta` bigint(20) DEFAULT NULL,
  `ifInUnknownProtos_rate` int(11) DEFAULT NULL,
  `ifInBroadcastPkts` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInBroadcastPkts_rate` int(11) DEFAULT NULL,
  `ifOutBroadcastPkts` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutBroadcastPkts_rate` int(11) DEFAULT NULL,
  `ifInMulticastPkts` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_prev` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_delta` bigint(20) DEFAULT NULL,
  `ifInMulticastPkts_rate` int(11) DEFAULT NULL,
  `ifOutMulticastPkts` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_prev` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_delta` bigint(20) DEFAULT NULL,
  `ifOutMulticastPkts_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_statistics`
--

LOCK TABLES `ports_statistics` WRITE;
/*!40000 ALTER TABLE `ports_statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_stp`
--

DROP TABLE IF EXISTS `ports_stp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_stp` (
  `port_stp_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL,
  `state` varchar(11) NOT NULL,
  `enable` varchar(8) NOT NULL,
  `pathCost` int(10) unsigned NOT NULL,
  `designatedRoot` varchar(32) NOT NULL,
  `designatedCost` smallint(5) unsigned NOT NULL,
  `designatedBridge` varchar(32) NOT NULL,
  `designatedPort` mediumint(9) NOT NULL,
  `forwardTransitions` int(10) unsigned NOT NULL,
  PRIMARY KEY (`port_stp_id`),
  UNIQUE KEY `device_id` (`device_id`,`port_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_stp`
--

LOCK TABLES `ports_stp` WRITE;
/*!40000 ALTER TABLE `ports_stp` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_stp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ports_vlans`
--

DROP TABLE IF EXISTS `ports_vlans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ports_vlans` (
  `port_vlan_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `vlan` int(11) NOT NULL,
  `baseport` int(11) NOT NULL,
  `priority` bigint(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cost` int(11) NOT NULL,
  `untagged` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`port_vlan_id`),
  UNIQUE KEY `unique` (`device_id`,`port_id`,`vlan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ports_vlans`
--

LOCK TABLES `ports_vlans` WRITE;
/*!40000 ALTER TABLE `ports_vlans` DISABLE KEYS */;
/*!40000 ALTER TABLE `ports_vlans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processes`
--

DROP TABLE IF EXISTS `processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processes` (
  `device_id` int(11) NOT NULL,
  `pid` int(255) NOT NULL,
  `vsz` int(255) NOT NULL,
  `rss` int(255) NOT NULL,
  `cputime` varchar(12) COLLATE utf8_bin NOT NULL,
  `user` varchar(50) COLLATE utf8_bin NOT NULL,
  `command` varchar(255) COLLATE utf8_bin NOT NULL,
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processes`
--

LOCK TABLES `processes` WRITE;
/*!40000 ALTER TABLE `processes` DISABLE KEYS */;
/*!40000 ALTER TABLE `processes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processors`
--

DROP TABLE IF EXISTS `processors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processors` (
  `processor_id` int(11) NOT NULL AUTO_INCREMENT,
  `entPhysicalIndex` int(11) NOT NULL DEFAULT '0',
  `hrDeviceIndex` int(11) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `processor_oid` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `processor_index` varchar(32) CHARACTER SET latin1 NOT NULL,
  `processor_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `processor_usage` int(11) NOT NULL,
  `processor_descr` varchar(64) CHARACTER SET latin1 NOT NULL,
  `processor_precision` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`processor_id`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processors`
--

LOCK TABLES `processors` WRITE;
/*!40000 ALTER TABLE `processors` DISABLE KEYS */;
/*!40000 ALTER TABLE `processors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxmox`
--

DROP TABLE IF EXISTS `proxmox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxmox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `vmid` int(11) NOT NULL,
  `cluster` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cluster_vm` (`cluster`,`vmid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxmox`
--

LOCK TABLES `proxmox` WRITE;
/*!40000 ALTER TABLE `proxmox` DISABLE KEYS */;
/*!40000 ALTER TABLE `proxmox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proxmox_ports`
--

DROP TABLE IF EXISTS `proxmox_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxmox_ports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vm_id` int(11) NOT NULL,
  `port` varchar(10) NOT NULL,
  `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vm_port` (`vm_id`,`port`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxmox_ports`
--

LOCK TABLES `proxmox_ports` WRITE;
/*!40000 ALTER TABLE `proxmox_ports` DISABLE KEYS */;
/*!40000 ALTER TABLE `proxmox_ports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pseudowires`
--

DROP TABLE IF EXISTS `pseudowires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pseudowires` (
  `pseudowire_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `port_id` int(11) NOT NULL,
  `peer_device_id` int(11) NOT NULL,
  `peer_ldp_id` int(11) NOT NULL,
  `cpwVcID` int(11) NOT NULL,
  `cpwOid` int(11) NOT NULL,
  `pw_type` varchar(32) NOT NULL,
  `pw_psntype` varchar(32) NOT NULL,
  `pw_local_mtu` int(11) NOT NULL,
  `pw_peer_mtu` int(11) NOT NULL,
  `pw_descr` varchar(128) NOT NULL,
  PRIMARY KEY (`pseudowire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pseudowires`
--

LOCK TABLES `pseudowires` WRITE;
/*!40000 ALTER TABLE `pseudowires` DISABLE KEYS */;
/*!40000 ALTER TABLE `pseudowires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `device_id` int(11) NOT NULL,
  `context_name` varchar(128) NOT NULL,
  `ipRouteDest` varchar(256) NOT NULL,
  `ipRouteIfIndex` varchar(256) DEFAULT NULL,
  `ipRouteMetric` varchar(256) NOT NULL,
  `ipRouteNextHop` varchar(256) NOT NULL,
  `ipRouteType` varchar(256) NOT NULL,
  `ipRouteProto` varchar(256) NOT NULL,
  `discoveredAt` int(11) NOT NULL,
  `ipRouteMask` varchar(256) NOT NULL,
  KEY `device` (`device_id`,`context_name`,`ipRouteDest`(255),`ipRouteNextHop`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `sensor_class` varchar(64) CHARACTER SET latin1 NOT NULL,
  `device_id` int(11) unsigned NOT NULL DEFAULT '0',
  `poller_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'snmp',
  `sensor_oid` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sensor_index` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sensor_descr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_divisor` int(11) NOT NULL DEFAULT '1',
  `sensor_multiplier` int(11) NOT NULL DEFAULT '1',
  `sensor_current` float DEFAULT NULL,
  `sensor_limit` float DEFAULT NULL,
  `sensor_limit_warn` float DEFAULT NULL,
  `sensor_limit_low` float DEFAULT NULL,
  `sensor_limit_low_warn` float DEFAULT NULL,
  `sensor_alert` tinyint(1) NOT NULL DEFAULT '1',
  `sensor_custom` enum('No','Yes') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `entPhysicalIndex` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `entPhysicalIndex_measured` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sensor_prev` float DEFAULT NULL,
  PRIMARY KEY (`sensor_id`),
  KEY `sensor_host` (`device_id`),
  KEY `sensor_class` (`sensor_class`),
  KEY `sensor_type` (`sensor_type`),
  CONSTRAINT `sensors_device_id_foreign` FOREIGN KEY (`device_id`) REFERENCES `devices` (`device_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors`
--

LOCK TABLES `sensors` WRITE;
/*!40000 ALTER TABLE `sensors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensors_to_state_indexes`
--

DROP TABLE IF EXISTS `sensors_to_state_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors_to_state_indexes` (
  `sensors_to_state_translations_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id` int(11) NOT NULL,
  `state_index_id` int(11) NOT NULL,
  PRIMARY KEY (`sensors_to_state_translations_id`),
  UNIQUE KEY `sensor_id_state_index_id` (`sensor_id`,`state_index_id`),
  KEY `state_index_id` (`state_index_id`),
  CONSTRAINT `sensors_to_state_indexes_ibfk_2` FOREIGN KEY (`state_index_id`) REFERENCES `state_indexes` (`state_index_id`),
  CONSTRAINT `sensors_to_state_indexes_sensor_id_foreign` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`sensor_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensors_to_state_indexes`
--

LOCK TABLES `sensors_to_state_indexes` WRITE;
/*!40000 ALTER TABLE `sensors_to_state_indexes` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensors_to_state_indexes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `service_ip` text NOT NULL,
  `service_type` varchar(16) NOT NULL,
  `service_desc` text NOT NULL,
  `service_param` text NOT NULL,
  `service_ignore` tinyint(1) NOT NULL,
  `service_status` tinyint(4) NOT NULL DEFAULT '0',
  `service_changed` int(11) NOT NULL DEFAULT '0',
  `service_message` text NOT NULL,
  `service_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `service_ds` varchar(400) NOT NULL COMMENT 'Data Sources available for this service',
  PRIMARY KEY (`service_id`),
  KEY `service_host` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_username` varchar(30) NOT NULL,
  `session_value` varchar(60) NOT NULL,
  `session_token` varchar(60) NOT NULL,
  `session_auth` varchar(16) NOT NULL,
  `session_expiry` int(11) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_value` (`session_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slas`
--

DROP TABLE IF EXISTS `slas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slas` (
  `sla_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `sla_nr` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8_bin NOT NULL,
  `tag` varchar(255) COLLATE utf8_bin NOT NULL,
  `rtt_type` varchar(16) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL,
  `opstatus` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sla_id`),
  UNIQUE KEY `unique_key` (`device_id`,`sla_nr`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slas`
--

LOCK TABLES `slas` WRITE;
/*!40000 ALTER TABLE `slas` DISABLE KEYS */;
/*!40000 ALTER TABLE `slas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_indexes`
--

DROP TABLE IF EXISTS `state_indexes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_indexes` (
  `state_index_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`state_index_id`),
  UNIQUE KEY `state_name` (`state_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_indexes`
--

LOCK TABLES `state_indexes` WRITE;
/*!40000 ALTER TABLE `state_indexes` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_indexes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_translations`
--

DROP TABLE IF EXISTS `state_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_translations` (
  `state_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_index_id` int(11) NOT NULL,
  `state_descr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_draw_graph` tinyint(1) NOT NULL,
  `state_value` smallint(5) unsigned NOT NULL,
  `state_generic_value` tinyint(1) NOT NULL,
  `state_lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`state_translation_id`),
  UNIQUE KEY `state_index_id_value` (`state_index_id`,`state_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_translations`
--

LOCK TABLES `state_translations` WRITE;
/*!40000 ALTER TABLE `state_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage`
--

DROP TABLE IF EXISTS `storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `storage` (
  `storage_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `storage_mib` varchar(16) NOT NULL,
  `storage_index` int(11) NOT NULL,
  `storage_type` varchar(32) DEFAULT NULL,
  `storage_descr` text NOT NULL,
  `storage_size` bigint(20) NOT NULL,
  `storage_units` int(11) NOT NULL,
  `storage_used` bigint(20) NOT NULL DEFAULT '0',
  `storage_free` bigint(20) NOT NULL DEFAULT '0',
  `storage_perc` int(11) NOT NULL DEFAULT '0',
  `storage_perc_warn` int(11) DEFAULT '60',
  `storage_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`storage_id`),
  UNIQUE KEY `index_unique` (`device_id`,`storage_mib`,`storage_index`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage`
--

LOCK TABLES `storage` WRITE;
/*!40000 ALTER TABLE `storage` DISABLE KEYS */;
/*!40000 ALTER TABLE `storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stp`
--

DROP TABLE IF EXISTS `stp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stp` (
  `stp_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `rootBridge` tinyint(1) NOT NULL,
  `bridgeAddress` varchar(32) NOT NULL,
  `protocolSpecification` varchar(16) NOT NULL,
  `priority` mediumint(9) NOT NULL,
  `timeSinceTopologyChange` int(10) unsigned NOT NULL,
  `topChanges` mediumint(9) NOT NULL,
  `designatedRoot` varchar(32) NOT NULL,
  `rootCost` mediumint(9) NOT NULL,
  `rootPort` mediumint(9) NOT NULL,
  `maxAge` mediumint(9) NOT NULL,
  `helloTime` mediumint(9) NOT NULL,
  `holdTime` mediumint(9) NOT NULL,
  `forwardDelay` mediumint(9) NOT NULL,
  `bridgeMaxAge` smallint(6) NOT NULL,
  `bridgeHelloTime` smallint(6) NOT NULL,
  `bridgeForwardDelay` smallint(6) NOT NULL,
  PRIMARY KEY (`stp_id`),
  KEY `stp_host` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stp`
--

LOCK TABLES `stp` WRITE;
/*!40000 ALTER TABLE `stp` DISABLE KEYS */;
/*!40000 ALTER TABLE `stp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syslog`
--

DROP TABLE IF EXISTS `syslog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `syslog` (
  `device_id` int(11) DEFAULT NULL,
  `facility` varchar(10) DEFAULT NULL,
  `priority` varchar(10) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `tag` varchar(10) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `program` varchar(32) DEFAULT NULL,
  `msg` text,
  `seq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`seq`),
  KEY `datetime` (`timestamp`),
  KEY `device_id` (`device_id`),
  KEY `program` (`program`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syslog`
--

LOCK TABLES `syslog` WRITE;
/*!40000 ALTER TABLE `syslog` DISABLE KEYS */;
/*!40000 ALTER TABLE `syslog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toner`
--

DROP TABLE IF EXISTS `toner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toner` (
  `toner_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL DEFAULT '0',
  `toner_index` int(11) NOT NULL,
  `toner_type` varchar(64) NOT NULL,
  `toner_oid` varchar(64) NOT NULL,
  `toner_descr` varchar(32) NOT NULL DEFAULT '',
  `toner_capacity` int(11) NOT NULL DEFAULT '0',
  `toner_current` int(11) NOT NULL DEFAULT '0',
  `toner_capacity_oid` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`toner_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toner`
--

LOCK TABLES `toner` WRITE;
/*!40000 ALTER TABLE `toner` DISABLE KEYS */;
/*!40000 ALTER TABLE `toner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tselregion`
--

DROP TABLE IF EXISTS `tselregion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tselregion` (
  `tselregion_id` int(11) NOT NULL AUTO_INCREMENT,
  `tselregion_name` varchar(45) NOT NULL,
  PRIMARY KEY (`tselregion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tselregion`
--

LOCK TABLES `tselregion` WRITE;
/*!40000 ALTER TABLE `tselregion` DISABLE KEYS */;
INSERT INTO `tselregion` VALUES (1,'SUMBAGSEL'),(2,'SUMBAGTENG'),(3,'SUMBAGUT');
/*!40000 ALTER TABLE `tselregion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ucd_diskio`
--

DROP TABLE IF EXISTS `ucd_diskio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ucd_diskio` (
  `diskio_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `diskio_index` int(11) NOT NULL,
  `diskio_descr` varchar(32) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`diskio_id`),
  KEY `device_id` (`device_id`),
  KEY `device_id_2` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ucd_diskio`
--

LOCK TABLES `ucd_diskio` WRITE;
/*!40000 ALTER TABLE `ucd_diskio` DISABLE KEYS */;
/*!40000 ALTER TABLE `ucd_diskio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `realname` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `descr` varchar(200) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `can_modify_passwd` tinyint(4) NOT NULL DEFAULT '1',
  `twofactor` varchar(255) NOT NULL,
  `dashboard` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$P$Bd8WoWNF4FzJXKplpR/9WAV9nG2oUK.','','rinto@sinaga.or.id','',10,1,'0',0),(2,'rintoexa','$P$BUXBeo92UcKtZS6F.LgPYgONNgU1ov1','Rinto Exandi','','',5,1,'0',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_prefs`
--

DROP TABLE IF EXISTS `users_prefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_prefs` (
  `user_id` int(16) NOT NULL,
  `pref` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id.pref` (`user_id`,`pref`),
  KEY `pref` (`pref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_prefs`
--

LOCK TABLES `users_prefs` WRITE;
/*!40000 ALTER TABLE `users_prefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_prefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_widgets`
--

DROP TABLE IF EXISTS `users_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_widgets` (
  `user_widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `widget_id` int(11) NOT NULL,
  `col` tinyint(4) NOT NULL,
  `row` tinyint(4) NOT NULL,
  `size_x` tinyint(4) NOT NULL,
  `size_y` tinyint(4) NOT NULL,
  `title` varchar(255) NOT NULL,
  `refresh` tinyint(4) NOT NULL DEFAULT '60',
  `settings` text NOT NULL,
  `dashboard_id` int(11) NOT NULL,
  PRIMARY KEY (`user_widget_id`),
  KEY `user_id` (`user_id`,`widget_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_widgets`
--

LOCK TABLES `users_widgets` WRITE;
/*!40000 ALTER TABLE `users_widgets` DISABLE KEYS */;
INSERT INTO `users_widgets` VALUES (20,2,16,1,1,6,3,'Bill Overview',60,'',2),(37,1,18,13,4,6,3,'Top Occupancy',60,'{\"title\":\"Top Occupancy\",\"witel_id\":\"24\",\"device_count\":\"10\"}',3),(41,2,18,7,1,6,3,'Top Occupancy',60,'',2),(69,1,19,13,1,6,3,'Occupancy Overview',60,'{\"title\":\"\",\"device_count\":\"10\"}',3),(79,1,20,1,1,12,6,'Occupancy By Witel',60,'',3);
/*!40000 ALTER TABLE `users_widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vlans`
--

DROP TABLE IF EXISTS `vlans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vlans` (
  `vlan_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) DEFAULT NULL,
  `vlan_vlan` int(11) DEFAULT NULL,
  `vlan_domain` int(11) DEFAULT NULL,
  `vlan_name` varchar(64) DEFAULT NULL,
  `vlan_type` varchar(16) DEFAULT NULL,
  `vlan_mtu` int(11) DEFAULT NULL,
  PRIMARY KEY (`vlan_id`),
  KEY `device_id` (`device_id`,`vlan_vlan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vlans`
--

LOCK TABLES `vlans` WRITE;
/*!40000 ALTER TABLE `vlans` DISABLE KEYS */;
/*!40000 ALTER TABLE `vlans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vminfo`
--

DROP TABLE IF EXISTS `vminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vminfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `vm_type` varchar(16) NOT NULL DEFAULT 'vmware',
  `vmwVmVMID` int(11) NOT NULL,
  `vmwVmDisplayName` varchar(128) NOT NULL,
  `vmwVmGuestOS` varchar(128) NOT NULL,
  `vmwVmMemSize` int(11) NOT NULL,
  `vmwVmCpus` int(11) NOT NULL,
  `vmwVmState` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_id` (`device_id`),
  KEY `vmwVmVMID` (`vmwVmVMID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vminfo`
--

LOCK TABLES `vminfo` WRITE;
/*!40000 ALTER TABLE `vminfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vminfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrf_lite_cisco`
--

DROP TABLE IF EXISTS `vrf_lite_cisco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrf_lite_cisco` (
  `vrf_lite_cisco_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `context_name` varchar(128) NOT NULL,
  `intance_name` varchar(128) DEFAULT '',
  `vrf_name` varchar(128) DEFAULT 'Default',
  PRIMARY KEY (`vrf_lite_cisco_id`),
  KEY `vrf` (`vrf_name`),
  KEY `context` (`context_name`),
  KEY `device` (`device_id`),
  KEY `mix` (`device_id`,`context_name`,`vrf_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrf_lite_cisco`
--

LOCK TABLES `vrf_lite_cisco` WRITE;
/*!40000 ALTER TABLE `vrf_lite_cisco` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrf_lite_cisco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vrfs`
--

DROP TABLE IF EXISTS `vrfs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vrfs` (
  `vrf_id` int(11) NOT NULL AUTO_INCREMENT,
  `vrf_oid` varchar(256) NOT NULL,
  `vrf_name` varchar(128) DEFAULT NULL,
  `mplsVpnVrfRouteDistinguisher` varchar(128) DEFAULT NULL,
  `mplsVpnVrfDescription` text NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`vrf_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vrfs`
--

LOCK TABLES `vrfs` WRITE;
/*!40000 ALTER TABLE `vrfs` DISABLE KEYS */;
/*!40000 ALTER TABLE `vrfs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `widget_id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_title` varchar(255) NOT NULL,
  `widget` varchar(255) NOT NULL,
  `base_dimensions` varchar(10) NOT NULL,
  PRIMARY KEY (`widget_id`),
  UNIQUE KEY `widget` (`widget`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,'Availability map','availability-map','6,3'),(2,'Device summary horizontal','device-summary-horiz','6,3'),(3,'Alerts','alerts','6,3'),(4,'Device summary vertical','device-summary-vert','6,3'),(5,'Globe map','globe','6,3'),(6,'Syslog','syslog','6,3'),(7,'Eventlog','eventlog','6,3'),(8,'World map','worldmap','6,3'),(9,'Graylog','graylog','6,3'),(10,'Graph','generic-graph','6,3'),(11,'Top Devices','top-devices','6,3'),(12,'Top Interfaces','top-interfaces','6,3'),(13,'Notes','notes','6,3'),(14,'External Images','generic-image','6,3'),(15,'Component Status','component-status','6,3'),(16,'Bill Overview','bill-summary-witel','12,3'),(18,'Top Occupancy','top-occupancies','6,3'),(19,'Occupancy Overview','occupancy-overview','6,3'),(20,'Occupancy By Witel','occupancy-overview-bywitel','12,6');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `witel`
--

DROP TABLE IF EXISTS `witel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `witel` (
  `witel_id` int(11) NOT NULL AUTO_INCREMENT,
  `witelname` varchar(35) NOT NULL,
  PRIMARY KEY (`witel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `witel`
--

LOCK TABLES `witel` WRITE;
/*!40000 ALTER TABLE `witel` DISABLE KEYS */;
INSERT INTO `witel` VALUES (19,'Aceh'),(20,'Bandar Lampung'),(21,'Batam'),(22,'Bengkulu'),(23,'Jambi'),(24,'Medan'),(25,'Padang'),(26,'Palembang'),(27,'Pangkal Pinang'),(28,'Pekanbaru'),(29,'Pematangsiantar');
/*!40000 ALTER TABLE `witel` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-12 18:30:06
