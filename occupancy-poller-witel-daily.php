#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo "Starting Polling Occupancy Witel Daily ... \n\n";

$query_witel="SELECT DISTINCT w.witel_id,w.witelname,
        
        COALESCE(bd1.numofnodb,0) as _25,
        COALESCE(bd2.numofnodb,0) as _2550,
        COALESCE(bd3.numofnodb,0) as _5075,
        COALESCE(bd4.numofnodb,0) as _75,
        DAY(NOW()) as period_d,
        MONTH(NOW()) as period_m,
        YEAR(NOW()) as period_y
        
    FROM witel w 
		LEFT JOIN devices d ON d.witel_id = w.witel_id
        LEFT JOIN ports p ON p.device_id = d.device_id
        LEFT JOIN bill_ports bp ON bp.port_id = p.port_id
        LEFT JOIN bills b ON b.bill_id=bp.bill_id      
        LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage<=25 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
			 ) bd1 ON w.witel_id=bd1.witel_id
		 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>25 AND b1.percentage<=50 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
			 ) bd2 ON w.witel_id=bd2.witel_id
		 LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>50 AND b1.percentage<=75 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
		 ) bd3 ON w.witel_id=bd3.witel_id

		LEFT JOIN (SELECT COUNT(b1.bill_id) as numofnodb,w2.witel_id FROM bill_daily b1
				JOIN bill_ports bp ON b1.bill_id = bp.bill_id
				JOIN ports p ON bp.port_id = p.port_id
				JOIN devices dev ON p.device_id = dev.device_id
				JOIN witel w2 ON dev.witel_id = w2.witel_id
				WHERE b1.percentage>75 AND b1.period_d=DAY(NOW()) AND b1.period_m=MONTH(NOW()) AND b1.period_y=YEAR(NOW())
                GROUP BY w2.witel_id
		 ) bd4 ON w.witel_id=bd4.witel_id";


foreach (dbFetchRows($query_witel) as $witel) {
        echo str_pad($witel['witel_id'].' '.$witel['witelname'], 30)." \n";
   
        unset($class);
        unset($rate_data);
   
        $_25 = $witel['_25'];
        $_2550 = $witel['_2550'];
        $_5075 = $witel['_5075'];
        $_75 = $witel['_75'];
        
        $period_d=$witel['period_d'];
        $period_m=$witel['period_m'];
        $period_y=$witel['period_y'];
        
        $witel_id=$witel['witel_id'];

        $now = dbFetchCell('SELECT NOW()');

        $rowExisting = billWitelDailyIsExist($witel_id);

        if ($rowExisting['exist']=='ok') { //Record Existing
            
                
                $fields = array('bill_day'=>$now,'percent25'=>$_25,'percent2550'=>$_2550,'percent5075'=>$_5075,'percent75'=>$_75,'last_updated'=>$now);
                
                dbUpdate($fields,'bill_witel_daily',"`witel_id`='" .$witel_id ."' AND `period_d`='" . $period_d . "' AND  `period_m`='" . $period_m . "' AND `period_y`='" . $period_y . "'");
       
                echo ' Updating Bill Witel Daily ! ';
        }

        else { //New Record
              
                $witel_daily = array(
                    'witel_id' => $witel_id,
                    'bill_day'=>$now,
                    'percent25'=> $_25,  
                    'percent2550'=>$_2550,
                    'percent5075'=>$_5075,
                    'percent75'=>$_75,
                    'period_d'=>$period_d,
                    'period_m'=>$period_m,
                    'period_y'=>$period_y,
                    'last_updated'=>$now,
                );            
                
                dbInsert($witel_daily,'bill_witel_daily');
           
                echo 'Bill Witel Daily Insert! ';
        } 

   echo "\n\n";        
   
}//end foreach
