#!/usr/bin/env php
<?php

/**
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 * 
 */

$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);

$options = getopt('h:m:i:n:r::d::v::a::f::q');

if ($options['h'] == 'all') {
    $where = ' ';
    $doing = 'all';
} elseif ($options['h']) {
    if (is_numeric($options['h'])) {
        $where = " AND B.bill_id = ".$options['h'];
        $doing = $options['h'];
    } else {
        if (preg_match('/\*/', $options['h'])) {
            $where = " AND D.hostname LIKE '".str_replace('*', '%', mres($options['h']))."'";
        } else {
            $where = " AND D.hostname = '".mres($options['h'])."'";
        }
        $doing = $options['h'];
    }
}


if (!$where) {
    echo "-h ip-address-device                              Poll single device\n";
    echo "\n";
    echo "No polling type specified!\n";
    exit;
}

$iter = '0';

echo "Starting Polling Occupancy ... \n\n";

$query = "SELECT B.bill_id,D.hostname FROM bills B INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id INNER JOIN  ports ON bill_ports.port_id=ports.port_id INNER JOIN devices AS D ON ports.device_id=D.device_id WHERE D.disabled = 0";

if ($where) {
     $query  .= $where; 
}

if(isset($options['h'])) 
{
    foreach (dbFetchRows($query) as $bill_data) {
        echo 'Occupancy Poller for Bill ID : ' . $bill_data['bill_id'] . '   IP Address: '.$bill_data['hostname'];
        echo "\n";
        OccupancyPoller($bill_data['bill_id']);
        
        $iter++;
    }

}


$poller_end  = microtime(true);
$poller_run  = ($poller_end - $poller_start);
$poller_time = substr($poller_run, 0, 5);

dbInsert(array('type' => 'occupancypoller', 'doing' => $doing, 'start' => $poller_start, 'duration' => $poller_time, 'devices' => 0, 'poller' => $config['distributed_poller_name'] ), 'perf_times');
if ($poller_time > 300) {
    logfile("BILLING: occupancy poller took longer than 5 minutes ($poller_time seconds)!");
}
echo "\nCompleted in $poller_time sec\n";