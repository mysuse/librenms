#!/usr/bin/env php
<?php

/*
 * LibreNMS
 *
 *   This file is part of LibreNMS.
 *
 * @package    LibreNMS
 * @subpackage billing
 * @copyright  (C) 2006 - 2012 Adam Armstrong
 */

// FIXME - implement cli switches, debugging, etc.
$init_modules = array();
require __DIR__ . '/includes/init.php';

$poller_start = microtime(true);
echo $config['project_name_version']." Poller\n";
$versions = version_info(false);
echo "Version info:\n";
$cur_sha = $versions['local_sha'];
echo "Commit SHA: $cur_sha\n";
echo "Commit Date: ".$versions['local_date']."\n";
echo "DB Schema: ".$versions['db_schema']."\n";
echo "PHP: ".$versions['php_ver']."\n";
echo "MySQL: ".$versions['mysql_ver']."\n";
echo "RRDTool: ".$versions['rrdtool_ver']."\n";
echo "SNMP: ".$versions['netsnmp_ver']."\n";

$options = getopt('h:m:i:n:r::d::v::a::f::q');

if ($options['h'] == 'all') {
    $where = ' ';
    $doing = 'all';
} elseif ($options['h']) {
    if (is_numeric($options['h'])) {
        $where = " AND B.bill_id = ".$options['h'];
        $doing = $options['h'];
    } else {
        if (preg_match('/\*/', $options['h'])) {
            $where = " AND D.hostname LIKE '".str_replace('*', '%', mres($options['h']))."'";
        } else {
            $where = " AND D.hostname = '".mres($options['h'])."'";
        }
        $doing = $options['h'];
    }
}


if (!$where) {
    echo "-h <device id>                               Poll single device\n";
    echo "-h all                                       Poll all devices\n\n";
    echo "\n";
    echo "No polling type specified!\n";
    exit;
}

$iter = '0';

//rrdtool_initialize();

$poller_start = microtime(true);
echo "Starting Polling Session ... \n\n";
// Wait for schema update, as running during update can break update
$dbVersion = dbFetchCell('SELECT version FROM dbSchema');
if ($dbVersion < 107) {
    logfile("BILLING: Cannot continue until dbSchema update to >= 107 is complete");
    exit(1);
}

$query = "SELECT B.bill_id FROM bills B INNER JOIN bill_ports ON B.bill_id=bill_ports.bill_id INNER JOIN  ports ON bill_ports.port_id=ports.port_id INNER JOIN devices AS D ON ports.device_id=D.device_id WHERE D.disabled = 0";

if ($where) {
     $query  .= $where; 
}

foreach (dbFetchRows($query) as $bill_data) {
    echo 'Bill ID : ' . $bill_data['bill_id'] . '   Bill Name: '.$bill_data['bill_name']."\n";

    // replace old bill_gb with bill_quota (we're now storing bytes, not gigabytes)
    if ($bill_data['bill_type'] == 'quota' && !is_numeric($bill_data['bill_quota'])) {
        $bill_data['bill_quota'] = ($bill_data['bill_gb'] * $config['billing']['base'] * $config['billing']['base']);
        dbUpdate(array('bill_quota' => $bill_data['bill_quota']), 'bills', '`bill_id` = ?', array($bill_data['bill_id']));
        echo 'Quota -> '.$bill_data['bill_quota'];
    }

     CollectData($bill_data['bill_id']);
    
    $iter++;
}

if ($argv[1]) {
    CollectData($argv[1]);
}

$poller_end  = microtime(true);
$poller_run  = ($poller_end - $poller_start);
$poller_time = substr($poller_run, 0, 5);

dbInsert(array('type' => 'pollbill', 'doing' => $doing, 'start' => $poller_start, 'duration' => $poller_time, 'devices' => 0, 'poller' => $config['distributed_poller_name'] ), 'perf_times');
if ($poller_time > 300) {
    logfile("BILLING: polling took longer than 5 minutes ($poller_time seconds)!");
}
echo "\nCompleted in $poller_time sec\n";
//rrdtool_close();
